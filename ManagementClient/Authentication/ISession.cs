﻿using System;
using System.Collections.Generic;
using Authentication.Accessibility;
using XmlCommunicationManager.Authorization;

namespace Authentication
{
  public interface ISession
  {
    /// <summary>
    /// Istanza dell'utente legato alla sessione
    /// </summary>
    User User { get; }

    /// <summary>
    /// Istanza della sorgente della sessione
    /// </summary>
    Source Source { get; }

    /// <summary>
    /// SessionTokens associati alla sessione
    /// </summary>
    SessionTokens SessionTokens { get; }

    /// <summary>
    /// Indica se la sessione è attiva
    /// </summary>
    bool IsActive { get; set; }

    /// <summary>
    /// Ultima attività della sessione
    /// </summary>
    DateTime LastActivity { get; }

    #region Position

    /// <summary>
    /// Restituisce tutte le posizioni associate alla sessione stessa
    /// </summary>
    IList<string> PositionCollection { get; }

    /// <summary>
    /// Restituisce il numero di posizioni attuali
    /// </summary>
    int GetPositionsCount();

    /// <summary>
    /// Aggiunge una posizione alla sessione
    /// </summary>
    /// <returns>True se aggiunta, altrimenti false</returns>
    bool AddPosition(string position);

    /// <summary>
    /// Aggiunge una collezione di posizioni
    /// </summary>
    /// <returns>La collezione delle posizioni aggiunte</returns>
    IList<string> AddRangePosition(IEnumerable<string> collection);

    /// <summary>
    /// Aggiunge una collezione di posizioni
    /// </summary>
    /// <returns>La collezione delle posizioni aggiunte</returns>
    IList<string> AddRangePosition(params string[] collection);

    /// <summary>
    /// Restituisce la prima posizione aggiunta 
    /// </summary>
    string GetFirstPosition();

    /// <summary>
    /// Restituisce le prime n posizioni aggiunta 
    /// </summary>
    IList<string> GetFirstPosition(int count);

    /// <summary>
    /// Restituisce l'ultima posizione aggiunta 
    /// </summary>
    string GetLastPosition();

    /// <summary>
    /// Restituisce le ultime n posizioni aggiunte
    /// </summary>
    IList<string> GetLastPosition(int count);

    /// <summary>
    /// Restituisce le posizioni non contenute
    /// </summary>
    IList<string> GetNotContainedPositions(IEnumerable<string> collection);

    /// <summary>
    /// Restituisce la posizione all'indice indicato
    /// </summary>
    string PositionAt(int index);

    /// <summary>
    /// Indica se la posizione è contenuta
    /// </summary>
    bool ContainsPosition(string position);

    /// <summary>
    /// Indica se le posizioni passate sono contenute, restituisce le posizioni contenute
    /// </summary>
    IList<string> ContainsRangePosition(IEnumerable<string> collection);

    /// <summary>
    /// Indica se le posizioni passate sono contenute, restituisce le posizioni contenute
    /// </summary>
    IList<string> ContainsRangePosition(params string[] collection);

    /// <summary>
    /// Indica se tutte le posizioni passate sono contenute
    /// </summary>
    bool ContainsAllPositions(IEnumerable<string> collection);

    /// <summary>
    /// Rimuove una posizione dalla collezione
    /// </summary>
    bool RemovePosition(string position);

    /// <summary>
    /// Rimuove un range di posizioni
    /// </summary>
    IList<string> RemoveRangePosition(IEnumerable<string> collection);

    /// <summary>
    /// Rimuove un range di posizioni
    /// </summary>
    IList<string> RemoveRangePosition(params string[] collection);

    #endregion
  }
}