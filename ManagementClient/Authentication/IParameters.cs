﻿using System.Collections.Generic;
using XmlCommunicationManager.Authorization;

namespace Authentication
{
  /// <summary>
  /// Restituisce la lista dei parametri da spedire al server
  /// </summary>
  public interface IParameters
  {
    IList<string> GetParameters();
  }

  public interface ILoginParameters : IParameters
  {
    ICredentials Credentials { get; set; }
  }
}