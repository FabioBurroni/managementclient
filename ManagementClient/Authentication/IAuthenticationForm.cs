﻿using System;
using System.Windows.Forms;

namespace Authentication
{
	public interface IAuthenticationForm : IDisposable
	{
		/// <summary>
		/// Chiude il form
		/// </summary>
		void Close();

		/// <summary>
		/// Permette di settare i dati letti tramite BCR
		/// </summary>
		void SetBcrData(string data);

		/// <summary>
		/// Mostra e fa partire il processo di autenticazione
		/// </summary>
		/// <param name="owner">Finesta che lo invoca</param>
		/// <param name="tryToInheritSession">Tenta di ereditare la sessione</param>
		/// <param name="quickLoginAsCassioli">Indica se loggarsi velocemente come cassioli</param>
		DialogResult ShowDialog(IWin32Window owner, bool tryToInheritSession, bool quickLoginAsCassioli);
	}
}