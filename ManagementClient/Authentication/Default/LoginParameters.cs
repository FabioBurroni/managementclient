﻿using System;
using System.Collections.Generic;
using System.Linq;
using XmlCommunicationManager.Authorization;

namespace Authentication.Default
{
  /// <summary>
  /// Classe che definisce i parametri di default.
  /// </summary>
  public class LoginParameters : ILoginParameters
  {
    //Esempio di parametri per login
    //0 POSIZIONI: string username, string password, string clientCode, SourceType sourceType, int portNumber, string childToken, string ipAddress
    //1 POSIZIONE: string username, string password, string clientCode, SourceType sourceType, int portNumber, string childToken, string posCode, string ipAddress
    //2 POSIZIONI: string username, string password, string clientCode, SourceType sourceType, int portNumber, string childToken, string posCode1, string posCode2, string ipAddress

    #region ILoginParameters Members

    //Implemento esplicitamente l'interfaccia così negli altri progetti non è settabile
    ICredentials ILoginParameters.Credentials
    {
      get { return Credentials; }
      set { Credentials = value; }
    }

    #endregion

    internal ICredentials Credentials { get; set; }
    public string ClientCode { get; }
    public SourceType SourceType { get; }
    public int PortNumber { get; }
    public int? ChildToken { get; }
    public IList<string> Positions { get; }

    public LoginParameters(string clientCode, SourceType sourceType, int portNumber, int? childToken, ICollection<string> positions)
    {
      if (string.IsNullOrEmpty(clientCode))
        throw new ArgumentNullException(clientCode);

      if (portNumber < 0 || portNumber > 65535)
        throw new ArgumentOutOfRangeException($"PortNumber {portNumber} out of range");

      if (positions != null)
      {
        var duplicates = positions.GroupBy(x => x, StringComparer.OrdinalIgnoreCase)
                                  .Where(group => group.Count() > 1)
                                  .Select(group => group.Key)
                                  .ToArray();

        if (duplicates.Any())
          throw new Exception($"Found duplicate positions '{string.Join(",", duplicates)}'");
      }

      ClientCode = clientCode;
      SourceType = sourceType;
      PortNumber = portNumber;
      ChildToken = childToken;
      Positions = positions == null ? new List<string>() : new List<string>(positions);
    }

    #region IParameters Members

    public IList<string> GetParameters()
    {
      var parameters = new List<string>();

      parameters.AddRange(Credentials.GetCredentials()); //qui le credenziali DEVONO essere per lo meno vuote, quindi se schianta qui non è un errore, i dati devono essere messi a posto da chi usa questa istanza
      parameters.Add(ClientCode);
      parameters.Add(SourceType.ToString());
      parameters.Add(PortNumber.ToString());
      parameters.Add(ChildToken?.ToString() ?? string.Empty);
      parameters.AddRange(Positions);

      return parameters;
    }

    #endregion
  }
}