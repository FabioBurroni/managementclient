﻿using System.Collections.Generic;
using XmlCommunicationManager.Authorization;

namespace Authentication.Default
{
  public class Credentials : ICredentials
  {
    public string Username { get; }
    public string PasswordBase64 { get; }

    public Credentials(string username = "", string passwordBase64 = "")
    {
      Username = username;
      PasswordBase64 = passwordBase64;
    }

    #region ICredentials Members

    public IList<string> GetCredentials()
    {
      return new[]
      {
        Username ?? string.Empty,
        PasswordBase64 ?? string.Empty
      };
    }

    #endregion
  }
}