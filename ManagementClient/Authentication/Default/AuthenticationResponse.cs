﻿using System;
using System.Collections.Generic;
using System.Linq;
using Authentication.Accessibility;
using Authentication.Extensions;
using Authentication.Profiling;
using XmlCommunicationManager.Authorization;
using XmlCommunicationManager.Common.Xml;

namespace Authentication.Default
{
  public class AuthenticationResponse : IAuthenticationResponse
  {
    /// <summary>
    /// Intero che definisce il risultato
    /// </summary>
    public int Result { get; private set; }

    /// <summary>
    /// Indica se è in errore o no
    /// </summary>
    public bool IsInError => Result != 1;

    /// <summary>
    /// Restituisce OK o NOK
    /// </summary>
    public string OkNok { get; private set; }

    /// <summary>
    /// Se il risultato è NOK, possono essere presenti una lista di errori informativi
    /// </summary>
    public IList<string> ErrorList { get; } = new List<string>();

    /// <summary>
    /// Parametri della sessione
    /// </summary>
    public ISession Session { get; private set; }

    /// <summary>
    /// Composizione della struttura utente
    /// </summary>
    public IList<UserStructureBase> UserStructureConfiguration { get; } = new List<UserStructureBase>();

    public void PartialFillByResultAndErrors(int result, IList<string> errorList)
    {
      Clear();

      Result = result;

      if (errorList != null)
      {
        foreach (var error in errorList)
        {
          ErrorList.Add(error);
        }
      }
    }

    public void PartialFillBySession(IList<string> sessionData)
    {
      Clear();

      int i = 0;

      var username = sessionData[i++];
      var userProfileCode = sessionData[i++];
      var userProfileHierarchy = sessionData[i++].ConvertTo<int>();
      var userClientCode = sessionData[i++];
      var sourceType = sessionData[i++].ConvertTo<SourceType>();
      var ipAddress = sessionData[i++];
      var portNumber = sessionData[i++].ConvertTo<int>();
      var lastActivity = sessionData[i++].ConvertTo<DateTime>();
      var rootToken = sessionData[i++].ConvertTo<int>();
      var childToken = sessionData[i++].ConvertTo<int>();
      var isActive = sessionData[i++].ConvertTo<bool>();

      var positions = new List<string>();

      //scorro le posizioni
      foreach (var posIndex in Enumerable.Range(i, sessionData.Count - i))
      {
        positions.Add(sessionData[posIndex]);
      }

      var user = new User(username, userProfileCode, userProfileHierarchy);
      var source = new Source(ipAddress, portNumber, userClientCode, sourceType);
      var tokens = new SessionTokens(rootToken, childToken);

      Session = new Session(user, source, tokens, isActive, lastActivity);
      Session.AddRangePosition(positions);
    }

    public void FullFill(IList<XmlCommandResponse.Item> parameters)
    {
      if (parameters == null)
        throw new ArgumentNullException(nameof(parameters));

      const int minimumNumberOfFields = 3;

      Clear();

      #region Field Constanti

      foreach (var paramIndex in Enumerable.Range(0, minimumNumberOfFields))
      {
        var items = parameters[paramIndex];

        if (items.Count == 0)
          continue;

        #region Item 1: OK/NOK e Risultato

        if (paramIndex + 1 == 1)
        {
          OkNok = items.getFieldVal(1);
          Result = items.getFieldVal(2).ConvertTo<int>();

          continue;
        }

        #endregion

        #region Item 2: Lista degli errori

        if (paramIndex + 1 == 2)
        {
          foreach (var field in items.getAllFieldPos())
          {
            ErrorList.Add(items.getFieldVal(field));
          }

          continue;
        }

        #endregion

        #region Item 3: Dati di sessione

        if (paramIndex + 1 == 3)
        {
          var length = items.Count;

          if (length > 0)
          {
            int i = 1;

            var username = items.getFieldVal(i++);
            var userProfileCode = items.getFieldVal(i++);
            var userProfileHierarchy = items.getFieldVal(i++).ConvertTo<int>();
            var userClientCode = items.getFieldVal(i++);
            var sourceType = items.getFieldVal(i++).ConvertTo<SourceType>();
            var ipAddress = items.getFieldVal(i++);
            var portNumber = items.getFieldVal(i++).ConvertTo<int>();
            var lastActivity = items.getFieldVal(i++).ConvertTo<DateTime>();
            var rootToken = items.getFieldVal(i++).ConvertTo<int>();
            var childToken = items.getFieldVal(i++).ConvertTo<int>();
            var isActive = items.getFieldVal(i).ConvertTo<bool>();

            var positions = new List<string>();

            //scorro le posizioni
            foreach (var posIndex in Enumerable.Range(i, length - i))
            {
              positions.Add(items.getFieldVal(posIndex + 1));
            }

            var user = new User(username, userProfileCode, userProfileHierarchy);
            var source = new Source(ipAddress, portNumber, userClientCode, sourceType);
            var tokens = new SessionTokens(rootToken, childToken);

            Session = new Session(user, source, tokens, isActive);
            Session.AddRangePosition(positions);
          }

          continue;
        }

        #endregion
      }

      #endregion

      #region Field Variabili

      foreach (var paramIndex in Enumerable.Range(minimumNumberOfFields, parameters.Count - minimumNumberOfFields))
      {
        #region Item 4=>N Dati della struttura

        var items = parameters[paramIndex];

        if (items.Count == 0)
          continue;

        int i = 1;

        var userActionCode = items.getFieldVal(i++);
        var userClientCode = items.getFieldVal(i++);
        var userProfileCode = items.getFieldVal(i++);
        var enabled = items.getFieldVal(i++).ConvertTo<bool>();
        var optionalData = items.getFieldVal(i);

        var userStructure = new UserStructureBase(userActionCode, userClientCode, userProfileCode, enabled, optionalData);

        UserStructureConfiguration.Add(userStructure);

        #endregion
      }

      //Aggiorno la struttura solo se è valida e c'è qualcosa
      if (Session != null && UserStructureConfiguration.Any())
        Session.User.UpdateUserStructureCollection(UserStructureConfiguration);

      #endregion
    }

    private void Clear()
    {
      ErrorList.Clear();
      Session = null;
      UserStructureConfiguration.Clear();
    }
  }
}