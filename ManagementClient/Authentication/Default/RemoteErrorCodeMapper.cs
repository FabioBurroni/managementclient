﻿namespace Authentication.Default
{
  /// <summary>
  /// Istanza per gli errori di autenticazione che arrivano SOLO dal server
  /// </summary>
  public class RemoteErrorCodeMapper : ErrorCodeMapper
  {
    public RemoteErrorCodeMapper()
    {
      //Generic
      ErrorCodeDictionary[1] = "Ok";
      ErrorCodeDictionary[2] = "Parameters Not Valid";

      //Errori Autenticazione
      ErrorCodeDictionary[20] = "Login Parameters Not Valid";  //parametri in ingresso per login non validi
      ErrorCodeDictionary[21] = "Logout Parameters Not Valid"; //parametri in ingresso per logout non validi
      ErrorCodeDictionary[25] = "Credentials Not Valid";       //credenziali non valide
      ErrorCodeDictionary[26] = "User Client Not Found";       //client non trovato
      ErrorCodeDictionary[27] = "Reached Max Allowed Number Of User Client"; //raggiunto il massimo numero dei client connessi nel sistema
      ErrorCodeDictionary[28] = "User Profile Not Found";      //profilo non trovato
      ErrorCodeDictionary[30] = "Session Not Found";           //sessione non trovata
      ErrorCodeDictionary[31] = "Multiple Source Type Not Allowed On Same IpAddress";      //non sono ammesse più sorgenti Client nello stesso indirizzo IP
      ErrorCodeDictionary[32] = "Multiple Client Username Not Allowed On Same IpAddress";  //non sono ammessi Username diversi, con sorgenti Client, nello stesso indirizzo IP
      ErrorCodeDictionary[33] = "Same Client Username Not Allowed On Different IpAddress"; //lo stesso username non è ammesso su indirizzi IP diversi (A.K.A. Ubiquità configurabile) 
      ErrorCodeDictionary[34] = "To Inherit Session Must Declare Login Parameters With Same Positions"; //per poter permettere l'eredità, nei parametro d'ingresso, devono essere dichiarate lo stesso numero di posizioni
      ErrorCodeDictionary[35] = "Positions Busy By Different Client User"; //posizione/i già presa/e da altro/i utente/i
      ErrorCodeDictionary[36] = "Cannot Renew Expired Session"; //non è possibile rinnovare una sessione già scaduta
      ErrorCodeDictionary[37] = "Login Without Position Not Allowed"; //non è possibile effettuare il login senza posizione/i
      ErrorCodeDictionary[38] = "Positions Not Authorized"; //non è possibile effettuare in posizioni non autorizzate
      ErrorCodeDictionary[50] = "Partial Positions Logout Not Allowed";    //logout parziale di posizioni non permesso (tipo non lo permette)
      ErrorCodeDictionary[51] = "Requested Logout For Not Contained Positions"; //richiesto logout per posizioni non contenute


      //Errori Profiler
      ErrorCodeDictionary[500] = "Action Code Not Valid"; //codice azione non valido
      ErrorCodeDictionary[501] = "Client Code Not Valid"; //codice client non valido
      ErrorCodeDictionary[502] = "Profile Code Not Valid"; //codice profilo non valido
      ErrorCodeDictionary[503] = "Action Code Not Found"; //codice azione non trovato
      ErrorCodeDictionary[504] = "Client Code Not Found"; //codice client non trovato
      ErrorCodeDictionary[505] = "Profile Code Not Found"; //codice profilo non trovato
      ErrorCodeDictionary[506] = "Couple UserAction And UserClient Not Found"; //combinazione azione e client non trovata
      ErrorCodeDictionary[507] = "UserStructure Already Exists With Given Tern Action-Client-Profile"; //struttura utente già esistente con la terna di parametri passati
      ErrorCodeDictionary[508] = "Database Error"; //errore nel database durante l'inserimento del record


      //Errori Utente Creazione/Aggiornamento
      ErrorCodeDictionary[550] = "Username Null Or Empty"; //username nullo o vuoto
      ErrorCodeDictionary[551] = "Unacceptable Creation, User Already Created"; //creazione inaccettabile, utente già creato
      ErrorCodeDictionary[552] = "User Profile Not Found";     //profilo non trovato
      ErrorCodeDictionary[553] = "Password Null Or Empty";     //password nulla o vuota
      ErrorCodeDictionary[554] = "Fullname Null Or Empty";     //nome nullo o vuoto
      ErrorCodeDictionary[555] = "Company Null Or Empty";      //società nulla o vuota
      ErrorCodeDictionary[556] = "Profile Code Null Or Empty"; //codice profilo nullo o vuoto
      ErrorCodeDictionary[557] = "Blocked Null Or Empty";  //bloccato nullo o vuoto
      ErrorCodeDictionary[558] = "Password Not Base64";    //password non in base 64
      ErrorCodeDictionary[559] = "Error Inserting User";   //errore inserimento utente
      ErrorCodeDictionary[560] = "Unacceptable Update, User Not Created"; //creazione inaccettabile, utente non creato
      ErrorCodeDictionary[561] = "User Profile Not Found"; //profilo non trovato
      ErrorCodeDictionary[562] = "Password Not Base64";    //password non in base 64
      ErrorCodeDictionary[563] = "Error Updating User";    //errore aggiornamento utente


      //Errori Utente Cancellazione
      ErrorCodeDictionary[568] = "Cannot Delete User With Active Session"; //impossibile cancellare un utente con una sessione attiva
      ErrorCodeDictionary[569] = "Database Exception";     //eccezione nel database
      ErrorCodeDictionary[570] = "Username Null Or Empty"; //username nullo o vuoto
      ErrorCodeDictionary[571] = "Username Not Found";     //username non trovato
      ErrorCodeDictionary[572] = "User Belonging To Cassioli Company Cannot Be Deleted"; //utenti della società Cassioli non possono essere cancellati
      ErrorCodeDictionary[573] = "User Linked To List Cannot Be Deleted"; //utenti collegati a liste non possono essere cancellati
      ErrorCodeDictionary[574] = "Error Deleting User"; //errore cancellazione utente
    }
  }
}