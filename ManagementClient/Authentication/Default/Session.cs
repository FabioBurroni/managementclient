﻿using System;
using System.Collections.Generic;
using System.Linq;
using Authentication.Accessibility;
using XmlCommunicationManager.Authorization;

namespace Authentication.Default
{
  public class Session : ISession
  {
    #region Fields

    /// <summary>
    /// Memorizza tutte le posizioni collegate alla sessione
    /// </summary>
    private readonly List<string> _positionCollection;

    #endregion

    #region Costruttore

    /// <summary>
    /// Costruttore per la sessione
    /// </summary>
    /// <param name="user">Istanza dell'utente</param>
    /// <param name="source">Sorgente della sessione</param>
    /// <param name="sessionTokens">Token di sessione, è iniettato da chi genera la sessione stessa per assicurarsi la sua univocità</param>
    /// <param name="isActive">Indica se la sessione è attiva</param>
    /// <param name="lastActivity">Indica l'ultima attività della sessione</param>
    public Session(User user, Source source, SessionTokens sessionTokens, bool isActive = true, DateTime lastActivity = default(DateTime))
    {
      if (user == null)
        throw new ArgumentNullException(nameof(user));

      if (source == null)
        throw new ArgumentNullException(nameof(source));

      if (sessionTokens == null)
        throw new ArgumentNullException(nameof(sessionTokens));

      _positionCollection = new List<string>();

      User = user;
      Source = source;
      SessionTokens = sessionTokens;
      IsActive = isActive;
      LastActivity = lastActivity;
    }

    #endregion

    #region ISessionMembers

    public User User { get; }
    public Source Source { get; }
    public SessionTokens SessionTokens { get; }
    public DateTime CreationTime { get; } = DateTime.Now;
    public bool IsActive { get; set; }
    public DateTime LastActivity { get; }

    #region PositionCollection

    public IList<string> PositionCollection
    {
      get
      {
        lock (_positionCollection)
        {
          return _positionCollection.ToList();
        }
      }
    }

    public int GetPositionsCount()
    {
      lock (_positionCollection)
      {
        return _positionCollection.Count;
      }
    }

    public bool AddPosition(string position)
    {
      return AddRangePosition(position).Count == 1;
    }

    public IList<string> AddRangePosition(IEnumerable<string> collection)
    {
      if (collection == null)
        return new List<string>();

      return AddRangePosition(collection.ToArray());
    }

    public IList<string> AddRangePosition(params string[] collection)
    {
      var ret = new List<string>();

      if (collection == null)
        return ret;

      lock (_positionCollection)
      {
        foreach (var posToAdd in collection)
        {
          if (string.IsNullOrEmpty(posToAdd))
            continue;

          if (_positionCollection.Contains(posToAdd, StringComparer.OrdinalIgnoreCase))
            continue;

          _positionCollection.Add(posToAdd);
          ret.Add(posToAdd);
        }
      }

      return ret;
    }

    public string GetFirstPosition()
    {
      lock (_positionCollection)
      {
        return _positionCollection.FirstOrDefault();
      }
    }

    public IList<string> GetFirstPosition(int count)
    {
      lock (_positionCollection)
      {
        return _positionCollection.Take(count).ToList();
      }
    }

    public string GetLastPosition()
    {
      lock (_positionCollection)
      {
        return _positionCollection.LastOrDefault();
      }
    }

    public IList<string> GetLastPosition(int count)
    {
      lock (_positionCollection)
      {
        return _positionCollection.ToArray().Reverse().Take(count).ToList();
      }
    }

    public IList<string> GetNotContainedPositions(IEnumerable<string> collection)
    {
      if (collection == null)
        return new List<string>();

      lock (_positionCollection)
      {
        return collection.Except(_positionCollection, StringComparer.OrdinalIgnoreCase).ToList();
      }
    }

    public string PositionAt(int index)
    {
      lock (_positionCollection)
      {
        return _positionCollection[index];
      }
    }

    public bool ContainsPosition(string position)
    {
      //if (string.IsNullOrEmpty(position))
      //  return false;

      //lock (_positionCollection)
      //{
      //  return _positionCollection.Any(p => p.EqualsIgnoreCase(position));
      //}

      return ContainsRangePosition(position).Count == 1;
    }

    public IList<string> ContainsRangePosition(IEnumerable<string> collection)
    {
      if (collection == null)
        return new List<string>();

      return ContainsRangePosition(collection.ToArray());
    }

    public IList<string> ContainsRangePosition(params string[] collection)
    {
      var ret = new List<string>();

      if (collection == null)
        return ret;

      lock (_positionCollection)
      {
        foreach (var posToCheck in collection)
        {
          //se anche una sola posizione è nulla o vuota, restituisco false
          if (string.IsNullOrEmpty(posToCheck))
            continue;

          //se la posizione è contenuta, la memorizzo
          if (!_positionCollection.Contains(posToCheck, StringComparer.OrdinalIgnoreCase))
            continue;

          ret.Add(posToCheck);
        }
      }

      return ret;
    }

    public bool ContainsAllPositions(IEnumerable<string> collection)
    {
      if (collection == null)
        return false;

      lock (_positionCollection)
      {
        return _positionCollection.OrderBy(p => p).SequenceEqual(collection.OrderBy(p => p), StringComparer.OrdinalIgnoreCase);
      }
    }

    public bool RemovePosition(string position)
    {
      if (string.IsNullOrEmpty(position))
        return false;

      return RemoveRangePosition().Count == 1;
    }

    public IList<string> RemoveRangePosition(IEnumerable<string> collection)
    {
      if (collection == null)
        return new List<string>();

      return RemoveRangePosition(collection.ToArray());
    }

    public IList<string> RemoveRangePosition(params string[] collection)
    {
      var ret = new List<string>();

      if (collection == null)
        return ret;

      lock (_positionCollection)
      {
        foreach (var posToRemove in collection)
        {
          if (string.IsNullOrEmpty(posToRemove))
            continue;

          if (!_positionCollection.Contains(posToRemove, StringComparer.OrdinalIgnoreCase))
            continue;

          _positionCollection.Remove(posToRemove);
          ret.Add(posToRemove);
        }
      }

      return ret;
    }

    #endregion

    #endregion
  }
}