﻿namespace Authentication
{
	/// <summary>
	/// Istanza per gli errori di autenticazione generati SOLO lato client.
	/// Non cambiare mai gli errori che ci sono, è però possibile aggiungere i codici che si vogliono.
	/// Per convenzione, tutti i codici al suo interno devono essere negativi per distinguerli da quelli lato server
	/// </summary>
	internal class LocalErrorCodeMapper : ErrorCodeMapper
	{
		public const int ServerNotReachable = -1;    //errore di connessione con il server
		public const int PasswordCannotBeBlank = -2; //password vuota
		public const int UsernameCannotBeBlank = -3; //username vuoto
		public const int SelectAtLeastOneSession = -4; //seleziona almeno una sessione
		public const int UpdateAtLeastOneConfiguration = -5; //aggiorna almeno una configurazione
		public const int UserCreationNotAllowedAtThisProfile = -6; //creazione utente non permessa a questo profilo
		public const int PositionCannotBeBlank = -7; //posizione vuota
		public const int BadgeCannotBeBlank = -8; //badge vuoto
		public const int OldPasswordCannotBeBlank = -9; //vecchia password vuota
		public const int NewPasswordCannotBeBlank = -10; //nuova password vuota
		public const int ConfirmPasswordCannotBeBlank = -11; //conferma password vuota
		public const int NewPasswordDoesNotMatchTheConfirmPassword = -12; //nuova password e conferma password non sono uguali

		public LocalErrorCodeMapper()
		{
			//Errori che provengono dall'interfaccia quindi locali al client
			ErrorCodeDictionary[ServerNotReachable] = "Server Not Reachable";
			ErrorCodeDictionary[PasswordCannotBeBlank] = "Password Cannot Be Blank";
			ErrorCodeDictionary[UsernameCannotBeBlank] = "Username Cannot Be Blank";
			ErrorCodeDictionary[SelectAtLeastOneSession] = "Select At Least One Session";
			ErrorCodeDictionary[UpdateAtLeastOneConfiguration] = "Update At Least One Configuration";
			ErrorCodeDictionary[UserCreationNotAllowedAtThisProfile] = "User Creation Not Allowed At This Profile";
			ErrorCodeDictionary[PositionCannotBeBlank] = "Position Cannot Be Blank";
			ErrorCodeDictionary[BadgeCannotBeBlank] = "Badge Cannot Be Blank";
			ErrorCodeDictionary[OldPasswordCannotBeBlank] = " Old Password Cannot Be Blank";
			ErrorCodeDictionary[NewPasswordCannotBeBlank] = "New Password Cannot Be Blank";
			ErrorCodeDictionary[ConfirmPasswordCannotBeBlank] = "Confirm Password Cannot Be Blank";
			ErrorCodeDictionary[NewPasswordDoesNotMatchTheConfirmPassword] = "New Password Does Not Match The Confirm Password";
		}
	}
}