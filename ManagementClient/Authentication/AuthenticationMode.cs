﻿namespace Authentication
{
  internal enum AuthenticationMode
  {
    None,
    XmlClient,
    CommandManager
  }
}