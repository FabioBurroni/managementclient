﻿using System;
using Authentication.Extensions;

namespace Authentication.Profiling
{
  public class UserProfile : IComparable<UserProfile>, IEquatable<UserProfile>
  {
    #region Properties

    public int Id { get; }
    public string Code { get; }
    public string Descr { get; }
    public int Hierarchy { get; }

    #endregion

    #region Constructor

    public UserProfile(int id, string code, string descr, int hierarchy)
    {
      if (id < 0)
        throw new ArgumentOutOfRangeException(nameof(id));

      if (string.IsNullOrEmpty(code))
        throw new ArgumentNullException(code);

      Id = id;
      Code = code;
      Descr = descr;
      Hierarchy = hierarchy;
    }

    #endregion

    #region Override

    public override bool Equals(object obj)
    {
      // Is null?
      if (ReferenceEquals(null, obj))
      {
        return false;
      }

      // Is the same object?
      if (ReferenceEquals(this, obj))
      {
        return true;
      }

      // Is the same type?
      if (obj.GetType() != GetType())
      {
        return false;
      }

      return IsEqual((UserProfile)obj);
    }

    public override int GetHashCode()
    {
      unchecked
      {
        // Choose large primes to avoid hashing collisions
        const int hashingBase = (int)2166136261;
        const int hashingMultiplier = 16777619;

        int hash = hashingBase;
        hash = (hash * hashingMultiplier) ^ Code.GetHashCode();
        return hash;
      }
    }

    public override string ToString()
    {
      return Code;
    }

    #endregion

    #region Private Methods

    private bool IsEqual(UserProfile profile)
    {
      // A pure implementation of value equality that avoids the routine checks above
      // We use Equals to really drive home our fear of an improperly overridden "=="
      return Code.EqualsIgnoreCase(profile.Code);
    }

    #endregion

    #region IComparable Members

    public int CompareTo(UserProfile other)
    {
      // If other is not a valid object reference, this instance is greater.
      if (other == null)
        return 1;

      return Code.CompareIgnoreCase(other.Code);
    }

    #endregion

    #region IEquatable Members

    public bool Equals(UserProfile profile)
    {
      // Is null?
      if (ReferenceEquals(null, profile))
      {
        return false;
      }

      // Is the same object?
      if (ReferenceEquals(this, profile))
      {
        return true;
      }

      return IsEqual(profile);
    }

    #endregion
  }
}