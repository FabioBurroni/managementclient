﻿using System;

namespace Authentication.Profiling
{
  /// <summary>
  /// Classe la definizione di UserStructure
  /// </summary>
  public class UserStructure : UserStructureBase
  {
    #region Properties

    public UserAction UserAction { get; }
    public UserClient UserClient { get; }
    public UserProfile UserProfile { get; }

    #endregion

    #region Constructor

    public UserStructure(UserAction action, UserClient client, UserProfile profile, bool enabled = false, string optionalData = "")
      : base(action.Code, client.Code, profile.Code, enabled, optionalData)
    {
      if (action == null)
        throw new ArgumentNullException(nameof(action));

      if (client == null)
        throw new ArgumentNullException(nameof(client));

      if (profile == null)
        throw new ArgumentNullException(nameof(profile));

      if (optionalData == null)
        throw new ArgumentNullException(nameof(optionalData));

      UserAction = action;
      UserClient = client;
      UserProfile = profile;
    }

    #endregion
  }
}