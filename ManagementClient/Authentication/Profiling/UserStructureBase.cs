﻿using System;

namespace Authentication.Profiling
{
  public class UserStructureBase : IComparable<UserStructureBase>, IEquatable<UserStructureBase>
  {
    #region Properties

    public string UserActionCode { get; }
    public string UserClientCode { get; }
    public string UserProfileCode { get; }

    /// <summary>
    /// Indica se la struttura è abilitata o no
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// Parametri opzionali, non sono e non devono essere settati nulli
    /// </summary>
    public string OptionalData { get; set; }

    #endregion

    #region Constructor

    public UserStructureBase(string userActionCode, string userClientCode, string userProfileCode, bool enabled, string optionalData)
    {
      if (string.IsNullOrEmpty(userActionCode))
        throw new ArgumentNullException(nameof(userActionCode));

      if (string.IsNullOrEmpty(userProfileCode))
        throw new ArgumentNullException(nameof(userProfileCode));

      if (string.IsNullOrEmpty(userClientCode))
        throw new ArgumentNullException(nameof(userClientCode));

      if (optionalData == null)
        throw new ArgumentNullException(nameof(optionalData));

      UserActionCode = userActionCode;
      UserClientCode = userClientCode;
      UserProfileCode = userProfileCode;

      Enabled = enabled;
      OptionalData = optionalData;
    }

    #endregion

    #region Public Methods

    public override string ToString()
    {
      return $"{UserActionCode}.{UserClientCode}.{UserProfileCode}";
    }

    #endregion

    #region IComparable<UserStructureBase> Members

    public int CompareTo(UserStructureBase other)
    {
      var result = 1;

      // If other is not a valid object reference, this instance is greater.
      if (other == null)
        return result;

      result = string.Compare(UserActionCode, other.UserActionCode, StringComparison.OrdinalIgnoreCase);
      if (result != 0)
        return result;

      result = string.Compare(UserClientCode, other.UserClientCode, StringComparison.OrdinalIgnoreCase);
      if (result != 0)
        return result;

      return string.Compare(UserProfileCode, other.UserProfileCode, StringComparison.OrdinalIgnoreCase);
    }

    #endregion

    #region IEquatable<UserStructureBase> Members

    public bool Equals(UserStructureBase other)
    {
      if (other == null)
        return false;

      return UserActionCode.Equals(other.UserActionCode, StringComparison.OrdinalIgnoreCase) &&
             UserClientCode.Equals(other.UserClientCode, StringComparison.OrdinalIgnoreCase) &&
             UserProfileCode.Equals(other.UserProfileCode, StringComparison.OrdinalIgnoreCase);
    }

    #endregion
  }
}