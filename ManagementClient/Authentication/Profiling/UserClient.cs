﻿using System;
using Authentication.Extensions;

namespace Authentication.Profiling
{
  public class UserClient : IComparable<UserClient>, IEquatable<UserClient>
  {
    #region Properties

    /// <summary>
    /// Id del client
    /// </summary>
    public int Id { get; }

    /// <summary>
    /// Codice del client
    /// </summary>
    public string Code { get; }

    /// <summary>
    /// Descrizione dell'azione
    /// </summary>
    public string Descr { get; }

    #endregion

    #region Constructor

    public UserClient(int id, string code, string descr)
    {
      if (id < 0)
        throw new ArgumentOutOfRangeException(nameof(id));

      if (string.IsNullOrEmpty(code))
        throw new ArgumentNullException(code);

      Id = id;
      Code = code;
      Descr = descr;
    }

    #endregion

    #region Override

    public override bool Equals(object obj)
    {
      // Is null?
      if (ReferenceEquals(null, obj))
      {
        return false;
      }

      // Is the same object?
      if (ReferenceEquals(this, obj))
      {
        return true;
      }

      // Is the same type?
      if (obj.GetType() != GetType())
      {
        return false;
      }

      return IsEqual((UserClient)obj);
    }

    public override int GetHashCode()
    {
      unchecked
      {
        // Choose large primes to avoid hashing collisions
        const int hashingBase = (int)2166136261;
        const int hashingMultiplier = 16777619;

        int hash = hashingBase;
        hash = (hash * hashingMultiplier) ^ Code.GetHashCode();
        return hash;
      }
    }

    public override string ToString()
    {
      return Code;
    }

    #endregion

    #region Private Methods

    private bool IsEqual(UserClient action)
    {
      // A pure implementation of value equality that avoids the routine checks above
      // We use Equals to really drive home our fear of an improperly overridden "=="
      return Code.EqualsIgnoreCase(action.Code);
    }

    #endregion

    #region IComparable Members

    public int CompareTo(UserClient other)
    {
      // If other is not a valid object reference, this instance is greater.
      if (other == null)
        return 1;

      return Code.CompareIgnoreCase(other.Code);
    }

    #endregion

    #region IEquatable Members

    public bool Equals(UserClient client)
    {
      // Is null?
      if (ReferenceEquals(null, client))
      {
        return false;
      }

      // Is the same object?
      if (ReferenceEquals(this, client))
      {
        return true;
      }

      return IsEqual(client);
    }

    #endregion
  }
}