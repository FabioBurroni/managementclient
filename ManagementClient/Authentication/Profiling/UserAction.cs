﻿using System;
using Authentication.Extensions;

namespace Authentication.Profiling
{
  public class UserAction : IComparable<UserAction>, IEquatable<UserAction>
  {
    #region Properties

    /// <summary>
    /// Id dell'azione
    /// </summary>
    public int Id { get; }

    /// <summary>
    /// Codice dell'azione. Se è di commessa ha il prefisso cXXXXXX
    /// </summary>
    public string Code { get; }

    /// <summary>
    /// Descrizione dell'azione
    /// </summary>
    public string Descr { get; }

    /// <summary>
    /// Definisce se è standard o di commessa
    /// </summary>
    public bool Standard { get; }

    #endregion

    #region Constructor

    public UserAction(int id, string code, string descr, bool standard)
    {
      if (id < 0)
        throw new ArgumentOutOfRangeException(nameof(id));

      if (string.IsNullOrEmpty(code))
        throw new ArgumentNullException(code);

      Id = id;
      Code = code;
      Descr = descr;
      Standard = standard;
    }

    #endregion

    #region Override

    public override bool Equals(object obj)
    {
      // Is null?
      if (ReferenceEquals(null, obj))
      {
        return false;
      }

      // Is the same object?
      if (ReferenceEquals(this, obj))
      {
        return true;
      }

      // Is the same type?
      if (obj.GetType() != GetType())
      {
        return false;
      }

      return IsEqual((UserAction)obj);
    }

    public override int GetHashCode()
    {
      unchecked
      {
        // Choose large primes to avoid hashing collisions
        const int hashingBase = (int)2166136261;
        const int hashingMultiplier = 16777619;

        int hash = hashingBase;
        hash = (hash * hashingMultiplier) ^ Code.GetHashCode();
        return hash;
      }
    }

    public override string ToString()
    {
      return Code;
    }

    #endregion

    #region Private Methods

    private bool IsEqual(UserAction action)
    {
      // A pure implementation of value equality that avoids the routine checks above
      // We use Equals to really drive home our fear of an improperly overridden "=="
      return Code.EqualsIgnoreCase(action.Code);
    }

    #endregion

    #region IComparable Members

    public int CompareTo(UserAction other)
    {
      // If other is not a valid object reference, this instance is greater.
      if (other == null)
        return 1;

      return Code.CompareIgnoreCase(other.Code);
    }

    #endregion

    #region IEquatable Members

    public bool Equals(UserAction action)
    {
      // Is null?
      if (ReferenceEquals(null, action))
      {
        return false;
      }

      // Is the same object?
      if (ReferenceEquals(this, action))
      {
        return true;
      }

      return IsEqual(action);
    }

    #endregion
  }
}