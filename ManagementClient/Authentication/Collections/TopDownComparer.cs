﻿using System.Collections.Generic;

namespace Authentication.Collections
{
	/// <summary>
	/// Compara gli interi ordinando in modo ascendente quelli positivi e in modo discendente quelle negativi
	/// </summary>
	internal class TopDownComparer : IComparer<int>
	{
		public int Compare(int x, int y)
		{
			if (x < 0 && y < 0)
				return y.CompareTo(x);
			return x.CompareTo(y);
		}
	}
}