﻿namespace Authentication.Custom.C160086
{
  /// <summary>
  /// Istanza per gli errori di autenticazione che arrivano SOLO dal server
  /// </summary>
  public class RemoteErrorCodeMapper : Default.RemoteErrorCodeMapper
  {
    public RemoteErrorCodeMapper()
    {
      ErrorCodeDictionary[100] = "Login In Multiple PickList Authorized Position On Same IpAddress Not Allowed";  //errore login su posizioni di PickList
      ErrorCodeDictionary[101] = "Same Client Username Not Allowed In Multiple PickList Authorized Position";  //errore login su posizioni di PickList

      ErrorCodeDictionary[564] = "Badge Already Paired With Other User";  //errore aggiornamento badge
      ErrorCodeDictionary[565] = "Error Updating User";   //errore aggiornamento utente
      ErrorCodeDictionary[566] = "Badge Already Paired With Other User";  //errore aggiornamento badge
      ErrorCodeDictionary[567] = "Error Updating User";   //errore aggiornamento utente
    }
  }
}