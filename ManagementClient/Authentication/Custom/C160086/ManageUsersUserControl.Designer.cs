﻿using Authentication.Controls;

namespace Authentication.Custom.C160086
{
  partial class ManageUsersUserControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
      this.coloredBorderPanel = new Authentication.Controls.ColoredBorderPanel();
      this.panelMain = new System.Windows.Forms.Panel();
      this.panelUsers = new System.Windows.Forms.Panel();
      this.dgvUsers = new System.Windows.Forms.DataGridView();
      this.dataGridViewColumnUser = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewColumnUsername = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewColumnBadge = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewColumnFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewColumnCompany = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewColumnProfile = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewColumnBlocked = new System.Windows.Forms.DataGridViewCheckBoxColumn();
      this.panelEditUser = new Authentication.Controls.ColoredBorderPanel();
      this.tableLayoutPanelDetailUser = new System.Windows.Forms.TableLayoutPanel();
      this.pnlLblUsername = new System.Windows.Forms.Panel();
      this.lblTrUsername = new System.Windows.Forms.Label();
      this.pnlUsername = new System.Windows.Forms.Panel();
      this.txbUsername = new System.Windows.Forms.TextBox();
      this.pnlLblPassword = new System.Windows.Forms.Panel();
      this.lblTrPassword = new System.Windows.Forms.Label();
      this.pnlPassword = new System.Windows.Forms.Panel();
      this.txbPassword = new System.Windows.Forms.TextBox();
      this.panelLblBadge = new System.Windows.Forms.Panel();
      this.lblTrBadge = new System.Windows.Forms.Label();
      this.panelBadge = new System.Windows.Forms.Panel();
      this.txbBadge = new System.Windows.Forms.TextBox();
      this.pnlLblFullName = new System.Windows.Forms.Panel();
      this.lblTrFullName = new System.Windows.Forms.Label();
      this.pnlFullName = new System.Windows.Forms.Panel();
      this.txbFullName = new System.Windows.Forms.TextBox();
      this.pnlLblCompany = new System.Windows.Forms.Panel();
      this.lblTrCompany = new System.Windows.Forms.Label();
      this.pnlCompany = new System.Windows.Forms.Panel();
      this.txbCompany = new System.Windows.Forms.TextBox();
      this.pnlLblProfile = new System.Windows.Forms.Panel();
      this.lblTrProfile = new System.Windows.Forms.Label();
      this.pnlProfile = new System.Windows.Forms.Panel();
      this.cmbProfile = new System.Windows.Forms.ComboBox();
      this.panelLblBlocked = new System.Windows.Forms.Panel();
      this.lblTrBlocked = new System.Windows.Forms.Label();
      this.panelBlocked = new System.Windows.Forms.Panel();
      this.checkBoxBlocked = new System.Windows.Forms.CheckBox();
      this.panelCancelSave = new System.Windows.Forms.Panel();
      this.panelCancel = new System.Windows.Forms.Panel();
      this.btnCancel = new Authentication.Controls.CtrlImageButton();
      this.panelSave = new System.Windows.Forms.Panel();
      this.btnSave = new Authentication.Controls.CtrlImageButton();
      this.panelButtons = new Authentication.Controls.ColoredBorderPanel();
      this.pnlMessage = new Authentication.Controls.CtrlCustomPanel();
      this.textBoxError = new System.Windows.Forms.TextBox();
      this.panelInfo = new System.Windows.Forms.Panel();
      this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
      this.btnDeleteUser = new Authentication.Controls.CtrlButton();
      this.btnEditUser = new Authentication.Controls.CtrlButton();
      this.btnCreateUser = new Authentication.Controls.CtrlButton();
      this.panelHeaderFilters = new System.Windows.Forms.Panel();
      this.panelFiltersAndRefresh = new System.Windows.Forms.Panel();
      this.panelFilters = new System.Windows.Forms.Panel();
      this.flowLayoutPanelFilters = new System.Windows.Forms.FlowLayoutPanel();
      this.panelUsernameFilter = new System.Windows.Forms.Panel();
      this.textBoxUsernameFilter = new System.Windows.Forms.TextBox();
      this.labelTrUsernameContains = new System.Windows.Forms.Label();
      this.panelBadgeFilter = new System.Windows.Forms.Panel();
      this.textBoxBadgeFilter = new System.Windows.Forms.TextBox();
      this.labelTrBadgeContains = new System.Windows.Forms.Label();
      this.panelFullNameFilter = new System.Windows.Forms.Panel();
      this.textBoxFullNameFilter = new System.Windows.Forms.TextBox();
      this.labelTrFullNameContains = new System.Windows.Forms.Label();
      this.panelCompanyFilter = new System.Windows.Forms.Panel();
      this.textBoxCompanyFilter = new System.Windows.Forms.TextBox();
      this.labelTrCompanyContains = new System.Windows.Forms.Label();
      this.panelProfileFilter = new System.Windows.Forms.Panel();
      this.textBoxProfileFilter = new System.Windows.Forms.TextBox();
      this.labelTrProfileContains = new System.Windows.Forms.Label();
      this.panelBlockedFilter = new System.Windows.Forms.Panel();
      this.comboBoxBlockedFilter = new System.Windows.Forms.ComboBox();
      this.labelTrBlockedContains = new System.Windows.Forms.Label();
      this.panelResetFilters = new System.Windows.Forms.Panel();
      this.btnResetFilters = new Authentication.Controls.CtrlButton();
      this.panelRefresh = new System.Windows.Forms.Panel();
      this.coloredBorderPanelAutoRefresh = new Authentication.Controls.ColoredBorderPanel();
      this.btnRefresh = new Authentication.Controls.CtrlButton();
      this.labelTrFilterUsers = new System.Windows.Forms.Label();
      this.panelTop = new System.Windows.Forms.Panel();
      this.lblTrUsers = new System.Windows.Forms.Label();
      this.coloredBorderPanel.SuspendLayout();
      this.panelMain.SuspendLayout();
      this.panelUsers.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvUsers)).BeginInit();
      this.panelEditUser.SuspendLayout();
      this.tableLayoutPanelDetailUser.SuspendLayout();
      this.pnlLblUsername.SuspendLayout();
      this.pnlUsername.SuspendLayout();
      this.pnlLblPassword.SuspendLayout();
      this.pnlPassword.SuspendLayout();
      this.panelLblBadge.SuspendLayout();
      this.panelBadge.SuspendLayout();
      this.pnlLblFullName.SuspendLayout();
      this.pnlFullName.SuspendLayout();
      this.pnlLblCompany.SuspendLayout();
      this.pnlCompany.SuspendLayout();
      this.pnlLblProfile.SuspendLayout();
      this.pnlProfile.SuspendLayout();
      this.panelLblBlocked.SuspendLayout();
      this.panelBlocked.SuspendLayout();
      this.panelCancelSave.SuspendLayout();
      this.panelCancel.SuspendLayout();
      this.panelSave.SuspendLayout();
      this.panelButtons.SuspendLayout();
      this.pnlMessage.SuspendLayout();
      this.flowLayoutPanel1.SuspendLayout();
      this.panelHeaderFilters.SuspendLayout();
      this.panelFiltersAndRefresh.SuspendLayout();
      this.panelFilters.SuspendLayout();
      this.flowLayoutPanelFilters.SuspendLayout();
      this.panelUsernameFilter.SuspendLayout();
      this.panelBadgeFilter.SuspendLayout();
      this.panelFullNameFilter.SuspendLayout();
      this.panelCompanyFilter.SuspendLayout();
      this.panelProfileFilter.SuspendLayout();
      this.panelBlockedFilter.SuspendLayout();
      this.panelResetFilters.SuspendLayout();
      this.panelRefresh.SuspendLayout();
      this.coloredBorderPanelAutoRefresh.SuspendLayout();
      this.panelTop.SuspendLayout();
      this.SuspendLayout();
      // 
      // coloredBorderPanel
      // 
      this.coloredBorderPanel.BorderColor = System.Drawing.Color.SkyBlue;
      this.coloredBorderPanel.BorderWidth = 2;
      this.coloredBorderPanel.Controls.Add(this.panelMain);
      this.coloredBorderPanel.Controls.Add(this.panelTop);
      this.coloredBorderPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.coloredBorderPanel.Location = new System.Drawing.Point(0, 0);
      this.coloredBorderPanel.Name = "coloredBorderPanel";
      this.coloredBorderPanel.Size = new System.Drawing.Size(1230, 600);
      this.coloredBorderPanel.TabIndex = 16;
      // 
      // panelMain
      // 
      this.panelMain.BackColor = System.Drawing.SystemColors.Control;
      this.panelMain.Controls.Add(this.panelUsers);
      this.panelMain.Controls.Add(this.panelEditUser);
      this.panelMain.Controls.Add(this.panelButtons);
      this.panelMain.Controls.Add(this.panelHeaderFilters);
      this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelMain.Location = new System.Drawing.Point(2, 44);
      this.panelMain.Name = "panelMain";
      this.panelMain.Size = new System.Drawing.Size(1226, 554);
      this.panelMain.TabIndex = 2;
      // 
      // panelUsers
      // 
      this.panelUsers.Controls.Add(this.dgvUsers);
      this.panelUsers.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelUsers.Location = new System.Drawing.Point(0, 146);
      this.panelUsers.Name = "panelUsers";
      this.panelUsers.Size = new System.Drawing.Size(781, 348);
      this.panelUsers.TabIndex = 30;
      // 
      // dgvUsers
      // 
      this.dgvUsers.AllowUserToAddRows = false;
      this.dgvUsers.AllowUserToDeleteRows = false;
      this.dgvUsers.AllowUserToOrderColumns = true;
      this.dgvUsers.AllowUserToResizeRows = false;
      this.dgvUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.dgvUsers.BackgroundColor = System.Drawing.Color.White;
      this.dgvUsers.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.dgvUsers.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
      dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvUsers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
      this.dgvUsers.ColumnHeadersHeight = 50;
      this.dgvUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgvUsers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewColumnUser,
            this.dataGridViewColumnUsername,
            this.dataGridViewColumnBadge,
            this.dataGridViewColumnFullName,
            this.dataGridViewColumnCompany,
            this.dataGridViewColumnProfile,
            this.dataGridViewColumnBlocked});
      dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
      dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
      dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgvUsers.DefaultCellStyle = dataGridViewCellStyle11;
      this.dgvUsers.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvUsers.Location = new System.Drawing.Point(0, 0);
      this.dgvUsers.MultiSelect = false;
      this.dgvUsers.Name = "dgvUsers";
      this.dgvUsers.ReadOnly = true;
      dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvUsers.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
      this.dgvUsers.RowHeadersVisible = false;
      this.dgvUsers.RowHeadersWidth = 50;
      this.dgvUsers.RowTemplate.Height = 28;
      this.dgvUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgvUsers.Size = new System.Drawing.Size(781, 348);
      this.dgvUsers.TabIndex = 13;
      this.dgvUsers.SelectionChanged += new System.EventHandler(this.dgvSessions_SelectionChanged);
      // 
      // dataGridViewColumnUser
      // 
      this.dataGridViewColumnUser.DataPropertyName = "User";
      this.dataGridViewColumnUser.HeaderText = "User";
      this.dataGridViewColumnUser.Name = "dataGridViewColumnUser";
      this.dataGridViewColumnUser.ReadOnly = true;
      this.dataGridViewColumnUser.Visible = false;
      // 
      // dataGridViewColumnUsername
      // 
      this.dataGridViewColumnUsername.DataPropertyName = "Username";
      dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      this.dataGridViewColumnUsername.DefaultCellStyle = dataGridViewCellStyle8;
      this.dataGridViewColumnUsername.HeaderText = "Username";
      this.dataGridViewColumnUsername.Name = "dataGridViewColumnUsername";
      this.dataGridViewColumnUsername.ReadOnly = true;
      // 
      // dataGridViewColumnBadge
      // 
      this.dataGridViewColumnBadge.DataPropertyName = "Badge";
      dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      this.dataGridViewColumnBadge.DefaultCellStyle = dataGridViewCellStyle9;
      this.dataGridViewColumnBadge.HeaderText = "Badge";
      this.dataGridViewColumnBadge.Name = "dataGridViewColumnBadge";
      this.dataGridViewColumnBadge.ReadOnly = true;
      // 
      // dataGridViewColumnFullName
      // 
      this.dataGridViewColumnFullName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
      this.dataGridViewColumnFullName.DataPropertyName = "FullName";
      dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      this.dataGridViewColumnFullName.DefaultCellStyle = dataGridViewCellStyle10;
      this.dataGridViewColumnFullName.HeaderText = "FullName";
      this.dataGridViewColumnFullName.Name = "dataGridViewColumnFullName";
      this.dataGridViewColumnFullName.ReadOnly = true;
      this.dataGridViewColumnFullName.Width = 103;
      // 
      // dataGridViewColumnCompany
      // 
      this.dataGridViewColumnCompany.DataPropertyName = "Company";
      this.dataGridViewColumnCompany.HeaderText = "Company";
      this.dataGridViewColumnCompany.Name = "dataGridViewColumnCompany";
      this.dataGridViewColumnCompany.ReadOnly = true;
      // 
      // dataGridViewColumnProfile
      // 
      this.dataGridViewColumnProfile.DataPropertyName = "Profile";
      this.dataGridViewColumnProfile.HeaderText = "Profile";
      this.dataGridViewColumnProfile.Name = "dataGridViewColumnProfile";
      this.dataGridViewColumnProfile.ReadOnly = true;
      // 
      // dataGridViewColumnBlocked
      // 
      this.dataGridViewColumnBlocked.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
      this.dataGridViewColumnBlocked.DataPropertyName = "Blocked";
      this.dataGridViewColumnBlocked.HeaderText = "Blocked";
      this.dataGridViewColumnBlocked.Name = "dataGridViewColumnBlocked";
      this.dataGridViewColumnBlocked.ReadOnly = true;
      this.dataGridViewColumnBlocked.Width = 75;
      // 
      // panelEditUser
      // 
      this.panelEditUser.BorderColor = System.Drawing.Color.Goldenrod;
      this.panelEditUser.BorderWidth = 2;
      this.panelEditUser.Controls.Add(this.tableLayoutPanelDetailUser);
      this.panelEditUser.Controls.Add(this.panelCancelSave);
      this.panelEditUser.Dock = System.Windows.Forms.DockStyle.Right;
      this.panelEditUser.Location = new System.Drawing.Point(781, 146);
      this.panelEditUser.Name = "panelEditUser";
      this.panelEditUser.Size = new System.Drawing.Size(445, 348);
      this.panelEditUser.TabIndex = 31;
      // 
      // tableLayoutPanelDetailUser
      // 
      this.tableLayoutPanelDetailUser.ColumnCount = 2;
      this.tableLayoutPanelDetailUser.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
      this.tableLayoutPanelDetailUser.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
      this.tableLayoutPanelDetailUser.Controls.Add(this.pnlLblUsername, 0, 0);
      this.tableLayoutPanelDetailUser.Controls.Add(this.pnlUsername, 1, 0);
      this.tableLayoutPanelDetailUser.Controls.Add(this.pnlLblPassword, 0, 1);
      this.tableLayoutPanelDetailUser.Controls.Add(this.pnlPassword, 1, 1);
      this.tableLayoutPanelDetailUser.Controls.Add(this.panelLblBadge, 0, 2);
      this.tableLayoutPanelDetailUser.Controls.Add(this.panelBadge, 1, 2);
      this.tableLayoutPanelDetailUser.Controls.Add(this.pnlLblFullName, 0, 3);
      this.tableLayoutPanelDetailUser.Controls.Add(this.pnlFullName, 1, 3);
      this.tableLayoutPanelDetailUser.Controls.Add(this.pnlLblCompany, 0, 4);
      this.tableLayoutPanelDetailUser.Controls.Add(this.pnlCompany, 1, 4);
      this.tableLayoutPanelDetailUser.Controls.Add(this.pnlLblProfile, 0, 5);
      this.tableLayoutPanelDetailUser.Controls.Add(this.pnlProfile, 1, 5);
      this.tableLayoutPanelDetailUser.Controls.Add(this.panelLblBlocked, 0, 6);
      this.tableLayoutPanelDetailUser.Controls.Add(this.panelBlocked, 1, 6);
      this.tableLayoutPanelDetailUser.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanelDetailUser.Location = new System.Drawing.Point(2, 2);
      this.tableLayoutPanelDetailUser.Name = "tableLayoutPanelDetailUser";
      this.tableLayoutPanelDetailUser.RowCount = 7;
      this.tableLayoutPanelDetailUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.2851F));
      this.tableLayoutPanelDetailUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.2851F));
      this.tableLayoutPanelDetailUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28939F));
      this.tableLayoutPanelDetailUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.2851F));
      this.tableLayoutPanelDetailUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.2851F));
      this.tableLayoutPanelDetailUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.2851F));
      this.tableLayoutPanelDetailUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.2851F));
      this.tableLayoutPanelDetailUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanelDetailUser.Size = new System.Drawing.Size(441, 289);
      this.tableLayoutPanelDetailUser.TabIndex = 18;
      // 
      // pnlLblUsername
      // 
      this.pnlLblUsername.Controls.Add(this.lblTrUsername);
      this.pnlLblUsername.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlLblUsername.Location = new System.Drawing.Point(3, 3);
      this.pnlLblUsername.Name = "pnlLblUsername";
      this.pnlLblUsername.Size = new System.Drawing.Size(148, 35);
      this.pnlLblUsername.TabIndex = 1;
      // 
      // lblTrUsername
      // 
      this.lblTrUsername.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblTrUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblTrUsername.Location = new System.Drawing.Point(0, 0);
      this.lblTrUsername.Name = "lblTrUsername";
      this.lblTrUsername.Size = new System.Drawing.Size(148, 35);
      this.lblTrUsername.TabIndex = 1;
      this.lblTrUsername.Text = "Username";
      this.lblTrUsername.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // pnlUsername
      // 
      this.pnlUsername.Controls.Add(this.txbUsername);
      this.pnlUsername.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlUsername.Location = new System.Drawing.Point(157, 3);
      this.pnlUsername.Name = "pnlUsername";
      this.pnlUsername.Padding = new System.Windows.Forms.Padding(5);
      this.pnlUsername.Size = new System.Drawing.Size(281, 35);
      this.pnlUsername.TabIndex = 0;
      // 
      // txbUsername
      // 
      this.txbUsername.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.txbUsername.BackColor = System.Drawing.SystemColors.Window;
      this.txbUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txbUsername.Location = new System.Drawing.Point(5, 5);
      this.txbUsername.MaxLength = 20;
      this.txbUsername.Name = "txbUsername";
      this.txbUsername.Size = new System.Drawing.Size(271, 26);
      this.txbUsername.TabIndex = 1;
      // 
      // pnlLblPassword
      // 
      this.pnlLblPassword.Controls.Add(this.lblTrPassword);
      this.pnlLblPassword.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlLblPassword.Location = new System.Drawing.Point(3, 44);
      this.pnlLblPassword.Name = "pnlLblPassword";
      this.pnlLblPassword.Size = new System.Drawing.Size(148, 35);
      this.pnlLblPassword.TabIndex = 2;
      // 
      // lblTrPassword
      // 
      this.lblTrPassword.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblTrPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblTrPassword.Location = new System.Drawing.Point(0, 0);
      this.lblTrPassword.Name = "lblTrPassword";
      this.lblTrPassword.Size = new System.Drawing.Size(148, 35);
      this.lblTrPassword.TabIndex = 1;
      this.lblTrPassword.Text = "Password";
      this.lblTrPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // pnlPassword
      // 
      this.pnlPassword.Controls.Add(this.txbPassword);
      this.pnlPassword.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlPassword.Location = new System.Drawing.Point(157, 44);
      this.pnlPassword.Name = "pnlPassword";
      this.pnlPassword.Padding = new System.Windows.Forms.Padding(5);
      this.pnlPassword.Size = new System.Drawing.Size(281, 35);
      this.pnlPassword.TabIndex = 1;
      // 
      // txbPassword
      // 
      this.txbPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.txbPassword.BackColor = System.Drawing.SystemColors.Window;
      this.txbPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txbPassword.Location = new System.Drawing.Point(5, 5);
      this.txbPassword.MaxLength = 20;
      this.txbPassword.Name = "txbPassword";
      this.txbPassword.PasswordChar = '*';
      this.txbPassword.Size = new System.Drawing.Size(271, 26);
      this.txbPassword.TabIndex = 2;
      // 
      // panelLblBadge
      // 
      this.panelLblBadge.Controls.Add(this.lblTrBadge);
      this.panelLblBadge.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelLblBadge.Location = new System.Drawing.Point(3, 85);
      this.panelLblBadge.Name = "panelLblBadge";
      this.panelLblBadge.Size = new System.Drawing.Size(148, 35);
      this.panelLblBadge.TabIndex = 15;
      // 
      // lblTrBadge
      // 
      this.lblTrBadge.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblTrBadge.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblTrBadge.Location = new System.Drawing.Point(0, 0);
      this.lblTrBadge.Name = "lblTrBadge";
      this.lblTrBadge.Size = new System.Drawing.Size(148, 35);
      this.lblTrBadge.TabIndex = 1;
      this.lblTrBadge.Text = "Badge";
      this.lblTrBadge.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // panelBadge
      // 
      this.panelBadge.Controls.Add(this.txbBadge);
      this.panelBadge.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelBadge.Location = new System.Drawing.Point(157, 85);
      this.panelBadge.Name = "panelBadge";
      this.panelBadge.Padding = new System.Windows.Forms.Padding(5);
      this.panelBadge.Size = new System.Drawing.Size(281, 35);
      this.panelBadge.TabIndex = 2;
      // 
      // txbBadge
      // 
      this.txbBadge.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.txbBadge.BackColor = System.Drawing.SystemColors.Window;
      this.txbBadge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txbBadge.Location = new System.Drawing.Point(5, 5);
      this.txbBadge.MaxLength = 12;
      this.txbBadge.Name = "txbBadge";
      this.txbBadge.Size = new System.Drawing.Size(271, 26);
      this.txbBadge.TabIndex = 3;
      // 
      // pnlLblFullName
      // 
      this.pnlLblFullName.Controls.Add(this.lblTrFullName);
      this.pnlLblFullName.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlLblFullName.Location = new System.Drawing.Point(3, 126);
      this.pnlLblFullName.Name = "pnlLblFullName";
      this.pnlLblFullName.Size = new System.Drawing.Size(148, 35);
      this.pnlLblFullName.TabIndex = 6;
      // 
      // lblTrFullName
      // 
      this.lblTrFullName.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblTrFullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblTrFullName.Location = new System.Drawing.Point(0, 0);
      this.lblTrFullName.Name = "lblTrFullName";
      this.lblTrFullName.Size = new System.Drawing.Size(148, 35);
      this.lblTrFullName.TabIndex = 0;
      this.lblTrFullName.Text = "FullName";
      this.lblTrFullName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // pnlFullName
      // 
      this.pnlFullName.Controls.Add(this.txbFullName);
      this.pnlFullName.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlFullName.Location = new System.Drawing.Point(157, 126);
      this.pnlFullName.Name = "pnlFullName";
      this.pnlFullName.Padding = new System.Windows.Forms.Padding(5);
      this.pnlFullName.Size = new System.Drawing.Size(281, 35);
      this.pnlFullName.TabIndex = 3;
      // 
      // txbFullName
      // 
      this.txbFullName.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.txbFullName.BackColor = System.Drawing.SystemColors.Window;
      this.txbFullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txbFullName.Location = new System.Drawing.Point(5, 5);
      this.txbFullName.MaxLength = 50;
      this.txbFullName.Name = "txbFullName";
      this.txbFullName.Size = new System.Drawing.Size(271, 26);
      this.txbFullName.TabIndex = 4;
      // 
      // pnlLblCompany
      // 
      this.pnlLblCompany.Controls.Add(this.lblTrCompany);
      this.pnlLblCompany.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlLblCompany.Location = new System.Drawing.Point(3, 167);
      this.pnlLblCompany.Name = "pnlLblCompany";
      this.pnlLblCompany.Size = new System.Drawing.Size(148, 35);
      this.pnlLblCompany.TabIndex = 4;
      // 
      // lblTrCompany
      // 
      this.lblTrCompany.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblTrCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblTrCompany.Location = new System.Drawing.Point(0, 0);
      this.lblTrCompany.Name = "lblTrCompany";
      this.lblTrCompany.Size = new System.Drawing.Size(148, 35);
      this.lblTrCompany.TabIndex = 1;
      this.lblTrCompany.Text = "Company";
      this.lblTrCompany.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // pnlCompany
      // 
      this.pnlCompany.Controls.Add(this.txbCompany);
      this.pnlCompany.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlCompany.ForeColor = System.Drawing.SystemColors.ControlText;
      this.pnlCompany.Location = new System.Drawing.Point(157, 167);
      this.pnlCompany.Name = "pnlCompany";
      this.pnlCompany.Padding = new System.Windows.Forms.Padding(5);
      this.pnlCompany.Size = new System.Drawing.Size(281, 35);
      this.pnlCompany.TabIndex = 4;
      // 
      // txbCompany
      // 
      this.txbCompany.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.txbCompany.BackColor = System.Drawing.SystemColors.Window;
      this.txbCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txbCompany.Location = new System.Drawing.Point(5, 5);
      this.txbCompany.MaxLength = 50;
      this.txbCompany.Name = "txbCompany";
      this.txbCompany.Size = new System.Drawing.Size(271, 26);
      this.txbCompany.TabIndex = 5;
      // 
      // pnlLblProfile
      // 
      this.pnlLblProfile.Controls.Add(this.lblTrProfile);
      this.pnlLblProfile.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlLblProfile.Location = new System.Drawing.Point(3, 208);
      this.pnlLblProfile.Name = "pnlLblProfile";
      this.pnlLblProfile.Size = new System.Drawing.Size(148, 35);
      this.pnlLblProfile.TabIndex = 8;
      // 
      // lblTrProfile
      // 
      this.lblTrProfile.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblTrProfile.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblTrProfile.Location = new System.Drawing.Point(0, 0);
      this.lblTrProfile.Name = "lblTrProfile";
      this.lblTrProfile.Size = new System.Drawing.Size(148, 35);
      this.lblTrProfile.TabIndex = 1;
      this.lblTrProfile.Text = "Profile";
      this.lblTrProfile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // pnlProfile
      // 
      this.pnlProfile.Controls.Add(this.cmbProfile);
      this.pnlProfile.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlProfile.Location = new System.Drawing.Point(157, 208);
      this.pnlProfile.Name = "pnlProfile";
      this.pnlProfile.Padding = new System.Windows.Forms.Padding(1);
      this.pnlProfile.Size = new System.Drawing.Size(281, 35);
      this.pnlProfile.TabIndex = 5;
      // 
      // cmbProfile
      // 
      this.cmbProfile.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.cmbProfile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbProfile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cmbProfile.ForeColor = System.Drawing.SystemColors.WindowText;
      this.cmbProfile.FormattingEnabled = true;
      this.cmbProfile.Location = new System.Drawing.Point(5, 5);
      this.cmbProfile.MaxDropDownItems = 3;
      this.cmbProfile.Name = "cmbProfile";
      this.cmbProfile.Size = new System.Drawing.Size(269, 28);
      this.cmbProfile.TabIndex = 6;
      // 
      // panelLblBlocked
      // 
      this.panelLblBlocked.Controls.Add(this.lblTrBlocked);
      this.panelLblBlocked.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelLblBlocked.Location = new System.Drawing.Point(3, 249);
      this.panelLblBlocked.Name = "panelLblBlocked";
      this.panelLblBlocked.Size = new System.Drawing.Size(148, 37);
      this.panelLblBlocked.TabIndex = 14;
      // 
      // lblTrBlocked
      // 
      this.lblTrBlocked.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblTrBlocked.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblTrBlocked.Location = new System.Drawing.Point(0, 0);
      this.lblTrBlocked.Name = "lblTrBlocked";
      this.lblTrBlocked.Size = new System.Drawing.Size(148, 37);
      this.lblTrBlocked.TabIndex = 1;
      this.lblTrBlocked.Text = "Blocked";
      this.lblTrBlocked.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // panelBlocked
      // 
      this.panelBlocked.Controls.Add(this.checkBoxBlocked);
      this.panelBlocked.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelBlocked.Location = new System.Drawing.Point(157, 249);
      this.panelBlocked.Name = "panelBlocked";
      this.panelBlocked.Size = new System.Drawing.Size(281, 37);
      this.panelBlocked.TabIndex = 6;
      // 
      // checkBoxBlocked
      // 
      this.checkBoxBlocked.AutoSize = true;
      this.checkBoxBlocked.Dock = System.Windows.Forms.DockStyle.Left;
      this.checkBoxBlocked.Location = new System.Drawing.Point(0, 0);
      this.checkBoxBlocked.Name = "checkBoxBlocked";
      this.checkBoxBlocked.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
      this.checkBoxBlocked.Size = new System.Drawing.Size(20, 37);
      this.checkBoxBlocked.TabIndex = 7;
      this.checkBoxBlocked.UseVisualStyleBackColor = true;
      // 
      // panelCancelSave
      // 
      this.panelCancelSave.Controls.Add(this.panelCancel);
      this.panelCancelSave.Controls.Add(this.panelSave);
      this.panelCancelSave.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panelCancelSave.Location = new System.Drawing.Point(2, 291);
      this.panelCancelSave.Name = "panelCancelSave";
      this.panelCancelSave.Size = new System.Drawing.Size(441, 55);
      this.panelCancelSave.TabIndex = 1;
      // 
      // panelCancel
      // 
      this.panelCancel.Controls.Add(this.btnCancel);
      this.panelCancel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelCancel.Location = new System.Drawing.Point(0, 0);
      this.panelCancel.Name = "panelCancel";
      this.panelCancel.Size = new System.Drawing.Size(221, 55);
      this.panelCancel.TabIndex = 12;
      // 
      // btnCancel
      // 
      this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCancel.BackColor1 = System.Drawing.Color.IndianRed;
      this.btnCancel.BackColor2 = System.Drawing.Color.Maroon;
      this.btnCancel.ButtonImage = global::Authentication.Properties.Resources.Cancel;
      this.btnCancel.ButtonText = "Cancel";
      this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold);
      this.btnCancel.Location = new System.Drawing.Point(40, 6);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.PressColor1 = System.Drawing.Color.Maroon;
      this.btnCancel.PressColor2 = System.Drawing.Color.Maroon;
      this.btnCancel.Size = new System.Drawing.Size(141, 45);
      this.btnCancel.TabIndex = 0;
      this.btnCancel.TextColor = System.Drawing.Color.White;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // panelSave
      // 
      this.panelSave.Controls.Add(this.btnSave);
      this.panelSave.Dock = System.Windows.Forms.DockStyle.Right;
      this.panelSave.Location = new System.Drawing.Point(221, 0);
      this.panelSave.Name = "panelSave";
      this.panelSave.Size = new System.Drawing.Size(220, 55);
      this.panelSave.TabIndex = 11;
      // 
      // btnSave
      // 
      this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSave.BackColor1 = System.Drawing.Color.SkyBlue;
      this.btnSave.BackColor2 = System.Drawing.Color.DeepSkyBlue;
      this.btnSave.ButtonImage = global::Authentication.Properties.Resources.Save;
      this.btnSave.ButtonText = "Save";
      this.btnSave.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold);
      this.btnSave.Location = new System.Drawing.Point(38, 6);
      this.btnSave.Name = "btnSave";
      this.btnSave.PressColor1 = System.Drawing.Color.DeepSkyBlue;
      this.btnSave.PressColor2 = System.Drawing.Color.DeepSkyBlue;
      this.btnSave.Size = new System.Drawing.Size(141, 45);
      this.btnSave.TabIndex = 1;
      this.btnSave.TextColor = System.Drawing.Color.White;
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      // 
      // panelButtons
      // 
      this.panelButtons.BorderColor = System.Drawing.Color.SkyBlue;
      this.panelButtons.BorderWidth = 2;
      this.panelButtons.Controls.Add(this.pnlMessage);
      this.panelButtons.Controls.Add(this.flowLayoutPanel1);
      this.panelButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panelButtons.Location = new System.Drawing.Point(0, 494);
      this.panelButtons.Name = "panelButtons";
      this.panelButtons.Size = new System.Drawing.Size(1226, 60);
      this.panelButtons.TabIndex = 11;
      // 
      // pnlMessage
      // 
      this.pnlMessage.BorderColor = System.Drawing.Color.DodgerBlue;
      this.pnlMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlMessage.BorderWidth = 2;
      this.pnlMessage.Controls.Add(this.textBoxError);
      this.pnlMessage.Controls.Add(this.panelInfo);
      this.pnlMessage.Curvature = 7;
      this.pnlMessage.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnlMessage.Location = new System.Drawing.Point(781, 2);
      this.pnlMessage.Name = "pnlMessage";
      this.pnlMessage.Padding = new System.Windows.Forms.Padding(6);
      this.pnlMessage.Size = new System.Drawing.Size(443, 56);
      this.pnlMessage.TabIndex = 11;
      this.pnlMessage.Visible = false;
      // 
      // textBoxError
      // 
      this.textBoxError.Dock = System.Windows.Forms.DockStyle.Fill;
      this.textBoxError.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxError.ForeColor = System.Drawing.Color.Red;
      this.textBoxError.Location = new System.Drawing.Point(75, 6);
      this.textBoxError.Multiline = true;
      this.textBoxError.Name = "textBoxError";
      this.textBoxError.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.textBoxError.Size = new System.Drawing.Size(362, 44);
      this.textBoxError.TabIndex = 1;
      this.textBoxError.Text = "Error";
      // 
      // panelInfo
      // 
      this.panelInfo.BackgroundImage = global::Authentication.Properties.Resources.Warning;
      this.panelInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.panelInfo.Dock = System.Windows.Forms.DockStyle.Left;
      this.panelInfo.Location = new System.Drawing.Point(6, 6);
      this.panelInfo.Name = "panelInfo";
      this.panelInfo.Size = new System.Drawing.Size(69, 44);
      this.panelInfo.TabIndex = 0;
      // 
      // flowLayoutPanel1
      // 
      this.flowLayoutPanel1.Controls.Add(this.btnDeleteUser);
      this.flowLayoutPanel1.Controls.Add(this.btnEditUser);
      this.flowLayoutPanel1.Controls.Add(this.btnCreateUser);
      this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
      this.flowLayoutPanel1.Name = "flowLayoutPanel1";
      this.flowLayoutPanel1.Size = new System.Drawing.Size(1222, 56);
      this.flowLayoutPanel1.TabIndex = 0;
      // 
      // btnDeleteUser
      // 
      this.btnDeleteUser.BackColor1 = System.Drawing.Color.IndianRed;
      this.btnDeleteUser.BackColor2 = System.Drawing.Color.Red;
      this.btnDeleteUser.ButtonText = "Delete User";
      this.btnDeleteUser.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnDeleteUser.Location = new System.Drawing.Point(3, 3);
      this.btnDeleteUser.Name = "btnDeleteUser";
      this.btnDeleteUser.Padding = new System.Windows.Forms.Padding(60, 0, 0, 0);
      this.btnDeleteUser.PressColor1 = System.Drawing.Color.Red;
      this.btnDeleteUser.PressColor2 = System.Drawing.Color.Red;
      this.btnDeleteUser.Size = new System.Drawing.Size(210, 45);
      this.btnDeleteUser.TabIndex = 29;
      this.btnDeleteUser.TextColor = System.Drawing.Color.White;
      this.btnDeleteUser.Click += new System.EventHandler(this.btnDeleteUser_Click);
      // 
      // btnEditUser
      // 
      this.btnEditUser.BackColor1 = System.Drawing.Color.Gold;
      this.btnEditUser.BackColor2 = System.Drawing.Color.Orange;
      this.btnEditUser.ButtonText = "Edit User";
      this.btnEditUser.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnEditUser.Location = new System.Drawing.Point(219, 3);
      this.btnEditUser.Name = "btnEditUser";
      this.btnEditUser.Padding = new System.Windows.Forms.Padding(60, 0, 0, 0);
      this.btnEditUser.PressColor1 = System.Drawing.Color.Orange;
      this.btnEditUser.PressColor2 = System.Drawing.Color.Orange;
      this.btnEditUser.Size = new System.Drawing.Size(210, 45);
      this.btnEditUser.TabIndex = 30;
      this.btnEditUser.TextColor = System.Drawing.Color.White;
      this.btnEditUser.Click += new System.EventHandler(this.btnEditUser_Click);
      // 
      // btnCreateUser
      // 
      this.btnCreateUser.BackColor1 = System.Drawing.Color.ForestGreen;
      this.btnCreateUser.BackColor2 = System.Drawing.Color.Green;
      this.btnCreateUser.ButtonText = "Create User";
      this.btnCreateUser.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnCreateUser.Location = new System.Drawing.Point(435, 3);
      this.btnCreateUser.Name = "btnCreateUser";
      this.btnCreateUser.Padding = new System.Windows.Forms.Padding(60, 0, 0, 0);
      this.btnCreateUser.PressColor1 = System.Drawing.Color.Green;
      this.btnCreateUser.PressColor2 = System.Drawing.Color.Green;
      this.btnCreateUser.Size = new System.Drawing.Size(210, 45);
      this.btnCreateUser.TabIndex = 31;
      this.btnCreateUser.TextColor = System.Drawing.Color.White;
      this.btnCreateUser.Click += new System.EventHandler(this.btnCreateUser_Click);
      // 
      // panelHeaderFilters
      // 
      this.panelHeaderFilters.Controls.Add(this.panelFiltersAndRefresh);
      this.panelHeaderFilters.Controls.Add(this.labelTrFilterUsers);
      this.panelHeaderFilters.Dock = System.Windows.Forms.DockStyle.Top;
      this.panelHeaderFilters.Location = new System.Drawing.Point(0, 0);
      this.panelHeaderFilters.Name = "panelHeaderFilters";
      this.panelHeaderFilters.Size = new System.Drawing.Size(1226, 146);
      this.panelHeaderFilters.TabIndex = 32;
      // 
      // panelFiltersAndRefresh
      // 
      this.panelFiltersAndRefresh.Controls.Add(this.panelFilters);
      this.panelFiltersAndRefresh.Controls.Add(this.panelRefresh);
      this.panelFiltersAndRefresh.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelFiltersAndRefresh.Location = new System.Drawing.Point(0, 25);
      this.panelFiltersAndRefresh.Name = "panelFiltersAndRefresh";
      this.panelFiltersAndRefresh.Size = new System.Drawing.Size(1226, 121);
      this.panelFiltersAndRefresh.TabIndex = 16;
      // 
      // panelFilters
      // 
      this.panelFilters.BackColor = System.Drawing.SystemColors.Control;
      this.panelFilters.Controls.Add(this.flowLayoutPanelFilters);
      this.panelFilters.Controls.Add(this.panelResetFilters);
      this.panelFilters.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelFilters.Location = new System.Drawing.Point(0, 0);
      this.panelFilters.Name = "panelFilters";
      this.panelFilters.Size = new System.Drawing.Size(1050, 121);
      this.panelFilters.TabIndex = 15;
      // 
      // flowLayoutPanelFilters
      // 
      this.flowLayoutPanelFilters.AutoScroll = true;
      this.flowLayoutPanelFilters.Controls.Add(this.panelUsernameFilter);
      this.flowLayoutPanelFilters.Controls.Add(this.panelBadgeFilter);
      this.flowLayoutPanelFilters.Controls.Add(this.panelFullNameFilter);
      this.flowLayoutPanelFilters.Controls.Add(this.panelCompanyFilter);
      this.flowLayoutPanelFilters.Controls.Add(this.panelProfileFilter);
      this.flowLayoutPanelFilters.Controls.Add(this.panelBlockedFilter);
      this.flowLayoutPanelFilters.Dock = System.Windows.Forms.DockStyle.Fill;
      this.flowLayoutPanelFilters.Location = new System.Drawing.Point(0, 0);
      this.flowLayoutPanelFilters.Name = "flowLayoutPanelFilters";
      this.flowLayoutPanelFilters.Size = new System.Drawing.Size(879, 121);
      this.flowLayoutPanelFilters.TabIndex = 30;
      // 
      // panelUsernameFilter
      // 
      this.panelUsernameFilter.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.panelUsernameFilter.Controls.Add(this.textBoxUsernameFilter);
      this.panelUsernameFilter.Controls.Add(this.labelTrUsernameContains);
      this.panelUsernameFilter.Location = new System.Drawing.Point(3, 3);
      this.panelUsernameFilter.Name = "panelUsernameFilter";
      this.panelUsernameFilter.Size = new System.Drawing.Size(165, 52);
      this.panelUsernameFilter.TabIndex = 20;
      // 
      // textBoxUsernameFilter
      // 
      this.textBoxUsernameFilter.Dock = System.Windows.Forms.DockStyle.Fill;
      this.textBoxUsernameFilter.Font = new System.Drawing.Font("Segoe UI", 9.75F);
      this.textBoxUsernameFilter.Location = new System.Drawing.Point(0, 21);
      this.textBoxUsernameFilter.Name = "textBoxUsernameFilter";
      this.textBoxUsernameFilter.Size = new System.Drawing.Size(165, 25);
      this.textBoxUsernameFilter.TabIndex = 20;
      this.textBoxUsernameFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
      // 
      // labelTrUsernameContains
      // 
      this.labelTrUsernameContains.AutoSize = true;
      this.labelTrUsernameContains.Dock = System.Windows.Forms.DockStyle.Top;
      this.labelTrUsernameContains.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTrUsernameContains.Location = new System.Drawing.Point(0, 0);
      this.labelTrUsernameContains.Name = "labelTrUsernameContains";
      this.labelTrUsernameContains.Size = new System.Drawing.Size(150, 21);
      this.labelTrUsernameContains.TabIndex = 15;
      this.labelTrUsernameContains.Text = "Username Contains";
      this.labelTrUsernameContains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // panelBadgeFilter
      // 
      this.panelBadgeFilter.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.panelBadgeFilter.Controls.Add(this.textBoxBadgeFilter);
      this.panelBadgeFilter.Controls.Add(this.labelTrBadgeContains);
      this.panelBadgeFilter.Location = new System.Drawing.Point(174, 3);
      this.panelBadgeFilter.Name = "panelBadgeFilter";
      this.panelBadgeFilter.Size = new System.Drawing.Size(165, 52);
      this.panelBadgeFilter.TabIndex = 21;
      // 
      // textBoxBadgeFilter
      // 
      this.textBoxBadgeFilter.Dock = System.Windows.Forms.DockStyle.Fill;
      this.textBoxBadgeFilter.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxBadgeFilter.Location = new System.Drawing.Point(0, 21);
      this.textBoxBadgeFilter.Name = "textBoxBadgeFilter";
      this.textBoxBadgeFilter.Size = new System.Drawing.Size(165, 25);
      this.textBoxBadgeFilter.TabIndex = 21;
      this.textBoxBadgeFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
      // 
      // labelTrBadgeContains
      // 
      this.labelTrBadgeContains.AutoSize = true;
      this.labelTrBadgeContains.Dock = System.Windows.Forms.DockStyle.Top;
      this.labelTrBadgeContains.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTrBadgeContains.Location = new System.Drawing.Point(0, 0);
      this.labelTrBadgeContains.Name = "labelTrBadgeContains";
      this.labelTrBadgeContains.Size = new System.Drawing.Size(124, 21);
      this.labelTrBadgeContains.TabIndex = 15;
      this.labelTrBadgeContains.Text = "Badge Contains";
      this.labelTrBadgeContains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // panelFullNameFilter
      // 
      this.panelFullNameFilter.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.panelFullNameFilter.Controls.Add(this.textBoxFullNameFilter);
      this.panelFullNameFilter.Controls.Add(this.labelTrFullNameContains);
      this.panelFullNameFilter.Location = new System.Drawing.Point(345, 3);
      this.panelFullNameFilter.Name = "panelFullNameFilter";
      this.panelFullNameFilter.Size = new System.Drawing.Size(165, 52);
      this.panelFullNameFilter.TabIndex = 22;
      // 
      // textBoxFullNameFilter
      // 
      this.textBoxFullNameFilter.Dock = System.Windows.Forms.DockStyle.Fill;
      this.textBoxFullNameFilter.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxFullNameFilter.Location = new System.Drawing.Point(0, 21);
      this.textBoxFullNameFilter.Name = "textBoxFullNameFilter";
      this.textBoxFullNameFilter.Size = new System.Drawing.Size(165, 25);
      this.textBoxFullNameFilter.TabIndex = 22;
      this.textBoxFullNameFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
      // 
      // labelTrFullNameContains
      // 
      this.labelTrFullNameContains.AutoSize = true;
      this.labelTrFullNameContains.Dock = System.Windows.Forms.DockStyle.Top;
      this.labelTrFullNameContains.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTrFullNameContains.Location = new System.Drawing.Point(0, 0);
      this.labelTrFullNameContains.Name = "labelTrFullNameContains";
      this.labelTrFullNameContains.Size = new System.Drawing.Size(145, 21);
      this.labelTrFullNameContains.TabIndex = 15;
      this.labelTrFullNameContains.Text = "FullName Contains";
      this.labelTrFullNameContains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // panelCompanyFilter
      // 
      this.panelCompanyFilter.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.panelCompanyFilter.Controls.Add(this.textBoxCompanyFilter);
      this.panelCompanyFilter.Controls.Add(this.labelTrCompanyContains);
      this.panelCompanyFilter.Location = new System.Drawing.Point(516, 3);
      this.panelCompanyFilter.Name = "panelCompanyFilter";
      this.panelCompanyFilter.Size = new System.Drawing.Size(165, 52);
      this.panelCompanyFilter.TabIndex = 23;
      // 
      // textBoxCompanyFilter
      // 
      this.textBoxCompanyFilter.Dock = System.Windows.Forms.DockStyle.Fill;
      this.textBoxCompanyFilter.Font = new System.Drawing.Font("Segoe UI", 9.75F);
      this.textBoxCompanyFilter.Location = new System.Drawing.Point(0, 21);
      this.textBoxCompanyFilter.Name = "textBoxCompanyFilter";
      this.textBoxCompanyFilter.Size = new System.Drawing.Size(165, 25);
      this.textBoxCompanyFilter.TabIndex = 23;
      this.textBoxCompanyFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
      // 
      // labelTrCompanyContains
      // 
      this.labelTrCompanyContains.AutoSize = true;
      this.labelTrCompanyContains.Dock = System.Windows.Forms.DockStyle.Top;
      this.labelTrCompanyContains.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTrCompanyContains.Location = new System.Drawing.Point(0, 0);
      this.labelTrCompanyContains.Name = "labelTrCompanyContains";
      this.labelTrCompanyContains.Size = new System.Drawing.Size(146, 21);
      this.labelTrCompanyContains.TabIndex = 15;
      this.labelTrCompanyContains.Text = "Company Contains";
      this.labelTrCompanyContains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // panelProfileFilter
      // 
      this.panelProfileFilter.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.panelProfileFilter.Controls.Add(this.textBoxProfileFilter);
      this.panelProfileFilter.Controls.Add(this.labelTrProfileContains);
      this.panelProfileFilter.Location = new System.Drawing.Point(687, 3);
      this.panelProfileFilter.Name = "panelProfileFilter";
      this.panelProfileFilter.Size = new System.Drawing.Size(165, 52);
      this.panelProfileFilter.TabIndex = 24;
      // 
      // textBoxProfileFilter
      // 
      this.textBoxProfileFilter.Dock = System.Windows.Forms.DockStyle.Fill;
      this.textBoxProfileFilter.Font = new System.Drawing.Font("Segoe UI", 9.75F);
      this.textBoxProfileFilter.Location = new System.Drawing.Point(0, 21);
      this.textBoxProfileFilter.Name = "textBoxProfileFilter";
      this.textBoxProfileFilter.Size = new System.Drawing.Size(165, 25);
      this.textBoxProfileFilter.TabIndex = 24;
      this.textBoxProfileFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
      // 
      // labelTrProfileContains
      // 
      this.labelTrProfileContains.AutoSize = true;
      this.labelTrProfileContains.Dock = System.Windows.Forms.DockStyle.Top;
      this.labelTrProfileContains.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTrProfileContains.Location = new System.Drawing.Point(0, 0);
      this.labelTrProfileContains.Name = "labelTrProfileContains";
      this.labelTrProfileContains.Size = new System.Drawing.Size(125, 21);
      this.labelTrProfileContains.TabIndex = 15;
      this.labelTrProfileContains.Text = "Profile Contains";
      this.labelTrProfileContains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // panelBlockedFilter
      // 
      this.panelBlockedFilter.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.panelBlockedFilter.Controls.Add(this.comboBoxBlockedFilter);
      this.panelBlockedFilter.Controls.Add(this.labelTrBlockedContains);
      this.panelBlockedFilter.Location = new System.Drawing.Point(3, 61);
      this.panelBlockedFilter.Name = "panelBlockedFilter";
      this.panelBlockedFilter.Size = new System.Drawing.Size(165, 52);
      this.panelBlockedFilter.TabIndex = 25;
      // 
      // comboBoxBlockedFilter
      // 
      this.comboBoxBlockedFilter.Dock = System.Windows.Forms.DockStyle.Fill;
      this.comboBoxBlockedFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBoxBlockedFilter.Font = new System.Drawing.Font("Segoe UI", 9.75F);
      this.comboBoxBlockedFilter.FormattingEnabled = true;
      this.comboBoxBlockedFilter.Location = new System.Drawing.Point(0, 21);
      this.comboBoxBlockedFilter.Name = "comboBoxBlockedFilter";
      this.comboBoxBlockedFilter.Size = new System.Drawing.Size(165, 25);
      this.comboBoxBlockedFilter.TabIndex = 25;
      this.comboBoxBlockedFilter.SelectedValueChanged += new System.EventHandler(this.comboBoxBlockedFilter_SelectedValueChanged);
      // 
      // labelTrBlockedContains
      // 
      this.labelTrBlockedContains.AutoSize = true;
      this.labelTrBlockedContains.Dock = System.Windows.Forms.DockStyle.Top;
      this.labelTrBlockedContains.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTrBlockedContains.Location = new System.Drawing.Point(0, 0);
      this.labelTrBlockedContains.Name = "labelTrBlockedContains";
      this.labelTrBlockedContains.Size = new System.Drawing.Size(136, 21);
      this.labelTrBlockedContains.TabIndex = 15;
      this.labelTrBlockedContains.Text = "Blocked Contains";
      this.labelTrBlockedContains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // panelResetFilters
      // 
      this.panelResetFilters.Controls.Add(this.btnResetFilters);
      this.panelResetFilters.Dock = System.Windows.Forms.DockStyle.Right;
      this.panelResetFilters.Location = new System.Drawing.Point(879, 0);
      this.panelResetFilters.Name = "panelResetFilters";
      this.panelResetFilters.Size = new System.Drawing.Size(171, 121);
      this.panelResetFilters.TabIndex = 24;
      // 
      // btnResetFilters
      // 
      this.btnResetFilters.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btnResetFilters.BackColor1 = System.Drawing.Color.Coral;
      this.btnResetFilters.BackColor2 = System.Drawing.Color.Orange;
      this.btnResetFilters.ButtonText = "Reset Filters";
      this.btnResetFilters.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnResetFilters.Location = new System.Drawing.Point(11, 40);
      this.btnResetFilters.Name = "btnResetFilters";
      this.btnResetFilters.PressColor1 = System.Drawing.Color.DarkOrange;
      this.btnResetFilters.PressColor2 = System.Drawing.Color.DarkOrange;
      this.btnResetFilters.Size = new System.Drawing.Size(150, 40);
      this.btnResetFilters.TabIndex = 24;
      this.btnResetFilters.TextColor = System.Drawing.Color.White;
      this.btnResetFilters.Click += new System.EventHandler(this.btnResetFilters_Click);
      // 
      // panelRefresh
      // 
      this.panelRefresh.Controls.Add(this.coloredBorderPanelAutoRefresh);
      this.panelRefresh.Dock = System.Windows.Forms.DockStyle.Right;
      this.panelRefresh.Location = new System.Drawing.Point(1050, 0);
      this.panelRefresh.Name = "panelRefresh";
      this.panelRefresh.Size = new System.Drawing.Size(176, 121);
      this.panelRefresh.TabIndex = 25;
      // 
      // coloredBorderPanelAutoRefresh
      // 
      this.coloredBorderPanelAutoRefresh.BorderColor = System.Drawing.Color.RoyalBlue;
      this.coloredBorderPanelAutoRefresh.BorderWidth = 2;
      this.coloredBorderPanelAutoRefresh.Controls.Add(this.btnRefresh);
      this.coloredBorderPanelAutoRefresh.Dock = System.Windows.Forms.DockStyle.Fill;
      this.coloredBorderPanelAutoRefresh.Location = new System.Drawing.Point(0, 0);
      this.coloredBorderPanelAutoRefresh.Name = "coloredBorderPanelAutoRefresh";
      this.coloredBorderPanelAutoRefresh.Size = new System.Drawing.Size(176, 121);
      this.coloredBorderPanelAutoRefresh.TabIndex = 25;
      // 
      // btnRefresh
      // 
      this.btnRefresh.BackColor1 = System.Drawing.Color.RoyalBlue;
      this.btnRefresh.BackColor2 = System.Drawing.SystemColors.Highlight;
      this.btnRefresh.ButtonText = "Refresh";
      this.btnRefresh.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnRefresh.Location = new System.Drawing.Point(43, 37);
      this.btnRefresh.Name = "btnRefresh";
      this.btnRefresh.PressColor1 = System.Drawing.SystemColors.HotTrack;
      this.btnRefresh.PressColor2 = System.Drawing.SystemColors.MenuHighlight;
      this.btnRefresh.Size = new System.Drawing.Size(92, 57);
      this.btnRefresh.TabIndex = 3;
      this.btnRefresh.TextColor = System.Drawing.Color.White;
      this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
      // 
      // labelTrFilterUsers
      // 
      this.labelTrFilterUsers.AutoSize = true;
      this.labelTrFilterUsers.Dock = System.Windows.Forms.DockStyle.Top;
      this.labelTrFilterUsers.Font = new System.Drawing.Font("Segoe UI", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTrFilterUsers.ForeColor = System.Drawing.Color.DarkGreen;
      this.labelTrFilterUsers.Location = new System.Drawing.Point(0, 0);
      this.labelTrFilterUsers.Name = "labelTrFilterUsers";
      this.labelTrFilterUsers.Size = new System.Drawing.Size(112, 25);
      this.labelTrFilterUsers.TabIndex = 0;
      this.labelTrFilterUsers.Text = "Filter Users";
      // 
      // panelTop
      // 
      this.panelTop.BackColor = System.Drawing.SystemColors.Control;
      this.panelTop.Controls.Add(this.lblTrUsers);
      this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.panelTop.Location = new System.Drawing.Point(2, 2);
      this.panelTop.Name = "panelTop";
      this.panelTop.Size = new System.Drawing.Size(1226, 42);
      this.panelTop.TabIndex = 16;
      // 
      // lblTrUsers
      // 
      this.lblTrUsers.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblTrUsers.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblTrUsers.ForeColor = System.Drawing.Color.DodgerBlue;
      this.lblTrUsers.Location = new System.Drawing.Point(0, 0);
      this.lblTrUsers.Name = "lblTrUsers";
      this.lblTrUsers.Size = new System.Drawing.Size(1226, 42);
      this.lblTrUsers.TabIndex = 0;
      this.lblTrUsers.Text = "Users";
      this.lblTrUsers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // ManageUsersUserControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.coloredBorderPanel);
      this.Name = "ManageUsersUserControl";
      this.Size = new System.Drawing.Size(1230, 600);
      this.coloredBorderPanel.ResumeLayout(false);
      this.panelMain.ResumeLayout(false);
      this.panelUsers.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgvUsers)).EndInit();
      this.panelEditUser.ResumeLayout(false);
      this.tableLayoutPanelDetailUser.ResumeLayout(false);
      this.pnlLblUsername.ResumeLayout(false);
      this.pnlUsername.ResumeLayout(false);
      this.pnlUsername.PerformLayout();
      this.pnlLblPassword.ResumeLayout(false);
      this.pnlPassword.ResumeLayout(false);
      this.pnlPassword.PerformLayout();
      this.panelLblBadge.ResumeLayout(false);
      this.panelBadge.ResumeLayout(false);
      this.panelBadge.PerformLayout();
      this.pnlLblFullName.ResumeLayout(false);
      this.pnlFullName.ResumeLayout(false);
      this.pnlFullName.PerformLayout();
      this.pnlLblCompany.ResumeLayout(false);
      this.pnlCompany.ResumeLayout(false);
      this.pnlCompany.PerformLayout();
      this.pnlLblProfile.ResumeLayout(false);
      this.pnlProfile.ResumeLayout(false);
      this.panelLblBlocked.ResumeLayout(false);
      this.panelBlocked.ResumeLayout(false);
      this.panelBlocked.PerformLayout();
      this.panelCancelSave.ResumeLayout(false);
      this.panelCancel.ResumeLayout(false);
      this.panelSave.ResumeLayout(false);
      this.panelButtons.ResumeLayout(false);
      this.pnlMessage.ResumeLayout(false);
      this.pnlMessage.PerformLayout();
      this.flowLayoutPanel1.ResumeLayout(false);
      this.panelHeaderFilters.ResumeLayout(false);
      this.panelHeaderFilters.PerformLayout();
      this.panelFiltersAndRefresh.ResumeLayout(false);
      this.panelFilters.ResumeLayout(false);
      this.flowLayoutPanelFilters.ResumeLayout(false);
      this.panelUsernameFilter.ResumeLayout(false);
      this.panelUsernameFilter.PerformLayout();
      this.panelBadgeFilter.ResumeLayout(false);
      this.panelBadgeFilter.PerformLayout();
      this.panelFullNameFilter.ResumeLayout(false);
      this.panelFullNameFilter.PerformLayout();
      this.panelCompanyFilter.ResumeLayout(false);
      this.panelCompanyFilter.PerformLayout();
      this.panelProfileFilter.ResumeLayout(false);
      this.panelProfileFilter.PerformLayout();
      this.panelBlockedFilter.ResumeLayout(false);
      this.panelBlockedFilter.PerformLayout();
      this.panelResetFilters.ResumeLayout(false);
      this.panelRefresh.ResumeLayout(false);
      this.coloredBorderPanelAutoRefresh.ResumeLayout(false);
      this.panelTop.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panelMain;
    private System.Windows.Forms.DataGridView dgvUsers;
    private System.Windows.Forms.Label lblTrUsers;
    private ColoredBorderPanel coloredBorderPanel;
    private ColoredBorderPanel panelButtons;
    private CtrlButton btnDeleteUser;
    private System.Windows.Forms.Panel panelTop;
    private ColoredBorderPanel panelEditUser;
    private System.Windows.Forms.Panel panelUsers;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanelDetailUser;
    private System.Windows.Forms.Label lblTrBlocked;
    private System.Windows.Forms.Panel pnlLblUsername;
    private System.Windows.Forms.Label lblTrUsername;
    private System.Windows.Forms.Panel pnlLblPassword;
    private System.Windows.Forms.Label lblTrPassword;
    private System.Windows.Forms.Panel pnlUsername;
    private System.Windows.Forms.TextBox txbUsername;
    private System.Windows.Forms.Panel pnlPassword;
    private System.Windows.Forms.TextBox txbPassword;
    private System.Windows.Forms.Panel pnlLblFullName;
    private System.Windows.Forms.Label lblTrFullName;
    private System.Windows.Forms.Panel pnlLblCompany;
    private System.Windows.Forms.Label lblTrCompany;
    private System.Windows.Forms.Panel pnlLblProfile;
    private System.Windows.Forms.Label lblTrProfile;
    private System.Windows.Forms.Panel pnlProfile;
    private System.Windows.Forms.ComboBox cmbProfile;
    private System.Windows.Forms.Panel pnlFullName;
    private System.Windows.Forms.TextBox txbFullName;
    private System.Windows.Forms.Panel pnlCompany;
    private System.Windows.Forms.TextBox txbCompany;
    private System.Windows.Forms.Panel panelCancel;
    private System.Windows.Forms.Panel panelSave;
    private System.Windows.Forms.Panel panelBlocked;
    private CtrlImageButton btnCancel;
    private CtrlImageButton btnSave;
    private CtrlButton btnCreateUser;
    private CtrlButton btnEditUser;
    private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    private System.Windows.Forms.Panel panelLblBlocked;
    private System.Windows.Forms.CheckBox checkBoxBlocked;
    private System.Windows.Forms.Panel panelCancelSave;
    private System.Windows.Forms.Panel panelInfo;
    private System.Windows.Forms.TextBox textBoxError;
    private CtrlCustomPanel pnlMessage;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnUser;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnUsername;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnBadge;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnFullName;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnCompany;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnProfile;
    private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewColumnBlocked;
    private System.Windows.Forms.Panel panelBadge;
    private System.Windows.Forms.TextBox txbBadge;
    private System.Windows.Forms.Panel panelLblBadge;
    private System.Windows.Forms.Label lblTrBadge;
    private System.Windows.Forms.Panel panelHeaderFilters;
    private System.Windows.Forms.Panel panelFiltersAndRefresh;
    private System.Windows.Forms.Panel panelFilters;
    private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelFilters;
    private System.Windows.Forms.Panel panelUsernameFilter;
    private System.Windows.Forms.TextBox textBoxUsernameFilter;
    private System.Windows.Forms.Label labelTrUsernameContains;
    private System.Windows.Forms.Panel panelBadgeFilter;
    private System.Windows.Forms.TextBox textBoxBadgeFilter;
    private System.Windows.Forms.Label labelTrBadgeContains;
    private System.Windows.Forms.Panel panelCompanyFilter;
    private System.Windows.Forms.TextBox textBoxCompanyFilter;
    private System.Windows.Forms.Label labelTrCompanyContains;
    private System.Windows.Forms.Panel panelProfileFilter;
    private System.Windows.Forms.TextBox textBoxProfileFilter;
    private System.Windows.Forms.Label labelTrProfileContains;
    private System.Windows.Forms.Panel panelBlockedFilter;
    private System.Windows.Forms.ComboBox comboBoxBlockedFilter;
    private System.Windows.Forms.Label labelTrBlockedContains;
    private System.Windows.Forms.Panel panelResetFilters;
    private CtrlButton btnResetFilters;
    private System.Windows.Forms.Panel panelRefresh;
    private ColoredBorderPanel coloredBorderPanelAutoRefresh;
    private CtrlButton btnRefresh;
    private System.Windows.Forms.Label labelTrFilterUsers;
    private System.Windows.Forms.Panel panelFullNameFilter;
    private System.Windows.Forms.TextBox textBoxFullNameFilter;
    private System.Windows.Forms.Label labelTrFullNameContains;
  }
}
