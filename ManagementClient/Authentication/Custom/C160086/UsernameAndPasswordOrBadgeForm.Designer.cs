﻿namespace Authentication.Custom.C160086
{
  partial class UsernameAndPasswordOrBadgeForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.mainPanelBottom = new System.Windows.Forms.Panel();
      this.buttonClose = new System.Windows.Forms.Button();
      this.buttonLogin = new System.Windows.Forms.Button();
      this.panelMainTop = new System.Windows.Forms.Panel();
      this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
      this.panelFillerTop = new System.Windows.Forms.Panel();
      this.mainPanelCenter = new Authentication.Controls.ColoredBorderPanel();
      this.panelCredentials = new System.Windows.Forms.Panel();
      this.controlCredentials = new System.Windows.Forms.GroupBox();
      this.textBoxPassword = new System.Windows.Forms.TextBox();
      this.labelPassword = new System.Windows.Forms.Label();
      this.labelUsername = new System.Windows.Forms.Label();
      this.textBoxUsername = new System.Windows.Forms.TextBox();
      this.controlError = new Authentication.Controls.ColoredBorderPanel();
      this.textBoxError = new System.Windows.Forms.RichTextBox();
      this.tlpErrorImage = new System.Windows.Forms.TableLayoutPanel();
      this.pictureBoxErrorImage = new System.Windows.Forms.PictureBox();
      this.panelLeft = new System.Windows.Forms.Panel();
      this.panelEntertain = new System.Windows.Forms.Panel();
      this.controlLoading = new System.Windows.Forms.Panel();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.labelLoading = new System.Windows.Forms.Label();
      this.labelPleaseWait = new System.Windows.Forms.Label();
      this.controlNeedLogin = new System.Windows.Forms.PictureBox();
      this.mainPanelBottom.SuspendLayout();
      this.panelMainTop.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
      this.mainPanelCenter.SuspendLayout();
      this.panelCredentials.SuspendLayout();
      this.controlCredentials.SuspendLayout();
      this.controlError.SuspendLayout();
      this.tlpErrorImage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxErrorImage)).BeginInit();
      this.panelLeft.SuspendLayout();
      this.panelEntertain.SuspendLayout();
      this.controlLoading.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.controlNeedLogin)).BeginInit();
      this.SuspendLayout();
      // 
      // mainPanelBottom
      // 
      this.mainPanelBottom.Controls.Add(this.buttonClose);
      this.mainPanelBottom.Controls.Add(this.buttonLogin);
      this.mainPanelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.mainPanelBottom.Location = new System.Drawing.Point(0, 421);
      this.mainPanelBottom.Name = "mainPanelBottom";
      this.mainPanelBottom.Size = new System.Drawing.Size(660, 81);
      this.mainPanelBottom.TabIndex = 2;
      // 
      // buttonClose
      // 
      this.buttonClose.BackColor = System.Drawing.Color.DarkOrange;
      this.buttonClose.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonClose.ForeColor = System.Drawing.Color.White;
      this.buttonClose.Location = new System.Drawing.Point(209, 18);
      this.buttonClose.Name = "buttonClose";
      this.buttonClose.Size = new System.Drawing.Size(150, 45);
      this.buttonClose.TabIndex = 2;
      this.buttonClose.Text = "Close";
      this.buttonClose.UseVisualStyleBackColor = false;
      this.buttonClose.Click += new System.EventHandler(this.buttonExit_Click);
      // 
      // buttonLogin
      // 
      this.buttonLogin.BackColor = System.Drawing.Color.SteelBlue;
      this.buttonLogin.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonLogin.ForeColor = System.Drawing.Color.White;
      this.buttonLogin.Location = new System.Drawing.Point(500, 18);
      this.buttonLogin.Name = "buttonLogin";
      this.buttonLogin.Size = new System.Drawing.Size(150, 45);
      this.buttonLogin.TabIndex = 1;
      this.buttonLogin.Text = "Login";
      this.buttonLogin.UseVisualStyleBackColor = false;
      this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
      // 
      // panelMainTop
      // 
      this.panelMainTop.BackColor = System.Drawing.Color.White;
      this.panelMainTop.Controls.Add(this.pictureBoxLogo);
      this.panelMainTop.Controls.Add(this.panelFillerTop);
      this.panelMainTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.panelMainTop.Location = new System.Drawing.Point(0, 0);
      this.panelMainTop.Name = "panelMainTop";
      this.panelMainTop.Size = new System.Drawing.Size(660, 85);
      this.panelMainTop.TabIndex = 5;
      // 
      // pictureBoxLogo
      // 
      this.pictureBoxLogo.BackColor = System.Drawing.Color.Transparent;
      this.pictureBoxLogo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pictureBoxLogo.Image = global::Authentication.Properties.Resources.CassioliLogo;
      this.pictureBoxLogo.InitialImage = null;
      this.pictureBoxLogo.Location = new System.Drawing.Point(0, 10);
      this.pictureBoxLogo.Name = "pictureBoxLogo";
      this.pictureBoxLogo.Size = new System.Drawing.Size(660, 75);
      this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBoxLogo.TabIndex = 3;
      this.pictureBoxLogo.TabStop = false;
      // 
      // panelFillerTop
      // 
      this.panelFillerTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.panelFillerTop.Location = new System.Drawing.Point(0, 0);
      this.panelFillerTop.Name = "panelFillerTop";
      this.panelFillerTop.Size = new System.Drawing.Size(660, 10);
      this.panelFillerTop.TabIndex = 0;
      // 
      // mainPanelCenter
      // 
      this.mainPanelCenter.BackColor = System.Drawing.Color.White;
      this.mainPanelCenter.BorderColor = System.Drawing.Color.LightBlue;
      this.mainPanelCenter.BorderWidth = 2;
      this.mainPanelCenter.Controls.Add(this.panelCredentials);
      this.mainPanelCenter.Controls.Add(this.panelLeft);
      this.mainPanelCenter.Dock = System.Windows.Forms.DockStyle.Fill;
      this.mainPanelCenter.Location = new System.Drawing.Point(0, 85);
      this.mainPanelCenter.Name = "mainPanelCenter";
      this.mainPanelCenter.Size = new System.Drawing.Size(660, 336);
      this.mainPanelCenter.TabIndex = 1;
      // 
      // panelCredentials
      // 
      this.panelCredentials.BackColor = System.Drawing.Color.Transparent;
      this.panelCredentials.Controls.Add(this.controlCredentials);
      this.panelCredentials.Controls.Add(this.controlError);
      this.panelCredentials.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelCredentials.Location = new System.Drawing.Point(197, 2);
      this.panelCredentials.Name = "panelCredentials";
      this.panelCredentials.Size = new System.Drawing.Size(461, 332);
      this.panelCredentials.TabIndex = 2;
      // 
      // controlCredentials
      // 
      this.controlCredentials.BackColor = System.Drawing.Color.Transparent;
      this.controlCredentials.Controls.Add(this.textBoxPassword);
      this.controlCredentials.Controls.Add(this.labelPassword);
      this.controlCredentials.Controls.Add(this.labelUsername);
      this.controlCredentials.Controls.Add(this.textBoxUsername);
      this.controlCredentials.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.controlCredentials.ForeColor = System.Drawing.SystemColors.ControlText;
      this.controlCredentials.Location = new System.Drawing.Point(6, 122);
      this.controlCredentials.Name = "controlCredentials";
      this.controlCredentials.Size = new System.Drawing.Size(447, 186);
      this.controlCredentials.TabIndex = 4;
      this.controlCredentials.TabStop = false;
      this.controlCredentials.Text = "Credentials";
      // 
      // textBoxPassword
      // 
      this.textBoxPassword.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxPassword.Location = new System.Drawing.Point(176, 115);
      this.textBoxPassword.Name = "textBoxPassword";
      this.textBoxPassword.PasswordChar = '*';
      this.textBoxPassword.Size = new System.Drawing.Size(265, 33);
      this.textBoxPassword.TabIndex = 3;
      this.textBoxPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxUsernameOrPassword_KeyPress);
      // 
      // labelPassword
      // 
      this.labelPassword.AutoSize = true;
      this.labelPassword.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelPassword.ForeColor = System.Drawing.SystemColors.ControlText;
      this.labelPassword.Location = new System.Drawing.Point(16, 118);
      this.labelPassword.Name = "labelPassword";
      this.labelPassword.Size = new System.Drawing.Size(100, 25);
      this.labelPassword.TabIndex = 2;
      this.labelPassword.Text = "Password";
      // 
      // labelUsername
      // 
      this.labelUsername.AutoSize = true;
      this.labelUsername.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelUsername.ForeColor = System.Drawing.SystemColors.ControlText;
      this.labelUsername.Location = new System.Drawing.Point(16, 57);
      this.labelUsername.Name = "labelUsername";
      this.labelUsername.Size = new System.Drawing.Size(105, 25);
      this.labelUsername.TabIndex = 1;
      this.labelUsername.Text = "Username";
      // 
      // textBoxUsername
      // 
      this.textBoxUsername.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxUsername.Location = new System.Drawing.Point(176, 54);
      this.textBoxUsername.Name = "textBoxUsername";
      this.textBoxUsername.Size = new System.Drawing.Size(265, 33);
      this.textBoxUsername.TabIndex = 0;
      this.textBoxUsername.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxUsernameOrPassword_KeyPress);
      // 
      // controlError
      // 
      this.controlError.BackColor = System.Drawing.Color.Transparent;
      this.controlError.BorderColor = System.Drawing.Color.Red;
      this.controlError.BorderWidth = 3;
      this.controlError.Controls.Add(this.textBoxError);
      this.controlError.Controls.Add(this.tlpErrorImage);
      this.controlError.Location = new System.Drawing.Point(6, 18);
      this.controlError.Name = "controlError";
      this.controlError.Size = new System.Drawing.Size(447, 87);
      this.controlError.TabIndex = 5;
      this.controlError.Visible = false;
      // 
      // textBoxError
      // 
      this.textBoxError.BackColor = System.Drawing.Color.MistyRose;
      this.textBoxError.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.textBoxError.Dock = System.Windows.Forms.DockStyle.Fill;
      this.textBoxError.Font = new System.Drawing.Font("Segoe UI Symbol", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxError.Location = new System.Drawing.Point(71, 3);
      this.textBoxError.Name = "textBoxError";
      this.textBoxError.ReadOnly = true;
      this.textBoxError.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
      this.textBoxError.Size = new System.Drawing.Size(373, 81);
      this.textBoxError.TabIndex = 0;
      this.textBoxError.Text = "• Errore 1\n• Errore 2\n• Errore 3";
      // 
      // tlpErrorImage
      // 
      this.tlpErrorImage.BackColor = System.Drawing.Color.MistyRose;
      this.tlpErrorImage.ColumnCount = 3;
      this.tlpErrorImage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tlpErrorImage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
      this.tlpErrorImage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tlpErrorImage.Controls.Add(this.pictureBoxErrorImage, 1, 1);
      this.tlpErrorImage.Dock = System.Windows.Forms.DockStyle.Left;
      this.tlpErrorImage.Location = new System.Drawing.Point(3, 3);
      this.tlpErrorImage.Name = "tlpErrorImage";
      this.tlpErrorImage.RowCount = 3;
      this.tlpErrorImage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tlpErrorImage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
      this.tlpErrorImage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tlpErrorImage.Size = new System.Drawing.Size(68, 81);
      this.tlpErrorImage.TabIndex = 1;
      // 
      // pictureBoxErrorImage
      // 
      this.pictureBoxErrorImage.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pictureBoxErrorImage.Image = global::Authentication.Properties.Resources.RedExclamationMark;
      this.pictureBoxErrorImage.Location = new System.Drawing.Point(6, 7);
      this.pictureBoxErrorImage.Name = "pictureBoxErrorImage";
      this.pictureBoxErrorImage.Size = new System.Drawing.Size(55, 66);
      this.pictureBoxErrorImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBoxErrorImage.TabIndex = 0;
      this.pictureBoxErrorImage.TabStop = false;
      // 
      // panelLeft
      // 
      this.panelLeft.BackColor = System.Drawing.Color.Transparent;
      this.panelLeft.Controls.Add(this.panelEntertain);
      this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
      this.panelLeft.Location = new System.Drawing.Point(2, 2);
      this.panelLeft.Name = "panelLeft";
      this.panelLeft.Size = new System.Drawing.Size(195, 332);
      this.panelLeft.TabIndex = 3;
      // 
      // panelEntertain
      // 
      this.panelEntertain.Controls.Add(this.controlLoading);
      this.panelEntertain.Controls.Add(this.controlNeedLogin);
      this.panelEntertain.Location = new System.Drawing.Point(12, 41);
      this.panelEntertain.Name = "panelEntertain";
      this.panelEntertain.Size = new System.Drawing.Size(169, 245);
      this.panelEntertain.TabIndex = 3;
      // 
      // controlLoading
      // 
      this.controlLoading.Controls.Add(this.tableLayoutPanel1);
      this.controlLoading.Dock = System.Windows.Forms.DockStyle.Fill;
      this.controlLoading.Location = new System.Drawing.Point(0, 0);
      this.controlLoading.Name = "controlLoading";
      this.controlLoading.Size = new System.Drawing.Size(169, 245);
      this.controlLoading.TabIndex = 2;
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 1;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 2);
      this.tableLayoutPanel1.Controls.Add(this.labelLoading, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.labelPleaseWait, 0, 3);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 5;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(169, 245);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // pictureBox1
      // 
      this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pictureBox1.Image = global::Authentication.Properties.Resources.Loading;
      this.pictureBox1.InitialImage = null;
      this.pictureBox1.Location = new System.Drawing.Point(3, 64);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(163, 116);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBox1.TabIndex = 0;
      this.pictureBox1.TabStop = false;
      // 
      // labelLoading
      // 
      this.labelLoading.Dock = System.Windows.Forms.DockStyle.Fill;
      this.labelLoading.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelLoading.Location = new System.Drawing.Point(3, 12);
      this.labelLoading.Name = "labelLoading";
      this.labelLoading.Size = new System.Drawing.Size(163, 49);
      this.labelLoading.TabIndex = 1;
      this.labelLoading.Text = "Loading...";
      this.labelLoading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // labelPleaseWait
      // 
      this.labelPleaseWait.Dock = System.Windows.Forms.DockStyle.Fill;
      this.labelPleaseWait.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelPleaseWait.Location = new System.Drawing.Point(3, 183);
      this.labelPleaseWait.Name = "labelPleaseWait";
      this.labelPleaseWait.Size = new System.Drawing.Size(163, 49);
      this.labelPleaseWait.TabIndex = 2;
      this.labelPleaseWait.Text = "Please Wait";
      this.labelPleaseWait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // controlNeedLogin
      // 
      this.controlNeedLogin.Dock = System.Windows.Forms.DockStyle.Fill;
      this.controlNeedLogin.Image = global::Authentication.Properties.Resources.PadlockOn;
      this.controlNeedLogin.Location = new System.Drawing.Point(0, 0);
      this.controlNeedLogin.Name = "controlNeedLogin";
      this.controlNeedLogin.Size = new System.Drawing.Size(169, 245);
      this.controlNeedLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.controlNeedLogin.TabIndex = 3;
      this.controlNeedLogin.TabStop = false;
      // 
      // UsernameAndPasswordForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.ClientSize = new System.Drawing.Size(660, 502);
      this.ControlBox = false;
      this.Controls.Add(this.mainPanelCenter);
      this.Controls.Add(this.mainPanelBottom);
      this.Controls.Add(this.panelMainTop);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "UsernameAndPasswordForm";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.Text = "Authentication Form";
      this.mainPanelBottom.ResumeLayout(false);
      this.panelMainTop.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
      this.mainPanelCenter.ResumeLayout(false);
      this.panelCredentials.ResumeLayout(false);
      this.controlCredentials.ResumeLayout(false);
      this.controlCredentials.PerformLayout();
      this.controlError.ResumeLayout(false);
      this.tlpErrorImage.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxErrorImage)).EndInit();
      this.panelLeft.ResumeLayout(false);
      this.panelEntertain.ResumeLayout(false);
      this.controlLoading.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.controlNeedLogin)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.Panel mainPanelBottom;
    private System.Windows.Forms.Label labelLoading;
    private System.Windows.Forms.Button buttonLogin;
    private System.Windows.Forms.Panel panelLeft;
    private System.Windows.Forms.Panel panelCredentials;
    private System.Windows.Forms.Label labelUsername;
    private System.Windows.Forms.TextBox textBoxUsername;
    private System.Windows.Forms.GroupBox controlCredentials;
    private System.Windows.Forms.Label labelPleaseWait;
    private System.Windows.Forms.Panel panelMainTop;
    private System.Windows.Forms.Button buttonClose;
    private System.Windows.Forms.PictureBox pictureBoxLogo;
    private System.Windows.Forms.Label labelPassword;
    private System.Windows.Forms.TextBox textBoxPassword;
    private System.Windows.Forms.Panel panelFillerTop;
    private Authentication.Controls.ColoredBorderPanel controlError;
    private System.Windows.Forms.RichTextBox textBoxError;
    private System.Windows.Forms.TableLayoutPanel tlpErrorImage;
    private System.Windows.Forms.PictureBox pictureBoxErrorImage;
    private System.Windows.Forms.Panel panelEntertain;
    private System.Windows.Forms.PictureBox controlNeedLogin;
    private System.Windows.Forms.Panel controlLoading;
    private Controls.ColoredBorderPanel mainPanelCenter;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
  }
}