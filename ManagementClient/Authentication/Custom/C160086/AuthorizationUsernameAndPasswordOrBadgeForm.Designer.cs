﻿namespace Authentication.Custom.C160086
{
  partial class AuthorizationUsernameAndPasswordOrBadgeForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthorizationUsernameAndPasswordOrBadgeForm));
      this.mainPanelBottom = new System.Windows.Forms.Panel();
      this.panelMain = new System.Windows.Forms.Panel();
      this.labelAuthorizeFunctionality = new System.Windows.Forms.Label();
      this.panelFunctionality = new Authentication.Controls.ColoredBorderPanel();
      this.panelCredentials = new System.Windows.Forms.Panel();
      this.controlCredentials = new System.Windows.Forms.GroupBox();
      this.textBoxPleaseInsert = new System.Windows.Forms.TextBox();
      this.pictureBoxRequiredPassword = new System.Windows.Forms.PictureBox();
      this.pictureBoxRequiredUsername = new System.Windows.Forms.PictureBox();
      this.textBoxPassword = new System.Windows.Forms.TextBox();
      this.labelPassword = new System.Windows.Forms.Label();
      this.labelUsername = new System.Windows.Forms.Label();
      this.textBoxUsername = new System.Windows.Forms.TextBox();
      this.panelFiller1 = new System.Windows.Forms.Panel();
      this.textBoxDescription = new System.Windows.Forms.RichTextBox();
      this.panelFiller2 = new System.Windows.Forms.Panel();
      this.panelWarning = new System.Windows.Forms.Panel();
      this.textBoxWarning = new System.Windows.Forms.RichTextBox();
      this.pictureBoxWarning = new System.Windows.Forms.PictureBox();
      this.buttonCancel = new Authentication.Controls.CtrlImageButton();
      this.buttonConfirm = new Authentication.Controls.CtrlImageButton();
      this.mainPanelBottom.SuspendLayout();
      this.panelMain.SuspendLayout();
      this.panelFunctionality.SuspendLayout();
      this.panelCredentials.SuspendLayout();
      this.controlCredentials.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRequiredPassword)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRequiredUsername)).BeginInit();
      this.panelWarning.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWarning)).BeginInit();
      this.SuspendLayout();
      // 
      // mainPanelBottom
      // 
      this.mainPanelBottom.Controls.Add(this.buttonCancel);
      this.mainPanelBottom.Controls.Add(this.buttonConfirm);
      this.mainPanelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.mainPanelBottom.Location = new System.Drawing.Point(0, 430);
      this.mainPanelBottom.Name = "mainPanelBottom";
      this.mainPanelBottom.Size = new System.Drawing.Size(634, 81);
      this.mainPanelBottom.TabIndex = 3;
      // 
      // panelMain
      // 
      this.panelMain.BackColor = System.Drawing.Color.Transparent;
      this.panelMain.Controls.Add(this.panelFunctionality);
      this.panelMain.Controls.Add(this.labelAuthorizeFunctionality);
      this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelMain.Location = new System.Drawing.Point(0, 0);
      this.panelMain.Name = "panelMain";
      this.panelMain.Size = new System.Drawing.Size(634, 430);
      this.panelMain.TabIndex = 4;
      // 
      // labelAuthorizeFunctionality
      // 
      this.labelAuthorizeFunctionality.BackColor = System.Drawing.Color.LimeGreen;
      this.labelAuthorizeFunctionality.Dock = System.Windows.Forms.DockStyle.Top;
      this.labelAuthorizeFunctionality.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelAuthorizeFunctionality.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.labelAuthorizeFunctionality.Location = new System.Drawing.Point(0, 0);
      this.labelAuthorizeFunctionality.Name = "labelAuthorizeFunctionality";
      this.labelAuthorizeFunctionality.Size = new System.Drawing.Size(634, 47);
      this.labelAuthorizeFunctionality.TabIndex = 6;
      this.labelAuthorizeFunctionality.Text = "Authorize Functionality";
      this.labelAuthorizeFunctionality.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // panelFunctionality
      // 
      this.panelFunctionality.BorderColor = System.Drawing.Color.NavajoWhite;
      this.panelFunctionality.BorderWidth = 3;
      this.panelFunctionality.Controls.Add(this.panelCredentials);
      this.panelFunctionality.Controls.Add(this.panelFiller1);
      this.panelFunctionality.Controls.Add(this.textBoxDescription);
      this.panelFunctionality.Controls.Add(this.panelFiller2);
      this.panelFunctionality.Controls.Add(this.panelWarning);
      this.panelFunctionality.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelFunctionality.Location = new System.Drawing.Point(0, 47);
      this.panelFunctionality.Name = "panelFunctionality";
      this.panelFunctionality.Size = new System.Drawing.Size(634, 383);
      this.panelFunctionality.TabIndex = 11;
      // 
      // panelCredentials
      // 
      this.panelCredentials.Controls.Add(this.controlCredentials);
      this.panelCredentials.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelCredentials.Location = new System.Drawing.Point(3, 153);
      this.panelCredentials.Name = "panelCredentials";
      this.panelCredentials.Size = new System.Drawing.Size(628, 227);
      this.panelCredentials.TabIndex = 10;
      // 
      // controlCredentials
      // 
      this.controlCredentials.BackColor = System.Drawing.Color.Transparent;
      this.controlCredentials.Controls.Add(this.textBoxPleaseInsert);
      this.controlCredentials.Controls.Add(this.pictureBoxRequiredPassword);
      this.controlCredentials.Controls.Add(this.pictureBoxRequiredUsername);
      this.controlCredentials.Controls.Add(this.textBoxPassword);
      this.controlCredentials.Controls.Add(this.labelPassword);
      this.controlCredentials.Controls.Add(this.labelUsername);
      this.controlCredentials.Controls.Add(this.textBoxUsername);
      this.controlCredentials.Dock = System.Windows.Forms.DockStyle.Fill;
      this.controlCredentials.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.controlCredentials.ForeColor = System.Drawing.SystemColors.ControlText;
      this.controlCredentials.Location = new System.Drawing.Point(0, 0);
      this.controlCredentials.Name = "controlCredentials";
      this.controlCredentials.Size = new System.Drawing.Size(628, 227);
      this.controlCredentials.TabIndex = 4;
      this.controlCredentials.TabStop = false;
      this.controlCredentials.Text = "Credentials";
      // 
      // textBoxPleaseInsert
      // 
      this.textBoxPleaseInsert.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.textBoxPleaseInsert.Dock = System.Windows.Forms.DockStyle.Top;
      this.textBoxPleaseInsert.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxPleaseInsert.Location = new System.Drawing.Point(3, 25);
      this.textBoxPleaseInsert.Multiline = true;
      this.textBoxPleaseInsert.Name = "textBoxPleaseInsert";
      this.textBoxPleaseInsert.ReadOnly = true;
      this.textBoxPleaseInsert.Size = new System.Drawing.Size(622, 67);
      this.textBoxPleaseInsert.TabIndex = 7;
      this.textBoxPleaseInsert.Text = "Please Insert Higher User Privileges Credentials To Whom This Action Is Allowed";
      // 
      // pictureBoxRequiredPassword
      // 
      this.pictureBoxRequiredPassword.Image = global::Authentication.Properties.Resources.ExclamationMark;
      this.pictureBoxRequiredPassword.Location = new System.Drawing.Point(545, 163);
      this.pictureBoxRequiredPassword.Name = "pictureBoxRequiredPassword";
      this.pictureBoxRequiredPassword.Size = new System.Drawing.Size(60, 43);
      this.pictureBoxRequiredPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBoxRequiredPassword.TabIndex = 6;
      this.pictureBoxRequiredPassword.TabStop = false;
      this.pictureBoxRequiredPassword.Visible = false;
      // 
      // pictureBoxRequiredUsername
      // 
      this.pictureBoxRequiredUsername.Image = global::Authentication.Properties.Resources.ExclamationMark;
      this.pictureBoxRequiredUsername.Location = new System.Drawing.Point(545, 98);
      this.pictureBoxRequiredUsername.Name = "pictureBoxRequiredUsername";
      this.pictureBoxRequiredUsername.Size = new System.Drawing.Size(60, 43);
      this.pictureBoxRequiredUsername.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBoxRequiredUsername.TabIndex = 5;
      this.pictureBoxRequiredUsername.TabStop = false;
      this.pictureBoxRequiredUsername.Visible = false;
      // 
      // textBoxPassword
      // 
      this.textBoxPassword.Font = new System.Drawing.Font("Segoe UI Emoji", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxPassword.Location = new System.Drawing.Point(249, 163);
      this.textBoxPassword.Name = "textBoxPassword";
      this.textBoxPassword.PasswordChar = '*';
      this.textBoxPassword.Size = new System.Drawing.Size(290, 43);
      this.textBoxPassword.TabIndex = 3;
      this.textBoxPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPassword_KeyPress);
      // 
      // labelPassword
      // 
      this.labelPassword.AutoSize = true;
      this.labelPassword.Font = new System.Drawing.Font("Segoe UI Emoji", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelPassword.ForeColor = System.Drawing.SystemColors.ControlText;
      this.labelPassword.Location = new System.Drawing.Point(43, 163);
      this.labelPassword.Name = "labelPassword";
      this.labelPassword.Size = new System.Drawing.Size(137, 37);
      this.labelPassword.TabIndex = 2;
      this.labelPassword.Text = "Password";
      // 
      // labelUsername
      // 
      this.labelUsername.AutoSize = true;
      this.labelUsername.Font = new System.Drawing.Font("Segoe UI Emoji", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelUsername.ForeColor = System.Drawing.SystemColors.ControlText;
      this.labelUsername.Location = new System.Drawing.Point(43, 101);
      this.labelUsername.Name = "labelUsername";
      this.labelUsername.Size = new System.Drawing.Size(144, 37);
      this.labelUsername.TabIndex = 1;
      this.labelUsername.Text = "Username";
      // 
      // textBoxUsername
      // 
      this.textBoxUsername.Font = new System.Drawing.Font("Segoe UI Emoji", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxUsername.Location = new System.Drawing.Point(249, 98);
      this.textBoxUsername.Name = "textBoxUsername";
      this.textBoxUsername.Size = new System.Drawing.Size(290, 43);
      this.textBoxUsername.TabIndex = 0;
      this.textBoxUsername.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxUsername_KeyPress);
      // 
      // panelFiller1
      // 
      this.panelFiller1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panelFiller1.Location = new System.Drawing.Point(3, 138);
      this.panelFiller1.Name = "panelFiller1";
      this.panelFiller1.Size = new System.Drawing.Size(628, 15);
      this.panelFiller1.TabIndex = 9;
      // 
      // textBoxDescription
      // 
      this.textBoxDescription.BackColor = System.Drawing.SystemColors.Control;
      this.textBoxDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.textBoxDescription.Dock = System.Windows.Forms.DockStyle.Top;
      this.textBoxDescription.Font = new System.Drawing.Font("Segoe UI Symbol", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxDescription.Location = new System.Drawing.Point(3, 93);
      this.textBoxDescription.Name = "textBoxDescription";
      this.textBoxDescription.ReadOnly = true;
      this.textBoxDescription.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
      this.textBoxDescription.Size = new System.Drawing.Size(628, 45);
      this.textBoxDescription.TabIndex = 8;
      this.textBoxDescription.Text = "Funcionality Description";
      // 
      // panelFiller2
      // 
      this.panelFiller2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panelFiller2.Location = new System.Drawing.Point(3, 78);
      this.panelFiller2.Name = "panelFiller2";
      this.panelFiller2.Size = new System.Drawing.Size(628, 15);
      this.panelFiller2.TabIndex = 10;
      // 
      // panelWarning
      // 
      this.panelWarning.Controls.Add(this.textBoxWarning);
      this.panelWarning.Controls.Add(this.pictureBoxWarning);
      this.panelWarning.Dock = System.Windows.Forms.DockStyle.Top;
      this.panelWarning.Location = new System.Drawing.Point(3, 3);
      this.panelWarning.Name = "panelWarning";
      this.panelWarning.Size = new System.Drawing.Size(628, 75);
      this.panelWarning.TabIndex = 11;
      // 
      // textBoxWarning
      // 
      this.textBoxWarning.BackColor = System.Drawing.SystemColors.Control;
      this.textBoxWarning.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.textBoxWarning.Dock = System.Windows.Forms.DockStyle.Fill;
      this.textBoxWarning.Font = new System.Drawing.Font("Segoe UI Symbol", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxWarning.ForeColor = System.Drawing.Color.Red;
      this.textBoxWarning.Location = new System.Drawing.Point(82, 0);
      this.textBoxWarning.Name = "textBoxWarning";
      this.textBoxWarning.ReadOnly = true;
      this.textBoxWarning.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
      this.textBoxWarning.Size = new System.Drawing.Size(546, 75);
      this.textBoxWarning.TabIndex = 7;
      this.textBoxWarning.Text = "This Functionality Requires Authorization From Higher Privileges User";
      // 
      // pictureBoxWarning
      // 
      this.pictureBoxWarning.Dock = System.Windows.Forms.DockStyle.Left;
      this.pictureBoxWarning.Image = global::Authentication.Properties.Resources.Warning;
      this.pictureBoxWarning.Location = new System.Drawing.Point(0, 0);
      this.pictureBoxWarning.Name = "pictureBoxWarning";
      this.pictureBoxWarning.Size = new System.Drawing.Size(82, 75);
      this.pictureBoxWarning.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBoxWarning.TabIndex = 0;
      this.pictureBoxWarning.TabStop = false;
      // 
      // buttonCancel
      // 
      this.buttonCancel.BackColor1 = System.Drawing.Color.IndianRed;
      this.buttonCancel.BackColor2 = System.Drawing.Color.Red;
      this.buttonCancel.ButtonImage = global::Authentication.Properties.Resources.Nok;
      this.buttonCancel.ButtonText = "Cancel";
      this.buttonCancel.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.buttonCancel.Location = new System.Drawing.Point(53, 10);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.PressColor1 = System.Drawing.Color.Red;
      this.buttonCancel.PressColor2 = System.Drawing.Color.Red;
      this.buttonCancel.Size = new System.Drawing.Size(210, 63);
      this.buttonCancel.TabIndex = 4;
      this.buttonCancel.TextColor = System.Drawing.Color.WhiteSmoke;
      this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
      // 
      // buttonConfirm
      // 
      this.buttonConfirm.BackColor1 = System.Drawing.Color.ForestGreen;
      this.buttonConfirm.BackColor2 = System.Drawing.Color.Green;
      this.buttonConfirm.ButtonImage = global::Authentication.Properties.Resources.Ok;
      this.buttonConfirm.ButtonText = "Confirm";
      this.buttonConfirm.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonConfirm.Location = new System.Drawing.Point(332, 10);
      this.buttonConfirm.Name = "buttonConfirm";
      this.buttonConfirm.PressColor1 = System.Drawing.Color.Green;
      this.buttonConfirm.PressColor2 = System.Drawing.Color.Green;
      this.buttonConfirm.Size = new System.Drawing.Size(210, 63);
      this.buttonConfirm.TabIndex = 3;
      this.buttonConfirm.TextColor = System.Drawing.Color.White;
      this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
      // 
      // AuthorizationUsernameAndPasswordForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(634, 511);
      this.Controls.Add(this.panelMain);
      this.Controls.Add(this.mainPanelBottom);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "AuthorizationUsernameAndPasswordForm";
      this.Text = "Authorization Form";
      this.mainPanelBottom.ResumeLayout(false);
      this.panelMain.ResumeLayout(false);
      this.panelFunctionality.ResumeLayout(false);
      this.panelCredentials.ResumeLayout(false);
      this.controlCredentials.ResumeLayout(false);
      this.controlCredentials.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRequiredPassword)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRequiredUsername)).EndInit();
      this.panelWarning.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWarning)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel mainPanelBottom;
    private System.Windows.Forms.Panel panelMain;
    private System.Windows.Forms.GroupBox controlCredentials;
    private System.Windows.Forms.TextBox textBoxPassword;
    private System.Windows.Forms.Label labelPassword;
    private System.Windows.Forms.Label labelUsername;
    private System.Windows.Forms.TextBox textBoxUsername;
    private System.Windows.Forms.Label labelAuthorizeFunctionality;
    private System.Windows.Forms.RichTextBox textBoxWarning;
    private System.Windows.Forms.RichTextBox textBoxDescription;
    private System.Windows.Forms.Panel panelCredentials;
    private System.Windows.Forms.Panel panelFiller1;
    private System.Windows.Forms.PictureBox pictureBoxWarning;
    private Controls.ColoredBorderPanel panelFunctionality;
    private System.Windows.Forms.Panel panelFiller2;
    private System.Windows.Forms.Panel panelWarning;
    private Controls.CtrlImageButton buttonConfirm;
    private Controls.CtrlImageButton buttonCancel;
    private System.Windows.Forms.PictureBox pictureBoxRequiredPassword;
    private System.Windows.Forms.PictureBox pictureBoxRequiredUsername;
    private System.Windows.Forms.TextBox textBoxPleaseInsert;
  }
}