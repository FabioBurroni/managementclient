﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Authentication.Extensions;
using Localization;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.Extensions;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlClient.Event;
using XmlCommunicationManager.XmlClient.Manager;

namespace Authentication.Custom.C160583
{
	internal partial class UpdatePasswordForm : Form
	{
		#region Fields

		private readonly AuthenticationManager _authenticationManager = AuthenticationManager.Instance;

		private AuthenticationMode _authenticationMode;

		private CommandManager _commandManager;
		private CommandManagerNotifier _cmNotifier;

		private IXmlClient _xmlClient;

		#endregion

		#region Constructor

		public UpdatePasswordForm()
		{
			InitializeComponent();

			InitGraphis();
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// Specifica l'eventuale preambolo da utilizzare per l'invio dei comandi
		/// </summary>
		public string XmlCommandPreamble { get; set; }

		#endregion

		#region Events

		#region Form

		private void UpdatePasswordForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			Dispose();
		}

		#endregion

		#region Buttons

		private void buttonConfirm_Click(object sender, EventArgs e)
		{
			SendLogin();
		}

		private void buttonClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		#endregion

		#region Textbox

		private void textBoxUsernameOrPassword_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar != (char)Keys.Enter)
				return;

			SendLogin();
		}

		#endregion

		#endregion

		#region Public Methods

		/// <summary>
		/// Mostra e fa partire il processo di autenticazione
		/// </summary>
		/// <param name="username">Username</param>
		public DialogResult ShowDialog(string username)
		{
			textBoxUsername.Text = username;

			SetConfiguration();

			SetGraphics(false, true);

			return base.ShowDialog();
		}

		#endregion

		#region Private Methods

		#region ReSet Configuration

		/// <summary>
		/// Setta la modalità di comunicazione
		/// </summary>
		private void SetConfiguration()
		{
			//recupera la modalità di comunicazione da AuthenticationManager perchè non c'è un modo per recuperarlo direttamente da chi invoca la schermata

			if (_authenticationManager.AuthenticationMode == AuthenticationMode.CommandManager)
			{
				SetCommandManager(_authenticationManager.CommandManager);
				return;
			}

			if (_authenticationManager.AuthenticationMode == AuthenticationMode.XmlClient)
			{
				SetXmlClient(_authenticationManager.XmlClient);
				return;
			}

			throw new Exception($"{nameof(SetConfiguration)}: configure AuthenticationMode properly");
		}

		/// <summary>
		/// Setta la modalità di autenticazine come CommandManager
		/// </summary>
		private void SetCommandManager(CommandManager commandManager)
		{
			if (commandManager == null)
				throw new ArgumentNullException(nameof(commandManager));

			//modalità già settata
			if (_authenticationMode == AuthenticationMode.CommandManager)
				return;

			if (_authenticationMode != AuthenticationMode.None)
				throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

			_authenticationMode = AuthenticationMode.CommandManager;

			_cmNotifier = new CommandManagerNotifier(this, GetHashCode().ToString());
			_cmNotifier.notifyCallBack += NotifyCallback;

			_commandManager = commandManager;
			_commandManager.addCommandManagerNotifier(_cmNotifier);
		}

		/// <summary>
		/// Setta la modalità di autenticazine come XmlClient
		/// </summary>
		private void SetXmlClient(IXmlClient xmlClient)
		{
			if (xmlClient == null)
				throw new ArgumentNullException(nameof(xmlClient));

			//modalità già settata
			if (_authenticationMode == AuthenticationMode.XmlClient)
				return;

			if (_authenticationMode != AuthenticationMode.None)
				throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

			_authenticationMode = AuthenticationMode.XmlClient;

			_xmlClient = xmlClient;

			_xmlClient.OnException += XmlClientOnException;
			_xmlClient.OnRetrieveReplyFromServer += XmlClientOnRetrieveReplyFromServer;
		}

		private void ResetCommandManagerOrXmlClient()
		{
			if (_authenticationMode == AuthenticationMode.CommandManager)
			{
				_cmNotifier.notifyCallBack -= NotifyCallback;
				_commandManager.removeCommandManagerNotifier(_cmNotifier);

				_cmNotifier = null;
				_commandManager = null;
			}
			else if (_authenticationMode == AuthenticationMode.XmlClient)
			{
				_xmlClient.OnException -= XmlClientOnException;
				_xmlClient.OnRetrieveReplyFromServer -= XmlClientOnRetrieveReplyFromServer;

				_xmlClient = null;
			}

			//resetto l'autenticazione
			_authenticationMode = AuthenticationMode.None;
		}

		#endregion

		#region Graphics

		/// <summary>
		/// Inizializza la parte grafica
		/// </summary>
		private void InitGraphis()
		{
			Translate(); //traduco le etichette
			CenterToScreen(); //metto il controllo al centro dello schermo
			SetGraphics(true, false); //setto la schermata di caricamento
		}


		/// <summary>
		/// Setta i controlli grafici in base ai parametri passati
		/// </summary>
		/// <param name="showLoading">Indica se mostrare la schermata di caricamento</param>
		/// <param name="enableCredentials">Indica se abilitare la schermata di credenziali</param>
		/// <param name="errorCode">Indica i codici degli errori a video da mostrare</param>
		/// <param name="additionalErrorList">Lista aggiuntiva degli errori (non sono tradotti)</param>
		private void SetGraphics(bool showLoading, bool enableCredentials, int errorCode, IList<string> additionalErrorList = null)
		{
			SetGraphics(showLoading, enableCredentials,
				new Dictionary<int, IList<string>>
				{
					{ errorCode, additionalErrorList }
				});
		}

		/// <summary>
		/// Setta i controlli grafici in base ai parametri passati
		/// </summary>
		/// <param name="showLoading">Indica se mostrare la schermata di caricamento</param>
		/// <param name="enableCredentials">Indica se abilitare la schermata di credenziali</param>
		/// <param name="errorCodeList">Indica i codici degli errori a video da mostrare</param>
		private void SetGraphics(bool showLoading, bool enableCredentials, IList<int> errorCodeList)
		{
			var dictionary = errorCodeList.ToDictionary<int, int, IList<string>>(errorCode => errorCode, errorCode => null);

			SetGraphics(showLoading, enableCredentials, dictionary);
		}

		/// <summary>
		/// Setta i controlli grafici in base ai parametri passati
		/// </summary>
		/// <param name="showLoading">Indica se mostrare la schermata di caricamento</param>
		/// <param name="enableCredentials">Indica se abilitare la schermata di credenziali</param>
		/// <param name="errorCodeList">Indica i codici degli errori a video da mostrare, il valore (se presente) indica gli errori aggiuntivi per quel particolare codice</param>
		private void SetGraphics(bool showLoading, bool enableCredentials, IDictionary<int, IList<string>> errorCodeList = null)
		{
			//controlLoading.Visible = showLoading;    //schermata con caricamento in atto
			//controlNeedLogin.Visible = !showLoading; //schermata con lucchetto

			controlCredentials.Enabled = enableCredentials;
			buttonUpdate.Enabled = enableCredentials;
			if (enableCredentials)
			{
				//setto il focus sul controllo della textbox
				ActiveControl = textBoxOldPassword;
			}

			#region Errors

			//se errore è diverso da nullo, faccio comparire la schermata, traduco l'errore e setto al scritta
			var showError = errorCodeList != null && errorCodeList.Count > 0/* && errors.All(e => e != 1)*/;
			if (showError)
			{
				//recupero per tutti i codici, la loro descrizione tradotta
				var errors = GetErrorCode(errorCodeList.Keys.ToArray());

				StringBuilder sb = new StringBuilder();

				foreach (var error in errors)
				{
					sb.Append($"• [{error.Key}] {error.Value.LocalizeAuthenticationString()}");

					#region Additional Erros

					//se gli errori aggiuntivi sono presenti, li scrivo
					IList<string> additionalErrors;
					if (errorCodeList.TryGetValue(error.Key, out additionalErrors))
					{
						if (additionalErrors != null && additionalErrors.Count > 0)
						{
							sb.Append(" (");

							for (int i = 0; i < additionalErrors.Count; i++)
							{
								sb.Append($"{additionalErrors[i]}");

								if (i + 1 < additionalErrors.Count)
									sb.Append(", ");
							}

							sb.Append(")");
						}
					}

					#endregion

					sb.Append($"{Environment.NewLine}");
				}

				textBoxError.Text = sb.ToString();
			}

			controlError.Visible = showError; //schermata errore

			#endregion
		}

		private IDictionary<int, string> GetErrorCode(params int[] errorCodes)
		{
			var error2Descr = new SortedList<int, string>(new Collections.TopDownComparer());

			if (errorCodes == null)
				return error2Descr;

			foreach (var errorCode in errorCodes.Distinct())
			{
				error2Descr.Add(errorCode, _authenticationManager.GetDescriptionForErrorCode(errorCode));
			}

			return error2Descr;
		}

		#endregion

		#region Translate

		private void Translate()
		{
			Text = Localize.LocalizeAuthenticationString("Update Password Form");
			//labelLoading.Text = Localize.LocalizeAuthenticationString("Loading") + "...";
			//labelPleaseWait.Text = Localize.LocalizeAuthenticationString("Please Wait");
			labelPasswordHasExpired.Text = Localize.LocalizeAuthenticationString("Password Has Expired And Must Be Changed");
			controlCredentials.Text = Localize.LocalizeAuthenticationString("Credentials");
			labelOldPassword.Text = Localize.LocalizeAuthenticationString("Old Password");
			labelNewPassword.Text = Localize.LocalizeAuthenticationString("New Password");
			labelConfirmPassword.Text = Localize.LocalizeAuthenticationString("Confirm Password");
			buttonClose.Text = Localize.LocalizeAuthenticationString("Close");
			buttonUpdate.Text = Localize.LocalizeAuthenticationString("Update");
		}

		#endregion

		#region SendLogin

		/// <summary>
		/// Manda il comando di aggiornamento della password, se username e password sono vuote, le recupera dall'interfaccia grafica
		/// altrimenti usa quelle gli vengono passate
		/// </summary>
		private void SendLogin()
		{
			string username, oldPassword, newPassword, confirmPassword;

			var errors = new List<int>();

			#region Validation

			username = textBoxUsername.Text;
			if (string.IsNullOrEmpty(username))
				errors.Add(LocalErrorCodeMapper.UsernameCannotBeBlank);

			oldPassword = textBoxOldPassword.Text;
			if (string.IsNullOrEmpty(oldPassword))
				errors.Add(LocalErrorCodeMapper.OldPasswordCannotBeBlank);

			newPassword = textBoxNewPassword.Text;
			if (string.IsNullOrEmpty(newPassword))
				errors.Add(LocalErrorCodeMapper.NewPasswordCannotBeBlank);

			confirmPassword = textBoxConfirmPassword.Text;
			if (string.IsNullOrEmpty(confirmPassword))
				errors.Add(LocalErrorCodeMapper.ConfirmPasswordCannotBeBlank);

			if (!string.IsNullOrEmpty(newPassword) && !string.IsNullOrEmpty(confirmPassword) && newPassword != confirmPassword)
				errors.Add(LocalErrorCodeMapper.NewPasswordDoesNotMatchTheConfirmPassword);

			#endregion

			//se ho almeno un errore, significa che qualcosa non va
			if (errors.Any())
			{
				SetGraphics(false, true, errors.ToArray());
				return;
			}

			SetGraphics(true, false, errors.ToArray());

			SendCommand_UpdateUserPassword(username, oldPassword.Base64Encode(), newPassword.Base64Encode());
		}

		#endregion

		#endregion

		#region XmlClient / CommandManager

		#region Richieste

		private const string CommandUpdateUserPassword = "UpdateUserPassword";

		private void SendCommand_UpdateUserPassword(string username, string oldPassword, string newPassword)
		{
			string cmd = $"{XmlCommandPreamble}{CommandUpdateUserPassword}".CreateXmlCommand(username, oldPassword, newPassword);
			SendCommand(cmd);
		}

		private void SendCommand(string command)
		{
			//controllo se ho settato la modalità
			if (_authenticationMode == AuthenticationMode.None)
			{
				//throw new Exception($"Set Authentication Mode before start, call method {nameof(Configure)} or  method {nameof(Configure)}");
				return;
			}

			if (_authenticationMode == AuthenticationMode.XmlClient)
			{
				var xmlCmd = _xmlClient.CreateXmlCommand(command, "cassioli");
				_xmlClient.ProcessXmlDoc(xmlCmd, "custom", this);
			}
			else if (_authenticationMode == AuthenticationMode.CommandManager)
			{
				_cmNotifier.sendCommand(command, "custom");
			}
		}

		#endregion

		#region Risposte

		#region CommandManager

		private void NotifyCallback(CommandManagerNotifier.NotifyObject notifyObject)
		{
			var xmlCmd = notifyObject.xmlCommand;
			var xmlCmdRes = notifyObject.iXmlStreamer as XmlCommandResponse;

			ManageResponse(xmlCmd, xmlCmdRes);
		}

		#endregion

		#region XmlClient

		private void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs msgEventArgs)
		{
			var xmlCmd = msgEventArgs.xmlCommand as XmlCommand;
			var xmlCmdRes = msgEventArgs.iXmlStreamer as XmlCommandResponse;

			ManageResponse(xmlCmd, xmlCmdRes);
		}

		private void XmlClientOnException(object sender, ExceptionEventArgs exEventArgs)
		{
			var xmlCmd = exEventArgs.xmlCommand as XmlCommand;
			XmlCommandResponse xmlCmdRes = null;

			ManageResponse(xmlCmd, xmlCmdRes);
		}

		#endregion

		#region ManageResponse

		private void ManageResponse(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
		{
			var commandMethodName = xmlCmd.name.TrimPreamble();

			#region CommandLogin

			if (commandMethodName.EqualsConsiderCase(CommandUpdateUserPassword))
			{
				InvokeControlAction(this, delegate { Response_UpdateUserPassword(xmlCmd, xmlCmdRes); });
			}

			#endregion
		}

		#endregion

		#endregion

		#region Gestione Risposte

		private void Response_UpdateUserPassword(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
		{
			var errors = new List<int>();

			if (xmlCmdRes != null)
			{
				var i = 1;

				//recupero la risposta
				var item = xmlCmdRes.Items.First();

				var okNok = item.getFieldVal(i++);
				var result = item.getFieldVal<int>(i++);

				if (okNok.EqualsIgnoreCase("ok"))
				{
					//risposta ok, password cambiata, chiudo
					DialogResult = DialogResult.OK;
					Close();
					return;
				}

				//errore nella risposta, salvo l'errore
				errors.Add(result);
			}
			else
			{
				//risposta non ricevuta
				errors.Add(LocalErrorCodeMapper.ServerNotReachable);
			}

			//se sono qui è perchè c'è stato un errore, esco
			SetGraphics(false, true, errors);
		}

		#endregion

		#endregion

		#region Utilities

		/// <summary>
		/// Contesto attuale del controllo
		/// </summary>
		private static readonly System.Threading.SynchronizationContext SyncContext = System.Threading.SynchronizationContext.Current;

		/// <summary>
		/// Permette di eseguire il metodo <param name="action"/> della classe <param name="cont"/>
		/// invocandolo tramite thread principale così da evitare le eccezioni
		/// </summary>
		/// <typeparam name="T">Tipo della classe invocante</typeparam>
		/// <param name="cont">Classe invocante</param>
		/// <param name="action">Metodo da invocare</param>
		protected static void InvokeControlAction<T>(T cont, Action<T> action)
			where T : Control
		{
			//if (cont.InvokeRequired)
			//{
			//  cont.Invoke(new Action<T, Action<T>>(InvokeControlAction), cont, action);
			//}
			//else
			//{
			//  action(cont);
			//}

			//NOTE la vecchia implementazione prevedeva il classico InvokeRequired ma questo è valido solo se chi istanzia il controllo è una WinForms.
			//NOTE Per rendere compatibile il controllo anche con WPF, devo utilizzare il contesto.
			//NOTE ATTENZIONE: non è necessario utilizzare ISynchronizeInvoke.InvokeRequired o Dispatcher.CheckAccess, il controllo è interno al contesto stesso.

			if (SyncContext == null)
			{
				action(cont);
			}
			else
			{
				SyncContext.Post(o => action(cont), null);
			}
		}

		private static bool IsWpfGuiThread()
		{
			//return System.Windows.Threading.Dispatcher.FromThread(System.Threading.Thread.CurrentThread) != null;
			return SyncContext is System.Windows.Threading.DispatcherSynchronizationContext;
		}

		private static bool IsWinFormsGuiThread()
		{
			return SyncContext is System.Windows.Forms.WindowsFormsSynchronizationContext;
		}

		#endregion

		#region Dispose

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				ResetCommandManagerOrXmlClient();
				components?.Dispose();
			}
			base.Dispose(disposing);
		}

		#endregion
	}
}
