﻿namespace Authentication.Custom.C160583
{
  /// <summary>
  /// Istanza per gli errori di autenticazione che arrivano SOLO dal server
  /// </summary>
  public class RemoteErrorCodeMapper : Default.RemoteErrorCodeMapper
  {
    public RemoteErrorCodeMapper()
    {
      ErrorCodeDictionary[580] = "Username Must Be At Least 8 Characters";	//lo username deve essere di almeno 8 caratteri
      ErrorCodeDictionary[581] = "Password Must Be At Least 6 Characters";  //la password deve essere di almeno 6 caratteri
			ErrorCodeDictionary[582] = "Error Inserting User";	//la password deve essere di almeno 6 caratteri
			ErrorCodeDictionary[583] = "Password Must Be At Least 6 Characters";	//la password deve essere di almeno 6 caratteri
			ErrorCodeDictionary[584] = "Password Already Used In Last 365 Days";	//la password deve essere di almeno 6 caratteri
			ErrorCodeDictionary[585] = "Error Updating User";	//la password deve essere di almeno 6 caratteri
			ErrorCodeDictionary[586] = "Error Updating User";	//la password deve essere di almeno 6 caratteri

			ErrorCodeDictionary[595] = "Password Has Expired And Must Be Changed"; //Password scaduta e deve essere cambiata
			ErrorCodeDictionary[596] = "Old And New Password Are Equal"; //Password scaduta e deve essere cambiata
		}
  }
}