﻿namespace Authentication.Custom.C160583
{
	partial class UpdatePasswordForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdatePasswordForm));
			this.borderPanelMain = new Authentication.Controls.ColoredBorderPanel();
			this.mainPanelBottom = new System.Windows.Forms.Panel();
			this.buttonClose = new System.Windows.Forms.Button();
			this.buttonUpdate = new System.Windows.Forms.Button();
			this.controlCredentials = new System.Windows.Forms.GroupBox();
			this.textBoxConfirmPassword = new System.Windows.Forms.TextBox();
			this.labelConfirmPassword = new System.Windows.Forms.Label();
			this.textBoxNewPassword = new System.Windows.Forms.TextBox();
			this.labelNewPassword = new System.Windows.Forms.Label();
			this.textBoxOldPassword = new System.Windows.Forms.TextBox();
			this.labelOldPassword = new System.Windows.Forms.Label();
			this.labelUsername = new System.Windows.Forms.Label();
			this.textBoxUsername = new System.Windows.Forms.TextBox();
			this.labelPasswordHasExpired = new System.Windows.Forms.Label();
			this.controlError = new Authentication.Controls.ColoredBorderPanel();
			this.textBoxError = new System.Windows.Forms.RichTextBox();
			this.tlpErrorImage = new System.Windows.Forms.TableLayoutPanel();
			this.pictureBoxErrorImage = new System.Windows.Forms.PictureBox();
			this.borderPanelMain.SuspendLayout();
			this.mainPanelBottom.SuspendLayout();
			this.controlCredentials.SuspendLayout();
			this.controlError.SuspendLayout();
			this.tlpErrorImage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxErrorImage)).BeginInit();
			this.SuspendLayout();
			// 
			// borderPanelMain
			// 
			this.borderPanelMain.BackColor = System.Drawing.Color.White;
			this.borderPanelMain.BorderColor = System.Drawing.Color.LightSkyBlue;
			this.borderPanelMain.BorderWidth = 1;
			this.borderPanelMain.Controls.Add(this.mainPanelBottom);
			this.borderPanelMain.Controls.Add(this.controlCredentials);
			this.borderPanelMain.Controls.Add(this.labelPasswordHasExpired);
			this.borderPanelMain.Controls.Add(this.controlError);
			this.borderPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.borderPanelMain.Location = new System.Drawing.Point(0, 0);
			this.borderPanelMain.Name = "borderPanelMain";
			this.borderPanelMain.Size = new System.Drawing.Size(534, 511);
			this.borderPanelMain.TabIndex = 7;
			// 
			// mainPanelBottom
			// 
			this.mainPanelBottom.BackColor = System.Drawing.SystemColors.Control;
			this.mainPanelBottom.Controls.Add(this.buttonClose);
			this.mainPanelBottom.Controls.Add(this.buttonUpdate);
			this.mainPanelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.mainPanelBottom.Location = new System.Drawing.Point(1, 447);
			this.mainPanelBottom.Name = "mainPanelBottom";
			this.mainPanelBottom.Size = new System.Drawing.Size(532, 63);
			this.mainPanelBottom.TabIndex = 9;
			// 
			// buttonClose
			// 
			this.buttonClose.BackColor = System.Drawing.Color.DarkOrange;
			this.buttonClose.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonClose.ForeColor = System.Drawing.Color.White;
			this.buttonClose.Location = new System.Drawing.Point(44, 9);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(150, 45);
			this.buttonClose.TabIndex = 2;
			this.buttonClose.Text = "Close";
			this.buttonClose.UseVisualStyleBackColor = false;
			this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
			// 
			// buttonUpdate
			// 
			this.buttonUpdate.BackColor = System.Drawing.Color.ForestGreen;
			this.buttonUpdate.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonUpdate.ForeColor = System.Drawing.Color.White;
			this.buttonUpdate.Location = new System.Drawing.Point(341, 9);
			this.buttonUpdate.Name = "buttonUpdate";
			this.buttonUpdate.Size = new System.Drawing.Size(150, 45);
			this.buttonUpdate.TabIndex = 1;
			this.buttonUpdate.Text = "Update";
			this.buttonUpdate.UseVisualStyleBackColor = false;
			this.buttonUpdate.Click += new System.EventHandler(this.buttonConfirm_Click);
			// 
			// controlCredentials
			// 
			this.controlCredentials.BackColor = System.Drawing.Color.Transparent;
			this.controlCredentials.Controls.Add(this.textBoxConfirmPassword);
			this.controlCredentials.Controls.Add(this.labelConfirmPassword);
			this.controlCredentials.Controls.Add(this.textBoxNewPassword);
			this.controlCredentials.Controls.Add(this.labelNewPassword);
			this.controlCredentials.Controls.Add(this.textBoxOldPassword);
			this.controlCredentials.Controls.Add(this.labelOldPassword);
			this.controlCredentials.Controls.Add(this.labelUsername);
			this.controlCredentials.Controls.Add(this.textBoxUsername);
			this.controlCredentials.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.controlCredentials.ForeColor = System.Drawing.SystemColors.ControlText;
			this.controlCredentials.Location = new System.Drawing.Point(15, 174);
			this.controlCredentials.Name = "controlCredentials";
			this.controlCredentials.Size = new System.Drawing.Size(507, 254);
			this.controlCredentials.TabIndex = 8;
			this.controlCredentials.TabStop = false;
			this.controlCredentials.Text = "Credentials";
			// 
			// textBoxConfirmPassword
			// 
			this.textBoxConfirmPassword.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxConfirmPassword.Location = new System.Drawing.Point(270, 205);
			this.textBoxConfirmPassword.Name = "textBoxConfirmPassword";
			this.textBoxConfirmPassword.PasswordChar = '*';
			this.textBoxConfirmPassword.Size = new System.Drawing.Size(231, 33);
			this.textBoxConfirmPassword.TabIndex = 7;
			this.textBoxConfirmPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxUsernameOrPassword_KeyPress);
			// 
			// labelConfirmPassword
			// 
			this.labelConfirmPassword.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelConfirmPassword.ForeColor = System.Drawing.SystemColors.ControlText;
			this.labelConfirmPassword.Location = new System.Drawing.Point(4, 206);
			this.labelConfirmPassword.Name = "labelConfirmPassword";
			this.labelConfirmPassword.Size = new System.Drawing.Size(260, 30);
			this.labelConfirmPassword.TabIndex = 6;
			this.labelConfirmPassword.Text = "Confirm Password";
			this.labelConfirmPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBoxNewPassword
			// 
			this.textBoxNewPassword.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxNewPassword.Location = new System.Drawing.Point(270, 149);
			this.textBoxNewPassword.Name = "textBoxNewPassword";
			this.textBoxNewPassword.PasswordChar = '*';
			this.textBoxNewPassword.Size = new System.Drawing.Size(231, 33);
			this.textBoxNewPassword.TabIndex = 5;
			this.textBoxNewPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxUsernameOrPassword_KeyPress);
			// 
			// labelNewPassword
			// 
			this.labelNewPassword.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelNewPassword.ForeColor = System.Drawing.SystemColors.ControlText;
			this.labelNewPassword.Location = new System.Drawing.Point(4, 152);
			this.labelNewPassword.Name = "labelNewPassword";
			this.labelNewPassword.Size = new System.Drawing.Size(260, 26);
			this.labelNewPassword.TabIndex = 4;
			this.labelNewPassword.Text = "New Password";
			this.labelNewPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBoxOldPassword
			// 
			this.textBoxOldPassword.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxOldPassword.Location = new System.Drawing.Point(270, 88);
			this.textBoxOldPassword.Name = "textBoxOldPassword";
			this.textBoxOldPassword.PasswordChar = '*';
			this.textBoxOldPassword.Size = new System.Drawing.Size(231, 33);
			this.textBoxOldPassword.TabIndex = 3;
			this.textBoxOldPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxUsernameOrPassword_KeyPress);
			// 
			// labelOldPassword
			// 
			this.labelOldPassword.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelOldPassword.ForeColor = System.Drawing.SystemColors.ControlText;
			this.labelOldPassword.Location = new System.Drawing.Point(6, 91);
			this.labelOldPassword.Name = "labelOldPassword";
			this.labelOldPassword.Size = new System.Drawing.Size(258, 26);
			this.labelOldPassword.TabIndex = 2;
			this.labelOldPassword.Text = "Old Password";
			this.labelOldPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// labelUsername
			// 
			this.labelUsername.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelUsername.ForeColor = System.Drawing.SystemColors.ControlText;
			this.labelUsername.Location = new System.Drawing.Point(6, 31);
			this.labelUsername.Name = "labelUsername";
			this.labelUsername.Size = new System.Drawing.Size(258, 26);
			this.labelUsername.TabIndex = 1;
			this.labelUsername.Text = "Username";
			this.labelUsername.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBoxUsername
			// 
			this.textBoxUsername.Enabled = false;
			this.textBoxUsername.Font = new System.Drawing.Font("Segoe UI Emoji", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxUsername.Location = new System.Drawing.Point(270, 28);
			this.textBoxUsername.Name = "textBoxUsername";
			this.textBoxUsername.Size = new System.Drawing.Size(231, 33);
			this.textBoxUsername.TabIndex = 0;
			// 
			// labelPasswordHasExpired
			// 
			this.labelPasswordHasExpired.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold);
			this.labelPasswordHasExpired.ForeColor = System.Drawing.Color.OrangeRed;
			this.labelPasswordHasExpired.Location = new System.Drawing.Point(10, 9);
			this.labelPasswordHasExpired.Name = "labelPasswordHasExpired";
			this.labelPasswordHasExpired.Size = new System.Drawing.Size(512, 56);
			this.labelPasswordHasExpired.TabIndex = 7;
			this.labelPasswordHasExpired.Text = "Password Has Expired And Must Be Changed";
			this.labelPasswordHasExpired.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// controlError
			// 
			this.controlError.BackColor = System.Drawing.Color.Transparent;
			this.controlError.BorderColor = System.Drawing.Color.Red;
			this.controlError.BorderWidth = 3;
			this.controlError.Controls.Add(this.textBoxError);
			this.controlError.Controls.Add(this.tlpErrorImage);
			this.controlError.Location = new System.Drawing.Point(45, 79);
			this.controlError.Name = "controlError";
			this.controlError.Size = new System.Drawing.Size(447, 87);
			this.controlError.TabIndex = 6;
			this.controlError.Visible = false;
			// 
			// textBoxError
			// 
			this.textBoxError.BackColor = System.Drawing.Color.MistyRose;
			this.textBoxError.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBoxError.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxError.Font = new System.Drawing.Font("Segoe UI Symbol", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxError.Location = new System.Drawing.Point(71, 3);
			this.textBoxError.Name = "textBoxError";
			this.textBoxError.ReadOnly = true;
			this.textBoxError.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
			this.textBoxError.Size = new System.Drawing.Size(373, 81);
			this.textBoxError.TabIndex = 0;
			this.textBoxError.Text = "• Errore 1\n• Errore 2\n• Errore 3";
			// 
			// tlpErrorImage
			// 
			this.tlpErrorImage.BackColor = System.Drawing.Color.MistyRose;
			this.tlpErrorImage.ColumnCount = 3;
			this.tlpErrorImage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tlpErrorImage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
			this.tlpErrorImage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tlpErrorImage.Controls.Add(this.pictureBoxErrorImage, 1, 1);
			this.tlpErrorImage.Dock = System.Windows.Forms.DockStyle.Left;
			this.tlpErrorImage.Location = new System.Drawing.Point(3, 3);
			this.tlpErrorImage.Name = "tlpErrorImage";
			this.tlpErrorImage.RowCount = 3;
			this.tlpErrorImage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tlpErrorImage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
			this.tlpErrorImage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tlpErrorImage.Size = new System.Drawing.Size(68, 81);
			this.tlpErrorImage.TabIndex = 1;
			// 
			// pictureBoxErrorImage
			// 
			this.pictureBoxErrorImage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBoxErrorImage.Image = global::Authentication.Properties.Resources.RedExclamationMark;
			this.pictureBoxErrorImage.Location = new System.Drawing.Point(6, 7);
			this.pictureBoxErrorImage.Name = "pictureBoxErrorImage";
			this.pictureBoxErrorImage.Size = new System.Drawing.Size(55, 66);
			this.pictureBoxErrorImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBoxErrorImage.TabIndex = 0;
			this.pictureBoxErrorImage.TabStop = false;
			// 
			// UpdatePasswordForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(534, 511);
			this.Controls.Add(this.borderPanelMain);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "UpdatePasswordForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Update Password Form";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UpdatePasswordForm_FormClosing);
			this.borderPanelMain.ResumeLayout(false);
			this.mainPanelBottom.ResumeLayout(false);
			this.controlCredentials.ResumeLayout(false);
			this.controlCredentials.PerformLayout();
			this.controlError.ResumeLayout(false);
			this.tlpErrorImage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxErrorImage)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private Controls.ColoredBorderPanel controlError;
		private System.Windows.Forms.RichTextBox textBoxError;
		private System.Windows.Forms.TableLayoutPanel tlpErrorImage;
		private System.Windows.Forms.PictureBox pictureBoxErrorImage;
		private Controls.ColoredBorderPanel borderPanelMain;
		private System.Windows.Forms.Label labelPasswordHasExpired;
		private System.Windows.Forms.GroupBox controlCredentials;
		private System.Windows.Forms.TextBox textBoxOldPassword;
		private System.Windows.Forms.Label labelOldPassword;
		private System.Windows.Forms.Label labelUsername;
		private System.Windows.Forms.TextBox textBoxUsername;
		private System.Windows.Forms.TextBox textBoxConfirmPassword;
		private System.Windows.Forms.Label labelConfirmPassword;
		private System.Windows.Forms.TextBox textBoxNewPassword;
		private System.Windows.Forms.Label labelNewPassword;
		private System.Windows.Forms.Panel mainPanelBottom;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.Button buttonUpdate;
	}
}