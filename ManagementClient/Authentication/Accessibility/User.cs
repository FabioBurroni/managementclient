﻿using System;
using System.Collections.Generic;
using System.Linq;
using Authentication.Extensions;
using Authentication.Profiling;

namespace Authentication.Accessibility
{
  public class User
  {
    #region Fields

    private readonly Dictionary<string, UserStructureBase> _userStructureCollection; //collezione di tutte le strutture, indicizzate per codice azione visto che codice profilo e client sono sempre uguali

    #endregion

    #region Public Properties

    /// <summary>
    /// Username dell'utente
    /// </summary>
    public string Username { get; }

    /// <summary>
    /// Codice del profilo utente
    /// </summary>
    public string UserProfileCode { get; private set; }

    /// <summary>
    /// Gerarchia dell'utente
    /// </summary>
    public int UserProfileHierarchy { get; private set; }

    public IList<UserStructureBase> UserStructures
    {
      get
      {
        lock (_userStructureCollection)
        {
          return _userStructureCollection.Values.ToList();
        }
      }
    }

    #endregion

    #region Constructor

    public User(string username, string userProfileCode, int userProfileHierarchy)
    {
      if (string.IsNullOrEmpty(username))
        throw new ArgumentNullException(nameof(username));

      if (string.IsNullOrEmpty(userProfileCode))
        throw new ArgumentNullException(nameof(userProfileCode));

      _userStructureCollection = new Dictionary<string, UserStructureBase>(StringComparer.OrdinalIgnoreCase);

      Username = username;
      UserProfileCode = userProfileCode;
      UserProfileHierarchy = userProfileHierarchy;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Indica se nel contesto attuale, l'azione è permessa
    /// </summary>
    public bool IsActionAllowed(string actionCode)
    {
      string optionalData;
      return IsActionAllowed(actionCode, out optionalData);
    }

    /// <summary>
    /// Indica se nel contesto attuale, l'azione è permessa
    /// </summary>
    public bool IsActionAllowed(string actionCode, out string optionalData)
    {
      optionalData = string.Empty;

      if (string.IsNullOrEmpty(actionCode))
        return false;

      lock (_userStructureCollection)
      {
        UserStructureBase userStructureBase;
        if (_userStructureCollection.TryGetValue(actionCode, out userStructureBase))
        {
          optionalData = userStructureBase.OptionalData;
          return userStructureBase.Enabled;
        }
      }

      return false;
    }

    #endregion

    #region Internal Methods

    /// <summary>
    /// Aggiorno le stutture utente. Se viene passato null, non ci sono modifiche.
    /// Le strutture vengono sempre rimpiazzate con quelle nuove passate
    /// </summary>
    internal void UpdateUserStructureCollection(IList<UserStructureBase> newUserStructures)
    {
      if (newUserStructures == null)
        return;

      lock (_userStructureCollection)
      {
        //azzero la collezione attuale
        _userStructureCollection.Clear();

        if (!newUserStructures.Any())
          return;

        foreach (var us in newUserStructures)
        {
          _userStructureCollection.Add(us.UserActionCode, us);
        }
      }
    }


    #endregion

    #region Override

    public override bool Equals(object obj)
    {
      // Is null?
      if (ReferenceEquals(null, obj))
      {
        return false;
      }

      // Is the same object?
      if (ReferenceEquals(this, obj))
      {
        return true;
      }

      // Is the same type?
      if (obj.GetType() != GetType())
      {
        return false;
      }

      return IsEqual((User)obj);
    }

    public override int GetHashCode()
    {
      unchecked
      {
        // Choose large primes to avoid hashing collisions
        const int hashingBase = (int)2166136261;
        const int hashingMultiplier = 16777619;

        int hash = hashingBase;
        hash = (hash * hashingMultiplier) ^ Username.GetHashCode();
        return hash;
      }
    }

    public override string ToString()
    {
      return Username;
    }

    #endregion

    #region Private Methods

    private bool IsEqual(User user)
    {
      // A pure implementation of value equality that avoids the routine checks above
      // We use Equals to really drive home our fear of an improperly overridden "=="
      return Username.EqualsIgnoreCase(user.Username);
    }

    #endregion
  }
}