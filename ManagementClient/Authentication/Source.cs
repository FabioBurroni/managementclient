﻿using System;
using System.Net;

namespace Authentication
{
  public class Source : IEquatable<Source>
  {
    public IPAddress IpAddress { get; }

    public string IpAddressString => IpAddress.ToString();
    //public string IpAddressString { get; }

    public int PortNumber { get; }

    public string UserClientCode { get; }

    public SourceType SourceType { get; private set; }

    public Source(string ipAddressString, int portNumber, string userClientCode, SourceType sourceType)
    {
      if (string.IsNullOrEmpty(ipAddressString))
        throw new ArgumentNullException(nameof(ipAddressString));

      IPAddress ipAddress;
      if (!IPAddress.TryParse(ipAddressString, out ipAddress))
        throw new ArgumentException($"{nameof(ipAddressString)} argument is not valid as IPAddress");

      if (portNumber < 0 || portNumber > 65535)
        throw new ArgumentOutOfRangeException(nameof(portNumber));

      if (string.IsNullOrEmpty(userClientCode))
        throw new ArgumentNullException(nameof(userClientCode));

      IpAddress = ipAddress;
      //IpAddressString = ipAddressString;
      PortNumber = portNumber;
      UserClientCode = userClientCode;
      SourceType = sourceType;
    }

    #region Public Methods

    public bool Equals(Source source)
    {
      // Is null?
      if (ReferenceEquals(null, source))
      {
        return false;
      }

      // Is the same object?
      if (ReferenceEquals(this, source))
      {
        return true;
      }

      return IsEqual(source);
    }

    #endregion

    #region Private Methods

    private bool IsEqual(Source source)
    {
      // A pure implementation of value equality that avoids the routine checks above
      // We use Equals to really drive home our fear of an improperly overridden "=="
      return IpAddress.Equals(source.IpAddress) && PortNumber.Equals(source.PortNumber);
    }

    #endregion

    #region Override

    public override bool Equals(object obj)
    {
      // Is null?
      if (ReferenceEquals(null, obj))
      {
        return false;
      }

      // Is the same object?
      if (ReferenceEquals(this, obj))
      {
        return true;
      }

      // Is the same type?
      if (obj.GetType() != GetType())
      {
        return false;
      }

      return IsEqual((Source)obj);
    }

    public override int GetHashCode()
    {
      unchecked
      {
        // Choose large primes to avoid hashing collisions
        const int hashingBase = (int)2166136261;
        const int hashingMultiplier = 16777619;

        int hash = hashingBase;
        hash = (hash * hashingMultiplier) ^ IpAddress.GetHashCode();
        hash = (hash * hashingMultiplier) ^ PortNumber.GetHashCode();
        return hash;
      }
    }

    #endregion
  }
}