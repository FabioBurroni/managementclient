﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Authentication.Default;
using Authentication.Extensions;
using XmlCommunicationManager.Authorization;

namespace Authentication.GUI.Authorization
{
  public partial class AuthorizationUsernameAndPasswordForm : Form
  {
    #region Fields

    private readonly IList<DialogResult> _validDialogResults = new[] { DialogResult.OK, DialogResult.Yes };

    private ICredentials _credentials;

    #endregion

    #region Constructor

    public AuthorizationUsernameAndPasswordForm()
    {
      InitializeComponent();

      Translate();

      ActiveControl = textBoxUsername;
    }

    #endregion

    #region Eventi

    private void textBoxPassword_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar != (char)Keys.Enter)
        return;

      ValidateData();
    }

    private void textBoxUsername_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar != (char)Keys.Enter)
        return;

      ValidateData();
    }

    private void buttonCancel_Click(object sender, EventArgs e)
    {
      SetDialogResultAndClose(DialogResult.Cancel);
    }

    private void buttonConfirm_Click(object sender, EventArgs e)
    {
      ValidateData();
    }

    #endregion

    #region Properties

    protected override CreateParams CreateParams
    {
      get
      {
        CreateParams cp = base.CreateParams;
        cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
        return cp;
      }
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Mostra la schermata di autorizzazione temporanea e mette nella textbox la descrizione (già tradotta) della funzionalità (stringa vuota se non necessaria).
    /// Restituisce true se l'utente vuole andare avanti.
    /// </summary>
    public bool ShowDialog(string description, out ICredentials credentials)
    {
      textBoxDescription.Text = description;
      var result = base.ShowDialog();

      //setto le credenziali (l'evento del bottone è scatenato prima di arrivare qui)
      credentials = _credentials;

      return _validDialogResults.Contains(result);
    }

    #endregion

    #region Private Methods

    private void ValidateData()
    {
      string username, password;

      if (!ValidateTextBoxes(out username, out password))
        return;

      SetDialogResultAndClose(DialogResult.OK, username, password);
    }

    private bool ValidateTextBoxes(out string username, out string password)
    {
      //validazione username
      username = textBoxUsername.Text;
      var isError = string.IsNullOrEmpty(username);
      ReSetError(textBoxUsername, pictureBoxRequiredUsername, isError);

      //validazione password
      password = textBoxPassword.Text;
      isError = string.IsNullOrEmpty(password);
      ReSetError(textBoxPassword, pictureBoxRequiredPassword, isError);

      return !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password);
    }

    private void ReSetError(TextBox textBox, PictureBox pictureBox, bool isError)
    {
      textBox.BackColor = isError ? Color.Salmon : Color.White;
      pictureBox.Visible = isError;
    }

    private void SetDialogResultAndClose(DialogResult result, string username = null, string password = null)
    {
      if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
        _credentials = new Credentials(username, password.Base64Encode());

      DialogResult = result;
      Close();
			Dispose();
    }

    private void Translate()
    {
      Text = Localization.Localize.LocalizeAuthenticationString("Authorization Form".ToUpper());
      labelAuthorizeFunctionality.Text = Localization.Localize.LocalizeAuthenticationString("Authorize Functionality".ToUpper());
      textBoxWarning.Text = Localization.Localize.LocalizeAuthenticationString("This Functionality Requires Authorization From Higher Privileges User".ToUpper());
      controlCredentials.Text = Localization.Localize.LocalizeAuthenticationString("Credentials".ToUpper());
      textBoxPleaseInsert.Text = Localization.Localize.LocalizeAuthenticationString("Please Insert Higher User Privileges Credentials To Whom This Action Is Allowed".ToUpper());
      labelUsername.Text = Localization.Localize.LocalizeAuthenticationString("Username".ToUpper());
      labelPassword.Text = Localization.Localize.LocalizeAuthenticationString("Password".ToUpper());
      buttonConfirm.Text = Localization.Localize.LocalizeAuthenticationString("Confirm".ToUpper());
      buttonCancel.Text = Localization.Localize.LocalizeAuthenticationString("Cancel".ToUpper());
    }

    #endregion
  }
}
