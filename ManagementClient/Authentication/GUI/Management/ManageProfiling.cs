﻿using Authentication.Collections;
using Authentication.Extensions;
using Authentication.Profiling;
using Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.Extensions;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlClient.Event;
using XmlCommunicationManager.XmlClient.Manager;

namespace Authentication.GUI.Management
{	
	public class ManageProfiling
  {
		#region eventi aggiornamento
		public delegate void OnListUpdatedHandler();
		public event OnListUpdatedHandler OnListUpdated;

		public delegate void OnNewMessageToShowHandler(string message, int type);
		public event OnNewMessageToShowHandler OnNewMessageToShow;
		#endregion

		#region Fields

		private readonly AuthenticationManager _authenticationManager = AuthenticationManager.Instance;

		private AuthenticationMode _authenticationMode;

		private string _xmlCommandPreamble;

		private string _clientFilter;

		private CommandManager _commandManager;
		private CommandManagerNotifier _cmNotifier;

		private IXmlClient _xmlClient;

		private readonly object _locker = new object();
	//	private readonly DataTable _dataTableStructures = new DataTable();

		private const string CommandGetAllPossibleUserStructure = "GetAllPossibleUserStructure";
		private const string CommandUpdateUserStructure = "UpdateUserStructure";

		/// <summary>  Contesto attuale del controllo </summary>
		private static readonly System.Threading.SynchronizationContext SyncContext = System.Threading.SynchronizationContext.Current;

		public List<UserStructure> ProfileOptionList { get; set; } = new List<UserStructure>();

		#endregion

		#region Constructor

		public ManageProfiling(IXmlClient xmlClient)
		{
			Configure(xmlClient);

			Reload();
		}
    
		#endregion

		#region Properties
		
		/// <summary>  Specifica l'eventuale preambolo da utilizzare per l'invio dei comandi </summary>
		private string XmlCommandPreamble
		{
			get
			{
				return _xmlCommandPreamble;
			}
			set
			{
				_xmlCommandPreamble = value ?? "";
			}
		}

		public int AuthenticationLevel { get; set; }
		#endregion

		#region public methods
		
		public void SaveChanges(List<UserStructure> updateProfileOptionList)
		{
			//controllo che ce ne sia almeno una cambiata
			var updatedUserStructures = GetUpdatedUserStructures(updateProfileOptionList);

			if (!updatedUserStructures.Any())
			{
				//non ho strutture aggiornate
				SendUIMessage(ShowError(LocalErrorCodeMapper.UpdateAtLeastOneConfiguration),2);
				//mando il comando per rinfrescare le strutture
				SendCommand_GetAllPossibleUserStructure();
			}
			else
			{
				SendCommand_UpdateUserStructure(updatedUserStructures);

				SendUIMessage("Please Wait".LocalizeAuthenticationString(),0);
			}
		}

		public void Reload()
		{
			//mando il comando per rinfrescare le strutture
			SendCommand_GetAllPossibleUserStructure();
		}

		public void Close()
		{
			ResetCommandManagerOrXmlClient();
		}
		#endregion

		#region Errors

		private string ShowError(int errorCode, UserStructureBase userStructure = null)
		{
			return ShowError(new List<Tuple<int, UserStructureBase>>
			{
				new Tuple<int, UserStructureBase>(errorCode, userStructure)
			});
		}

		private string ShowError(IList<int> errorCodeList)
		{
			var errorList = errorCodeList.Select(code => new Tuple<int, UserStructureBase>(code, null)).ToList();

			return ShowError(errorList);
		}

		private string ShowError(IList<Tuple<int, UserStructureBase>> errorList)
		{
			//se errore è diverso da nullo, faccio comparire la schermata, traduco l'errore e setto al scritta
			var showError = errorList != null && errorList.Count > 0/* && errors.All(e => e != 1)*/;
			if (showError)
			{
				var index = 0;
				var count = errorList.Count;

				StringBuilder sb = new StringBuilder();

				foreach (var error in errorList)
				{
					//recupero per tutti i codici, la loro descrizione non tradotta
					var errorCode2Descr = GetErrorCode(error.Item1);

					var errorCode = errorCode2Descr.Single().Key;
					var errorDescr = errorCode2Descr.Single().Value;

					sb.Append($"• [{errorCode}] {errorDescr.LocalizeAuthenticationString()}");

					#region Additional Erros

					//se gli errori aggiuntivi sono presenti, li scrivo
					UserStructureBase userStructureBase = error.Item2;

					if (userStructureBase != null)
					{
						sb.Append(" (");

						sb.Append(userStructureBase);

						sb.Append(")");
					}

					#endregion

					if (++index < count)
						sb.Append($"{Environment.NewLine}");
				}

				return sb.ToString();
			}

			return "Ok".LocalizeAuthenticationString(); //schermata errore
		}

		private IDictionary<int, string> GetErrorCode(params int[] errorCodes)
		{
			var error2Descr = new SortedList<int, string>();

			if (errorCodes == null)
				return error2Descr;

			foreach (var errorCode in errorCodes.Distinct())
			{
				error2Descr.Add(errorCode, _authenticationManager.GetDescriptionForErrorCode(errorCode));
			}

			return error2Descr;
		}

		#endregion
		
		#region private methods
		/// <summary>
		/// Restituisce tutte le strutture utente modificate
		/// </summary>
		private IList<UserStructure> GetUpdatedUserStructures(List<UserStructure> updateProfileOptionList)
		{
			var updatedUserStructures = new List<UserStructure>();

			if (ProfileOptionList is null || updateProfileOptionList is null)
				return updatedUserStructures;

			//lock (_locker)
			//{
			
					updatedUserStructures.AddRange(updateProfileOptionList.Where(x =>
																																			ProfileOptionList.Any(y => 
																																												(x.UserActionCode == y.UserActionCode) &&
																																												(x.UserClientCode == y.UserClientCode) &&
																																												(x.UserProfileCode == y.UserProfileCode) &&
																																												((x.Enabled != y.Enabled) || (x.OptionalData != y.OptionalData))
																																									)));
			//foreach(var upd in updateProfileOptionList)
   //   {
			//	ProfileOptionList.Where(x=> )


			//}

			//}

			return updatedUserStructures;
		}

		/// <summary>
		/// Setta la modalità di autenticazine come XmlClient
		/// È possibile mostrare la profilazione solo per il client indicato
		/// </summary>
		private void Configure(IXmlClient xmlClient, string clientFilter = null)
		{
			if (xmlClient == null)
				throw new ArgumentNullException(nameof(xmlClient));

			//modalità già settata
			if (_authenticationMode == AuthenticationMode.XmlClient)
				return;

			if (_authenticationMode != AuthenticationMode.None)
				throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

			_authenticationMode = AuthenticationMode.XmlClient;

			_xmlClient = xmlClient;

			_xmlClient.OnException += XmlClientOnException;
			_xmlClient.OnRetrieveReplyFromServer += XmlClientOnRetrieveReplyFromServer;

			_clientFilter = clientFilter;
		}

		#region XmlClient event handler

		private void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs msgEventArgs)
		{
			var xmlCmd = msgEventArgs.xmlCommand as XmlCommand;
			var xmlCmdRes = msgEventArgs.iXmlStreamer as XmlCommandResponse;

			ManageResponse(xmlCmd, xmlCmdRes);
		}

		private void XmlClientOnException(object sender, ExceptionEventArgs exEventArgs)
		{
			var xmlCmd = exEventArgs.xmlCommand as XmlCommand;
			XmlCommandResponse xmlCmdRes = null;

			ManageResponse(xmlCmd, xmlCmdRes);
		}

		private void ManageResponse(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
		{
			var commandMethodName = xmlCmd.name.TrimPreamble();

			#region GetAllPossibleUserStructure

			if (commandMethodName.EqualsConsiderCase(CommandGetAllPossibleUserStructure))
			{
				InvokeControlAction(this, delegate { Response_GetAllPossibleUserStructure(xmlCmd, xmlCmdRes); });
			}

			#endregion

			#region UpdateUserStructure

			else if (commandMethodName.EqualsConsiderCase(CommandUpdateUserStructure))
			{
				InvokeControlAction(this, delegate { Response_UpdateUserStructure(xmlCmd, xmlCmdRes); });
			}

			#endregion
		}

		/// <summary>
		/// Permette di eseguire il metodo <param name="action"/> della classe <param name="cont"/>
		/// invocandolo tramite thread principale così da evitare le eccezioni
		/// </summary>
		/// <typeparam name="T">Tipo della classe invocante</typeparam>
		/// <param name="cont">Classe invocante</param>
		/// <param name="action">Metodo da invocare</param>
		protected static void InvokeControlAction<T>(T cont, Action<T> action) //where T : control
		{
			if (SyncContext == null)
			{
				action(cont);
			}
			else
			{
				SyncContext.Post(o => action(cont), null);
			}
		}

		#endregion

		/// <summary>
		/// Invia comando per prelevare tutte le strutture a disposizione
		/// </summary>
		private void SendCommand_GetAllPossibleUserStructure()
		{
			var parameters = new List<string>();
			var session = _authenticationManager.Session;

			//se la sessione è nulla oppure sono cassioli, non filtro (quindi prendo tutto) altrimenti prendo il livello dell'utente
			if (session == null || session.User.Username.EqualsIgnoreCase("cassioli"))
			{
				//nessun filtro
			}
			else
			{
				const string hierarchy = "hierarchy";
				int hierarchyLevel = session.User.UserProfileHierarchy;
				AuthenticationLevel = hierarchyLevel;

				parameters.Add(hierarchy);
				parameters.Add(hierarchyLevel.ToString());
			}

			//se il filtro per client è impostato, lo setto nei parametri (tutti sono soggetti a filtro, anche cassioli)
			if (!string.IsNullOrEmpty(_clientFilter))
			{
				const string client = "client";

				parameters.Add(client);
				parameters.Add(_clientFilter);
			}

			var cmd = $"{XmlCommandPreamble}{CommandGetAllPossibleUserStructure}".CreateXmlCommand(parameters);

			SendCommand(cmd);
		}

		/// <summary>
		/// Invia comando per aggiornare le user structure modificate
		/// </summary>
		private void SendCommand_UpdateUserStructure(IList<UserStructure> updatedUserStructures)
		{
			var parameters = new List<string>();

			foreach (var updated in updatedUserStructures)
			{
				parameters.Add(updated.UserActionCode);
				parameters.Add(updated.UserClientCode);
				parameters.Add(updated.UserProfileCode);
				parameters.Add(updated.Enabled.ToString());
				parameters.Add(updated.OptionalData);
			}

			string cmd = $"{XmlCommandPreamble}{CommandUpdateUserStructure}".CreateXmlCommand(parameters);
			SendCommand(cmd);
		}
		
		private void SendCommand(string command)
		{
			//controllo se ho settato la modalità
			if (_authenticationMode == AuthenticationMode.None)
			{
				//throw new Exception($"Set Authentication Mode before start, call method {nameof(Configure)} or  method {nameof(Configure)}");
				return;
			}

			if (_authenticationMode == AuthenticationMode.XmlClient)
			{
				var xmlCmd = _xmlClient.CreateXmlCommand(command, "cassioli");
				_xmlClient.ProcessXmlDoc(xmlCmd, "custom", this);
			}
			else if (_authenticationMode == AuthenticationMode.CommandManager)
			{
				_cmNotifier.sendCommand(command, "custom");
			}
		}

		private void Response_GetAllPossibleUserStructure(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
		{
			if (xmlCmdRes != null)
			{
				var usList = Profiler.CreateUserStructureCollection(xmlCmdRes);

				ManageReceivedGetAllPossibleUserStructure(usList);
			}
			else
			{
				//risposta non ricevuta
				ManageNotReceivedGetAllPossibleUserStructure();
			}
		}

		private void Response_UpdateUserStructure(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
		{
			if (xmlCmdRes != null)
			{
				//recupero la risposta

				//lista di tutti i risultati
				var genericResps = GetGenericResponses(xmlCmdRes);

				//lista di tutte le nuove strutture
				var usList = Profiler.CreateUserStructureCollection(xmlCmdRes, 3);

				ManageReceivedUpdateUserStructureResponse(genericResps, usList);
			}
			else
			{
				//risposta non ricevuta
				ManageNotReceivedUpdateUserStructureResponse();
			}
		}

		private void ManageReceivedUpdateUserStructureResponse(IList<GenericResponse> genericResponses, IList<UserStructureBase> usList)
		{
			var errors = new List<Tuple<int, UserStructureBase>>();

			foreach (var i in Enumerable.Range(0, genericResponses.Count))
			{
				var response = genericResponses[i];

				//mostro solo le risposte con errore
				if (!response.IsOk)
				{
					if (usList.Count > 0)
					{
						UserStructureBase userStructure = usList[i];
						errors.Add(new Tuple<int, UserStructureBase>(response.Result, userStructure));
					}
					else
          {
						errors.Add(new Tuple<int, UserStructureBase>(response.Result, null));
					}
				}
			}

			ManageUpdateUserStructureResponse(errors);
		}

		private void ManageNotReceivedUpdateUserStructureResponse()
		{
			ManageUpdateUserStructureResponse(new List<Tuple<int, UserStructureBase>>
			{
				new Tuple<int, UserStructureBase>(LocalErrorCodeMapper.ServerNotReachable, null)
			});
		}
		private void ManageUpdateUserStructureResponse(IList<Tuple<int, UserStructureBase>> errors)
		{
      if (!errors.Any())
      { //non ho errori
				SendUIMessage("Update completed successfully!".LocalizeAuthenticationString(),1);
			}
			else
			{	//mostro a video gli errori
				SendUIMessage(ShowError(errors), 2);
			}

			// aggiorno le sessioni
      SendCommand_GetAllPossibleUserStructure();
    }

		private void ManageReceivedGetAllPossibleUserStructure(IList<UserStructure> userStructureList)
		{
			ProfileOptionList.Clear();
			ProfileOptionList.AddRange(userStructureList);

			UpdateUIList();
		}

		private void ManageNotReceivedGetAllPossibleUserStructure()
		{
			UpdateUIList();
		}


		private void ResetCommandManagerOrXmlClient()
		{
			if (_authenticationMode == AuthenticationMode.CommandManager)
			{
				_cmNotifier.notifyCallBack -= NotifyCallback;
				_commandManager.removeCommandManagerNotifier(_cmNotifier);

				_cmNotifier = null;
				_commandManager = null;
			}
			else if (_authenticationMode == AuthenticationMode.XmlClient)
			{
				_xmlClient.OnException -= XmlClientOnException;
				_xmlClient.OnRetrieveReplyFromServer -= XmlClientOnRetrieveReplyFromServer;

				_xmlClient = null;
			}

			//resetto l'autenticazione
			_authenticationMode = AuthenticationMode.None;
		}

		/// <summary>
		/// Update user interface event "OnListUpdated" call
		/// </summary>
		private void UpdateUIList()
    {
			OnListUpdated?.Invoke();
		}

		/// <summary>
		/// Update user interface event "OnNewMessageToShow" call
		/// </summary>
		private void SendUIMessage(string s, int type)
		{
			OnNewMessageToShow?.Invoke(s,type);
		}

		#region CommandManager

		private void NotifyCallback(CommandManagerNotifier.NotifyObject notifyObject)
		{
			var xmlCmd = notifyObject.xmlCommand;
			var xmlCmdRes = notifyObject.iXmlStreamer as XmlCommandResponse;

			ManageResponse(xmlCmd, xmlCmdRes);
		}

		#endregion
		
		#endregion

		#region GenericResponse

		private class GenericResponse
		{
			/// <summary>
			/// Riporta il valore di un operazione
			/// </summary>
			public string Code { get; }

			/// <summary>
			/// Riporta il dato dell'operazione
			/// </summary>
			public int Result { get; }

			/// <summary>
			/// Restituisce true se code = "ok"
			/// </summary>
			public bool IsOk => Code.EqualsIgnoreCase("ok");

			public GenericResponse(string code, int result)
			{
				if (string.IsNullOrEmpty(code))
					throw new ArgumentNullException(nameof(code));

				Code = code;
				Result = result;
			}
		}

		private GenericResponse GetGenericResponse(XmlCommandResponse.Item item)
		{
			if (item == null)
				throw new ArgumentNullException(nameof(item));

			var code = item.getFieldVal(1);
			var result = item.getFieldVal(2).ConvertTo<int>();

			return new GenericResponse(code, result);
		}

		private IList<GenericResponse> GetGenericResponses(XmlCommandResponse xmlResp)
		{
			if (xmlResp == null)
				throw new ArgumentNullException(nameof(xmlResp));

			var genericResponses = new List<GenericResponse>();

			foreach (var item in xmlResp.Items)
			{
				var genericResponse = GetGenericResponse(item);

				genericResponses.Add(genericResponse);
			}

			return genericResponses;
		}

		#endregion
	}
}
