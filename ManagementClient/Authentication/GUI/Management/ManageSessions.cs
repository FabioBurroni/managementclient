﻿using Authentication.Collections;
using Authentication.Extensions;
using Authentication.Profiling;
using Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.Extensions;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlClient.Event;
using XmlCommunicationManager.XmlClient.Manager;

namespace Authentication.GUI.Management
{
  public partial class ManageSessions
  {
    #region eventi aggiornamento
    public delegate void OnListUpdatedHandler();
    public event OnListUpdatedHandler OnListUpdated;

    public delegate void OnNewMessageToShowHandler(string message, int type);
    public event OnNewMessageToShowHandler OnNewMessageToShow;
    #endregion

    #region Fields

    private readonly AuthenticationManager _authenticationManager = AuthenticationManager.Instance;
    private AuthenticationMode _authenticationMode;

    private CommandManager _commandManager;
    private CommandManagerNotifier _cmNotifier;

    private string _xmlCommandPreamble;
    private IXmlClient _xmlClient;

    private readonly object _locker = new object();

    public List<ISession> SessionList { get; set; } = new List<ISession>();

    #endregion

    #region Constructor

    public ManageSessions(IXmlClient xmlClient)
    {
      Configure(xmlClient);

      Reload();

    }

    #endregion

    #region public methods
    

    public void Reload()
    {
      SendCommand_GetSessions();
    }

    public void Logout(IList<int> tokensToLogout)
    {
      //controllo che ce ne sia almeno una selezionata
      var tokens = GetSelectedSessions(tokensToLogout);

      if (!tokens.Any())
      {
        SendUIMessage(ShowError(LocalErrorCodeMapper.SelectAtLeastOneSession),2);
      }
      else
      {
        //chiedo conferma
        //if (!MessageBoxConfirmForceSessionsLogout())
        //  return;

        SendCommand_Logout(tokens);
      }
    }


    public void Close()
    {      
      ResetCommandManagerOrXmlClient();
    }
    #endregion

    #region Properties
        
    /// <summary>
    /// Specifica l'eventuale preambolo da utilizzare per l'invio dei comandi
    /// </summary>
    private string XmlCommandPreamble
    {
      get
      {
        return _xmlCommandPreamble;
      }
      set
      {
        _xmlCommandPreamble = value ?? "";
      }
    }

    public int AuthenticationLevel { get; set; }
    #endregion
       
    #region Private Methods

    /// <summary>
    /// Setta la modalità di autenticazine come CommandManager
    /// </summary>
    private void Configure(CommandManager commandManager)
    {
      if (commandManager == null)
        throw new ArgumentNullException(nameof(commandManager));

      //modalità già settata
      if (_authenticationMode == AuthenticationMode.CommandManager)
        return;

      if (_authenticationMode != AuthenticationMode.None)
        throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

      _authenticationMode = AuthenticationMode.CommandManager;

      _cmNotifier = new CommandManagerNotifier(this, GetHashCode().ToString());
      _cmNotifier.notifyCallBack += NotifyCallback;

      _commandManager = commandManager;
      _commandManager.addCommandManagerNotifier(_cmNotifier);
    }

    /// <summary>
    /// Setta la modalità di autenticazine come XmlClient
    /// </summary>
    private void Configure(IXmlClient xmlClient)
    {
      if (xmlClient == null)
        throw new ArgumentNullException(nameof(xmlClient));

      //modalità già settata
      if (_authenticationMode == AuthenticationMode.XmlClient)
        return;

      if (_authenticationMode != AuthenticationMode.None)
        throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

      _authenticationMode = AuthenticationMode.XmlClient;

      _xmlClient = xmlClient;

      _xmlClient.OnException += XmlClientOnException;
      _xmlClient.OnRetrieveReplyFromServer += XmlClientOnRetrieveReplyFromServer;
    }


    private void ResetCommandManagerOrXmlClient()
    {
      if (_authenticationMode == AuthenticationMode.CommandManager)
      {
        _cmNotifier.notifyCallBack -= NotifyCallback;
        _commandManager.removeCommandManagerNotifier(_cmNotifier);

        _cmNotifier = null;
        _commandManager = null;
      }
      else if (_authenticationMode == AuthenticationMode.XmlClient)
      {
        _xmlClient.OnException -= XmlClientOnException;
        _xmlClient.OnRetrieveReplyFromServer -= XmlClientOnRetrieveReplyFromServer;

        _xmlClient = null;
      }

      //resetto l'autenticazione
      _authenticationMode = AuthenticationMode.None;
    }

    private void CountdownRefreshOnTick(object sender, EventArgs eventArgs)
    {
        SendCommand_GetSessions();
    }

    /// <summary>
    /// Restituisce i token da buttare fuori
    /// </summary>
    private IList<int> GetSelectedSessions(IList<int> tokens)
    {
      if (SessionList is null)
        return null;

      IList<int> tokenToLogout = new List<int>();

      foreach (var s in SessionList)
      {
        foreach (var tok in tokens)
        {
          if (s.SessionTokens.ChildToken == tok)
          {
            tokenToLogout.Add(tok);
          }
        }
      }


      return tokenToLogout;
    }

    private static bool MessageBoxConfirmForceSessionsLogout()
    {
      var text = Localize.LocalizeAuthenticationString("Are You Sure To Force Logout For Selected Sessions?");
      var caption = Localize.LocalizeAuthenticationString("Confirm Choice");

      var result = MessageBox.Show(text, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

      return result == DialogResult.Yes;
    }

    private static bool MessageBoxUpdateDone()
    {
      var text = Localize.LocalizeAuthenticationString("Sessions removed successfully!");
      var caption = Localize.LocalizeAuthenticationString("Result");

      var result = MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

      return result == DialogResult.OK;
    }


    #endregion

    #region DataGridView

    private void ShowSessions(IList<ISession> sessions)
    {
      SessionList.Clear();

      lock (_locker)
      {
        //recupero la sessione
        var currentSession = _authenticationManager.Session;

        //indica se l'utente attuale non è cassioli
        var isNotCassioli = currentSession != null && !currentSession.User.Username.EqualsIgnoreCase("cassioli");

        //memorizzo tutti i nuovi ChildToken (identificatore univoco della sessione), mi servono dopo
        var childTokenList = new List<int>();

        //scorro tutte le sessioni
        foreach (var session in sessions)
        {
          #region Esclusioni
          //NOTE alcune informazioni le faccio vedere solo se mi loggo come cassioli, ovvero: tutti gli utenti Cassioli, tipi i System
          if (isNotCassioli)
          {
            if (session.User.Username.EqualsIgnoreCase("cassioli"))
              continue;

            if (session.Source.SourceType.Equals(SourceType.DynamicSystem))
              continue;
          }
          #endregion

          SessionList.Add(session);

          //memorizzo i token aggiunti
          childTokenList.Add(session.SessionTokens.ChildToken);          
        }

      
        FilterDataGridViewSessions();

        UpdateUIList();
      }
    }

    private void ManageLogoutResponse(IList<int> errors)
    {
      //mostro a video gli errori, riabilito il bottone ed aggiorno le sessioni
      ShowError(errors);

      SendCommand_GetSessions();

      //non ho errori
      if (!errors.Any())
      {
        MessageBoxUpdateDone();
      }
    }

    private void FilterDataGridViewSessions()
    {
      //var usernameFilter = textBoxUsernameFilter.Text;
      //var profileFilter = textBoxProfileFilter.Text;
      //var clientFilter = textBoxClientFilter.Text;
      //var sourceFilter = textBoxSourceFilter.Text;
      //var positionsFilter = textBoxPositionsFilter.Text;

      //StringBuilder filter = new StringBuilder();

      //filter.AppendLine("1=1");

      //if (!string.IsNullOrEmpty(usernameFilter))
      //  filter.AppendLine($"AND {dataGridViewColumnUsername.DataPropertyName} Like '%{usernameFilter}%'");

      //if (!string.IsNullOrEmpty(profileFilter))
      //  filter.AppendLine($"AND {dataGridViewColumnProfile.DataPropertyName} Like '%{profileFilter}%'");

      //if (!string.IsNullOrEmpty(clientFilter))
      //  filter.AppendLine($"AND {dataGridViewColumnClient.DataPropertyName} Like '%{clientFilter}%'");

      //if (!string.IsNullOrEmpty(sourceFilter))
      //  filter.AppendLine($"AND {dataGridViewColumnSource.DataPropertyName} Like '%{sourceFilter}%'");

      //if (!string.IsNullOrEmpty(positionsFilter))
      //  filter.AppendLine($"AND {dataGridViewColumnPositions.DataPropertyName} Like '%{positionsFilter}%'");

      //lock (_locker)
      //{
      //  DataView dv = dgvSessions.DataSource as DataView;

      //  if (dv == null)
      //    return;

      //  dv.RowFilter = filter.ToString();
      //  dgvSessions.ClearSelection();
      //}
    }

    #endregion

   

    #region Errors

    private string ShowError(int errorCode, UserStructureBase userStructure = null)
    {
      return ShowError(new List<Tuple<int, UserStructureBase>>
      {
        new Tuple<int, UserStructureBase>(errorCode, userStructure)
      });
    }

    private string ShowError(IList<int> errorCodeList)
    {
      var errorList = errorCodeList.Select(code => new Tuple<int, UserStructureBase>(code, null)).ToList();

      return ShowError(errorList);
    }

    private string ShowError(IList<Tuple<int, UserStructureBase>> errorList)
    {
      //se errore è diverso da nullo, faccio comparire la schermata, traduco l'errore e setto al scritta
      var showError = errorList != null && errorList.Count > 0/* && errors.All(e => e != 1)*/;
      if (showError)
      {
        var index = 0;
        var count = errorList.Count;

        StringBuilder sb = new StringBuilder();

        foreach (var error in errorList)
        {
          //recupero per tutti i codici, la loro descrizione non tradotta
          var errorCode2Descr = GetErrorCode(error.Item1);

          var errorCode = errorCode2Descr.Single().Key;
          var errorDescr = errorCode2Descr.Single().Value;

          sb.Append($"• [{errorCode}] {errorDescr.LocalizeAuthenticationString()}");

          #region Additional Erros

          //se gli errori aggiuntivi sono presenti, li scrivo
          UserStructureBase userStructureBase = error.Item2;

          if (userStructureBase != null)
          {
            sb.Append(" (");

            sb.Append(userStructureBase);

            sb.Append(")");
          }

          #endregion

          if (++index < count)
            sb.Append($"{Environment.NewLine}");
        }

        return sb.ToString();
      }

      return "Ok".LocalizeAuthenticationString(); //schermata errore
    }

    private IDictionary<int, string> GetErrorCode(params int[] errorCodes)
    {
      var error2Descr = new SortedList<int, string>();

      if (errorCodes == null)
        return error2Descr;

      foreach (var errorCode in errorCodes.Distinct())
      {
        error2Descr.Add(errorCode, _authenticationManager.GetDescriptionForErrorCode(errorCode));
      }

      return error2Descr;
    }

    #endregion

    #region Manage Response

    private void ManageReceivedGetSessionsResponse(IList<ISession> sessions)
    {
      ShowSessions(sessions);
    }

    private void ManageNotReceivedGetSessionsResponse()
    {
      ShowSessions(new List<ISession>());
    }

    private void ManageReceivedLogoutResponse(IList<IAuthenticationResponse> responseList)
    {
      var errors = new List<int>();

      foreach (var response in responseList)
      {
        //mostro solo le risposte con errore
        if (response.IsInError)
        {
          errors.Add(response.Result);
        }
      }

      ManageLogoutResponse(errors);
    }

    private void ManageNotReceivedLogoutResponse()
    {
      ManageLogoutResponse(new[] { LocalErrorCodeMapper.ServerNotReachable });
    }

    /// <summary>
		/// Update user interface event "OnListUpdated" call
		/// </summary>
		private void UpdateUIList()
    {
      OnListUpdated?.Invoke();
    }

    /// <summary>
    /// Update user interface event "OnNewMessageToShow" call
    /// </summary>
    private void SendUIMessage(string s, int type)
    {
      OnNewMessageToShow?.Invoke(s, type);
    }

    #endregion

    #region XmlClient / CommandManager

    #region Richieste

    private const string CommandGetSessions = "GetSessions";
    private const string CommandLogout = "Logout";

    private void SendCommand_GetSessions()
    {
      //parametri
      var parameters = new List<string>();

      var session = _authenticationManager.Session;
      if (session != null && !session.User.Username.EqualsIgnoreCase("cassioli"))
      {
        //se ho una sessione attiva, filtro quelle che posso vedere, al massimo posso vedere i miei pari profilo e chi è sotto di me in gerarchia
        parameters.Add("profile");
        parameters.Add(session.User.UserProfileCode);
        parameters.Add(UserProfileFilterRule.LowerOrEqualPrivilege.ToString());
      }

      string cmd = $"{XmlCommandPreamble}{CommandGetSessions}".CreateXmlCommand(parameters);
      SendCommand(cmd);
    }
    private void SendCommand_Logout(IList<int> tokens)
    {
      string cmd = $"{XmlCommandPreamble}{CommandLogout}".CreateXmlCommand(tokens.Cast<object>().ToArray());
      SendCommand(cmd);
    }

    private void SendCommand(string command)
    {
      //controllo se ho settato la modalità
      if (_authenticationMode == AuthenticationMode.None)
      {
        //throw new Exception($"Set Authentication Mode before start, call method {nameof(Configure)} or  method {nameof(Configure)}");
        return;
      }

      if (_authenticationMode == AuthenticationMode.XmlClient)
      {
        var xmlCmd = _xmlClient.CreateXmlCommand(command, "cassioli");
        _xmlClient.ProcessXmlDoc(xmlCmd, "custom", this);
      }
      else if (_authenticationMode == AuthenticationMode.CommandManager)
      {
        _cmNotifier.sendCommand(command, "custom");
      }
    }

    #endregion

    #region Risposte

    #region CommandManager

    private void NotifyCallback(CommandManagerNotifier.NotifyObject notifyObject)
    {
      var xmlCmd = notifyObject.xmlCommand;
      var xmlCmdRes = notifyObject.iXmlStreamer as XmlCommandResponse;

      ManageResponse(xmlCmd, xmlCmdRes);
    }

    #endregion

    #region XmlClient

    private void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs msgEventArgs)
    {
      var xmlCmd = msgEventArgs.xmlCommand as XmlCommand;
      var xmlCmdRes = msgEventArgs.iXmlStreamer as XmlCommandResponse;

      ManageResponse(xmlCmd, xmlCmdRes);
    }

    private void XmlClientOnException(object sender, ExceptionEventArgs exEventArgs)
    {
      var xmlCmd = exEventArgs.xmlCommand as XmlCommand;
      XmlCommandResponse xmlCmdRes = null;

      ManageResponse(xmlCmd, xmlCmdRes);
    }

    #endregion

    #region ManageResponse

    private void ManageResponse(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      var commandMethodName = xmlCmd.name.TrimPreamble();

      #region GetSessions

      if (commandMethodName.EqualsConsiderCase(CommandGetSessions))
      {
        InvokeControlAction(this, delegate { Response_GetSessions(xmlCmd, xmlCmdRes); });
      }

      #endregion

      #region CommandLogout

      else if (commandMethodName.EqualsConsiderCase(CommandLogout))
      {
        InvokeControlAction(this, delegate { Response_Logout(xmlCmd, xmlCmdRes); });
      }

      #endregion
    }

    #endregion

    #endregion

    #region Gestione Risposte

    private void Response_GetSessions(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      if (xmlCmdRes != null)
      {
        //recupero la risposta
        IList<ISession> sessions = new List<ISession>();

        foreach (var item in xmlCmdRes.Items)
        {
          //devo convertire il tutto in un formato particolare
          var items = new List<string>();

          using (var field = item.GetEnumerator())
          {
            while (field.MoveNext())
            {
              items.Add(field.Current);
            }
          }

          sessions.Add(_authenticationManager.CreateAndFillAuthenticationResponse(items).Session);
        }

        ManageReceivedGetSessionsResponse(sessions);
      }
      else
      {
        //risposta non ricevuta
        ManageNotReceivedGetSessionsResponse();
      }
    }

    private void Response_Logout(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      if (xmlCmdRes != null)
      {
        //recupero la risposta

        IList<IAuthenticationResponse> authenticationResponseList = new List<IAuthenticationResponse>();

        var items = xmlCmdRes.Items;

        //devo prendere i pacchetti a 3 a 3 perchè nel logout si risponde per multipli di 3 (ogni sessione ha 3 item)
        for (int i = 0; i < items.Count; i += 3)
        {
          var resp = new List<XmlCommandResponse.Item>
          {
            items[i],
            items[i + 1],
            items[i + 2]
          };

          authenticationResponseList.Add(_authenticationManager.CreateAndFillAuthenticationResponse(resp));
        }

        ManageReceivedLogoutResponse(authenticationResponseList);
      }
      else
      {
        //risposta non ricevuta
        ManageNotReceivedLogoutResponse();
      }
    }

    #endregion

    #endregion

    #region Utilities

    /// <summary>
    /// Contesto attuale del controllo
    /// </summary>
    private static readonly System.Threading.SynchronizationContext SyncContext = System.Threading.SynchronizationContext.Current;

    /// <summary>
    /// Permette di eseguire il metodo <param name="action"/> della classe <param name="cont"/>
    /// invocandolo tramite thread principale così da evitare le eccezioni
    /// </summary>
    /// <typeparam name="T">Tipo della classe invocante</typeparam>
    /// <param name="cont">Classe invocante</param>
    /// <param name="action">Metodo da invocare</param>
    protected static void InvokeControlAction<T>(T cont, Action<T> action) //where T : control
    {
      if (SyncContext == null)
      {
        action(cont);
      }
      else
      {
        SyncContext.Post(o => action(cont), null);
      }
    }


    private static bool IsWpfGuiThread()
    {
      //return System.Windows.Threading.Dispatcher.FromThread(System.Threading.Thread.CurrentThread) != null;
      return SyncContext is System.Windows.Threading.DispatcherSynchronizationContext;
    }

    private static bool IsWinFormsGuiThread()
    {
      return SyncContext is System.Windows.Forms.WindowsFormsSynchronizationContext;
    }

    #endregion

    #region Support Class

    private enum UserProfileFilterRule
    {
      LowerPrivilege,
      LowerOrEqualPrivilege,
      HigherPrivilege,
      HigherOrEqualPrivilege
    }

    #endregion
  }
}
