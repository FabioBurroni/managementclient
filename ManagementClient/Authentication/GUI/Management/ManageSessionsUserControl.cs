﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Authentication.Collections;
using Authentication.Extensions;
using Localization;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.Extensions;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlClient.Event;
using XmlCommunicationManager.XmlClient.Manager;

namespace Authentication.GUI.Management
{
  public partial class ManageSessionsUserControl : UserControl
  {
    #region Fields

    private readonly AuthenticationManager _authenticationManager = AuthenticationManager.Instance;

    private AuthenticationMode _authenticationMode;

    private string _xmlCommandPreamble;

    private CommandManager _commandManager;
    private CommandManagerNotifier _cmNotifier;

    private IXmlClient _xmlClient;

    private readonly object _locker = new object();
    private readonly DataTable _dataTableSessions = new DataTable();

    #endregion

    #region Constructor

    public ManageSessionsUserControl()
    {
      InitializeComponent();
    }

    #endregion

    #region Events

    private void btnRefresh_Click(object sender, EventArgs e)
    {
      SetCountdown(true);
      SendCommand_GetSessions();
    }

    private void btnResetFilters_Click(object sender, EventArgs e)
    {
      textBoxUsernameFilter.Text = string.Empty;
      textBoxProfileFilter.Text = string.Empty;
      textBoxClientFilter.Text = string.Empty;
      textBoxSourceFilter.Text = string.Empty;
      textBoxPositionsFilter.Text = string.Empty;
    }

    private void btnLogoutSessions_Click(object sender, EventArgs e)
    {
      //controllo che ce ne sia almeno una selezionata
      var tokens = GetSelectedSessions();

      if (!tokens.Any())
      {
        ShowError(LocalErrorCodeMapper.SelectAtLeastOneSession);
      }
      else
      {
        pnlMessage.Visible = false;

        //chiedo conferma
        if (!MessageBoxConfirmForceSessionsLogout())
          return;

        SendCommand_Logout(tokens);
        btnLogoutSessions.Enabled = false;
      }
    }

    private void textBoxFilter_TextChanged(object sender, EventArgs e)
    {
      FilterDataGridViewSessions();
    }

    private void dgvSessions_CurrentCellDirtyStateChanged(object sender, EventArgs e)
    {
      //permette di forzare il commit subito dopo il click dentro la cella
      if (dgvSessions.IsCurrentCellDirty)
      {
        dgvSessions.CommitEdit(DataGridViewDataErrorContexts.Commit);
      }
    }

    private void dgvSessions_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar == (char)Keys.Space)
      {
        //se è stata premuta la barra spaziatrice, inverto i valore delle checkbox delle righe selezionate 

        lock (_locker)
        {
          //memorizzo in un dizionario, il riferimento alla checkbox e il suo valore attuale
          var selectedCheckBoxes = new List<DataGridViewCheckBoxCell>();

          bool atLeastOneTrue = false;  //ne ho trovato almeno uno con il valore true
          bool atLeastOneFalse = false; //ne ho trovato almeno uno con il valore false

          foreach (DataGridViewRow selectedRow in dgvSessions.SelectedRows)
          {
            DataGridViewCheckBoxCell checkBoxCell = (DataGridViewCheckBoxCell)selectedRow.Cells[dataGridViewColumnSelect.Name];

            //recupero il valore attuale (se è nullo, il valore sarà false)
            var actualValue = checkBoxCell.Value.ConvertTo<bool>(true);

            if (selectedCheckBoxes.Contains(checkBoxCell))
              continue;

            selectedCheckBoxes.Add(checkBoxCell);

            if (actualValue)
              atLeastOneTrue = true;
            else
              atLeastOneFalse = true;
          }

          //scorro tutte le chechbox
          foreach (var checkBox in selectedCheckBoxes)
          {
            if (atLeastOneTrue && !atLeastOneFalse)
            {
              // c'è almeno una checkbox selezionata e nessuna deselezionata, deseleziono tutto
              checkBox.Value = false;
            }
            else if (atLeastOneTrue && atLeastOneFalse)
            {
              //c'è almeno una checkbox selezionata e almeno una deselezionata, seleziono tutto
              checkBox.Value = true;
            }
            else if (!atLeastOneTrue && atLeastOneFalse)
            {
              //nessuna chechbox selezionata e almeno una deselezionata, seleziono tutto
              checkBox.Value = true;
            }
          }
        }
      }
    }

    #endregion

    #region Properties

    protected override CreateParams CreateParams
    {
      get
      {
        CreateParams cp = base.CreateParams;
        cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
        return cp;
      }
    }

    /// <summary>
    /// Specifica l'eventuale preambolo da utilizzare per l'invio dei comandi
    /// </summary>
    public string XmlCommandPreamble
    {
      get
      {
        return _xmlCommandPreamble;
      }
      set
      {
        _xmlCommandPreamble = value ?? "";
      }
    }

    #endregion

    #region Public Methods

    public void InitCtrl()
    {
      StartCountdown();

      //mando subito il comando per rinfrescare la sessione
      SendCommand_GetSessions();
    }

    public void CloseCtrl()
    {
      StopCountdown();
      ResetCommandManagerOrXmlClient();
    }

    /// <summary>
    /// Setta la modalità di autenticazine come CommandManager
    /// </summary>
    public void Configure(CommandManager commandManager)
    {
      if (commandManager == null)
        throw new ArgumentNullException(nameof(commandManager));

      //modalità già settata
      if (_authenticationMode == AuthenticationMode.CommandManager)
        return;

      if (_authenticationMode != AuthenticationMode.None)
        throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

      _authenticationMode = AuthenticationMode.CommandManager;

      _cmNotifier = new CommandManagerNotifier(this, GetHashCode().ToString());
      _cmNotifier.notifyCallBack += NotifyCallback;

      _commandManager = commandManager;
      _commandManager.addCommandManagerNotifier(_cmNotifier);
    }

    /// <summary>
    /// Setta la modalità di autenticazine come XmlClient
    /// </summary>
    public void Configure(IXmlClient xmlClient)
    {
      if (xmlClient == null)
        throw new ArgumentNullException(nameof(xmlClient));

      //modalità già settata
      if (_authenticationMode == AuthenticationMode.XmlClient)
        return;

      if (_authenticationMode != AuthenticationMode.None)
        throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

      _authenticationMode = AuthenticationMode.XmlClient;

      _xmlClient = xmlClient;

      _xmlClient.OnException += XmlClientOnException;
      _xmlClient.OnRetrieveReplyFromServer += XmlClientOnRetrieveReplyFromServer;
    }

    public void Translate()
    {
      lblTrSessions.Text = Localize.LocalizeAuthenticationString("Sessions");
      labelTrFilterUsers.Text = Localize.LocalizeAuthenticationString("Filter Sessions");
      labelTrUsernameContains.Text = Localize.LocalizeAuthenticationString("Username Contains");
      labelTrProfileContains.Text = Localize.LocalizeAuthenticationString("Profile Contains");
      labelTrClientContains.Text = Localize.LocalizeAuthenticationString("Client Contains");
      labelTrSourceContains.Text = Localize.LocalizeAuthenticationString("Source Contains");
      labelPositionsContains.Text = Localize.LocalizeAuthenticationString("Positions Contains");
      btnResetFilters.ButtonText = Localize.LocalizeAuthenticationString("Reset Filters");
      labelTrAutoRefresh.Text = Localize.LocalizeAuthenticationString("Auto Refresh");
      btnRefresh.ButtonText = Localize.LocalizeAuthenticationString("Refresh");
      btnResetFilters.ButtonText = Localize.LocalizeAuthenticationString("Reset Filters");
      btnLogoutSessions.ButtonText = Localize.LocalizeAuthenticationString("Logout Sessions");
      TranslateDataGridView();
    }

    private void TranslateDataGridView()
    {
      lock (_locker)
      {
        if (dgvSessions.Columns.Count > 0)
        {
          dgvSessions.Columns[dataGridViewColumnUsername.Name].HeaderText = Localize.LocalizeAuthenticationString("Username".ToUpper());
          dgvSessions.Columns[dataGridViewColumnProfile.Name].HeaderText = Localize.LocalizeAuthenticationString("Profile".ToUpper());
          dgvSessions.Columns[dataGridViewColumnClient.Name].HeaderText = Localize.LocalizeAuthenticationString("Client".ToUpper());
          dgvSessions.Columns[dataGridViewColumnSource.Name].HeaderText = Localize.LocalizeAuthenticationString("Source".ToUpper());
          dgvSessions.Columns[dataGridViewColumnIPAddress.Name].HeaderText = Localize.LocalizeAuthenticationString("IP Address".ToUpper());
          dgvSessions.Columns[dataGridViewColumnPortNumber.Name].HeaderText = Localize.LocalizeAuthenticationString("Port Number".ToUpper());
          dgvSessions.Columns[dataGridViewColumnLastActivity.Name].HeaderText = Localize.LocalizeAuthenticationString("Last Activity".ToUpper());
          dgvSessions.Columns[dataGridViewColumnRootToken.Name].HeaderText = Localize.LocalizeAuthenticationString("Root Token".ToUpper());
          dgvSessions.Columns[dataGridViewColumnChildToken.Name].HeaderText = Localize.LocalizeAuthenticationString("Child Token".ToUpper());
          dgvSessions.Columns[dataGridViewColumnPositions.Name].HeaderText = Localize.LocalizeAuthenticationString("Positions".ToUpper());
        }
      }
    }

    #endregion

    #region Private Methods

    private void ResetCommandManagerOrXmlClient()
    {
      if (_authenticationMode == AuthenticationMode.CommandManager)
      {
        _cmNotifier.notifyCallBack -= NotifyCallback;
        _commandManager.removeCommandManagerNotifier(_cmNotifier);

        _cmNotifier = null;
        _commandManager = null;
      }
      else if (_authenticationMode == AuthenticationMode.XmlClient)
      {
        _xmlClient.OnException -= XmlClientOnException;
        _xmlClient.OnRetrieveReplyFromServer -= XmlClientOnRetrieveReplyFromServer;

        _xmlClient = null;
      }

      //resetto l'autenticazione
      _authenticationMode = AuthenticationMode.None;
    }

    #region Countdown

    private Timer _countdownRefresh;
    private const int CountdownStart = 60;
    private int _countdown;

    private void StartCountdown()
    {
      _countdownRefresh = new Timer
      {
        Interval = (int)TimeSpan.FromSeconds(0.9).TotalMilliseconds
      };
      _countdownRefresh.Tick += CountdownRefreshOnTick;
      _countdownRefresh.Start();
    }

    private void StopCountdown()
    {
      _countdownRefresh.Stop();
    }

    private void CountdownRefreshOnTick(object sender, EventArgs eventArgs)
    {
      //mando il comando solo se sono a zero
      if (SetCountdown())
        SendCommand_GetSessions();
    }

    /// <summary>
    /// Set il nuovo valore al countdown. Può essere passato un booleano che indica se è necessario resettare il countdown.
    /// Restituisce se il timer è andato a zero
    /// </summary>
    private bool SetCountdown(bool reset = false)
    {
      bool isElapsed;
      int value;

      lock (_locker)
      {
        //se non devo resettare e il mio countdown è maggiore di zero
        if (!reset && _countdown > 0)
        {
          _countdown--;
        }
        else
        {
          _countdown = CountdownStart;
        }

        value = _countdown;
        isElapsed = value == 0;
      }

      labelCountdown.Text = value.ToString();
      return isElapsed;
    }

    #endregion

    #region DataGridView

    private void ShowSessions(IList<ISession> sessions)
    {
      var culture = System.Threading.Thread.CurrentThread.CurrentCulture;

      lock (_locker)
      {
        //se non ho colonne, le aggiungo (solo avvio)
        if (_dataTableSessions.Columns.Count == 0)
        {
          _dataTableSessions.Columns.Add(dataGridViewColumnSelect.DataPropertyName, typeof(bool));
          _dataTableSessions.Columns.Add(dataGridViewColumnUsername.DataPropertyName, typeof(string));
          _dataTableSessions.Columns.Add(dataGridViewColumnProfile.DataPropertyName, typeof(string));
          _dataTableSessions.Columns.Add(dataGridViewColumnClient.DataPropertyName, typeof(string));
          _dataTableSessions.Columns.Add(dataGridViewColumnSource.DataPropertyName, typeof(string));
          _dataTableSessions.Columns.Add(dataGridViewColumnIPAddress.DataPropertyName, typeof(string));
          _dataTableSessions.Columns.Add(dataGridViewColumnPortNumber.DataPropertyName, typeof(int));
          _dataTableSessions.Columns.Add(dataGridViewColumnLastActivity.DataPropertyName, typeof(DateTime));
          _dataTableSessions.Columns.Add(dataGridViewColumnRootToken.DataPropertyName, typeof(string));
          _dataTableSessions.Columns.Add(dataGridViewColumnChildToken.DataPropertyName, typeof(string));
          _dataTableSessions.Columns.Add(dataGridViewColumnPositions.DataPropertyName, typeof(string));
        }

        //recupero la sessione
        var currentSession = _authenticationManager.Session;

        //indica se l'utente attuale non è cassioli
        var isNotCassioli = currentSession != null && !currentSession.User.Username.EqualsIgnoreCase("cassioli");


        //memorizzo tutti i nuovi ChildToken (identificatore univoco della sessione), mi servono dopo
        var childTokenList = new List<int>();

        //scorro tutte le sessioni
        foreach (var session in sessions)
        {

          #region Esclusioni

          //NOTE alcune informazioni le faccio vedere solo se mi loggo come cassioli, ovvero: 
          //NOTE tutti gli utenti Cassioli, tipi i System

          if (isNotCassioli)
          {
            if (session.User.Username.EqualsIgnoreCase("cassioli"))
              continue;

            if (session.Source.SourceType.Equals(SourceType.DynamicSystem))
              continue;
          }

          #endregion

          //memorizzo i token aggiunti
          childTokenList.Add(session.SessionTokens.ChildToken);

          //se la sessione già esiste, la aggiorno, altrimenti la aggiungo
          var rows = _dataTableSessions.Select($"{dataGridViewColumnChildToken.DataPropertyName} = '{session.SessionTokens.ChildToken}'");

          DataRow row;

          if (rows.Any())
          {
            //sessione trovata, la aggiorno
            row = rows.First();
          }
          else
          {
            //sessione non trovata, la creo nuova
            row = _dataTableSessions.NewRow();
            _dataTableSessions.Rows.Add(row);
          }

          row[dataGridViewColumnUsername.DataPropertyName] = session.User.Username;
          row[dataGridViewColumnProfile.DataPropertyName] = session.User.UserProfileCode;
          row[dataGridViewColumnClient.DataPropertyName] = session.Source.UserClientCode;
          row[dataGridViewColumnSource.DataPropertyName] = session.Source.SourceType;
          row[dataGridViewColumnIPAddress.DataPropertyName] = session.Source.IpAddressString;
          row[dataGridViewColumnPortNumber.DataPropertyName] = session.Source.PortNumber;
          row[dataGridViewColumnLastActivity.DataPropertyName] = session.LastActivity.ToString(culture.DateTimeFormat);
          row[dataGridViewColumnRootToken.DataPropertyName] = session.SessionTokens.RootToken;
          row[dataGridViewColumnChildToken.DataPropertyName] = session.SessionTokens.ChildToken;
          row[dataGridViewColumnPositions.DataPropertyName] = string.Join(",", session.PositionCollection.OrderBy(p => p, StringComparer.OrdinalIgnoreCase).ToArray());
        }

        //scorro tutta la nuova tabella, rimuovo i token che non sono contenuti perchè sono stati rimossi
        for (int i = _dataTableSessions.Rows.Count - 1; i >= 0; i--)
        {
          DataRow dr = _dataTableSessions.Rows[i];

          var childToken = dr[dataGridViewColumnChildToken.DataPropertyName].ConvertTo<int>();

          //se il token non è contenuto, lo rimouvo
          if (!childTokenList.Contains(childToken))
            dr.Delete();
        }

        var dataView = new DataView(_dataTableSessions);
        dgvSessions.DataSource = dataView;
        dgvSessions.ClearSelection();

        FilterDataGridViewSessions();
      }
    }

    private void ManageLogoutResponse(IList<int> errors)
    {
      //mostro a video gli errori, riabilito il bottone ed aggiorno le sessioni
      ShowError(errors);

      btnLogoutSessions.Enabled = true;
      SendCommand_GetSessions();

      //non ho errori
      if (!errors.Any())
      {
        MessageBoxUpdateDone();
      }
    }

    private void FilterDataGridViewSessions()
    {
      var usernameFilter = textBoxUsernameFilter.Text;
      var profileFilter = textBoxProfileFilter.Text;
      var clientFilter = textBoxClientFilter.Text;
      var sourceFilter = textBoxSourceFilter.Text;
      var positionsFilter = textBoxPositionsFilter.Text;

      StringBuilder filter = new StringBuilder();

      filter.AppendLine("1=1");

      if (!string.IsNullOrEmpty(usernameFilter))
        filter.AppendLine($"AND {dataGridViewColumnUsername.DataPropertyName} Like '%{usernameFilter}%'");

      if (!string.IsNullOrEmpty(profileFilter))
        filter.AppendLine($"AND {dataGridViewColumnProfile.DataPropertyName} Like '%{profileFilter}%'");

      if (!string.IsNullOrEmpty(clientFilter))
        filter.AppendLine($"AND {dataGridViewColumnClient.DataPropertyName} Like '%{clientFilter}%'");

      if (!string.IsNullOrEmpty(sourceFilter))
        filter.AppendLine($"AND {dataGridViewColumnSource.DataPropertyName} Like '%{sourceFilter}%'");

      if (!string.IsNullOrEmpty(positionsFilter))
        filter.AppendLine($"AND {dataGridViewColumnPositions.DataPropertyName} Like '%{positionsFilter}%'");

      lock (_locker)
      {
        DataView dv = dgvSessions.DataSource as DataView;

        if (dv == null)
          return;

        dv.RowFilter = filter.ToString();
        dgvSessions.ClearSelection();
      }
    }

    #endregion

    /// <summary>
    /// Restituisce i token da buttare fuori
    /// </summary>
    private IList<int> GetSelectedSessions()
    {
      IList<int> tokenToLogout = new List<int>();

      lock (_locker)
      {
        foreach (DataRow row in _dataTableSessions.Rows)
        {
          if (row[dataGridViewColumnSelect.DataPropertyName].ConvertTo<bool>(true))
            tokenToLogout.Add(row[dataGridViewColumnChildToken.DataPropertyName].ConvertTo<int>());
        }
      }

      return tokenToLogout;
    }

    private static bool MessageBoxConfirmForceSessionsLogout()
    {
      var text = Localize.LocalizeAuthenticationString("Are You Sure To Force Logout For Selected Sessions?");
      var caption = Localize.LocalizeAuthenticationString("Confirm Choice");

      var result = MessageBox.Show(text, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

      return result == DialogResult.Yes;
    }

    private static bool MessageBoxUpdateDone()
    {
      var text = Localize.LocalizeAuthenticationString("Sessions removed successfully!");
      var caption = Localize.LocalizeAuthenticationString("Result");

      var result = MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

      return result == DialogResult.OK;
    }


    #region Errors

    private void ShowError(int errorCode, IList<string> additionalErrorList = null)
    {
      ShowError(new List<Tuple<int, IList<string>>>
      {
        new Tuple<int, IList<string>>(errorCode, additionalErrorList)
      });
    }

    private void ShowError(IList<int> errorCodeList)
    {
      var errorList = errorCodeList.Select(code => new Tuple<int, IList<string>>(code, null)).ToList();

      ShowError(errorList);
    }

    private void ShowError(IList<Tuple<int, IList<string>>> errorList = null)
    {
      //se errore è diverso da nullo, faccio comparire la schermata, traduco l'errore e setto al scritta
      var showError = errorList != null && errorList.Count > 0/* && errors.All(e => e != 1)*/;
      if (showError)
      {
        var index = 0;
        var count = errorList.Count;

        StringBuilder sb = new StringBuilder();

        foreach (var error in errorList)
        {
          //recupero per tutti i codici, la loro descrizione tradotta
          var errorCode2Descr = GetErrorCode(error.Item1);

          var errorCode = errorCode2Descr.Single().Key;
          var errorDescr = errorCode2Descr.Single().Value;

          sb.Append($"• [{errorCode}] {errorDescr.LocalizeAuthenticationString()}");

          #region Additional Erros

          var additionalErrors = error.Item2;

          if (additionalErrors != null)
          {
            //se gli errori aggiuntivi sono presenti, li scrivo

            sb.Append(" (");

            for (int i = 0; i < additionalErrors.Count; i++)
            {
              sb.Append($"{additionalErrors[i]}");

              if (i + 1 < additionalErrors.Count)
                sb.Append(", ");
            }

            sb.Append(")");
          }

          #endregion

          if (++index < count)
            sb.Append($"{Environment.NewLine}");
        }

        textBoxError.Text = sb.ToString();
      }

      pnlMessage.Visible = showError; //schermata errore
    }

    private IDictionary<int, string> GetErrorCode(params int[] errorCodes)
    {
      var error2Descr = new SortedList<int, string>();

      if (errorCodes == null)
        return error2Descr;

      foreach (var errorCode in errorCodes.Distinct())
      {
        error2Descr.Add(errorCode, _authenticationManager.GetDescriptionForErrorCode(errorCode));
      }

      return error2Descr;
    }

    #endregion

    #endregion

    #region Manage Response

    private void ManageReceivedGetSessionsResponse(IList<ISession> sessions)
    {
      ShowSessions(sessions);
    }

    private void ManageNotReceivedGetSessionsResponse()
    {
      ShowSessions(new List<ISession>());
    }

    private void ManageReceivedLogoutResponse(IList<IAuthenticationResponse> responseList)
    {
      var errors = new List<int>();

      foreach (var response in responseList)
      {
        //mostro solo le risposte con errore
        if (response.IsInError)
        {
          errors.Add(response.Result);
        }
      }

      ManageLogoutResponse(errors);
    }

    private void ManageNotReceivedLogoutResponse()
    {
      ManageLogoutResponse(new[] { LocalErrorCodeMapper.ServerNotReachable });
    }

    #endregion

    #region XmlClient / CommandManager

    #region Richieste

    private const string CommandGetSessions = "GetSessions";
    private const string CommandLogout = "Logout";

    private void SendCommand_GetSessions()
    {
      //parametri
      var parameters = new List<string>();

      var session = _authenticationManager.Session;
      if (session != null && !session.User.Username.EqualsIgnoreCase("cassioli"))
      {
        //se ho una sessione attiva, filtro quelle che posso vedere, al massimo posso vedere i miei pari profilo e chi è sotto di me in gerarchia
        parameters.Add("profile");
        parameters.Add(session.User.UserProfileCode);
        parameters.Add(UserProfileFilterRule.LowerOrEqualPrivilege.ToString());
      }

      string cmd = $"{XmlCommandPreamble}{CommandGetSessions}".CreateXmlCommand(parameters);
      SendCommand(cmd);
    }
    private void SendCommand_Logout(IList<int> tokens)
    {
      string cmd = $"{XmlCommandPreamble}{CommandLogout}".CreateXmlCommand(tokens.Cast<object>().ToArray());
      SendCommand(cmd);
    }

    private void SendCommand(string command)
    {
      //controllo se ho settato la modalità
      if (_authenticationMode == AuthenticationMode.None)
      {
        //throw new Exception($"Set Authentication Mode before start, call method {nameof(Configure)} or  method {nameof(Configure)}");
        return;
      }

      if (_authenticationMode == AuthenticationMode.XmlClient)
      {
        var xmlCmd = _xmlClient.CreateXmlCommand(command, "cassioli");
        _xmlClient.ProcessXmlDoc(xmlCmd, "custom", this);
      }
      else if (_authenticationMode == AuthenticationMode.CommandManager)
      {
        _cmNotifier.sendCommand(command, "custom");
      }
    }

    #endregion

    #region Risposte

    #region CommandManager

    private void NotifyCallback(CommandManagerNotifier.NotifyObject notifyObject)
    {
      var xmlCmd = notifyObject.xmlCommand;
      var xmlCmdRes = notifyObject.iXmlStreamer as XmlCommandResponse;

      ManageResponse(xmlCmd, xmlCmdRes);
    }

    #endregion

    #region XmlClient

    private void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs msgEventArgs)
    {
      var xmlCmd = msgEventArgs.xmlCommand as XmlCommand;
      var xmlCmdRes = msgEventArgs.iXmlStreamer as XmlCommandResponse;

      ManageResponse(xmlCmd, xmlCmdRes);
    }

    private void XmlClientOnException(object sender, ExceptionEventArgs exEventArgs)
    {
      var xmlCmd = exEventArgs.xmlCommand as XmlCommand;
      XmlCommandResponse xmlCmdRes = null;

      ManageResponse(xmlCmd, xmlCmdRes);
    }

    #endregion

    #region ManageResponse

    private void ManageResponse(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      var commandMethodName = xmlCmd.name.TrimPreamble();

      #region GetSessions

      if (commandMethodName.EqualsConsiderCase(CommandGetSessions))
      {
        InvokeControlAction(this, delegate { Response_GetSessions(xmlCmd, xmlCmdRes); });
      }

      #endregion

      #region CommandLogout

      else if (commandMethodName.EqualsConsiderCase(CommandLogout))
      {
        InvokeControlAction(this, delegate { Response_Logout(xmlCmd, xmlCmdRes); });
      }

      #endregion
    }

    #endregion

    #endregion

    #region Gestione Risposte

    private void Response_GetSessions(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      if (xmlCmdRes != null)
      {
        //recupero la risposta
        IList<ISession> sessions = new List<ISession>();

        foreach (var item in xmlCmdRes.Items)
        {
          //devo convertire il tutto in un formato particolare
          var items = new List<string>();

          using (var field = item.GetEnumerator())
          {
            while (field.MoveNext())
            {
              items.Add(field.Current);
            }
          }

          sessions.Add(_authenticationManager.CreateAndFillAuthenticationResponse(items).Session);
        }

        ManageReceivedGetSessionsResponse(sessions);
      }
      else
      {
        //risposta non ricevuta
        ManageNotReceivedGetSessionsResponse();
      }
    }

    private void Response_Logout(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      if (xmlCmdRes != null)
      {
        //recupero la risposta

        IList<IAuthenticationResponse> authenticationResponseList = new List<IAuthenticationResponse>();

        var items = xmlCmdRes.Items;

        //devo prendere i pacchetti a 3 a 3 perchè nel logout si risponde per multipli di 3 (ogni sessione ha 3 item)
        for (int i = 0; i < items.Count; i += 3)
        {
          var resp = new List<XmlCommandResponse.Item>
          {
            items[i],
            items[i + 1],
            items[i + 2]
          };

          authenticationResponseList.Add(_authenticationManager.CreateAndFillAuthenticationResponse(resp));
        }

        ManageReceivedLogoutResponse(authenticationResponseList);
      }
      else
      {
        //risposta non ricevuta
        ManageNotReceivedLogoutResponse();
      }
    }

    #endregion

    #endregion

    #region Utilities

    /// <summary>
    /// Contesto attuale del controllo
    /// </summary>
    private static readonly System.Threading.SynchronizationContext SyncContext = System.Threading.SynchronizationContext.Current;

    /// <summary>
    /// Permette di eseguire il metodo <param name="action"/> della classe <param name="cont"/>
    /// invocandolo tramite thread principale così da evitare le eccezioni
    /// </summary>
    /// <typeparam name="T">Tipo della classe invocante</typeparam>
    /// <param name="cont">Classe invocante</param>
    /// <param name="action">Metodo da invocare</param>
    protected static void InvokeControlAction<T>(T cont, Action<T> action)
      where T : Control
    {
      //if (cont.InvokeRequired)
      //{
      //  cont.Invoke(new Action<T, Action<T>>(InvokeControlAction), cont, action);
      //}
      //else
      //{
      //  action(cont);
      //}

      //NOTE la vecchia implementazione prevedeva il classico InvokeRequired ma questo è valido solo se chi istanzia il controllo è una WinForms.
      //NOTE Per rendere compatibile il controllo anche con WPF, devo utilizzare il contesto.
      //NOTE ATTENZIONE: non è necessario utilizzare ISynchronizeInvoke.InvokeRequired o Dispatcher.CheckAccess, il controllo è interno al contesto stesso.

      if (SyncContext == null)
      {
        action(cont);
      }
      else
      {
        SyncContext.Post(o => action(cont), null);
      }
    }

    private static bool IsWpfGuiThread()
    {
      //return System.Windows.Threading.Dispatcher.FromThread(System.Threading.Thread.CurrentThread) != null;
      return SyncContext is System.Windows.Threading.DispatcherSynchronizationContext;
    }

    private static bool IsWinFormsGuiThread()
    {
      return SyncContext is System.Windows.Forms.WindowsFormsSynchronizationContext;
    }

    #endregion

    #region Support Class

    private enum UserProfileFilterRule
    {
      LowerPrivilege,
      LowerOrEqualPrivilege,
      HigherPrivilege,
      HigherOrEqualPrivilege
    }

    #endregion
  }
}