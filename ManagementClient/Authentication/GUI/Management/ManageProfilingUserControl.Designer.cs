﻿using Authentication.Controls;

namespace Authentication.GUI.Management
{
  partial class ManageProfilingUserControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			this.coloredBorderPanel = new Authentication.Controls.ColoredBorderPanel();
			this.panelMain = new System.Windows.Forms.Panel();
			this.dgvProfiling = new System.Windows.Forms.DataGridView();
			this.dataGridViewColumnUserStructure = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnUserStructureCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnClient = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnProfile = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnHierarchy = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnAction = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnStandard = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.dataGridViewColumnEnabled = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.dataGridViewColumnOptionalData = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.panelHeaderFilters = new System.Windows.Forms.Panel();
			this.panelFilterLeft = new System.Windows.Forms.Panel();
			this.panelFilters = new System.Windows.Forms.Panel();
			this.tableLayoutPanelFilter = new System.Windows.Forms.TableLayoutPanel();
			this.panelProfileFilter = new Authentication.Controls.ColoredBorderPanel();
			this.labelTrProfile = new System.Windows.Forms.Label();
			this.ctrlComboProfile = new Authentication.Controls.CtrlCombo();
			this.panelClientFilter = new Authentication.Controls.ColoredBorderPanel();
			this.labelTrClient = new System.Windows.Forms.Label();
			this.ctrlComboClient = new Authentication.Controls.CtrlCombo();
			this.panelReloadProfiles = new System.Windows.Forms.Panel();
			this.btnReloadProfiles = new Authentication.Controls.CtrlButton();
			this.labelTrManageActionClientProfile = new System.Windows.Forms.Label();
			this.panelTop = new System.Windows.Forms.Panel();
			this.lblTrProfiling = new System.Windows.Forms.Label();
			this.panelBottom = new System.Windows.Forms.Panel();
			this.pnlMessage = new Authentication.Controls.CtrlCustomPanel();
			this.textBoxError = new System.Windows.Forms.TextBox();
			this.panelInfo = new System.Windows.Forms.Panel();
			this.panelLogout = new System.Windows.Forms.Panel();
			this.btnSaveChanges = new Authentication.Controls.CtrlButton();
			this.coloredBorderPanel.SuspendLayout();
			this.panelMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvProfiling)).BeginInit();
			this.panelHeaderFilters.SuspendLayout();
			this.panelFilterLeft.SuspendLayout();
			this.panelFilters.SuspendLayout();
			this.tableLayoutPanelFilter.SuspendLayout();
			this.panelProfileFilter.SuspendLayout();
			this.panelClientFilter.SuspendLayout();
			this.panelReloadProfiles.SuspendLayout();
			this.panelTop.SuspendLayout();
			this.panelBottom.SuspendLayout();
			this.pnlMessage.SuspendLayout();
			this.panelLogout.SuspendLayout();
			this.SuspendLayout();
			// 
			// coloredBorderPanel
			// 
			this.coloredBorderPanel.BorderColor = System.Drawing.Color.SkyBlue;
			this.coloredBorderPanel.BorderWidth = 2;
			this.coloredBorderPanel.Controls.Add(this.panelMain);
			this.coloredBorderPanel.Controls.Add(this.panelTop);
			this.coloredBorderPanel.Controls.Add(this.panelBottom);
			this.coloredBorderPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.coloredBorderPanel.Location = new System.Drawing.Point(0, 0);
			this.coloredBorderPanel.Name = "coloredBorderPanel";
			this.coloredBorderPanel.Size = new System.Drawing.Size(1000, 600);
			this.coloredBorderPanel.TabIndex = 16;
			// 
			// panelMain
			// 
			this.panelMain.BackColor = System.Drawing.SystemColors.Control;
			this.panelMain.Controls.Add(this.dgvProfiling);
			this.panelMain.Controls.Add(this.panelHeaderFilters);
			this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelMain.Location = new System.Drawing.Point(2, 44);
			this.panelMain.Name = "panelMain";
			this.panelMain.Size = new System.Drawing.Size(996, 484);
			this.panelMain.TabIndex = 2;
			// 
			// dgvProfiling
			// 
			this.dgvProfiling.AllowUserToAddRows = false;
			this.dgvProfiling.AllowUserToDeleteRows = false;
			this.dgvProfiling.AllowUserToOrderColumns = true;
			this.dgvProfiling.AllowUserToResizeRows = false;
			this.dgvProfiling.BackgroundColor = System.Drawing.Color.White;
			this.dgvProfiling.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dgvProfiling.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvProfiling.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvProfiling.ColumnHeadersHeight = 50;
			this.dgvProfiling.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.dgvProfiling.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewColumnUserStructure,
            this.dataGridViewColumnUserStructureCode,
            this.dataGridViewColumnClient,
            this.dataGridViewColumnProfile,
            this.dataGridViewColumnHierarchy,
            this.dataGridViewColumnAction,
            this.dataGridViewColumnDescription,
            this.dataGridViewColumnStandard,
            this.dataGridViewColumnEnabled,
            this.dataGridViewColumnOptionalData});
			dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dgvProfiling.DefaultCellStyle = dataGridViewCellStyle5;
			this.dgvProfiling.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvProfiling.Location = new System.Drawing.Point(0, 126);
			this.dgvProfiling.Name = "dgvProfiling";
			dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvProfiling.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
			this.dgvProfiling.RowHeadersVisible = false;
			this.dgvProfiling.RowHeadersWidth = 50;
			this.dgvProfiling.RowTemplate.Height = 28;
			this.dgvProfiling.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvProfiling.Size = new System.Drawing.Size(996, 358);
			this.dgvProfiling.TabIndex = 13;
			this.dgvProfiling.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProfiling_CellValueChanged);
			this.dgvProfiling.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvProfiling_CurrentCellDirtyStateChanged);
			this.dgvProfiling.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvProfiling_KeyPress);
			// 
			// dataGridViewColumnUserStructure
			// 
			this.dataGridViewColumnUserStructure.DataPropertyName = "UserStructure";
			this.dataGridViewColumnUserStructure.HeaderText = "UserStructure";
			this.dataGridViewColumnUserStructure.Name = "dataGridViewColumnUserStructure";
			this.dataGridViewColumnUserStructure.Visible = false;
			// 
			// dataGridViewColumnUserStructureCode
			// 
			this.dataGridViewColumnUserStructureCode.DataPropertyName = "UserStructureCode";
			this.dataGridViewColumnUserStructureCode.HeaderText = "UserStructureCode";
			this.dataGridViewColumnUserStructureCode.Name = "dataGridViewColumnUserStructureCode";
			this.dataGridViewColumnUserStructureCode.Visible = false;
			// 
			// dataGridViewColumnClient
			// 
			this.dataGridViewColumnClient.DataPropertyName = "Client";
			this.dataGridViewColumnClient.HeaderText = "Client";
			this.dataGridViewColumnClient.Name = "dataGridViewColumnClient";
			this.dataGridViewColumnClient.Visible = false;
			// 
			// dataGridViewColumnProfile
			// 
			this.dataGridViewColumnProfile.DataPropertyName = "Profile";
			this.dataGridViewColumnProfile.HeaderText = "Profile";
			this.dataGridViewColumnProfile.Name = "dataGridViewColumnProfile";
			this.dataGridViewColumnProfile.Visible = false;
			// 
			// dataGridViewColumnHierarchy
			// 
			this.dataGridViewColumnHierarchy.DataPropertyName = "Hierarchy";
			this.dataGridViewColumnHierarchy.HeaderText = "Hierarchy";
			this.dataGridViewColumnHierarchy.Name = "dataGridViewColumnHierarchy";
			this.dataGridViewColumnHierarchy.Visible = false;
			// 
			// dataGridViewColumnAction
			// 
			this.dataGridViewColumnAction.DataPropertyName = "Action";
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			this.dataGridViewColumnAction.DefaultCellStyle = dataGridViewCellStyle2;
			this.dataGridViewColumnAction.HeaderText = "Action";
			this.dataGridViewColumnAction.MinimumWidth = 300;
			this.dataGridViewColumnAction.Name = "dataGridViewColumnAction";
			this.dataGridViewColumnAction.ReadOnly = true;
			this.dataGridViewColumnAction.Width = 300;
			// 
			// dataGridViewColumnDescription
			// 
			this.dataGridViewColumnDescription.DataPropertyName = "Description";
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			this.dataGridViewColumnDescription.DefaultCellStyle = dataGridViewCellStyle3;
			this.dataGridViewColumnDescription.HeaderText = "Description";
			this.dataGridViewColumnDescription.MinimumWidth = 350;
			this.dataGridViewColumnDescription.Name = "dataGridViewColumnDescription";
			this.dataGridViewColumnDescription.ReadOnly = true;
			this.dataGridViewColumnDescription.Width = 350;
			// 
			// dataGridViewColumnStandard
			// 
			this.dataGridViewColumnStandard.DataPropertyName = "Standard";
			this.dataGridViewColumnStandard.HeaderText = "Standard";
			this.dataGridViewColumnStandard.MinimumWidth = 100;
			this.dataGridViewColumnStandard.Name = "dataGridViewColumnStandard";
			this.dataGridViewColumnStandard.ReadOnly = true;
			this.dataGridViewColumnStandard.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewColumnStandard.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.dataGridViewColumnStandard.Width = 101;
			// 
			// dataGridViewColumnEnabled
			// 
			this.dataGridViewColumnEnabled.DataPropertyName = "Enabled";
			this.dataGridViewColumnEnabled.HeaderText = "Enabled";
			this.dataGridViewColumnEnabled.MinimumWidth = 100;
			this.dataGridViewColumnEnabled.Name = "dataGridViewColumnEnabled";
			this.dataGridViewColumnEnabled.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewColumnEnabled.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			// 
			// dataGridViewColumnOptionalData
			// 
			this.dataGridViewColumnOptionalData.DataPropertyName = "OptionalData";
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			this.dataGridViewColumnOptionalData.DefaultCellStyle = dataGridViewCellStyle4;
			this.dataGridViewColumnOptionalData.HeaderText = "Optional Data";
			this.dataGridViewColumnOptionalData.MinimumWidth = 300;
			this.dataGridViewColumnOptionalData.Name = "dataGridViewColumnOptionalData";
			this.dataGridViewColumnOptionalData.Width = 300;
			// 
			// panelHeaderFilters
			// 
			this.panelHeaderFilters.Controls.Add(this.panelFilterLeft);
			this.panelHeaderFilters.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelHeaderFilters.Location = new System.Drawing.Point(0, 0);
			this.panelHeaderFilters.Name = "panelHeaderFilters";
			this.panelHeaderFilters.Size = new System.Drawing.Size(996, 126);
			this.panelHeaderFilters.TabIndex = 15;
			// 
			// panelFilterLeft
			// 
			this.panelFilterLeft.Controls.Add(this.panelFilters);
			this.panelFilterLeft.Controls.Add(this.labelTrManageActionClientProfile);
			this.panelFilterLeft.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelFilterLeft.Location = new System.Drawing.Point(0, 0);
			this.panelFilterLeft.Name = "panelFilterLeft";
			this.panelFilterLeft.Size = new System.Drawing.Size(996, 126);
			this.panelFilterLeft.TabIndex = 16;
			// 
			// panelFilters
			// 
			this.panelFilters.BackColor = System.Drawing.SystemColors.Control;
			this.panelFilters.Controls.Add(this.tableLayoutPanelFilter);
			this.panelFilters.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelFilters.Location = new System.Drawing.Point(0, 38);
			this.panelFilters.Name = "panelFilters";
			this.panelFilters.Size = new System.Drawing.Size(996, 88);
			this.panelFilters.TabIndex = 14;
			// 
			// tableLayoutPanelFilter
			// 
			this.tableLayoutPanelFilter.ColumnCount = 5;
			this.tableLayoutPanelFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
			this.tableLayoutPanelFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
			this.tableLayoutPanelFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
			this.tableLayoutPanelFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelFilter.Controls.Add(this.panelProfileFilter, 1, 0);
			this.tableLayoutPanelFilter.Controls.Add(this.panelClientFilter, 2, 0);
			this.tableLayoutPanelFilter.Controls.Add(this.panelReloadProfiles, 3, 0);
			this.tableLayoutPanelFilter.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanelFilter.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanelFilter.Name = "tableLayoutPanelFilter";
			this.tableLayoutPanelFilter.RowCount = 1;
			this.tableLayoutPanelFilter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanelFilter.Size = new System.Drawing.Size(996, 88);
			this.tableLayoutPanelFilter.TabIndex = 7;
			// 
			// panelProfileFilter
			// 
			this.panelProfileFilter.BorderColor = System.Drawing.Color.CornflowerBlue;
			this.panelProfileFilter.BorderWidth = 2;
			this.panelProfileFilter.Controls.Add(this.labelTrProfile);
			this.panelProfileFilter.Controls.Add(this.ctrlComboProfile);
			this.panelProfileFilter.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelProfileFilter.Location = new System.Drawing.Point(3, 0);
			this.panelProfileFilter.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
			this.panelProfileFilter.Name = "panelProfileFilter";
			this.panelProfileFilter.Size = new System.Drawing.Size(320, 88);
			this.panelProfileFilter.TabIndex = 1;
			// 
			// labelTrProfile
			// 
			this.labelTrProfile.Dock = System.Windows.Forms.DockStyle.Fill;
			this.labelTrProfile.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelTrProfile.Location = new System.Drawing.Point(2, 2);
			this.labelTrProfile.Name = "labelTrProfile";
			this.labelTrProfile.Size = new System.Drawing.Size(316, 44);
			this.labelTrProfile.TabIndex = 1;
			this.labelTrProfile.Text = "Profile";
			this.labelTrProfile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ctrlComboProfile
			// 
			this.ctrlComboProfile.BackColorValue = System.Drawing.Color.LightGray;
			this.ctrlComboProfile.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.ctrlComboProfile.FontSize = 12;
			this.ctrlComboProfile.Location = new System.Drawing.Point(2, 46);
			this.ctrlComboProfile.Name = "ctrlComboProfile";
			this.ctrlComboProfile.Size = new System.Drawing.Size(316, 40);
			this.ctrlComboProfile.TabIndex = 2;
			this.ctrlComboProfile.SelectionChanged += new Authentication.Controls.SelectionChangedHandler(this.ctrlComboClientOrProfile_SelectionChanged);
			// 
			// panelClientFilter
			// 
			this.panelClientFilter.BorderColor = System.Drawing.Color.CornflowerBlue;
			this.panelClientFilter.BorderWidth = 2;
			this.panelClientFilter.Controls.Add(this.labelTrClient);
			this.panelClientFilter.Controls.Add(this.ctrlComboClient);
			this.panelClientFilter.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelClientFilter.Location = new System.Drawing.Point(353, 0);
			this.panelClientFilter.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
			this.panelClientFilter.Name = "panelClientFilter";
			this.panelClientFilter.Size = new System.Drawing.Size(320, 88);
			this.panelClientFilter.TabIndex = 5;
			// 
			// labelTrClient
			// 
			this.labelTrClient.Dock = System.Windows.Forms.DockStyle.Fill;
			this.labelTrClient.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
			this.labelTrClient.Location = new System.Drawing.Point(2, 2);
			this.labelTrClient.Name = "labelTrClient";
			this.labelTrClient.Size = new System.Drawing.Size(316, 44);
			this.labelTrClient.TabIndex = 15;
			this.labelTrClient.Text = "Client";
			this.labelTrClient.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ctrlComboClient
			// 
			this.ctrlComboClient.BackColorValue = System.Drawing.Color.LightGray;
			this.ctrlComboClient.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.ctrlComboClient.FontSize = 12;
			this.ctrlComboClient.Location = new System.Drawing.Point(2, 46);
			this.ctrlComboClient.Name = "ctrlComboClient";
			this.ctrlComboClient.Size = new System.Drawing.Size(316, 40);
			this.ctrlComboClient.TabIndex = 16;
			this.ctrlComboClient.SelectionChanged += new Authentication.Controls.SelectionChangedHandler(this.ctrlComboClientOrProfile_SelectionChanged);
			// 
			// panelReloadProfiles
			// 
			this.panelReloadProfiles.Controls.Add(this.btnReloadProfiles);
			this.panelReloadProfiles.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelReloadProfiles.Location = new System.Drawing.Point(703, 0);
			this.panelReloadProfiles.Margin = new System.Windows.Forms.Padding(0);
			this.panelReloadProfiles.Name = "panelReloadProfiles";
			this.panelReloadProfiles.Size = new System.Drawing.Size(150, 88);
			this.panelReloadProfiles.TabIndex = 6;
			// 
			// btnReloadProfiles
			// 
			this.btnReloadProfiles.BackColor1 = System.Drawing.Color.RoyalBlue;
			this.btnReloadProfiles.BackColor2 = System.Drawing.SystemColors.Highlight;
			this.btnReloadProfiles.ButtonText = "Reload Profiles";
			this.btnReloadProfiles.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.btnReloadProfiles.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnReloadProfiles.Location = new System.Drawing.Point(0, 31);
			this.btnReloadProfiles.Name = "btnReloadProfiles";
			this.btnReloadProfiles.PressColor1 = System.Drawing.SystemColors.HotTrack;
			this.btnReloadProfiles.PressColor2 = System.Drawing.SystemColors.MenuHighlight;
			this.btnReloadProfiles.Size = new System.Drawing.Size(150, 57);
			this.btnReloadProfiles.TabIndex = 4;
			this.btnReloadProfiles.TextColor = System.Drawing.Color.White;
			this.btnReloadProfiles.Click += new System.EventHandler(this.btnReloadProfiles_Click);
			// 
			// labelTrManageActionClientProfile
			// 
			this.labelTrManageActionClientProfile.AutoSize = true;
			this.labelTrManageActionClientProfile.Dock = System.Windows.Forms.DockStyle.Fill;
			this.labelTrManageActionClientProfile.Font = new System.Drawing.Font("Segoe UI", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelTrManageActionClientProfile.ForeColor = System.Drawing.Color.DarkGreen;
			this.labelTrManageActionClientProfile.Location = new System.Drawing.Point(0, 0);
			this.labelTrManageActionClientProfile.Name = "labelTrManageActionClientProfile";
			this.labelTrManageActionClientProfile.Size = new System.Drawing.Size(334, 25);
			this.labelTrManageActionClientProfile.TabIndex = 0;
			this.labelTrManageActionClientProfile.Text = "Manage Actions, Clients and Profiles";
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.Control;
			this.panelTop.Controls.Add(this.lblTrProfiling);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(2, 2);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(996, 42);
			this.panelTop.TabIndex = 16;
			// 
			// lblTrProfiling
			// 
			this.lblTrProfiling.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTrProfiling.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTrProfiling.ForeColor = System.Drawing.Color.DodgerBlue;
			this.lblTrProfiling.Location = new System.Drawing.Point(0, 0);
			this.lblTrProfiling.Name = "lblTrProfiling";
			this.lblTrProfiling.Size = new System.Drawing.Size(996, 42);
			this.lblTrProfiling.TabIndex = 0;
			this.lblTrProfiling.Text = "Profiling";
			this.lblTrProfiling.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelBottom
			// 
			this.panelBottom.Controls.Add(this.pnlMessage);
			this.panelBottom.Controls.Add(this.panelLogout);
			this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelBottom.Location = new System.Drawing.Point(2, 528);
			this.panelBottom.Name = "panelBottom";
			this.panelBottom.Size = new System.Drawing.Size(996, 70);
			this.panelBottom.TabIndex = 30;
			// 
			// pnlMessage
			// 
			this.pnlMessage.BorderColor = System.Drawing.Color.DodgerBlue;
			this.pnlMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnlMessage.BorderWidth = 2;
			this.pnlMessage.Controls.Add(this.textBoxError);
			this.pnlMessage.Controls.Add(this.panelInfo);
			this.pnlMessage.Curvature = 7;
			this.pnlMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlMessage.Location = new System.Drawing.Point(200, 0);
			this.pnlMessage.Name = "pnlMessage";
			this.pnlMessage.Padding = new System.Windows.Forms.Padding(6);
			this.pnlMessage.Size = new System.Drawing.Size(796, 70);
			this.pnlMessage.TabIndex = 10;
			this.pnlMessage.Visible = false;
			// 
			// textBoxError
			// 
			this.textBoxError.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxError.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxError.ForeColor = System.Drawing.Color.Red;
			this.textBoxError.Location = new System.Drawing.Point(75, 6);
			this.textBoxError.Multiline = true;
			this.textBoxError.Name = "textBoxError";
			this.textBoxError.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBoxError.Size = new System.Drawing.Size(715, 58);
			this.textBoxError.TabIndex = 1;
			this.textBoxError.Text = "Error";
			// 
			// panelInfo
			// 
			this.panelInfo.BackgroundImage = global::Authentication.Properties.Resources.Warning;
			this.panelInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.panelInfo.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelInfo.Location = new System.Drawing.Point(6, 6);
			this.panelInfo.Name = "panelInfo";
			this.panelInfo.Size = new System.Drawing.Size(69, 58);
			this.panelInfo.TabIndex = 0;
			// 
			// panelLogout
			// 
			this.panelLogout.Controls.Add(this.btnSaveChanges);
			this.panelLogout.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelLogout.Location = new System.Drawing.Point(0, 0);
			this.panelLogout.Name = "panelLogout";
			this.panelLogout.Size = new System.Drawing.Size(200, 70);
			this.panelLogout.TabIndex = 11;
			// 
			// btnSaveChanges
			// 
			this.btnSaveChanges.BackColor1 = System.Drawing.Color.ForestGreen;
			this.btnSaveChanges.BackColor2 = System.Drawing.Color.Green;
			this.btnSaveChanges.ButtonText = "Save Changes";
			this.btnSaveChanges.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSaveChanges.Location = new System.Drawing.Point(37, 10);
			this.btnSaveChanges.Name = "btnSaveChanges";
			this.btnSaveChanges.PressColor1 = System.Drawing.Color.DarkGreen;
			this.btnSaveChanges.PressColor2 = System.Drawing.Color.DarkGreen;
			this.btnSaveChanges.Size = new System.Drawing.Size(131, 46);
			this.btnSaveChanges.TabIndex = 29;
			this.btnSaveChanges.TextColor = System.Drawing.Color.White;
			this.btnSaveChanges.Click += new System.EventHandler(this.btnSaveChanges_Click);
			// 
			// ManageProfilingUserControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Transparent;
			this.Controls.Add(this.coloredBorderPanel);
			this.Name = "ManageProfilingUserControl";
			this.Size = new System.Drawing.Size(1000, 600);
			this.coloredBorderPanel.ResumeLayout(false);
			this.panelMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvProfiling)).EndInit();
			this.panelHeaderFilters.ResumeLayout(false);
			this.panelFilterLeft.ResumeLayout(false);
			this.panelFilterLeft.PerformLayout();
			this.panelFilters.ResumeLayout(false);
			this.tableLayoutPanelFilter.ResumeLayout(false);
			this.panelProfileFilter.ResumeLayout(false);
			this.panelClientFilter.ResumeLayout(false);
			this.panelReloadProfiles.ResumeLayout(false);
			this.panelTop.ResumeLayout(false);
			this.panelBottom.ResumeLayout(false);
			this.pnlMessage.ResumeLayout(false);
			this.pnlMessage.PerformLayout();
			this.panelLogout.ResumeLayout(false);
			this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panelMain;
    private System.Windows.Forms.DataGridView dgvProfiling;
    private System.Windows.Forms.Label lblTrProfiling;
    private System.Windows.Forms.Panel panelFilters;
    private System.Windows.Forms.Label labelTrManageActionClientProfile;
    private ColoredBorderPanel coloredBorderPanel;
    private System.Windows.Forms.Panel panelInfo;
    private CtrlCustomPanel pnlMessage;
    private System.Windows.Forms.Label labelTrClient;
    private System.Windows.Forms.Panel panelBottom;
    private System.Windows.Forms.Panel panelLogout;
    private CtrlButton btnSaveChanges;
    private Authentication.Controls.ColoredBorderPanel panelProfileFilter;
    private Authentication.Controls.ColoredBorderPanel panelClientFilter;
    private System.Windows.Forms.Panel panelHeaderFilters;
    private System.Windows.Forms.Panel panelFilterLeft;
    private System.Windows.Forms.TextBox textBoxError;
    private System.Windows.Forms.Panel panelTop;
    private CtrlCombo ctrlComboClient;
    private System.Windows.Forms.Label labelTrProfile;
    private CtrlCombo ctrlComboProfile;
    private System.Windows.Forms.Panel panelReloadProfiles;
    private CtrlButton btnReloadProfiles;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanelFilter;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnUserStructure;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnUserStructureCode;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnClient;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnProfile;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnHierarchy;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnAction;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnDescription;
		private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewColumnStandard;
		private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewColumnEnabled;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnOptionalData;
	}
}
