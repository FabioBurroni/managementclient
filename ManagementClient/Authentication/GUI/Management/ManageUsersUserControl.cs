﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Authentication.Collections;
using Authentication.Extensions;
using Authentication.Profiling;
using Localization;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.Extensions;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlClient.Event;
using XmlCommunicationManager.XmlClient.Manager;

namespace Authentication.GUI.Management
{
  public partial class ManageUsersUserControl : UserControl
  {
    #region Fields

    #region ComboBoxItem

    private readonly IList<ComboboxItem> _comboboxBlockedItems = new[]
    {
      new ComboboxItem("Not Defined", -1),
      new ComboboxItem("Unblocked", 0),
      new ComboboxItem("Blocked", 1)
    };

    private readonly IList<ComboboxItem> _comboBoxProfilesItems = new List<ComboboxItem>();

    #endregion

    private const string DefaultEmptyPassword = "**********";

    private string _company = "Customer";

    private readonly AuthenticationManager _authenticationManager = AuthenticationManager.Instance;

    private AuthenticationMode _authenticationMode;

    private string _xmlCommandPreamble;

    private CommandManager _commandManager;
    private CommandManagerNotifier _cmNotifier;

    private IXmlClient _xmlClient;

    private readonly object _locker = new object();
    private readonly DataTable _dataTableUsers = new DataTable();

    #endregion

    #region Constructor

    public ManageUsersUserControl()
    {
      InitializeComponent();

      dgvUsers.DataError += delegate (object sender, DataGridViewDataErrorEventArgs args)
      {
        Console.WriteLine(args);
      };
    }

    #endregion

    #region Events

    private void btnDeleteUser_Click(object sender, EventArgs e)
    {
      var username = GetSelectedUser();

      if (string.IsNullOrEmpty(username))
        return;

      if (!MessageBoxConfirmDeleteUser())
        return;

      SendCommand_DeleteUser(username);
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      ReSetDetailUser(null, false);
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      //valido la creazione/modifica, se è ok, vado avanti
      User user;
      if (!ValidateUser(out user))
        return;

      //tutto ok, mando il comando
      SendCommand_CreateOrUpdateUser(user);
    }

    private void btnEditUser_Click(object sender, EventArgs e)
    {
      var selectedRows = dgvUsers.SelectedRows;

      var selectedRow = selectedRows.Count > 0 ? selectedRows[0] : default(DataGridViewRow);

      ReSetDetailUser(selectedRow, false);
    }

    private void btnCreateUser_Click(object sender, EventArgs e)
    {
      //posso continuare solo se ho almeno due profili, se ne ho solo uno significa che sono un utente con la gerarchia di profilo più bassa
      //e in queste condizioni non posso creare utenti perchè sarebbero di pari livello
      if (_comboBoxProfilesItems.Count <= 1)
      {
        MessageBoxError(LocalErrorCodeMapper.UserCreationNotAllowedAtThisProfile);

        return;
      }

      ReSetDetailUser(null, true);
    }

    private void btnRefresh_Click(object sender, EventArgs e)
    {
      Sendcommand_GetAllUserProfile();
      SendCommand_GetAllUsers();
    }

    private void btnResetFilters_Click(object sender, EventArgs e)
    {
      ResetFilters();
    }

    private void comboBoxBlockedFilter_SelectedValueChanged(object sender, EventArgs e)
    {
      FilterDataGridViewUsers();
    }

    private void textBoxFilter_TextChanged(object sender, EventArgs e)
    {
      FilterDataGridViewUsers();
    }

    private void dgvSessions_SelectionChanged(object sender, EventArgs e)
    {
      var selectedRows = dgvUsers.SelectedRows;

      var selectedRow = selectedRows.Count > 0 ? selectedRows[0] : default(DataGridViewRow);

      SetButtons(selectedRow);
      ReSetDetailUser(null, false); //qui la selezione è cambiata, quindi pulisco i dettagli
    }

    #endregion

    #region Properties

    protected override CreateParams CreateParams
    {
      get
      {
        CreateParams cp = base.CreateParams;
        cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
        return cp;
      }
    }

    /// <summary>
    /// Specifica l'eventuale preambolo da utilizzare per l'invio dei comandi
    /// </summary>
    public string XmlCommandPreamble
    {
      get
      {
        return _xmlCommandPreamble;
      }
      set
      {
        _xmlCommandPreamble = value ?? "";
      }
    }

    #endregion

    #region Public Methods

    public void InitCtrl()
    {
      //mando subito il comando per sapere tutti i profili nel sistema
      Sendcommand_GetAllUserProfile();

      //mando subito il comando per rinfrescare gli utenti attivi
      SendCommand_GetAllUsers();
    }

    public void CloseCtrl()
    {
      ResetCommandManagerOrXmlClient();
    }

    /// <summary>
    /// Setta la modalità di autenticazine come CommandManager
    /// È necessario indicare anche la società del cliente, in caso stringa nulla o vuota verrà inserito 'Customer'
    /// Inoltre si deve specificare se è possibile creare/modificare/cancellare utenti
    /// </summary>
    public void Configure(CommandManager commandManager, string company, bool canCreateUser = true, bool canEditUser = true, bool canEditUserPassword = true, bool canDeleteUser = true)
    {
      if (commandManager == null)
        throw new ArgumentNullException(nameof(commandManager));

      //modalità già settata
      if (_authenticationMode == AuthenticationMode.CommandManager)
        return;

      if (_authenticationMode != AuthenticationMode.None)
        throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

      _authenticationMode = AuthenticationMode.CommandManager;

      _cmNotifier = new CommandManagerNotifier(this, GetHashCode().ToString());
      _cmNotifier.notifyCallBack += NotifyCallback;

      _commandManager = commandManager;
      _commandManager.addCommandManagerNotifier(_cmNotifier);

      if (!string.IsNullOrEmpty(company))
        _company = company;

      btnCreateUser.Visible = canCreateUser;
      btnEditUser.Visible = canEditUser;
      lblTrPassword.Visible = canEditUserPassword;
      txbPassword.Visible = canEditUserPassword;
      btnDeleteUser.Visible = canDeleteUser;
    }

    /// <summary>
    /// Setta la modalità di autenticazine come XmlClient.
    /// È necessario indicare anche la società del cliente, in caso stringa nulla o vuota verrà inserito 'Customer'.
    /// Inoltre si deve specificare se è possibile creare/modificare/cancellare utenti
    /// </summary>
    public void Configure(IXmlClient xmlClient, string company, bool canCreateUser = true, bool canEditUser = true, bool canEditUserPassword = true, bool canDeleteUser = true)
    {
      if (xmlClient == null)
        throw new ArgumentNullException(nameof(xmlClient));

      //modalità già settata
      if (_authenticationMode == AuthenticationMode.XmlClient)
        return;

      if (_authenticationMode != AuthenticationMode.None)
        throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

      _authenticationMode = AuthenticationMode.XmlClient;

      _xmlClient = xmlClient;

      _xmlClient.OnException += XmlClientOnException;
      _xmlClient.OnRetrieveReplyFromServer += XmlClientOnRetrieveReplyFromServer;

      if (!string.IsNullOrEmpty(company))
        _company = company;

      btnCreateUser.Visible = canCreateUser;
      btnEditUser.Visible = canEditUser;
      lblTrPassword.Visible = canEditUserPassword;
      txbPassword.Visible = canEditUserPassword;
      btnDeleteUser.Visible = canDeleteUser;
    }

    public void Translate()
    {
      lblTrUsers.Text = Localize.LocalizeAuthenticationString("Users".ToUpper());
      labelTrFilterUsers.Text = Localize.LocalizeAuthenticationString("Filter Users".ToUpper());
      labelTrUsernameContains.Text = Localize.LocalizeAuthenticationString("Username Contains".ToUpper());
      labelTrFullNameContains.Text = Localize.LocalizeAuthenticationString("FullName Contains".ToUpper());
      labelTrCompanyContains.Text = Localize.LocalizeAuthenticationString("Company Contains".ToUpper());
      labelTrProfileContains.Text = Localize.LocalizeAuthenticationString("Profile Contains".ToUpper());
      labelTrBlockedContains.Text = Localize.LocalizeAuthenticationString("Blocked Contains".ToUpper());
      btnResetFilters.ButtonText = Localize.LocalizeAuthenticationString("Reset Filters".ToUpper());
      btnRefresh.ButtonText = Localize.LocalizeAuthenticationString("Refresh".ToUpper());
      btnDeleteUser.ButtonText = Localize.LocalizeAuthenticationString("Delete User".ToUpper());
      btnCreateUser.ButtonText = Localize.LocalizeAuthenticationString("Create User".ToUpper());
      btnEditUser.ButtonText = Localize.LocalizeAuthenticationString("Edit User".ToUpper());
      lblTrUsername.Text = Localize.LocalizeAuthenticationString("Username".ToUpper());
      lblTrPassword.Text = Localize.LocalizeAuthenticationString("Password".ToUpper());
      lblTrFullName.Text = Localize.LocalizeAuthenticationString("FullName".ToUpper());
      lblTrCompany.Text = Localize.LocalizeAuthenticationString("Company".ToUpper());
      lblTrProfile.Text = Localize.LocalizeAuthenticationString("Profile".ToUpper());
      lblTrBlocked.Text = Localize.LocalizeAuthenticationString("Blocked".ToUpper());
      btnCancel.ButtonText = Localize.LocalizeAuthenticationString("Cancel".ToUpper());
      btnSave.ButtonText = Localize.LocalizeAuthenticationString("Save".ToUpper());
      AddItemsAndTranslateComboBox();
      TranslateDataGridView();
    }

    private void AddItemsAndTranslateComboBox()
    {
      var selectedIndex = comboBoxBlockedFilter.SelectedIndex;

      comboBoxBlockedFilter.Items.Clear();

      foreach (var item in _comboboxBlockedItems)
      {
        item.TranslatedText = Localize.LocalizeAuthenticationString(item.Text);
        comboBoxBlockedFilter.Items.Add(item);
      }

      comboBoxBlockedFilter.SelectedIndex = selectedIndex;
    }

    private void TranslateDataGridView()
    {
      lock (_locker)
      {
        if (dgvUsers.Columns.Count > 0)
        {
          dgvUsers.Columns[dataGridViewColumnUsername.Name].HeaderText = Localize.LocalizeAuthenticationString("Username".ToUpper());
          dgvUsers.Columns[dataGridViewColumnFullName.Name].HeaderText = Localize.LocalizeAuthenticationString("FullName".ToUpper());
          dgvUsers.Columns[dataGridViewColumnCompany.Name].HeaderText = Localize.LocalizeAuthenticationString("Company".ToUpper());
          dgvUsers.Columns[dataGridViewColumnProfile.Name].HeaderText = Localize.LocalizeAuthenticationString("Profile".ToUpper());
          dgvUsers.Columns[dataGridViewColumnBlocked.Name].HeaderText = Localize.LocalizeAuthenticationString("Blocked".ToUpper());
        }
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Indica se siamo in fase di creazione o di aggiornamento
    /// </summary>
    private bool IsCreation()
    {
      return txbUsername.Enabled;
    }

    private void ResetFilters()
    {
      lock (_locker)
      {
        dgvUsers.ClearSelection();
      }

      textBoxUsernameFilter.Text = string.Empty;
      textBoxFullNameFilter.Text = string.Empty;
      textBoxCompanyFilter.Text = string.Empty;
      textBoxProfileFilter.Text = string.Empty;
      comboBoxBlockedFilter.SelectedIndex = -1;
    }

    private void ResetCommandManagerOrXmlClient()
    {
      if (_authenticationMode == AuthenticationMode.CommandManager)
      {
        _cmNotifier.notifyCallBack -= NotifyCallback;
        _commandManager.removeCommandManagerNotifier(_cmNotifier);

        _cmNotifier = null;
        _commandManager = null;
      }
      else if (_authenticationMode == AuthenticationMode.XmlClient)
      {
        _xmlClient.OnException -= XmlClientOnException;
        _xmlClient.OnRetrieveReplyFromServer -= XmlClientOnRetrieveReplyFromServer;

        _xmlClient = null;
      }

      //resetto l'autenticazione
      _authenticationMode = AuthenticationMode.None;
    }

    #region DataGridView

    private void SetUserProfiles(IList<UserProfile> profiles)
    {
      _comboBoxProfilesItems.Clear();
      cmbProfile.Items.Clear();

      foreach (var profile in profiles)
      {
        var item = new ComboboxItem(profile.Code, profile.Hierarchy);
        item.TranslatedText = Localize.LocalizeAuthenticationString(item.Text);

        _comboBoxProfilesItems.Add(item);
        cmbProfile.Items.Add(item);
      }
    }

    private void ShowUsers(IList<User> users)
    {
      lock (_locker)
      {
        //se non ho colonne, le aggiungo (solo avvio)
        if (_dataTableUsers.Columns.Count == 0)
        {
          _dataTableUsers.Columns.Add(dataGridViewColumnUser.DataPropertyName, typeof(User));
          _dataTableUsers.Columns.Add(dataGridViewColumnUsername.DataPropertyName, typeof(string));
          _dataTableUsers.Columns.Add(dataGridViewColumnFullName.DataPropertyName, typeof(string));
          _dataTableUsers.Columns.Add(dataGridViewColumnCompany.DataPropertyName, typeof(string));
          _dataTableUsers.Columns.Add(dataGridViewColumnProfile.DataPropertyName, typeof(string));
          _dataTableUsers.Columns.Add(dataGridViewColumnBlocked.DataPropertyName, typeof(bool));
        }

        //memorizzo tutti i nuovi username, mi servono dopo
        var usernameList = new List<string>();

        //scorro tutte le sessioni
        foreach (var user in users)
        {
          //memorizzo gli username aggiunti
          usernameList.Add(user.Username);

          //se la sessione già esiste, la aggiorno, altrimenti la aggiungo
          var rows = _dataTableUsers.Select($"{dataGridViewColumnUsername.DataPropertyName} = '{user.Username}'");

          DataRow row;

          if (rows.Any())
          {
            //sessione trovata, la aggiorno
            row = rows.First();
          }
          else
          {
            //sessione non trovata, la creo nuova
            row = _dataTableUsers.NewRow();
            _dataTableUsers.Rows.Add(row);
          }

          row[dataGridViewColumnUser.DataPropertyName] = user;
          row[dataGridViewColumnUsername.DataPropertyName] = user.Username;
          row[dataGridViewColumnFullName.DataPropertyName] = user.Fullname;
          row[dataGridViewColumnCompany.DataPropertyName] = user.Company;
          row[dataGridViewColumnProfile.DataPropertyName] = user.Profilecode;
          row[dataGridViewColumnBlocked.DataPropertyName] = user.Blocked;
        }

        //scorro tutta la nuova tabella, rimuovo i token che non sono contenuti perchè sono stati rimossi
        for (int i = _dataTableUsers.Rows.Count - 1; i >= 0; i--)
        {
          DataRow dr = _dataTableUsers.Rows[i];

          var username = dr[dataGridViewColumnUsername.DataPropertyName].ConvertTo<string>();

          //se il token non è contenuto, lo rimouvo
          if (!usernameList.Contains(username))
            dr.Delete();
        }

        var dataView = new DataView(_dataTableUsers);
        dgvUsers.DataSource = dataView;
        dgvUsers.ClearSelection();

        FilterDataGridViewUsers();
      }
    }

    private void ManageCreateOrUpdateUserResponse(IList<int> errors)
    {
      //mostro a video gli errori, riabilito il bottone ed aggiorno le sessioni
      MessageBoxError(errors);

      //TODO dovrei disabilitare i bottoni mentre mando i comandi
      //btnDeleteUser.Enabled = true;

      //non ho errori
      if (!errors.Any())
      {
        //rinfresco i dati solo se è andato tutto bene
        SendCommand_GetAllUsers();

        MessageBoxUpdateDone();
      }
    }

    private void FilterDataGridViewUsers()
    {
      var usernameFilter = textBoxUsernameFilter.Text;
      var fullNameFilter = textBoxFullNameFilter.Text;
      var companyFilter = textBoxCompanyFilter.Text;
      var profileFilter = textBoxProfileFilter.Text;
      var blockedFilter = comboBoxBlockedFilter.SelectedItem as ComboboxItem;

      StringBuilder filter = new StringBuilder();

      filter.AppendLine("1=1");

      if (!string.IsNullOrEmpty(usernameFilter))
        filter.AppendLine($"AND {dataGridViewColumnUsername.DataPropertyName} Like '%{usernameFilter}%'");

      if (!string.IsNullOrEmpty(fullNameFilter))
        filter.AppendLine($"AND {dataGridViewColumnFullName.DataPropertyName} Like '%{fullNameFilter}%'");

      if (!string.IsNullOrEmpty(companyFilter))
        filter.AppendLine($"AND {dataGridViewColumnCompany.DataPropertyName} Like '%{companyFilter}%'");

      if (!string.IsNullOrEmpty(profileFilter))
        filter.AppendLine($"AND {dataGridViewColumnProfile.DataPropertyName} Like '%{profileFilter}%'");

      if (blockedFilter != null && blockedFilter.Value != -1)
        filter.AppendLine($"AND {dataGridViewColumnBlocked.DataPropertyName} = {blockedFilter.Value}");

      lock (_locker)
      {

        DataView dv = dgvUsers.DataSource as DataView;

        if (dv == null)
          return;

        dv.RowFilter = filter.ToString();
        dgvUsers.ClearSelection();
      }
    }

    #endregion

    /// <summary>
    /// Restituisce i token da buttare fuori
    /// </summary>
    private string GetSelectedUser()
    {
      lock (_locker)
      {
        var selectedRows = dgvUsers.SelectedRows;

        if (selectedRows.Count == 0)
          return null;

        return selectedRows[0].Cells[dataGridViewColumnUsername.Name].Value as string;
      }
    }

    private static bool MessageBoxConfirmDeleteUser()
    {
      var text = Localize.LocalizeAuthenticationString("Are You Sure To Delete Selected User?");
      var caption = Localize.LocalizeAuthenticationString("Confirm Choice");

      var result = MessageBox.Show(text, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

      return result == DialogResult.Yes;
    }

    private static bool MessageBoxUpdateDone()
    {
      var text = Localize.LocalizeAuthenticationString("Changes Updated Successfully!");
      var caption = Localize.LocalizeAuthenticationString("Result");

      var result = MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

      return result == DialogResult.OK;
    }

    private void MessageBoxError(int errorCode, IList<string> additionalErrorList = null)
    {
      MessageBoxError(new List<Tuple<int, IList<string>>>
      {
        new Tuple<int, IList<string>>(errorCode, additionalErrorList)
      });
    }

    private void MessageBoxError(IList<int> errorCodeList)
    {
      var errorList = errorCodeList.Select(code => new Tuple<int, IList<string>>(code, null)).ToList();

      MessageBoxError(errorList);
    }

    private void MessageBoxError(IList<Tuple<int, IList<string>>> errorList = null)
    {
      //se errore è diverso da nullo, faccio comparire la schermata, traduco l'errore e setto al scritta
      var showError = errorList != null && errorList.Count > 0/* && errors.All(e => e != 1)*/;
      if (showError)
      {
        var index = 0;
        var count = errorList.Count;

        StringBuilder sb = new StringBuilder();

        foreach (var error in errorList)
        {
          //recupero per tutti i codici, la loro descrizione tradotta
          var errorCode2Descr = GetErrorCode(error.Item1);

          var errorCode = errorCode2Descr.Single().Key;
          var errorDescr = errorCode2Descr.Single().Value;

          sb.Append($"• [{errorCode}] {errorDescr.LocalizeAuthenticationString()}");

          #region Additional Erros

          var additionalErrors = error.Item2;

          if (additionalErrors != null)
          {
            //se gli errori aggiuntivi sono presenti, li scrivo

            sb.Append(" (");

            for (int i = 0; i < additionalErrors.Count; i++)
            {
              sb.Append($"{additionalErrors[i]}");

              if (i + 1 < additionalErrors.Count)
                sb.Append(", ");
            }

            sb.Append(")");
          }

          #endregion

          if (++index < count)
            sb.Append($"{Environment.NewLine}");
        }

        textBoxError.Text = sb.ToString();

        var caption = Localize.LocalizeAuthenticationString("Error");

        MessageBox.Show(sb.ToString(), caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void SetButtons(DataGridViewRow selectedRow)
    {
      var isUserSelected = selectedRow != null;

      //NOTE chiedo scusa al lettore per questo accrocco.
      //giustamente dopo aver visto questa roba, il lettore si chiederà che cavolo mi passava per la testa per scrivere questo obrobio.
      //"Thomas, perchè non hai disabilitato subito i bottoni senza usare le azioni!"
      //c'è una spiegazione valida per tutto questo. Per qualche strano motivo, se disabilitavo i bottoni contentualmente all'evento di cambio selezione della datagridview,
      //mi veniva sparata un'eccezione indexoutofrange che non ha una spiegazione valida. Ho bisogno di disabilitare quei bottoni, quindi il concetto è che li disabilito
      //in modo asincrono e così nessuno si arrabbia. Provare per credere!

      var disable = new Action(() =>
      {
        InvokeControlAction(this, delegate
        {
          //solo se almeno uno selezionato
          btnEditUser.Enabled = isUserSelected;

          //solo se almeno uno selezionato
          btnDeleteUser.Enabled = isUserSelected;
        });
      });
      disable.BeginInvoke(delegate (IAsyncResult ar) { try { disable.EndInvoke(ar); } catch { } }, null);
    }

    private void ReSetDetailUser(DataGridViewRow selectedRow, bool isNewUser)
    {
      var isUserSelected = selectedRow != null;

      if (!isUserSelected)
      {
        ResetControl(txbUsername);
        ResetControl(txbPassword);
        ResetControl(txbFullName);
        //ResetControl(txbCompany);
        txbCompany.Text = _company; //il default è sempre il valore impostato da fuori
        cmbProfile.SelectedItem = isNewUser ? _comboBoxProfilesItems.OrderByDescending(p => p.Value).First() : null; //se l'utente è nuovo, prendo l'item con gerarchia più grande (quello che conta meno)
        checkBoxBlocked.Checked = false;
      }
      else
      {
        var user = (User)selectedRow.Cells[dataGridViewColumnUser.Name].Value;

        txbUsername.Text = user.Username;
        txbPassword.Text = DefaultEmptyPassword;
        txbFullName.Text = user.Fullname;
        txbCompany.Text = user.Company;
        cmbProfile.SelectedItem = _comboBoxProfilesItems.Single(p => p.Text.EqualsConsiderCase(user.Profilecode)); //tramite il codice profilo recupero l'item da selezionare
        checkBoxBlocked.Checked = user.Blocked;
      }

      //abilitata solo se l'utente attuale è nullo o cassioli, il cliente non può modificare questo campo  
      var session = _authenticationManager.Session;
      txbCompany.Enabled = session == null || session.User.Username.EqualsIgnoreCase("cassioli");

      txbUsername.Enabled = isNewUser;
      panelEditUser.Enabled = isUserSelected || isNewUser;
    }

    private static void ResetControl(Control control)
    {
      control.Text = string.Empty;
      control.BackColor = Color.White;
    }

    private bool ValidateUser(out User user)
    {
      user = null;

      string username, password, fullname, company, profile;
      bool blocked;

      bool isValid = true;

      if (!ValidateControl(txbUsername, out username))
        isValid = false;

      //ad eccezione degli altri la password può essere nulla
      if (!ValidateControl(txbPassword, out password))
        isValid = false;

      if (!ValidateControl(txbFullName, out fullname))
        isValid = false;

      if (!ValidateControl(txbCompany, out company))
        isValid = false;

      //controllo fasullo perchè riempio prima la checkbox, comunque lo lascio per far capire il concetto che c'è dietro
      if (!ValidateControl(cmbProfile, out profile))
        isValid = false;

      blocked = checkBoxBlocked.Checked;

      if (isValid)
      {
        user = new User(username, fullname, company, profile, blocked);

        //la password va aggiunta solo se non è nulla, vuota o uguale a quella di default
        if (!string.IsNullOrEmpty(password) && !password.Equals(DefaultEmptyPassword))
          user.Password = password.Base64Encode();
      }

      return isValid;
    }

    private static bool ValidateControl(Control control, out string text)
    {
      text = control.Text;
      var isValid = !string.IsNullOrEmpty(text);
      control.BackColor = isValid ? Color.White : Color.Red;
      return isValid;
    }

    #region Errors

    private void ShowError(int errorCode, IList<string> additionalErrorList = null)
    {
      ShowError(new List<Tuple<int, IList<string>>>
      {
        new Tuple<int, IList<string>>(errorCode, additionalErrorList)
      });
    }

    private void ShowError(IList<int> errorCodeList)
    {
      var errorList = errorCodeList.Select(code => new Tuple<int, IList<string>>(code, null)).ToList();

      ShowError(errorList);
    }

    private void ShowError(IList<Tuple<int, IList<string>>> errorList = null)
    {
      //se errore è diverso da nullo, faccio comparire la schermata, traduco l'errore e setto al scritta
      var showError = errorList != null && errorList.Count > 0/* && errors.All(e => e != 1)*/;
      if (showError)
      {
        var index = 0;
        var count = errorList.Count;

        StringBuilder sb = new StringBuilder();

        foreach (var error in errorList)
        {
          //recupero per tutti i codici, la loro descrizione tradotta
          var errorCode2Descr = GetErrorCode(error.Item1);

          var errorCode = errorCode2Descr.Single().Key;
          var errorDescr = errorCode2Descr.Single().Value;

          sb.Append($"• [{errorCode}] {errorDescr.LocalizeAuthenticationString()}");

          #region Additional Erros

          var additionalErrors = error.Item2;

          if (additionalErrors != null)
          {
            //se gli errori aggiuntivi sono presenti, li scrivo

            sb.Append(" (");

            for (int i = 0; i < additionalErrors.Count; i++)
            {
              sb.Append($"{additionalErrors[i]}");

              if (i + 1 < additionalErrors.Count)
                sb.Append(", ");
            }

            sb.Append(")");
          }

          #endregion

          if (++index < count)
            sb.Append($"{Environment.NewLine}");
        }

        textBoxError.Text = sb.ToString();
      }

      pnlMessage.Visible = showError; //schermata errore
    }

    private IDictionary<int, string> GetErrorCode(params int[] errorCodes)
    {
      var error2Descr = new SortedList<int, string>();

      if (errorCodes == null)
        return error2Descr;

      foreach (var errorCode in errorCodes.Distinct())
      {
        error2Descr.Add(errorCode, _authenticationManager.GetDescriptionForErrorCode(errorCode));
      }

      return error2Descr;
    }

    #endregion

    #endregion

    #region Manage Response

    private void ManageReceivedGetAllUserProfilesResponse(IList<UserProfile> profiles)
    {
      SetUserProfiles(profiles);
    }

    private void ManageNotReceivedGetAllUserProfilesResponse()
    {
      SetUserProfiles(new List<UserProfile>());
    }

    private void ManageReceivedGetAllUsersResponse(IList<User> users)
    {
      ShowUsers(users);
    }

    private void ManageNotReceivedGetAllUsersResponse()
    {
      ShowUsers(new List<User>());
    }

    private void ManageReceivedCreateOrUpdateUserResponse(IList<GenericResponse> responseList)
    {
      var errors = new List<int>();

      foreach (var response in responseList)
      {
        //mostro solo le risposte con errore
        if (!response.IsOk)
        {
          errors.Add(response.Result);
        }
      }

      ManageCreateOrUpdateUserResponse(errors);
    }

    private void ManageNotReceivedCreateOrUpdateUserResponse()
    {
      ManageCreateOrUpdateUserResponse(new[] { LocalErrorCodeMapper.ServerNotReachable });
    }

    private void ManageReceivedDeleteUserResponse(IList<GenericResponse> responseList)
    {
      var errors = new List<int>();

      foreach (var response in responseList)
      {
        //mostro solo le risposte con errore
        if (!response.IsOk)
        {
          errors.Add(response.Result);
        }
      }

      ManageCreateOrUpdateUserResponse(errors);
    }

    private void ManageNotReceivedDeleteUserResponse()
    {
      ManageCreateOrUpdateUserResponse(new[] { LocalErrorCodeMapper.ServerNotReachable });
    }

    #endregion

    #region XmlClient / CommandManager

    #region Richieste

    private const string CommandGetAllUsers = "GetAllUsers";
    private const string CommandGetAllUserProfile = "GetAllUserProfile";
    private const string CommandCreateUser = "CreateUser";
    private const string CommandUpdateUser = "UpdateUser";
    private const string CommandDeleteUser = "DeleteUser";

    private void Sendcommand_GetAllUserProfile()
    {
      var parameters = new List<string>();
      var session = _authenticationManager.Session;

      //se la sessione è nulla oppure sono cassioli, non filtro (quindi prendo tutto) altrimenti prendo il livello dell'utente
      if (session == null || session.User.Username.EqualsIgnoreCase("cassioli"))
      {
        //nessun filtro
      }
      else
      {
        const string hierarchy = "hierarchy";
        int hierarchyLevel = session.User.UserProfileHierarchy;

        parameters.Add(hierarchy);
        parameters.Add(hierarchyLevel.ToString());
      }

      string cmd = $"{XmlCommandPreamble}{CommandGetAllUserProfile}".CreateXmlCommand(parameters);
      SendCommand(cmd);
    }

    private void SendCommand_GetAllUsers()
    {
      var parameters = new List<string>();
      var session = _authenticationManager.Session;

      //se la sessione è nulla oppure sono cassioli, non filtro (quindi prendo tutto) altrimenti prendo il livello dell'utente
      if (session == null || session.User.Username.EqualsIgnoreCase("cassioli"))
      {
        //nessun filtro
      }
      else
      {
        const string hierarchy = "hierarchy";
        int hierarchyLevel = session.User.UserProfileHierarchy;

        parameters.Add(hierarchy);
        parameters.Add(hierarchyLevel.ToString());


        const string username = "username";

        parameters.Add(username);
        parameters.Add(session.User.Username);
      }

      string cmd = $"{XmlCommandPreamble}{CommandGetAllUsers}".CreateXmlCommand(parameters);
      SendCommand(cmd);
    }

    private void SendCommand_CreateOrUpdateUser(User user)
    {
      //se sono in fase di creazione mando un comando, altrimenti l'altro
      string methodName = IsCreation() ? CommandCreateUser : CommandUpdateUser;

      var cmd = $"{XmlCommandPreamble}{methodName}".CreateXmlCommand(user.Username, user.Password, user.Fullname, user.Company, user.Profilecode, user.Blocked);

      SendCommand(cmd);
    }

    private void SendCommand_DeleteUser(string username)
    {
      string cmd = $"{XmlCommandPreamble}{CommandDeleteUser}".CreateXmlCommand(username);
      SendCommand(cmd);
    }

    private void SendCommand(string command)
    {
      //controllo se ho settato la modalità
      if (_authenticationMode == AuthenticationMode.None)
      {
        //throw new Exception($"Set Authentication Mode before start, call method {nameof(Configure)} or  method {nameof(Configure)}");
        return;
      }

      if (_authenticationMode == AuthenticationMode.XmlClient)
      {
        var xmlCmd = _xmlClient.CreateXmlCommand(command, "cassioli");
        _xmlClient.ProcessXmlDoc(xmlCmd, "custom", this);
      }
      else if (_authenticationMode == AuthenticationMode.CommandManager)
      {
        _cmNotifier.sendCommand(command, "custom");
      }
    }

    #endregion

    #region Risposte

    #region CommandManager

    private void NotifyCallback(CommandManagerNotifier.NotifyObject notifyObject)
    {
      var xmlCmd = notifyObject.xmlCommand;
      var xmlCmdRes = notifyObject.iXmlStreamer as XmlCommandResponse;

      ManageResponse(xmlCmd, xmlCmdRes);
    }

    #endregion

    #region XmlClient

    private void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs msgEventArgs)
    {
      var xmlCmd = msgEventArgs.xmlCommand as XmlCommand;
      var xmlCmdRes = msgEventArgs.iXmlStreamer as XmlCommandResponse;

      ManageResponse(xmlCmd, xmlCmdRes);
    }

    private void XmlClientOnException(object sender, ExceptionEventArgs exEventArgs)
    {
      var xmlCmd = exEventArgs.xmlCommand as XmlCommand;
      XmlCommandResponse xmlCmdRes = null;

      ManageResponse(xmlCmd, xmlCmdRes);
    }

    #endregion

    #region ManageResponse

    private void ManageResponse(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      var commandMethodName = xmlCmd.name.TrimPreamble();

      #region GetAllUsers

      if (commandMethodName.EqualsConsiderCase(CommandGetAllUsers))
      {
        InvokeControlAction(this, delegate { Response_GetAllUsers(xmlCmd, xmlCmdRes); });
      }

      #endregion

      #region CommandGetAllUserProfile

      if (commandMethodName.EqualsConsiderCase(CommandGetAllUserProfile))
      {
        InvokeControlAction(this, delegate { Response_GetAllUserProfile(xmlCmd, xmlCmdRes); });
      }

      #endregion

      #region CommandCreateOrUpdateUser

      else if (commandMethodName.EqualsConsiderCase(CommandCreateUser) || commandMethodName.EqualsConsiderCase(CommandUpdateUser))
      {
        InvokeControlAction(this, delegate { Response_CreateOrUpdateUser(xmlCmd, xmlCmdRes); });
      }

      #endregion

      #region CommandDeleteUser

      else if (commandMethodName.EqualsConsiderCase(CommandDeleteUser))
      {
        InvokeControlAction(this, delegate { Response_DeleteUserUser(xmlCmd, xmlCmdRes); });
      }

      #endregion
    }

    #endregion

    #endregion

    #region Gestione Risposte

    private void Response_GetAllUsers(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      if (xmlCmdRes != null)
      {
        //recupero gli utenti
        var users = GetUser(xmlCmdRes);

        ManageReceivedGetAllUsersResponse(users);
      }
      else
      {
        //risposta non ricevuta
        ManageNotReceivedGetAllUsersResponse();
      }
    }

    private void Response_GetAllUserProfile(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      if (xmlCmdRes != null)
      {
        //recupero gli utenti
        var profiles = Profiler.CreateUserProfileCollection(xmlCmdRes);

        ManageReceivedGetAllUserProfilesResponse(profiles);
      }
      else
      {
        //risposta non ricevuta
        ManageNotReceivedGetAllUserProfilesResponse();
      }
    }

    private void Response_CreateOrUpdateUser(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      if (xmlCmdRes != null)
      {
        //recupero la risposta
        var responses = GetGenericResponses(xmlCmdRes);

        ManageReceivedCreateOrUpdateUserResponse(responses);
      }
      else
      {
        //risposta non ricevuta
        ManageNotReceivedCreateOrUpdateUserResponse();
      }
    }

    private void Response_DeleteUserUser(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      if (xmlCmdRes != null)
      {
        //recupero la risposta
        var responses = GetGenericResponses(xmlCmdRes);

        ManageReceivedDeleteUserResponse(responses);
      }
      else
      {
        //risposta non ricevuta
        ManageNotReceivedDeleteUserResponse();
      }
    }

    #endregion

    #endregion

    #region Utilities

    /// <summary>
    /// Contesto attuale del controllo
    /// </summary>
    private static readonly System.Threading.SynchronizationContext SyncContext = System.Threading.SynchronizationContext.Current;

    /// <summary>
    /// Permette di eseguire il metodo <param name="action"/> della classe <param name="cont"/>
    /// invocandolo tramite thread principale così da evitare le eccezioni
    /// </summary>
    /// <typeparam name="T">Tipo della classe invocante</typeparam>
    /// <param name="cont">Classe invocante</param>
    /// <param name="action">Metodo da invocare</param>
    protected static void InvokeControlAction<T>(T cont, Action<T> action)
      where T : Control
    {
      //if (cont.InvokeRequired)
      //{
      //  cont.Invoke(new Action<T, Action<T>>(InvokeControlAction), cont, action);
      //}
      //else
      //{
      //  action(cont);
      //}

      //NOTE la vecchia implementazione prevedeva il classico InvokeRequired ma questo è valido solo se chi istanzia il controllo è una WinForms.
      //NOTE Per rendere compatibile il controllo anche con WPF, devo utilizzare il contesto.
      //NOTE ATTENZIONE: non è necessario utilizzare ISynchronizeInvoke.InvokeRequired o Dispatcher.CheckAccess, il controllo è interno al contesto stesso.

      if (SyncContext == null)
      {
        action(cont);
      }
      else
      {
        SyncContext.Post(o => action(cont), null);
      }
    }

    private static bool IsWpfGuiThread()
    {
      //return System.Windows.Threading.Dispatcher.FromThread(System.Threading.Thread.CurrentThread) != null;
      return SyncContext is System.Windows.Threading.DispatcherSynchronizationContext;
    }

    private static bool IsWinFormsGuiThread()
    {
      return SyncContext is System.Windows.Forms.WindowsFormsSynchronizationContext;
    }

    #endregion

    #region GenericResponse

    private class GenericResponse
    {
      /// <summary>
      /// Riporta il valore di un operazione
      /// </summary>
      public string Code { get; }

      /// <summary>
      /// Riporta il dato dell'operazione
      /// </summary>
      public int Result { get; }

      /// <summary>
      /// Restituisce true se code = "ok"
      /// </summary>
      public bool IsOk => Code.EqualsIgnoreCase("ok");

      public GenericResponse(string code, int result)
      {
        if (string.IsNullOrEmpty(code))
          throw new ArgumentNullException(nameof(code));

        Code = code;
        Result = result;
      }
    }

    private GenericResponse GetGenericResponse(XmlCommandResponse.Item item)
    {
      if (item == null)
        throw new ArgumentNullException(nameof(item));

      var code = item.getFieldVal(1);
      var result = item.getFieldVal(2).ConvertTo<int>();

      return new GenericResponse(code, result);
    }

    private IList<GenericResponse> GetGenericResponses(XmlCommandResponse xmlResp)
    {
      if (xmlResp == null)
        throw new ArgumentNullException(nameof(xmlResp));

      var genericResponses = new List<GenericResponse>();

      foreach (var item in xmlResp.Items)
      {
        var genericResponse = GetGenericResponse(item);

        genericResponses.Add(genericResponse);
      }

      return genericResponses;
    }

    #endregion

    #region User

    private class User
    {
      public string Username { get; }
      public string Password { get; set; } = "";
      public string Fullname { get; }
      public string Company { get; }
      public string Profilecode { get; }
      public int Hierarchy { get; }
      public bool Blocked { get; }

      public User(string username, string fullname, string company, string profilecode, bool blocked)
        : this(username, fullname, company, profilecode, -1, blocked)
      {
      }

      public User(string username, string fullname, string company, string profilecode, int hierarchy, bool blocked)
      {
        Username = username;
        Fullname = fullname;
        Company = company;
        Profilecode = profilecode;
        Hierarchy = hierarchy;
        Blocked = blocked;
      }
    }

    private User GetUser(XmlCommandResponse.Item item)
    {
      if (item == null)
        throw new ArgumentNullException(nameof(item));

      var i = 1;

      var username = item.getFieldVal(i++);
      var fullname = item.getFieldVal(i++);
      var company = item.getFieldVal(i++);
      var profilecode = item.getFieldVal(i++);
      var hierarchy = item.getFieldVal(i++).ConvertTo<int>();
      var blocked = item.getFieldVal(i++).ConvertTo<bool>();

      return new User(username, fullname, company, profilecode, hierarchy, blocked);
    }

    private IList<User> GetUser(XmlCommandResponse xmlResp)
    {
      if (xmlResp == null)
        throw new ArgumentNullException(nameof(xmlResp));

      var users = new List<User>();

      foreach (var item in xmlResp.Items)
      {
        var user = GetUser(item);

        users.Add(user);
      }

      return users;
    }

    #endregion

    #region ComboboxItem

    private class ComboboxItem
    {
      public string Text { get; }
      public string TranslatedText { get; set; }
      public int Value { get; }

      public ComboboxItem(string text, int value)
      {
        Text = text;
        Value = value;
      }

      public override string ToString()
      {
        return TranslatedText;
      }
    }

    #endregion
  }
}