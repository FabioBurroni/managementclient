﻿using Authentication.Controls;

namespace Authentication.GUI.Management
{
  partial class ManageSessionsUserControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			this.coloredBorderPanel = new Authentication.Controls.ColoredBorderPanel();
			this.panelMain = new System.Windows.Forms.Panel();
			this.dgvSessions = new System.Windows.Forms.DataGridView();
			this.dataGridViewColumnSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.dataGridViewColumnUsername = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnProfile = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnClient = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnSource = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnIPAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnPortNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnLastActivity = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnRootToken = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnChildToken = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnPositions = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.panelHeaderFilters = new System.Windows.Forms.Panel();
			this.panelFiltersAndRefresh = new System.Windows.Forms.Panel();
			this.panelFilters = new System.Windows.Forms.Panel();
			this.flowLayoutPanelFilters = new System.Windows.Forms.FlowLayoutPanel();
			this.panelUsernameFilter = new System.Windows.Forms.Panel();
			this.textBoxUsernameFilter = new System.Windows.Forms.TextBox();
			this.labelTrUsernameContains = new System.Windows.Forms.Label();
			this.panelProfileFilter = new System.Windows.Forms.Panel();
			this.textBoxProfileFilter = new System.Windows.Forms.TextBox();
			this.labelTrProfileContains = new System.Windows.Forms.Label();
			this.panelClientFilter = new System.Windows.Forms.Panel();
			this.textBoxClientFilter = new System.Windows.Forms.TextBox();
			this.labelTrClientContains = new System.Windows.Forms.Label();
			this.panelSourceFilter = new System.Windows.Forms.Panel();
			this.textBoxSourceFilter = new System.Windows.Forms.TextBox();
			this.labelTrSourceContains = new System.Windows.Forms.Label();
			this.panelPositionsFilter = new System.Windows.Forms.Panel();
			this.textBoxPositionsFilter = new System.Windows.Forms.TextBox();
			this.labelPositionsContains = new System.Windows.Forms.Label();
			this.panelResetFilters = new System.Windows.Forms.Panel();
			this.btnResetFilters = new Authentication.Controls.CtrlButton();
			this.panelRefresh = new System.Windows.Forms.Panel();
			this.coloredBorderPanelAutoRefresh = new Authentication.Controls.ColoredBorderPanel();
			this.labelTrAutoRefresh = new System.Windows.Forms.Label();
			this.btnRefresh = new Authentication.Controls.CtrlButton();
			this.labelCountdown = new System.Windows.Forms.Label();
			this.labelTrFilterUsers = new System.Windows.Forms.Label();
			this.panelTop = new System.Windows.Forms.Panel();
			this.lblTrSessions = new System.Windows.Forms.Label();
			this.panelBottom = new System.Windows.Forms.Panel();
			this.pnlMessage = new Authentication.Controls.CtrlCustomPanel();
			this.textBoxError = new System.Windows.Forms.TextBox();
			this.panelInfo = new System.Windows.Forms.Panel();
			this.panelLogout = new System.Windows.Forms.Panel();
			this.btnLogoutSessions = new Authentication.Controls.CtrlButton();
			this.coloredBorderPanel.SuspendLayout();
			this.panelMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvSessions)).BeginInit();
			this.panelHeaderFilters.SuspendLayout();
			this.panelFiltersAndRefresh.SuspendLayout();
			this.panelFilters.SuspendLayout();
			this.flowLayoutPanelFilters.SuspendLayout();
			this.panelUsernameFilter.SuspendLayout();
			this.panelProfileFilter.SuspendLayout();
			this.panelClientFilter.SuspendLayout();
			this.panelSourceFilter.SuspendLayout();
			this.panelPositionsFilter.SuspendLayout();
			this.panelResetFilters.SuspendLayout();
			this.panelRefresh.SuspendLayout();
			this.coloredBorderPanelAutoRefresh.SuspendLayout();
			this.panelTop.SuspendLayout();
			this.panelBottom.SuspendLayout();
			this.pnlMessage.SuspendLayout();
			this.panelLogout.SuspendLayout();
			this.SuspendLayout();
			// 
			// coloredBorderPanel
			// 
			this.coloredBorderPanel.BorderColor = System.Drawing.Color.SkyBlue;
			this.coloredBorderPanel.BorderWidth = 2;
			this.coloredBorderPanel.Controls.Add(this.panelMain);
			this.coloredBorderPanel.Controls.Add(this.panelTop);
			this.coloredBorderPanel.Controls.Add(this.panelBottom);
			this.coloredBorderPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.coloredBorderPanel.Location = new System.Drawing.Point(0, 0);
			this.coloredBorderPanel.Name = "coloredBorderPanel";
			this.coloredBorderPanel.Size = new System.Drawing.Size(1230, 600);
			this.coloredBorderPanel.TabIndex = 16;
			// 
			// panelMain
			// 
			this.panelMain.BackColor = System.Drawing.SystemColors.Control;
			this.panelMain.Controls.Add(this.dgvSessions);
			this.panelMain.Controls.Add(this.panelHeaderFilters);
			this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelMain.Location = new System.Drawing.Point(2, 44);
			this.panelMain.Name = "panelMain";
			this.panelMain.Size = new System.Drawing.Size(1226, 484);
			this.panelMain.TabIndex = 2;
			// 
			// dgvSessions
			// 
			this.dgvSessions.AllowUserToAddRows = false;
			this.dgvSessions.AllowUserToDeleteRows = false;
			this.dgvSessions.AllowUserToOrderColumns = true;
			this.dgvSessions.AllowUserToResizeRows = false;
			this.dgvSessions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dgvSessions.BackgroundColor = System.Drawing.Color.White;
			this.dgvSessions.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dgvSessions.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvSessions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dgvSessions.ColumnHeadersHeight = 50;
			this.dgvSessions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.dgvSessions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewColumnSelect,
            this.dataGridViewColumnUsername,
            this.dataGridViewColumnProfile,
            this.dataGridViewColumnClient,
            this.dataGridViewColumnSource,
            this.dataGridViewColumnIPAddress,
            this.dataGridViewColumnPortNumber,
            this.dataGridViewColumnLastActivity,
            this.dataGridViewColumnRootToken,
            this.dataGridViewColumnChildToken,
            this.dataGridViewColumnPositions});
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dgvSessions.DefaultCellStyle = dataGridViewCellStyle2;
			this.dgvSessions.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvSessions.Location = new System.Drawing.Point(0, 146);
			this.dgvSessions.Name = "dgvSessions";
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvSessions.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.dgvSessions.RowHeadersVisible = false;
			this.dgvSessions.RowHeadersWidth = 50;
			this.dgvSessions.RowTemplate.Height = 28;
			this.dgvSessions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvSessions.Size = new System.Drawing.Size(1226, 338);
			this.dgvSessions.TabIndex = 13;
			this.dgvSessions.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvSessions_CurrentCellDirtyStateChanged);
			this.dgvSessions.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvSessions_KeyPress);
			// 
			// dataGridViewColumnSelect
			// 
			this.dataGridViewColumnSelect.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.dataGridViewColumnSelect.DataPropertyName = "Select";
			this.dataGridViewColumnSelect.HeaderText = "";
			this.dataGridViewColumnSelect.Name = "dataGridViewColumnSelect";
			this.dataGridViewColumnSelect.Width = 40;
			// 
			// dataGridViewColumnUsername
			// 
			this.dataGridViewColumnUsername.DataPropertyName = "Username";
			this.dataGridViewColumnUsername.HeaderText = "Username";
			this.dataGridViewColumnUsername.Name = "dataGridViewColumnUsername";
			this.dataGridViewColumnUsername.ReadOnly = true;
			// 
			// dataGridViewColumnProfile
			// 
			this.dataGridViewColumnProfile.DataPropertyName = "Profile";
			this.dataGridViewColumnProfile.HeaderText = "Profile";
			this.dataGridViewColumnProfile.Name = "dataGridViewColumnProfile";
			this.dataGridViewColumnProfile.ReadOnly = true;
			// 
			// dataGridViewColumnClient
			// 
			this.dataGridViewColumnClient.DataPropertyName = "Client";
			this.dataGridViewColumnClient.HeaderText = "Client";
			this.dataGridViewColumnClient.Name = "dataGridViewColumnClient";
			this.dataGridViewColumnClient.ReadOnly = true;
			// 
			// dataGridViewColumnSource
			// 
			this.dataGridViewColumnSource.DataPropertyName = "Source";
			this.dataGridViewColumnSource.HeaderText = "Source";
			this.dataGridViewColumnSource.Name = "dataGridViewColumnSource";
			this.dataGridViewColumnSource.ReadOnly = true;
			// 
			// dataGridViewColumnIPAddress
			// 
			this.dataGridViewColumnIPAddress.DataPropertyName = "IPAddress";
			this.dataGridViewColumnIPAddress.HeaderText = "IP Address";
			this.dataGridViewColumnIPAddress.Name = "dataGridViewColumnIPAddress";
			this.dataGridViewColumnIPAddress.ReadOnly = true;
			// 
			// dataGridViewColumnPortNumber
			// 
			this.dataGridViewColumnPortNumber.DataPropertyName = "PortNumber";
			this.dataGridViewColumnPortNumber.HeaderText = "Port Number";
			this.dataGridViewColumnPortNumber.Name = "dataGridViewColumnPortNumber";
			this.dataGridViewColumnPortNumber.ReadOnly = true;
			// 
			// dataGridViewColumnLastActivity
			// 
			this.dataGridViewColumnLastActivity.DataPropertyName = "LastActivity";
			this.dataGridViewColumnLastActivity.HeaderText = "Last Activity";
			this.dataGridViewColumnLastActivity.Name = "dataGridViewColumnLastActivity";
			this.dataGridViewColumnLastActivity.ReadOnly = true;
			// 
			// dataGridViewColumnRootToken
			// 
			this.dataGridViewColumnRootToken.DataPropertyName = "RootToken";
			this.dataGridViewColumnRootToken.HeaderText = "Root Token";
			this.dataGridViewColumnRootToken.Name = "dataGridViewColumnRootToken";
			this.dataGridViewColumnRootToken.ReadOnly = true;
			// 
			// dataGridViewColumnChildToken
			// 
			this.dataGridViewColumnChildToken.DataPropertyName = "ChildToken";
			this.dataGridViewColumnChildToken.HeaderText = "Child Token";
			this.dataGridViewColumnChildToken.Name = "dataGridViewColumnChildToken";
			this.dataGridViewColumnChildToken.ReadOnly = true;
			// 
			// dataGridViewColumnPositions
			// 
			this.dataGridViewColumnPositions.DataPropertyName = "Positions";
			this.dataGridViewColumnPositions.HeaderText = "Positions";
			this.dataGridViewColumnPositions.Name = "dataGridViewColumnPositions";
			this.dataGridViewColumnPositions.ReadOnly = true;
			// 
			// panelHeaderFilters
			// 
			this.panelHeaderFilters.Controls.Add(this.panelFiltersAndRefresh);
			this.panelHeaderFilters.Controls.Add(this.labelTrFilterUsers);
			this.panelHeaderFilters.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelHeaderFilters.Location = new System.Drawing.Point(0, 0);
			this.panelHeaderFilters.Name = "panelHeaderFilters";
			this.panelHeaderFilters.Size = new System.Drawing.Size(1226, 146);
			this.panelHeaderFilters.TabIndex = 15;
			// 
			// panelFiltersAndRefresh
			// 
			this.panelFiltersAndRefresh.Controls.Add(this.panelFilters);
			this.panelFiltersAndRefresh.Controls.Add(this.panelRefresh);
			this.panelFiltersAndRefresh.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelFiltersAndRefresh.Location = new System.Drawing.Point(0, 25);
			this.panelFiltersAndRefresh.Name = "panelFiltersAndRefresh";
			this.panelFiltersAndRefresh.Size = new System.Drawing.Size(1226, 121);
			this.panelFiltersAndRefresh.TabIndex = 16;
			// 
			// panelFilters
			// 
			this.panelFilters.BackColor = System.Drawing.SystemColors.Control;
			this.panelFilters.Controls.Add(this.flowLayoutPanelFilters);
			this.panelFilters.Controls.Add(this.panelResetFilters);
			this.panelFilters.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelFilters.Location = new System.Drawing.Point(0, 0);
			this.panelFilters.Name = "panelFilters";
			this.panelFilters.Size = new System.Drawing.Size(1050, 121);
			this.panelFilters.TabIndex = 14;
			// 
			// flowLayoutPanelFilters
			// 
			this.flowLayoutPanelFilters.AutoScroll = true;
			this.flowLayoutPanelFilters.Controls.Add(this.panelUsernameFilter);
			this.flowLayoutPanelFilters.Controls.Add(this.panelProfileFilter);
			this.flowLayoutPanelFilters.Controls.Add(this.panelClientFilter);
			this.flowLayoutPanelFilters.Controls.Add(this.panelSourceFilter);
			this.flowLayoutPanelFilters.Controls.Add(this.panelPositionsFilter);
			this.flowLayoutPanelFilters.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanelFilters.Location = new System.Drawing.Point(0, 0);
			this.flowLayoutPanelFilters.Name = "flowLayoutPanelFilters";
			this.flowLayoutPanelFilters.Size = new System.Drawing.Size(879, 121);
			this.flowLayoutPanelFilters.TabIndex = 30;
			// 
			// panelUsernameFilter
			// 
			this.panelUsernameFilter.Controls.Add(this.textBoxUsernameFilter);
			this.panelUsernameFilter.Controls.Add(this.labelTrUsernameContains);
			this.panelUsernameFilter.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelUsernameFilter.Location = new System.Drawing.Point(3, 3);
			this.panelUsernameFilter.Name = "panelUsernameFilter";
			this.panelUsernameFilter.Size = new System.Drawing.Size(168, 52);
			this.panelUsernameFilter.TabIndex = 1;
			// 
			// textBoxUsernameFilter
			// 
			this.textBoxUsernameFilter.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxUsernameFilter.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxUsernameFilter.Location = new System.Drawing.Point(0, 21);
			this.textBoxUsernameFilter.Name = "textBoxUsernameFilter";
			this.textBoxUsernameFilter.Size = new System.Drawing.Size(168, 25);
			this.textBoxUsernameFilter.TabIndex = 4;
			this.textBoxUsernameFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
			// 
			// labelTrUsernameContains
			// 
			this.labelTrUsernameContains.AutoSize = true;
			this.labelTrUsernameContains.Dock = System.Windows.Forms.DockStyle.Top;
			this.labelTrUsernameContains.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelTrUsernameContains.Location = new System.Drawing.Point(0, 0);
			this.labelTrUsernameContains.Name = "labelTrUsernameContains";
			this.labelTrUsernameContains.Size = new System.Drawing.Size(150, 21);
			this.labelTrUsernameContains.TabIndex = 1;
			this.labelTrUsernameContains.Text = "Username Contains";
			// 
			// panelProfileFilter
			// 
			this.panelProfileFilter.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.panelProfileFilter.Controls.Add(this.textBoxProfileFilter);
			this.panelProfileFilter.Controls.Add(this.labelTrProfileContains);
			this.panelProfileFilter.Location = new System.Drawing.Point(177, 3);
			this.panelProfileFilter.Name = "panelProfileFilter";
			this.panelProfileFilter.Size = new System.Drawing.Size(168, 52);
			this.panelProfileFilter.TabIndex = 5;
			// 
			// textBoxProfileFilter
			// 
			this.textBoxProfileFilter.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxProfileFilter.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxProfileFilter.Location = new System.Drawing.Point(0, 21);
			this.textBoxProfileFilter.Name = "textBoxProfileFilter";
			this.textBoxProfileFilter.Size = new System.Drawing.Size(168, 25);
			this.textBoxProfileFilter.TabIndex = 20;
			this.textBoxProfileFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
			// 
			// labelTrProfileContains
			// 
			this.labelTrProfileContains.AutoSize = true;
			this.labelTrProfileContains.Dock = System.Windows.Forms.DockStyle.Top;
			this.labelTrProfileContains.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelTrProfileContains.Location = new System.Drawing.Point(0, 0);
			this.labelTrProfileContains.Name = "labelTrProfileContains";
			this.labelTrProfileContains.Size = new System.Drawing.Size(125, 21);
			this.labelTrProfileContains.TabIndex = 15;
			this.labelTrProfileContains.Text = "Profile Contains";
			this.labelTrProfileContains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelClientFilter
			// 
			this.panelClientFilter.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.panelClientFilter.Controls.Add(this.textBoxClientFilter);
			this.panelClientFilter.Controls.Add(this.labelTrClientContains);
			this.panelClientFilter.Location = new System.Drawing.Point(351, 3);
			this.panelClientFilter.Name = "panelClientFilter";
			this.panelClientFilter.Size = new System.Drawing.Size(168, 52);
			this.panelClientFilter.TabIndex = 21;
			// 
			// textBoxClientFilter
			// 
			this.textBoxClientFilter.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxClientFilter.Font = new System.Drawing.Font("Segoe UI", 9.75F);
			this.textBoxClientFilter.Location = new System.Drawing.Point(0, 21);
			this.textBoxClientFilter.Name = "textBoxClientFilter";
			this.textBoxClientFilter.Size = new System.Drawing.Size(168, 25);
			this.textBoxClientFilter.TabIndex = 20;
			this.textBoxClientFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
			// 
			// labelTrClientContains
			// 
			this.labelTrClientContains.AutoSize = true;
			this.labelTrClientContains.Dock = System.Windows.Forms.DockStyle.Top;
			this.labelTrClientContains.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelTrClientContains.Location = new System.Drawing.Point(0, 0);
			this.labelTrClientContains.Name = "labelTrClientContains";
			this.labelTrClientContains.Size = new System.Drawing.Size(119, 21);
			this.labelTrClientContains.TabIndex = 15;
			this.labelTrClientContains.Text = "Client Contains";
			this.labelTrClientContains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelSourceFilter
			// 
			this.panelSourceFilter.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.panelSourceFilter.Controls.Add(this.textBoxSourceFilter);
			this.panelSourceFilter.Controls.Add(this.labelTrSourceContains);
			this.panelSourceFilter.Location = new System.Drawing.Point(525, 3);
			this.panelSourceFilter.Name = "panelSourceFilter";
			this.panelSourceFilter.Size = new System.Drawing.Size(168, 52);
			this.panelSourceFilter.TabIndex = 22;
			// 
			// textBoxSourceFilter
			// 
			this.textBoxSourceFilter.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxSourceFilter.Font = new System.Drawing.Font("Segoe UI", 9.75F);
			this.textBoxSourceFilter.Location = new System.Drawing.Point(0, 21);
			this.textBoxSourceFilter.Name = "textBoxSourceFilter";
			this.textBoxSourceFilter.Size = new System.Drawing.Size(168, 25);
			this.textBoxSourceFilter.TabIndex = 20;
			this.textBoxSourceFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
			// 
			// labelTrSourceContains
			// 
			this.labelTrSourceContains.AutoSize = true;
			this.labelTrSourceContains.Dock = System.Windows.Forms.DockStyle.Top;
			this.labelTrSourceContains.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelTrSourceContains.Location = new System.Drawing.Point(0, 0);
			this.labelTrSourceContains.Name = "labelTrSourceContains";
			this.labelTrSourceContains.Size = new System.Drawing.Size(128, 21);
			this.labelTrSourceContains.TabIndex = 15;
			this.labelTrSourceContains.Text = "Source Contains";
			this.labelTrSourceContains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelPositionsFilter
			// 
			this.panelPositionsFilter.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.panelPositionsFilter.Controls.Add(this.textBoxPositionsFilter);
			this.panelPositionsFilter.Controls.Add(this.labelPositionsContains);
			this.panelPositionsFilter.Location = new System.Drawing.Point(699, 3);
			this.panelPositionsFilter.Name = "panelPositionsFilter";
			this.panelPositionsFilter.Size = new System.Drawing.Size(168, 52);
			this.panelPositionsFilter.TabIndex = 23;
			// 
			// textBoxPositionsFilter
			// 
			this.textBoxPositionsFilter.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxPositionsFilter.Font = new System.Drawing.Font("Segoe UI", 9.75F);
			this.textBoxPositionsFilter.Location = new System.Drawing.Point(0, 21);
			this.textBoxPositionsFilter.Name = "textBoxPositionsFilter";
			this.textBoxPositionsFilter.Size = new System.Drawing.Size(168, 25);
			this.textBoxPositionsFilter.TabIndex = 20;
			this.textBoxPositionsFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
			// 
			// labelPositionsContains
			// 
			this.labelPositionsContains.AutoSize = true;
			this.labelPositionsContains.Dock = System.Windows.Forms.DockStyle.Top;
			this.labelPositionsContains.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelPositionsContains.Location = new System.Drawing.Point(0, 0);
			this.labelPositionsContains.Name = "labelPositionsContains";
			this.labelPositionsContains.Size = new System.Drawing.Size(142, 21);
			this.labelPositionsContains.TabIndex = 15;
			this.labelPositionsContains.Text = "Positions Contains";
			this.labelPositionsContains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelResetFilters
			// 
			this.panelResetFilters.Controls.Add(this.btnResetFilters);
			this.panelResetFilters.Dock = System.Windows.Forms.DockStyle.Right;
			this.panelResetFilters.Location = new System.Drawing.Point(879, 0);
			this.panelResetFilters.Name = "panelResetFilters";
			this.panelResetFilters.Size = new System.Drawing.Size(171, 121);
			this.panelResetFilters.TabIndex = 24;
			// 
			// btnResetFilters
			// 
			this.btnResetFilters.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.btnResetFilters.BackColor1 = System.Drawing.Color.Coral;
			this.btnResetFilters.BackColor2 = System.Drawing.Color.Orange;
			this.btnResetFilters.ButtonText = "Reset Filters";
			this.btnResetFilters.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnResetFilters.Location = new System.Drawing.Point(10, 37);
			this.btnResetFilters.Name = "btnResetFilters";
			this.btnResetFilters.PressColor1 = System.Drawing.Color.DarkOrange;
			this.btnResetFilters.PressColor2 = System.Drawing.Color.DarkOrange;
			this.btnResetFilters.Size = new System.Drawing.Size(150, 40);
			this.btnResetFilters.TabIndex = 24;
			this.btnResetFilters.TextColor = System.Drawing.Color.White;
			this.btnResetFilters.Click += new System.EventHandler(this.btnResetFilters_Click);
			// 
			// panelRefresh
			// 
			this.panelRefresh.Controls.Add(this.coloredBorderPanelAutoRefresh);
			this.panelRefresh.Dock = System.Windows.Forms.DockStyle.Right;
			this.panelRefresh.Location = new System.Drawing.Point(1050, 0);
			this.panelRefresh.Name = "panelRefresh";
			this.panelRefresh.Size = new System.Drawing.Size(176, 121);
			this.panelRefresh.TabIndex = 24;
			// 
			// coloredBorderPanelAutoRefresh
			// 
			this.coloredBorderPanelAutoRefresh.BorderColor = System.Drawing.Color.RoyalBlue;
			this.coloredBorderPanelAutoRefresh.BorderWidth = 2;
			this.coloredBorderPanelAutoRefresh.Controls.Add(this.labelTrAutoRefresh);
			this.coloredBorderPanelAutoRefresh.Controls.Add(this.btnRefresh);
			this.coloredBorderPanelAutoRefresh.Controls.Add(this.labelCountdown);
			this.coloredBorderPanelAutoRefresh.Dock = System.Windows.Forms.DockStyle.Fill;
			this.coloredBorderPanelAutoRefresh.Location = new System.Drawing.Point(0, 0);
			this.coloredBorderPanelAutoRefresh.Name = "coloredBorderPanelAutoRefresh";
			this.coloredBorderPanelAutoRefresh.Size = new System.Drawing.Size(176, 121);
			this.coloredBorderPanelAutoRefresh.TabIndex = 25;
			// 
			// labelTrAutoRefresh
			// 
			this.labelTrAutoRefresh.AutoSize = true;
			this.labelTrAutoRefresh.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelTrAutoRefresh.Location = new System.Drawing.Point(8, 3);
			this.labelTrAutoRefresh.Name = "labelTrAutoRefresh";
			this.labelTrAutoRefresh.Size = new System.Drawing.Size(106, 21);
			this.labelTrAutoRefresh.TabIndex = 27;
			this.labelTrAutoRefresh.Text = "Auto Refresh";
			// 
			// btnRefresh
			// 
			this.btnRefresh.BackColor1 = System.Drawing.Color.RoyalBlue;
			this.btnRefresh.BackColor2 = System.Drawing.SystemColors.Highlight;
			this.btnRefresh.ButtonText = "Refresh";
			this.btnRefresh.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnRefresh.Location = new System.Drawing.Point(43, 37);
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.PressColor1 = System.Drawing.SystemColors.HotTrack;
			this.btnRefresh.PressColor2 = System.Drawing.SystemColors.MenuHighlight;
			this.btnRefresh.Size = new System.Drawing.Size(92, 57);
			this.btnRefresh.TabIndex = 3;
			this.btnRefresh.TextColor = System.Drawing.Color.White;
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
			// 
			// labelCountdown
			// 
			this.labelCountdown.AutoSize = true;
			this.labelCountdown.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelCountdown.Location = new System.Drawing.Point(130, 3);
			this.labelCountdown.Name = "labelCountdown";
			this.labelCountdown.Size = new System.Drawing.Size(28, 21);
			this.labelCountdown.TabIndex = 28;
			this.labelCountdown.Text = "60";
			// 
			// labelTrFilterUsers
			// 
			this.labelTrFilterUsers.AutoSize = true;
			this.labelTrFilterUsers.Dock = System.Windows.Forms.DockStyle.Top;
			this.labelTrFilterUsers.Font = new System.Drawing.Font("Segoe UI", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelTrFilterUsers.ForeColor = System.Drawing.Color.DarkGreen;
			this.labelTrFilterUsers.Location = new System.Drawing.Point(0, 0);
			this.labelTrFilterUsers.Name = "labelTrFilterUsers";
			this.labelTrFilterUsers.Size = new System.Drawing.Size(136, 25);
			this.labelTrFilterUsers.TabIndex = 0;
			this.labelTrFilterUsers.Text = "Filter Sessions";
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.Control;
			this.panelTop.Controls.Add(this.lblTrSessions);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(2, 2);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(1226, 42);
			this.panelTop.TabIndex = 16;
			// 
			// lblTrSessions
			// 
			this.lblTrSessions.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTrSessions.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTrSessions.ForeColor = System.Drawing.Color.DodgerBlue;
			this.lblTrSessions.Location = new System.Drawing.Point(0, 0);
			this.lblTrSessions.Name = "lblTrSessions";
			this.lblTrSessions.Size = new System.Drawing.Size(1226, 42);
			this.lblTrSessions.TabIndex = 0;
			this.lblTrSessions.Text = "Sessions";
			this.lblTrSessions.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelBottom
			// 
			this.panelBottom.Controls.Add(this.pnlMessage);
			this.panelBottom.Controls.Add(this.panelLogout);
			this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelBottom.Location = new System.Drawing.Point(2, 528);
			this.panelBottom.Name = "panelBottom";
			this.panelBottom.Size = new System.Drawing.Size(1226, 70);
			this.panelBottom.TabIndex = 30;
			// 
			// pnlMessage
			// 
			this.pnlMessage.BorderColor = System.Drawing.Color.DodgerBlue;
			this.pnlMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnlMessage.BorderWidth = 2;
			this.pnlMessage.Controls.Add(this.textBoxError);
			this.pnlMessage.Controls.Add(this.panelInfo);
			this.pnlMessage.Curvature = 7;
			this.pnlMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlMessage.Location = new System.Drawing.Point(200, 0);
			this.pnlMessage.Name = "pnlMessage";
			this.pnlMessage.Padding = new System.Windows.Forms.Padding(6);
			this.pnlMessage.Size = new System.Drawing.Size(1026, 70);
			this.pnlMessage.TabIndex = 10;
			this.pnlMessage.Visible = false;
			// 
			// textBoxError
			// 
			this.textBoxError.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxError.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxError.ForeColor = System.Drawing.Color.Red;
			this.textBoxError.Location = new System.Drawing.Point(75, 6);
			this.textBoxError.Multiline = true;
			this.textBoxError.Name = "textBoxError";
			this.textBoxError.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBoxError.Size = new System.Drawing.Size(945, 58);
			this.textBoxError.TabIndex = 1;
			this.textBoxError.Text = "Error";
			// 
			// panelInfo
			// 
			this.panelInfo.BackgroundImage = global::Authentication.Properties.Resources.Warning;
			this.panelInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.panelInfo.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelInfo.Location = new System.Drawing.Point(6, 6);
			this.panelInfo.Name = "panelInfo";
			this.panelInfo.Size = new System.Drawing.Size(69, 58);
			this.panelInfo.TabIndex = 0;
			// 
			// panelLogout
			// 
			this.panelLogout.Controls.Add(this.btnLogoutSessions);
			this.panelLogout.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelLogout.Location = new System.Drawing.Point(0, 0);
			this.panelLogout.Name = "panelLogout";
			this.panelLogout.Size = new System.Drawing.Size(200, 70);
			this.panelLogout.TabIndex = 11;
			// 
			// btnLogoutSessions
			// 
			this.btnLogoutSessions.BackColor1 = System.Drawing.Color.IndianRed;
			this.btnLogoutSessions.BackColor2 = System.Drawing.Color.Red;
			this.btnLogoutSessions.ButtonText = "Logout Sessions";
			this.btnLogoutSessions.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnLogoutSessions.Location = new System.Drawing.Point(37, 10);
			this.btnLogoutSessions.Name = "btnLogoutSessions";
			this.btnLogoutSessions.PressColor1 = System.Drawing.Color.Maroon;
			this.btnLogoutSessions.PressColor2 = System.Drawing.Color.DarkRed;
			this.btnLogoutSessions.Size = new System.Drawing.Size(131, 46);
			this.btnLogoutSessions.TabIndex = 29;
			this.btnLogoutSessions.TextColor = System.Drawing.Color.White;
			this.btnLogoutSessions.Click += new System.EventHandler(this.btnLogoutSessions_Click);
			// 
			// ManageSessionsUserControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Transparent;
			this.Controls.Add(this.coloredBorderPanel);
			this.Name = "ManageSessionsUserControl";
			this.Size = new System.Drawing.Size(1230, 600);
			this.coloredBorderPanel.ResumeLayout(false);
			this.panelMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvSessions)).EndInit();
			this.panelHeaderFilters.ResumeLayout(false);
			this.panelHeaderFilters.PerformLayout();
			this.panelFiltersAndRefresh.ResumeLayout(false);
			this.panelFilters.ResumeLayout(false);
			this.flowLayoutPanelFilters.ResumeLayout(false);
			this.panelUsernameFilter.ResumeLayout(false);
			this.panelUsernameFilter.PerformLayout();
			this.panelProfileFilter.ResumeLayout(false);
			this.panelProfileFilter.PerformLayout();
			this.panelClientFilter.ResumeLayout(false);
			this.panelClientFilter.PerformLayout();
			this.panelSourceFilter.ResumeLayout(false);
			this.panelSourceFilter.PerformLayout();
			this.panelPositionsFilter.ResumeLayout(false);
			this.panelPositionsFilter.PerformLayout();
			this.panelResetFilters.ResumeLayout(false);
			this.panelRefresh.ResumeLayout(false);
			this.coloredBorderPanelAutoRefresh.ResumeLayout(false);
			this.coloredBorderPanelAutoRefresh.PerformLayout();
			this.panelTop.ResumeLayout(false);
			this.panelBottom.ResumeLayout(false);
			this.pnlMessage.ResumeLayout(false);
			this.pnlMessage.PerformLayout();
			this.panelLogout.ResumeLayout(false);
			this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panelMain;
    private System.Windows.Forms.DataGridView dgvSessions;
    private System.Windows.Forms.Label lblTrSessions;
    private CtrlButton btnRefresh;
    private System.Windows.Forms.Panel panelFilters;
    private System.Windows.Forms.TextBox textBoxUsernameFilter;
    private System.Windows.Forms.Label labelTrUsernameContains;
    private System.Windows.Forms.Label labelTrFilterUsers;
    private ColoredBorderPanel coloredBorderPanel;
    private System.Windows.Forms.Panel panelInfo;
    private CtrlCustomPanel pnlMessage;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnPositions;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnChildToken;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnRootToken;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnLastActivity;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnPortNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnIPAddress;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnSource;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnClient;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnProfile;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewColumnUsername;
    private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewColumnSelect;
    private System.Windows.Forms.Label labelTrProfileContains;
    private CtrlButton btnResetFilters;
    private System.Windows.Forms.TextBox textBoxProfileFilter;
    private System.Windows.Forms.Label labelTrAutoRefresh;
    private System.Windows.Forms.Label labelCountdown;
    private System.Windows.Forms.Panel panelBottom;
    private System.Windows.Forms.Panel panelLogout;
    private CtrlButton btnLogoutSessions;
    private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelFilters;
    private System.Windows.Forms.Panel panelUsernameFilter;
    private System.Windows.Forms.Panel panelProfileFilter;
    private System.Windows.Forms.Panel panelClientFilter;
    private System.Windows.Forms.TextBox textBoxClientFilter;
    private System.Windows.Forms.Label labelTrClientContains;
    private System.Windows.Forms.Panel panelSourceFilter;
    private System.Windows.Forms.TextBox textBoxSourceFilter;
    private System.Windows.Forms.Label labelTrSourceContains;
    private System.Windows.Forms.Panel panelPositionsFilter;
    private System.Windows.Forms.TextBox textBoxPositionsFilter;
    private System.Windows.Forms.Label labelPositionsContains;
    private System.Windows.Forms.Panel panelHeaderFilters;
    private System.Windows.Forms.Panel panelFiltersAndRefresh;
    private System.Windows.Forms.TextBox textBoxError;
    private System.Windows.Forms.Panel panelTop;
    private System.Windows.Forms.Panel panelResetFilters;
    private System.Windows.Forms.Panel panelRefresh;
    private ColoredBorderPanel coloredBorderPanelAutoRefresh;
  }
}
