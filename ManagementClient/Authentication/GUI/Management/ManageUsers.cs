﻿using Authentication.Collections;
using Authentication.Extensions;
using Authentication.Profiling;
using Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.Extensions;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlClient.Event;
using XmlCommunicationManager.XmlClient.Manager;

namespace Authentication.GUI.Management
{
  public partial class ManageUsers
  {
    #region eventi aggiornamento

    public delegate void OnUsersUpdatedHandler();

    public event OnUsersUpdatedHandler OnUsersUpdated;

    public delegate void OnUserProfilesUpdatedHandler();

    public event OnUserProfilesUpdatedHandler OnUserProfilesUpdated;

    public delegate void OnNewMessageToShowHandler(string message, int type);

    public event OnNewMessageToShowHandler OnNewMessageToShow;

    #endregion eventi aggiornamento

    #region Fields

    private const string DefaultEmptyPassword = "**********";

    private string _company = "Customer";

    private readonly AuthenticationManager _authenticationManager = AuthenticationManager.Instance;

    private AuthenticationMode _authenticationMode;

    private string _xmlCommandPreamble;

    private CommandManager _commandManager;
    private CommandManagerNotifier _cmNotifier;

    private IXmlClient _xmlClient;

    private readonly object _locker = new object();

    public List<UserProfile> UserProfiles { get; set; } = new List<UserProfile>();
    public List<User> Users { get; set; } = new List<User>();

    #endregion Fields

    #region Constructor

    public ManageUsers(IXmlClient xmlClient, string company)
    {
      Configure(xmlClient, company);

      Reload();
    }

    #endregion Constructor

    #region Properties

    public string XmlCommandPreamble
    {
      get
      {
        return _xmlCommandPreamble;
      }
      set
      {
        _xmlCommandPreamble = value ?? "";
      }
    }

    #endregion Properties

    #region Public Methods

    private void SetUserProfiles(IList<UserProfile> profiles)
    {
      UserProfiles.Clear();

      lock (_locker)
      {
        UserProfiles = profiles.ToList();

        OnUserProfilesUpdated?.Invoke();
      }
    }

    private void ShowUsers(IList<User> users)
    {
      lock (_locker)
      {
        Users = users.ToList();

        OnUsersUpdated?.Invoke();
      }
    }

    public void Reload()
    {
      //mando subito il comando per sapere tutti i profili nel sistema
      Sendcommand_GetAllUserProfile();

      //mando subito il comando per rinfrescare gli utenti attivi
      SendCommand_GetAllUsers();
    }

    public void Close()
    {
      ResetCommandManagerOrXmlClient();
    }

    /// <summary>
    /// Setta la modalità di autenticazine come CommandManager
    /// È necessario indicare anche la società del cliente, in caso stringa nulla o vuota verrà inserito 'Customer'
    /// Inoltre si deve specificare se è possibile creare/modificare/cancellare utenti
    /// </summary>
    public void Configure(CommandManager commandManager, string company)
    {
      if (commandManager == null)
        throw new ArgumentNullException(nameof(commandManager));

      //modalità già settata
      if (_authenticationMode == AuthenticationMode.CommandManager)
        return;

      if (_authenticationMode != AuthenticationMode.None)
        throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

      _authenticationMode = AuthenticationMode.CommandManager;

      _cmNotifier = new CommandManagerNotifier(this, GetHashCode().ToString());
      _cmNotifier.notifyCallBack += NotifyCallback;

      _commandManager = commandManager;
      _commandManager.addCommandManagerNotifier(_cmNotifier);

      if (!string.IsNullOrEmpty(company))
        _company = company;
    }

    /// <summary>
    /// Setta la modalità di autenticazine come XmlClient.
    /// È necessario indicare anche la società del cliente, in caso stringa nulla o vuota verrà inserito 'Customer'.
    /// Inoltre si deve specificare se è possibile creare/modificare/cancellare utenti
    /// </summary>
    public void Configure(IXmlClient xmlClient, string company)
    {
      if (xmlClient == null)
        throw new ArgumentNullException(nameof(xmlClient));

      //modalità già settata
      if (_authenticationMode == AuthenticationMode.XmlClient)
        return;

      if (_authenticationMode != AuthenticationMode.None)
        throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

      _authenticationMode = AuthenticationMode.XmlClient;

      _xmlClient = xmlClient;

      _xmlClient.OnException += XmlClientOnException;
      _xmlClient.OnRetrieveReplyFromServer += XmlClientOnRetrieveReplyFromServer;

      if (!string.IsNullOrEmpty(company))
        _company = company;
    }

    public void CreateOrUpdateUser(User user, bool create)
    {
      //valido la creazione/modifica, se è ok, vado avanti
      User userNew;
      if (!ValidateUser(user.Username, user.Password, user.Fullname, user.Company, user.Profilecode, user.Blocked, out userNew))
        return;

      //tutto ok, mando il comando
      SendCommand_CreateOrUpdateUser(userNew, create);
    }

    public void DeleteUser(string username)
    {
      if (string.IsNullOrEmpty(username))
        return;

      SendCommand_DeleteUser(username);
    }

    #endregion Public Methods

    #region Private Methods

    private void ResetCommandManagerOrXmlClient()
    {
      if (_authenticationMode == AuthenticationMode.CommandManager)
      {
        _cmNotifier.notifyCallBack -= NotifyCallback;
        _commandManager.removeCommandManagerNotifier(_cmNotifier);

        _cmNotifier = null;
        _commandManager = null;
      }
      else if (_authenticationMode == AuthenticationMode.XmlClient)
      {
        _xmlClient.OnException -= XmlClientOnException;
        _xmlClient.OnRetrieveReplyFromServer -= XmlClientOnRetrieveReplyFromServer;

        _xmlClient = null;
      }

      //resetto l'autenticazione
      _authenticationMode = AuthenticationMode.None;
    }

    #endregion Private Methods

    #region DataGridView

    private void ManageCreateOrUpdateUserResponse(IList<int> errors)
    {
      //mostro a video gli errori, riabilito il bottone ed aggiorno le sessioni
      MessageBoxError(errors);

      //TODO dovrei disabilitare i bottoni mentre mando i comandi
      //btnDeleteUser.Enabled = true;

      //non ho errori
      if (!errors.Any())
      {
        //rinfresco i dati solo se è andato tutto bene
        SendCommand_GetAllUsers();

        SendUIMessage(Localize.LocalizeAuthenticationString("Changes Updated Successfully!"), 1);
      }
    }

    /// <summary>
    /// Update user interface event "OnNewMessageToShow" call
    /// </summary>
    private void SendUIMessage(string s, int type)
    {
      OnNewMessageToShow?.Invoke(s, type);
    }

    private void MessageBoxError(IList<int> errorCodeList)
    {
      var errorList = errorCodeList.Select(code => new Tuple<int, IList<string>>(code, null)).ToList();

      MessageBoxError(errorList);
    }

    private void MessageBoxError(IList<Tuple<int, IList<string>>> errorList = null)
    {
      //se errore è diverso da nullo, faccio comparire la schermata, traduco l'errore e setto al scritta
      var showError = errorList != null && errorList.Count > 0/* && errors.All(e => e != 1)*/;
      if (showError)
      {
        var index = 0;
        var count = errorList.Count;

        StringBuilder sb = new StringBuilder();

        foreach (var error in errorList)
        {
          //recupero per tutti i codici, la loro descrizione tradotta
          var errorCode2Descr = GetErrorCode(error.Item1);

          var errorCode = errorCode2Descr.Single().Key;
          var errorDescr = errorCode2Descr.Single().Value;

          sb.Append($"• [{errorCode}] {errorDescr.LocalizeAuthenticationString()}");

          #region Additional Erros

          var additionalErrors = error.Item2;

          if (additionalErrors != null)
          {
            //se gli errori aggiuntivi sono presenti, li scrivo

            sb.Append(" (");

            for (int i = 0; i < additionalErrors.Count; i++)
            {
              sb.Append($"{additionalErrors[i]}");

              if (i + 1 < additionalErrors.Count)
                sb.Append(", ");
            }

            sb.Append(")");
          }

          #endregion Additional Erros

          if (++index < count)
            sb.Append($"{Environment.NewLine}");
        }

        SendUIMessage(sb.ToString(), 2);
      }
    }

    private bool ValidateUser(string username, string password, string fullname, string company, string profile, bool blocked, out User user)
    {
      user = null;

      bool isValid = true;

      if (!ValidateControl(username))
        isValid = false;

      //ad eccezione degli altri la password può essere nulla
      if (!ValidateControl(password))
        isValid = false;

      if (!ValidateControl(fullname))
        isValid = false;

      if (!ValidateControl(company))
        isValid = false;

      //controllo fasullo perchè riempio prima la checkbox, comunque lo lascio per far capire il concetto che c'è dietro
      if (!ValidateControl(profile))
        isValid = false;

      if (isValid)
      {
        user = new User(username, fullname, company, profile, blocked);

        //la password va aggiunta solo se non è nulla, vuota o uguale a quella di default
        if (!string.IsNullOrEmpty(password) && !password.Equals(DefaultEmptyPassword))
          user.Password = password.Base64Encode();
      }

      return isValid;
    }

    private static bool ValidateControl(string value)
    {
      var isValid = !string.IsNullOrEmpty(value);
      return isValid;
    }

    #region Errors

    private IDictionary<int, string> GetErrorCode(params int[] errorCodes)
    {
      var error2Descr = new SortedList<int, string>();

      if (errorCodes == null)
        return error2Descr;

      foreach (var errorCode in errorCodes.Distinct())
      {
        error2Descr.Add(errorCode, _authenticationManager.GetDescriptionForErrorCode(errorCode));
      }

      return error2Descr;
    }

    #endregion Errors

    #endregion DataGridView

    #region Manage Response

    private void ManageReceivedGetAllUserProfilesResponse(IList<UserProfile> profiles)
    {
      SetUserProfiles(profiles);
    }

    private void ManageNotReceivedGetAllUserProfilesResponse()
    {
      SetUserProfiles(new List<UserProfile>());
    }

    private void ManageReceivedGetAllUsersResponse(IList<User> users)
    {
      ShowUsers(users);
    }

    private void ManageNotReceivedGetAllUsersResponse()
    {
      ShowUsers(new List<User>());
    }

    private void ManageReceivedCreateOrUpdateUserResponse(IList<GenericResponse> responseList)
    {
      var errors = new List<int>();

      foreach (var response in responseList)
      {
        //mostro solo le risposte con errore
        if (!response.IsOk)
        {
          errors.Add(response.Result);
        }
      }

      ManageCreateOrUpdateUserResponse(errors);
    }

    private void ManageNotReceivedCreateOrUpdateUserResponse()
    {
      ManageCreateOrUpdateUserResponse(new[] { LocalErrorCodeMapper.ServerNotReachable });
    }

    private void ManageReceivedDeleteUserResponse(IList<GenericResponse> responseList)
    {
      var errors = new List<int>();

      foreach (var response in responseList)
      {
        //mostro solo le risposte con errore
        if (!response.IsOk)
        {
          errors.Add(response.Result);
        }
      }

      ManageCreateOrUpdateUserResponse(errors);
    }

    private void ManageNotReceivedDeleteUserResponse()
    {
      ManageCreateOrUpdateUserResponse(new[] { LocalErrorCodeMapper.ServerNotReachable });
    }

    #endregion Manage Response

    #region XmlClient / CommandManager

    #region Richieste

    private const string CommandGetAllUsers = "GetAllUsers";
    private const string CommandGetAllUserProfile = "GetAllUserProfile";
    private const string CommandCreateUser = "CreateUser";
    private const string CommandUpdateUser = "UpdateUser";
    private const string CommandDeleteUser = "DeleteUser";

    private void Sendcommand_GetAllUserProfile()
    {
      var parameters = new List<string>();
      var session = _authenticationManager.Session;

      //se la sessione è nulla oppure sono cassioli, non filtro (quindi prendo tutto) altrimenti prendo il livello dell'utente
      if (session == null || session.User.Username.EqualsIgnoreCase("cassioli"))
      {
        //nessun filtro
      }
      else
      {
        const string hierarchy = "hierarchy";
        int hierarchyLevel = session.User.UserProfileHierarchy;

        parameters.Add(hierarchy);
        parameters.Add(hierarchyLevel.ToString());
      }

      string cmd = $"{XmlCommandPreamble}{CommandGetAllUserProfile}".CreateXmlCommand(parameters);
      SendCommand(cmd);
    }

    private void SendCommand_GetAllUsers()
    {
      var parameters = new List<string>();
      var session = _authenticationManager.Session;

      //se la sessione è nulla oppure sono cassioli, non filtro (quindi prendo tutto) altrimenti prendo il livello dell'utente
      if (session == null || session.User.Username.EqualsIgnoreCase("cassioli"))
      {
        //nessun filtro
      }
      else
      {
        const string hierarchy = "hierarchy";
        int hierarchyLevel = session.User.UserProfileHierarchy;

        parameters.Add(hierarchy);
        parameters.Add(hierarchyLevel.ToString());

        const string username = "username";

        parameters.Add(username);
        parameters.Add(session.User.Username);
      }

      string cmd = $"{XmlCommandPreamble}{CommandGetAllUsers}".CreateXmlCommand(parameters);
      SendCommand(cmd);
    }

    private void SendCommand_CreateOrUpdateUser(User user, bool isCreation)
    {
      //se sono in fase di creazione mando un comando, altrimenti l'altro
      string methodName = isCreation ? CommandCreateUser : CommandUpdateUser;

      var cmd = $"{XmlCommandPreamble}{methodName}".CreateXmlCommand(user.Username, user.Password, user.Fullname, user.Company, user.Profilecode, user.Blocked);

      SendCommand(cmd);
    }

    private void SendCommand_DeleteUser(string username)
    {
      string cmd = $"{XmlCommandPreamble}{CommandDeleteUser}".CreateXmlCommand(username);
      SendCommand(cmd);
    }

    private void SendCommand(string command)
    {
      //controllo se ho settato la modalità
      if (_authenticationMode == AuthenticationMode.None)
      {
        //throw new Exception($"Set Authentication Mode before start, call method {nameof(Configure)} or  method {nameof(Configure)}");
        return;
      }

      if (_authenticationMode == AuthenticationMode.XmlClient)
      {
        var xmlCmd = _xmlClient.CreateXmlCommand(command, "cassioli");
        _xmlClient.ProcessXmlDoc(xmlCmd, "custom", this);
      }
      else if (_authenticationMode == AuthenticationMode.CommandManager)
      {
        _cmNotifier.sendCommand(command, "custom");
      }
    }

    #endregion Richieste

    #region Risposte

    #region CommandManager

    private void NotifyCallback(CommandManagerNotifier.NotifyObject notifyObject)
    {
      var xmlCmd = notifyObject.xmlCommand;
      var xmlCmdRes = notifyObject.iXmlStreamer as XmlCommandResponse;

      ManageResponse(xmlCmd, xmlCmdRes);
    }

    #endregion CommandManager

    #region XmlClient

    private void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs msgEventArgs)
    {
      var xmlCmd = msgEventArgs.xmlCommand as XmlCommand;
      var xmlCmdRes = msgEventArgs.iXmlStreamer as XmlCommandResponse;

      ManageResponse(xmlCmd, xmlCmdRes);
    }

    private void XmlClientOnException(object sender, ExceptionEventArgs exEventArgs)
    {
      var xmlCmd = exEventArgs.xmlCommand as XmlCommand;
      XmlCommandResponse xmlCmdRes = null;

      ManageResponse(xmlCmd, xmlCmdRes);
    }

    #endregion XmlClient

    #region ManageResponse

    private void ManageResponse(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      var commandMethodName = xmlCmd.name.TrimPreamble();

      #region GetAllUsers

      if (commandMethodName.EqualsConsiderCase(CommandGetAllUsers))
      {
        InvokeControlAction(this, delegate { Response_GetAllUsers(xmlCmd, xmlCmdRes); });
      }

      #endregion GetAllUsers

      #region CommandGetAllUserProfile

      if (commandMethodName.EqualsConsiderCase(CommandGetAllUserProfile))
      {
        InvokeControlAction(this, delegate { Response_GetAllUserProfile(xmlCmd, xmlCmdRes); });
      }

      #endregion CommandGetAllUserProfile

      #region CommandCreateOrUpdateUser

      else if (commandMethodName.EqualsConsiderCase(CommandCreateUser) || commandMethodName.EqualsConsiderCase(CommandUpdateUser))
      {
        InvokeControlAction(this, delegate { Response_CreateOrUpdateUser(xmlCmd, xmlCmdRes); });
      }

      #endregion CommandCreateOrUpdateUser

      #region CommandDeleteUser

      else if (commandMethodName.EqualsConsiderCase(CommandDeleteUser))
      {
        InvokeControlAction(this, delegate { Response_DeleteUserUser(xmlCmd, xmlCmdRes); });
      }

      #endregion CommandDeleteUser
    }

    #endregion ManageResponse

    #endregion Risposte

    #region Gestione Risposte

    private void Response_GetAllUsers(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      if (xmlCmdRes != null)
      {
        //recupero gli utenti
        var users = GetUser(xmlCmdRes);

        ManageReceivedGetAllUsersResponse(users);
      }
      else
      {
        //risposta non ricevuta
        ManageNotReceivedGetAllUsersResponse();
      }
    }

    private void Response_GetAllUserProfile(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      if (xmlCmdRes != null)
      {
        //recupero gli utenti
        var profiles = Profiler.CreateUserProfileCollection(xmlCmdRes);

        ManageReceivedGetAllUserProfilesResponse(profiles);
      }
      else
      {
        //risposta non ricevuta
        ManageNotReceivedGetAllUserProfilesResponse();
      }
    }

    private void Response_CreateOrUpdateUser(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      if (xmlCmdRes != null)
      {
        //recupero la risposta
        var responses = GetGenericResponses(xmlCmdRes);

        ManageReceivedCreateOrUpdateUserResponse(responses);
      }
      else
      {
        //risposta non ricevuta
        ManageNotReceivedCreateOrUpdateUserResponse();
      }
    }

    private void Response_DeleteUserUser(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
    {
      if (xmlCmdRes != null)
      {
        //recupero la risposta
        var responses = GetGenericResponses(xmlCmdRes);

        ManageReceivedDeleteUserResponse(responses);
      }
      else
      {
        //risposta non ricevuta
        ManageNotReceivedDeleteUserResponse();
      }
    }

    #endregion Gestione Risposte

    #endregion XmlClient / CommandManager

    #region GenericResponse

    private class GenericResponse
    {
      /// <summary>
      /// Riporta il valore di un operazione
      /// </summary>
      public string Code { get; }

      /// <summary>
      /// Riporta il dato dell'operazione
      /// </summary>
      public int Result { get; }

      /// <summary>
      /// Restituisce true se code = "ok"
      /// </summary>
      public bool IsOk => Code.EqualsIgnoreCase("ok");

      public GenericResponse(string code, int result)
      {
        if (string.IsNullOrEmpty(code))
          throw new ArgumentNullException(nameof(code));

        Code = code;
        Result = result;
      }
    }

    private GenericResponse GetGenericResponse(XmlCommandResponse.Item item)
    {
      if (item == null)
        throw new ArgumentNullException(nameof(item));

      var code = item.getFieldVal(1);
      var result = item.getFieldVal(2).ConvertTo<int>();

      return new GenericResponse(code, result);
    }

    private IList<GenericResponse> GetGenericResponses(XmlCommandResponse xmlResp)
    {
      if (xmlResp == null)
        throw new ArgumentNullException(nameof(xmlResp));

      var genericResponses = new List<GenericResponse>();

      foreach (var item in xmlResp.Items)
      {
        var genericResponse = GetGenericResponse(item);

        genericResponses.Add(genericResponse);
      }

      return genericResponses;
    }

    #endregion GenericResponse

    #region Utilities

    /// <summary>
    /// Contesto attuale del controllo
    /// </summary>
    private static readonly System.Threading.SynchronizationContext SyncContext = System.Threading.SynchronizationContext.Current;

    /// <summary>
    /// Permette di eseguire il metodo <param name="action"/> della classe <param name="cont"/>
    /// invocandolo tramite thread principale così da evitare le eccezioni
    /// </summary>
    /// <typeparam name="T">Tipo della classe invocante</typeparam>
    /// <param name="cont">Classe invocante</param>
    /// <param name="action">Metodo da invocare</param>
    protected static void InvokeControlAction<T>(T cont, Action<T> action) //where T : control
    {
      if (SyncContext == null)
      {
        action(cont);
      }
      else
      {
        SyncContext.Post(o => action(cont), null);
      }
    }

    private static bool IsWpfGuiThread()
    {
      //return System.Windows.Threading.Dispatcher.FromThread(System.Threading.Thread.CurrentThread) != null;
      return SyncContext is System.Windows.Threading.DispatcherSynchronizationContext;
    }

    private static bool IsWinFormsGuiThread()
    {
      return SyncContext is System.Windows.Forms.WindowsFormsSynchronizationContext;
    }

    #endregion Utilities

    #region User

    public class User
    {
      public string Username { get; set; }
      public string Password { get; set; } = "";
      public string Fullname { get; set; }
      public string Company { get; set; }
      public string Profilecode { get; set; }
      public int Hierarchy { get; set; }
      public bool Blocked { get; set; }

      public User(string username, string fullname, string company, string profilecode, bool blocked)
        : this(username, fullname, company, profilecode, -1, blocked)
      {
      }

      public User(string username, string fullname, string company, string profilecode, int hierarchy, bool blocked)
      {
        Username = username;
        Fullname = fullname;
        Company = company;
        Profilecode = profilecode;
        Hierarchy = hierarchy;
        Blocked = blocked;
      }

      public User()
      {
      }
    }

    private User GetUser(XmlCommandResponse.Item item)
    {
      if (item == null)
        throw new ArgumentNullException(nameof(item));

      var i = 1;

      var username = item.getFieldVal(i++);
      var fullname = item.getFieldVal(i++);
      var company = item.getFieldVal(i++);
      var profilecode = item.getFieldVal(i++);
      var hierarchy = item.getFieldVal(i++).ConvertTo<int>();
      var blocked = item.getFieldVal(i++).ConvertTo<bool>();

      return new User(username, fullname, company, profilecode, hierarchy, blocked);
    }

    private IList<User> GetUser(XmlCommandResponse xmlResp)
    {
      if (xmlResp == null)
        throw new ArgumentNullException(nameof(xmlResp));

      var users = new List<User>();

      foreach (var item in xmlResp.Items)
      {
        var user = GetUser(item);

        users.Add(user);
      }

      return users;
    }

    #endregion User
  }
}