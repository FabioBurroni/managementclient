﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Authentication.Collections;
using Authentication.Controls;
using Authentication.Extensions;
using Authentication.Profiling;
using Localization;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.Extensions;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlClient.Event;
using XmlCommunicationManager.XmlClient.Manager;

namespace Authentication.GUI.Management
{
	public partial class ManageProfilingUserControl : UserControl
	{
		#region Fields

		private readonly AuthenticationManager _authenticationManager = AuthenticationManager.Instance;

		private AuthenticationMode _authenticationMode;

		private string _xmlCommandPreamble;

		private string _clientFilter;

		private CommandManager _commandManager;
		private CommandManagerNotifier _cmNotifier;

		private IXmlClient _xmlClient;

		private bool _isPopulatingdataGridView = true;

		private readonly object _locker = new object();
		private readonly DataTable _dataTableStructures = new DataTable();

		#endregion

		#region Constructor

		public ManageProfilingUserControl()
		{
			InitializeComponent();
		}

		#endregion

		#region Events

		private void ctrlComboClientOrProfile_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			//selezione client o profilo cambiata, richiedo il filtro
			FilterDataGridViewSessions();
		}

		private void btnReloadProfiles_Click(object sender, EventArgs e)
		{
			//controllo se ho almeno una modifica fatta
			var updatedUserStrutures = GetUpdatedUserStructures();

			//se ho almeno una struttura cambiata, chiedo se effettivamente si vogliono ricaricare i profili perdendo delle modifiche
			if (updatedUserStrutures.Any())
			{
				//chiedo conferma
				if (!MessageBoxConfirmReloadWihoutSaving())
					return;
			}

			SendCommand_GetAllPossibleUserStructure();
		}

		private void btnSaveChanges_Click(object sender, EventArgs e)
		{
			//controllo che ce ne sia almeno una cambiata
			var updatedUserStructures = GetUpdatedUserStructures();

			if (!updatedUserStructures.Any())
			{
				//non ho strutture aggiornate
				ShowError(LocalErrorCodeMapper.UpdateAtLeastOneConfiguration);
			}
			else
			{
				//chiedo conferma
				//if (!ConfirmForceSessionsLogout())
				//  return;

				SendCommand_UpdateUserStructure(updatedUserStructures);
				btnSaveChanges.Enabled = false;
				pnlMessage.Visible = false;
			}
		}

		private void dgvProfiling_CurrentCellDirtyStateChanged(object sender, EventArgs e)
		{
			//permette di forzare il commit subito dopo il click dentro la cella
			if (dgvProfiling.IsCurrentCellDirty)
			{
				dgvProfiling.CommitEdit(DataGridViewDataErrorContexts.Commit);
			}
		}

		private void dgvProfiling_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)Keys.Space)
			{
				//se è stata premuta la barra spaziatrice, inverto i valore delle checkbox delle righe selezionate 

				lock (_locker)
				{
					//memorizzo in un dizionario, il riferimento alla checkbox e il suo valore attuale
					var selectedCheckBoxes = new List<DataGridViewCheckBoxCell>();

					bool atLeastOneTrue = false;  //ne ho trovato almeno uno con il valore true
					bool atLeastOneFalse = false; //ne ho trovato almeno uno con il valore false

					foreach (DataGridViewRow selectedRow in dgvProfiling.SelectedRows)
					{
						DataGridViewCheckBoxCell checkBoxCell = (DataGridViewCheckBoxCell)selectedRow.Cells[dataGridViewColumnEnabled.Name];

						//recupero il valore attuale (se è nullo, il valore sarà false)
						var actualValue = checkBoxCell.Value.ConvertTo<bool>(true);

						if (selectedCheckBoxes.Contains(checkBoxCell))
							continue;

						selectedCheckBoxes.Add(checkBoxCell);

						if (actualValue)
							atLeastOneTrue = true;
						else
							atLeastOneFalse = true;
					}

					//scorro tutte le chechbox
					foreach (var checkBox in selectedCheckBoxes)
					{
						if (atLeastOneTrue && !atLeastOneFalse)
						{
							// c'è almeno una checkbox selezionata e nessuna deselezionata, deseleziono tutto
							checkBox.Value = false;
						}
						else if (atLeastOneTrue && atLeastOneFalse)
						{
							//c'è almeno una checkbox selezionata e almeno una deselezionata, seleziono tutto
							checkBox.Value = true;
						}
						else if (!atLeastOneTrue && atLeastOneFalse)
						{
							//nessuna chechbox selezionata e almeno una deselezionata, seleziono tutto
							checkBox.Value = true;
						}
					}
				}
			}
		}

		private void dgvProfiling_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			//se sono in fase di caricamento, non faccio niente
			if (_isPopulatingdataGridView)
				return;

			//dopo che ho popolato la griglia, permette di settare in modo opportuno le abilitazioni/disabilitazioni delle azioni permesse

			var enabledColumn = dgvProfiling.Columns[dataGridViewColumnEnabled.Name];

			//controllo se è la colonna che mi interessa
			if (e.ColumnIndex == enabledColumn?.Index && e.RowIndex >= 0)
			{
				//recupero la riga interessata dai cambiamenti
				var cellEnabled = dgvProfiling.Rows[e.RowIndex].Cells[dataGridViewColumnEnabled.Name];
				var cellAction = dgvProfiling.Rows[e.RowIndex].Cells[dataGridViewColumnAction.Name];
				var cellHierarchy = dgvProfiling.Rows[e.RowIndex].Cells[dataGridViewColumnHierarchy.Name];

				var isChecked = cellEnabled.Value.ConvertTo<bool>();
				var action = cellAction.Value.ConvertTo<string>();
				var hierarchy = cellHierarchy.Value.ConvertTo<int>();

				//recupero i valori attuali dalle combo e recupero i record
				var clientFilter = ctrlComboClient.CurrentKey;

				StringBuilder filter = new StringBuilder();

				filter.AppendLine(" 1=1 ");

				if (!string.IsNullOrEmpty(action))
					filter.AppendLine($" AND {dataGridViewColumnAction.DataPropertyName} = '{action}' ");

				if (!string.IsNullOrEmpty(clientFilter))
					filter.AppendLine($" AND {dataGridViewColumnClient.DataPropertyName} = '{clientFilter}' ");

				//se il valore è true, devo settare a true tutti i profili con privilegi superiori a lui per la stessa action-client, se è false tutti quelli sotto di lui
				if (isChecked)
					filter.AppendLine($" AND {dataGridViewColumnHierarchy.DataPropertyName} < '{hierarchy}' ");
				else
					filter.AppendLine($" AND {dataGridViewColumnHierarchy.DataPropertyName} > '{hierarchy}' ");

				lock (_locker)
				{
					var records = _dataTableStructures.Select(filter.ToString());

					foreach (var record in records)
					{
						//se il valore è già uguale, vado avanti
						if (record[dataGridViewColumnEnabled.DataPropertyName].ConvertTo<bool>() == isChecked)
							continue;

						//setto il nuovo valore
						record[dataGridViewColumnEnabled.DataPropertyName] = isChecked;

						//se non è checkato, retto i dati opzionali
						if (!isChecked)
							record[dataGridViewColumnOptionalData.DataPropertyName] = "";
					}
				}
			}
		}

		#endregion

		#region Properties

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams cp = base.CreateParams;
				cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
				return cp;
			}
		}

		/// <summary>
		/// Specifica l'eventuale preambolo da utilizzare per l'invio dei comandi
		/// </summary>
		public string XmlCommandPreamble
		{
			get
			{
				return _xmlCommandPreamble;
			}
			set
			{
				_xmlCommandPreamble = value ?? "";
			}
		}

		#endregion

		#region Public Methods

		public void InitCtrl()
		{
			//mando subito il comando per rinfrescare le strutture
			SendCommand_GetAllPossibleUserStructure();
		}

		public void CloseCtrl()
		{
			ResetCommandManagerOrXmlClient();
		}

		/// <summary>
		/// Setta la modalità di autenticazine come CommandManager.
		/// È possibile mostrare la profilazione solo per il client indicato
		/// </summary>
		public void Configure(CommandManager commandManager, string clientFilter = null)
		{
			if (commandManager == null)
				throw new ArgumentNullException(nameof(commandManager));

			//modalità già settata
			if (_authenticationMode == AuthenticationMode.CommandManager)
				return;

			if (_authenticationMode != AuthenticationMode.None)
				throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

			_authenticationMode = AuthenticationMode.CommandManager;

			_cmNotifier = new CommandManagerNotifier(this, GetHashCode().ToString());
			_cmNotifier.notifyCallBack += NotifyCallback;

			_commandManager = commandManager;
			_commandManager.addCommandManagerNotifier(_cmNotifier);

			_clientFilter = clientFilter;
		}

		/// <summary>
		/// Setta la modalità di autenticazine come XmlClient
		/// È possibile mostrare la profilazione solo per il client indicato
		/// </summary>
		public void Configure(IXmlClient xmlClient, string clientFilter = null)
		{
			if (xmlClient == null)
				throw new ArgumentNullException(nameof(xmlClient));

			//modalità già settata
			if (_authenticationMode == AuthenticationMode.XmlClient)
				return;

			if (_authenticationMode != AuthenticationMode.None)
				throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

			_authenticationMode = AuthenticationMode.XmlClient;

			_xmlClient = xmlClient;

			_xmlClient.OnException += XmlClientOnException;
			_xmlClient.OnRetrieveReplyFromServer += XmlClientOnRetrieveReplyFromServer;

			_clientFilter = clientFilter;
		}

		public void Translate()
		{
			lblTrProfiling.Text = Localize.LocalizeAuthenticationString("Profiling");
			labelTrManageActionClientProfile.Text = Localize.LocalizeAuthenticationString("Manage Actions, Clients and Profiles");
			labelTrProfile.Text = Localize.LocalizeAuthenticationString("Profile");
			labelTrClient.Text = Localize.LocalizeAuthenticationString("Client");
			btnSaveChanges.ButtonText = Localize.LocalizeAuthenticationString("Save Changes");
			btnReloadProfiles.ButtonText = Localize.LocalizeAuthenticationString("Reload Profiles");
			TranslateDataGridView();
		}

		private void TranslateDataGridView()
		{
			lock (_locker)
			{
				if (dgvProfiling.Columns.Count > 0)
				{
					//Traduzione Header
					dgvProfiling.Columns[dataGridViewColumnAction.Name].HeaderText = Localize.LocalizeAuthenticationString("Action");
					dgvProfiling.Columns[dataGridViewColumnDescription.Name].HeaderText = Localize.LocalizeAuthenticationString("Description");
					dgvProfiling.Columns[dataGridViewColumnStandard.Name].HeaderText = Localize.LocalizeAuthenticationString("Standard");
					dgvProfiling.Columns[dataGridViewColumnEnabled.Name].HeaderText = Localize.LocalizeAuthenticationString("Enabled");
					dgvProfiling.Columns[dataGridViewColumnOptionalData.Name].HeaderText = Localize.LocalizeAuthenticationString("Optional Data");

					//Traduzione celle
					foreach (DataGridViewRow row in dgvProfiling.Rows)
					{
						var userStructure = (UserStructure)row.Cells[dataGridViewColumnUserStructure.Name].Value;

						//traduco la descrizione
						row.Cells[dataGridViewColumnDescription.Name].Value = Localize.LocalizeAuthenticationString(userStructure.UserAction.Descr);
					}
				}
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Restituisce tutte le strutture utente modificate
		/// </summary>
		private IList<UpdatedUserStructure> GetUpdatedUserStructures()
		{
			var updatedUserStructures = new List<UpdatedUserStructure>();

			lock (_locker)
			{
				foreach (DataRow row in _dataTableStructures.Rows)
				{
					//recupero il nuovo valore di abilitazione
					var newEnabled = row[dataGridViewColumnEnabled.DataPropertyName].ConvertTo<bool>();

					//recupero il nuovo valore di dati opzionali
					var newOptionalData = row[dataGridViewColumnOptionalData.DataPropertyName].ToString();

					//recupero la user structure originale
					var userStructure = (UserStructureBase)row[dataGridViewColumnUserStructure.DataPropertyName];

					//se ho almeno una variazione, li memorizzo
					if (userStructure.Enabled != newEnabled || userStructure.OptionalData != newOptionalData)
					{
						updatedUserStructures.Add(new UpdatedUserStructure(userStructure, newEnabled, newOptionalData));
					}
				}
			}

			return updatedUserStructures;
		}

		private void ResetCommandManagerOrXmlClient()
		{
			if (_authenticationMode == AuthenticationMode.CommandManager)
			{
				_cmNotifier.notifyCallBack -= NotifyCallback;
				_commandManager.removeCommandManagerNotifier(_cmNotifier);

				_cmNotifier = null;
				_commandManager = null;
			}
			else if (_authenticationMode == AuthenticationMode.XmlClient)
			{
				_xmlClient.OnException -= XmlClientOnException;
				_xmlClient.OnRetrieveReplyFromServer -= XmlClientOnRetrieveReplyFromServer;

				_xmlClient = null;
			}

			//resetto l'autenticazione
			_authenticationMode = AuthenticationMode.None;
		}

		#region DataGridView

		private void FilterDataGridViewSessions()
		{
			//se non ho ancora inizilizzato le combo, esco
			if (ctrlComboClient.ItemsCount <= 0 || ctrlComboProfile.ItemsCount <= 0)
				return;

			//recupero i valori attuali dalle combo e filtro la tabella
			var profileFilter = ctrlComboProfile.CurrentKey;
			var clientFilter = ctrlComboClient.CurrentKey;

			StringBuilder filter = new StringBuilder();

			filter.AppendLine(" 1=1 ");

			if (!string.IsNullOrEmpty(profileFilter))
				filter.AppendLine($" AND {dataGridViewColumnProfile.DataPropertyName} = '{profileFilter}' ");

			if (!string.IsNullOrEmpty(clientFilter))
				filter.AppendLine($" AND {dataGridViewColumnClient.DataPropertyName} = '{clientFilter}' ");

			lock (_locker)
			{
				DataView dv = dgvProfiling.DataSource as DataView;

				if (dv == null)
					return;

				dv.RowFilter = filter.ToString();
				dgvProfiling.ClearSelection();
			}
		}

		private void ShowUserStructureCollection(IList<UserStructure> usList)
		{
			_isPopulatingdataGridView = true;

			lock (_locker)
			{
				//se non ho colonne, le aggiungo (solo avvio)
				if (_dataTableStructures.Columns.Count == 0)
				{
					_dataTableStructures.Columns.Add(dataGridViewColumnUserStructure.DataPropertyName, typeof(UserStructureBase));
					_dataTableStructures.Columns.Add(dataGridViewColumnUserStructureCode.DataPropertyName, typeof(string));
					_dataTableStructures.Columns.Add(dataGridViewColumnClient.DataPropertyName, typeof(string));
					_dataTableStructures.Columns.Add(dataGridViewColumnProfile.DataPropertyName, typeof(string));
					_dataTableStructures.Columns.Add(dataGridViewColumnHierarchy.DataPropertyName, typeof(int));
					_dataTableStructures.Columns.Add(dataGridViewColumnAction.DataPropertyName, typeof(string));
					_dataTableStructures.Columns.Add(dataGridViewColumnDescription.DataPropertyName, typeof(string));
					_dataTableStructures.Columns.Add(dataGridViewColumnStandard.DataPropertyName, typeof(bool));
					_dataTableStructures.Columns.Add(dataGridViewColumnEnabled.DataPropertyName, typeof(bool));
					_dataTableStructures.Columns.Add(dataGridViewColumnOptionalData.DataPropertyName, typeof(string));
				}

				//recupero la sessione
				var currentSession = _authenticationManager.Session;

				//indica se l'utente attuale non è cassioli
				var isNotCassioli = currentSession != null && !currentSession.User.Username.EqualsIgnoreCase("cassioli");


				//memorizzo tutti i codici univoci delle strutture, mi servono dopo
				var userStructureToShowList = new Dictionary<string, UserStructure>();

				//scorro tutte le sessioni
				foreach (var userStructure in usList)
				{

					#region Esclusioni

					//NOTE alcune informazioni le faccio vedere solo se mi loggo come cassioli, ovvero: 
					//NOTE cassioli può vedere tutto, gli altri possono vedere solo le configurazioni dei profili inferiori al proprio in gerarchia

					if (isNotCassioli)
					{
						if (userStructure.UserProfile.Hierarchy <= currentSession.User.UserProfileHierarchy)
							continue;
					}

					#endregion

					//memorizzo i codici struttura
					userStructureToShowList.Add(userStructure.ToString(), userStructure);

					//se la struttura già esiste, la aggiorno, altrimenti la aggiungo
					var rows = _dataTableStructures.Select($"{dataGridViewColumnUserStructureCode.DataPropertyName} = '{userStructure}'");

					DataRow row;

					if (rows.Any())
					{
						//sessione trovata, la aggiorno
						row = rows.First();
					}
					else
					{
						//sessione non trovata, la creo nuova
						row = _dataTableStructures.NewRow();
						_dataTableStructures.Rows.Add(row);
					}

					row[dataGridViewColumnUserStructure.DataPropertyName] = userStructure; //memorizzo anche tutta la struttura così la recupero facilmente
					row[dataGridViewColumnUserStructureCode.DataPropertyName] = userStructure.ToString();
					row[dataGridViewColumnClient.DataPropertyName] = userStructure.UserClient.Code;
					row[dataGridViewColumnProfile.DataPropertyName] = userStructure.UserProfile.Code;
					row[dataGridViewColumnHierarchy.DataPropertyName] = userStructure.UserProfile.Hierarchy;
					row[dataGridViewColumnAction.DataPropertyName] = userStructure.UserAction.Code;
					row[dataGridViewColumnDescription.DataPropertyName] = Localize.LocalizeAuthenticationString(userStructure.UserAction.Descr);
					row[dataGridViewColumnStandard.DataPropertyName] = userStructure.UserAction.Standard;
					row[dataGridViewColumnEnabled.DataPropertyName] = userStructure.Enabled;
					row[dataGridViewColumnOptionalData.DataPropertyName] = userStructure.OptionalData;
				}

				#region Colonne non visibili

				//se non sono cassioli, nascondo le colonne che non sono necessarie al cliente
				if (currentSession != null && !currentSession.User.Username.EqualsIgnoreCase("cassioli"))
				{
					dataGridViewColumnStandard.Visible = false;
					dataGridViewColumnOptionalData.Visible = false;
				}

				#endregion

				//scorro tutta la nuova tabella, rimuovo tutte le strutture che non sono contenuti perchè sono stati rimossi
				for (int i = _dataTableStructures.Rows.Count - 1; i >= 0; i--)
				{
					DataRow dr = _dataTableStructures.Rows[i];

					var userStructureCode = dr[dataGridViewColumnUserStructureCode.DataPropertyName].ToString();

					//se il token non è contenuto, lo rimouvo
					if (!userStructureToShowList.ContainsKey(userStructureCode))
						dr.Delete();
				}


				//setto le combobox (solo con quello che è possibile mostrare, il resto è bypassato)
				SetComboBox(userStructureToShowList.Values.ToList());


				var dataView = new DataView(_dataTableStructures);
				dgvProfiling.DataSource = dataView;
				dgvProfiling.ClearSelection();

				//filtro la vista attuale
				FilterDataGridViewSessions();
			}

			_isPopulatingdataGridView = false;
		}

		private void SetComboBox(IList<UserStructure> usList)
		{
			lock (_locker)
			{
				//recupero tutti i profili
				var profileCodeList = usList.Select(us => us.UserProfile).Select(us => us.Code).Distinct(StringComparer.OrdinalIgnoreCase).OrderBy(p => p, StringComparer.OrdinalIgnoreCase).ToArray();

				//se tutti i codici profilo sono identici a quelli già caricati, non faccio niente, altrimenti rinfresco
				if (!ctrlComboProfile.GetKeys().Distinct(StringComparer.OrdinalIgnoreCase).OrderBy(c => c, StringComparer.OrdinalIgnoreCase).SequenceEqual(profileCodeList))
				{
					//le sequenze non sono uguali

					//ordino i codici profilo per per gerarchia
					var profiles =
						usList.Select(us => us.UserProfile)
							.Distinct()
							.OrderBy(p => p.Hierarchy)
							.ToDictionary(p => p.Code, p => $"{p} ({p.Hierarchy})"); //la chiave è il nome del profilo, il valore è NomeProfilo (1) dove 1 è il valore di gerarchia

					//setto i dati
					ctrlComboProfile.SetData(profiles);

					//prendo il primo elemento (zero)
					ctrlComboProfile.Init(0);
				}


				//recupero tutti i client
				var clientCodeList = usList.Select(us => us.UserClient).Select(us => us.Code).Distinct(StringComparer.OrdinalIgnoreCase).OrderBy(p => p, StringComparer.OrdinalIgnoreCase).ToArray();

				//se tutti i codici client sono identici a quelli già caricati, non faccio niente, altrimenti rinfresco
				if (!ctrlComboClient.GetKeys().Distinct(StringComparer.OrdinalIgnoreCase).OrderBy(c => c, StringComparer.OrdinalIgnoreCase).SequenceEqual(clientCodeList))
				{
					//le sequenze non sono uguali

					//ordino i codici client in modo alfabetico
					var clients =
						usList.Select(us => us.UserClient)
							.Distinct()
							.OrderBy(p => p)
							.ToDictionary(p => p.Code, p => p.Descr);

					//setto i dati
					ctrlComboClient.SetData(clients);

					//prendo il primo elemento (zero)
					ctrlComboClient.Init(0);
				}
			}
		}

		private void ManageUpdateUserStructureResponse(IList<Tuple<int, UserStructureBase>> errors)
		{
			//mostro a video gli errori, riabilito il bottone ed aggiorno le sessioni
			ShowError(errors);

			btnSaveChanges.Enabled = true;
			SendCommand_GetAllPossibleUserStructure();

			//non ho errori
			if (!errors.Any())
			{
				MessageBoxUpdateDone();
			}
		}

		#endregion

		private static bool MessageBoxConfirmReloadWihoutSaving()
		{
			var text = Localize.LocalizeAuthenticationString("Changes Were Made, Are You Sure To Reload Profiles Losing All The Modifications?");
			var caption = Localize.LocalizeAuthenticationString("Confirm Choice");

			var result = MessageBox.Show(text, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

			return result == DialogResult.Yes;
		}

		private static bool MessageBoxUpdateDone()
		{
			var text = Localize.LocalizeAuthenticationString("Update completed successfully!");
			var caption = Localize.LocalizeAuthenticationString("Result");

			var result = MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

			return result == DialogResult.OK;
		}

		#region Errors

		private void ShowError(int errorCode, UserStructureBase userStructure = null)
		{
			ShowError(new List<Tuple<int, UserStructureBase>>
			{
				new Tuple<int, UserStructureBase>(errorCode, userStructure)
			});
		}

		private void ShowError(IList<int> errorCodeList)
		{
			var errorList = errorCodeList.Select(code => new Tuple<int, UserStructureBase>(code, null)).ToList();

			ShowError(errorList);
		}

		private void ShowError(IList<Tuple<int, UserStructureBase>> errorList)
		{
			//se errore è diverso da nullo, faccio comparire la schermata, traduco l'errore e setto al scritta
			var showError = errorList != null && errorList.Count > 0/* && errors.All(e => e != 1)*/;
			if (showError)
			{
				var index = 0;
				var count = errorList.Count;

				StringBuilder sb = new StringBuilder();

				foreach (var error in errorList)
				{
					//recupero per tutti i codici, la loro descrizione non tradotta
					var errorCode2Descr = GetErrorCode(error.Item1);

					var errorCode = errorCode2Descr.Single().Key;
					var errorDescr = errorCode2Descr.Single().Value;

					sb.Append($"• [{errorCode}] {errorDescr.LocalizeAuthenticationString()}");

					#region Additional Erros

					//se gli errori aggiuntivi sono presenti, li scrivo
					UserStructureBase userStructureBase = error.Item2;

					if (userStructureBase != null)
					{
						sb.Append(" (");

						sb.Append(userStructureBase);

						sb.Append(")");
					}

					#endregion

					if (++index < count)
						sb.Append($"{Environment.NewLine}");
				}

				textBoxError.Text = sb.ToString();
			}

			pnlMessage.Visible = showError; //schermata errore
		}

		private IDictionary<int, string> GetErrorCode(params int[] errorCodes)
		{
			var error2Descr = new SortedList<int, string>();

			if (errorCodes == null)
				return error2Descr;

			foreach (var errorCode in errorCodes.Distinct())
			{
				error2Descr.Add(errorCode, _authenticationManager.GetDescriptionForErrorCode(errorCode));
			}

			return error2Descr;
		}

		#endregion

		#endregion

		#region Manage Response

		private void ManageReceivedGetAllPossibleUserStructure(IList<UserStructure> userStructureList)
		{
			ShowUserStructureCollection(userStructureList);
		}

		private void ManageNotReceivedGetAllPossibleUserStructure()
		{
			ShowUserStructureCollection(new List<UserStructure>());
		}

		private void ManageReceivedUpdateUserStructureResponse(IList<GenericResponse> genericResponses, IList<UserStructureBase> usList)
		{
			var errors = new List<Tuple<int, UserStructureBase>>();

			foreach (var i in Enumerable.Range(0, genericResponses.Count))
			{
				var response = genericResponses[i];

				//mostro solo le risposte con errore
				if (!response.IsOk)
				{
					var userStructure = usList[i];

					errors.Add(new Tuple<int, UserStructureBase>(response.Result, userStructure));
				}
			}

			ManageUpdateUserStructureResponse(errors);
		}

		private void ManageNotReceivedUpdateUserStructureResponse()
		{
			ManageUpdateUserStructureResponse(new List<Tuple<int, UserStructureBase>>
			{
				new Tuple<int, UserStructureBase>(LocalErrorCodeMapper.ServerNotReachable, null)
			});
		}

		#endregion

		#region XmlClient / CommandManager

		#region Richieste

		private const string CommandGetAllPossibleUserStructure = "GetAllPossibleUserStructure";
		private const string CommandUpdateUserStructure = "UpdateUserStructure";

		private void SendCommand_GetAllPossibleUserStructure()
		{
			var parameters = new List<string>();
			var session = _authenticationManager.Session;

			//se la sessione è nulla oppure sono cassioli, non filtro (quindi prendo tutto) altrimenti prendo il livello dell'utente
			if (session == null || session.User.Username.EqualsIgnoreCase("cassioli"))
			{
				//nessun filtro
			}
			else
			{
				const string hierarchy = "hierarchy";
				int hierarchyLevel = session.User.UserProfileHierarchy;

				parameters.Add(hierarchy);
				parameters.Add(hierarchyLevel.ToString());
			}

			//se il filtro per client è impostato, lo setto nei parametri (tutti sono soggetti a filtro, anche cassioli)
			if (!string.IsNullOrEmpty(_clientFilter))
			{
				const string client = "client";

				parameters.Add(client);
				parameters.Add(_clientFilter);
			}

			var cmd = $"{XmlCommandPreamble}{CommandGetAllPossibleUserStructure}".CreateXmlCommand(parameters);

			SendCommand(cmd);
		}
		private void SendCommand_UpdateUserStructure(IList<UpdatedUserStructure> updatedUserStructures)
		{
			var parameters = new List<string>();

			foreach (var updated in updatedUserStructures)
			{
				parameters.Add(updated.UserStructure.UserActionCode);
				parameters.Add(updated.UserStructure.UserClientCode);
				parameters.Add(updated.UserStructure.UserProfileCode);
				parameters.Add(updated.Enabled.ToString());
				parameters.Add(updated.OptionalData);
			}

			string cmd = $"{XmlCommandPreamble}{CommandUpdateUserStructure}".CreateXmlCommand(parameters);
			SendCommand(cmd);
		}

		private void SendCommand(string command)
		{
			//controllo se ho settato la modalità
			if (_authenticationMode == AuthenticationMode.None)
			{
				//throw new Exception($"Set Authentication Mode before start, call method {nameof(Configure)} or  method {nameof(Configure)}");
				return;
			}

			if (_authenticationMode == AuthenticationMode.XmlClient)
			{
				var xmlCmd = _xmlClient.CreateXmlCommand(command, "cassioli");
				_xmlClient.ProcessXmlDoc(xmlCmd, "custom", this);
			}
			else if (_authenticationMode == AuthenticationMode.CommandManager)
			{
				_cmNotifier.sendCommand(command, "custom");
			}
		}

		#endregion

		#region Risposte

		#region CommandManager

		private void NotifyCallback(CommandManagerNotifier.NotifyObject notifyObject)
		{
			var xmlCmd = notifyObject.xmlCommand;
			var xmlCmdRes = notifyObject.iXmlStreamer as XmlCommandResponse;

			ManageResponse(xmlCmd, xmlCmdRes);
		}

		#endregion

		#region XmlClient

		private void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs msgEventArgs)
		{
			var xmlCmd = msgEventArgs.xmlCommand as XmlCommand;
			var xmlCmdRes = msgEventArgs.iXmlStreamer as XmlCommandResponse;

			ManageResponse(xmlCmd, xmlCmdRes);
		}

		private void XmlClientOnException(object sender, ExceptionEventArgs exEventArgs)
		{
			var xmlCmd = exEventArgs.xmlCommand as XmlCommand;
			XmlCommandResponse xmlCmdRes = null;

			ManageResponse(xmlCmd, xmlCmdRes);
		}

		#endregion

		#region ManageResponse

		private void ManageResponse(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
		{
			var commandMethodName = xmlCmd.name.TrimPreamble();

			#region GetAllPossibleUserStructure

			if (commandMethodName.EqualsConsiderCase(CommandGetAllPossibleUserStructure))
			{
				InvokeControlAction(this, delegate { Response_GetAllPossibleUserStructure(xmlCmd, xmlCmdRes); });
			}

			#endregion

			#region UpdateUserStructure

			else if (commandMethodName.EqualsConsiderCase(CommandUpdateUserStructure))
			{
				InvokeControlAction(this, delegate { Response_UpdateUserStructure(xmlCmd, xmlCmdRes); });
			}

			#endregion
		}

		#endregion

		#endregion

		#region Gestione Risposte

		private void Response_GetAllPossibleUserStructure(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
		{
			if (xmlCmdRes != null)
			{
				var usList = Profiler.CreateUserStructureCollection(xmlCmdRes);

				ManageReceivedGetAllPossibleUserStructure(usList);
			}
			else
			{
				//risposta non ricevuta
				ManageNotReceivedGetAllPossibleUserStructure();
			}
		}

		private void Response_UpdateUserStructure(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
		{
			if (xmlCmdRes != null)
			{
				//recupero la risposta

				//lista di tutti i risultati
				var genericResps = GetGenericResponses(xmlCmdRes);

				//lista di tutte le nuove strutture
				var usList = Profiler.CreateUserStructureCollection(xmlCmdRes, 3);

				ManageReceivedUpdateUserStructureResponse(genericResps, usList);
			}
			else
			{
				//risposta non ricevuta
				ManageNotReceivedUpdateUserStructureResponse();
			}
		}

		#endregion

		#endregion

		#region Utilities

		/// <summary>
		/// Contesto attuale del controllo
		/// </summary>
		private static readonly System.Threading.SynchronizationContext SyncContext = System.Threading.SynchronizationContext.Current;

		/// <summary>
		/// Permette di eseguire il metodo <param name="action"/> della classe <param name="cont"/>
		/// invocandolo tramite thread principale così da evitare le eccezioni
		/// </summary>
		/// <typeparam name="T">Tipo della classe invocante</typeparam>
		/// <param name="cont">Classe invocante</param>
		/// <param name="action">Metodo da invocare</param>
		protected static void InvokeControlAction<T>(T cont, Action<T> action)
			where T : Control
		{
			//if (cont.InvokeRequired)
			//{
			//  cont.Invoke(new Action<T, Action<T>>(InvokeControlAction), cont, action);
			//}
			//else
			//{
			//  action(cont);
			//}

			//NOTE la vecchia implementazione prevedeva il classico InvokeRequired ma questo è valido solo se chi istanzia il controllo è una WinForms.
			//NOTE Per rendere compatibile il controllo anche con WPF, devo utilizzare il contesto.
			//NOTE ATTENZIONE: non è necessario utilizzare ISynchronizeInvoke.InvokeRequired o Dispatcher.CheckAccess, il controllo è interno al contesto stesso.

			if (SyncContext == null)
			{
				action(cont);
			}
			else
			{
				SyncContext.Post(o => action(cont), null);
			}
		}

		private static bool IsWpfGuiThread()
		{
			//return System.Windows.Threading.Dispatcher.FromThread(System.Threading.Thread.CurrentThread) != null;
			return SyncContext is System.Windows.Threading.DispatcherSynchronizationContext;
		}

		private static bool IsWinFormsGuiThread()
		{
			return SyncContext is System.Windows.Forms.WindowsFormsSynchronizationContext;
		}

		#endregion

		#region UpdatedUserStructure

		/// <summary>
		/// Classe di supporto per le strutture in cui memorizzo la variazione di configurazione
		/// </summary>
		private class UpdatedUserStructure
		{
			public UserStructureBase UserStructure { get; }
			public bool Enabled { get; }
			public string OptionalData { get; }

			public UpdatedUserStructure(UserStructureBase userStructure, bool enabled, string optionalData)
			{
				if (userStructure == null)
					throw new ArgumentNullException(nameof(userStructure));

				UserStructure = userStructure;
				Enabled = enabled;
				OptionalData = optionalData ?? "";
			}
		}

		#endregion

		#region GenericResponse

		private class GenericResponse
		{
			/// <summary>
			/// Riporta il valore di un operazione
			/// </summary>
			public string Code { get; }

			/// <summary>
			/// Riporta il dato dell'operazione
			/// </summary>
			public int Result { get; }

			/// <summary>
			/// Restituisce true se code = "ok"
			/// </summary>
			public bool IsOk => Code.EqualsIgnoreCase("ok");

			public GenericResponse(string code, int result)
			{
				if (string.IsNullOrEmpty(code))
					throw new ArgumentNullException(nameof(code));

				Code = code;
				Result = result;
			}
		}

		private GenericResponse GetGenericResponse(XmlCommandResponse.Item item)
		{
			if (item == null)
				throw new ArgumentNullException(nameof(item));

			var code = item.getFieldVal(1);
			var result = item.getFieldVal(2).ConvertTo<int>();

			return new GenericResponse(code, result);
		}

		private IList<GenericResponse> GetGenericResponses(XmlCommandResponse xmlResp)
		{
			if (xmlResp == null)
				throw new ArgumentNullException(nameof(xmlResp));

			var genericResponses = new List<GenericResponse>();

			foreach (var item in xmlResp.Items)
			{
				var genericResponse = GetGenericResponse(item);

				genericResponses.Add(genericResponse);
			}

			return genericResponses;
		}

		#endregion
	}
}