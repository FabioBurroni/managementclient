﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Windows.Forms;
using Authentication.Default;
using Authentication.Extensions;
using Localization;
using XmlCommunicationManager.Authorization;
using XmlCommunicationManager.XmlClient.Form;

namespace Authentication.GUI
{
  /// <summary>
  /// Implementazione del form di default per l'autenticazione.
  /// Nel caso in cui cambi l'ordine dei parametri o proprio i parametri, fare una classe
  /// simile a questa. Per esempio, se si vuole fare un login con l'impronta digitale, fare un'altro form
  /// dove non saranno richiesti username e password ma solo la scansione del dito, in più cambieranno
  /// le credenziali dietro all'interfaccia.
  /// </summary>
  public partial class UsernameAndPasswordForm : FormCommunicationBase, IAuthenticationForm
	{
    #region Fields

    private const string UsernameCassioli = "cassioli";
    private const string PasswordCassioli = "cmcSoft";

    private readonly AuthenticationManager _authenticationManager = AuthenticationManager.Instance;

    //mi serve per capire se è il primo tentativo di login o no dopo una riconnessione.
    //nel caso sia il primo non mostro a video gli errori
    private bool _isFirstLoginAttemptAfterReconnection = true;

    //indica se devo tentare di ereditare la sessione o no
    private bool _tryToInheritSession;

    #endregion

    #region Constructor

    public UsernameAndPasswordForm()
    {
      InitializeComponent();

      InitGraphis();

      SubscribeToEvents();
    }

    #endregion

    #region Events

    private void AuthenticationManagerOnAuthenticationResponseReceived(object sender, AuthenticationResponseEventArgs eventArgs)
    {
      InvokeControlAction(this, delegate { ManageAuthenticationResponse(eventArgs.AuthenticationResponse); });
    }

    #region Bottoni

    private void buttonLogin_Click(object sender, EventArgs e)
    {
      SendLogin();
    }

    private void buttonExit_Click(object sender, EventArgs e)
    {
      _authenticationManager.RequestCloseApplication();
    }

    #endregion

    #region Textbox
    private void textBoxUsernameOrPassword_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar != (char)Keys.Enter)
        return;

      SendLogin();
    }

    #endregion

    #endregion

    #region Properties

    protected override CreateParams CreateParams
    {
      get
      {
        CreateParams cp = base.CreateParams;
        cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
        return cp;
      }
    } 

    #endregion

    #region Public Methods

		public void SetBcrData(string data)
		{
			//throw new NotImplementedException();
		}

    public DialogResult ShowDialog(IWin32Window owner, bool tryToInheritSession, bool quickLoginAsCassioli)
    {
      var credentials = GetCredentials(quickLoginAsCassioli ? UsernameCassioli : null, quickLoginAsCassioli ? PasswordCassioli.Base64Encode() : null);
      _authenticationManager.Start(credentials, tryToInheritSession);

      //se non devo ereditare niente, abilito i controlli
      _tryToInheritSession = tryToInheritSession;
      if (!tryToInheritSession)
        SetGraphics(false, true);
    
      return base.ShowDialog(owner);
    }

    #endregion

    #region Private Methods

    #region Quick Login as Cassioli

    /// <summary>
    /// Questo rettangolo corrisponde esattamente alle coordinate dove si trova lo spazio bianco
    /// della A del logo Cassioli!!!
    /// </summary>
    private readonly Rectangle _quickLoginArea = new Rectangle(290, 65, 10, 10);

    private volatile int _consecutiveClicks;

    /// <summary>
    /// Permette di entrare come utente Cassioli se sono fatti esattamente 10 click consecutivi all'interno di una certa area del form
    /// </summary>
    private void QuickLoginAsCassioli(Point clickedPoint)
    {
	    Rectangle quickLoginArea;

	    #region ScalingFactor

	    //recupero il fattore di scala dello schermo ovvero l'eventuale zoom presente
	    var scalingFactor = GetScalingFactor();

	    //verifico se il mio fattore di scala è uguale o diverso da 1 (ovvero zoom al 100% che è il valore di default)
	    if (Math.Abs(scalingFactor - 1) <= double.Epsilon)
	    {
		    //fattore di scala uguale a 1, prendo le coordinate di default
		    quickLoginArea = _quickLoginArea;
	    }
	    else
	    {
		    //c'è un fattore di scala, il mio rettangolo deve tenerne conto
		    quickLoginArea = new Rectangle((int)(_quickLoginArea.X * scalingFactor), (int)(_quickLoginArea.Y * scalingFactor),
			    (int)(_quickLoginArea.Width * scalingFactor), (int)(_quickLoginArea.Height * scalingFactor));
	    }

	    #endregion

			//controllo se il punto dove è stato premuto il mouse è all'interno del mio rettangolo
			if (quickLoginArea.Contains(clickedPoint))
      {
        //contiene, incremento il contatore
        _consecutiveClicks++;
      }
      else
      {
        //non lo contiene, azzero il contatore
        _consecutiveClicks = 0;
      }

      if (_consecutiveClicks >= 10)
      {
        SendLogin(UsernameCassioli, PasswordCassioli);
        _consecutiveClicks = 0;
      }

      //Loggare che è stato fatto un tentativo di accesso come Cassioli?!
    }

	  #region ScalingFactor

	  [DllImport("gdi32.dll")]
	  private static extern int GetDeviceCaps(IntPtr hdc, int nIndex);
	  public enum DeviceCap
	  {
		  VERTRES = 10,
		  DESKTOPVERTRES = 117,
		  LOGPIXELSY = 90,

		  // http://pinvoke.net/default.aspx/gdi32/GetDeviceCaps.html
	  }

	  private static float GetScalingFactor()
	  {
		  Graphics g = Graphics.FromHwnd(IntPtr.Zero);
		  IntPtr desktop = g.GetHdc();
		  //int logicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.VERTRES);
		  //int physicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.DESKTOPVERTRES);
		  int logpixelsy = GetDeviceCaps(desktop, (int)DeviceCap.LOGPIXELSY);

		  //float screenScalingFactor = (float)physicalScreenHeight / (float)logicalScreenHeight; // 1.25 = 125%
		  float dpiScalingFactor = (float)logpixelsy / (float)96;

		  return dpiScalingFactor;
	  }

	  #endregion

		#endregion

		#region SendLogin

		/// <summary>
		/// Manda il comando di login, se username e password sono vuote, le recupera dall'interfaccia grafica
		/// altrimenti usa quelle gli vengono passate
		/// </summary>
		private void SendLogin(string username = null, string password = null)
    {
      var errors = new List<int>();

      if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
      {
        username = textBoxUsername.Text;
        password = textBoxPassword.Text;

        if (string.IsNullOrEmpty(username))
        {
          errors.Add(LocalErrorCodeMapper.UsernameCannotBeBlank);
        }

        if (string.IsNullOrEmpty(password))
        {
          errors.Add(LocalErrorCodeMapper.PasswordCannotBeBlank);
        }
      }

      //se ho almeno un errore, significa che qualcosa non va
      if (errors.Any())
      {
        SetGraphics(false, true, errors.ToArray());
        return;
      }

      SetGraphics(true, false, errors.ToArray());

      var passwordBase64 = password.Base64Encode();
      var credentials = GetCredentials(username, passwordBase64);
      _authenticationManager.SendLogin(credentials);
    }

    #endregion

    #region Response

    private void ManageAuthenticationResponse(IAuthenticationResponse response)
    {
      //controllo il risultato della comunicazione

      #region Risposta Login OK

      //risposta ok, mi sono loggato
      if (!response.IsInError)
      {
        //chiudo il controllo e arrivederci
        Close();
				Dispose();
        return;
      }

      #endregion

      #region Risposta Login NOK => Server non raggiungibile

      //risposta nok

      //se il server non è raggiungibile, mostro subito l'errore
      if (response.Result == LocalErrorCodeMapper.ServerNotReachable)
      {
        SetGraphics(true, false, response.Result);
        _isFirstLoginAttemptAfterReconnection = true;
        return;
      }

      #endregion

      #region Risposta Login NOK => Altro motivo

      if (_tryToInheritSession && _isFirstLoginAttemptAfterReconnection)
      {
        //se è il primo tentativo di login (e voglio ereditare la sessione), non mostro gli errori a video, abilito solo l'interfaccia
        _isFirstLoginAttemptAfterReconnection = !_isFirstLoginAttemptAfterReconnection;
        SetGraphics(false, true);
        return;
      }

      //non è il primo tentativo, mostro gli errori
      SetGraphics(false, true, response.Result, response.ErrorList);

      #endregion
    }

    #endregion

    #region Graphics

    /// <summary>
    /// Inizializza la parte grafica
    /// </summary>
    private void InitGraphis()
    {
      Translate(); //traduco le etichette
      CenterToScreen(); //metto il controllo al centro dello schermo
      SetGraphics(true, false); //setto la schermata di caricamento
    }


    /// <summary>
    /// Setta i controlli grafici in base ai parametri passati
    /// </summary>
    /// <param name="showLoading">Indica se mostrare la schermata di caricamento</param>
    /// <param name="enableCredentials">Indica se abilitare la schermata di credenziali</param>
    /// <param name="errorCode">Indica i codici degli errori a video da mostrare</param>
    /// <param name="additionalErrorList">Lista aggiuntiva degli errori (non sono tradotti)</param>
    private void SetGraphics(bool showLoading, bool enableCredentials, int errorCode, IList<string> additionalErrorList = null)
    {
      SetGraphics(showLoading, enableCredentials,
        new Dictionary<int, IList<string>>
        {
          { errorCode, additionalErrorList }
        });
    }

    /// <summary>
    /// Setta i controlli grafici in base ai parametri passati
    /// </summary>
    /// <param name="showLoading">Indica se mostrare la schermata di caricamento</param>
    /// <param name="enableCredentials">Indica se abilitare la schermata di credenziali</param>
    /// <param name="errorCodeList">Indica i codici degli errori a video da mostrare</param>
    private void SetGraphics(bool showLoading, bool enableCredentials, IList<int> errorCodeList)
    {
      var dictionary = errorCodeList.ToDictionary<int, int, IList<string>>(errorCode => errorCode, errorCode => null);

      SetGraphics(showLoading, enableCredentials, dictionary);
    }

    /// <summary>
    /// Setta i controlli grafici in base ai parametri passati
    /// </summary>
    /// <param name="showLoading">Indica se mostrare la schermata di caricamento</param>
    /// <param name="enableCredentials">Indica se abilitare la schermata di credenziali</param>
    /// <param name="errorCodeList">Indica i codici degli errori a video da mostrare, il valore (se presente) indica gli errori aggiuntivi per quel particolare codice</param>
    private void SetGraphics(bool showLoading, bool enableCredentials, IDictionary<int, IList<string>> errorCodeList = null)
    {
      controlLoading.Visible = showLoading;    //schermata con caricamento in atto
      controlNeedLogin.Visible = !showLoading; //schermata con lucchetto

      controlCredentials.Enabled = enableCredentials;
      buttonLogin.Enabled = enableCredentials;
      if (enableCredentials)
      {
        //setto il focus sul controllo della textbox
        ActiveControl = textBoxUsername;
      }

      #region Errors

      //se errore è diverso da nullo, faccio comparire la schermata, traduco l'errore e setto al scritta
      var showError = errorCodeList != null && errorCodeList.Count > 0/* && errors.All(e => e != 1)*/;
      if (showError)
      {
        //recupero per tutti i codici, la loro descrizione tradotta
        var errors = GetErrorCode(errorCodeList.Keys.ToArray());

        StringBuilder sb = new StringBuilder();

        foreach (var error in errors)
        {
          sb.Append($"• [{error.Key}] {error.Value.LocalizeAuthenticationString()}");

          #region Additional Erros

          //se gli errori aggiuntivi sono presenti, li scrivo
          IList<string> additionalErrors;
          if (errorCodeList.TryGetValue(error.Key, out additionalErrors))
          {
            if (additionalErrors != null && additionalErrors.Count > 0)
            {
              sb.Append(" (");

              for (int i = 0; i < additionalErrors.Count; i++)
              {
                sb.Append($"{additionalErrors[i]}");

                if (i + 1 < additionalErrors.Count)
                  sb.Append(", ");
              }

              sb.Append(")");
            }
          }

          #endregion

          sb.Append($"{Environment.NewLine}");
        }

        textBoxError.Text = sb.ToString();
      }

      controlError.Visible = showError; //schermata errore

      #endregion
    }

    private IDictionary<int, string> GetErrorCode(params int[] errorCodes)
    {
      var error2Descr = new SortedList<int, string>();

      if (errorCodes == null)
        return error2Descr;

      foreach (var errorCode in errorCodes.Distinct())
      {
        error2Descr.Add(errorCode, _authenticationManager.GetDescriptionForErrorCode(errorCode));
      }

      return error2Descr;
    }

    #endregion

    #region Events

    private void SubscribeToEvents()
    {
      _authenticationManager.AuthenticationResponseReceived += AuthenticationManagerOnAuthenticationResponseReceived;
    }

    private void UnscribeFromEvents()
    {
      _authenticationManager.AuthenticationResponseReceived -= AuthenticationManagerOnAuthenticationResponseReceived;
    }

    #endregion

    #region Translate

    private void Translate()
    {
      Text = Localize.LocalizeAuthenticationString("Authentication Form".ToUpper());
      labelLoading.Text = Localize.LocalizeAuthenticationString("Loading".ToUpper()) + "...";
      labelPleaseWait.Text = Localize.LocalizeAuthenticationString("Please Wait".ToUpper());
      controlCredentials.Text = Localize.LocalizeAuthenticationString("Credentials".ToUpper());
      labelUsername.Text = Localize.LocalizeAuthenticationString("Username".ToUpper());
      labelPassword.Text = Localize.LocalizeAuthenticationString("Password".ToUpper());
      buttonClose.Text = Localize.LocalizeAuthenticationString("Close".ToUpper());
      buttonLogin.Text = Localize.LocalizeAuthenticationString("Login".ToUpper());
    }

    #endregion

    #endregion

    #region Static Support

    /// <summary>
    /// Questa schermata è vincolata a credenziali di tipo username e password.
    /// Se cambiano le esigenze, fare una schermata diversa.
    /// </summary>
    /// <returns></returns>
    private static ICredentials GetCredentials(string username = "", string passwordBase64 = "")
    {
      return new Credentials(username, passwordBase64);
    }

		#region WndProc

		private const uint WM_PARENTNOTIFY = 0x0210;
	  private const uint WM_TOUCH = 0x0240;

	  private const uint WM_LBUTTONDOWN = 0x0201;

		/// <summary>
		/// Permette di intercettare TUTTI i click all'interno di questo form
		/// </summary>
		[PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    protected override void WndProc(ref Message m)
    {
			if ((m.Msg == WM_PARENTNOTIFY || m.Msg == WM_TOUCH) && m.WParam.ToInt32() == WM_LBUTTONDOWN)
			{
        // get the clicked position
        var x = m.LParam.ToInt32() & 0xFFFF;
        var y = m.LParam.ToInt32() >> 16;

        var point = new Point(x, y);

        // get the clicked control
        //var childControl = this.GetChildAtPoint(point);

        // call onClick (which fires Click event)
        OnClick(EventArgs.Empty);

        //invoco il metodo di gestione del QuickLogin
        QuickLoginAsCassioli(point);
      }
      base.WndProc(ref m);
    }

    #endregion

    #endregion

    #region Utilities

    /// <summary>
    /// Contesto attuale del controllo
    /// </summary>
    private static readonly System.Threading.SynchronizationContext SyncContext = System.Threading.SynchronizationContext.Current;

    /// <summary>
    /// Permette di eseguire il metodo <param name="action"/> della classe <param name="cont"/>
    /// invocandolo tramite thread principale così da evitare le eccezioni
    /// </summary>
    /// <typeparam name="T">Tipo della classe invocante</typeparam>
    /// <param name="cont">Classe invocante</param>
    /// <param name="action">Metodo da invocare</param>
    protected static void InvokeControlAction<T>(T cont, Action<T> action)
      where T : Control
    {
      //if (cont.InvokeRequired)
      //{
      //  cont.Invoke(new Action<T, Action<T>>(InvokeControlAction), cont, action);
      //}
      //else
      //{
      //  action(cont);
      //}

      //NOTE la vecchia implementazione prevedeva il classico InvokeRequired ma questo è valido solo se chi istanzia il controllo è una WinForms.
      //NOTE Per rendere compatibile il controllo anche con WPF, devo utilizzare il contesto.
      //NOTE ATTENZIONE: non è necessario utilizzare ISynchronizeInvoke.InvokeRequired o Dispatcher.CheckAccess, il controllo è interno al contesto stesso.

      if (SyncContext == null)
      {
        action(cont);
      }
      else
      {
        SyncContext.Post(o => action(cont), null);
      }
    }

    private static bool IsWpfGuiThread()
    {
      //return System.Windows.Threading.Dispatcher.FromThread(System.Threading.Thread.CurrentThread) != null;
      return SyncContext is System.Windows.Threading.DispatcherSynchronizationContext;
    }

    private static bool IsWinFormsGuiThread()
    {
      return SyncContext is System.Windows.Forms.WindowsFormsSynchronizationContext;
    }

    #endregion

    #region Dispose

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        UnscribeFromEvents();
        components?.Dispose();
      }
      base.Dispose(disposing);
    }

    #endregion
  }
}