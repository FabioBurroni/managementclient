﻿using System.Windows;

namespace Authentication.GUI
{
  /// <summary>
  /// Interaction logic for UsernameAndPasswordWindow.xaml
  /// </summary>
  internal partial class UsernameAndPasswordWindow : Window
  {
    public UsernameAndPasswordWindow()
    {
      InitializeComponent();
    }
  }
}