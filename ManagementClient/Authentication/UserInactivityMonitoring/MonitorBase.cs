// MonitorBase Class
// Revision 1 (2004-12-18)
// Copyright (C) 2004 Dennis Dietrich
//
// Released unter the BSD License
// http://www.opensource.org/licenses/bsd-license.php
// http://www.codeproject.com/Articles/9104/How-to-check-for-user-inactivity-with-and-without

using System;
using System.ComponentModel;
using System.Timers;

namespace Authentication.UserInactivityMonitoring
{
	/// <summary>
	/// Provides the abstract base class for inactivity
	/// monitors which is independent from how the
	/// events are intercepted
	/// </summary>
	internal abstract class MonitorBase : IInactivityMonitor
	{
		#region Private Fields

		private bool _disposed;
		private bool _enabled;

		private bool _monitorMouse = true;
		private bool _monitorKeyboard = true;

		private bool _timeElapsed;
		private bool _reactivated;

		private Timer _monitorTimer;

		#endregion Private Fields

		#region Constructors

		/// <summary>
		/// Creates a new instance of <see cref="MonitorBase"/> which
		/// includes a <see cref="System.Timers.Timer"/> object
		/// </summary>
		protected MonitorBase()
		{
			_monitorTimer = new Timer();
			_monitorTimer.AutoReset = false;
			_monitorTimer.Elapsed += TimerElapsed;
		}

		#endregion Constructors

		#region Dispose Pattern

		/// <summary>
		/// Unregisters all event handlers and disposes internal objects
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Actual deconstructor in accordance with the dispose pattern
		/// </summary>
		/// <param name="disposing">
		/// True if managed and unmanaged resources will be freed
		/// (otherwise only unmanaged resources are handled)
		/// </param>
		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				_disposed = true;
				if (disposing)
				{
					Delegate[] delegateBuffer = null;

					_monitorTimer.Elapsed -= TimerElapsed;
					_monitorTimer.Dispose();

					delegateBuffer = Elapsed.GetInvocationList();
					foreach (ElapsedEventHandler item in delegateBuffer)
						Elapsed -= item;
					Elapsed = null;

					delegateBuffer = Reactivated.GetInvocationList();
					foreach (EventHandler item in delegateBuffer)
						Reactivated -= item;
					Reactivated = null;
				}
			}
		}

		/// <summary>
		/// Deconstructor method for use by the garbage collector
		/// </summary>
		~MonitorBase()
		{
			Dispose(false);
		}

		#endregion Dispose Pattern

		#region Public Events

		/// <summary>
		/// Occurs when the period of time defined by <see cref="Interval"/>
		/// has passed without any user interaction
		/// </summary>
		public event ElapsedEventHandler Elapsed;

		/// <summary>
		/// Occurs when the user continues to interact with the system after
		/// <see cref="Interval"/> has elapsed
		/// </summary>
		public event EventHandler Reactivated;

		#endregion Public Events

		#region Protected Properties

		/// <summary>
		/// True after <see cref="Elapsed"/> has been raised until
		/// <see cref="Reset"/> is called
		/// </summary>
		protected bool TimeElapsed
		{
			get
			{
				return _timeElapsed;
			}
		}

		/// <summary>
		/// True after <see cref="Reactivated"/> has been raised until
		/// <see cref="Reset"/> is called
		/// </summary>
		protected bool ReactivatedRaised
		{
			get
			{
				return _reactivated;
			}
		}

		#endregion Protected Properties

		#region Public Properties

		/// <summary>
		/// Period of time without user interaction after which
		/// <see cref="Elapsed"/> is raised
		/// </summary>
		public virtual double Interval
		{
			get
			{
				return _monitorTimer.Interval;
			}
			set
			{
				_monitorTimer.Interval = value;
			}
		}

		/// <summary>
		/// Specifies if the instances raises events
		/// </summary>
		public virtual bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				_monitorTimer.Enabled = _enabled = value;
			}
		}

		/// <summary>
		/// Specifies if the instances monitors mouse events
		/// </summary>
		public virtual bool MonitorMouseEvents
		{
			get
			{
				return _monitorMouse;
			}
			set
			{
				_monitorMouse = value;
			}
		}

		/// <summary>
		/// Specifies if the instances monitors keyboard events
		/// </summary>
		public virtual bool MonitorKeyboardEvents
		{
			get
			{
				return _monitorKeyboard;
			}
			set
			{
				_monitorKeyboard = value;
			}
		}

		/// <summary>
		/// Object to use for synchronization (the execution of
		/// event handlers will be marshalled to the thread that
		/// owns the synchronization object)
		/// </summary>
		public virtual ISynchronizeInvoke SynchronizingObject
		{
			get
			{
				return _monitorTimer.SynchronizingObject;
			}
			set
			{
				_monitorTimer.SynchronizingObject = value;
			}
		}

		#endregion Properties

		#region Public Methods

		/// <summary>
		/// Resets the internal timer and status information
		/// </summary>
		public virtual void Reset()
		{
			if (_disposed)
				throw new ObjectDisposedException("Object has already been disposed");

			if (_enabled)
			{
				_monitorTimer.Interval = _monitorTimer.Interval;
				_timeElapsed = false;
				_reactivated = false;
			}
		}

		#endregion Public Methods

		#region Proteced Methods

		/// <summary>
		/// Method to raise the <see cref="Elapsed"/> event
		/// (performs consistency checks before raising <see cref="Elapsed"/>)
		/// </summary>
		/// <param name="e">
		/// <see cref="ElapsedEventArgs"/> object provided by the internal timer object
		/// </param>
		protected void OnElapsed(ElapsedEventArgs e)
		{
			_timeElapsed = true;
			if (Elapsed != null && _enabled && (_monitorKeyboard || _monitorMouse))
				Elapsed(this, e);
		}

		/// <summary>
		/// Method to raise the <see cref="Reactivated"/> event (performs
		/// consistency checks before raising <see cref="Reactivated"/>)
		/// </summary>
		/// <param name="e">
		/// <see cref="EventArgs"/> object
		/// </param>
		protected void OnReactivated(EventArgs e)
		{
			_reactivated = true;
			if (Reactivated != null && _enabled && (_monitorKeyboard || _monitorMouse))
				Reactivated(this, e);
		}

		#endregion Proteced Methods

		#region Private Methods

		private void TimerElapsed(object sender, ElapsedEventArgs e)
		{
			if (_monitorTimer.SynchronizingObject != null)
				if (_monitorTimer.SynchronizingObject.InvokeRequired)
				{
					_monitorTimer.SynchronizingObject.BeginInvoke(
						new ElapsedEventHandler(TimerElapsed),
						new object[] { sender, e });
					return;
				}
			OnElapsed(e);
		}

		#endregion Private Methods
	}
}