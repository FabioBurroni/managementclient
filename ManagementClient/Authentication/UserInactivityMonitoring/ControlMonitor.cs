// ControlMonitor Class
// Revision 2 (2004-12-22)
// Copyright (C) 2004 Dennis Dietrich
//
// Released unter the BSD License
// http://www.opensource.org/licenses/bsd-license.php
// http://www.codeproject.com/Articles/9104/How-to-check-for-user-inactivity-with-and-without

using System;
using System.Windows.Forms;

namespace Authentication.UserInactivityMonitoring
{
  /// <summary>
  /// Class for monitoring user inactivity without any platform invokes
  /// </summary>
  internal class ControlMonitor : MonitorBase
  {
    #region Private Fields

    private bool _disposed;

    private Control _targetControl;

    #endregion Private Fields

    #region Public Properties

    /// <summary>
    /// Specifies if the instances monitors mouse events
    /// </summary>
    public override bool MonitorMouseEvents
    {
      get
      {
        return base.MonitorMouseEvents;
      }
      set
      {
        if (_disposed)
          throw new ObjectDisposedException("Object has already been disposed");

        if (base.MonitorMouseEvents != value)
        {
          if (value)
            RegisterMouseEvents(_targetControl);
          else
            UnRegisterMouseEvents(_targetControl);
          base.MonitorMouseEvents = value;
        }
      }
    }

    /// <summary>
    /// Specifies if the instances monitors keyboard events
    /// </summary>
    public override bool MonitorKeyboardEvents
    {
      get
      {
        return base.MonitorKeyboardEvents;
      }
      set
      {
        if (_disposed)
          throw new ObjectDisposedException("Object has already been disposed");

        if (base.MonitorKeyboardEvents != value)
        {
          if (value)
            RegisterKeyboardEvents(_targetControl);
          else
            UnRegisterKeyboardEvents(_targetControl);
          base.MonitorKeyboardEvents = value;
        }
      }
    }

    #endregion Public Properties

    #region Constructors

    /// <summary>
    /// Creates a new instance of <see cref="ControlMonitor"/>
    /// </summary>
    /// <param name="target">
    /// The control (including all child controls) to be monitored
    /// </param>
    public ControlMonitor(Control target) : base()
    {
      if (target == null)
        throw new ArgumentException("Parameter target must not be null");
      _targetControl = target;
      ControlAdded(this, new ControlEventArgs(_targetControl));
      if (MonitorKeyboardEvents)
        RegisterKeyboardEvents(_targetControl);
      if (MonitorMouseEvents)
        RegisterMouseEvents(_targetControl);
    }

    #endregion Constructors

    #region Protected Methods

    /// <summary>
    /// Actual deconstructor in accordance with the dispose pattern
    /// </summary>
    /// <param name="disposing">
    /// True if managed and unmanaged resources will be freed
    /// (otherwise only unmanaged resources are handled)
    /// </param>
    protected override void Dispose(bool disposing)
    {
      if (!_disposed)
      {
        _disposed = true;
        if (disposing)
        {
          ControlRemoved(this, new ControlEventArgs(_targetControl));
          if (MonitorKeyboardEvents)
            UnRegisterKeyboardEvents(_targetControl);
          if (MonitorMouseEvents)
            UnRegisterMouseEvents(_targetControl);
          _targetControl = null;

        }
      }
      base.Dispose(disposing);
    }

    #endregion

    #region Private Methods

    private void RegisterKeyboardEvents(Control c)
    {
      c.KeyDown += KeyboardEventOccured;
      c.KeyUp += KeyboardEventOccured;
      foreach (Control item in c.Controls)
        RegisterKeyboardEvents(item);
    }

    private void UnRegisterKeyboardEvents(Control c)
    {
      c.KeyDown -= KeyboardEventOccured;
      c.KeyUp -= KeyboardEventOccured;
      foreach (Control item in c.Controls)
        UnRegisterKeyboardEvents(item);
    }

    private void RegisterMouseEvents(Control c)
    {
      c.MouseDown += MouseEventOccured;
      c.MouseUp += MouseEventOccured;
      c.MouseMove += MouseEventOccured;
      c.MouseWheel += MouseEventOccured;
      foreach (Control item in c.Controls)
        RegisterMouseEvents(item);
    }

    private void UnRegisterMouseEvents(Control c)
    {
      c.MouseDown -= MouseEventOccured;
      c.MouseUp -= MouseEventOccured;
      c.MouseMove -= MouseEventOccured;
      c.MouseWheel -= MouseEventOccured;
      foreach (Control item in c.Controls)
        UnRegisterMouseEvents(item);
    }

    private void MouseEventOccured(object sender, MouseEventArgs e)
    {
      ResetBase();
    }

    private void KeyboardEventOccured(object sender, KeyEventArgs e)
    {
      ResetBase();
    }

    private void ResetBase()
    {
      if (TimeElapsed && !ReactivatedRaised)
        OnReactivated(new EventArgs());
      base.Reset();
    }

    private void ControlAdded(object sender, ControlEventArgs e)
    {
      e.Control.ControlAdded += ControlAdded;
      e.Control.ControlRemoved += ControlRemoved;
      foreach (Control item in e.Control.Controls)
        ControlAdded(this, new ControlEventArgs(item));
    }

    private void ControlRemoved(object sender, ControlEventArgs e)
    {
      e.Control.ControlAdded -= ControlAdded;
      e.Control.ControlRemoved -= ControlRemoved;
      foreach (Control item in e.Control.Controls)
        ControlRemoved(this, new ControlEventArgs(item));
    }

    #endregion Private Methods
  }
}
