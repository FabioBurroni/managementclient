﻿using System;
using System.Collections.Generic;

namespace Authentication
{
  #region Public Delegate and EventArgs

  public delegate void CloseRequestHandler(object sender, EventArgs e);

  public delegate void LoginSessionHandler(object sender, LoginSessionEventArgs sessionArgs);
  public delegate void LogoutSessionHandler(object sender, LogoutSessionEventArgs sessionArgs);

  public class SessionEventArgs : EventArgs
  {
    /// <summary>
    /// Sessione oggetto dell'evento
    /// </summary>
    public ISession Session { get; }

    public SessionEventArgs(ISession session)
    {
      if (session == null)
        throw new ArgumentNullException(nameof(session));

      Session = session;
    }
  }

  public class LoginSessionEventArgs : SessionEventArgs
  {
    /// <summary>
    /// Posizioni aggiunte alla sessione
    /// </summary>
    public IList<string> AddedPositions { get; }

    /// <summary>
    /// Costruttore della classe per l'evento di login
    /// </summary>
    /// <param name="session">Sessione oggetto della chiamata</param>
    /// <param name="addedPositions">Posizioni aggiunte</param>
    public LoginSessionEventArgs(ISession session, IEnumerable<string> addedPositions)
      : base(session)
    {
      AddedPositions = addedPositions != null ? new List<string>(addedPositions) : new List<string>();
    }
  }

  public class LogoutSessionEventArgs : SessionEventArgs
  {
    /// <summary>
    /// Posizioni rimosse dalla sessione
    /// </summary>
    public IList<string> RemovedPositions { get; }

    /// <summary>
    /// Indica se la sessione è stata terminata o è ancora valida
    /// </summary>
    public bool IsSessionTerminated { get; }

    /// <summary>
    /// Costruttore della classe per l'evento di logout
    /// </summary>
    /// <param name="session">Indirizzo ip</param>
    /// <param name="removedPositions">Posizioni rimosse dalla sessione</param>
    /// <param name="isSessionTerminated">Sessione terminata o no</param>
    public LogoutSessionEventArgs(ISession session, IEnumerable<string> removedPositions, bool isSessionTerminated)
      : base(session)
    {
      RemovedPositions = removedPositions != null ? new List<string>(removedPositions) : new List<string>();
      IsSessionTerminated = isSessionTerminated;
    }
  }

  #endregion

  #region Internal Delegate and EventArgs

  internal delegate void AuthenticationResponseHandler(object sender, AuthenticationResponseEventArgs eventArgs);

  internal class AuthenticationResponseEventArgs : EventArgs
  {
    public IAuthenticationResponse AuthenticationResponse { get; }

    public AuthenticationResponseEventArgs(IAuthenticationResponse response)
    {
      if (response == null)
        throw new NullReferenceException(nameof(response));

      AuthenticationResponse = response;
    }
  }

  #endregion
}