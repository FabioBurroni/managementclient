using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace Authentication.Utils
{
  /// <summary>
  /// Gestore del file di log
  /// </summary>
  internal static class Logging
  {
    #region Fields

    private static readonly string PathFile = Path.Combine(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), "Log");
    private static readonly Logger Logger = Logger.Instance;

    #endregion

    #region Properties

    /// <summary>
    /// Propriet�in sola lettura che restituisce il nome dell'applicazione
    /// </summary>
    public static string appName
    {
      get
      {
        //return AppDomain.CurrentDomain.FriendlyName.Substring(0,AppDomain.CurrentDomain.FriendlyName.IndexOf("."));
        return Path.GetFileNameWithoutExtension(Process.GetCurrentProcess().MainModule.ModuleName);
      }
    }

    #endregion

    #region Public Methods

    #region Error

    /// <summary>
    /// Scrive l'errore contenuto nell'eccezione nel log di errore
    /// </summary>
    /// <param name="vSource">Classe e metodo che ha gestito l'eccezione</param>
    /// <param name="ex">Eccezione da scrivere nel log</param>
    public static void Error(string vSource, Exception ex)
    {
      Error(vSource, null, null, ex);
    }

    /// <summary>
    /// Scrive l'errore contenuto nell'eccezione nel log di errore
    /// </summary>
    /// <param name="vSource">Classe e metodo che ha gestito l'eccezione</param>
    /// <param name="vMessage">Messaggio aggiuntivo da scrivere nel log</param>
    /// <param name="ex">Eccezione da scrivere nel log</param>
    public static void Error(string vSource, string vMessage, Exception ex)
    {
      Error(vSource, null, vMessage, ex);
    }

    public static void Error(string vSource, string pathFile, string vMessage, Exception ex)
    {
      string errorMessage = string.Format("[{0}]", vSource);
      if (ex != null && ex.Message.Length > 0)
        errorMessage += "\r\n -> Errore: " + ex.Message + " in " + ex.Source + "\r\n" + ex.StackTrace;
      if (!string.IsNullOrEmpty(vMessage))
        errorMessage += "\r\n -> Commento: " + vMessage;
      //WriteToFile(AppDomain.CurrentDomain.SetupInformation.ApplicationName,pathFile,ErrorMessage,true);
      if (pathFile == null)
        pathFile = PathFile;
      WriteToFile(appName, pathFile, errorMessage, true);
    }

    #endregion

    #region ToFile

    /// <summary>
    /// Scrive il messaggio testuale nel file specificato
    /// </summary>
    /// <param name="vFile">File di log</param>
    /// <param name="vMessage">Messaggio</param>
    public static void ToFile(string vFile, string vMessage)
    {
      WriteToFile(vFile, vMessage);
    }

    /// <summary>
    /// Scrive il messaggio testuale nel file specificato. vMessageFormat e vMessageArgs compongono il messaggio stesso
    /// formattandolo come fosse string.Format()
    /// </summary>
    /// <param name="vFile">File di log</param>
    /// <param name="vMessageFormat">Messaggio da formattare</param>
    /// <param name="vMessageArgs">Argomenti del messaggio</param>
    public static void ToFile(string vFile, string vMessageFormat, params object[] vMessageArgs)
    {
      WriteToFile(vFile, string.Format(vMessageFormat, vMessageArgs));
    }

    /// <summary>
    /// Scrive il messaggio testuale nel file specificato
    /// </summary>
    /// <param name="vFile">File di log</param>
    /// <param name="pathFile">Percorso del file, se diverso da quello dell'applicativo</param>
    /// <param name="vMessage">Messaggio</param>
    public static void ToFile(string vFile, string pathFile, string vMessage)
    {
      WriteToFile(vFile, pathFile, vMessage, false);
    }

    /// <summary>
    /// Scrive il messaggio testuale nel file specificato. vMessageFormat e vMessageArgs compongono il messaggio stesso
    /// formattandolo come fosse string.Format()
    /// </summary>
    /// <param name="vFile">File di log</param>
    /// <param name="pathFile">Percorso del file, se diverso da quello dell'applicativo</param>
    /// <param name="vMessageFormat">Messaggio da formattare</param>
    /// <param name="vMessageArgs">Argomenti del messaggio</param>
    public static void ToFile(string vFile, string pathFile, string vMessageFormat, params object[] vMessageArgs)
    {
      WriteToFile(vFile, pathFile, string.Format(vMessageFormat, vMessageArgs), false);
    }

    #endregion

    #region Message

    /// <summary>
    /// Scrive il messaggio in un file di log con lo stesso nome dell'applicazione
    /// </summary>
    /// <param name="vMessage">Messaggio</param>
    public static void Message(string vMessage)
    {
      WriteToFile(appName, vMessage);
    }

    /// <summary>
    /// Scrive il messaggio in un file di log con lo stesso nome dell'applicazione. vMessageFormat e vMessageArgs compongono il messaggio stesso
    /// formattandolo come fosse string.Format()
    /// </summary>
    /// <param name="vMessageFormat">Messaggio da formattare</param>
    /// <param name="vMessageArgs">Argomenti del messaggio</param>
    public static void Message(string vMessageFormat, params object[] vMessageArgs)
    {
      WriteToFile(appName, string.Format(vMessageFormat, vMessageArgs));
    }

    /// <summary>
    /// Scrive il messaggio in un file di log
    /// </summary>
    /// <param name="pathFile">Percorso del file, se diverso da quello dell'applicativo</param>
    /// <param name="vMessage">Messaggio</param>
    public static void Message(string pathFile, string vMessage)
    {
      WriteToFile(appName, pathFile, vMessage, false);
    }

    /// <summary>
    /// Scrive il messaggio in un file di log. vMessageFormat e vMessageArgs compongono il messaggio stesso
    /// formattandolo come fosse string.Format()
    /// </summary>
    /// <param name="pathFile">Percorso del file, se diverso da quello dell'applicativo</param>
    /// <param name="vMessageFormat">Messaggio da formattare</param>
    /// <param name="vMessageArgs">Argomenti del messaggio</param>
    public static void Message(string pathFile, string vMessageFormat, params object[] vMessageArgs)
    {
      WriteToFile(appName, pathFile, string.Format(vMessageFormat, vMessageArgs), false);
    }

    #endregion

    #region WriteToFile

    /// <summary>
    /// Scrive il messaggio in un file di log
    /// </summary>
    /// <param name="vFile">Nome del file di log</param>
    /// <param name="vMessage">Messaggio</param>
    public static void WriteToFile(string vFile, string vMessage)
    {
      WriteToFile(vFile, PathFile, vMessage, false);
    }

    /// <summary>
    /// Scrive il messaggio in un file di log. vMessageFormat e vMessageArgs compongono il messaggio stesso
    /// formattandolo come fosse string.Format()
    /// </summary>
    /// <param name="vFile">Nome del file di log</param>
    /// <param name="vMessageFormat">Messaggio da formattare</param>
    /// <param name="vMessageArgs">Argomenti del messaggio</param>
    public static void WriteToFile(string vFile, string vMessageFormat, params object[] vMessageArgs)
    {
      WriteToFile(vFile, PathFile, string.Format(vMessageFormat, vMessageArgs), false);
    }

    /// <summary>
    /// Scrive il messaggio in un file di log
    /// </summary>
    /// <param name="vFile">Nome del file di log</param>
    /// <param name="vMessage">Messaggio</param>
    /// <param name="vErrorSuffix">Specifica se inserire il suffisso _ERR nel nome del file</param>
    public static void WriteToFile(string vFile, string vMessage, bool vErrorSuffix)
    {
      WriteToFile(vFile, PathFile, vMessage, vErrorSuffix);
    }

    /// <summary>
    /// Scrive il messaggio in un file di log
    /// </summary>
    /// <param name="vFile">Nome del file di log</param>
    /// <param name="pathFile">Percorso del file, se diverso da quello dell'applicativo</param>
    /// <param name="vMessage">Messaggio</param>
    /// <param name="vErrorSuffix">Specifica se inserire il suffisso _ERR nel nome del file</param>
    public static void WriteToFile(string vFile, string pathFile, string vMessage, bool vErrorSuffix)
    {
      Logger.LogMessage(vFile, pathFile, vMessage, vErrorSuffix);
    }

    #endregion

    #endregion

    #region Extensions

    /// <summary>
    /// Scrive il messaggio in un file di log con lo stesso nome dell'applicazione
    /// </summary>
    /// <param name="msg">Messaggio</param>
    public static void WriteToLog(this string msg)
    {
      Message(msg);
    }

    /// <summary>
    /// Scrive il messaggio in un file di log con lo stesso nome dell'applicazione
    /// </summary>
    /// <param name="vMessageFormat">>Messaggio da formattare</param>
    /// <param name="vMessageArgs">Argomenti del messaggio</param>
    public static void WriteToLog(this string vMessageFormat, params object[] vMessageArgs)
    {
      Message(string.Format(vMessageFormat, vMessageArgs));
    }

    /// <summary>
    /// Scrive il messaggio in un file di log
    /// </summary>
    /// <param name="msg">Messaggio da formattar</param>
    /// <param name="extension">Estensione del file di log</param>
    public static void WriteToLog(this string msg, string extension)
    {
      WriteToFile(appName + extension, msg);
    }

    #endregion
  }

  internal sealed class Logger : IDisposable
  {
    #region Fields

    private const string DateTimeFormat = "yyyy/MM/dd HH.mm.ss.fff";
    private const string ErrorSuffix = "_ERR";

    private readonly double _maxFileLength = 4 * Math.Pow(2, 20);

    private string _fileExtension = ".log";

    private readonly Queue<LogInfo> _logQueue = new Queue<LogInfo>();
    private readonly ManualResetEvent _hasNewItems = new ManualResetEvent(false);
    private readonly ManualResetEvent _terminate = new ManualResetEvent(false);
    private readonly ManualResetEvent _waiting = new ManualResetEvent(false);

    private readonly Thread _loggerThread;

    #region Clean

    private readonly object _cleanUpLocker = new object();
    private readonly int _maxLogStorageDays = 180; //numero massimo dei giorni di mantenimento dei log
    private Timer _cleanUpLogTimer; //timer di pulizia per i log
    private bool _needToCleanUpLog;

    #endregion

    #endregion

    #region Properties

    /// <summary>
    /// Propriet� in lettura/scrittura per specificare l'estensione del file di log. Per default � .log
    /// </summary>
    public string FileExtension
    {
      get { return _fileExtension; }
      set { _fileExtension = value; }
    }

    #endregion

    #region Constructor

    private Logger()
    {
      _loggerThread = new Thread(ProcessQueue) { IsBackground = true };
      _loggerThread.Start();

      StartTimer();
    }

    public static Logger Instance { get { return LoggerNested.instance; } }

    private class LoggerNested
    {
      // Explicit static constructor to tell C# compiler
      // not to mark type as beforefieldinit
      static LoggerNested()
      {
      }

      internal static readonly Logger instance = new Logger();
    }

    #endregion

    #region Public Methods

    public void LogMessage(string vFile, string pathFile, string vMessage, bool vErrorSuffix)
    {
      EnqueueToMessageQueue(new LogInfo(vFile, pathFile, vMessage, vErrorSuffix));
    }

    public void Flush()
    {
      _waiting.WaitOne();
    }

    #endregion

    #region Private Methods

    #region Clean Up Log

    private void StartTimer()
    {
      _cleanUpLogTimer = new Timer(CleanUpLogCallback, null, TimeSpan.FromSeconds(15), TimeSpan.FromMilliseconds(-1));
    }

    private void StopTimer()
    {
      RescheduleTimer(TimeSpan.FromMilliseconds(-1), true);
    }

    private void CleanUpLogCallback(object state)
    {
      _needToCleanUpLog = true;
    }

    /// <summary>
    /// Restituisce il tempo rimanente da ora fino al primo giorno del prossimo mese
    /// </summary>
    private static TimeSpan GetLeftTimeToNextFirstDayofMonth()
    {
      var now = DateTime.Now;
      var today = DateTime.Today;
      DateTime firstDayNextMonth = today.AddMonths(1).AddDays(1 - today.Day);

      return firstDayNextMonth - now;
    }

    private void CleanUpLog(string path)
    {
      if (!_needToCleanUpLog)
        return;

      try
      {
        if (!Directory.Exists(path))
          return;

        //scorro i file nella directory
        foreach (var fileName in Directory.GetFiles(path))
        {
          //controllo se � un file log, se non lo � non lo cancello
          if (!string.Equals(Path.GetExtension(fileName), _fileExtension, StringComparison.OrdinalIgnoreCase))
            continue;

          try
          {
            var fileInfo = new FileInfo(fileName);

            //controllo se l'ultima scrittura del file risale al massimo dei giorni configurati
            if ((DateTime.UtcNow - fileInfo.LastWriteTimeUtc).TotalDays > _maxLogStorageDays)
            {
              //� da cancellare
              File.Delete(fileName);
            }
          }
          catch (Exception)
          { }
        }
      }
      catch (Exception)
      { }

      _needToCleanUpLog = false;

      //rischedulo il timer per il prossimo giorno del mese
      RescheduleTimer(GetLeftTimeToNextFirstDayofMonth());
    }

    private void RescheduleTimer(TimeSpan newDueTime, bool dispose = false)
    {
      lock (_cleanUpLocker)
      {
        _cleanUpLogTimer?.Change(newDueTime, TimeSpan.FromMilliseconds(-1));

        if (dispose)
        {
          _cleanUpLogTimer?.Dispose();
          _cleanUpLogTimer = null;
        }
      }
    }

    #endregion

    private void EnqueueToMessageQueue(LogInfo log)
    {
      if (log == null)
        return;

      lock (_logQueue)
      {
        _logQueue.Enqueue(log);
      }
      _hasNewItems.Set();
    }

    private int QueueCount()
    {
      lock (_logQueue)
      {
        return _logQueue.Count;
      }
    }

    private void ProcessQueue()
    {
      while (true)
      {
        _waiting.Set();

        int i = WaitHandle.WaitAny(new WaitHandle[] { _hasNewItems, _terminate });
        // terminate was signaled 
        if (i == 1 && QueueCount() == 0) return;

        _hasNewItems.Reset();
        _waiting.Reset();

        Queue<LogInfo> queueCopy;
        lock (_logQueue)
        {
          queueCopy = new Queue<LogInfo>(_logQueue);
          _logQueue.Clear();
        }

        foreach (var log in queueCopy)
        {
          WriteLog(log);
        }
      }
    }

    private void WriteLog(LogInfo log)
    {
      if (log == null)
        return;

      var vFile = log.vFile;
      var pathFile = log.pathFile;
      var vMessage = log.vMessage;
      var vErrorSuffix = log.vErrorSuffix;
      var logDateBorn = log.DateBorn;

      try
      {
        if (pathFile != null)
        {
          //if (pathFile.Substring(pathFile.Length - 1) != "\\" && pathFile.Substring(pathFile.Length - 1) != "/")
          //  pathFile += "\\";
        }
        else
        {
          pathFile = "";
        }

        if (!Directory.Exists(pathFile))
          Directory.CreateDirectory(pathFile);

        //pulisco i vecchi file di log
        CleanUpLog(pathFile);

        //string fileName =  vFile + "_" + logDateBorn.ToString("yyMM") + ((vErrorSuffix) ? ErrorSuffix : "") + FileExtension;
        string fileName = string.Format("{0}_{1}{2}{3}", vFile, logDateBorn.ToString("yyMM"), vErrorSuffix ? ErrorSuffix : "", FileExtension);
        string filePath = Path.Combine(pathFile, fileName);

        if (File.Exists(filePath))
        {
          if (GetFileLen(filePath) > _maxFileLength)
          {
            var fileNameOld = string.Format("OLD_{0}", fileName);
            var filePathOld = Path.Combine(pathFile, fileNameOld);

            File.Copy(filePath, filePathOld, true);
            File.Delete(filePath);
          }
        }

        using (var sw = new StreamWriter(filePath, true, Encoding.UTF8))
        {
          //sw.WriteLine("[" + DateTime.Now.ToString("yyyy/MM/dd HH.mm.ss.fff") + "] " + vMessage);
          sw.WriteLine("[{0}] {1}", logDateBorn.ToString(DateTimeFormat), vMessage);
        }
      }
      catch (Exception)
      {
        //Console.WriteLine(ex.StackTrace + " " + ex.Message);
      }
    }

    private static long GetFileLen(string filePath)
    {
      return new FileInfo(filePath).Length;
    }

    #endregion

    #region IDisposable Members

    private bool _disposed;

    // implements IDisposable
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (!_disposed)
      {
        // if this is a dispose call dispose on all state you
        // hold, and take yourself off the Finalization queue.
        if (disposing)
        {
          // get rid of managed resources
        }

        // get rid of unmanaged resources
        StopTimer();
        _terminate.Set();
        _loggerThread.Join();

        // free your own state (unmanaged objects)
        _disposed = true;
      }
    }

    // finalizer simply calls Dispose(false)
    ~Logger()
    {
      Dispose(false);
    }

    #endregion

    #region Class Message

    private class LogInfo
    {
      public string vFile { get; }
      public string pathFile { get; }
      public string vMessage { get; }
      public bool vErrorSuffix { get; }
      public DateTime DateBorn { get; }

      public LogInfo(string vFile, string pathFile, string vMessage, bool vErrorSuffix)
      {
        this.vFile = vFile;
        this.pathFile = pathFile;
        this.vMessage = vMessage;
        this.vErrorSuffix = vErrorSuffix;
        DateBorn = DateTime.Now;
      }
    }

    #endregion
  }
}