﻿using System.Collections.Generic;
using System.Linq;
using Authentication.Extensions;
using Authentication.Profiling;
using XmlCommunicationManager.Common.Xml;

namespace Authentication
{
  public static class Profiler
  {
    #region Fields

    private static readonly AuthenticationManager AuthenticationManager = AuthenticationManager.Instance;

    #endregion

    #region Internal Methods

    /// <summary>
    /// Restituisce le UserStructure nel pacchetto di risposta
    /// </summary>
    internal static IList<UserStructure> CreateUserStructureCollection(XmlCommandResponse xmlCmdRes)
    {
      var usList = new List<UserStructure>();

      if (xmlCmdRes == null)
        return usList;

      foreach (var item in xmlCmdRes.Items)
      {
        int i = 1;

        //dati struttura
        var enabled = item.getFieldVal(i++).ConvertTo<bool>();
        var optionalData = item.getFieldVal(i++);

        //dati azione
        var actionId = item.getFieldVal(i++).ConvertTo<int>();
        var actionCode = item.getFieldVal(i++);
        var actionDescr = item.getFieldVal(i++);
        var actionStandard = item.getFieldVal(i++).ConvertTo<bool>();

        //dati client
        var clientId = item.getFieldVal(i++).ConvertTo<int>();
        var clientCode = item.getFieldVal(i++);
        var clientDescr = item.getFieldVal(i++);

        //dati profilo
        var profileId = item.getFieldVal(i++).ConvertTo<int>();
        var profileCode = item.getFieldVal(i++);
        var profileDescr = item.getFieldVal(i++);
        var profileHierarchy = item.getFieldVal(i++).ConvertTo<int>();

        //recupero l'azione (se esiste) così non creo due istanze uguali
        var action = usList.Select(us => us.UserAction).FirstOrDefault(us => us.Id == actionId);
        if (action == null)
          action = new UserAction(actionId, actionCode, actionDescr, actionStandard);

        //recupero il client (se esiste) così non creo due istanze uguali
        var client = usList.Select(uc => uc.UserClient).FirstOrDefault(uc => uc.Id == clientId);
        if (client == null)
          client = new UserClient(clientId, clientCode, clientDescr);

        //recupero il profilo (se esiste) così non creo due istanze uguali
        var profile = usList.Select(up => up.UserProfile).FirstOrDefault(up => up.Id == profileId);
        if (profile == null)
          profile = new UserProfile(profileId, profileCode, profileDescr, profileHierarchy);

        var structure = new UserStructure(action, client, profile, enabled, optionalData);

        usList.Add(structure);
      }

      return usList;
    }

    /// <summary>
    /// Restituisce le UserStructure nel pacchetto di risposta. Si può specificare l'indice da cui partire per leggere la risposta
    /// </summary>
    internal static IList<UserStructureBase> CreateUserStructureCollection(XmlCommandResponse xmlCmdRes, int startIndex)
    {
      var usList = new List<UserStructureBase>();

      if (xmlCmdRes == null)
        return usList;

      foreach (var item in xmlCmdRes.Items)
      {
        int i = startIndex;

        //dati struttura
        var enabled = item.getFieldVal(i++).ConvertTo<bool>();
        var optionalData = item.getFieldVal(i++);

        //dati azione
        var actionId = item.getFieldVal(i++).ConvertTo<int>();
        var actionCode = item.getFieldVal(i++);
        var actionDescr = item.getFieldVal(i++);
        var actionStandard = item.getFieldVal(i++).ConvertTo<bool>();

        //dati client
        var clientId = item.getFieldVal(i++).ConvertTo<int>();
        var clientCode = item.getFieldVal(i++);
        var clientDescr = item.getFieldVal(i++);

        //dati profilo
        var profileId = item.getFieldVal(i++).ConvertTo<int>();
        var profileCode = item.getFieldVal(i++);
        var profileDescr = item.getFieldVal(i++);
        var profileHierarchy = item.getFieldVal(i++).ConvertTo<int>();

        //20210607 il profiler andava in errore in caso di risposta non ok...
        //Nella risposta non venivano restituite le righe in errore
        if (string.IsNullOrEmpty(actionCode))
          continue;
        if (string.IsNullOrEmpty(clientCode))
          continue;
        if (string.IsNullOrEmpty(profileCode))
          continue; 
        if (optionalData == null)
          continue;

        var structure = new UserStructureBase(actionCode, clientCode, profileCode, enabled, optionalData);

        usList.Add(structure);
      }

      return usList;
    }

    internal static UserProfile CreateUserProfile(XmlCommandResponse.Item item)
    {
      var i = 1;

      var id = item.getFieldVal(i++).ConvertTo<int>();
      var code = item.getFieldVal(i++);
      var descr = item.getFieldVal(i++);
      var hierarchy = item.getFieldVal(i).ConvertTo<int>();

      return new UserProfile(id, code, descr, hierarchy);
    }

    internal static IList<UserProfile> CreateUserProfileCollection(XmlCommandResponse xmlResp)
    {
      var profiles = new List<UserProfile>();

      foreach (var item in xmlResp.Items)
      {
        var profile = CreateUserProfile(item);

        profiles.Add(profile);
      }

      return profiles;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Indica se nel contesto attuale, l'azione è permessa
    /// </summary>
    public static bool IsActionAllowed(string actionCode)
    {
      string optionalData;
      return IsActionAllowed(actionCode, out optionalData);
    }

    /// <summary>
    /// Indica se nel contesto attuale, l'azione è permessa
    /// </summary>
    public static bool IsActionAllowed(string actionCode, out string optionalData)
    {
      optionalData = string.Empty;

      if (string.IsNullOrEmpty(actionCode))
        return false;

      var session = AuthenticationManager.Session;

      if (session == null)
        return false;

      return session.User.IsActionAllowed(actionCode);
    }

    /// <summary>
    /// Restituisce tutte le strutture utente
    /// </summary>
    public static IList<UserStructureBase> GetUserStructures()
    {
      var session = AuthenticationManager.Session;

      if (session == null)
        return new List<UserStructureBase>();

      return session.User.UserStructures;
    }

    #endregion
  }
}