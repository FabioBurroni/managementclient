﻿using System.Collections.Generic;

namespace Authentication
{
  /// <summary>
  /// Istanza di base per gli errori di autenticazione generati.
  /// </summary>
  public abstract class ErrorCodeMapper : IErrorCodeMapper
  {
    protected Dictionary<int, string> ErrorCodeDictionary { get; }

    private const string ErrorCodeNotDefined = "Error Code Not Defined";

    protected ErrorCodeMapper()
    {
      ErrorCodeDictionary = new Dictionary<int, string>();
    }

    #region IErrorCodeMapper

    public virtual bool TryGetDescription(int errorCode, out string descr)
    {
      var found = ErrorCodeDictionary.TryGetValue(errorCode, out descr);

      if (!found)
      {
        descr = ErrorCodeNotDefined;
      }

      return found;
    }

    public virtual string this[int errorCode]
    {
      get
      {
        string descr;
        TryGetDescription(errorCode, out descr);
        return descr;
      }
    }

    #endregion
  }
}