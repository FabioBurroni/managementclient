﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Authentication.Extensions;
using XmlCommunicationManager.Authorization;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.Extensions;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlClient.Event;
using XmlCommunicationManager.XmlClient.Manager;
using XmlCommunicationManager.XmlServer;
using XmlCommunicationManager.XmlServer.Event;

namespace Authentication
{
	public sealed class AuthenticationManager : IDisposable
	{
		#region Events

		internal event AuthenticationResponseHandler AuthenticationResponseReceived;

		/// <summary>
		/// Evento per richiedere la chiusura del form di autenticazione o di tutta l'applicazione, dipende dalle esigenze
		/// </summary>
		public event CloseRequestHandler CloseRequest;

		/// <summary>
		/// Evento lanciato in seguito ad un login totale o parziale
		/// </summary>
		public event LoginSessionHandler LoginSession;

		/// <summary>
		/// Evento lanciato in seguito ad un logout totale o parziale
		/// </summary>
		public event LogoutSessionHandler LogoutSession;

		#endregion

		#region Fields

		private readonly object _locker = new object();

		private ISession _session;

		private double _inactivityTimeoutMilliseconds = TimeSpan.FromMinutes(15).TotalMilliseconds;

		private int _maxUnreceivedUpdateSessionActivity = 1;
		private int _unreceivedUpdateSessionActivityCounter;

		private AuthenticationMode _authenticationMode;

		private CommandManager _commandManager;
		private CommandManagerNotifier _cmNotifier;

		private IXmlClient _xmlClient;
		private IXmlServer _xmlServer;

		private ILoginParameters _loginParameters;

		private Type _authenticationResponseType; //tipo di risposta da istanziare

		private readonly IErrorCodeMapper _localErrorCodeMapper = new LocalErrorCodeMapper();
		private IErrorCodeMapper _remoteErrorCodeMapper;

		private UserInactivityController _userInactivityController;

		#endregion

		#region Singleton

		private AuthenticationManager()
		{
		}

		public static AuthenticationManager Instance { get { return AuthenticationManagerNested.instance; } }

		private class AuthenticationManagerNested
		{
			// Explicit static constructor to tell C# compiler
			// not to mark type as beforefieldinit
			static AuthenticationManagerNested()
			{
			}

			internal static readonly AuthenticationManager instance = new AuthenticationManager();
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// Istanza della sessione attuale
		/// </summary>
		public ISession Session
		{
			get
			{
				lock (_locker)
				{
					return _session;
				}
			}
			private set
			{
				lock (_locker)
				{
					_session = value;
					SetSessionTokens(_session?.SessionTokens);
				}
			}
		}

		/// <summary>
		/// Indica se il client che usa la libreria è un figlio degli altri client oppure
		/// è il padre. Se è il padre ha la capacità di sloggare i suoi figli
		/// </summary>
		public bool IsChildClient { get; set; } = true;

		/// <summary>
		/// Tempo di inattività del computer oltre il quale viene mandato il logout.
		/// Il default è 15 minuti, il valore è rappresentato in millisecondi.
		/// Settare questa proprietà prima di configurare il manager. In caso di timeout minore o uguale a zero,
		/// il controllo sull'inattività non viene fatto
		/// </summary>
		public double InactivityTimeoutMilliseconds
		{
			get
			{
				lock (_locker)
				{
					return _inactivityTimeoutMilliseconds;
				}
			}
			set
			{
				lock (_locker)
				{
					if (Math.Abs(_inactivityTimeoutMilliseconds - value) > double.Epsilon)
					{
						_inactivityTimeoutMilliseconds = value;
						InvalidateUserInactivityController();
					}
				}
			}
		}

		/// <summary>
		/// Specifica l'eventuale preambolo da utilizzare per l'invio dei comandi
		/// </summary>
		public string XmlCommandPreamble { get; set; }

		/// <summary>
		/// Numero massimo di risposte consecutive non ricevute per il rinfresco della sessione.
		/// Raggiunto il valore impostato, la sessione scade e viene effettuato il logout forzato.
		/// </summary>
		public int MaxUnreceivedUpdateSessionActivity
		{
			get { return _maxUnreceivedUpdateSessionActivity; }
			set
			{
				if (value <= 0 || value > 32)
					throw new Exception($"{nameof(MaxUnreceivedUpdateSessionActivity)} must be between 1 and 32");

				_maxUnreceivedUpdateSessionActivity = value;
			}
		}

		/// <summary>
		/// Indica se il numero massimo delle risposte consecutive non ricevute dal server è stato raggiunto
		/// </summary>
		public bool ReachedMaxUnreceivedUpdateSessionActivity
		{
			get { return _unreceivedUpdateSessionActivityCounter >= MaxUnreceivedUpdateSessionActivity; }
		}

		#endregion

		#region Internal Properties

		/// <summary>
		/// Modalità di autenticazione
		/// </summary>
		internal AuthenticationMode AuthenticationMode => _authenticationMode;

		/// <summary>
		/// Istanza di CommandManager (se presente)
		/// </summary>
		internal CommandManager CommandManager => _commandManager;

		/// <summary>
		/// Istanza di XmlClient (se presente)
		/// </summary>
		internal IXmlClient XmlClient => _xmlClient;

		#endregion

		#region Public Methods

		/// <summary>
		/// Setta i dati necessari la manager per poter comunicare con il server
		/// </summary>
		/// <param name="commandManager">Istanza del command manager</param>
		/// <param name="xmlServer">Istanza di xml server</param>
		/// <param name="loginParameters">Parametri di login</param>
		/// <param name="authenticationResponse">Istanza vuota della risposta da utilizzare</param>
		/// <param name="remoteErrorCodeMapper">Istanza che definisce i codici di errore</param>
		public void Configure(CommandManager commandManager, IXmlServer xmlServer, ILoginParameters loginParameters, IAuthenticationResponse authenticationResponse, IErrorCodeMapper remoteErrorCodeMapper)
		{
			SetCommandManager(commandManager);
			SetXmlServer(xmlServer);
			SetLoginParameters(loginParameters);
			SetAuthenticationResponse(authenticationResponse);
			SetRemoveErrorCodeMapper(remoteErrorCodeMapper);
		}

		/// <summary>
		/// Setta i dati necessari la manager per poter comunicare con il server
		/// </summary>
		/// <param name="xmlClient">Istanza di xml client</param>
		/// <param name="xmlServer">Istanza di xml server</param>
		/// <param name="loginParameters">Parametri di login</param>
		/// <param name="authenticationResponse">Istanza vuota della risposta da utilizzare</param>
		/// <param name="remoteErrorCodeMapper">Istanza che definisce i codici di errore</param>
		public void Configure(IXmlClient xmlClient, IXmlServer xmlServer, ILoginParameters loginParameters, IAuthenticationResponse authenticationResponse, IErrorCodeMapper remoteErrorCodeMapper)
		{
			SetXmlClient(xmlClient);
			SetXmlServer(xmlServer);
			SetLoginParameters(loginParameters);
			SetAuthenticationResponse(authenticationResponse);
			SetRemoveErrorCodeMapper(remoteErrorCodeMapper);
		}

		/// <summary>
		/// Fa partire il manager e, se settato, inizia a mandare i comandi per ereditare la sessione (se lecito). Gli deve essere passata l'istanza delle credenziali da usare.
		/// Attenzione, non chiamare questo metodo se si usa una interfaccia grafica! Chiamare direttamente questo metodo solo in caso di gestione senza finestre
		/// </summary>
		public void Start(ICredentials credentials, bool tryToInheritSession)
		{
			//controllo se ho settato la modalità
			if (_authenticationMode == AuthenticationMode.None)
				throw new Exception($"Set Authentication Mode before start, call method {nameof(SetCommandManager)} or method {nameof(SetXmlClient)}");

			SetCredentials(credentials);

			if (tryToInheritSession)
				SendLoginCommand();
		}

		/// <summary>
		/// Ferma il manager, spedendo il comando di logout (se la sessione è presente) e pulisce i dati pendenti
		/// </summary>
		public void StopAndClean()
		{
			//NOTE l'ordine di queste chiamate è fondamentale, non cambiarle!

			var session = Session;
			if (session != null)
			{
				//se è un child, può mandare solo il suo token, altrimenti se si slogga lui, vengono sloggati anche gli altri
				var token = IsChildClient ? session.SessionTokens.ChildToken : session.SessionTokens.RootToken;
				SendCommand_Logout(token);
			}

			SetCredentials(null);
			ResetCommandManagerOrXmlClient();
			ResetXmlServer();

			if (session != null)
			{
				//importante, questa deve essere l'ultima istruzione perchè dentro ci sono gli eventi
				ManageNotReceivedLogoutResponse(); //forzo subito il logout, non aspetto la risposta  
			}
		}

		#endregion

		#region Internal Methods

		#region RequestCloseApplication


		#endregion

		/// <summary>
		/// Permette di mandare l'evento di richiesta chiusura applicazione
		/// </summary>
		internal void RequestCloseApplication()
		{
			CloseRequest?.Invoke(this, new EventArgs());
		}

		internal void SendLogin(ICredentials credentials)
		{
			//setto le credenziali, mando il comando e le pulisco subito, username e password non devono essere mantenute in memoria

			SetCredentials(credentials);
			SendLoginCommand();
			//SetCredentials(null); //TODO per ora commento
		}

		/// <summary>
		/// Ottiene la traduzione dell'errore cercando nel mapper remoto e locale
		/// </summary>
		internal string GetDescriptionForErrorCode(int errorCode)
		{
			string descr;

			//cerco prima negli errori locali
			if (_localErrorCodeMapper.TryGetDescription(errorCode, out descr))
				return descr;

			//non l'ho trovato, cerco negli errori remoti e comunque vada restituisco qualcosa
			_remoteErrorCodeMapper.TryGetDescription(errorCode, out descr);
			return descr;
		}

		#endregion

		#region Private Methods

		#region Set Configuration

		/// <summary>
		/// Setta la modalità di autenticazine come CommandManager
		/// </summary>
		private void SetCommandManager(CommandManager commandManager)
		{
			if (commandManager == null)
				throw new ArgumentNullException(nameof(commandManager));

			//modalità già settata
			if (_authenticationMode == AuthenticationMode.CommandManager)
				return;

			if (_authenticationMode != AuthenticationMode.None)
				throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

			_authenticationMode = AuthenticationMode.CommandManager;

			_cmNotifier = new CommandManagerNotifier(this, GetHashCode().ToString());
			_cmNotifier.notifyCallBack += NotifyCallback;

			_commandManager = commandManager;
			_commandManager.addCommandManagerNotifier(_cmNotifier);
		}

		/// <summary>
		/// Setta la modalità di autenticazine come XmlClient
		/// </summary>
		private void SetXmlClient(IXmlClient xmlClient)
		{
			if (xmlClient == null)
				throw new ArgumentNullException(nameof(xmlClient));

			//modalità già settata
			if (_authenticationMode == AuthenticationMode.XmlClient)
				return;

			if (_authenticationMode != AuthenticationMode.None)
				throw new Exception($"AuthenticationMode already setted to value '{_authenticationMode}'");

			_authenticationMode = AuthenticationMode.XmlClient;

			_xmlClient = xmlClient;

			_xmlClient.OnException += XmlClientOnException;
			_xmlClient.OnRetrieveReplyFromServer += XmlClientOnRetrieveReplyFromServer;
		}

		private void SetXmlServer(IXmlServer xmlServer)
		{
			if (xmlServer == null)
				throw new ArgumentNullException(nameof(xmlServer));

			if (_xmlServer != null)
				throw new Exception($"{nameof(xmlServer)} already configured");

			_xmlServer = xmlServer;

			xmlServer.OnRequest += XmlServerOnRequest;
		}

		/// <summary>
		/// Setta i parametri di login
		/// TODO permettere in qualche modo di manipolare i codici delle posizioni, quindi aggiunta e rimozione
		/// </summary>
		private void SetLoginParameters(ILoginParameters loginParameters)
		{
			if (loginParameters == null)
				throw new ArgumentNullException(nameof(loginParameters));

			_loginParameters = loginParameters;
		}

		private void SetAuthenticationResponse(IAuthenticationResponse authenticationResponse)
		{
			if (authenticationResponse == null)
				throw new ArgumentNullException(nameof(authenticationResponse));

			_authenticationResponseType = authenticationResponse.GetType();
		}

		private void SetRemoveErrorCodeMapper(IErrorCodeMapper remoteErrorCodeMapper)
		{
			if (remoteErrorCodeMapper == null)
				throw new ArgumentNullException(nameof(remoteErrorCodeMapper));

			_remoteErrorCodeMapper = remoteErrorCodeMapper;
		}

		private void SetCredentials(ICredentials credentials)
		{
			if (_loginParameters != null)
				_loginParameters.Credentials = credentials;
		}

		private void ResetCommandManagerOrXmlClient()
		{
			//resetto il token
			SetSessionTokens(null);

			if (_authenticationMode == AuthenticationMode.CommandManager)
			{
				_cmNotifier.notifyCallBack -= NotifyCallback;
				_commandManager.removeCommandManagerNotifier(_cmNotifier);

				_cmNotifier = null;
				_commandManager = null;
			}
			else if (_authenticationMode == AuthenticationMode.XmlClient)
			{
				_xmlClient.OnException -= XmlClientOnException;
				_xmlClient.OnRetrieveReplyFromServer -= XmlClientOnRetrieveReplyFromServer;

				_xmlClient = null;
			}

			//resetto l'autenticazione
			_authenticationMode = AuthenticationMode.None;
		}

		private void ResetXmlServer()
		{
			if (_xmlServer != null) _xmlServer.OnRequest -= XmlServerOnRequest;
			_xmlServer = null;
		}

		#endregion

		#region Get Configuration

		private IList<string> GetLoginParameters()
		{
			if (_loginParameters == null)
				throw new NullReferenceException("Login Parameters not configured");

			return _loginParameters.GetParameters();
		}

		#endregion

		#region Authentication Response

		internal IAuthenticationResponse CreateAndFillAuthenticationResponse(IList<XmlCommandResponse.Item> parameters)
		{
			var response = (IAuthenticationResponse)Activator.CreateInstance(_authenticationResponseType);
			response.FullFill(parameters);

			return response;
		}

		internal IAuthenticationResponse CreateAndFillAuthenticationResponse(int result, IList<string> errorList)
		{
			var response = (IAuthenticationResponse)Activator.CreateInstance(_authenticationResponseType);
			response.PartialFillByResultAndErrors(result, errorList);

			return response;
		}

		internal IAuthenticationResponse CreateAndFillAuthenticationResponse(IList<string> sessionData)
		{
			var response = (IAuthenticationResponse)Activator.CreateInstance(_authenticationResponseType);
			response.PartialFillBySession(sessionData);

			return response;
		}

		#endregion

		#region Session

		private bool IsSessionActive()
		{
			int childToken;
			return IsSessionActive(out childToken);
		}

		private bool IsSessionActive(out int childToken)
		{
			bool isActive;

			lock (_locker)
			{
				isActive = _session != null && _session.IsActive;
				childToken = isActive ? _session.SessionTokens.ChildToken : 0;
			}

			return isActive;
		}

		#endregion

		#region Manage Response

		#region Login

		private void ManageReceivedLoginResponse(IAuthenticationResponse response)
		{
			//devo capire quello che mi ha risposto il server

			ISession sessionForEvent = null;
			IList<string> addedPositionsForEvent = null;

			lock (_locker)
			{
				if (response.Session != null)
				{
					if (_session == null)
					{
						//se non ho sessione e mi arriva una sessione valida, entro
						_session = response.Session;

						sessionForEvent = _session;
						addedPositionsForEvent = response.Session.PositionCollection;
					}
					else if (_session != null && _session.SessionTokens.ChildToken == response.Session.SessionTokens.ChildToken)
					{
						//i due token sono uguali, quindi la mia sessione è sempre la stessa, controllo se ho aggiunto posizioni

						//recupero le nuove posizioni aggiunte
						addedPositionsForEvent = response.Session.PositionCollection.Except(_session.PositionCollection, StringComparer.OrdinalIgnoreCase).ToList();

						if (addedPositionsForEvent.Any())
							sessionForEvent = _session;
					}

					//mi sono loggato, setto i token su xmlclient o commandmanager
					SetSessionTokens(response.Session.SessionTokens);
				}
			}

			//notifico l'interfaccia del risultato del login (bene o male che sia andato) per mostrare errore o ok
			RaiseAuthenticationResponse(response);

			if (sessionForEvent != null)
			{
				//faccio partire il timer di refresh della sessione
				StartTimerUpdateSessionActivity();

				//se ho la sessione avvalorata, allora sparo l'evento di login
				RaiseLoginEvent(sessionForEvent, addedPositionsForEvent);

				//attivo il monitor per il controllo inattività
				StartOrUpdateUserInactivityController();
			}
		}

		private void ManageNotReceivedLoginResponse()
		{
			//non mi è arrivata risposta dal server, vado in timeout

			//rimando il comando di login fra x secondi
			SendLoginCommand(TimeSpan.FromSeconds(20));

			//creo la risposta del login senza connessione con il server
			var response = CreateAndFillAuthenticationResponse(LocalErrorCodeMapper.ServerNotReachable, null);

			//notifico l'interfaccia del risultato senza connessione del login
			RaiseAuthenticationResponse(response);
		}

		#endregion

		#region UpdateSessionActivity

		private void ManageReceivedUpdateSessionActivityResponse(IAuthenticationResponse response)
		{
			ManageUpdateSessionActivityResponse(response.Session);
		}

		private void ManageNotReceivedUpdateSessionActivityResponse()
		{
			ManageUpdateSessionActivityResponse(null);
		}

		private void ManageUpdateSessionActivityResponse(ISession responseSession)
		{
			//devo capire quello che mi ha risposto il server

			#region UnreceivedUpdateSessionActivityCounter

			if (responseSession != null)
			{
				//ho ricevuto risposta dal server, resetto il conteggio delle risposte non ricevute
				_unreceivedUpdateSessionActivityCounter = 0;
			}
			else
			{
				//risposta non ricevuta, incremento il conteggio
				_unreceivedUpdateSessionActivityCounter++;
			}

			#endregion

			if (ReachedMaxUnreceivedUpdateSessionActivity ||
					(responseSession != null && responseSession?.IsActive == false) ||
					(responseSession != null && responseSession?.ContainsAllPositions(responseSession.PositionCollection) == false))
			{
				//non ho ricevuto risposta dal server da x tentativi oppure sessione non attiva oppure non tutte le posizioni sono quelle che mi aspetto, chiamo il logout
				ManageNotReceivedLogoutResponse();
			}
			else
			{
				//sessione ancora viva, non faccio niente, rischedulo e basta il timer
				RescheduleTimerUpdateSessionActivity();
			}
		}

		#endregion

		#region Logout

		private void ManageReceivedLogoutResponse(IAuthenticationResponse response)
		{
			ManageLogoutResponse(response.Session);
		}

		private void ManageNotReceivedLogoutResponse()
		{
			ManageLogoutResponse(null);
		}

		private void ManageLogoutResponse(ISession responseSession)
		{
			//devo capire quello che mi ha risposto il server

			bool isSessionTerminated = false;
			ISession sessionForEvent = null;
			IList<string> removedPositionsForEvent = null;

			lock (_locker)
			{
				if ((responseSession == null /*&& _session != null*/) || (_session != null && responseSession != null && _session.SessionTokens.ChildToken == responseSession.SessionTokens.ChildToken))
				{
					//ho sessione e mi arriva il mio logout
					sessionForEvent = _session;

					if (responseSession == null || !responseSession.IsActive)
					{
						isSessionTerminated = true;

						if (_session != null)
						{
							//se la sessione non è più attiva, esco
							_session.IsActive = false;
							removedPositionsForEvent = _session.PositionCollection;
						}

						//resetto la sessione
						Session = null;
					}
					else
					{
						//la sessione è attiva, ho avuto solo una rimozione parziale di posizioni

						var oldPositions = _session.PositionCollection;
						var newPositions = responseSession.PositionCollection;

						//recupero le posizioni rimosse
						removedPositionsForEvent = oldPositions.Except(newPositions).ToList();

						//rimuovo le posizioni dalla sessione
						_session.RemoveRangePosition(removedPositionsForEvent);
					}
				}
			}

			if (sessionForEvent != null)
			{
				Utils.Logging.Message(string.Format("Logout session: RootToken={0} ChildToken={1}", sessionForEvent.SessionTokens.RootToken, sessionForEvent.SessionTokens.ChildToken));

				//fermo tutto solo se la sessione è terminata
				if (isSessionTerminated)
				{
					//fermo il timer di refresh della sessione
					StopTimerUpdateSessionActivity();

					//attivo il monitor per il controllo inattività
					StopUserInactivityController();
				}

				//se ho la sessione avvalorata, allora sparo l'evento di login
				RaiseLogoutEvent(sessionForEvent, removedPositionsForEvent, isSessionTerminated);
			}
		}

		#endregion

		#endregion

		#region Raise Event

		private void RaiseAuthenticationResponse(IAuthenticationResponse response)
		{
			AuthenticationResponseReceived?.Invoke(this, new AuthenticationResponseEventArgs(response));
		}

		private void RaiseLoginEvent(ISession session, IEnumerable<string> addedPositions)
		{
			LoginSession?.Invoke(this, new LoginSessionEventArgs(session, addedPositions));
		}

		private void RaiseLogoutEvent(ISession session, IEnumerable<string> removedPositions, bool isSessionTerminated)
		{
			LogoutSession?.Invoke(this, new LogoutSessionEventArgs(session, removedPositions, isSessionTerminated));
		}

		#endregion

		#region User Inactivity

		private void StartOrUpdateUserInactivityController()
		{
			lock (_locker)
			{
				var timeout = InactivityTimeoutMilliseconds;

				//verifico se il timeout è maggiore di 0
				if (timeout > 0)
				{
					if (_userInactivityController == null)
					{
						//il controller non è creato, lo faccio ora
						_userInactivityController = new UserInactivityController(timeout);
						_userInactivityController.InactiveUserTimeout += UserInactivityControllerOnInactiveUserTimeout;
					}
					else
					{
						//il controller già esiste, aggiorno solo il timeout
						_userInactivityController.InactivityTimeout = timeout;
					}
				}
			}
		}

		private void StopUserInactivityController()
		{
			lock (_locker)
			{
				if (_userInactivityController != null)
				{
					_userInactivityController.InactiveUserTimeout -= UserInactivityControllerOnInactiveUserTimeout;
					_userInactivityController.Dispose();
				}

				_userInactivityController = null;
			}
		}

		private void InvalidateUserInactivityController()
		{
			lock (_locker)
			{
				var timeout = InactivityTimeoutMilliseconds;

				if (timeout <= 0)
				{
					//il nuovo timeout è minore od uguale a 0, fermo il controllo di inattività
					StopUserInactivityController();
				}
				else if (Session != null)
				{
					//il nuovo timeout è maggiore di zero, aggiorno il mio valore
					StartOrUpdateUserInactivityController();
				}
			}
		}

		private void UserInactivityControllerOnInactiveUserTimeout(object sender, EventArgs eventArgs)
		{
			//ho raggiunto il timeout, mando il logout
			StopAndClean();
		}

		#endregion

		#region Tokens

		private void SetSessionTokens(SessionTokens tokens)
		{
			if (_authenticationMode.Equals(AuthenticationMode.CommandManager))
			{
				_commandManager.SessionTokens = tokens;
			}
			else if (_authenticationMode.Equals(AuthenticationMode.XmlClient))
			{
				_xmlClient.SessionTokens = tokens;
			}
		}

		#endregion

		#region Timer Update Session Activity

		private Timer _timerUpdateSessionActivity;
		private readonly TimeSpan _timerUpdateSessionActivityCycleTime = TimeSpan.FromSeconds(30);

		private void StartTimerUpdateSessionActivity()
		{
			RescheduleTimerUpdateSessionActivity();
		}

		private void RescheduleTimerUpdateSessionActivity(bool stopTimer = false)
		{
			lock (_locker)
			{
				if (stopTimer)
				{
					_timerUpdateSessionActivity?.Change(Timeout.Infinite, Timeout.Infinite);
					_timerUpdateSessionActivity = null;

					return;
				}

				//schedulo il timer solo se la sessione è attiva
				if (!IsSessionActive())
					return;

				if (_timerUpdateSessionActivity == null)
				{
					_timerUpdateSessionActivity = new Timer(TimerUpdateSessionActivityCallback);
				}

				_timerUpdateSessionActivity?.Change(_timerUpdateSessionActivityCycleTime, TimeSpan.FromMilliseconds(-1));
			}
		}

		private void StopTimerUpdateSessionActivity()
		{
			RescheduleTimerUpdateSessionActivity(true);
		}

		private void TimerUpdateSessionActivityCallback(object state)
		{
			//mando il comando di rinfresco sessione solo se la sessione è attiva
			int childToken;
			if (!IsSessionActive(out childToken))
				return;

			SendCommand_UpdateSessionActivity(childToken);
		}

		#endregion

		#region Send Login Command

		private void SendLoginCommand()
		{
			SendLoginCommand(TimeSpan.Zero);
		}

		/// <summary>
		/// Manda il comando di login
		/// </summary>
		/// <param name="delay">Ritardo per il comando</param>
		private void SendLoginCommand(TimeSpan delay)
		{
			//non accetto ritardi negativi, quindi spedisco subito
			if (delay <= TimeSpan.Zero)
			{
				SendCommand_Login();
				return;
			}

			//è stato messo un tempino di attesa, aspetto il timeout e sparo il comando
			Action sendLoginAction = delegate
			{
				Thread.Sleep(delay);
				SendCommand_Login();
			};
			sendLoginAction.BeginInvoke(sendLoginAction.EndInvoke, null);
		}

		#endregion

		#region XmlClient / CommandManager

		#region Richieste

		private const string CommandLogin = "Login";
		private const string CommandLogout = "Logout";
		private const string CommandUpdateSessionActivity = "UpdateSessionActivity";

		private void SendCommand_Login()
		{
			string cmd = $"{XmlCommandPreamble}{CommandLogin}".CreateXmlCommand(GetLoginParameters());
			SendCommand(cmd);
		}

		private void SendCommand_Logout(int token)
		{
			string cmd = $"{XmlCommandPreamble}{CommandLogout}".CreateXmlCommand(token);
			SendCommand(cmd);
		}

		private void SendCommand_UpdateSessionActivity(int childToken)
		{
			string cmd = $"{XmlCommandPreamble}{CommandUpdateSessionActivity}".CreateXmlCommand(childToken);
			SendCommand(cmd);
		}

		private void SendCommand(string command)
		{
			//controllo se ho settato la modalità
			if (_authenticationMode == AuthenticationMode.None)
			{
				//throw new Exception($"Set Authentication Mode before start, call method {nameof(Configure)} or  method {nameof(Configure)}");
				return;
			}

			if (_authenticationMode == AuthenticationMode.XmlClient)
			{
				var xmlCmd = _xmlClient.CreateXmlCommand(command, "cassioli");
				_xmlClient.ProcessXmlDoc(xmlCmd, "custom", this);
			}
			else if (_authenticationMode == AuthenticationMode.CommandManager)
			{
				_cmNotifier.sendCommand(command, "custom");
			}
		}

		#endregion

		#region Risposte

		#region CommandManager

		private void NotifyCallback(CommandManagerNotifier.NotifyObject notifyObject)
		{
			var xmlCmd = notifyObject.xmlCommand;
			var xmlCmdRes = notifyObject.iXmlStreamer as XmlCommandResponse;

			ManageResponse(xmlCmd, xmlCmdRes);
		}

		#endregion

		#region XmlClient

		private void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs msgEventArgs)
		{
			var xmlCmd = msgEventArgs.xmlCommand as XmlCommand;
			var xmlCmdRes = msgEventArgs.iXmlStreamer as XmlCommandResponse;

			ManageResponse(xmlCmd, xmlCmdRes);
		}

		private void XmlClientOnException(object sender, ExceptionEventArgs exEventArgs)
		{
			var xmlCmd = exEventArgs.xmlCommand as XmlCommand;
			XmlCommandResponse xmlCmdRes = null;

			ManageResponse(xmlCmd, xmlCmdRes);
		}

		#endregion

		#region ManageResponse

		private void ManageResponse(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
		{
			var commandMethodName = xmlCmd.name.TrimPreamble();

			#region CommandLogin

			if (commandMethodName.EqualsConsiderCase(CommandLogin))
			{
				Response_Login(xmlCmd, xmlCmdRes);
			}

			#endregion

			#region CommandUpdateSessionActivity

			else if (commandMethodName.EqualsConsiderCase(CommandUpdateSessionActivity))
			{
				Response_UpdateSessionActivity(xmlCmd, xmlCmdRes);
			}

			#endregion

			#region CommandLogout

			else if (commandMethodName.EqualsConsiderCase(CommandLogout))
			{
				Response_Logout(xmlCmd, xmlCmdRes);
			}

			#endregion
		}

		#endregion

		#endregion

		#region Gestione Risposte

		private void Response_Login(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
		{
			if (xmlCmdRes != null)
			{
				//recupero la risposta
				var response = CreateAndFillAuthenticationResponse(xmlCmdRes.Items);

				ManageReceivedLoginResponse(response);
			}
			else
			{
				//risposta non ricevuta
				ManageNotReceivedLoginResponse();
			}
		}

		private void Response_UpdateSessionActivity(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
		{
			if (xmlCmdRes != null)
			{
				//recupero la risposta
				var response = CreateAndFillAuthenticationResponse(xmlCmdRes.Items);

				ManageReceivedUpdateSessionActivityResponse(response);
			}
			else
			{
				//risposta non ricevuta
				ManageNotReceivedUpdateSessionActivityResponse();

				Utils.Logging.Message("Non ricevuta risposta per l'attività di sessione");
			}
		}

		private void Response_Logout(XmlCommand xmlCmd, XmlCommandResponse xmlCmdRes)
		{
			if (xmlCmdRes != null)
			{
				//recupero la risposta
				var response = CreateAndFillAuthenticationResponse(xmlCmdRes.Items);

				ManageReceivedLogoutResponse(response);
			}
			else
			{
				//risposta non ricevuta
				ManageNotReceivedLogoutResponse();
			}
		}

		#endregion

		#endregion

		#region Xml Server 

		private void XmlServerOnRequest(object sender, XmlOnRequestEventArgs e)
		{
			var xc = (XmlCommand)e.request.request;

			string commandName = xc.commandMethodName;

			//...preparazione risposta
			XmlCommandResponse xmlCmdRes = new XmlCommandResponse(xc);

			if (commandName.EqualsConsiderCase("NotificationLogout"))
			{
				Utils.Logging.Message("Ricevuta notifica di logout");

				//forza il client a cercare nuovi aggiornamenti software
				var response = CreateAndFillAuthenticationResponse(xc.commandMethodParameters);

				ManageReceivedLogoutResponse(response);
			}

			//...si manda indietro la risposta (vuota, tanto serve solo a chiudere in modo opportuno lo scambio)
			XmlResponse xmlr = new XmlResponse(e.request);
			xmlr.setResponse(xmlCmdRes);
			_xmlServer?.SendResponse(xmlr);
		}

		#endregion

		#endregion

		#region IDisposable Support

		private bool _disposedValue; // To detect redundant calls

		private void Dispose(bool disposing)
		{
			if (!_disposedValue)
			{
				if (disposing)
				{
					// dispose managed state (managed objects).
				}

				// free unmanaged resources (unmanaged objects) and override a finalizer below.
				// set large fields to null.

				StopAndClean();

				_disposedValue = true;
			}
		}

		// override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		~AuthenticationManager()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(false);
		}

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// uncomment the following line if the finalizer is overridden above.
			GC.SuppressFinalize(this);
		}

		#endregion
	}
}