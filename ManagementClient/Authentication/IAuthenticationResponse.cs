﻿using System.Collections.Generic;
using Authentication.Profiling;
using XmlCommunicationManager.Common.Xml;

namespace Authentication
{
  /// <summary>
  /// Interfaccia che definisce il tipo di risposta
  /// </summary>
  public interface IAuthenticationResponse
  {
    /// <summary>
    /// Intero che definisce il risultato
    /// </summary>
    int Result { get; }

    /// <summary>
    /// Indica se è in errore o no
    /// </summary>
    bool IsInError { get; }

    /// <summary>
    /// Restituisce OK o NOK
    /// </summary>
    string OkNok { get; }

    /// <summary>
    /// Se il risultato è NOK, possono essere presenti una lista di errori informativi
    /// </summary>
    IList<string> ErrorList { get; }

    /// <summary>
    /// Parametri della sessione
    /// </summary>
    ISession Session { get; }

    /// <summary>
    /// Composizione della struttura utente
    /// </summary>
    IList<UserStructureBase> UserStructureConfiguration { get; }

    /// <summary>
    /// Riempimento parziale solo con risultato e lista degli errori
    /// </summary>
    void PartialFillByResultAndErrors(int result, IList<string> errorList);

    /// <summary>
    /// Riempimento parziale solo con i dati di sessione
    /// </summary>
    void PartialFillBySession(IList<string> sessionData);

    /// <summary>
    /// Legge una XmlCommandResponse e setta i dati nelle apposite strutture dati
    /// </summary>
    void FullFill(IList<XmlCommandResponse.Item> parameters);
  }
}