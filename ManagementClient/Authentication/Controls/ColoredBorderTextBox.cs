﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Authentication.Controls
{
  internal class ColoredBorderTextBox : TextBox
  {
    [DllImport("user32")]
    private static extern IntPtr GetWindowDC(IntPtr hwnd);

    struct RECT
    {
      public float Left, Top, Right, Bottom;
    }

    struct NCCALSIZE_PARAMS
    {
      public RECT newWindow;
      public RECT oldWindow;
      public RECT clientWindow;
      IntPtr windowPos;
    }

    float _clientPadding = 2;
    int _actualBorderWidth = 4;
    Color _borderColor = Color.Red;
    protected override void WndProc(ref Message m)
    {
      //We have to change the clientsize to make room for borders
      //if not, the border is limited in how thick it is.
      if (m.Msg == 0x83) //WM_NCCALCSIZE   
      {
        if (m.WParam == IntPtr.Zero)
        {
          RECT rect = (RECT)Marshal.PtrToStructure(m.LParam, typeof(RECT));
          rect.Left += _clientPadding;
          rect.Right -= _clientPadding;
          rect.Top += _clientPadding;
          rect.Bottom -= _clientPadding;
          Marshal.StructureToPtr(rect, m.LParam, false);
        }
        else
        {
          NCCALSIZE_PARAMS rects = (NCCALSIZE_PARAMS)Marshal.PtrToStructure(m.LParam, typeof(NCCALSIZE_PARAMS));
          rects.newWindow.Left += _clientPadding;
          rects.newWindow.Right -= _clientPadding;
          rects.newWindow.Top += _clientPadding;
          rects.newWindow.Bottom -= _clientPadding;
          Marshal.StructureToPtr(rects, m.LParam, false);
        }
      }
      if (m.Msg == 0x85) //WM_NCPAINT    
      {
        IntPtr wDC = GetWindowDC(Handle);
        using (Graphics g = Graphics.FromHdc(wDC))
        {
          ControlPaint.DrawBorder(g, new Rectangle(0, 0, Size.Width, Size.Height), _borderColor, _actualBorderWidth, ButtonBorderStyle.Solid,
        _borderColor, _actualBorderWidth, ButtonBorderStyle.Solid, _borderColor, _actualBorderWidth, ButtonBorderStyle.Solid,
        _borderColor, _actualBorderWidth, ButtonBorderStyle.Solid);
        }
        return;
      }
      base.WndProc(ref m);
    }
  }
}
