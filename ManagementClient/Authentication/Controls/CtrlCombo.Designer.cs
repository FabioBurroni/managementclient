using System.ComponentModel;
using System.Windows.Forms;

namespace Authentication.Controls
{
  partial class CtrlCombo
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblValue = new System.Windows.Forms.Label();
      this.pnlBackward = new Authentication.Controls.CtrlCustomPanel();
      this.btnBackward = new System.Windows.Forms.Button();
      this.pnlForward = new Authentication.Controls.CtrlCustomPanel();
      this.btnForward = new System.Windows.Forms.Button();
      this.pnlBackward.SuspendLayout();
      this.pnlForward.SuspendLayout();
      this.SuspendLayout();
      // 
      // lblValue
      // 
      this.lblValue.BackColor = System.Drawing.Color.LightGray;
      this.lblValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblValue.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblValue.Location = new System.Drawing.Point(41, 0);
      this.lblValue.Name = "lblValue";
      this.lblValue.Size = new System.Drawing.Size(83, 32);
      this.lblValue.TabIndex = 7;
      this.lblValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pnlBackward
      // 
      this.pnlBackward.BackColor = System.Drawing.Color.LightBlue;
      this.pnlBackward.BackColor2 = System.Drawing.Color.Blue;
      this.pnlBackward.BorderColor = System.Drawing.SystemColors.MenuHighlight;
      this.pnlBackward.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlBackward.Controls.Add(this.btnBackward);
      this.pnlBackward.Curvature = 20;
      this.pnlBackward.CurveMode = ((Authentication.Controls.CornerCurveMode)((Authentication.Controls.CornerCurveMode.TopLeft | Authentication.Controls.CornerCurveMode.BottomLeft)));
      this.pnlBackward.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlBackward.ForeColor = System.Drawing.SystemColors.ControlText;
      this.pnlBackward.GradientMode = Authentication.Controls.LinearGradientMode.Vertical;
      this.pnlBackward.Location = new System.Drawing.Point(0, 0);
      this.pnlBackward.Name = "pnlBackward";
      this.pnlBackward.Size = new System.Drawing.Size(41, 32);
      this.pnlBackward.TabIndex = 6;
      // 
      // btnBackward
      // 
      this.btnBackward.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnBackward.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnBackward.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnBackward.ForeColor = System.Drawing.Color.White;
      this.btnBackward.Location = new System.Drawing.Point(0, 0);
      this.btnBackward.Name = "btnBackward";
      this.btnBackward.Size = new System.Drawing.Size(40, 32);
      this.btnBackward.TabIndex = 9;
      this.btnBackward.Text = "<<";
      this.btnBackward.UseVisualStyleBackColor = true;
      this.btnBackward.Click += new System.EventHandler(this.btnBackward_Click);
      // 
      // pnlForward
      // 
      this.pnlForward.BackColor = System.Drawing.Color.LightBlue;
      this.pnlForward.BackColor2 = System.Drawing.Color.Blue;
      this.pnlForward.BorderColor = System.Drawing.SystemColors.MenuHighlight;
      this.pnlForward.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlForward.Controls.Add(this.btnForward);
      this.pnlForward.Curvature = 20;
      this.pnlForward.CurveMode = ((Authentication.Controls.CornerCurveMode)((Authentication.Controls.CornerCurveMode.TopRight | Authentication.Controls.CornerCurveMode.BottomRight)));
      this.pnlForward.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnlForward.GradientMode = Authentication.Controls.LinearGradientMode.Vertical;
      this.pnlForward.Location = new System.Drawing.Point(124, 0);
      this.pnlForward.Name = "pnlForward";
      this.pnlForward.Size = new System.Drawing.Size(40, 32);
      this.pnlForward.TabIndex = 5;
      // 
      // btnForward
      // 
      this.btnForward.Dock = System.Windows.Forms.DockStyle.Right;
      this.btnForward.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnForward.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnForward.ForeColor = System.Drawing.Color.White;
      this.btnForward.Location = new System.Drawing.Point(0, 0);
      this.btnForward.Name = "btnForward";
      this.btnForward.Size = new System.Drawing.Size(40, 32);
      this.btnForward.TabIndex = 8;
      this.btnForward.Text = ">>";
      this.btnForward.UseVisualStyleBackColor = true;
      this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
      // 
      // CtrlCombo
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lblValue);
      this.Controls.Add(this.pnlBackward);
      this.Controls.Add(this.pnlForward);
      this.Name = "CtrlCombo";
      this.Size = new System.Drawing.Size(164, 32);
      this.pnlBackward.ResumeLayout(false);
      this.pnlForward.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private CtrlCustomPanel pnlForward;
    private CtrlCustomPanel pnlBackward;
    private Button btnForward;
    private Button btnBackward;
    private Label lblValue;
  }
}
