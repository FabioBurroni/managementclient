	using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Authentication.Controls
{
  [ToolboxBitmap(typeof(Panel))]
  internal partial class CtrlCustomPanel : Panel
  {
    // Fields
    private Color _backColour1 = SystemColors.Window;
    private Color _backColour2 = SystemColors.Window;
    private LinearGradientMode _gradientMode = LinearGradientMode.None;
    private BorderStyle _borderStyle = BorderStyle.None;
    private Color _borderColour = SystemColors.WindowFrame;
    private int _borderWidth = 1;
    private int _curvature;
    private bool _doubleBuffered;

    // Properties
    //   Shadow the Backcolor property so that the base class will still render with a transparent backcolor
    private CornerCurveMode _curveMode = CornerCurveMode.All;

    [DefaultValue(typeof(Color), "Window"), Category("Appearance"), Description("The primary background color used to display text and graphics in the control.")]
    public new Color BackColor
    {
      get
      {
        return _backColour1;
      }
      set
      {
        _backColour1 = value;
        if (DesignMode)
        {
          Invalidate();
        }
      }
    }

    [DefaultValue(typeof(Color), "Window"), Category("Appearance"), Description("The secondary background color used to paint the control.")]
    public Color BackColor2
    {
      get
      {
        return _backColour2;
      }
      set
      {
        _backColour2 = value;
        if (DesignMode)
        {
          Invalidate();
        }
      }
    }

    [DefaultValue(typeof(LinearGradientMode), "None"), Category("Appearance"), Description("The gradient direction used to paint the control.")]
    public LinearGradientMode GradientMode
    {
      get
      {
        return _gradientMode;
      }
      set
      {
        _gradientMode = value;
        if (DesignMode)
        {
          Invalidate();
        }
      }
    }

    [DefaultValue(typeof(BorderStyle), "None"), Category("Appearance"), Description("The border style used to paint the control.")]
    public new BorderStyle BorderStyle
    {
      get
      {
        return _borderStyle;
      }
      set
      {
        _borderStyle = value;
        if (DesignMode)
        {
          Invalidate();
        }
      }
    }

    [DefaultValue(typeof(Color), "WindowFrame"), Category("Appearance"), Description("The border color used to paint the control.")]
    public Color BorderColor
    {
      get
      {
        return _borderColour;
      }
      set
      {
        _borderColour = value;
        if (DesignMode)
        {
          Invalidate();
        }
      }
    }

    [DefaultValue(typeof(int), "1"), Category("Appearance"), Description("The width of the border used to paint the control.")]
    public int BorderWidth
    {
      get
      {
        return _borderWidth;
      }
      set
      {
        _borderWidth = value;
        if (DesignMode)
        {
          Invalidate();
        }
      }
    }

    [DefaultValue(typeof(int), "0"), Category("Appearance"), Description("The radius of the curve used to paint the corners of the control.")]
    public int Curvature
    {
      get
      {
        return _curvature;
      }
      set
      {
        _curvature = value;
        if (DesignMode)
        {
          Invalidate();
        }
      }
    }

    [DefaultValue(typeof(CornerCurveMode), "All"), Category("Appearance"), Description("The style of the curves to be drawn on the control.")]
    public CornerCurveMode CurveMode
    {
      get
      {
        return _curveMode;
      }
      set
      {
        _curveMode = value;
        if (DesignMode)
        {
          Invalidate();
        }
      }
    }

    [DefaultValue(typeof(bool), "false"), Category("Layout"), Description("Use a double buffer for the control.")]
    protected override bool DoubleBuffered
    {
      get
      {
        return _doubleBuffered;
      }
      set
      {
        _doubleBuffered = value;
        base.DoubleBuffered = _doubleBuffered;
        if (DesignMode)
        {
          Invalidate();
        }
      }
    }

    private int AdjustedCurve
    {
      get
      {
        int curve = 0;
        if (_curveMode != CornerCurveMode.None)
        {
          if (_curvature > ClientRectangle.Width / 2)
          {
            curve = DoubleToInt(ClientRectangle.Width / (float)2);
          }
          else
          {
            curve = _curvature;
          }
          if (curve > ClientRectangle.Height / 2)
          {
            curve = DoubleToInt(ClientRectangle.Height / (float)2);
          }
        }
        return curve;
      }
    }

    public CtrlCustomPanel()
    {
      SetDefaultControlStyles();
      CustomInitialization();
    }

    private void SetDefaultControlStyles()
    {
      SetStyle(ControlStyles.DoubleBuffer, true);
      SetStyle(ControlStyles.AllPaintingInWmPaint, false);
      SetStyle(ControlStyles.ResizeRedraw, true);
      SetStyle(ControlStyles.UserPaint, true);
      SetStyle(ControlStyles.SupportsTransparentBackColor, true);
    }

    private void CustomInitialization()
    {
      SuspendLayout();
      base.BackColor = Color.Transparent;
      //BorderStyle = BorderStyle.None;
      _borderStyle = BorderStyle.None;

      ResumeLayout(false);
    }

    protected override void OnPaintBackground(PaintEventArgs pEvent)
    {
      base.OnPaintBackground(pEvent);

      pEvent.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
      using (var graphPath = GetPath())
      {
        //Create Gradient Brush (Cannot be width or height 0)
        LinearGradientBrush filler;
        Rectangle rect = ClientRectangle;
        if (ClientRectangle.Width == 0)
        {
          rect.Width += 1;
        }
        if (ClientRectangle.Height == 0)
        {
          rect.Height += 1;
        }

        if (_gradientMode == LinearGradientMode.None)
        {
          filler = new LinearGradientBrush(rect, _backColour1, _backColour1, System.Drawing.Drawing2D.LinearGradientMode.Vertical);
        }
        else
        {
          filler = new LinearGradientBrush(rect, _backColour1, _backColour2, (System.Drawing.Drawing2D.LinearGradientMode)_gradientMode);
        }
        pEvent.Graphics.FillPath(filler, graphPath);
        filler.Dispose();
        if (_borderStyle == BorderStyle.FixedSingle)
        {
          using (var borderPen = new Pen(_borderColour, _borderWidth))
          {
            pEvent.Graphics.DrawPath(borderPen, graphPath);
          }
        }
        else if (_borderStyle == BorderStyle.Fixed3D)
        {
          DrawBorder3D(pEvent.Graphics, ClientRectangle);
        }
        else if (_borderStyle == BorderStyle.None)
        {
        }
        filler.Dispose();
      }
    }

    protected GraphicsPath GetPath()
    {
      GraphicsPath graphPath = new GraphicsPath();
      if (_borderStyle == BorderStyle.Fixed3D)
      {
        graphPath.AddRectangle(ClientRectangle);
      }
      else
      {
        try
        {
          int curve = 0;
          Rectangle rect = ClientRectangle;
          int offset = 0;
          if (_borderStyle == BorderStyle.FixedSingle)
          {
            if (_borderWidth > 1)
            {
              offset = DoubleToInt(BorderWidth / (float)2);
            }
            curve = AdjustedCurve;
          }
          else if (_borderStyle == BorderStyle.Fixed3D)
          {
          }
          else if (_borderStyle == BorderStyle.None)
          {
            curve = AdjustedCurve;
          }
          if (curve == 0)
          {
            graphPath.AddRectangle(Rectangle.Inflate(rect, -offset, -offset));
          }
          else
          {
            int rectWidth = rect.Width - 1 - offset;
            int rectHeight = rect.Height - 1 - offset;
            int curveWidth;
            if ((_curveMode & CornerCurveMode.TopRight) != 0)
            {
              curveWidth = curve * 2;
            }
            else
            {
              curveWidth = 1;
            }
            graphPath.AddArc(rectWidth - curveWidth, offset, curveWidth, curveWidth, 270, 90);
            if ((_curveMode & CornerCurveMode.BottomRight) != 0)
            {
              curveWidth = curve * 2;
            }
            else
            {
              curveWidth = 1;
            }
            graphPath.AddArc(rectWidth - curveWidth, rectHeight - curveWidth, curveWidth, curveWidth, 0, 90);
            if ((_curveMode & CornerCurveMode.BottomLeft) != 0)
            {
              curveWidth = curve * 2;
            }
            else
            {
              curveWidth = 1;
            }
            graphPath.AddArc(offset, rectHeight - curveWidth, curveWidth, curveWidth, 90, 90);
            if ((_curveMode & CornerCurveMode.TopLeft) != 0)
            {
              curveWidth = curve * 2;
            }
            else
            {
              curveWidth = 1;
            }
            graphPath.AddArc(offset, offset, curveWidth, curveWidth, 180, 90);
            graphPath.CloseFigure();
          }
        }
        catch (Exception)
        {
          graphPath.AddRectangle(ClientRectangle);
        }
      }
      return graphPath;
    }

    public static void DrawBorder3D(Graphics graphics, Rectangle rectangle)
    {
      graphics.SmoothingMode = SmoothingMode.Default;
      graphics.DrawLine(SystemPens.ControlDark, rectangle.X, rectangle.Y, rectangle.Width - 1, rectangle.Y);
      graphics.DrawLine(SystemPens.ControlDark, rectangle.X, rectangle.Y, rectangle.X, rectangle.Height - 1);
      graphics.DrawLine(SystemPens.ControlDarkDark, rectangle.X + 1, rectangle.Y + 1, rectangle.Width - 1, rectangle.Y + 1);
      graphics.DrawLine(SystemPens.ControlDarkDark, rectangle.X + 1, rectangle.Y + 1, rectangle.X + 1, rectangle.Height - 1);
      graphics.DrawLine(SystemPens.ControlLight, rectangle.X + 1, rectangle.Height - 2, rectangle.Width - 2, rectangle.Height - 2);
      graphics.DrawLine(SystemPens.ControlLight, rectangle.Width - 2, rectangle.Y + 1, rectangle.Width - 2, rectangle.Height - 2);
      graphics.DrawLine(SystemPens.ControlLightLight, rectangle.X, rectangle.Height - 1, rectangle.Width - 1, rectangle.Height - 1);
      graphics.DrawLine(SystemPens.ControlLightLight, rectangle.Width - 1, rectangle.Y, rectangle.Width - 1, rectangle.Height - 1);
    }

    private static int DoubleToInt(double value)
    {
      //return Decimal.ToInt32(Decimal.Floor(Decimal.Parse((value).ToString())));
      return (int)Math.Floor(value);
    }
  }
}