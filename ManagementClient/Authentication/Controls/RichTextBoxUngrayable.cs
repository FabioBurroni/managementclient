﻿using System.Drawing;
using System.Windows.Forms;

namespace Authentication.Controls
{
  /// <summary>
  /// RichTextBox identica all'originale. L'unica modifica riguarda la proprietà Enabled, se viene settata
  /// a false, il ForeColor non diventa grigio ma rimane con il suo colore settato. L'effetto collaterale è che
  /// se è disabilitato, gli scroll non funzionano nonostante il word wrap sia attivo
  /// </summary>
  internal class RichTextBoxUngrayable : RichTextBox
  {
    public RichTextBoxUngrayable()
    {
      SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      if (Enabled)
      {
        //use normal realization
        base.OnPaint(e);
        return;
      }

      //drawing text by ForeColor
      using (var brush = new SolidBrush(ForeColor))
      {
        e.Graphics.DrawString(Text, Font, brush, ClientRectangle);
      }
    }
  }
}