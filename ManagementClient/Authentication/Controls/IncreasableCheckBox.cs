﻿using System.Drawing;
using System.Windows.Forms;

namespace Authentication.Controls
{
  internal class IncreasableCheckBox : CheckBox
  {
    public IncreasableCheckBox()
    {
      TextAlign = ContentAlignment.MiddleRight;
    }
    public override bool AutoSize
    {
      get { return base.AutoSize; }
      set { base.AutoSize = false; }
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);
      e.Graphics.Clear(this.BackColor);
      int h = this.ClientSize.Height - 2;
      Rectangle rc = new Rectangle(new Point(0, 1), new Size(h, h));
      ControlPaint.DrawCheckBox(e.Graphics, rc, Checked ? ButtonState.Checked : ButtonState.Normal);
    }
  }
}