﻿namespace Authentication.Controls
{
  internal enum LinearGradientMode
  {
    None = 0,
    Horizontal = 1,
    Vertical = 2,
    ForwardDiagonal = 3,
    BackwardDiagonal = 4,
  }
}