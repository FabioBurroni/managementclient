﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Authentication.Controls
{
  internal class ColoredBorderPanel : Panel
  {
    private Color _borderColor = Color.Blue;
    private int _borderWidth = 5;

    public ColoredBorderPanel()
    {
      SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
    }

    public int BorderWidth
    {
      get { return _borderWidth; }
      set
      {
        _borderWidth = value;
        Invalidate();
        PerformLayout();
      }
    }

    public Color BorderColor
    {
      get { return _borderColor; }
      set { _borderColor = value; Invalidate(); }
    }

    public override Rectangle DisplayRectangle
    {
      get
      {
        return new Rectangle(_borderWidth, _borderWidth, Bounds.Width - _borderWidth * 2, Bounds.Height - _borderWidth * 2);
      }
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      //using (SolidBrush brush = new SolidBrush(BackColor))
      //{
      //  e.Graphics.FillRectangle(brush, ClientRectangle);
      //}

      using (Pen p = new Pen(_borderColor, _borderWidth))
      {
        Rectangle r = ClientRectangle;
        // now for the funky stuff...
        // to get the rectangle drawn correctly, we actually need to 
        // adjust the rectangle as .net centers the line, based on width, 
        // on the provided rectangle.
        r.Inflate(-Convert.ToInt32(_borderWidth / 2.0 + .5), -Convert.ToInt32(_borderWidth / 2.0 + .5));
        e.Graphics.DrawRectangle(p, r);
      }
    }

    protected override void OnResize(EventArgs e)
    {
      base.OnResize(e);
      SetDisplayRectLocation(_borderWidth, _borderWidth);
    }
  }
}