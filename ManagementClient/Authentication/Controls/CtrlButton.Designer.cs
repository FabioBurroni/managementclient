using System.ComponentModel;
using System.Windows.Forms;

namespace Authentication.Controls
{
  internal partial class CtrlButton
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.ctrlPanel = new CtrlCustomPanel();
      this.labText = new System.Windows.Forms.Label();
      this.ctrlPanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // ctrlPanel
      // 
      this.ctrlPanel.BackColor = System.Drawing.Color.LightGreen;
      this.ctrlPanel.BackColor2 = System.Drawing.Color.DarkGreen;
      this.ctrlPanel.BorderColor = System.Drawing.Color.RoyalBlue;
      this.ctrlPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.ctrlPanel.BorderWidth = 2;
      this.ctrlPanel.Controls.Add(this.labText);
      this.ctrlPanel.Curvature = 5;
      this.ctrlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ctrlPanel.GradientMode = LinearGradientMode.Vertical;
      this.ctrlPanel.Location = new System.Drawing.Point(0, 0);
      this.ctrlPanel.Name = "ctrlPanel";
      this.ctrlPanel.Size = new System.Drawing.Size(150, 39);
      this.ctrlPanel.TabIndex = 0;
      // 
      // labText
      // 
      this.labText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.labText.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labText.ForeColor = System.Drawing.Color.White;
      this.labText.Location = new System.Drawing.Point(0, 0);
      this.labText.Name = "labText";
      this.labText.Size = new System.Drawing.Size(150, 39);
      this.labText.TabIndex = 0;
      this.labText.Text = "Text";
      this.labText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.labText.Click += new System.EventHandler(this.labText_Click);
      this.labText.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labText_MouseDown);
      this.labText.MouseLeave += new System.EventHandler(this.labText_MouseLeave);
      this.labText.MouseHover += new System.EventHandler(this.labText_MouseHover);
      this.labText.MouseUp += new System.Windows.Forms.MouseEventHandler(this.labText_MouseUp);
      // 
      // CtrlButton
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.ctrlPanel);
      this.Name = "CtrlButton";
      this.Size = new System.Drawing.Size(150, 39);
      this.Enter += new System.EventHandler(this.CtrlButton_Enter);
      this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CtrlButton_KeyDown);
      this.Leave += new System.EventHandler(this.CtrlButton_Leave);
      this.ctrlPanel.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private CtrlCustomPanel ctrlPanel;
    private Label labText;
  }
}
