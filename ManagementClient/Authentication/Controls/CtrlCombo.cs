using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Authentication.Controls
{
  internal partial class CtrlCombo : UserControl
  {
    #region Delegati ed eventi

    public event SelectionChangedHandler SelectionChanged;

    #endregion

    #region Campi

    private int _fontSize = 12;

    /// <summary>
    /// Collezione di dati 
    /// </summary>
    private readonly SortedList<int, ComboItem> _items = new SortedList<int, ComboItem>();

    #endregion

    #region Costruttore 

    public CtrlCombo()
    {
      InitializeComponent();
    }

    #endregion

    #region ProprietÓ

    /// <summary>
    /// Indice currentemente selezionato
    /// </summary>
    public int SelectedIndex { get; private set; }

    /// <summary>
    /// Numero degli items
    /// </summary>
    public int ItemsCount => _items.Count;

    /// <summary>
    /// Restituisce l'indice selezionato
    /// </summary>
    public int CurrentIndex => _items[SelectedIndex].Index;

    /// <summary>
    /// Restituisce la chiave selezionata
    /// </summary>
    public string CurrentKey => _items[SelectedIndex].Key;

    /// <summary>
    /// Restituisce il valore selezionato
    /// </summary>
    public string CurrentValue => _items[SelectedIndex].Value;

    [DefaultValue(typeof(int), "None"), Category("Appearance"), Description("Font Size")]
    public int FontSize
    {
      get
      {
        return _fontSize;
      }
      set
      {
        _fontSize = value;
        lblValue.Font = new Font("Microsoft Sans Serif", _fontSize, FontStyle.Bold, GraphicsUnit.Point, 0);

        //if (DesignMode)
        //{
        //this.Invalidate();
        //}
      }
    }

    [Category("Appearance"), Description("Back Color Value")]
    public Color BackColorValue
    {
      get { return lblValue.BackColor; }
      set { lblValue.BackColor = value; }
    }

    #endregion

    #region Metodi pubblici

    /// <summary>
    /// Restituisce tutte le chiavi all'interno della collezione
    /// </summary>
    public IList<string> GetKeys()
    {
      return _items.Values.Select(c => c.Key).ToList();
    }

    /// <summary>
    /// Impostazione dati. I dati sono costituiti da una lista di chiavi e valore.
    /// </summary>
    /// <param name="items">Lista di chiave - valore. il valore viene visualizzato</param>
    public void SetData(IDictionary<string, string> items)
    {
      _items.Clear();

      foreach (var index in Enumerable.Range(0, items.Count))
      {
        var item = items.ElementAt(index);

        ComboItem cbi = new ComboItem(index, item.Key, item.Value);
        _items.Add(index, cbi);
      }
    }

    /// <summary>
    /// Inizializza i dati e seleziona l'item con la chiave specificata.
    /// Se null seleziona il primo elemento.
    /// </summary>
    public void Init(int selectedIndex)
    {
      if (_items.Any() && _items.ElementAtOrDefault(selectedIndex).Value == null)
        throw new IndexOutOfRangeException($"{nameof(selectedIndex)} '{selectedIndex}' out of range");

      SelectItem(-1, selectedIndex);
    }

    public void Reset()
    {
      SelectedIndex = 0;
      SelectItem(0, 0);
    }

    #endregion

    #region Metodi privati

    private void SelectItem(int previousIndex, int currentIndex)
    {
      ComboItem current = _items.ElementAtOrDefault(currentIndex).Value;

      if (current == null)
        return;

      SelectedIndex = currentIndex;

      lblValue.Text = current.Value;

      if (previousIndex == currentIndex)
        return;

      SelectionChanged?.Invoke(this, new SelectionChangedEventArgs(previousIndex, currentIndex));
    }

    //private void EnableButtons()
    //{
    //  //...forward
    //  if(_selectedIndex<_items.Count-1)
    //  {
    //      btnForward.Enabled = true;
    //  }
    //  else
    //  {
    //      btnForward.Enabled = false;
    //  }

    //  if(_selectedIndex>0)
    //  {
    //      btnBackward.Enabled = true;
    //  }
    //  else
    //  {
    //      btnBackward.Enabled = false;
    //  }
    //}

    #endregion

    #region Eventi

    private void btnForward_Click(object sender, EventArgs e)
    {
      int previousIndex = SelectedIndex;
      if (SelectedIndex == _items.Count - 1) SelectedIndex = 0;
      else SelectedIndex++;

      var currentIndex = SelectedIndex;
      SelectItem(previousIndex, currentIndex);
    }

    private void btnBackward_Click(object sender, EventArgs e)
    {
      int previousIndex = SelectedIndex;
      if (SelectedIndex == 0) SelectedIndex = _items.Count - 1;
      else SelectedIndex--;

      var currentIndex = SelectedIndex;
      SelectItem(previousIndex, currentIndex);
    }

    #endregion
  }

  #region ComboItem

  internal class ComboItem
  {
    public int Index { get; }
    public string Key { get; }
    public string Value { get; }

    public ComboItem(int index, string key, string value)
    {
      if (string.IsNullOrEmpty(key))
        throw new ArgumentNullException(nameof(key));

      if (string.IsNullOrEmpty(value))
        throw new ArgumentNullException(nameof(value));

      Index = index;
      Key = key;
      Value = value;
    }
  }

  #endregion

  #region Delegates and Events

  internal delegate void SelectionChangedHandler(object sender, SelectionChangedEventArgs e);

  internal class SelectionChangedEventArgs : EventArgs
  {
    public int OldKey { get; }

    public int NewKey { get; }

    public SelectionChangedEventArgs(int oldKey, int newKey)
    {
      if (oldKey == newKey)
        throw new Exception($"OldKey and NewKey have same value '{oldKey}'");

      OldKey = oldKey;
      NewKey = newKey;
    }
  }

  #endregion
}