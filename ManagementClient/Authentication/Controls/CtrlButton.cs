using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Authentication.Controls
{
  internal partial class CtrlButton : UserControl
  {
    #region DELEGATI ED EVENTI

    public new event EventHandler Click;

    #endregion

    private ButtonStyle _bottonStyle = ButtonStyle.None;
    private int _borderWidth = 1;
    private Color _borderColor = Color.Black;
    private Color _backColor1 = Color.White;
    private Color _backColor2 = Color.WhiteSmoke;
    private Color _pressColor1 = Color.WhiteSmoke;
    private Color _pressColor2 = Color.White;
    private Color _textColor = Color.Black;

    [DefaultValue(typeof(ButtonStyle), "None"), Category("Appearance"), Description("Style of Button.")]
    public ButtonStyle BottonDefaultStyle
    {
      get
      {
        return _bottonStyle;
      }
      set
      {
        _bottonStyle = value;
        SetStyle();
        //if (DesignMode)
        //{
        //  //this.Invalidate();
        //}
      }
    }

    [DefaultValue(typeof(int), "1"), Category("Appearance"), Description("Button Border Width")]
    public int BorderWidth
    {
      get
      {
        return _borderWidth;
      }
      set
      {
        _borderWidth = value;
        SetStyle();
        //if (DesignMode)
        //{
        //  //this.Invalidate();
        //}

      }
    }

    [DefaultValue(typeof(Color), "Black"), Category("Appearance"), Description("Button Border Color")]
    public Color BorderColor
    {
      get
      {
        return _borderColor;
      }
      set
      {
        if (_bottonStyle.Equals(ButtonStyle.None))
          _borderColor = value;
        SetStyle();
      }
    }

    [DefaultValue(typeof(Color), "White"), Category("Appearance"), Description("Button BackColor 1 Color")]
    public Color BackColor1
    {
      get
      {
        return _backColor1;
      }
      set
      {
        if (_bottonStyle.Equals(ButtonStyle.None))
          _backColor1 = value;
        SetStyle();
      }
    }

    [DefaultValue(typeof(Color), "WhiteSmoke"), Category("Appearance"), Description("Button BackColor 2 Color")]
    public Color BackColor2
    {
      get
      {
        return _backColor2;
      }
      set
      {
        if (_bottonStyle.Equals(ButtonStyle.None))
          _backColor2 = value;
        SetStyle();
      }
    }

    [DefaultValue(typeof(Color), "WhiteSmoke"), Category("Appearance"), Description("Button Press Color 1")]
    public Color PressColor1
    {
      get
      {
        return _pressColor1;
      }
      set
      {
        if (_bottonStyle.Equals(ButtonStyle.None))
          _pressColor1 = value;
        SetStyle();
      }
    }

    [DefaultValue(typeof(Color), "White"), Category("Appearance"), Description("Button Press Color 2")]
    public Color PressColor2
    {
      get
      {
        return _pressColor2;
      }
      set
      {
        if (_bottonStyle.Equals(ButtonStyle.None))
          _pressColor2 = value;
        SetStyle();
      }
    }

    [DefaultValue(typeof(Color), "Black"), Category("Appearance"), Description("Button Press Color 2")]
    public Color TextColor
    {
      get
      {
        return _textColor;
      }
      set
      {
        if (_bottonStyle.Equals(ButtonStyle.None))
          _textColor = value;
        SetStyle();
      }
    }

    [DefaultValue(typeof(Font), "Segoe UI; 11,25pt; style=Bold"), Category("Appearance"), Description("Font of Button")]
    public new Font Font
    {
      get { return labText.Font; }
      set { labText.Font = value; }
    }

    public CtrlButton()
    {
      InitializeComponent();
    }

    #region METODI PUBBLICI

    //[DefaultValue(typeof(string), "Text"), Category("Appearance"), Description("ButtonText")]
    [DefaultValue(false)]
    [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
    public string ButtonText
    {
      set { labText.Text = value; }
      get { return labText.Text; }
    }
    #endregion

    #region EVENTI

    private void labText_Click(object sender, EventArgs e)
    {
      //ctrlPanel.Curvature = 15;
      //ctrlPanel.Invalidate();

      ButtonPressedEffect();

      Click?.Invoke(this, new EventArgs());
    }

    private void ButtonPressedEffect()
    {
      ctrlPanel.Curvature = 5;
      ctrlPanel.Invalidate();
    }

    private void labText_MouseUp(object sender, MouseEventArgs e)
    {
      ctrlPanel.Curvature = 5;
      ctrlPanel.Invalidate();
    }

    private void labText_MouseDown(object sender, MouseEventArgs e)
    {
      ctrlPanel.Curvature = 10;
      ctrlPanel.Invalidate();
    }

    private void labText_MouseHover(object sender, EventArgs e)
    {
      SetStyleReversed();
    }

    private void labText_MouseLeave(object sender, EventArgs e)
    {
      SetStyle();
    }

    #endregion

    #region VISUALIZZAZIONE
    private void SetStyle()
    {
      if (_bottonStyle == ButtonStyle.Blue)
      {
        ctrlPanel.BackColor = Color.LightBlue;
        ctrlPanel.BackColor2 = Color.DarkBlue;
        ctrlPanel.BorderColor = Color.Blue;
        labText.ForeColor = Color.White;
      }
      else if (_bottonStyle == ButtonStyle.Red)
      {
        ctrlPanel.BackColor = Color.OrangeRed;
        ctrlPanel.BackColor2 = Color.Brown;
        ctrlPanel.BorderColor = Color.DarkRed;
        labText.ForeColor = Color.White;
      }
      else if (_bottonStyle == ButtonStyle.Green)
      {
        ctrlPanel.BackColor = Color.LightGreen;
        ctrlPanel.BackColor2 = Color.Green;
        ctrlPanel.BorderColor = Color.Green;
        labText.ForeColor = Color.White;
      }
      else if (_bottonStyle == ButtonStyle.SkyBlue)
      {
        ctrlPanel.BackColor2 = Color.Blue;
        ctrlPanel.BackColor = Color.LightSkyBlue;
        ctrlPanel.BorderColor = Color.Blue;
        labText.ForeColor = Color.White;
      }
      else if (_bottonStyle == ButtonStyle.Grey)
      {
        ctrlPanel.BackColor2 = Color.Gray;
        ctrlPanel.BackColor = Color.DarkGray;
        ctrlPanel.BorderColor = Color.Black;
        labText.ForeColor = Color.White;
      }
      else if (_bottonStyle == ButtonStyle.Menu)
      {
        ctrlPanel.BackColor2 = Color.DodgerBlue;
        ctrlPanel.BackColor = Color.RoyalBlue;
        ctrlPanel.BorderColor = Color.RoyalBlue;
        labText.ForeColor = Color.White;
      }
      else if (_bottonStyle == ButtonStyle.None)
      {
        ctrlPanel.BackColor2 = _backColor2;
        ctrlPanel.BackColor = _backColor1;
        ctrlPanel.BorderColor = _borderColor;
        labText.ForeColor = _textColor;
      }

      ctrlPanel.BorderWidth = _borderWidth;
      ctrlPanel.Invalidate();
    }


    private void SetStyleReversed()
    {
      if (_bottonStyle == ButtonStyle.Blue)
      {
        ctrlPanel.BackColor = Color.DarkBlue;
        ctrlPanel.BackColor2 = Color.LightBlue;
      }
      else if (_bottonStyle == ButtonStyle.Red)
      {
        ctrlPanel.BackColor2 = Color.OrangeRed;
        ctrlPanel.BackColor = Color.Brown;
      }
      else if (_bottonStyle == ButtonStyle.Green)
      {
        ctrlPanel.BackColor2 = Color.LightGreen;
        ctrlPanel.BackColor = Color.Green;
      }
      else if (_bottonStyle == ButtonStyle.SkyBlue)
      {
        ctrlPanel.BackColor2 = Color.LightSkyBlue;
        ctrlPanel.BackColor = Color.Blue;
      }
      else if (_bottonStyle == ButtonStyle.Grey)
      {
        ctrlPanel.BackColor2 = Color.DarkGray;
        ctrlPanel.BackColor = Color.Gray;
      }
      else if (_bottonStyle == ButtonStyle.Menu)
      {
        ctrlPanel.BackColor2 = Color.RoyalBlue;
        ctrlPanel.BackColor = Color.DodgerBlue;
      }
      else if (_bottonStyle == ButtonStyle.None)
      {
        ctrlPanel.BackColor = _pressColor1;
        ctrlPanel.BackColor2 = _pressColor2;
      }

      ctrlPanel.Invalidate();
    }


    #endregion

    private void CtrlButton_Enter(object sender, EventArgs e)
    {
      if (_bottonStyle == ButtonStyle.Blue)
      {
        ctrlPanel.BorderColor = Color.DarkBlue;
      }
      else if (_bottonStyle == ButtonStyle.Red)
      {
        ctrlPanel.BorderColor = Color.DarkRed;
      }
      else if (_bottonStyle == ButtonStyle.Green)
      {
        ctrlPanel.BorderColor = Color.DarkGreen;
      }
      else if (_bottonStyle == ButtonStyle.SkyBlue)
      {
        ctrlPanel.BorderColor = Color.DarkBlue;
      }
      else if (_bottonStyle == ButtonStyle.Grey)
      {
        ctrlPanel.BorderColor = Color.Black;
      }
      else if (_bottonStyle == ButtonStyle.Menu)
      {
        ctrlPanel.BorderColor = Color.RoyalBlue;
      }
      else if (_bottonStyle == ButtonStyle.None)
      {
        ctrlPanel.BorderColor = _borderColor;
      }

      ctrlPanel.BorderWidth = 1;
      ctrlPanel.Invalidate();
    }

    private void CtrlButton_Leave(object sender, EventArgs e)
    {
      if (_bottonStyle == ButtonStyle.Blue)
      {
        ctrlPanel.BorderColor = Color.Blue;
      }
      else if (_bottonStyle == ButtonStyle.Red)
      {
        ctrlPanel.BorderColor = Color.Red;
      }
      else if (_bottonStyle == ButtonStyle.Green)
      {
        ctrlPanel.BorderColor = Color.Green;
      }
      else if (_bottonStyle == ButtonStyle.SkyBlue)
      {
        ctrlPanel.BorderColor = Color.Blue;
      }
      else if (_bottonStyle == ButtonStyle.Grey)
      {
        ctrlPanel.BorderColor = Color.DarkGray;
      }
      else if (_bottonStyle == ButtonStyle.Menu)
      {
        ctrlPanel.BorderColor = Color.RoyalBlue;
      }
      else if (_bottonStyle == ButtonStyle.None)
      {
        ctrlPanel.BorderColor = _borderColor;
      }

      ctrlPanel.BorderWidth = _borderWidth;
      ctrlPanel.Invalidate();
    }

    private void CtrlButton_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Return) labText_Click(sender, e);
    }
  }

  public enum ButtonStyle
  {
    None,
    Green,
    Red,
    Blue,
    SkyBlue,
    Grey,
    Orange,
    Menu
  }
}
