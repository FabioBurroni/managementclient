﻿namespace Authentication
{
  public enum SourceType
  {
    /// <summary>
    /// A questo tipo è permesso loggarsi in un numero di posizioni posizioni fisso (0-1-2..) dichiarato al primo login. 
    /// Non si possono aggiungere o togliere posizioni a runtime, le posizioni sono vincolate a quelle dichiarate all'avvio, quindi nasce e muore con lo stesso numero di posizioni.
    /// Prende il sopravvento su una posizione, è possibile loggarsi solo se libera da altri tipi Client.
    /// In caso di tentativo di login su 2 o più posizioni, devono essere tutte libere altrimenti il login è negato.
    /// La sua sessione permette l'ereditarietà tra software nello stesso pc.
    /// L'eredità funziona solo se si dichiara un numero di posizioni uguali a quelle dei software precedentemente loggati altrimenti la sessione non è valida. Eredita quindi solo da tipi uguali a se stesso.
    /// Possono esserci più software nello stessa macchina, non è possibile mischiarla con tipi Client diversi da se stesso.
    /// Effettua il Logout comunicando ChildToken.
    /// In caso di RootToken, tutte le sessioni appartenenti vengono sloggate
    /// </summary>
    StaticClient,

    /// <summary>
    /// A questo tipo è permesso loggarsi in un numero di posizioni dinamico che può incrementare e diminuire a runtime (per esempio da 0 a N e poi da N a 0). 
    /// Prende il sopravvento su una posizione, è possibile loggarsi solo se libera da altri tipi Client.
    /// Non permette mai ereditarietà e nello stesso dispositivo (stesso indirizzo ip), è possibile avere al massimo un tipo Client.
    /// Effettua il Logout comunicando ChildToken o il RootToken, tanto saranno sempre univoci.
    /// </summary>
    DynamicClient,

    /// <summary>
    /// A questo tipo è permesso loggarsi in un numero di posizioni posizioni dinamico che può incrementare e diminuire a runtime (per esempio da 0 a N e poi da N a 0). 
    /// Non prende mai il sopravvento su una posizione, è possibile loggarsi su sempre una posizione, non interferisce sui tipi Client o altri System.
    /// Più tipi System possono convivere nello stesso computer (stesso indirizzo ip) e non permette mai ereditarietà.
    /// Effettua il Logout comunicando ChildToken o il RootToken, tanto saranno sempre univoci.
    /// </summary>
    DynamicSystem
  }
}