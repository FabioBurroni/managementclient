﻿namespace Authentication
{
  public interface IErrorCodeMapper
  {
    /// <summary>
    /// A partire da un codice di errore, restituisce la sua descrizione
    /// che sarà usata anche per le traduzioni
    /// </summary>
    bool TryGetDescription(int errorCode, out string descr);

    /// <summary>
    /// A partire da un codice di errore, restituisce la sua descrizione
    /// che sarà usata anche per le traduzioni
    /// </summary>
    string this[int errorCode] { get; }
  }
}