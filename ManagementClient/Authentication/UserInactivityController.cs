﻿using System;
using System.Timers;
using Authentication.UserInactivityMonitoring;

namespace Authentication
{
	internal class UserInactivityController : IDisposable
	{
		/// <summary>
		/// Quando l'utente è inattivo e scatta il timeout, viene lanciato questo evento
		/// </summary>
		public event EventHandler InactiveUserTimeout;

		private readonly LastInputMonitor _monitor;

		public UserInactivityController(double inactivityTimeout)
		{
			_monitor = new LastInputMonitor
			{
				Interval = inactivityTimeout, //tempo di inattività massimo
				Enabled = true //abilitazione del controllo di timeout
			};

			_monitor.Elapsed += MonitorOnElapsed;
		}

		public double InactivityTimeout
		{
			get { return _monitor.Interval; }
			set { _monitor.Interval = value; }
		}

		private void MonitorOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
		{
			InactiveUserTimeout?.Invoke(this, EventArgs.Empty);
		}

		public void Dispose()
		{
			_monitor.Elapsed -= MonitorOnElapsed;
			_monitor.Dispose();
		}
	}
}