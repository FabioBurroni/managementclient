﻿using System.ComponentModel;
using System.Diagnostics;

namespace Authentication.Extensions
{
  internal static class DesignTimeHelper
  {
    public static bool IsInDesignMode
    {
      get
      {
        bool isInDesignMode = LicenseManager.UsageMode == LicenseUsageMode.Designtime /*|| Debugger.IsAttached*/;

        if (!isInDesignMode)
        {
          using (var process = Process.GetCurrentProcess())
          {
            const string devenv = "devenv";
            isInDesignMode = process.ProcessName.ContainsIgnoreCase(devenv);
          }
        }

        return isInDesignMode;
      }
    }
  }
}