﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Authentication.Extensions
{
  internal static class StringExtension
  {
    #region EqualsIgnoreCase

    /// <summary>
    /// Determina l'uguaglianza di due stringhe ignorando minuscole e maiuscole.
    /// </summary>
    /// <param name="a">Prima stringa, può essere nulla</param>
    /// <param name="b">Seconda stringa, può essere nulla</param>
    /// <returns>True se uguali, in caso una delle due stringhe sia vuota, il risultato sarà sempre false</returns>
    public static bool EqualsIgnoreCase(this string a, string b)
    {
      if (string.IsNullOrEmpty(a) || string.IsNullOrEmpty(b))
        return false;

      return string.Equals(a, b, StringComparison.OrdinalIgnoreCase);
    }

    #endregion

    #region EqualsConsiderCase

    /// <summary>
    /// Determina l'uguaglianza di due stringhe considerando minuscole e maiuscole.
    /// </summary>
    /// <param name="a">Prima stringa, può essere nulla</param>
    /// <param name="b">Seconda stringa, può essere nulla</param>
    /// <returns>True se uguali, in caso una delle due stringhe sia vuota, il risultato sarà sempre false</returns>
    public static bool EqualsConsiderCase(this string a, string b)
    {
      if (string.IsNullOrEmpty(a) || string.IsNullOrEmpty(b))
        return false;

      return string.Equals(a, b, StringComparison.Ordinal);
    }

    #endregion

    #region CompareIgnoreCase

    /// <summary>
    /// Effettua la comparazione di due stringhe ignorando minuscole e maiuscole.
    /// </summary>
    /// <param name="a">Prima stringa, può essere nulla</param>
    /// <param name="b">Seconda stringa, può essere nulla</param>
    /// <returns>True se uguali, in caso una delle due stringhe sia vuota, il risultato sarà sempre false</returns>
    public static int CompareIgnoreCase(this string a, string b)
    {
      return string.Compare(a, b, StringComparison.OrdinalIgnoreCase);
    }

    #endregion

    #region Contains

    public static bool ContainsIgnoreCase(this string source, string toCheck)
    {
      if (source == null || toCheck == null)
        return false;

      return source.IndexOf(toCheck, StringComparison.OrdinalIgnoreCase) >= 0;
    }

    #endregion

    #region IsNumber

    /// <summary>
    /// Verifica se la stringa può essere convertita in un intero
    /// </summary>
    public static bool IsNumber(this string str)
    {
      int res;
      return !string.IsNullOrEmpty(str) && int.TryParse(str, out res) && res.ToString() == str.TrimStart('0');
    }

    #endregion

    #region Array Check

    /// <summary>
    /// Verifica se la stringa contiene almeno uno dei caratteri all'interno di chars
    /// </summary>
    /// <param name="str">Stringa da controllare</param>
    /// <param name="chars">Caratteri di verifica</param>
    /// <returns>True se esiste almeno un carattere di chars in str</returns>
    public static bool ContainsAny(this string str, IEnumerable<char> chars)
    {
      if (str == null || chars == null)
        return false;
      return chars.Any(str.Contains);
    }

    /// <summary>
    /// Verifica se la stringa contiene almeno tutti i caratteri all'interno di chars
    /// </summary>
    /// <param name="str">Stringa da controllare</param>
    /// <param name="chars">Caratteri di verifica</param>
    /// <returns>True se esistono tutti i caratteri di chars in str</returns>
    public static bool ContainsAll(this string str, IEnumerable<char> chars)
    {
      if (str == null || chars == null)
        return false;
      return chars.All(str.Contains);
    }
    #endregion

    #region DateTimeCheck

    #region DateTime Format
    private const string DateFormat = "yyyy-MM-dd";
    private const string TimeFormat = "HH.mm.ss";
    private const string DateTimeFormat = "yyyy-MM-dd HH.mm.ss";
    #endregion

    /// <summary>
    /// Verifica se può essere fatto il cast da una stringa a data nel formato HMS HH.mm.ss
    /// </summary>
    /// <param name="stringToCheck">Stringa da verificare</param>
    /// <returns>Se può essere fatto il cast</returns>
    public static bool IsTimeFormat(this string stringToCheck)
    {
      return stringToCheck.IsDateTimeFormatPri(TimeFormat);
    }

    /// <summary>
    /// Verifica se può essere fatto il cast da una stringa a data nel formato HMS yyyy-MM-dd
    /// </summary>
    /// <param name="stringToCheck">Stringa da verificare</param>
    /// <returns>Se può essere fatto il cast</returns>
    public static bool IsDateFormat(this string stringToCheck)
    {
      return stringToCheck.IsDateTimeFormatPri(DateFormat);
    }

    /// <summary>
    /// Verifica se può essere fatto il cast da una stringa a data nel formato HMS yyyy-MM-dd HH.mm.ss
    /// </summary>
    /// <param name="stringToCheck">Stringa da verificare</param>
    /// <returns>Se può essere fatto il cast</returns>
    public static bool IsDateTimeFormat(this string stringToCheck)
    {
      return stringToCheck.IsDateTimeFormatPri(DateTimeFormat);
    }

    /// <summary>
    /// Verifica se può essere fatto il cast da una stringa a data nel formato specificato
    /// </summary>
    /// <param name="stringToCheck">Stringa da verificare</param>
    /// <param name="format">Formato di controllo</param>
    /// <returns>Se può essere fatto il cast</returns>
    private static bool IsDateTimeFormatPri(this string stringToCheck, string format)
    {
      if (string.IsNullOrEmpty(stringToCheck) || string.IsNullOrEmpty(format) ||
          !stringToCheck.Length.Equals(format.Length))
        return false;

      DateTime dateTime;
      return (DateTime.TryParseExact(stringToCheck, format, null, DateTimeStyles.None, out dateTime) &&
              dateTime.ToString(format).Equals(stringToCheck));
    }

    #endregion

    #region IndexOf

    /// <summary>
    /// Cerca la prima occorrenza di un certo pattern all'interno di una stringa di valori seguendo
    /// l'algoritmo di Boyer–Moore.
    /// </summary>
    /// <param name="value">Array di valori</param>
    /// <param name="pattern">Pattern da cercare</param>
    /// <returns>Restituisce l'indice della prima occorrenza trovata</returns>
    public static int IndexOf(this string value, string pattern)
    {
      if (string.IsNullOrEmpty(value))
        throw new ArgumentNullException("value");

      if (string.IsNullOrEmpty(pattern))
        throw new ArgumentNullException("pattern");

      int valueLength = value.Length;
      int patternLength = pattern.Length;

      if ((valueLength == 0) || (patternLength == 0) || (patternLength > valueLength))
        return -1;

      var badCharacters = new int[256];

      for (int i = 0; i < 256; ++i)
        badCharacters[i] = patternLength;

      int lastPatternByte = patternLength - 1;

      for (int i = 0; i < lastPatternByte; ++i)
        badCharacters[pattern[i]] = lastPatternByte - i;

      // Beginning

      int index = 0;

      while (index <= (valueLength - patternLength))
      {
        for (int i = lastPatternByte; value[(index + i)] == pattern[i]; --i)
        {
          if (i == 0)
            return index;
        }

        index += badCharacters[value[(index + lastPatternByte)]];
      }

      return -1;
    }

    #endregion

    #region IndexesOf

    /// <summary>
    /// Cerca le occorrenze di un certo pattern di string all'interno di una atringa seguendo
    /// l'algoritmo di Boyer–Moore.
    /// </summary>
    /// <param name="value">Array di valori</param>
    /// <param name="pattern">Pattern da cercare</param>
    /// <returns>Restituisce gli indici di tutte le occorrenze trovata</returns>
    public static IEnumerable<int> IndexesOf(this string value, string pattern)
    {
      if (string.IsNullOrEmpty(value))
        throw new ArgumentNullException("value");

      if (string.IsNullOrEmpty(pattern))
        throw new ArgumentNullException("pattern");

      int valueLength = value.Length;
      int patternLength = pattern.Length;

      if ((valueLength == 0) || (patternLength == 0) || (patternLength > valueLength))
      {
        //return (new List<int>());
        yield break;
      }

      var badCharacters = new int[256];

      for (int i = 0; i < 256; ++i)
        badCharacters[i] = patternLength;

      int lastPatternByte = patternLength - 1;

      for (int i = 0; i < lastPatternByte; ++i)
        badCharacters[pattern[i]] = lastPatternByte - i;

      int index = 0;

      while (index <= (valueLength - patternLength))
      {
        for (var i = lastPatternByte; value[(index + i)] == pattern[i]; --i)
        {
          if (i == 0)
          {
            //indexes.Add(index);
            //break;
            yield return index;
            break;
          }
        }

        index += badCharacters[value[(index + lastPatternByte)]];
      }
    }

    /// <summary>
    /// Cerca le occorrenze di un certo pattern di stringa all'interno di un array seguendo
    /// l'algoritmo di Boyer–Moore.
    /// Rispetto alla versione base permette di specificare l'offest massimo di ricerca all'interno dell'array
    /// quindi una lunghezza inferiore alla sua grandezza
    /// </summary>
    /// <param name="value">Array di valori</param>
    /// <param name="valueLength">Grandezza dell'array di valori</param>
    /// <param name="pattern">Pattern da cercare</param>
    /// <returns>Restituisce gli indici di tutte le occorrenze trovata</returns>
    public static IEnumerable<int> IndexesOf(this string value, int valueLength, string pattern)
    {
      if (string.IsNullOrEmpty(value))
        throw new ArgumentNullException("value");

      if (string.IsNullOrEmpty(pattern))
        throw new ArgumentNullException("pattern");

      //int valueLength = value.Length;
      int patternLength = pattern.Length;

      if ((valueLength == 0) || (patternLength == 0) || (patternLength > valueLength))
      {
        //return (new List<int>());
        yield break;
      }

      var badCharacters = new int[256];

      for (int i = 0; i < 256; ++i)
        badCharacters[i] = patternLength;

      int lastPatternByte = patternLength - 1;

      for (int i = 0; i < lastPatternByte; ++i)
        badCharacters[pattern[i]] = lastPatternByte - i;

      int index = 0;

      while (index <= (valueLength - patternLength))
      {
        for (var i = lastPatternByte; value[(index + i)] == pattern[i]; --i)
        {
          if (i == 0)
          {
            //indexes.Add(index);
            //break;
            yield return index;
            break;
          }
        }

        index += badCharacters[value[(index + lastPatternByte)]];
      }
    }

    #endregion

    #region Base64

    public static string Base64Encode(this string plainText)
    {
      if (plainText == null)
        throw new ArgumentNullException("plainText");

      var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
      return Convert.ToBase64String(plainTextBytes);
    }

    public static string Base64Decode(this string base64EncodedData)
    {
      if (base64EncodedData == null)
        throw new ArgumentNullException("base64EncodedData");

      var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
      return Encoding.UTF8.GetString(base64EncodedBytes);
    }

    #endregion

    #region Preamble

    /// <summary>
    /// Compone il comando xml formato dall'unione del preambolo e del nome del metodo.
    /// Attenzione, i parametri non fanno parte di questo metodo. Usare quelli presenti 
    /// in XmlCommunicationManager
    /// </summary>
    public static string ComposeCommand(this string methodName, string preamble)
    {
      if (string.IsNullOrEmpty(methodName))
        throw new ArgumentNullException(nameof(methodName));

      if (preamble == null)
        preamble = string.Empty;

      return string.Concat(preamble, methodName);
    }

    /// <summary>
    /// Separa nel comando xml, il metodo dal preambolo. Supporta anche comando senza preambolo
    /// </summary>
    public static void SeparateCommand(this string command, out string methodName, out string preamble)
    {
      if (string.IsNullOrEmpty(command))
        throw new ArgumentNullException(nameof(command));

      //cerco il carattere parentesi che divide il nome dai parametri
      var index = command.IndexOf('(');

      //se indice è minore di zero, significa che non la parentesi non c'è
      if (index < 0)
        throw new FormatException($"{nameof(command)} '{command}' is not a valid string, missing '('");

      //prendo la sottostringa preambolo+nome
      var commandWithoutParams = command.Substring(0, index);

      //cerco il carattere due punti che divide nome del metodo dal preambolo
      index = commandWithoutParams.IndexOf(':');

      if (index < 0)
      {
        //indice è minore di zero, significa che il preambolo non è presente, nessun problema
        preamble = string.Empty;
        index = 0;
      }
      else
      {
        //il preambolo è presente
        preamble = commandWithoutParams.Substring(0, ++index);
      }

      //prendo la sottostringa nome
      methodName = commandWithoutParams.Substring(index, commandWithoutParams.Length - index);
    }


    /// <summary>
    /// Rimuove il preambolo dal comando (se presente) e restituisce il nome del comando stesso senza i parametri
    /// </summary>
    public static string TrimPreamble(this string command)
    {
      string methodName, preamble;
      SeparateCommand(command, out methodName, out preamble);
      return methodName;
    }

    #endregion
  }
}