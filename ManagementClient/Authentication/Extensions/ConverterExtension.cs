﻿using System;
using System.Globalization;
using System.Linq;

namespace Authentication.Extensions
{
  internal static class ConverterExtension
  {
    #region DateTime Format

    private const string DateFormat = "yyyy-MM-dd";
    private const string TimeFormat = "HH.mm.ss";
    //private const string DateTimeFormat = "yyyy-MM-dd HH.mm.ss";

    #endregion

    #region String to Date

    /// <summary>
    /// Converte una stringa formattata nel formato HMS yyyy-MM-dd in una data
    /// </summary>
    /// <param name="stringToConvert">Stringa da convertire</param>
    /// <returns>Se ok, la data convertire, se errore MinValue</returns>
    public static DateTime ConvertToDate(this string stringToConvert)
    {
      return stringToConvert.ConvertToDateTime(DateFormat);
    }

    #endregion

    #region String to Time

    /// <summary>
    /// Converte una stringa formattata nel formato HMS HH.mm.ss in una data
    /// </summary>
    /// <param name="stringToConvert">Stringa da convertire</param>
    /// <returns>Se ok, la data convertire, se errore MinValue</returns>
    public static DateTime ConvertToTime(this string stringToConvert)
    {
      return stringToConvert.ConvertToDateTime(TimeFormat);
    }

    #endregion

    #region String to DateTime

    /// <summary>
    /// Converte una stringa formattata nel formato HMS yyyy-MM-dd HH.mm.ss in una data
    /// </summary>
    /// <param name="stringToConvert">Stringa da convertire</param>
    /// <returns>Se ok, la data convertire, se errore MinValue</returns>
    public static DateTime ConvertToDateTime(this string stringToConvert)
    {
      return stringToConvert.ConvertToDateTime(DateTimeFormat);
    }

    /// <summary>
    /// Converte una stringa nel formato specificato in una data
    /// </summary>
    /// <param name="stringToConvert">Stringa da convertire</param>
    /// <param name="format">Stringa da convertire</param>
    /// <returns>Se ok, la data convertire, se errore MinValue</returns>
    private static DateTime ConvertToDateTime(this string stringToConvert, string format)
    {
      if (string.IsNullOrEmpty(stringToConvert) || string.IsNullOrEmpty(format) || !stringToConvert.Length.Equals(format.Length))
      {
        return DateTime.MinValue;
      }

      DateTime dateTime;
      return DateTime.TryParseExact(stringToConvert, format, null, DateTimeStyles.None, out dateTime) ? dateTime : DateTime.MinValue;
    }

    #endregion

    #region String to Int

    /// <summary>
    /// Converte una stringa in un intero. Se non è possibile convertire, restituisce 0
    /// </summary>
    public static int ConvertToInt(this string stringToConvert)
    {
      int tempInt;
      return int.TryParse(stringToConvert, out tempInt) ? tempInt : 0;
    }

    #endregion

    #region String to UShort

    /// <summary>
    /// Converte una stringa in un intero corto. Se non è possibile convertire, restituisce 0
    /// </summary>
    public static ushort ConvertToUShort(this string stringToConvert)
    {
      ushort tempUShort;
      return ushort.TryParse(stringToConvert, out tempUShort) ? tempUShort : (ushort)0;
    }

    #endregion

    #region String to Bool

    /// <summary>
    /// Converte una stringa in un bool. Se non è possibile, restituisce false
    /// </summary>
    /// <param name="stringToConvert"></param>
    /// <returns></returns>
    public static bool ConvertToBool(this string stringToConvert)
    {
      return !string.IsNullOrEmpty(stringToConvert) && new[] { "1", "true" }.Contains(stringToConvert, StringComparer.OrdinalIgnoreCase);
    }

    #endregion

    #region String to Enum

    /// <summary>
    /// Converte una stringa nel suo corrispettivo definito nell'enum
    /// </summary>
    /// <typeparam name="T">Tipo dell'enumerativo</typeparam>
    /// <param name="stringToConvert">Stringa da convertire</param>
    /// <returns>Il valore dell'enum convertito o il default in caso non esistesse</returns>
    public static T ConvertToEnum<T>(this string stringToConvert)
      where T : struct
    {
      T tempEnum;
      stringToConvert.TryParse(out tempEnum);
      return tempEnum;
    }

    #endregion

    #region String TryParse

    public static bool TryParse<T>(this string s, out T result)
      where T : struct
    {
      bool retVal = false;
      try
      {
        result = (T)Enum.Parse(typeof(T), s, true);
        retVal = true;
      }
      catch (ArgumentException)
      {
        result = default(T);
      }
      return retVal;
    }

    #endregion

    #region Date to String

    /// <summary>
    /// Permette di convertire una data in una stringa già formattata nel formato HMS per data
    /// </summary>
    /// <param name="dateTimeToConvert">Data da convertire</param>
    /// <returns>Stringa formattata nel formato yyyy-MM-dd</returns>
    public static string ConvertToDateFormatString(this DateTime dateTimeToConvert)
    {
      return dateTimeToConvert.ToString(DateFormat);
    }

    #endregion

    #region Time to String

    /// <summary>
    /// Permette di convertire una data in una stringa già formattata nel formato HMS per tempo
    /// </summary>
    /// <param name="dateTimeToConvert">Data da convertire</param>
    /// <returns>Stringa formattata nel formato HH.mm.ss</returns>
    public static string ConvertToTimeFormatString(this DateTime dateTimeToConvert)
    {
      return dateTimeToConvert.ToString(TimeFormat);
    }

    #endregion

    #region DateTime to String

    /// <summary>
    /// Permette di convertire una data in una stringa formattata nel formato HMS per data e tempo
    /// </summary>
    /// <param name="dateTimeToConvert">Data da convertire</param>
    /// <returns>Stringa formattata nel formato yyyy-MM-dd HH.mm.ss</returns>
    public static string ConvertToDateTimeFormatString(this DateTime dateTimeToConvert)
    {
      return dateTimeToConvert.ToString(DateTimeFormat);
    }

    #endregion

    #region ConvertTo

    private static readonly string[] BoolFalseValues = { "0", "false" };
    private static readonly string[] BoolTrueValues = { "1", "true" };
    private const string DateTimeFormat = "yyyy-MM-dd HH.mm.ss";

    /// <summary>
    /// Converte una stringa in un oggetto seguendo le regole interne
    /// </summary>
    /// <typeparam name="T">Tipo destinazione</typeparam>
    /// <param name="value">Valore da convertire</param>
    /// <param name="defaultIfNull">Se il valore è nullo o vuoto, indica se deve restituire il default</param>
    /// <returns>Istanza convertita</returns>
    public static T ConvertTo<T>(this object value, bool defaultIfNull = false)
    {
      //se il valore è nullo o vuoto ed è settato opportunamente, restituisco il default di T
      if (defaultIfNull && value == null)
        return default(T);

      //provo il cast diretto
      if (value is T)
        return (T)value;

      //valore nullo e non è stato settato il default, eccezione
      if (!defaultIfNull && value == null)
        throw new InvalidCastException($"Cannot convert a null value to type '{typeof(T)}'");

      return ConvertTo<T>(value.ToString(), defaultIfNull);
    }

    /// <summary>
    /// Converte una stringa in un oggetto seguendo le regole interne
    /// </summary>
    /// <typeparam name="T">Tipo destinazione</typeparam>
    /// <param name="value">Valore da convertire</param>
    /// <param name="defaultIfNullOrEmpty">Se il valore è nullo o vuoto, indica se deve restituire il default</param>
    /// <returns>Istanza convertita</returns>
    public static T ConvertTo<T>(this string value, bool defaultIfNullOrEmpty = false)
    {
      //se il valore è nullo o vuoto ed è settato opportunamente, restituisco il default di T
      if (defaultIfNullOrEmpty && string.IsNullOrEmpty(value))
        return default(T);

      Type type = typeof(T);
      Type underlyingNullableType = Nullable.GetUnderlyingType(type);

      var fallbackType = underlyingNullableType ?? type;

      if (fallbackType.IsEnum)
      {

        // The specified type is an enum or Nullable{T} where T is an enum.

        var convertedEnum = (T)Enum.Parse(typeof(T), value, true);

        // It's necessary to check if the converted value is defined in an enum 

        if (!Enum.IsDefined(fallbackType, convertedEnum))
        {
          throw new ArgumentException("The specified value is not defined by the enumeration.", nameof(value));
        }

        return convertedEnum;
      }
      else if (type.IsValueType && underlyingNullableType == null)
      {

        // The specified type is a non-nullable value type.

        if (value == null || DBNull.Value.Equals(value))
        {
          throw new InvalidCastException("Cannot convert a null value to a non-nullable type.");
        }

        #region Backward Compatibility

        //mi scuso con i lettori di questa porcheria, vorrei fare un convertitore generico per tutte le stagioni, ma ho da mantenere la retrocompatibilità...

        #region bool
        if (type == typeof(bool))
        {
          if (BoolTrueValues.Contains(value, StringComparer.OrdinalIgnoreCase))
            value = true.ToString();
          else if (BoolFalseValues.Contains(value, StringComparer.OrdinalIgnoreCase))
            value = false.ToString();
          else
            throw new InvalidCastException($"Cannot convert value '{value}' to {type.Name}");
        }
        #endregion
        #region DateTime
        else if (type == typeof(DateTime))
        {
          DateTime dateTime;

          if (DateTime.TryParse(value, out dateTime))
            value = dateTime.ToString();
          else if (DateTime.TryParseExact(value, DateTimeFormat, null, DateTimeStyles.None, out dateTime))
            value = dateTime.ToString();
        }
        #endregion

        #endregion

        return (T)Convert.ChangeType(value, type);
      }

      #region Backward Compatibility

      //mi scuso con i lettori di questa porcheria, vorrei fare un convertitore generico per tutte le stagioni, ma ho da mantenere la retrocompatibilità...

      if (!string.IsNullOrEmpty(value))
      {
        #region bool
        if (type == typeof(bool?))
        {
          if (BoolTrueValues.Contains(value, StringComparer.OrdinalIgnoreCase))
            value = true.ToString();
          else if (BoolFalseValues.Contains(value, StringComparer.OrdinalIgnoreCase))
            value = false.ToString();
          else
            throw new InvalidCastException($"Cannot convert value '{value}' to {type.Name}");
        }
        #endregion
        #region DateTime
        else if (type == typeof(DateTime?))
        {
          DateTime dateTime;

          if (DateTime.TryParse(value, out dateTime))
            value = dateTime.ToString();
          else if (DateTime.TryParseExact(value, DateTimeFormat, null, DateTimeStyles.None, out dateTime))
            value = dateTime.ToString();
        }
        #endregion
      }

      #endregion

      // The specified type is a reference type or Nullable{T} where T is not an enum.
      return (string.IsNullOrEmpty(value) || DBNull.Value.Equals(value)) ? default(T) : (T)Convert.ChangeType(value, fallbackType);
    }

    #endregion
  }
}