﻿namespace Authentication.Extensions
{
  internal static class IntExtension
  {
    #region IsEven

    /// <summary>
    /// Indica se un intero è pari
    /// </summary>
    /// <param name="value">Valore intero da controllare</param>
    /// <returns>True se è pari</returns>
    public static bool IsEven(this int value)
    {
      return value % 2 == 0;
    }

    #endregion

    #region IsOdd

    /// <summary>
    /// Indica se un intero è dispari
    /// </summary>
    /// <param name="value">Valore intero da controllare</param>
    /// <returns>True se è dispari</returns>
    public static bool IsOdd(this int value)
    {
      return value % 2 != 0;
    }

    #endregion

    #region IsWithin

    /// <summary>
    /// Indica se un intero è compreso nel range specificato
    /// </summary>
    /// <param name="value">Valore intero da controllare</param>
    /// <param name="minimum">Valore minimo dell'intervallo</param>
    /// <param name="maximum">Valore massimo dell'intervallo</param>
    /// <returns>True se sussistono le condizioni</returns>
    public static bool IsWithin(this int value, int minimum, int maximum)
    {
      return value >= minimum && value <= maximum;
    }

    #endregion
  }
}