﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using Utilities.Atomic;

namespace Utilities.Threading
{
  public sealed class LooperThread : IComparable<LooperThread>, IDisposable, IEquatable<LooperThread>
  {
    #region Field
    private const int MaxWaitTime = 30;

    private readonly Thread _thread;
    private readonly TimeSpan _loopTime;

    /// <summary>
    /// Metodo da invocare
    /// </summary>
    private readonly MethodInfo _method;
    /// <summary>
    /// Istanza della classe su cui invocare il metodo
    /// </summary>
    private readonly object _target;
    /// <summary>
    /// Parametri per il metodo
    /// </summary>
    private readonly object[] _args;

    private readonly ThreadSafeSingleShotGuard _canStart = new ThreadSafeSingleShotGuard();
    private readonly AtomicNullableBoolean _looping = new AtomicNullableBoolean(false);
    private readonly ManualResetEvent _loopEvent = new ManualResetEvent(false);

    /// <summary>
    /// A value indicating whether this instance of the given entity has 
    /// been disposed.
    /// </summary>
    /// <value>
    /// <see langword="true"/> if this instance has been disposed; otherwise, 
    /// <see langword="false"/>.
    /// </value>
    /// <remarks>
    /// If the entity is disposed, it must not be disposed a second
    /// time. The isDisposed field is set the first time the entity
    /// is disposed. If the isDisposed field is true, then the Dispose()
    /// method will not dispose again. This help not to prolong the entity's
    /// life in the Garbage Collector.
    /// </remarks>
    private bool _isDisposed;

    #endregion

    #region Constructor

    #region ThreadStart

    public LooperThread(ThreadStart methodToInvoke, int millisecondsLoopTime)
      : this(methodToInvoke, TimeSpan.FromMilliseconds(millisecondsLoopTime))
    {
    }

    public LooperThread(ThreadStart methodToInvoke, TimeSpan loopTime)
      : this(methodToInvoke, loopTime, false)
    {
    }

    public LooperThread(ThreadStart methodToInvoke, int millisecondsLoopTime, bool startImmediately)
      : this(methodToInvoke, TimeSpan.FromMilliseconds(millisecondsLoopTime), startImmediately)
    {
    }

    public LooperThread(ThreadStart methodToInvoke, TimeSpan loopTime, bool startImmediately)
      : this((Delegate)methodToInvoke, null, loopTime, startImmediately)
    {
    }

    #endregion

    #region ParameterizedThreadStart

    public LooperThread(ParameterizedThreadStart methodToInvoke, int millisecondsLoopTime)
      : this(methodToInvoke, TimeSpan.FromMilliseconds(millisecondsLoopTime))
    {
    }

    public LooperThread(ParameterizedThreadStart methodToInvoke, TimeSpan loopTime)
      : this(methodToInvoke, loopTime, false)
    {
    }

    public LooperThread(ParameterizedThreadStart methodToInvoke, int millisecondsLoopTime, bool startImmediately)
      : this(methodToInvoke, TimeSpan.FromMilliseconds(millisecondsLoopTime), startImmediately)
    {
    }

    public LooperThread(ParameterizedThreadStart methodToInvoke, TimeSpan loopTime, bool startImmediately)
      : this(methodToInvoke, null, loopTime, startImmediately)
    {
    }

    public LooperThread(ParameterizedThreadStart methodToInvoke, IEnumerable<object> args, TimeSpan loopTime, bool startImmediately)
      : this((Delegate)methodToInvoke, args, loopTime, startImmediately)
    {
    }

    #endregion

    #region Delegate

    private LooperThread(Delegate delegateToInvoke, IEnumerable<object> args, TimeSpan loopTime, bool startImmediately)
    {
      if (delegateToInvoke == null)
        throw new ArgumentNullException(nameof(delegateToInvoke));

      if (loopTime <= TimeSpan.Zero)
        throw new ArgumentOutOfRangeException(nameof(loopTime), "loopTime must be greater than 0");

      _method = delegateToInvoke.Method;
      _target = delegateToInvoke.Target;
      _args = args?.ToArray();
      _loopTime = loopTime;

      _thread = new Thread(Worker);

      if (startImmediately)
        Start();
    }

    #endregion

    #endregion

    #region Properties

    public bool IsAlive
    {
      get { return _thread.IsAlive; }
    }

    public bool IsBackground
    {
      get { return _thread.IsBackground; }
      set { _thread.IsBackground = value; }
    }

    public bool IsThreadPoolThread
    {
      get { return _thread.IsThreadPoolThread; }
    }

    public int ManagedThreadId
    {
      get { return _thread.ManagedThreadId; }
    }

    public string Name
    {
      get { return _thread.Name; }
      set { _thread.Name = value; }
    }

    public ThreadPriority Priority
    {
      get { return _thread.Priority; }
      set { _thread.Priority = value; }
    }

    public ThreadState ThreadState
    {
      get { return _thread.ThreadState; }
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Fa partire il thread creato
    /// </summary>
    public void Start()
    {
      if (!_canStart.CheckAndSetFirstCall)
        return;

      _thread.Start();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Thread di lavoro
    /// </summary>
    private void Worker()
    {
      _looping.SetValue(true);
      while (_looping.Value.HasValue && _looping.Value.Value)
      {
        _method.Invoke(_target, _args);

        _loopEvent.WaitOne(_loopTime);
      }

      _looping.SetValue(null);
    }

    /// <summary>
    /// Ferma il thread
    /// </summary>
    private void Stop()
    {
      _looping.SetValue(false);
      _loopEvent.Set();

      var closingTime = DateTime.Now;
      while (_looping.Value.HasValue)
      {
        //aspetto massimo x secondi, poi chiudo il thread forzandolo
        Thread.Sleep(500);
        if (DateTime.Now.Subtract(closingTime).TotalSeconds >= MaxWaitTime)
        {
          try { if (_thread.IsAlive) _thread.Abort(); }
          catch { }
          break;
        }
      }
    }

    #endregion

    #region IComparable<LooperThread> Members

    public int CompareTo(LooperThread other)
    {
      // If other is not a valid object reference, this instance is greater.
      if (other == null)
        return 1;

      return ManagedThreadId.CompareTo(other.ManagedThreadId);
    }

    #endregion

    #region IDisposable Members

    /// <summary>
    /// Disposes the object and frees resources for the Garbage Collector.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);

      // This object will be cleaned up by the Dispose method.
      // Therefore, you should call GC.SupressFinalize to
      // take this object off the finalization queue 
      // and prevent finalization code for this object
      // from executing a second time.
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Disposes the object and frees resources for the Garbage Collector.
    /// </summary>
    /// <param name="disposing">If true, the object gets disposed.</param>
    private void Dispose(bool disposing)
    {
      if (_isDisposed)
      {
        return;
      }

      if (disposing)
      {
        // Dispose of any managed resources here.
      }

      // Call the appropriate methods to clean up
      // unmanaged resources here.
      // Note disposing is done.
      Stop();
      _isDisposed = true;
    }

    // Use C# destructor syntax for finalization code.
    // This destructor will run only if the Dispose method
    // does not get called.
    // It gives your base class the opportunity to finalize.
    // Do not provide destructors in types derived from this class.
    ~LooperThread()
    {
      // Do not re-create Dispose clean-up code here.
      // Calling Dispose(false) is optimal in terms of
      // readability and maintainability.
      Dispose(false);
    }

    #endregion

    #region IEquatable<LooperThread> Members

    public bool Equals(LooperThread other)
    {
      if (other == null)
        return false;

      return ManagedThreadId.Equals(other.ManagedThreadId);
    }

    #endregion
  }
}