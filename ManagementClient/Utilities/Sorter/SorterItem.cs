using System;

//using System.Collections.Generic;
//using System.Text;

namespace Utilities.Sorter
{
  public enum Order
  {
    ASC = 1,
    DESC = 2,
  }

  public class SorterItem
  {
    private readonly Order _order=Order.ASC;
    private readonly Type _type=null;
    
    public SorterItem(Type t,Order ord)
    {
      _type = t;
      _order = ord;
    }
    public Type type
    {
      get { return _type; }
    }

    public Order order
    {
      get { return _order; }
    }

  }
}
