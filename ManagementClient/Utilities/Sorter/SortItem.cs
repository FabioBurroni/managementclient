using System;

//using System.Collections.Generic;
//using System.Text;

namespace Utilities.Sorter
{
  public class SortItem
  {
    private readonly Type _type=null;
    private readonly object _val=null;
    public SortItem(Type t,object v)
    {
      if (v == null || t == null)
      {
        throw new Exception(GetType().FullName + ": type or object not valid");
      }
      if(!v.GetType().Equals(t))
        try
        {
          v=Convert.ChangeType(v, t);
        }
        catch
        {
          throw new Exception(GetType().FullName + ": type or object not valid");
        }
      _type = t;
      _val = v;
    }
    public object val
    {
      get { return _val; }
    }

    public Type type
    {
      get { return _type; }
    }

  }
}
