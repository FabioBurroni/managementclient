using System;
using System.Collections.Generic;
using System.Reflection;
//using System.Text;

namespace Utilities.Sorter
{
  public class SorterManager
  {
    #region Classi SortingOrderDesc
    public class Int16SortingOrderDesc : IComparer<short>
    {
      public int Compare(short  x, short  y)
      {
        if (x.Equals(y))
          return 0;
        return (x > y) ? -1 : 1;
      }
    }
    public class UInt16SortingOrderDesc : IComparer<ushort>
    {
      public int Compare(ushort  x, ushort  y)
      {
        if (x.Equals(y))
          return 0;
        return (x > y) ? -1 : 1;
      }
    }
    public class Int32SortingOrderDesc : IComparer<int>
    {
      public int Compare(int x, int y)
      {
        if (x.Equals(y))
          return 0;
        return (x > y) ? -1 : 1;
      }
    }
    public class UInt32SortingOrderDesc : IComparer<uint>
    {
      public int Compare(uint x, uint y)
      {
        if (x.Equals(y))
          return 0;
        return (x > y) ? -1 : 1;
      }
    }
    public class Int64SortingOrderDesc : IComparer<long>
    {
      public int Compare(long x, long y)
      {
        if (x.Equals(y))
          return 0;
        return (x > y) ? -1 : 1;
      }
    }
    public class UInt64SortingOrderDesc : IComparer<ulong>
    {
      public int Compare(ulong x, ulong y)
      {
        if (x.Equals(y))
          return 0;
        return (x > y) ? -1 : 1;
      }
    }
    public class DoubleSortingOrderDesc : IComparer<double>
    {
      public int Compare(double x, double y)
      {
        if (x.Equals(y))
          return 0;
        return (x > y) ? -1 : 1;
      }
    }
    public class SingleSortingOrderDesc : IComparer<float>
    {
      public int Compare(float x, float y)
      {
        if (x.Equals(y))
          return 0;
        return (x > y) ? -1 : 1;
      }
    }
    public class DateTimeSortingOrderDesc : IComparer<DateTime>
    {
      public int Compare(DateTime x, DateTime y)
      {
        //if (x.Equals(y))
        //  return 0;
        //return (x > y) ? -1 : 1;
        return DateTime.Compare(x, y);
      }
    }
    //public class StringSortingOrderDesc : IComparer<string>
    //{
    //  public int Compare(string x, string y)
    //  {
    //    int ret = 0;
    //    if (x.Equals(y))
    //      ret = 0;
    //    //return x.CompareTo(y);
    //    if (string.IsNullOrEmpty(x) && string.IsNullOrEmpty(y))
    //      ret = 0;
    //    else if (string.IsNullOrEmpty(x))
    //      ret = 1;
    //    else if (string.IsNullOrEmpty(y))
    //      ret = -1;
    //    else if (x.StartsWith(y))
    //      ret = -1;
    //    else if (y.StartsWith(x))
    //      ret = 1;
    //    else
    //    {
    //      int min = Math.Min(x.Length, y.Length);
    //      for (int i = 0; i < min; i++)
    //      {
    //        if (x[i] < y[i])
    //        {
    //          ret = 1;
    //          break;
    //        }
    //        else if (x[i] > y[i])
    //        {
    //          ret = -1;
    //          break;
    //        }
    //      }
    //    }
    //    return ret;
    //  }
    //}
    #endregion

    private readonly SorterStructure _sorterStruct = null;
    private object _objSL = null;
    private int _objCount = 0;

    public SorterManager(SorterStructure sorterStruct)
    {
      if (sorterStruct == null || sorterStruct.itemCount == 0 || sorterStruct.objToSortType == null)
        throw new Exception(GetType().FullName + ": SorterStructure is null or empty");
      _sorterStruct = sorterStruct;
      instanceSl();
    }

    public void clear()
    {
      instanceSl();
      _objCount = 0;
    }

    public int objCount{get{ return _objCount;}}

    private void instanceSl()
    {
      Type tList = typeof (List<>);
      Type[] tListArg = { typeof(SortObject) };
      Type tVal = tList.MakeGenericType(tListArg);
      for (int i = _sorterStruct.itemCount - 1; i >= 0; i--)
      {
        //tVal = createSlType(_sorterStruct.getSortItem(i).type, tVal);
        Type typ = getSortItemType(_sorterStruct.getSortItem(i));
        tVal = createSlType(typ, tVal);
      }
      //_objSL = Activator.CreateInstance(tVal);

      object objSort = instanceDescendingSorter(0);
      if (objSort == null)
        _objSL = Activator.CreateInstance(tVal);
      else
        _objSL = Activator.CreateInstance(tVal, new object[] { objSort });

    }

    //private static Type createSlType(Type tKey, Type tVal)
    private Type createSlType(Type tKey, Type tVal)
    {
      Type t = typeof(SortedList<,>);
      Type[] typeArgs = { tKey, tVal };
      Type ret = t.MakeGenericType(typeArgs);
      return ret;
    }

    public void addItem(SortObject item)
    {
      if(isItemCompliant(item))
        appendOrCreateObj(item);
    }

    public List<object> getSortedArray()
    {
      List<object > ret = new List<object>();
      if (_objCount > 0)
        getListObject(ret, _objSL);
      return ret;
    }

    private void getListObject(List<object> ls, object sl)
    {
      MethodInfo mi = sl.GetType().GetMethod("GetEnumerator");
      object obj = mi.Invoke(sl, null);
      mi = obj.GetType().GetMethod("MoveNext");
      while ((bool)mi.Invoke(obj, null))
      {
        PropertyInfo pi = obj.GetType().GetProperty("Current");
        object current = pi.GetValue(obj, null);

        pi = current.GetType().GetProperty("Value");
        object currentValue = pi.GetValue(current, null);

        if (!currentValue.GetType().GetGenericTypeDefinition().Equals(typeof(List<>)))
        {
          getListObject(ls, currentValue);
        }
        else
        {
          pi = currentValue.GetType().GetProperty("Count");
          int count = (int)pi.GetValue(currentValue, null);
          for (int i = 0; i < count; i++)
          {
            pi = currentValue.GetType().GetProperty("Item", new Type[] { typeof(int) });
            object o = pi.GetValue(currentValue, new object[] { i });
            ls.Add(((SortObject)o).objToSort);
          }
        }
      }
    }

    public List<SortObject> getSortedObjects()
    {
      List<SortObject> ret = new List<SortObject>();
      if (_objCount > 0)
        getList(ret, _objSL);

      return ret;
    }

    //private static void getList(List<SortObject> ls, object sl)
    private void getList(List<SortObject> ls, object sl)
    {
      MethodInfo mi = sl.GetType().GetMethod("GetEnumerator");
      object obj = mi.Invoke(sl, null);
      mi = obj.GetType().GetMethod("MoveNext");
      while((bool)mi.Invoke(obj,null))
      {
        PropertyInfo pi = obj.GetType().GetProperty("Current");
        object current = pi.GetValue(obj, null);

        pi = current.GetType().GetProperty("Value");
        object currentValue = pi.GetValue(current, null);

        if(!currentValue.GetType().GetGenericTypeDefinition().Equals(typeof(List<>)))
        {
          getList(ls,currentValue);
        }
        else
        {
          pi = currentValue.GetType().GetProperty("Count");
          int count = (int)pi.GetValue(currentValue, null);
          for(int i=0;i<count;i++)
          {
            pi = currentValue.GetType().GetProperty("Item", new Type[] { typeof(int) });
            object o = pi.GetValue(currentValue, new object[] {i});
            ls.Add((SortObject)o);
          }
        }
      }
    }


    //private static object instanceDescendingSorter(SorterStructure sorterStruct)
    private object instanceDescendingSorter(int level)
    {
      object ret = null;
      if(_sorterStruct.itemCount>0 && _sorterStruct.itemCount>level)
      {
        SorterItem sItem = _sorterStruct.getSortItem(level);
        if(sItem.order==Order.DESC)
        {
          //Type typeSort = Type.GetType(GetType().FullName+"."+ sItem.type.Name + "SortingOrderDesc");
          //Type typeSort = GetType().GetNestedType(sItem.type.Name + "SortingOrderDesc");
          Type typeSort = GetType().GetNestedType(getSortItemType(sItem).Name + "SortingOrderDesc");
          if (typeSort != null)
          {
            ret = Activator.CreateInstance(typeSort);
          }
        }
      }
      return ret;
    }

    //private static object instanceList()
    private object instanceList()
    {
      object ret;
      Type tList = typeof (List<>);
      Type[] tListArg = {typeof (SortObject)};
      Type tVal = tList.MakeGenericType(tListArg);
      ret = Activator.CreateInstance(tVal);
      //object objSort = instanceDescendingSorter(0);
      //if(objSort==null)
      //  ret = Activator.CreateInstance(tVal);
      //else
      //  ret = Activator.CreateInstance(tVal,new object[]{tVal});
      return ret;
    }

    private object instanceSLFromIndex(SortObject item, int index)
    {
      object ret;
      object objVal = instanceList();
      MethodInfo mi = objVal.GetType().GetMethod("Add");
      mi.Invoke(objVal, new object[] { item });

      Type tVal = objVal.GetType();

      //for (int i = _sorterStruct.itemCount - 1; i >= index; i--)
      for (int i = _sorterStruct.itemCount - 1; i >= index + 1; i--)
      {
        Type sortItemType = getSortItemType(_sorterStruct.getSortItem(i));
        object sortItemVal = getSortItemVal(item.getSortItem(i));

        //tVal = createSlType(_sorterStruct.getSortItem(i).type, tVal);
        tVal = createSlType(sortItemType, tVal);

        //object objKey = Activator.CreateInstance(tVal);
        object objKey;
        object objSort = instanceDescendingSorter(i);
        if (objSort == null)
          objKey = Activator.CreateInstance(tVal);
        else
          objKey = Activator.CreateInstance(tVal, new object[] { objSort });
        mi = objKey.GetType().GetMethod("Add");
        //mi.Invoke(objKey, new object[] { item.getSortItem(i).val,objVal });
        mi.Invoke(objKey, new object[] { sortItemVal, objVal });
        objVal = objKey;
      }
      ret = objVal;
      return ret;
    }

    private void appendOrCreateObj(SortObject item)
    {
      object objInternal = _objSL;
      for (int i = 0; i < _sorterStruct.itemCount; i++)
      {
        SortItem sortItem = item.getSortItem(i);
        object objSortItemVal = getSortItemVal(sortItem);
        //if (containKey(objInternal, sortItem.val))
        if (containKey(objInternal, objSortItemVal))
        {
          //PropertyInfo pi = objInternal.GetType().GetProperty("Item", new Type[] {sortItem.type});
          PropertyInfo pi = objInternal.GetType().GetProperty("Item", new Type[] { getSortItemType(sortItem) });
          //objInternal = pi.GetValue(objInternal, new object[] {sortItem.val});
          objInternal = pi.GetValue(objInternal, new object[] { objSortItemVal });
          if (i == _sorterStruct.itemCount - 1)
          {
            MethodInfo mi = objInternal.GetType().GetMethod("Add");
            //mi.Invoke(objInternal, new object[] { item.objToSort });
            mi.Invoke(objInternal, new object[] { item });
          }
        }
        else
        {
          object obj = instanceSLFromIndex(item, i);
          MethodInfo mi = objInternal.GetType().GetMethod("Add");
          //PropertyInfo pi = obj.GetType().GetProperty("Item", new Type[] { sortItem.type });
          //object o = pi.GetValue(obj, new object[] { sortItem.val });
          //mi.Invoke(objInternal, new object[] { item.getSortItem(i).val, o });
          //mi.Invoke(objInternal, new object[] { item.getSortItem(i).val, obj });
          mi.Invoke(objInternal, new object[] { objSortItemVal, obj });
          break;
        }
      }
      _objCount++;
    }

    private Type getSortItemType(SorterItem sorterItem)
    {
      return getSortItemType(sorterItem.type);
    }

    private Type getSortItemType(SortItem sortItem)
    {
      return getSortItemType(sortItem.type);
    }

    private Type getSortItemType(Type sortItemType)
    {
      Type ret;
      if (sortItemType.Equals(typeof(string)))
        ret = typeof (double);
      else
        ret = sortItemType;
      return ret;
    }

    private object getSortItemVal(SortItem sortItem)
    {
      return getSortItemVal(sortItem.type, sortItem.val);
    }

    private object getSortItemVal(Type t,object val)
    {
      object ret = 0;
      if (val != null && t != null)
      {
        if (t.Equals(typeof (string)))
          ret = getStringVal(val.ToString());
        else
          ret = val;
      }
      return ret;
    }

    private double getStringVal(string str)
    {
      double ret = 0;
      if (!string.IsNullOrEmpty(str))
      {
        string base64 = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(str));
        try
        {
          for (int i = 0; i < base64.Length; i++)
            ret += Math.Pow(2, i+1) * base64[i];
        }
        catch
        {
        }
      }
      return ret;
    }

    //private static bool containKey(object objSL,object val)
    private bool containKey(object objSL, object val)
    {
      bool ret = false;
      if(objSL!=null && val!=null)
      {
        MethodInfo mi = objSL.GetType().GetMethod("ContainsKey");
        ret = (bool) mi.Invoke(objSL, new object[] {val});
      }
      return ret;
    }

    private bool isItemCompliant(SortObject item)
    {
      bool ret = false;
      if(item!=null && item.itemCount==_sorterStruct.itemCount)
      {
        ret = true;
        for(int i=0;i<item.itemCount;i++)
        {
          if(!item.getSortItem(i).type.Equals(_sorterStruct.getSortItem(i).type))
          {
            ret = false;
            break;
          }
        }
      }
      return ret;
    }

    public SorterStructure sorterStructure
    {
      get { return _sorterStruct;}
    }

  }
}
