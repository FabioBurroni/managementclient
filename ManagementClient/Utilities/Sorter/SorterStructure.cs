using System;
using System.Collections.Generic;

//using System.Text;

namespace Utilities.Sorter
{
  public class SorterStructure
  {
    private readonly List<SorterItem> _items = null;
    private readonly Type _objToSortType = null;
    
    public SorterStructure(Type objToSortT)
    {
      _items = new List<SorterItem>();
      _objToSortType = objToSortT;
    }

    public Type objToSortType
    {
      get { return _objToSortType;}
    }

    public void addItem(SorterItem item)
    {
      if(item!=null)
        _items.Add(item);
    }

    public SorterItem[] getAllItems()
    {
      SorterItem[] ret = null;
      if (_items.Count > 0)
        ret = _items.ToArray();
      return ret;
    }

    public SorterItem getSortItem(int index)
    {
      SorterItem ret = null;
      if (index >= 0 && _items.Count > index)
        ret = _items[index];
      return ret;
    }

    public int itemCount
    {
      get{ return _items.Count;}
    }

  }
}
