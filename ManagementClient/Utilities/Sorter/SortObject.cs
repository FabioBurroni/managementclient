using System.Collections.Generic;

namespace Utilities.Sorter
{
  public class SortObject
  {
    private readonly List<SortItem> _items = null;
    private readonly object _objToSort = null;
    public SortObject(object objToSrt)
    {
      _objToSort = objToSrt;
      _items = new List<SortItem>();
    }

    public void addItem(SortItem item)
    {
      if (item != null)
        _items.Add(item);
    }

    public int itemCount
    {
      get { return _items.Count; }
    }

    public SortItem[] getAllItems()
    {
      SortItem[] ret = null;
      if (_items.Count > 0)
        ret = _items.ToArray();
      return ret;
    }

    public SortItem getSortItem(int index)
    {
      SortItem ret = null;
      if (index >= 0 && _items.Count > index)
        ret = _items[index];
      return ret;
    }

    public object objToSort
    {
      get { return _objToSort; }
    }
  }
}
