using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Utilities
{
  /// <summary>
  /// Summary description for DataCrypter.
  /// </summary>
  public class DataCrypter
  {
    private static byte[] _keyb;
    private static readonly byte[] Ivb = { 10, 61, 25, 12, 122, 120, 80, 248, 13, 182, 196, 212, 176, 46, 23, 85 };

    private static byte[] LoadKey(string keyVal)
    {
      //Byte[] key = new Byte[32];
      byte[] key = Encoding.UTF8.GetBytes(keyVal);
      return key;
    }

    // cripta la stringa
    public static string EncryptString(string src, string key)
    {
      _keyb = LoadKey(key);

      byte[] p = Encoding.ASCII.GetBytes(src.ToCharArray());
      byte[] encodedBytes;

      MemoryStream ms = new MemoryStream();
      RijndaelManaged rv = new RijndaelManaged();
      CryptoStream cs = new CryptoStream(ms, rv.CreateEncryptor(_keyb, Ivb), CryptoStreamMode.Write);

      try
      {
        cs.Write(p, 0, p.Length);
        cs.FlushFinalBlock();
        encodedBytes = ms.ToArray();
      }
      finally
      {
        ms.Close();
        cs.Close();
      }

      return Convert.ToBase64String(encodedBytes);
    }

    // descripta
    public static string DecryptString(string src, string key)
    {
      try
      {
        if (src != string.Empty)
        {
          _keyb = LoadKey(key);

          byte[] p = Convert.FromBase64String(src);
          byte[] initialText = new byte[p.Length];

          RijndaelManaged rv = new RijndaelManaged();
          MemoryStream ms = new MemoryStream(p);
          CryptoStream cs = new CryptoStream(ms, rv.CreateDecryptor(_keyb, Ivb), CryptoStreamMode.Read);

          try
          {
            cs.Read(initialText, 0, initialText.Length);
          }
          finally
          {
            ms.Close();
            cs.Close();
          }

          StringBuilder sb = new StringBuilder();

          for (int i = 0; i < initialText.Length; ++i)
          {
            sb.Append((char)initialText[i]);
          }
          string ret = sb.ToString();
          for (int i = ret.Length - 1; i > 0; i--)
          {
            if (ret[i] == '\0')
              ret = ret.Substring(0, ret.Length - 1);
            else
              break;
          }
          return ret;
        }
      }
      catch
      {

      }
      return src;
    }
  }
}