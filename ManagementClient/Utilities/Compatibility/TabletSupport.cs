﻿using System;

namespace Utilities.Compatibility
{
	public static class TabletSupport
	{
		#region ShowOnScreenKeyboard

		/// <summary>
		/// Se l'applicazione esegue su tablet, il metodo permette di aprire la tastiera virtuale ogni volta che
		/// l'utente clicca su controllo editable (TextBox, RichTextbox ecc).
		/// Il metodo va invocato una volta sola nell'evento Load della schermata principale.
		/// </summary>
		public static void ShowOnScreenKeyboard(System.Windows.Forms.Form mainFrom)
		{
			ShowOnScreenKeyboard(mainFrom.Handle);
		}

		/// <summary>
		/// Se l'applicazione esegue su tablet, il metodo permette di aprire la tastiera virtuale ogni volta che
		/// l'utente clicca su controllo editable (TextBox, RichTextbox ecc).
		/// Il metodo va invocato una volta sola nell'evento Loaded della schermata principale.
		/// </summary>
		public static void ShowOnScreenKeyboard(System.Windows.Window mainWindow)
		{
			ShowOnScreenKeyboard(new System.Windows.Interop.WindowInteropHelper(mainWindow).Handle);
		}

		private static void ShowOnScreenKeyboard(IntPtr intPtr)
		{
			var asForm = System.Windows.Automation.AutomationElement.FromHandle(intPtr);
		}

		#endregion
	}
}