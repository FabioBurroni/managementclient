﻿using System;
using System.Runtime.Serialization;

namespace Utilities.Exceptions
{
  [Serializable]
  public class DuplicateKeyException : Exception
  {
    public DuplicateKeyException()
      : base()
    { }

    public DuplicateKeyException(string message)
      : base(message)
    { }

    public DuplicateKeyException(string format, params object[] args)
      : base(string.Format(format, args))
    { }

    public DuplicateKeyException(string message, Exception innerException)
      : base(message, innerException)
    { }

    public DuplicateKeyException(string format, Exception innerException, params object[] args)
      : base(string.Format(format, args), innerException)
    { }

    protected DuplicateKeyException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    { }
  }
}