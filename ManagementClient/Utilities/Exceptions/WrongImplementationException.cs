﻿using System;

namespace Utilities.Exceptions
{
  public class WrongImplementationException:Exception
  {
    public WrongImplementationException(string recommendedUsage)
    {
      this.recommendedUsage = recommendedUsage;
    }
    public string recommendedUsage { get; private set; }

  }
}
