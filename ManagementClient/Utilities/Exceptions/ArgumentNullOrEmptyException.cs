﻿using System;
using System.Runtime.Serialization;

namespace Utilities.Exceptions
{
  [Serializable]
  public class ArgumentNullOrEmptyException : Exception
  {
    public ArgumentNullOrEmptyException()
      : base()
    { }

    public ArgumentNullOrEmptyException(string message)
      : base(message)
    { }

    public ArgumentNullOrEmptyException(string format, params object[] args)
      : base(string.Format(format, args))
    { }

    public ArgumentNullOrEmptyException(string message, Exception innerException)
      : base(message, innerException)
    { }

    public ArgumentNullOrEmptyException(string format, Exception innerException, params object[] args)
      : base(string.Format(format, args), innerException)
    { }

    protected ArgumentNullOrEmptyException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    { }
  }
}