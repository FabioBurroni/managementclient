﻿using System;
using System.Runtime.Serialization;

namespace Utilities.Exceptions
{
  [Serializable]
  public class LengthException : Exception
  {
    public LengthException()
      : base()
    { }

    public LengthException(string message)
      : base(message)
    { }

    public LengthException(string format, params object[] args)
      : base(string.Format(format, args))
    { }

    public LengthException(string message, Exception innerException)
      : base(message, innerException)
    { }

    public LengthException(string format, Exception innerException, params object[] args)
      : base(string.Format(format, args), innerException)
    { }

    protected LengthException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    { }
  }
}