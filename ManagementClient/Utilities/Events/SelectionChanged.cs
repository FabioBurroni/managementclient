﻿using System;

namespace Utilities.Events
{
  public delegate void SelectionChangedHandler(object sender, SelectionChangedEventArgs e);

  public class SelectionChangedEventArgs : EventArgs
  {
    public int OldKey { get; }

    public int NewKey { get; }

    public SelectionChangedEventArgs(int oldKey, int newKey)
    {
      if (oldKey == newKey)
        throw new Exception($"OldKey and NewKey have same value '{oldKey}'");

      OldKey = oldKey;
      NewKey = newKey;
    }
  }
}