﻿using System;
using Utilities.Exceptions;

namespace Utilities.Events
{
  public delegate void CultureChangedHandler(object sender, CultureChangedEventArgs e);

  public class CultureChangedEventArgs : EventArgs
  {
    public string OldCulture { get; }

    public string NewCulture { get; }

    public CultureChangedEventArgs(string oldCulture, string newCulture)
    {
      if (string.IsNullOrEmpty(oldCulture))
        throw new ArgumentNullOrEmptyException(nameof(oldCulture));

      if (string.IsNullOrEmpty(newCulture))
        throw new ArgumentNullOrEmptyException(nameof(newCulture));

      OldCulture = oldCulture;
      NewCulture = newCulture;
    }
  }
}