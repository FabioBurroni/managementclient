﻿using System;
using System.Linq;
using System.Reflection;

namespace Utilities.Extensions
{
	public static class TypeExtension
	{
		#region GetValue

		/// <summary>
		/// Permette di ottenere il valore di una proprietà pubblica su certo oggetto
		/// </summary>
		/// <typeparam name="T">Tipo del valore di ritorno</typeparam>
		/// <param name="type">Tipo dell'oggetto</param>
		/// <param name="obj">Istanza dell'oggetto</param>
		/// <param name="property">Proprietà da settare</param>
		/// <param name="value">Valore ottenuto</param>
		/// <returns>True se tutto è andato bene, se false non prendere il considerazione il valore di value</returns>
		public static bool GetValueOnPublicProperty<T>(this Type type, object obj, string property, out T value)
		{
			return type.GetValueOnProperty(obj, property, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public, out value);
		}

		/// <summary>
		/// Permette di ottenere il valore di una proprietà privata su certo oggetto
		/// </summary>
		/// <typeparam name="T">Tipo del valore di ritorno</typeparam>
		/// <param name="type">Tipo dell'oggetto</param>
		/// <param name="obj">Istanza dell'oggetto</param>
		/// <param name="property">Proprietà da settare</param>
		/// <param name="value">Valore ottenuto</param>
		/// <returns>True se tutto è andato bene, se false non prendere il considerazione il valore di value</returns>
		public static bool GetValueOnPrivateProperty<T>(this Type type, object obj, string property, out T value)
		{
			return type.GetValueOnProperty(obj, property, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.NonPublic, out value);
		}

		/// <summary>
		/// Permette di ottenere il valore di una proprietà pubblica o privata su certo oggetto
		/// </summary>
		/// <typeparam name="T">Tipo del valore di ritorno</typeparam>
		/// <param name="type">Tipo dell'oggetto</param>
		/// <param name="obj">Istanza dell'oggetto</param>
		/// <param name="property">Proprietà da settare</param>
		/// <param name="value">Valore ottenuto</param>
		/// <returns>True se tutto è andato bene, se false non prendere il considerazione il valore di value</returns>
		public static bool GetValueOnProperty<T>(this Type type, object obj, string property, out T value)
		{
			return type.GetValueOnProperty(obj, property, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, out value);
		}

		/// <summary>
		/// Permette di ottenere il valore di una proprietà su certo oggetto
		/// </summary>
		/// <typeparam name="T">Tipo del valore di ritorno</typeparam>
		/// <param name="type">Tipo dell'oggetto</param>
		/// <param name="obj">Istanza dell'oggetto</param>
		/// <param name="property">Proprietà da settare</param>
		/// <param name="flags">Flags per la reflection</param>
		/// <param name="value">Valore ottenuto</param>
		/// <returns>True se tutto è andato bene, se false non prendere il considerazione il valore di value</returns>
		private static bool GetValueOnProperty<T>(this IReflect type, object obj, string property, BindingFlags flags, out T value)
		{
			if (type == null)
			{
				throw new ArgumentNullException(nameof(type));
			}

			if (obj == null)
			{
				throw new ArgumentNullException(nameof(obj));
			}

			if (string.IsNullOrEmpty(property))
			{
				throw new ArgumentNullException(nameof(property));
			}

			value = default(T);

			//recupero la proprietà
			var propInfo = type.GetProperty(property, flags);

			//la proprietà non esiste, si esce
			if (propInfo == null)
				return false;

			//recupero il valore
			value = (T)propInfo.GetValue(obj, null);

			return true;
		}

		#endregion

		#region SetValue

		/// <summary>
		///  Permette di settare il valore di una proprietà pubblica su un certo oggetto
		/// </summary>
		/// <param name="type">Tipo dell'oggetto</param>
		/// <param name="obj">Istanza dell'oggetto</param>
		/// <param name="property">Proprietà da settare</param>
		/// <param name="value">Valore</param>
		/// <returns>True se tutto è andato bene</returns>
		public static bool SetValueOnPublicProperty(this Type type, object obj, string property, object value)
		{
			return type.SetValueOnProperty(obj, property, value, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
		}

		/// <summary>
		///  Permette di settare il valore di una proprietà private su un certo oggetto
		/// </summary>
		/// <param name="type">Tipo dell'oggetto</param>
		/// <param name="obj">Istanza dell'oggetto</param>
		/// <param name="property">Proprietà da settare</param>
		/// <param name="value">Valore</param>
		/// <returns>True se tutto è andato bene</returns>
		public static bool SetValueOnPrivateProperty(this Type type, object obj, string property, object value)
		{
			return type.SetValueOnProperty(obj, property, value, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.NonPublic);
		}

		/// <summary>
		///  Permette di settare il valore di una proprietà pubblica o privata su un certo oggetto
		/// </summary>
		/// <param name="type">Tipo dell'oggetto</param>
		/// <param name="obj">Istanza dell'oggetto</param>
		/// <param name="property">Proprietà da settare</param>
		/// <param name="value">Valore</param>
		/// <returns>True se tutto è andato bene</returns>
		public static bool SetValueOnProperty(this Type type, object obj, string property, object value)
		{
			return type.SetValueOnProperty(obj, property, value, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
		}

		/// <summary>
		/// Permette di settare il valore di una proprietà su un certo oggetto
		/// </summary>
		/// <param name="type">Tipo dell'oggetto</param>
		/// <param name="obj">Istanza dell'oggetto</param>
		/// <param name="property">Proprietà da settare</param>
		/// <param name="value">Valore</param>
		/// <param name="flags">Flags per reflection</param>
		/// <returns>True se tutto è andato bene</returns>
		private static bool SetValueOnProperty(this IReflect type, object obj, string property, object value, BindingFlags flags)
		{
			if (type == null)
			{
				throw new ArgumentNullException(nameof(type));
			}

			if (obj == null)
			{
				throw new ArgumentNullException(nameof(obj));
			}

			if (string.IsNullOrEmpty(property))
			{
				throw new ArgumentNullException(nameof(property));
			}

			//recupero la proprietà
			var propInfo = type.GetProperty(property, flags);

			//se la proprietà non esiste, si esce
			if (propInfo == null)
				return false;

			//setto il valore
			propInfo.SetValue(obj, value, null);

			return true;
		}

		#endregion

		#region ContainsMethod

		/// <summary>
		/// Verifica se esiste il metodo pubblico
		/// </summary>
		/// <param name="type">Tipo di oggetto</param>
		/// <param name="method">Nome del metodo</param>
		public static bool ContainsPublicMethod(this Type type, string method)
		{
			return ContainsMethod(type, method, BindingFlags.Instance | BindingFlags.Public);
		}

		/// <summary>
		/// Verifica se esiste il metodo pubblico
		/// </summary>
		/// <param name="type">Tipo di oggetto</param>
		/// <param name="method">Nome del metodo</param>
		public static bool ContainsPrivateMethod(this Type type, string method)
		{
			return ContainsMethod(type, method, BindingFlags.Instance | BindingFlags.NonPublic);
		}

		/// <summary>
		/// Verifica se esiste il metodo pubblico
		/// </summary>
		/// <param name="type">Tipo di oggetto</param>
		/// <param name="method">Nome del metodo</param>
		public static bool ContainsMethod(this Type type, string method)
		{
			return ContainsMethod(type, method, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
		}

		/// <summary>
		/// Verifica se esiste il metodo pubblico
		/// </summary>
		/// <param name="type">Tipo di oggetto</param>
		/// <param name="method">Nome del metodo</param>
		/// <param name="bindingFlags">Flags per reflection</param>
		private static bool ContainsMethod(this IReflect type, string method, BindingFlags bindingFlags)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			if (method == null)
				throw new ArgumentNullException(nameof(method));

			//recupero il metodo
			var mi = type.GetMethod(method, bindingFlags);

			//verifico se il metodo esiste
			return mi != null;
		}

		#endregion

		#region InvokeMethod

		/// <summary>
		/// Invoca un metodo marcato come public
		/// </summary>
		/// <param name="type">Tipo dell'istanza</param>
		/// <param name="instance">Istanza su cui invocare il metodo</param>
		/// <param name="method">Metodo da invocare</param>
		/// <param name="parameters">Eventuali parametri da passare</param>
		/// <returns>Restituisce l'eventuale valore di ritorno della chiamata</returns>
		public static object InvokePublicMethod(this Type type, object instance, string method, object[] parameters)
		{
			return InvokeMethod(type, instance, method, parameters, BindingFlags.Instance | BindingFlags.Public);
		}

		/// <summary>
		/// Invoca un metodo marcato come public
		/// </summary>
		/// <param name="type">Tipo dell'istanza</param>
		/// <param name="instance">Istanza su cui invocare il metodo</param>
		/// <param name="method">Metodo da invocare</param>
		/// <param name="types">Tipi degli eventuali parametri</param>
		/// <param name="parameters">Eventuali parametri da passare</param>
		/// <returns>Restituisce l'eventuale valore di ritorno della chiamata</returns>
		public static object InvokePublicMethod(this Type type, object instance, string method, Type[] types, object[] parameters)
		{
			return InvokeMethod(type, instance, method, types, parameters, BindingFlags.Instance | BindingFlags.Public);
		}

		/// <summary>
		/// Invoca un metodo marcato come private o internal
		/// </summary>
		/// <param name="type">Tipo dell'istanza</param>
		/// <param name="instance">Istanza su cui invocare il metodo</param>
		/// <param name="method">Metodo da invocare</param>
		/// <param name="parameters">Eventuali parametri da passare</param>
		/// <returns>Restituisce l'eventuale valore di ritorno della chiamata</returns>
		public static object InvokePrivateMethod(this Type type, object instance, string method, object[] parameters)
		{
			return InvokeMethod(type, instance, method, parameters, BindingFlags.Instance | BindingFlags.NonPublic);
		}

		/// <summary>
		/// Invoca un metodo marcato come private o internal
		/// </summary>
		/// <param name="type">Tipo dell'istanza</param>
		/// <param name="instance">Istanza su cui invocare il metodo</param>
		/// <param name="method">Metodo da invocare</param>
		/// <param name="types">Tipi degli eventuali parametri</param>
		/// <param name="parameters">Eventuali parametri da passare</param>
		/// <returns>Restituisce l'eventuale valore di ritorno della chiamata</returns>
		public static object InvokePrivateMethod(this Type type, object instance, string method, Type[] types, object[] parameters)
		{
			return InvokeMethod(type, instance, method, types, parameters, BindingFlags.Instance | BindingFlags.NonPublic);
		}

		/// <summary>
		/// Invoca un metodo marcato come public
		/// </summary>
		/// <param name="type">Tipo dell'istanza</param>
		/// <param name="instance">Istanza su cui invocare il metodo</param>
		/// <param name="method">Metodo da invocare</param>
		/// <param name="parameters">Eventuali parametri da passare</param>
		/// <returns>Restituisce l'eventuale valore di ritorno della chiamata</returns>
		public static object InvokeMethod(this Type type, object instance, string method, object[] parameters)
		{
			return InvokeMethod(type, instance, method, parameters, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
		}

		/// <summary>
		/// Invoca un metodo marcato come public
		/// </summary>
		/// <param name="type">Tipo dell'istanza</param>
		/// <param name="instance">Istanza su cui invocare il metodo</param>
		/// <param name="method">Metodo da invocare</param>
		/// <param name="types">Tipi degli eventuali parametri</param>
		/// <param name="parameters">Eventuali parametri da passare</param>
		/// <returns>Restituisce l'eventuale valore di ritorno della chiamata</returns>
		public static object InvokeMethod(this Type type, object instance, string method, Type[] types, object[] parameters)
		{
			return InvokeMethod(type, instance, method, types, parameters, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
		}

		/// <summary>
		/// Invoca un metodo
		/// </summary>
		/// <param name="type">Tipo dell'istanza</param>
		/// <param name="instance">Istanza su cui invocare il metodo</param>
		/// <param name="method">Metodo da invocare</param>
		/// <param name="parameters">Eventuali parametri da passare</param>
		/// <param name="bindingFlags">Flags per reflection</param>
		/// <returns>Restituisce l'eventuale valore di ritorno della chiamata</returns>
		private static object InvokeMethod(this IReflect type, object instance, string method, object[] parameters, BindingFlags bindingFlags)
		{
			if (parameters != null && parameters.Contains(null))
				throw new ArgumentException($"{nameof(parameters)} cannot contain null value");

			var types = parameters?.Select(p => p.GetType()).ToArray();

			return InvokeMethod(type, instance, method, types, parameters, bindingFlags);
		}

		/// <summary>
		/// Invoca un metodo
		/// </summary>
		/// <param name="type">Tipo dell'istanza</param>
		/// <param name="instance">Istanza su cui invocare il metodo</param>
		/// <param name="method">Metodo da invocare</param>
		/// <param name="types">Tipi degli eventuali parametri</param>
		/// <param name="parameters">Eventuali parametri da passare</param>
		/// <param name="bindingFlags">Flags per reflection</param>
		/// <returns>Restituisce l'eventuale valore di ritorno della chiamata</returns>
		private static object InvokeMethod(this IReflect type, object instance, string method, Type[] types, object[] parameters, BindingFlags bindingFlags)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			if (instance == null)
				throw new ArgumentNullException(nameof(instance));

			if (string.IsNullOrEmpty(method))
				throw new ArgumentNullException(nameof(method));

			if (!instance.GetType().Equals(type))
				throw new Exception(string.Format("Argument {0} and {1} must be of the same data type", type, instance));

			if (types.Length != parameters.Length)
				throw new ArgumentException($"{nameof(types)} length must be equal to {nameof(parameters)} length");

			//recupero il metodo
			var mi = type.GetMethod(method, bindingFlags, null, types, null);

			//se la proprietà non esiste, si esce
			if (mi == null)
				return null;

			return mi.Invoke(instance, parameters);
		}

		#endregion

		#region IsSimpleType

		/// <summary>
		/// Determine whether a type is simple (String, Decimal, DateTime, etc) 
		/// or complex (i.e. custom class with public properties and methods).
		/// </summary>
		/// <see cref="http://stackoverflow.com/questions/2442534/how-to-test-if-type-is-primitive"/>
		public static bool IsSimpleType(this Type type)
		{
			return
				type.IsValueType ||
				type.IsPrimitive ||
				new[] {
					typeof(string),
					typeof(decimal),
					typeof(DateTime),
					typeof(DateTimeOffset),
					typeof(TimeSpan),
					typeof(Guid)
				}.Contains(type) ||
				Convert.GetTypeCode(type) != TypeCode.Object;
		}

		#endregion

		#region IsDefault

		/// <summary>
		/// Indica se il tipo T è uguale al valore di default
		/// </summary>
		public static bool IsDefault<T>(this T value) where T : struct
		{
			return value.Equals(default(T));
		}

		#endregion

		#region GetType

		/// <summary>
		/// Restituisce il tipo a partire dalla nome cercandolo in tutto il dominio dell'applicazione
		/// </summary>
		public static Type GetTypeInAppDomain(this string typeName)
		{
			var type = Type.GetType(typeName);
			if (type != null) return type;
			foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
			{
				type = a.GetType(typeName);
				if (type != null)
					return type;
			}
			return null;
		}

		#endregion
	}
}