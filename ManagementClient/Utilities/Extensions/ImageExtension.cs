﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Utilities.Extensions
{
  public static class ImageExtension
  {
    /// <summary>
    /// Ridimensiona l'immagine con le misure passate come argomento mantenendo le proporzioni dell'immagine stessa
    /// </summary>
    public static Image ResizePreservingAspectRatio(this Image imgPhoto, int width, int height)
    {
      if (imgPhoto == null)
        throw new NullReferenceException(nameof(imgPhoto));

      int sourceWidth = imgPhoto.Width;
      int sourceHeight = imgPhoto.Height;
      int sourceX = 0;
      int sourceY = 0;
      int destX = 0;
      int destY = 0;

      float nPercent;

      var nPercentW = (float)width / sourceWidth;
      var nPercentH = (float)height / sourceHeight;
      if (nPercentH < nPercentW)
      {
        nPercent = nPercentH;
        destX = Convert.ToInt16((width - sourceWidth * nPercent) / 2);
      }
      else
      {
        nPercent = nPercentW;
        destY = Convert.ToInt16((height - sourceHeight * nPercent) / 2);
      }

      int destWidth = (int)(sourceWidth * nPercent);
      int destHeight = (int)(sourceHeight * nPercent);

      Bitmap bmPhoto = new Bitmap(width, height, PixelFormat.Format24bppRgb);
      bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

      using (var grPhoto = Graphics.FromImage(bmPhoto))
      {
        grPhoto.Clear(Color.White);
        grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

        grPhoto.DrawImage(imgPhoto,
          new Rectangle(destX, destY, destWidth, destHeight),
          new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
          GraphicsUnit.Pixel);
      }

      return bmPhoto;
    }
  }
}