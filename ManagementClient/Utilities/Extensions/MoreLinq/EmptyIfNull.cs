﻿using System.Collections.Generic;
using System.Linq;

namespace Utilities.Extensions.MoreLinq
{
  static partial class MoreEnumerable
  {
    /// <summary>
    /// If the collection is null, return an empty projection of it.
    /// </summary>
    /// <typeparam name="TSource">The IEnumerable type.</typeparam>
    /// <param name="source">The enumerable, which may be null or empty.</param>
    public static IEnumerable<TSource> EmptyIfNull<TSource>(this IEnumerable<TSource> source)
    {
      return source ?? Enumerable.Empty<TSource>();
    }
  }
}