#region License and Terms
// MoreLINQ - Extensions to LINQ to Objects
// Copyright (c) 2008 Jonathan Skeet. All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#endregion

using System.Collections.Generic;
using System.Linq;

namespace Utilities.Extensions.MoreLinq
{
  static partial class MoreEnumerable
  {
    /// <summary>
    /// Determines whether the collection is null or contains no elements.
    /// </summary>
    /// <typeparam name="TSource">The IEnumerable type.</typeparam>
    /// <param name="source">The enumerable, which may be null or empty.</param>
    /// <returns>
    /// <c>true</c> if the IEnumerable is null or empty; otherwise, <c>false</c>.
    /// </returns>

    public static bool IsNullOrEmpty<TSource>(this IEnumerable<TSource> source)
    {
      if (source == null)
      {
        return true;
      }
      /* If this is a list, use the Count property for efficiency. 
       * The Count property is O(1) while IEnumerable.Count() is O(N). */
      var collection = source as ICollection<TSource>;
      if (collection != null)
      {
        return collection.Count < 1;
      }
      return !source.Any();
    }
  }
}