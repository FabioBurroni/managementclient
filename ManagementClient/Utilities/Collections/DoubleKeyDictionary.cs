﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utilities.Collections
{
  /// <summary>
  /// Double Key Dictionary Class
  /// </summary>	
  /// <typeparam name="K">Primary Key Type</typeparam>
  /// <typeparam name="S">Sub Key Type</typeparam>
  /// <typeparam name="V">Value Type</typeparam>
  public class DoubleKeyDictionary<K, S, V>
  {
    #region Fields

    private readonly object _objForLock = new object();

    private readonly IEqualityComparer<K> _primaryKeyComparer;
    private readonly IEqualityComparer<S> _subKeyComparer;

    private readonly Dictionary<K, V> _dicBase;
    private readonly Dictionary<S, K> _dicSub;
    private readonly Dictionary<K, S> _dicBase2Sub;

    #endregion

    #region Properties

    /// <summary>
    /// Get Value from its own SubKey. If V is a reference type, for no subKey found will be returned a null value, otherwhise a KeyNotFoundException
    /// </summary>
    /// <param name="subKey">SubKey to search</param>
    /// <returns>Value corresponding to SubKey</returns>
    public V this[S subKey]
    {
      get
      {
        //V item;
        //if (TryGetValue(subKey, out item))
        //  return item;

        //V ret = default(V);
        //if ((!typeof(S).IsValueType && EqualityComparer<S>.Default.Equals(subKey, default(S)) && TryGetValue(subKey, out ret))
        //    || (typeof(S).IsValueType && TryGetValue(subKey, out ret)))
        //  return ret;

        //if ((!typeof(S).IsValueType && EqualityComparer<S>.Default.Equals(subKey, default(S))) && TryGetValue(subKey, out ret))
        //  throw new ArgumentNullException("subKey", "Not valid parameters");

        if (!typeof(S).IsValueType && _subKeyComparer.Equals(subKey, default(S)))
          throw new ArgumentNullException("subKey", "Not valid parameters");

        V ret;
        TryGetValue(subKey, out ret);
        return ret;

        //if (!typeof(V).IsValueType)
        //  return ret;

        //throw new KeyNotFoundException(string.Format("Sub key not found: {0}",subKey));
      }
      set
      {
        bool err = true;
        if (typeof(S).IsValueType || !_subKeyComparer.Equals(subKey, default(S)))
        {
          lock (_objForLock)
          {
            if (_dicSub.ContainsKey(subKey))
            {
              K pk = _dicSub[subKey];
              if (_dicBase.ContainsKey(pk))
              {
                _dicBase[pk] = value;
                err = false;
              }
            }
          }
        }
        if (err)
          throw new KeyNotFoundException(string.Format("Sub key not found: {0}", subKey));
      }
    }

    /// <summary>
    /// Get Value from its own PrimaryKey. If V is a reference type, for no primaryKey found will be returned a null value, otherwhise a KeyNotFoundException
    /// </summary>
    /// <param name="primaryKey">PrimaryKey to search</param>
    /// <returns>Value corresponding to PrimaryKey</returns>
		public V this[K primaryKey]
    {
      get
      {
        //V item;
        //if (!primaryKey.Equals(default(K)) && TryGetValue(primaryKey, out item))
        //  return item;

        //V ret = default(V);
        //if ( (!typeof(K).IsValueType && EqualityComparer<K>.Default.Equals(primaryKey, default(K)) && TryGetValue(primaryKey,out ret))
        //    || (typeof(K).IsValueType && TryGetValue(primaryKey, out ret)))
        //  return ret;

        //if ((!typeof(K).IsValueType && EqualityComparer<K>.Default.Equals(primaryKey, default(K))) && TryGetValue(primaryKey, out ret))
        //  throw new ArgumentNullException("primaryKey", "Not valid parameters");

        if (!typeof(K).IsValueType && _primaryKeyComparer.Equals(primaryKey, default(K)))
          throw new ArgumentNullException("primaryKey", "Not valid parameters");

        V ret;
        TryGetValue(primaryKey, out ret);
        return ret;

        //if (!typeof(V).IsValueType)
        //  return ret;

        //throw new KeyNotFoundException(string.Format("Primary key not found: {0}", primaryKey));
      }
      set
      {
        bool err = true;
        if (typeof(K).IsValueType || !_primaryKeyComparer.Equals(primaryKey, default(K)))
        {
          lock (_objForLock)
          {
            if (_dicBase.ContainsKey(primaryKey))
            {
              _dicBase[primaryKey] = value;
              err = false;
            }
          }
        }
        if (err)
          throw new KeyNotFoundException(string.Format("Primary key not found: {0}", primaryKey));
      }
    }

    #endregion

    #region Constructors

    public DoubleKeyDictionary()
      : this(EqualityComparer<K>.Default, EqualityComparer<S>.Default)
    {
    }

    public DoubleKeyDictionary(IEqualityComparer<K> primaryKeyComparer)
      : this(primaryKeyComparer, EqualityComparer<S>.Default)
    {
    }

    public DoubleKeyDictionary(IEqualityComparer<S> subKeyComparer)
      : this(EqualityComparer<K>.Default, subKeyComparer)
    {
    }

    public DoubleKeyDictionary(IEqualityComparer<K> primaryKeyComparer, IEqualityComparer<S> subKeyComparer)
    {
      if (primaryKeyComparer == null)
        throw new ArgumentNullException(nameof(primaryKeyComparer));

      if (subKeyComparer == null)
        throw new ArgumentNullException(nameof(subKeyComparer));

      _primaryKeyComparer = primaryKeyComparer;
      _subKeyComparer = subKeyComparer;

      _dicBase = new Dictionary<K, V>(_primaryKeyComparer);
      _dicBase2Sub = new Dictionary<K, S>(_primaryKeyComparer);
      _dicSub = new Dictionary<S, K>(_subKeyComparer);
    }

    #endregion

    public bool AssociateSubKey(S subKey, K primaryKey)
    {
      lock (_objForLock)
      {
        return AssociateSubKeyPri(subKey, primaryKey);
      }
    }

    private bool AssociateSubKeyPri(S subKey, K primaryKey)
    {
      if (!typeof(K).IsValueType && _primaryKeyComparer.Equals(primaryKey, default(K)))
        throw new ArgumentNullException("primaryKey", "Not valid parameters");

      if (!typeof(S).IsValueType && _subKeyComparer.Equals(subKey, default(S)))
        throw new ArgumentNullException("subKey", "Not valid parameters");

      if (!_dicBase.ContainsKey(primaryKey))
        throw new KeyNotFoundException(string.Format("The base dictionary does not contain the key '{0}'", primaryKey));

      if (_dicBase2Sub.ContainsKey(primaryKey)) // Remove the old mapping first
      {
        if (_dicSub.ContainsKey(_dicBase2Sub[primaryKey]))
        {
          _dicSub.Remove(_dicBase2Sub[primaryKey]);
        }

        _dicBase2Sub.Remove(primaryKey);
      }

      _dicSub[subKey] = primaryKey;
      _dicBase2Sub[primaryKey] = subKey;

      return true;
    }

    public bool TryGetValue(S subKey, out V val)
    {
      bool ret = false;
      val = default(V);
      lock (_objForLock)
      {
        K primaryKey;
        if (_dicSub.TryGetValue(subKey, out primaryKey))
        {
          ret = _dicBase.TryGetValue(primaryKey, out val);
        }
      }
      return ret;
    }

    public bool TryGetValue(K primaryKey, out V val)
    {
      bool ret;
      lock (_objForLock)
      {
        ret = _dicBase.TryGetValue(primaryKey, out val);
      }
      return ret;
    }

    public bool ContainsKey(S subKey)
    {
      V val;
      return TryGetValue(subKey, out val);
    }

    public bool ContainsKey(K primaryKey)
    {
      V val;
      return TryGetValue(primaryKey, out val);
    }

    public void Remove(K primaryKey)
    {
      lock (_objForLock)
      {
        if (_dicBase2Sub.ContainsKey(primaryKey))
        {
          if (_dicSub.ContainsKey(_dicBase2Sub[primaryKey]))
          {
            _dicSub.Remove(_dicBase2Sub[primaryKey]);
          }

          _dicBase2Sub.Remove(primaryKey);
        }
        _dicBase.Remove(primaryKey);
      }
    }

    public void Remove(S subKey)
    {
      lock (_objForLock)
      {
        _dicBase.Remove(_dicSub[subKey]);
        _dicBase2Sub.Remove(_dicSub[subKey]);
        _dicSub.Remove(subKey);
      }
    }

    public void Add(K primaryKey, V val)
    {
      lock (_objForLock)
      {
        if (!_dicBase.ContainsKey(primaryKey))
          _dicBase.Add(primaryKey, val);
      }
    }

    public void Add(K primaryKey, S subKey, V val)
    {
      //if ((!typeof(K).IsValueType && EqualityComparer<K>.Default.Equals(primaryKey, default(K)))
      //  || (!typeof(S).IsValueType && EqualityComparer<S>.Default.Equals(subKey, default(S))))
      //  return;

      if (!typeof(K).IsValueType && _primaryKeyComparer.Equals(primaryKey, default(K)))
        throw new ArgumentNullException("primaryKey", "Not valid parameters");
      if (!typeof(S).IsValueType && _subKeyComparer.Equals(subKey, default(S)))
        throw new ArgumentNullException("subKey", "Not valid parameters");

      lock (_objForLock)
      {
        if (!_dicBase.ContainsKey(primaryKey) && !_dicSub.ContainsKey(subKey))
        {
          _dicBase.Add(primaryKey, val);
          AssociateSubKeyPri(subKey, primaryKey);
        }
      }

      //Add(primaryKey, val);
      //AssociateSubKey(subKey, primaryKey);
    }

    public V[] CloneValues()
    {
      V[] ret = null;
      lock (_objForLock)
      {
        if (_dicBase.Values.Count > 0)
        {
          ret = new V[_dicBase.Values.Count];
          _dicBase.Values.CopyTo(ret, 0);
        }
      }
      return ret;
    }

    public List<V> Values
    {
      get
      {
        List<V> ret;
        lock (_objForLock)
        {
          ret = _dicBase.Values.ToList();
        }
        return ret;
      }
    }

    public K[] ClonePrimaryKeys()
    {
      K[] ret = null;
      lock (_objForLock)
      {
        if (_dicBase.Keys.Count > 0)
        {
          ret = new K[_dicBase.Keys.Count];
          _dicBase.Keys.CopyTo(ret, 0);
        }
      }
      return ret;
    }

    public S[] CloneSubKeys()
    {
      S[] ret = null;
      lock (_objForLock)
      {
        if (_dicSub.Keys.Count > 0)
        {
          ret = new S[_dicSub.Keys.Count];
          _dicSub.Keys.CopyTo(ret, 0);
        }
      }
      return ret;
    }

    public void Clear()
    {
      lock (_objForLock)
      {
        _dicBase.Clear();
        _dicSub.Clear();
        _dicBase2Sub.Clear();
      }
    }

    public int Count
    {
      get
      {
        lock (_objForLock)
        {
          return _dicBase.Count;
        }
      }
    }

    public IEnumerator<KeyValuePair<K, V>> GetEnumerator()
    {
      IEnumerator<KeyValuePair<K, V>> ret;
      lock (_objForLock)
      {
        ret = _dicBase.GetEnumerator();
      }
      return ret;
    }
  }
}