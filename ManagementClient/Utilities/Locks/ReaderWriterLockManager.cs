﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Utilities.Locks
{
  #region LockBase

  public abstract class LockBase : IDisposable
  {
    private ReaderWriterLock _lock;
    private bool _disposed;

    ~LockBase()
    {
      Dispose(false);
    }

    protected LockBase(ReaderWriterLock @lock)
    {
      _lock = @lock;
    }

    public bool IsDisposed
    {
      get
      {
        return _disposed;
      }
    }

    public void Dispose()
    {
      Dispose(true);
    }

    protected internal ReaderWriterLock Lock
    {
      get
      {
        if (_disposed)
        {
          throw new ObjectDisposedException("Can't access lock for disposed object.");
        }
        return _lock;
      }
    }

    protected virtual void Dispose(bool disposing)
    {
      if (!_disposed)
      {
        _disposed = true;
        if (disposing)
        {
          GC.SuppressFinalize(this);
        }
        if (_lock != null)
        {
          _lock = null;
        }
      }
    }

    protected void AcquireReaderLock()
    {
      AcquireReaderLock(Timeout.Infinite);
    }

    protected void AcquireReaderLock(int milliseconds)
    {
      Debug.WriteLine("Acquiring ReaderLock");
      Lock.AcquireReaderLock(milliseconds);
    }

    protected void AcquireWriterLock()
    {
      AcquireWriterLock(Timeout.Infinite);
    }

    protected void AcquireWriterLock(int milliseconds)
    {
      Debug.WriteLine("Acquiring WriterLock");
      Lock.AcquireWriterLock(milliseconds);
    }

    protected void ReleaseReaderLock()
    {
      Debug.WriteLine("Releasing ReaderLock");
      if (Lock.IsReaderLockHeld)
      {
        Lock.ReleaseReaderLock();
      }
    }
  }

  #endregion

  #region ReaderLock

  public class ReaderLock : LockBase
  {
    public ReaderLock(ReaderWriterLock readerWriterLock)
      : base(readerWriterLock)
    {
      AcquireReaderLock();
    }

    ~ReaderLock()
    {
      Dispose(false);
    }

    protected override void Dispose(bool disposing)
    {
      if (IsDisposed)
      {
        return;
      }
      Debug.WriteLine("Disposing ReaderLock");
      try
      {
        ReleaseReaderLock();
      }
      catch (Exception ex)
      {
        Debug.WriteLine(ex);
        //eat error quietly
      }
      finally
      {
        base.Dispose(disposing);
      }
    }
  }

  #endregion

  #region WriterLock

  public class WriterLock : LockBase
  {
    private LockCookie _cookie;
    private bool _upgraded;

    public WriterLock(ReaderWriterLock readerWriterLock)
      : base(readerWriterLock)
    {
      AcquireWriterLock();
    }

    public WriterLock(ReaderLock readerLock)
      : base(readerLock.Lock)
    {
      UpgradeToWriterLock();
    }

    private void UpgradeToWriterLock()
    {
      UpgradeToWriterLock(Timeout.Infinite);
    }

    private void UpgradeToWriterLock(int milliseconds)
    {
      Debug.WriteLine("Upgrading ReaderLock to WriterLock.");
      _cookie = Lock.UpgradeToWriterLock(milliseconds);
      _upgraded = true;
    }

    protected override void Dispose(bool disposing)
    {
      if (IsDisposed)
      {
        return;
      }
      Debug.WriteLine("Disposing WriteLock");
      try
      {
        if (Lock.IsWriterLockHeld)
        {
          if (!_upgraded)
          {
            Lock.ReleaseWriterLock();
          }
          else
          {
            Lock.DowngradeFromWriterLock(ref _cookie);
            _upgraded = false;
          }
        }
      }
      catch (Exception ex)
      {
        Debug.WriteLine(ex);
        //eat error quietly
      }
      finally
      {
        base.Dispose(disposing);
      }
    }
  }
  #endregion
}
