﻿using System;
using System.Threading;

namespace Utilities.Locks
{
  /// <summary>
  /// Wrapper for the ReaderWriterLockSlim
  /// </summary>
  public class ReaderWriterLockSlimManager
  {
    private readonly ReaderWriterLockSlim _lockHost = new ReaderWriterLockSlim();

    /// <summary>
    /// Use with a Using statement, While in the using you have a read lock
    /// </summary>
    /// 
    public ReaderLock ReadLock() { return new ReaderLock(_lockHost); }

    /// <summary>
    /// Use with a Using statement, while in the using you have a write lock
    /// </summary>
    /// 
    public WriterLock WriteLock() { return new WriterLock(_lockHost); }

    /// <summary>
    /// Use with a Using statement, while in the using you have a upgradable lock
    /// </summary>
    public UpgradableLock UpgradeLock() { return new UpgradableLock(_lockHost); }

    /// <summary>
    /// Syntax helper for the ReaderWriterLockSlim
    /// </summary>
    public class ReaderLock : IDisposable
    {
      /// <summary>
      /// Reader Lock
      /// </summary>
      /// The ReaderWriterLockSlim
      public ReaderLock(ReaderWriterLockSlim host)
      {
        _lockHost = host;
        _lockHost.EnterReadLock();
      }

      private readonly ReaderWriterLockSlim _lockHost = new ReaderWriterLockSlim();

      /// <summary>
      /// IDisposable implementation
      /// </summary>
      public void Dispose() { _lockHost.ExitReadLock(); }
    }

    /// <summary>
    /// Syntax helper for the ReaderWriterLockSlim
    /// </summary>
    public class WriterLock : IDisposable
    {
      /// <summary>
      /// Writer lock
      /// </summary>
      /// The ReaderWriterLockSlim
      public WriterLock(ReaderWriterLockSlim host)
      {
        _lockHost = host;
        _lockHost.EnterWriteLock();
      }

      private readonly ReaderWriterLockSlim _lockHost = new ReaderWriterLockSlim();

      /// <summary>
      /// IDisposable implementation
      /// </summary>
      public void Dispose() { _lockHost.ExitWriteLock(); }
    }

    /// <summary>
    /// Syntax helper for the ReaderWriterLockSlim
    /// </summary>
    public class UpgradableLock : IDisposable
    {
      /// <summary>
      /// Creates an upgradable list
      /// </summary>
      /// The ReaderWriterLockSlim
      public UpgradableLock(ReaderWriterLockSlim host)
      {
        _lockHost = host;
        _lockHost.EnterUpgradeableReadLock();
      }

      private readonly ReaderWriterLockSlim _lockHost = new ReaderWriterLockSlim();
      private bool _isUpgraded;

      /// <summary>
      /// Upgrade the lock
      /// </summary>
      public void Upgrade()
      {
        if (_isUpgraded) return;

        _lockHost.EnterWriteLock();
        _isUpgraded = true;
      }

      /// <summary>
      /// IDisposable implementation
      /// </summary>
      public void Dispose()
      {
        if (_isUpgraded) _lockHost.ExitWriteLock();

        _lockHost.ExitUpgradeableReadLock();
      }
    }
  }
}