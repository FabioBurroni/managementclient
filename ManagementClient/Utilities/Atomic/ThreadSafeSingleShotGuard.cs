﻿using System.Threading;

namespace Utilities.Atomic
{
  /// <summary>
  /// Thread safe enter once into a code block:
  /// the first call to CheckAndSetFirstCall returns always true,
  /// all subsequent call return false.
  /// </summary>
  public class ThreadSafeSingleShotGuard
  {
    private const int NotCalled = 0;
    private const int Called = 1;
    private int _state = NotCalled;

    /// <summary>
    /// Explicit call to check and set if this is the first call
    /// </summary>
    public bool CheckAndSetFirstCall
    {
      get
      {
        return Interlocked.Exchange(ref _state, Called) == NotCalled;
      }
    }
  }
}