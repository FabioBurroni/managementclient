﻿using System.Threading;

namespace Utilities.Atomic
{
  /// <summary>
  /// Provides non-blocking, thread-safe access to a int value.
  /// </summary>
  public class AtomicInt
  {
    #region Member Variables

    private int _currentValue;

    #endregion

    #region Constructor

    public AtomicInt()
      : this(0)
    { }

    public AtomicInt(int initialValue)
    {
      _currentValue = initialValue;
    }

    #endregion

    #region Public Properties and Methods

    public int Value
    {
      get
      {
        return Interlocked.CompareExchange(ref _currentValue, 0, 0);
      }
    }

    /// <summary>
    /// Sets the boolean value.
    /// </summary>
    /// <param name="newValue"></param>
    /// <returns>The original value.</returns>
    public int SetValue(int newValue)
    {
      return Interlocked.Exchange(ref _currentValue, newValue);
    }

    /// <summary>
    /// Compares with expected value and if same, assigns the new value.
    /// </summary>
    /// <param name="expectedValue"></param>
    /// <param name="newValue"></param>
    /// <returns>True if able to compare and set, otherwise false.</returns>
    public bool CompareAndSet(int expectedValue, int newValue)
    {
      return Interlocked.CompareExchange(ref _currentValue, newValue, expectedValue) == expectedValue;
    }

    public int Increment()
    {
      return Interlocked.Increment(ref _currentValue);
    }

    public int Decrement()
    {
      return Interlocked.Decrement(ref _currentValue);
    }

    #endregion
  }
}