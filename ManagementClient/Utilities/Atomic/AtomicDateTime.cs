﻿using System;
using System.Threading;

namespace Utilities.Atomic
{
  /// <summary>
  /// Provides non-blocking, thread-safe access to a boolean valueB.
  /// </summary>
  public class AtomicDateTime
  {
    #region Member Variables

    private long _currentValue;

    #endregion

    #region Constructor

    public AtomicDateTime()
      : this(DateTime.Now)
    { }

    public AtomicDateTime(DateTime initialValue)
    {
      _currentValue = DateTimeToBinary(initialValue);
    }

    #endregion

    #region Private Methods

    private static long DateTimeToBinary(DateTime value)
    {
      return value.ToBinary();
    }

    private static DateTime BinaryToDateTime(long value)
    {
      return DateTime.FromBinary(value);
    }

    #endregion

    #region Public Properties and Methods

    public DateTime Value
    {
      get
      {
        return BinaryToDateTime(Interlocked.CompareExchange(ref _currentValue, 0, 0));
      }
    }

    /// <summary>
    /// Sets the boolean value.
    /// </summary>
    /// <param name="newValue"></param>
    /// <returns>The original value.</returns>
    public DateTime SetValue(DateTime newValue)
    {
      return BinaryToDateTime(Interlocked.Exchange(ref _currentValue, DateTimeToBinary(newValue)));
    }

    /// <summary>
    /// Compares with expected value and if same, assigns the new value.
    /// </summary>
    /// <param name="expectedValue"></param>
    /// <param name="newValue"></param>
    /// <returns>True if able to compare and set, otherwise false.</returns>
    public bool CompareAndSet(DateTime expectedValue, DateTime newValue)
    {
      long expectedVal = DateTimeToBinary(expectedValue);
      long newVal = DateTimeToBinary(newValue);
      return Interlocked.CompareExchange(ref _currentValue, newVal, expectedVal) == expectedVal;
    }

    #endregion

  }
}