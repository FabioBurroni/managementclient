﻿using System.Threading;

namespace Utilities.Atomic
{
  /// <summary>
  /// Provides non-blocking, thread-safe access to a boolean valueB.
  /// </summary>
  public class AtomicNullableBoolean
  {
    #region Member Variables

    private const int VALUE_TRUE = 1;
    private const int VALUE_FALSE = 0;
    private const int VALUE_NULL = -1;

    private int _currentValue;

    #endregion

    #region Constructor

    public AtomicNullableBoolean(bool? initialValue)
    {
      _currentValue = NullableBoolToInt(initialValue);
    }

    #endregion

    #region Private Methods

    private static int NullableBoolToInt(bool? value)
    {
      if (!value.HasValue)
        return VALUE_NULL;

      return value.Value ? VALUE_TRUE : VALUE_FALSE;
    }

    private static bool? IntToNullableBool(int value)
    {
      if (value == VALUE_NULL)
        return null;

      return value == VALUE_TRUE;
    }

    #endregion

    #region Public Properties and Methods

    public bool? Value
    {
      get
      {
        return IntToNullableBool(Interlocked.Add(ref _currentValue, 0));
      }
    }

    /// <summary>
    /// Sets the boolean value.
    /// </summary>
    /// <param name="newValue"></param>
    /// <returns>The original value.</returns>
    public bool? SetValue(bool? newValue)
    {
      return IntToNullableBool(Interlocked.Exchange(ref _currentValue, NullableBoolToInt(newValue)));
    }

    /// <summary>
    /// Compares with expected value and if same, assigns the new value.
    /// </summary>
    /// <param name="expectedValue"></param>
    /// <param name="newValue"></param>
    /// <returns>True if able to compare and set, otherwise false.</returns>
    public bool CompareAndSet(bool? expectedValue, bool? newValue)
    {
      int expectedVal = NullableBoolToInt(expectedValue);
      int newVal = NullableBoolToInt(newValue);
      return Interlocked.CompareExchange(ref _currentValue, newVal, expectedVal) == expectedVal;
    }

    #endregion

  }
}