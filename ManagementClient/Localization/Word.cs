﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Localization
{
  public class Word: INotifyPropertyChanged
  {
    public event PropertyChangedEventHandler PropertyChanged;
    protected void NotifyPropertyChanged(string propertyName = "")
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }


    public Word(string key, string defaultVal, string localized)
    {
      _Key = key;
      _DefaultVal = defaultVal;
      _Localized = localized;
    }

    private string _Key;
    public string Key
    {
      get { return _Key; }
      set
      {
        if (value != _Key)
        {
          _Key = value;
        }
      }
    }

    private string _DefaultVal;
    public string DefaultVal
    {
      get { return _DefaultVal; }
      set
      {
        if (value != _DefaultVal)
        {
          _DefaultVal = value;
        }
      }
    }


    private string _Localized;
    public string Localized
    {
      get { return _Localized; }
      set
      {
        if (value != _Localized)
        {
          _Localized = value;
          NotifyPropertyChanged();
        }
      }
    }
      

  }
}
