﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using CassioliLangTranslator;
using Utilities;
using Utilities.Extensions;

namespace Localization
{
  public static class Localize
  {
    #region Field

    private static readonly CultureManager CultureManager = CultureManager.Instance;

    private static readonly string ApplicationPath = AppDomain.CurrentDomain.BaseDirectory;
    private const string VocabularyFolder = "Vocabulary";

    private const string DefaulVocabulary = "vocabulary.xml";
    private const string AuthenticationVocabulary = "vocabulary_authentication.xml";
    private const string AlarmServiceVocabulary = "vocabulary_alarm_service.xml";
    private const string DefaultVocabularyResult = "vocabulary_result.xml";

    private static readonly string CustomVocabulary = string.Empty;
    private static readonly string CustomAlarmServiceVocabulary = string.Empty;
    private static readonly string CustomVocabularyResult = string.Empty;

    private static readonly string DefaultVocabularyPath;
    private static readonly string AuthenticationVocabularyPath;
    private static readonly string AlarmServiceVocabularyPath;
    private static readonly string DefaultVocabularyResultPath;

    private static string CustomVocabularyPath = "";
    private static string CustomAlarmServiceVocabularyPath = "";
    private static string CustomVocabularyResultPath = "";

    private static readonly LangTranslatorManager DefaultTranslator;
    private static readonly LangTranslatorManager AuthenticationTranslator;
    private static readonly LangTranslatorManager AlarmServiceTranslator;
    private static readonly LangTranslatorManager DefaultResultTranslator;

    private static readonly LangTranslatorManager CustomTranslator;
    private static readonly LangTranslatorManager CustomAlarmServiceTranslator;
    private static readonly LangTranslatorManager CustomResultTranslator;
    #endregion

    #region Public Properties
    public static string VocabularyFileName => !string.IsNullOrEmpty(DefaulVocabulary)? DefaulVocabulary : String.Empty;
    public static string VocabularyFilePath => !string.IsNullOrEmpty(DefaultVocabularyPath) ? DefaultVocabularyPath : String.Empty;
    public static string VocabularyAlarmFileName => !string.IsNullOrEmpty(AlarmServiceVocabulary) ? AlarmServiceVocabulary : String.Empty;
    public static string VocabularyAlarmFilePath => !string.IsNullOrEmpty(AlarmServiceVocabularyPath) ? AlarmServiceVocabularyPath : String.Empty;
    public static string VocabularyAuthenticationFileName => !string.IsNullOrEmpty(AuthenticationVocabulary) ? AuthenticationVocabulary : String.Empty;
    public static string VocabularyAuthenticationFilePath => !string.IsNullOrEmpty(AuthenticationVocabularyPath) ? AuthenticationVocabularyPath : String.Empty;
    public static string VocabularyResultFileName => !string.IsNullOrEmpty(DefaultVocabularyResult) ? DefaultVocabularyResult : String.Empty;
    public static string VocabularyResultFilePath => !string.IsNullOrEmpty(DefaultVocabularyResultPath) ? DefaultVocabularyResultPath : String.Empty;


    public static string CustomVocabularyFileName => !string.IsNullOrEmpty(CustomVocabulary) ? CustomVocabulary : String.Empty;
    public static string CustomVocabularyFilePath => !string.IsNullOrEmpty(CustomVocabularyPath) ? CustomVocabularyPath : String.Empty;
    public static string CustomVocabularyAlarmFileName => !string.IsNullOrEmpty(CustomAlarmServiceVocabulary) ? CustomAlarmServiceVocabulary : String.Empty;
    public static string CustomVocabularyAlarmFilePath => !string.IsNullOrEmpty(CustomAlarmServiceVocabularyPath) ? CustomAlarmServiceVocabularyPath : String.Empty;
    public static string CustomVocabularyResultFileName => !string.IsNullOrEmpty(CustomVocabularyResult) ? CustomVocabularyResult : String.Empty;
    public static string CustomVocabularyResultFilePath => !string.IsNullOrEmpty(CustomVocabularyResultPath) ? CustomVocabularyResultPath : String.Empty;
    #endregion

    #region Constructor

    static Localize()
    {
      //Inizializza i path dei vocabolario. Il file vocabulary.xml e derivati(autenticazione, allarmi ecc) si possono trovare o nello stesso path
      //dell'eseguibile oppure nella cartella Vocabulary. In caso di collisione, si spara eccezione.
      string project = Configuration.ConfigurationManager.Parameters.AuthenticationSetting.Customer.Project;

      //controllo se il vocabolario di default esiste, si può partire anche senza
      DefaultVocabularyPath = InitializeVocabularyPath(ApplicationPath, VocabularyFolder, DefaulVocabulary);
      if (!string.IsNullOrEmpty(DefaultVocabularyPath))
        DefaultTranslator = new LangTranslatorManager(DefaultVocabularyPath, 2);

      // custom vocabulary
      if (!string.IsNullOrEmpty(project))
      {
        try
        {
          CustomVocabulary = project + "_" + DefaulVocabulary;
          CustomVocabularyPath = InitializeVocabularyPath(ApplicationPath, VocabularyFolder, CustomVocabulary);
          if (!string.IsNullOrEmpty(CustomVocabularyPath))
            CustomTranslator = new LangTranslatorManager(CustomVocabularyPath, 2);
        }
        catch (Exception ex)
        {
          string exc = ex.ToString();
        }
      }

      //controllo se il vocabolario dell'autenticazione esiste, si può partire anche senza
      AuthenticationVocabularyPath = InitializeVocabularyPath(ApplicationPath, VocabularyFolder, AuthenticationVocabulary);
      if (!string.IsNullOrEmpty(AuthenticationVocabularyPath))
        AuthenticationTranslator = new LangTranslatorManager(AuthenticationVocabularyPath, 2);


      //controllo se il vocabolario degli allarmi e dei servizi esiste, si può partire anche senza
      AlarmServiceVocabularyPath = InitializeVocabularyPath(ApplicationPath, VocabularyFolder, AlarmServiceVocabulary);
      if (!string.IsNullOrEmpty(AlarmServiceVocabularyPath))
        AlarmServiceTranslator = new LangTranslatorManager(AlarmServiceVocabularyPath, 2);

      // custom alarmservice
      if (!string.IsNullOrEmpty(project))
      {
        try
        {
          CustomAlarmServiceVocabulary = project + "_" + AlarmServiceVocabulary;
          CustomAlarmServiceVocabularyPath = InitializeVocabularyPath(ApplicationPath, VocabularyFolder, CustomAlarmServiceVocabulary);
          if (!string.IsNullOrEmpty(CustomAlarmServiceVocabularyPath))
            CustomAlarmServiceTranslator = new LangTranslatorManager(CustomAlarmServiceVocabularyPath, 2);
        }
        catch (Exception ex)
        {
          string exc = ex.ToString();
        }
      }

      //controllo se il vocabolario di default dei result esiste, si può partire anche senza
      DefaultVocabularyResultPath = InitializeVocabularyPath(ApplicationPath, VocabularyFolder, DefaultVocabularyResult);
      if (!string.IsNullOrEmpty(DefaultVocabularyResultPath))
        DefaultResultTranslator = new LangTranslatorManager(DefaultVocabularyResultPath, 2);

      // custom vocabulary
      if (!string.IsNullOrEmpty(project))
      {
        try
        {
          CustomVocabularyResult = project + "_" + DefaultVocabularyResult;
          CustomVocabularyResultPath = InitializeVocabularyPath(ApplicationPath, VocabularyFolder, CustomVocabularyResult);
          if (!string.IsNullOrEmpty(CustomVocabularyResultPath))
            CustomResultTranslator = new LangTranslatorManager(CustomVocabularyResultPath, 2);
        }
        catch (Exception ex)
        {
          string exc = ex.ToString();
        }
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Restituisce il corretto file path per il vocabolario.
    /// Se è definito sia quello nella cartella di applicazione sia quello nella cartella di vocabolario, si spara eccezione
    /// </summary>
    private static string InitializeVocabularyPath(string applicationPath, string vocabularyFolder, string vocabularyFileName)
    {
      var vocabularyPathWithoutFolder = CombinePaths(applicationPath, vocabularyFileName); //vocabolario nel path di applicazione quindi senza cartella
      var vocabularyPathWithFolder = CombinePaths(applicationPath, vocabularyFolder, vocabularyFileName); //vocabolario nella cartella di vocabolario

      var existsVocabularyPathWithoutFolder = File.Exists(vocabularyPathWithoutFolder);
      var existsVocabularyPathWithFolder = File.Exists(vocabularyPathWithFolder);

      if (existsVocabularyPathWithoutFolder && existsVocabularyPathWithFolder)
        throw new Exception(string.Format("File {0} is defined in multiple paths '{1}' and '{2}'. Please remove one.", vocabularyFileName, vocabularyPathWithoutFolder, vocabularyPathWithFolder));

      //if (!existsVocabularyPathWithoutFolder && !existsVocabularyPathWithFolder)
      //  throw new Exception(string.Format("File {0} is not defined in any path. Please add it.", vocabularyFileName));

      if (existsVocabularyPathWithFolder)
        return vocabularyPathWithFolder;

      if (existsVocabularyPathWithoutFolder)
        return vocabularyPathWithoutFolder;

      return string.Empty;
    }

    private static string CombinePaths(params string[] paths)
    {
      if (paths == null)
      {
        throw new ArgumentNullException("paths");
      }

      return paths.Aggregate(Path.Combine);
    }

    #endregion

    #region LOCALIZE
   
    #region vocabulary default
    /// <summary>
    /// Effettua la traduzione di una stringa in base alla cultura corrente cercando nel vocabolario di default
    /// </summary>
    /// <param name="stringToLocalize">String da localizzare</param>
    /// <returns>Stringa tradotta o la stringa stessa se non c'è corrispondenza</returns>
    public static string LocalizeDefaultString(this string stringToLocalize)
    {
      return LocalizeDefaultString(stringToLocalize.ToUpperInvariant(), CultureManager.Culture);
    }

    /// <summary>
    /// Effettua la traduzione di una stringa in base alla cultura corrente cercando nel vocabolario di default
    /// </summary>
    /// <param name="stringToLocalize">String da localizzare</param>
    /// <param name="culture">Cultura corrente</param>
    /// <returns>Stringa tradotta o la stringa stessa se non c'è corrispondenza</returns>
    public static string LocalizeDefaultString(this string stringToLocalize, string culture)
    {
      string localizedString = LocalizeString(stringToLocalize, culture, DefaultTranslator);

      if (string.IsNullOrEmpty(localizedString) && CustomTranslator != null)
      {
        localizedString = LocalizeString(stringToLocalize, culture, CustomTranslator);
      }

      if (string.IsNullOrEmpty(localizedString))
      {
        Log(stringToLocalize, VocabolaryToUse.DEFAULT);
      }

      return !string.IsNullOrEmpty(localizedString) ? localizedString : stringToLocalize;
    }
    #endregion

    #region vocabulary CUSTOM
    public static string LocalizeCustomString(this string stringToLocalize)
    {
      return LocalizeCustomString(stringToLocalize.ToUpperInvariant(), CultureManager.Culture);
    }
    public static string LocalizeCustomString(this string stringToLocalize, string culture)
    {
      string localizedString = "";

      if (CustomTranslator != null)
      {
        localizedString = LocalizeString(stringToLocalize, culture, CustomTranslator);
      }

      if (string.IsNullOrEmpty(localizedString))
      {
        Log(stringToLocalize, VocabolaryToUse.CUSTOM);
      }

      return !string.IsNullOrEmpty(localizedString) ? localizedString : stringToLocalize;
    }
    #endregion

    #region authentication
    /// <summary>
    /// Effettua la traduzione di una stringa in base alla cultura corrente cercando nel vocabolario di autenticazione
    /// </summary>
    /// <param name="stringToLocalize">String da localizzare</param>
    /// <returns>Stringa tradotta o la stringa stessa se non c'è corrispondenza</returns>
    public static string LocalizeAuthenticationString(this string stringToLocalize)
    {
      return LocalizeAuthenticationString(stringToLocalize, CultureManager.Culture);
    }

    /// <summary>
    /// Effettua la traduzione di una stringa in base alla cultura corrente cercando nel vocabolario di autenticazione
    /// </summary>
    /// <param name="stringToLocalize">String da localizzare</param>
    /// <param name="culture">Cultura corrente</param>
    /// <returns>Stringa tradotta o la stringa stessa se non c'è corrispondenza</returns>
    public static string LocalizeAuthenticationString(this string stringToLocalize, string culture)
    {
      string localizedString = LocalizeString(stringToLocalize, culture, AuthenticationTranslator);
      if (string.IsNullOrEmpty(localizedString))
      {
        Log(stringToLocalize, VocabolaryToUse.AUTHENTICATION);
      }

      return !string.IsNullOrEmpty(localizedString) ? localizedString : stringToLocalize;
    }
    #endregion

    #region Alarm default

    /// <summary>
    /// Effettua la traduzione di una stringa in base alla cultura corrente cercando nel vocabolario degli allarmi e servizi
    /// </summary>
    /// <param name="stringToLocalize">String da localizzare</param>
    /// <returns>Stringa tradotta o la stringa stessa se non c'è corrispondenza</returns>
    public static string LocalizeAlarmServiceString(this string stringToLocalize)
    {
      return LocalizeAlarmServiceString(stringToLocalize, CultureManager.Culture);
    }

    /// <summary>
    /// Effettua la traduzione di una stringa in base alla cultura corrente cercando nel vocabolario degli allarmi e servizi
    /// </summary>
    /// <param name="stringToLocalize">String da localizzare</param>
    /// <param name="culture">Cultura corrente</param>
    /// <returns>Stringa tradotta o la stringa stessa se non c'è corrispondenza</returns>
    public static string LocalizeAlarmServiceString(this string stringToLocalize, string culture)
    {
      //return LocalizeString(stringToLocalize, culture, AlarmServiceTranslator,VocabolaryToUse.ALARM);
      string localizedString = LocalizeString(stringToLocalize, culture, AlarmServiceTranslator);
      if (string.IsNullOrEmpty(localizedString) && CustomAlarmServiceTranslator != null)
      {
        localizedString = LocalizeString(stringToLocalize, culture, CustomAlarmServiceTranslator);
      }

      if (string.IsNullOrEmpty(localizedString))
      {
        Log(stringToLocalize, VocabolaryToUse.DEFAULT);
      }

      return !string.IsNullOrEmpty(localizedString) ? localizedString : stringToLocalize;
    }

    #endregion

    #region CUSTOM ALARMS
    public static string LocalizeCustomAlarmString(this string stringToLocalize)
    {
      return LocalizeCustomAlarmString(stringToLocalize, CultureManager.Culture);
    }

    public static string LocalizeCustomAlarmString(this string stringToLocalize, string culture)
    {
      string localizedString = "";

      if (CustomAlarmServiceTranslator != null)
      {
        localizedString = LocalizeString(stringToLocalize, culture, CustomAlarmServiceTranslator);
      }

      if (string.IsNullOrEmpty(localizedString))
      {
        Log(stringToLocalize, VocabolaryToUse.CUSTOM_ALARM);
      }

      return !string.IsNullOrEmpty(localizedString) ? localizedString : stringToLocalize;
    }
    #endregion

    #region Result
    /// <summary>
    /// Effettua la traduzione di una stringa in base alla cultura corrente cercando nel vocabolario di default dei result
    /// </summary>
    /// <param name="stringToLocalize">String da localizzare</param>
    /// <returns>Stringa tradotta o la stringa stessa se non c'è corrispondenza</returns>
    public static string LocalizeDefaultResultString(this string stringToLocalize)
    {
      return LocalizeDefaultResultString(stringToLocalize.ToUpperInvariant(), CultureManager.Culture);
    }

    /// <summary>
    /// Effettua la traduzione di una stringa in base alla cultura corrente cercando nel vocabolario di default
    /// </summary>
    /// <param name="stringToLocalize">String da localizzare</param>
    /// <param name="culture">Cultura corrente</param>
    /// <returns>Stringa tradotta o la stringa stessa se non c'è corrispondenza</returns>
    public static string LocalizeDefaultResultString(this string stringToLocalize, string culture)
    {
      string localizedString = LocalizeString(stringToLocalize, culture, DefaultResultTranslator);

      if (string.IsNullOrEmpty(localizedString) && CustomResultTranslator != null)
      {
        localizedString = LocalizeString(stringToLocalize, culture, CustomResultTranslator);
      }

      if (string.IsNullOrEmpty(localizedString))
      {
        Log(stringToLocalize, VocabolaryToUse.DEFAULT_RESULT);
      }

      return !string.IsNullOrEmpty(localizedString) ? localizedString : stringToLocalize;
    }
    #endregion

    private static List<string> _cacheNotFound = new List<string>();
    /// <summary>
    /// Effettua la traduzione di una stringa in base alla cultura corrente
    /// </summary>
    /// <param name="stringToLocalize">String da localizzare</param>
    /// <param name="culture">Cultura corrente</param>
    /// <param name="translator">Istanza del traduttore</param>
    /// <returns>Se è stata trovata la traduzione, restituisce la stringa localizzata, altrimenti la stringa originale</returns>
    private static string LocalizeString(this string stringToLocalize, string culture, LangTranslatorManager translator)
    {
      if (DesignTimeHelper.IsInDesignMode)
        return stringToLocalize;

      if (stringToLocalize == null)
        throw new ArgumentNullException(nameof(stringToLocalize));

      if (string.IsNullOrEmpty(culture))
        throw new ArgumentNullException(nameof(culture));

      if (translator == null)
        return stringToLocalize;


      if (stringToLocalize.Equals(string.Empty))
        return stringToLocalize;

      var localizedString = translator.TranslateItem(stringToLocalize, culture);

      return localizedString;

    }

    #endregion

    #region Log

    private static void Log(string stringToLocalize, VocabolaryToUse vocabolary)
    {
        if (!_cacheNotFound.Contains(stringToLocalize))
        {
          _cacheNotFound.Add(stringToLocalize);
          string clrn = "\r\n";
          string msg = clrn;
          msg += "<item id=\"0\">" + clrn;

            msg += $"<default><![CDATA[{stringToLocalize}]]></default>" + clrn;

          foreach (var lang in Configuration.ConfigurationManager.Parameters.Localization.Languages.ToList())
          {
            msg += $"<{lang.Name}><![CDATA[{stringToLocalize}]]></{lang.Name}>" + clrn;
          }
          msg += "</item>" + clrn;

          switch (vocabolary)
          {
            case VocabolaryToUse.DEFAULT:
              Logging.ToFile("vocabulary", msg);
              break;
            case VocabolaryToUse.CUSTOM:
              Logging.ToFile("vocabulary_custom", msg);
              break;
            case VocabolaryToUse.AUTHENTICATION:
              Logging.ToFile("vocabulary_auth", msg);
              break;
            case VocabolaryToUse.ALARM:
              Logging.ToFile("vocabulary_alarm", msg);
              break;
            case VocabolaryToUse.CUSTOM_ALARM:
              Logging.ToFile("vocabulary_custom_alarm", msg);
              break;
          case VocabolaryToUse.DEFAULT_RESULT:
            Logging.ToFile("vocabulary_result", msg);
            break;
          case VocabolaryToUse.CUSTOM_RESULT:
            Logging.ToFile("vocabulary_custom_result", msg);
            break;
          default:
              break;
          }
        }
      }

    #endregion


    #region Languages

    /// <summary>
    /// Restituisce la lista delle lingue disponibili per la traduzione nel vocabolario di default
    /// </summary>
    /// <returns></returns>
    public static IList<string> GetLanguagesForDefaultVocabulary()
    {
      return GetLanguagesForVocabulary(DefaultTranslator);
    }

    /*
    /// <summary>
    /// Restituisce la lista delle lingue disponibili per la traduzione nel vocabolario degli allarmi e servizi
    /// </summary>
    /// <returns></returns>
    public static IList<string> GetLanguagesForAlarmServiceVocabulary()
    {
      return GetLanguagesForVocabulary(AlarmServiceTranslator);
    }
    */

    private static IList<string> GetLanguagesForVocabulary(LangTranslatorManager translator)
    {
      if (translator == null)
        return new Collection<string>();

      var langs = translator.GetAllLanguagesInVocuabulary();

      if (langs == null)
        return new Collection<string>();

      return langs.Select(l => l.name).ToArray();
    }

    #endregion
  }

  public enum VocabolaryToUse
  {
    DEFAULT,
    AUTHENTICATION,
    ALARM,
    CUSTOM,
    CUSTOM_ALARM,
    DEFAULT_RESULT,
    CUSTOM_RESULT
  }
}