﻿using System;
using System.Globalization;
using System.Linq;
using Utilities.Events;
using Utilities.Exceptions;
using Utilities.Extensions;
using WPFLocalizeExtension.Engine;

namespace Localization
{
	public sealed class CultureManager
	{
		#region Event

		public event CultureChangedHandler CultureChanged;

		#endregion

		#region Field

		/// <summary>
		/// Attuale cultura impostata
		/// Notare che sono costretto a controllare se sono in design mode altrimenti qualche form non viene mostrata
		/// </summary>
		private string _culture = "";

		/// <summary>
		/// Istanza del gestore della localizzazione
		/// </summary>
		private readonly LocalizeDictionary _localizeDictionary = LocalizeDictionary.Instance;

		#endregion

		#region Property

		public string Culture
		{
			get { return _culture; }
			set
			{
				//controllo che il nuovo valore sia diverso da nullo
				if (string.IsNullOrEmpty(value))
					throw new ArgumentNullOrEmptyException(nameof(value));

				//controllo che la nuova lingua sia definita
				if (!Localize.GetLanguagesForDefaultVocabulary().Any(l => l.EqualsIgnoreCase(value)))
					throw new Exception($"Language {value} not defined in vocabulary");

				if (_culture.EqualsIgnoreCase(value))
					return;

				var oldCulture = _culture;
				var newCulture = value;

				_culture = value;
				_localizeDictionary.Culture = CultureInfo.GetCultureInfo(value);

				CultureChanged?.Invoke(null, new CultureChangedEventArgs(oldCulture, newCulture));
			}
		}

		public CultureInfo CultureInfo => new CultureInfo(_culture);

		#endregion

		#region Singleton

		private CultureManager()
		{
		}

		public static CultureManager Instance => CultureManagerNested.Instance;

		private class CultureManagerNested
		{
			// Explicit static constructor to tell C# compiler
			// not to mark type as beforefieldinit
			static CultureManagerNested()
			{
			}

			internal static readonly CultureManager Instance = new CultureManager();
		}

		#endregion
	}
}