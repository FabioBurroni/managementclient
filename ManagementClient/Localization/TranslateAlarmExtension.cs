﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace Localization
{
	public class TranslateAlarmExtension : MarkupExtension
	{
		public string PostAppend { get; set; }
		public string PreAppend { get; set; }

		private string _value="";
		public TranslateAlarmExtension(string value)
		{
			_value = value;
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			//return Localization.Localize.LocalizeDefaultString(_value);
			if (Utilities.Extensions.DesignTimeHelper.IsInDesignMode)
				return _value;
			
			//return (PreAppend !=null ? PreAppend : "") + Localization.Localize.LocalizeDefaultString(_value.Replace("_", " ")) + (PostAppend != null ? PostAppend : "");
			return (PreAppend !=null ? PreAppend : "") + Localization.Localize.LocalizeAlarmServiceString(_value) + (PostAppend != null ? PostAppend : "");
    }
	}
}
