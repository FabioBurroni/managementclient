﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Configuration
{
    public class CassioliVirtualKeyboard
    {
        public bool IsEnabled { get; private set; }

        public CassioliVirtualKeyboard(bool isEnabled)
        {
            IsEnabled = isEnabled;
        }
    }
}
