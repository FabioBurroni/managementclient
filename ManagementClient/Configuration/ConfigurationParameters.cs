﻿using Configuration.Custom.C150611;

namespace Configuration
{
  public class ConfigurationParameters : BaseConfigurationParameters
  {
    //private static ConfigurationParameters _instance = null;

    public ConfigurationParameters()
    {
    }


        #region Cassioli virtual keyboard

        /// <summary>
        /// decreta se verrà utilizzata la tastiera vistuale di Windows o quella cassioli
        /// </summary>
        public CassioliVirtualKeyboard CassioliVirtualKeyboard { get; internal set; }

        #endregion

        #region PARAMETER

        #region Database Connection

        /// <summary>
        /// Impostazioni per connessione al database
        /// </summary>
        public DatabaseConnection DatabaseConnection { get; internal set; }

    #endregion

    #region WebServer

    /// <summary>
    /// Impostazioni WebServer
    /// </summary>
    public WebServer WebServer { get; internal set; }

    #endregion

    #endregion
  }
}