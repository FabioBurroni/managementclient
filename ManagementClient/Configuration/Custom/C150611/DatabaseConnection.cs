﻿using System.Data.SqlClient;

namespace Configuration.Custom.C150611
{
  public class DatabaseConnection
  {
    private const string CryptKey = "cmcSecrets";

    public DatabaseConnection(string connectionName, string dataSource, string catalog, string userId, string password)
    {
      ConnectionName = connectionName;
      DataSource = dataSource;
      Catalog = catalog;
      UserID = userId;
      Password = password;

      var builder = new SqlConnectionStringBuilder
      {
        DataSource = dataSource,
        InitialCatalog = catalog,
        UserID = Utilities.DataCrypter.DecryptString(userId, CryptKey),
        Password = Utilities.DataCrypter.DecryptString(password, CryptKey),
      };

      ConnectionString = builder.ToString();
    }

    public string ConnectionName { get; }
    public string DataSource { get; }
    public string Catalog { get; }
    public string UserID { get; }
    public string Password { get; }

    /// <summary>
    /// Restituisce la stringa di connessione al database formatta in modo opportuno
    /// </summary>
    public string ConnectionString { get; }
  }
}