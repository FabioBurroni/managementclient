﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Configuration.Custom.C150611
{
  public class WebServer
  {
    public WebServer(string url)
    {
      Url = url;
    }

    public string Url { get; }
  }
}