﻿using System.Collections.Generic;
using WindowsInput;

namespace Configuration.Settings.UI
{
  public class KeyboardLayout
  {
    public string Name { get; set; }

    public List<KeyboardRow> Rows { get; set; }
  }

  public class KeyboardRow
  {
    public int Id { get; set; }

    public List<KeyboardKey> Keys { get; set; }

    public bool SpecialKey { get; set; }

    public bool Numpad { get; set; }
  }

  public class KeyboardKey
  {
    public VirtualKeyCode Code { get; set; }

    public VirtualKeyCode ModifierCode { get; set; }

    public string KeyDisplay0 { get; set; }

    public string KeyDisplay1 { get; set; }

    public double Length { get; set; }
  }
}