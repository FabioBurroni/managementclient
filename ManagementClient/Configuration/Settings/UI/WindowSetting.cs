﻿namespace Configuration.Settings.UI
{
  public class WindowSetting
  {
    public int Width { get; internal set; } = 1400;

    public int Height { get; internal set; } = 900;

    public bool Resizable { get; internal set; }

    public bool CanMinimize { get; internal set; }

    public bool CanMaximize { get; internal set; }

    public bool Maximized { get; internal set; }
  }
}