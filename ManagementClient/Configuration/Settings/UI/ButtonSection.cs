﻿using System;

namespace Configuration.Settings.UI
{
	public class ButtonSection
	{
		private ControlSection _control;

		public string Name { get; }

		public string Text { get; }

		public int Position { get; }

		public string Action { get; }

		public Uri ImageUri { get; }

		public string Icon { get; }

		public string IconColor { get; }

		public ControlSection Control
		{
			get { return _control; }
			internal set
			{
				if (_control != null)
					throw new Exception($"{nameof(Control)} already setted");

				_control = value;
			}
		}

		public ButtonSection(string name, string text, int position, string action, string imageUri, string icon, string iconColor)
		{
			if (string.IsNullOrEmpty(name))
				throw new ArgumentNullException(nameof(name));

			Name = name;
			Text = text;
			Position = position;
			Action = action ?? string.Empty;
			ImageUri = !string.IsNullOrEmpty(imageUri) ? new Uri(imageUri) : null;
			Icon = !string.IsNullOrEmpty(icon) ? icon : null; ;
			IconColor = !string.IsNullOrEmpty(iconColor)? iconColor :  "Black" ;

		}
	}
}