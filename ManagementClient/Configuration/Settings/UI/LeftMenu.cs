﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Comparer;
using Utilities.Exceptions;

namespace Configuration.Settings.UI
{
  public class LeftMenu
  {
    private readonly SortedList<int, MenuSection> _menuSections = new SortedList<int, MenuSection>(new DescendingComparer<int>());

    public int Count => _menuSections.Count;

    public ControlSection MainControl { get; internal set; }

    internal void Add(MenuSection menuSection)
    {
      if (menuSection == null)
        throw new ArgumentNullException(nameof(menuSection));

      if (_menuSections.ContainsKey(menuSection.Position))
        throw new DuplicateKeyException($"Key {menuSection.Position} already inserted");

      _menuSections.Add(menuSection.Position, menuSection);
    }

    public IList<MenuSection> GetAll()
    {
      return _menuSections.Values.ToArray();
    }
  }
}