﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Exceptions;

namespace Configuration.Settings.UI
{
  public class MenuSection
  {
    //private readonly SortedList<int, ButtonSection> _buttonsSection = new SortedList<int, ButtonSection>(new DescendingComparer<int>());
    private readonly SortedList<int, ButtonSection> _buttonsSection = new SortedList<int, ButtonSection>();

    public MenuSection(string name, string text, int position, string action, string allowedPositions, string icon)
    {
      if (string.IsNullOrEmpty(name))
        throw new ArgumentNullOrEmptyException(nameof(name));

      Name = name;
      Text = text;
      Position = position;
      Action = action ?? string.Empty;

      var allowedPositionsA = !string.IsNullOrEmpty(allowedPositions) ? allowedPositions.Split(';') : null;

      if(allowedPositionsA!=null && allowedPositionsA.Length>0)
        AllowedPositionsL.AddRange(allowedPositionsA);

      Icon = icon;
    }

    public string Name { get; private set; }

    public string Text { get; private set; }

    public int Position { get; private set; }

    public string Action { get; private set; }

    public string Icon { get; private set; }

    public List<string> AllowedPositionsL { get; private set; } = new List<string>();

    public int Count => _buttonsSection.Count;

    public void Add(ButtonSection buttonSection)
    {
      if (buttonSection == null)
        throw new ArgumentNullException(nameof(buttonSection));

      if (_buttonsSection.ContainsKey(buttonSection.Position))
        throw new DuplicateKeyException($"Key {buttonSection.Position} already inserted");

      _buttonsSection.Add(buttonSection.Position, buttonSection);
    }

    public IList<ButtonSection> GetAll()
    {
      return _buttonsSection.Values.ToArray();
    }


    public bool IsPositionAllowed(List<string> configurationPositions)
    {
      if (configurationPositions.Count == 0)
        return true;
      if(AllowedPositionsL.Count==0)
        return true;

      if (AllowedPositionsL.Any(ap => configurationPositions.Any(cp => cp.ToLower() == ap.ToLower())))
        return true;
      return false;
    }

  }
}