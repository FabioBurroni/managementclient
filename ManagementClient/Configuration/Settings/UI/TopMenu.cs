﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Exceptions;

namespace Configuration.Settings.UI
{
  public class TopMenu
  {
    private readonly SortedList<int, MenuSection> _menuSections = new SortedList<int, MenuSection>();

    public int Count => _menuSections.Count;

    public MenuSection Program { get; internal set; }

    internal void Add(MenuSection menuSection)
    {
      if (menuSection == null)
        throw new ArgumentNullException(nameof(menuSection));

      if (_menuSections.ContainsKey(menuSection.Position))
        throw new DuplicateKeyException($"Key {menuSection.Position} already inserted");

      _menuSections.Add(menuSection.Position, menuSection);
    }

    public IList<MenuSection> GetAll()
    {
      return _menuSections.Values.ToArray();
    }
  }
}