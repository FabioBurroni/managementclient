﻿namespace Configuration.Settings.Functionalities
{
  public class Functionality
  {
    private string _name = "";
    public string Name
    {
      get { return _name; }
      set { _name = value; }
    }

    private string _descr = "";
    public string Descr
    {
      get { return _descr; }
      set { _descr = value; }
    }

    private string _ctrl = "";
    public string Ctrl
    {
      get { return _ctrl; }
      set { _ctrl = value; }
    }

    private string _user_profile_enabled = "";
    /// <summary>
    /// Profilo utente abilitato alla funzione.
    /// Se * = tutti.
    /// </summary>
    public string UserProfileEnabled
    {
      get { return _user_profile_enabled; }
      set { _user_profile_enabled = value; }
    }
  }
}
