﻿using System.Collections.Generic;

namespace Configuration.Settings.Functionalities
{
  public class Functionalities
  {
    SortedList<string, Functionality> _functionalities = new SortedList<string, Functionality>();

    public bool Add(Functionality functionality)
    {
      bool ret = false;

      if (!_functionalities.ContainsKey(functionality.Name))
      {
        _functionalities.Add(functionality.Name, functionality);
        return true;
      }
      return ret;
    }

    public Functionality[] GetAll()
    {
      Functionality[] ret = null;
      if (_functionalities.Count > 0)
      {
        ret = new Functionality[_functionalities.Count];
        int i = 0;
        foreach (KeyValuePair<string, Functionality> plKv in _functionalities)
        {
          ret[i] = plKv.Value;
          i++;
        }
      }
      return ret;
    }

    public Functionality this[string code]
    {
      get
      {
        Functionality ret = null;
        if (_functionalities.ContainsKey(code))
          ret = _functionalities[code];
        return ret;
      }
      set
      {
        if (_functionalities.ContainsKey(code))
          _functionalities[code] = value;
        else
          Add(value);
      }
    }



    public int Count
    {
      get
      {
        return _functionalities.Count;
      }

    }
  }
}
