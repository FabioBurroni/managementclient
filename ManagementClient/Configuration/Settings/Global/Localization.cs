﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Extensions;

namespace Configuration.Settings.Global
{
	public class Localization
	{
		#region Fields

		/// <summary>
		/// Lingue supportate
		/// </summary>
		private readonly List<Language> _languages = new List<Language>();

		#endregion

		#region Properties

		/// <summary>
		/// Numero delle lingue configurate
		/// </summary>
		public int LanguageCount => _languages.Count;

		/// <summary>
		/// Lingue configurate
		/// </summary>
		public IList<Language> Languages => _languages.ToArray();

		/// <summary>
		/// Lingua di default
		/// </summary>
		public Language DefaultLanguage
		{
			get
			{
				//recupero la prima lingua con l'attributo default=true, se l'attributo non è stato specificato, prendo la prima che trovo
				return _languages.FirstOrDefault(l => l.IsDefault) ?? _languages.First();
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Aggiunge una lisngua supportae
		/// </summary>
		internal void AddLanguage(Language language)
		{
			if (language == null)
				throw new ArgumentNullException(nameof(language));

			if (_languages.Any(l => l.Name.EqualsIgnoreCase(language.Name)))
				throw new Exception($"Language '{language.Name}' already inserted");

			_languages.Add(language);
		}

		public bool IsLanguageContained(string languageName)
		{
			if (languageName == null)
				throw new ArgumentNullException(nameof(languageName));

			return _languages.Any(l => l.Name.EqualsIgnoreCase(languageName));
		}

		public Language GetLanguage(string languageName)
		{
			if (languageName == null)
				throw new ArgumentNullException(nameof(languageName));

			return _languages.SingleOrDefault(l => l.Name.EqualsIgnoreCase(languageName));
		}

		#endregion
	}
}