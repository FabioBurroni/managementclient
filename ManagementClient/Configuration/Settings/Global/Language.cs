﻿using System;
using System.Globalization;

namespace Configuration.Settings.Global
{
	public class Language
	{
		public string Name { get; }
		public bool IsDefault { get; }
		public Uri ImageUri { get; }
		/// <summary>
		/// Cultura derivante dal nome (es "en-US")
		/// </summary>
		public CultureInfo Culture => new CultureInfo(Name);
		/// <summary>
		/// Cultura derivante dal nome generico (es "en")
		/// </summary>
		public CultureInfo GenericCulture => new CultureInfo(Culture.TwoLetterISOLanguageName);

		public Language(string name, bool isDefault, string imageUri)
		{
			if (name == null)
				throw new ArgumentNullException(nameof(name));
			if (imageUri == null)
				throw new ArgumentNullException(nameof(imageUri));

			Name = name;
			IsDefault = isDefault;
			ImageUri = new Uri(imageUri);
		}

		public override string ToString()
		{
			return Name;
		}
	}
}