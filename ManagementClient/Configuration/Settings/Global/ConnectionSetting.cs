﻿using System;
using Utilities.Exceptions;

namespace Configuration.Settings.Global
{
  public class ConnectionSetting
  {
    #region PROPRIETA'

    public string IpAddress { get; private set; }

    public int PortNumber { get; private set; }

    public int TimeOut { get; private set; }

    public int Version { get; private set; }

    public bool Enabled { get; private set; }

    #endregion

    internal ConnectionSetting(bool enabled)
      : this("0.0.0.0", 0, 0, 0, enabled)
    {
    }

    internal ConnectionSetting(string ipAddress, int portNumber, int timeOut, int version)
      : this(ipAddress, portNumber, timeOut, version, true)
    {
    }

    internal ConnectionSetting(string ipAddress, int portNumber, int timeOut, int version, bool enabled)
    {
      if (string.IsNullOrEmpty(ipAddress))
        throw new ArgumentNullOrEmptyException(nameof(ipAddress));

      if (portNumber < 0 || portNumber > 65535)
        throw new ArgumentOutOfRangeException(nameof(portNumber));

      IpAddress = ipAddress;
      PortNumber = portNumber;
      TimeOut = timeOut;
      Version = version;
      Enabled = enabled;
    }
  }
}