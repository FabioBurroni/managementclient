﻿using System;
using Configuration.Settings.Global.Bcr;

namespace Configuration.Settings.Global
{
	public class BcrSetting
	{
		/// <summary>
		/// Impostazione per BCR con emulazione di tastiera
		/// </summary>
		public EmulatedKeyboardBcrSetting EmulatedKeyboardBcrSetting { get; }

		/// <summary>
		/// Impostazione per BCR seriale
		/// </summary>
		public SerialBcrSetting SerialBcrSetting { get; }

		public BcrSetting(EmulatedKeyboardBcrSetting emulatedKeyboardBcrSetting, SerialBcrSetting serialBcrSetting)
		{
			EmulatedKeyboardBcrSetting = emulatedKeyboardBcrSetting;
			SerialBcrSetting = serialBcrSetting;
		}
	}
}