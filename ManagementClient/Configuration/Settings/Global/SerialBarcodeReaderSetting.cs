﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Configuration.Settings.Global
{
  public class SerialBarcodeReaderSetting
  {
    /// <summary>
    /// Impostazione per l'abilitazione del Bcr
    /// </summary>
    public bool Enabled { get; }

    /// <summary>
    /// Porta seriale
    /// </summary>
    public string Port { get; }

    /// <summary>
    /// Baud Rate
    /// </summary>
    public int BaudRate { get; }

    /// <summary>
    /// Terminatore per il riconoscimento del fine stringa
    /// </summary>
    public string Terminator { get; }

    /// <summary>
    /// Terminatore per il riconoscimento del fine stringa in esadecimale
    /// </summary>
    public IList<string> TerminatorHex { get; }

    public SerialBarcodeReaderSetting(bool enabled, string port, int baudRate, string[] terminatorHex)
    {
      if (!enabled)
        return;

      if (port == null)
        throw new ArgumentNullException(nameof(port));
      if (baudRate <= 0)
        throw new ArgumentOutOfRangeException(nameof(baudRate));

      Enabled = enabled;
      Port = port;
      BaudRate = baudRate;
      TerminatorHex = terminatorHex;
      Terminator = ConvertHexToString(string.Join("", terminatorHex));
    }

    #region Private Methods

    private static string ConvertStringToHex(string asciiString)
    {
      string hexValue = "";
      foreach (char c in asciiString)
      {
        int tmp = c;
        hexValue += $"{Convert.ToUInt32(tmp.ToString()):x2}";
      }
      return hexValue;
    }

    private static string ConvertHexToString(string hexValue)
    {
      string asciiString = "";
      while (hexValue.Length > 0)
      {
        asciiString += Convert.ToChar(Convert.ToUInt32(hexValue.Substring(0, 2), 16)).ToString();
        hexValue = hexValue.Substring(2, hexValue.Length - 2);
      }
      return asciiString;
    }

    #endregion
  }
}