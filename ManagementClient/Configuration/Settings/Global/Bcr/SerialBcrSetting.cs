﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Configuration.Settings.Global.Bcr
{
	public class SerialBcrSetting
	{
		/// <summary>
		/// Impostazione per l'abilitazione del Bcr
		/// </summary>
		public bool Enabled { get; }

		/// <summary>
		/// Porta seriale
		/// </summary>
		public string Port { get; }

		/// <summary>
		/// Baud Rate
		/// </summary>
		public int BaudRate { get; }

		/// <summary>
		/// Terminatore per il riconoscimento del fine stringa
		/// </summary>
		public string Terminator { get; }

		/// <summary>
		/// Terminatore per il riconoscimento del fine stringa in decimale
		/// </summary>
		public IList<string> TerminatorDec { get; }

		public SerialBcrSetting(bool enabled, string port, int baudRate, string[] terminatorDec)
		{
			if (!enabled)
				return;

			if (port == null)
				throw new ArgumentNullException(nameof(port));
			if (baudRate <= 0)
				throw new ArgumentOutOfRangeException(nameof(baudRate));

			Enabled = enabled;
			Port = port;
			BaudRate = baudRate;
			TerminatorDec = terminatorDec;
			Terminator = ConvertDecToString(terminatorDec);
		}

		#region Private Methods

		private static string ConvertDecToString(string[] decValues)
		{
			if (decValues == null)
				return "";

			var asciiValues = decValues.Select(v => (char)Convert.ToInt32(v)).ToArray();

			return new string(asciiValues);
		}

		private static string ConvertStringToHex(string asciiString)
		{
			string hexValue = "";
			foreach (char c in asciiString)
			{
				int tmp = c;
				hexValue += $"{Convert.ToUInt32(tmp.ToString()):x2}";
			}
			return hexValue;
		}

		private static string ConvertHexToString(string hexValue)
		{
			string asciiString = "";
			while (hexValue.Length > 0)
			{
				asciiString += Convert.ToChar(Convert.ToUInt32(hexValue.Substring(0, 2), 16)).ToString();
				hexValue = hexValue.Substring(2, hexValue.Length - 2);
			}
			return asciiString;
		}

		#endregion
	}
}