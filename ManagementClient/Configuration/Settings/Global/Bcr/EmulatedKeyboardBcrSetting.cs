﻿using System;

namespace Configuration.Settings.Global.Bcr
{
  public class EmulatedKeyboardBcrSetting
	{
    /// <summary>
    /// Impostazione per l'abilitazione del Bcr
    /// </summary>
    public bool Enabled { get; }

    /// <summary>
    /// Carattere per il riconoscimento dell'inizio stringa
    /// </summary>
    public char StartChar { get; }

    /// <summary>
    /// Carattere per il riconoscimento del fine stringa
    /// </summary>
    public char EndChar { get; }

    public EmulatedKeyboardBcrSetting(bool enabled, string startChar, string endChar)
    {
	    if (!enabled)
		    return;

      if (string.IsNullOrEmpty(startChar))
        startChar = "0";
      if (string.IsNullOrEmpty(endChar))
        endChar = "0";

	    Enabled = enabled;
			StartChar = (char)Convert.ToInt32(startChar);
      EndChar = (char)Convert.ToInt32(endChar);
    }
  }
}