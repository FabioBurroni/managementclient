﻿namespace Configuration.Settings.Global
{
  public class LoggingSetting
  {
    /// <summary>
    /// Indica se loggare le attività di XmlClient
    /// </summary>
    public bool LogXmlClient { get; private set; }

    /// <summary>
    /// Indica se loggare le attività di XmlServer
    /// </summary>
    public bool LogXmlServer { get; private set; }

    /// <summary>
    /// Indica se loggare le attività generali dell'applicazione
    /// </summary>
    public bool LogGeneralActivity { get; private set; }

    public LoggingSetting(bool logXmlClient, bool logXmlServer, bool logGeneralActivity)
    {
      LogXmlClient = logXmlClient;
      LogXmlServer = logXmlServer;
      LogGeneralActivity = logGeneralActivity;
    }
  }
}