﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.XPath;
using Configuration.Settings.Global;
using Configuration.Settings.Global.Bcr;
using Configuration.Settings.UI;
using Utilities.Extensions;
using WindowsInput;

namespace Configuration
{
  /// <summary>
  /// Classe che si occupa di caricare la configurazione globale a partire dal
  /// file di configurazione xml
  /// </summary>
  public class BaseConfigurationReader
	{
		#region Fields

		private XPathDocument _xDoc;
		private XPathNavigator _xNav;
		private readonly BaseConfigurationParameters _parameters;

		#endregion

		#region Constructor

		public BaseConfigurationReader(BaseConfigurationParameters parameters)
		{
			if (parameters == null)
				throw new ArgumentNullException(nameof(parameters));

			_parameters = parameters;
		}

		#endregion

		#region Public Methods

		public virtual void LoadConfiguration(string filePath)
		{
			if (!File.Exists(filePath))
				throw new FileNotFoundException("Configuration file non found", filePath);

			_xDoc = new XPathDocument(filePath);
			_xNav = _xDoc.CreateNavigator();
			ReadXml();
		}

		#endregion

		#region Private Methods

		#region ReadXml

		private void ReadXml()
		{
			Utilities.Logging.Message("BaseConfigurationReader: caricamento dati....");

			XPathNodeIterator ite = _xNav.Select("/configuration");
			if (ite.MoveNext())
			{
				XPathNodeIterator itera = ite.Current.SelectChildren(XPathNodeType.All);
				while (itera.MoveNext())
				{
					#region DIMENSIONI FINESTRA
					if (itera.Current.Name.EqualsIgnoreCase("window"))
					{
						var mainWindow = new WindowSetting
						{
							Height = itera.Current.GetAttribute("height", "").ConvertTo<int>(),
							Width = itera.Current.GetAttribute("width", "").ConvertTo<int>(),
							Resizable = itera.Current.GetAttribute("resizable", "").ConvertTo<bool>(true),
							CanMinimize = itera.Current.GetAttribute("can_minimize", "").ConvertTo<bool>(true),
							CanMaximize = itera.Current.GetAttribute("can_maximize", "").ConvertTo<bool>(true),
							Maximized = itera.Current.GetAttribute("maximized", "").ConvertTo<bool>(true)
						};

						_parameters.Window = mainWindow;
					}
					#endregion

					#region AUTHENTICATION

					else if (itera.Current.Name.EqualsIgnoreCase("authentication"))
					{
						SetAuthentication(itera.Current);
					}

					#endregion

					#region DATE_FORMAT
					else if (itera.Current.Name.EqualsIgnoreCase("date_format"))
					{
						_parameters.DateFormat = itera.Current.GetAttribute("value", "");
					}
					#endregion

					#region WEIGHT_FORMAT
					else if (itera.Current.Name.EqualsIgnoreCase("weight_format"))
					{
						_parameters.WeightFormat = itera.Current.GetAttribute("value", "");
					}
					#endregion

					#region XML_CLIENT
					else if (itera.Current.Name.EqualsIgnoreCase("xml_client_settings"))
					{
						var ipAddress = itera.Current.GetAttribute("ip_address", "");
						var portNumber = itera.Current.GetAttribute("port_number", "").ConvertTo<int>();
						var timeout = itera.Current.GetAttribute("time_out", "").ConvertTo<int>();
						var version = itera.Current.GetAttribute("version", "").ConvertTo<int>();

						_parameters.XmlClientConnectionSetting = new ConnectionSetting(ipAddress, portNumber, timeout, version);
					}
					#endregion

					#region XML_SERVER
					else if (itera.Current.Name.EqualsIgnoreCase("xml_server_settings"))
					{
						ConnectionSetting xmlServerSetting;

						var enabledString = itera.Current.GetAttribute("enabled", "");
						var enabled = string.IsNullOrEmpty(enabledString) || enabledString.ConvertTo<bool>();

						if (!enabled)
						{
							//se non è da abilitare, nemmeno leggo gli altri dati
							xmlServerSetting = new ConnectionSetting(enabled);
						}
						else
						{
							var ipAddress = itera.Current.GetAttribute("ip_address", "");
							var portNumber = itera.Current.GetAttribute("port_number", "").ConvertTo<int>();
							var timeout = itera.Current.GetAttribute("time_out", "").ConvertTo<int>();
							var version = itera.Current.GetAttribute("version", "").ConvertTo<int>();

							xmlServerSetting = new ConnectionSetting(ipAddress, portNumber, timeout, version, enabled);
						}

						_parameters.XmlServerConnectionSetting = xmlServerSetting;
					}
					#endregion

					#region UPDATE
					else if (itera.Current.Name.EqualsIgnoreCase("update"))
					{
						var feedUrl = itera.Current.GetAttribute("feedUrl", "");
						var checkingIntervalMinutes = itera.Current.GetAttribute("checkingIntervalMinutes", "").ConvertTo<int>(true);
						var remindMeLaterIntervalMinutes = itera.Current.GetAttribute("remindMeLaterIntervalMinutes", "").ConvertTo<int>(true);
						var installWithoutUserConfirm = itera.Current.GetAttribute("installWithoutUserConfirm", "").ConvertTo<bool>(true);

						_parameters.UpdateSetting = new UpdateSetting(feedUrl, checkingIntervalMinutes, remindMeLaterIntervalMinutes, installWithoutUserConfirm);
					}
					#endregion

					#region RUN_APPLICATION_AT_WINDOWS_STARTUP

					else if (itera.Current.Name.EqualsConsiderCase("run_application_at_windows_startup"))
					{
						_parameters.RunApplicationAtWindowsStartup = itera.Current.GetAttribute("enable", "").ConvertTo<bool>(true);
					}

					#endregion

					#region BCR

					else if (itera.Current.Name.EqualsConsiderCase("bcr"))
					{
						SetBcr(itera.Current);
					}

					#endregion

					#region COMMAND_PREAMBLE
					else if (itera.Current.Name.EqualsIgnoreCase("command_preambles"))
					{
						SetCommandPreambles(itera.Current);
					}
					#endregion

					#region LOG
					else if (itera.Current.Name.EqualsIgnoreCase("log"))
					{
						SetLogging(itera.Current);
					}
					#endregion

					#region HOMEPAGE
					else if (itera.Current.Name.EqualsIgnoreCase("homepage"))
					{
						SetHomePage(itera.Current);
					}
					#endregion

					#region MENU

					else if (itera.Current.Name.EqualsConsiderCase("menu"))
					{
						var menuType = itera.Current.GetAttribute("type", "").ConvertTo<MenuType>();

						_parameters.MenuType = menuType;
					}

					#endregion

					#region LEFT MENU
					else if (itera.Current.Name.EqualsIgnoreCase("left_menu"))
					{
						SetLeftMenu(itera.Current);
					}
					#endregion

					#region TOP MENU
					else if (itera.Current.Name.EqualsIgnoreCase("top_menu"))
					{
						SetTopMenu(itera.Current);
					}
					#endregion

					#region LOCALIZATION
					else if (itera.Current.Name.EqualsIgnoreCase("localization"))
					{
						SetLocalization(itera.Current);
					}
					#endregion

					#region LAYOUT SETTINGS
					else if (itera.Current.Name.EqualsIgnoreCase("layout"))
					{
						SetLayout(itera.Current);
					}
					#endregion

        }
			}
		}

		#endregion

		#region COMMAND PREAMBLE

		private void SetCommandPreambles(XPathNavigator ite)
		{
			var preambles = new CommandPreambleSetting();

			XPathNodeIterator itera = ite.SelectChildren(XPathNodeType.All);
			while (itera.MoveNext())
			{
				if (itera.Current.Name.EqualsIgnoreCase("command_preamble"))
				{
					var type = itera.Current.GetAttribute("type", "");
					var preamble = itera.Current.Value;

					CommandPreamble commandPreamble = new CommandPreamble(type, preamble);

					preambles.AddPreamble(commandPreamble);
				}
			}

			_parameters.CommandPreambleSetting = preambles;
		}

		#endregion

		#region LOGGING

		private void SetLogging(XPathNavigator ite)
		{
			bool xmlClientLog = true, xmlServerLog = true, generalLog = true;

			XPathNodeIterator itera = ite.SelectChildren(XPathNodeType.All);
			while (itera.MoveNext())
			{
				if (itera.Current.Name.EqualsIgnoreCase("log_xml_client_commands"))
				{
					xmlClientLog = itera.Current.GetAttribute("value", "").ConvertTo<bool>();
				}
				else if (itera.Current.Name.EqualsIgnoreCase("log_xml_server_commands"))
				{
					xmlServerLog = itera.Current.GetAttribute("value", "").ConvertTo<bool>();
				}
				else if (itera.Current.Name.EqualsIgnoreCase("log_activity"))
				{
					generalLog = itera.Current.GetAttribute("value", "").ConvertTo<bool>();
				}
			}

			_parameters.LogSetting = new LoggingSetting(xmlClientLog, xmlServerLog, generalLog);
		}

		#endregion

		#region BCR

		private void SetBcr(XPathNavigator ite)
		{
			EmulatedKeyboardBcrSetting emulatedKeyboardBcrSetting = null;
			SerialBcrSetting serialBcrSetting = null;

			XPathNodeIterator itera = ite.SelectChildren(XPathNodeType.All);
			while (itera.MoveNext())
			{
				if (itera.Current.Name.EqualsIgnoreCase("emulated_keyboard_bcr"))
				{
					var enabled = itera.Current.GetAttribute("enabled", "").ConvertTo<bool>(true);
					var startCharDec = itera.Current.GetAttribute("start_char_dec", "").ConvertTo<string>(true);
					var endCharDec = itera.Current.GetAttribute("end_char_dec", "").ConvertTo<string>(true);

					emulatedKeyboardBcrSetting = new EmulatedKeyboardBcrSetting(enabled, startCharDec, endCharDec);
				}
				else if (itera.Current.Name.EqualsIgnoreCase("serial_bcr"))
				{
					var enabled = itera.Current.GetAttribute("enabled", "").ConvertTo<bool>(true);
					var port = itera.Current.GetAttribute("port", "").ConvertTo<string>(true);
					var baudRate = itera.Current.GetAttribute("baud_rate", "").ConvertTo<int>(true);
					var terminatorDec = (itera.Current.GetAttribute("terminator_dec", "").ConvertTo<string>(true) ?? "").Split('-', '_', ',', '.', ';');

					serialBcrSetting = new SerialBcrSetting(enabled, port, baudRate, terminatorDec);
				}
			}

			_parameters.BcrSetting = new BcrSetting(emulatedKeyboardBcrSetting, serialBcrSetting);
		}

		#endregion

		#region HOMEPAGE

		private void SetHomePage(XPathNavigator ite)
		{
			XPathNodeIterator itIn = ite.SelectChildren(XPathNodeType.All);

			var homepageList = new List<Homepage>();

			while (itIn.MoveNext())
			{
				if (itIn.Current.Name.EqualsIgnoreCase("ctrl"))
				{
					Homepage page = new Homepage();

					page.Name = itIn.Current.GetAttribute("name", "");
					page.Container = itIn.Current.GetAttribute("container", "");
					page.Value = itIn.Current.Value;


					homepageList.Add(page);
				}

			}

			_parameters.Homepages = homepageList;
		}
	

	
		#endregion

		#region LEFT / TOP MENU

		private void SetLeftMenu(XPathNavigator ite)
		{
			XPathNodeIterator itera = ite.SelectChildren(XPathNodeType.All);
			LeftMenu leftMenu = new LeftMenu();

			while (itera.MoveNext())
			{
				if (itera.Current.Name.EqualsIgnoreCase("section"))
				{
					var name = itera.Current.GetAttribute("name", "");
					var text = itera.Current.GetAttribute("text", "");
					var position = itera.Current.GetAttribute("position", "").ConvertTo<int>();
					var action = itera.Current.GetAttribute("action", "");
					var icon = itera.Current.GetAttribute("icon", "");

					string allowed_position = "";
          try
          {
						allowed_position = itera.Current.GetAttribute("allowed_position", "");
          }
          catch (Exception)
          {
          }

					MenuSection menuSection = new MenuSection(name, text, position, action, allowed_position,icon);

					SetButtonSection(itera.Current, menuSection);
					leftMenu.Add(menuSection);
				}
				else if (itera.Current.Name.EqualsIgnoreCase("main_ctrl"))
				{
					var mainControlName = itera.Current.GetAttribute("name", "");
					var mainControlContainer = itera.Current.GetAttribute("container", "");
					var mainControlPath = itera.Current.Value;

					leftMenu.MainControl = new ControlSection(mainControlName, mainControlContainer, mainControlPath);
				}
			}

			_parameters.LeftMenu = leftMenu;
		}

		private void SetTopMenu(XPathNavigator ite)
		{
			XPathNodeIterator itera = ite.SelectChildren(XPathNodeType.All);
			TopMenu topMenu = new TopMenu();

			while (itera.MoveNext())
			{
				if (itera.Current.Name.EqualsIgnoreCase("section"))
				{
					var name = itera.Current.GetAttribute("name", "");
					var text = itera.Current.GetAttribute("text", "");
					var position = itera.Current.GetAttribute("position", "").ConvertTo<int>();
					var action = itera.Current.GetAttribute("action", "");
					var icon = itera.Current.GetAttribute("icon", "");
					string allowed_position = "";
					try
					{
						allowed_position = itera.Current.GetAttribute("allowed_position", "");
					}
					catch (Exception)
					{
					}
					MenuSection menuSection = new MenuSection(name, text, position, action, allowed_position, icon);

					SetButtonSection(itera.Current, menuSection);
					topMenu.Add(menuSection);
				}
				else if (itera.Current.Name.EqualsIgnoreCase("program"))
				{
					var name = itera.Current.GetAttribute("name", "");
					var text = itera.Current.GetAttribute("text", "");
					var position = itera.Current.GetAttribute("position", "").ConvertTo<int>(true);
					var action = itera.Current.GetAttribute("action", "");
					var icon = itera.Current.GetAttribute("icon", "");

					string allowed_position = "";
					try
					{
						allowed_position = itera.Current.GetAttribute("allowed_position", "");
					}
					catch (Exception)
					{
					}

					MenuSection menuSection = new MenuSection(name, text, position, action, allowed_position, icon);

					SetButtonSection(itera.Current, menuSection);

					topMenu.Program = menuSection;
				}
			}

			_parameters.TopMenu = topMenu;
		}

		private void SetButtonSection(XPathNavigator ite, MenuSection menuSection)
		{
			XPathNodeIterator itera = ite.SelectChildren(XPathNodeType.All);

			while (itera.MoveNext())
			{
				if (itera.Current.Name.EqualsIgnoreCase("button"))
				{
					var name = itera.Current.GetAttribute("name", "");
					var text = itera.Current.GetAttribute("text", "");
					var position = itera.Current.GetAttribute("position", "").ConvertTo<int>();
					var action = itera.Current.GetAttribute("action", "");
					var image = itera.Current.GetAttribute("image", "");
					var icon = itera.Current.GetAttribute("icon", "");
					var iconColor = itera.Current.GetAttribute("iconColor", "");

					ButtonSection buttonSection = new ButtonSection(name, text, position, action, image, icon, iconColor);

					SetButtonCtrl(itera.Current, buttonSection);
					menuSection.Add(buttonSection);
				}
			}
		}

		private void SetButtonCtrl(XPathNavigator ite, ButtonSection buttonSection)
		{
			XPathNodeIterator itera = ite.SelectChildren(XPathNodeType.All);

			while (itera.MoveNext())
			{
				if (itera.Current.Name.EqualsIgnoreCase("ctrl"))
				{
					var name = itera.Current.GetAttribute("name", "");
					var container = itera.Current.GetAttribute("container", "");
					var path = itera.Current.Value;

					buttonSection.Control = new ControlSection(name, container, path);
				}
			}
		}

		#endregion

		#region LOCALIZATION/LANGUAGE

		private void SetLocalization(XPathNavigator ite)
		{
			XPathNodeIterator itIn = ite.SelectChildren(XPathNodeType.All);

			var localization = new Localization();

			while (itIn.MoveNext())
			{
				XPathNodeIterator itera = itIn.Current.SelectChildren(XPathNodeType.All);

				if (itera.Current.Name.EqualsIgnoreCase("language"))
				{
					var name = itera.Current.Value;
					var isDefault = itera.Current.GetAttribute("default", "").ConvertTo<bool>(true);
					var image = itera.Current.GetAttribute("image", "");

					localization.AddLanguage(new Language(name, isDefault, image));
				}
			}

			_parameters.Localization = localization;

			SetKeyboardLayout();
		}




		#endregion

		#region AUTHENTICATION

		private void SetAuthentication(XPathNavigator ite)
		{
			XPathNodeIterator itIn = ite.SelectChildren(XPathNodeType.Element);

			LoginSetting loginSetting = null;
			LogoutSetting logoutSetting = new LogoutSetting();
			UserInactivity userInactivity = null;
			Customer customer = null;

			string form = "", remoteErrorCodeMapper = "";

			//se presente, recupero l'attributo di abilitazione. Se non esiste, allora l'abilitazione è abilitata di default per mantenere la retrocompatibilità.
			var enabled = itIn.Current.GetAttribute("enabled", "").ConvertTo<bool?>(true) ?? true;

			while (itIn.MoveNext())
			{
				#region LOGIN

				if (itIn.Current.Name.EqualsIgnoreCase("login"))
				{
					var clientCode = itIn.Current.GetAttribute("client_code", "");
					var sourceType = itIn.Current.GetAttribute("source_type", "");
					var positions = new List<string>();

					XPathNodeIterator iteraPositions = itIn.Current.SelectChildren(XPathNodeType.Element);

					while (iteraPositions.MoveNext())
					{
						if (iteraPositions.Current.Name.EqualsIgnoreCase("position"))
						{
							var pos = iteraPositions.Current.InnerXml;

							if (string.IsNullOrEmpty(pos))
								continue;

							positions.Add(pos);
						}
					}

					loginSetting = new LoginSetting(clientCode, sourceType, positions);
				}

				#endregion

				#region LOGOUT

				else if (itIn.Current.Name.EqualsIgnoreCase("logout"))
				{
					//NOTE non gestito
				}

				#endregion

				#region USER INACTIVITY

				else if (itIn.Current.Name.EqualsIgnoreCase("user_inactivity"))
				{
					var timeout = itIn.Current.GetAttribute("timeout_minutes", "").ConvertTo<int>();

					userInactivity = new UserInactivity(timeout);
				}

				#endregion

				#region CUSTOMER

				else if (itIn.Current.Name.EqualsIgnoreCase("customer"))
				{
					var company = itIn.Current.GetAttribute("company", "");
					customer = new Customer(company);

					try
					{
						var project = itIn.Current.GetAttribute("project", "");
						customer.Project = project;
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex);
					}
				}

				#endregion

				#region Form

				else if (itIn.Current.Name.EqualsIgnoreCase("form"))
				{
					form = itIn.Current.InnerXml;
				}

				#endregion

				#region RemoteErrorCodeMapper

				else if (itIn.Current.Name.EqualsIgnoreCase("remote_error_code_mapper"))
				{
					remoteErrorCodeMapper = itIn.Current.InnerXml;
				}

				#endregion
			}

			_parameters.AuthenticationSetting = new AuthenticationSetting(enabled, loginSetting, logoutSetting, userInactivity, customer);

			if (!string.IsNullOrEmpty(form)) _parameters.AuthenticationSetting.Form = form;
			if (!string.IsNullOrEmpty(remoteErrorCodeMapper)) _parameters.AuthenticationSetting.RemoteErrorCodeMapper = remoteErrorCodeMapper;
		}

		#endregion

		#region LAYOUT

		private void SetLayout(XPathNavigator ite)
		{
			XPathNodeIterator itIn = ite.SelectChildren(XPathNodeType.All);

			var layoutSectionList = new List<LayoutSettingSection>();

			while (itIn.MoveNext())
			{
				if (itIn.Current.Name.EqualsIgnoreCase("LayoutSection"))
				{
					LayoutSettingSection settingSection = new LayoutSettingSection();

					settingSection.Title = itIn.Current.GetAttribute("title", "");
					settingSection.Icon = itIn.Current.GetAttribute("icon", "");
					settingSection.Descr = itIn.Current.GetAttribute("descr", "");
					settingSection.Visible = itIn.Current.GetAttribute("visible", "");

					XPathNodeIterator itera = itIn.Current.SelectChildren(XPathNodeType.All);
					List<LayoutSetting> layoutList = new List<LayoutSetting>();

					while (itera.MoveNext())
					{
						if (itera.Current.Name.EqualsIgnoreCase("setting"))
						{
							LayoutSetting setting = new LayoutSetting();
							setting.Name = itera.Current.GetAttribute("name", "");
							setting.Title = itera.Current.GetAttribute("title", "");
							setting.Descr = itera.Current.GetAttribute("descr", "");
							setting.Type = itera.Current.GetAttribute("type", "");
							setting.Icon = itera.Current.GetAttribute("icon", "");
							setting.Default = itera.Current.GetAttribute("default", "");
							setting.Value = itera.Current.Value;

							layoutList.Add(setting);
						}
						
					}
					settingSection.Settings = layoutList;

					layoutSectionList.Add(settingSection);
				}

			}

			_parameters.Layout = layoutSectionList;

		}

		#endregion

		#region KEYBOARDLAYOUT

		private void SetKeyboardLayout()
		{
			_parameters.Keyboards = new List<KeyboardLayout>();
			foreach (var lan in _parameters.Localization.Languages)
			{
				var filePath = Path.Combine(Path.Combine(ConfigurationManager.AssemblyDirectory, "Keyboards"), lan.Name + ".xml");
				if (File.Exists(filePath))
				{
					var xDoc = new XPathDocument(filePath);
					var xNav = xDoc.CreateNavigator();

					XPathNodeIterator conf = xNav.Select("/configuration");

					if (conf.MoveNext())
					{
						XPathNodeIterator keyboard = conf.Current.SelectChildren(XPathNodeType.All);

						while (keyboard.MoveNext())
						{
							if (keyboard.Current.Name.EqualsIgnoreCase("keyboard"))
							{
                var myKeyboard = new KeyboardLayout();
								myKeyboard.Rows = new List<KeyboardRow>();

								myKeyboard.Name = keyboard.Current.GetAttribute("Name", "");

								XPathNodeIterator rows = keyboard.Current.SelectChildren(XPathNodeType.All);
								while (rows.MoveNext())
                {
									if (rows.Current.Name.EqualsIgnoreCase("row"))
                  {
                    KeyboardRow row = new KeyboardRow();
										List<KeyboardKey> keys = new List<KeyboardKey>();
										row.Keys = keys;

										row.Id = rows.Current.GetAttribute("Id", "").ConvertTo<int>(true);
										row.SpecialKey = rows.Current.GetAttribute("SpecialKey", "").ConvertTo<bool>(true);
										row.Numpad = rows.Current.GetAttribute("Numpad", "").ConvertTo<bool>(true);

										XPathNodeIterator xmlkey = rows.Current.SelectChildren(XPathNodeType.All);

                    while (xmlkey.MoveNext())
                    {
                      if (xmlkey.Current.Name.EqualsIgnoreCase("key"))
                      {
                        KeyboardKey key = new KeyboardKey();

												key.Code = xmlkey.Current.GetAttribute("Code", "").ConvertTo<VirtualKeyCode>(true);
												key.ModifierCode = xmlkey.Current.GetAttribute("ModifierCode", "").ConvertTo<VirtualKeyCode>(true);
												//key.Length = xmlkey.Current.GetAttribute("Length", "").ConvertTo<double>(true);

												System.Globalization.CultureInfo customCulture = new System.Globalization.CultureInfo("it-IT");
												string sLength =	xmlkey.Current.GetAttribute("Length", "");
												double dLength;
												if (double.TryParse(sLength, System.Globalization.NumberStyles.AllowDecimalPoint, customCulture, out dLength))
													key.Length = dLength;

												key.KeyDisplay0 = xmlkey.Current.GetAttribute("KeyDisplay0", "");
												key.KeyDisplay1 = xmlkey.Current.GetAttribute("KeyDisplay1", "");

												keys.Add(key);
                      }

                    }

                    row.Keys = keys;

                    myKeyboard.Rows.Add(row);
                  }
								}

								_parameters.Keyboards.Add(myKeyboard);
							}
						}
					}
				}
			}
		}

      #endregion

		#endregion
	}
  }