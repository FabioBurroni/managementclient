﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Utilities;
using static KeyPad.KeyboardBase;

namespace KeyPad
{
  /// <summary>
  /// Interaction logic for TextBoxKeyboard.xaml
  /// </summary>
  public partial class TextBoxKeyboard : TextBox
  {
    private static KeyboardBase Keybord;
    private static TextBoxKeyboard prevTbox;

    public enum KeyboardType
    {
      Default,
      Numeric
    }

    public KeyboardType InputKeyboardType
    {
      get;
      set;
    } = KeyboardType.Default;

    public KeyboardStartupLocation _keyboardStartLocation = KeyboardStartupLocation.BottomScreen;
    public KeyboardStartupLocation KeyboardStartLocation
    {
      get
      {
        return _keyboardStartLocation;
      }
      set
      {
        _keyboardStartLocation = value;
      }
    }

    public TextBoxKeyboard()
    {
      InitializeComponent();
    }


    private void ShowKeyboard()
    {
      if (InputKeyboardType == KeyboardType.Default)
      {
        Keybord = new VirtualKeyboard(this, Window.GetWindow(this));
      }
      else if (InputKeyboardType == KeyboardType.Numeric)
      {
        Keybord = new Keypad(this, Window.GetWindow(this));
      }
      Keybord.KeyboardStartLocation = KeyboardStartLocation;
      Keybord.Show();
      Keybord.Closed += Keybord_Closed;
    }

    private void Keybord_Closed(object sender, EventArgs e)
    {
      Keybord.Closed -= Keybord_Closed;
      Keybord = null;
    }

    /// <summary>
    /// return True if is possible open a new window , false otherwise
    /// </summary>
    /// <param name="tb"></param>
    /// <returns></returns>
    private bool CheckKeyboardShowed(TextBoxKeyboard tb)
    {
      if (Keybord != null)
      {
        if (prevTbox != null && tb == prevTbox)
          return false;
        Keybord.Close();
      }
      prevTbox = tb;
      return true;
    }

    protected override void OnMouseDown(MouseButtonEventArgs e)
    {
      base.OnMouseDown(e);
      if (Configuration.ConfigurationManager.Parameters.CassioliVirtualKeyboard.IsEnabled)
      {
        try
        {
          if (CheckKeyboardShowed((TextBoxKeyboard)this))
            ShowKeyboard();
        }
        catch (Exception ex)
        {
          $"******************exception :\n{ex.Message} - {ex.StackTrace}".WriteToLog("MOUSE_DOWN");
        }
      }
      
    }

    protected override void OnTouchDown(TouchEventArgs e)
    {
      base.OnTouchDown(e);
      if (Configuration.ConfigurationManager.Parameters.CassioliVirtualKeyboard.IsEnabled)
      {
        try
        {
          if (CheckKeyboardShowed((TextBoxKeyboard)this))
            ShowKeyboard();
        }
        catch (Exception ex)
        {
          $"******************exception :\n{ex.Message} - {ex.StackTrace}".WriteToLog("TOUCH_DOWN");
        }
      }
    }
    
  }
}
