﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace KeyPad
{
  /// <summary>
  /// Logica di interazione per VirtualKeyboard.xaml
  /// </summary>
  public partial class VirtualKeyboard : KeyboardBase, INotifyPropertyChanged
  {
    #region Public Properties
    
    private bool _showNumericKeyboard;
    public bool ShowNumericKeyboard
    {
      get { return _showNumericKeyboard; }
      set { _showNumericKeyboard = value; this.OnPropertyChanged("ShowNumericKeyboard"); }
    }
    
    #endregion

    #region Constructor

    public VirtualKeyboard(TextBox owner, Window wndOwner)
      :base(owner, wndOwner)
    {
      InitializeComponent();
      this.DataContext = this;
    }

    #endregion

    #region Callbacks

    private void button_Click(object sender, RoutedEventArgs e)
    {
      Button button = sender as Button;
      if (button != null)
      {
        int currIndex = _tbox.CaretIndex;
        switch (button.CommandParameter.ToString())
        {
          case "LSHIFT":
            Regex upperCaseRegex = new Regex("[A-Z]");
            Regex lowerCaseRegex = new Regex("[a-z]");
            Button btn;
            foreach (UIElement elem in AlfaKeyboard.Children) //iterate the main grid
            {
              Grid grid = elem as Grid;
              if (grid != null)
              {
                foreach (UIElement uiElement in grid.Children)  //iterate the single rows
                {
                  btn = uiElement as Button;
                  if (btn != null) // if button contains only 1 character
                  {
                    if (btn.Content.ToString().Length == 1)
                    {
                      if (upperCaseRegex.Match(btn.Content.ToString()).Success) // if the char is a letter and uppercase
                        btn.Content = btn.Content.ToString().ToLower();
                      else if (lowerCaseRegex.Match(button.Content.ToString()).Success) // if the char is a letter and lower case
                        btn.Content = btn.Content.ToString().ToUpper();
                    }

                  }
                }
              }
            }
            break;

          case "ALT":
          case "CTRL":
            break;

          case "ESC":
          case "RETURN":
            Close();
            break;

          case "BACK":
            if (currIndex > 0)
            {
              _tbox.Text = _tbox.Text.Remove(currIndex-1, 1);
              _tbox.CaretIndex = currIndex-1;
            }
              
            break;

          default:
            _tbox.Text = _tbox.Text.Insert(currIndex, button.Content.ToString());
            _tbox.CaretIndex = currIndex + 1;
            break;
        }
        
      }
    }

    #endregion

    #region INotifyPropertyChanged members

    public event PropertyChangedEventHandler PropertyChanged;
    protected void OnPropertyChanged(string propertyName)
    {
      if (PropertyChanged != null)
        this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    }

    #endregion
  }
}
