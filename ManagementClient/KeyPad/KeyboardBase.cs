﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace KeyPad
{
  public class KeyboardBase : Window
  {
    public enum KeyboardStartupLocation
    {
      AboveTextBox,
      BelowTextBox,
      OnLeftTextBox,
      OnRigthTextBox,
      BottomScreen,
      ScreenCenter
    }

    protected TextBox _tbox;

    public KeyboardStartupLocation _keyboardStartLocation = KeyboardStartupLocation.BottomScreen;
    public KeyboardStartupLocation KeyboardStartLocation {
      get
      {
        return _keyboardStartLocation;
      }
      set
      {
        _keyboardStartLocation = value;
        SetWindowLocationAtStart();
      }
    }

    public KeyboardBase() { }

    public KeyboardBase(TextBox owner, Window wndOwner)
    {
      WindowStyle = WindowStyle.ToolWindow;
      SetWindowLocationAtStart();
      ShowInTaskbar = false;
      Title = "";
      _tbox = owner;
      Owner = wndOwner;
      _tbox.IsVisibleChanged += _tbox_IsVisibleChanged;
    }

    protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
    {
      base.OnRenderSizeChanged(sizeInfo);
      SetWindowLocationAtStart();
    }

    private void SetWindowLocationAtStart()
    {
      switch (KeyboardStartLocation)
      {
        case KeyboardStartupLocation.ScreenCenter:
          {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
          }
          break;
        case KeyboardStartupLocation.BottomScreen:
          {
            WindowStartupLocation = WindowStartupLocation.Manual;
            var desktopWorkingArea = SystemParameters.WorkArea;
            this.Left = (desktopWorkingArea.Right - this.ActualWidth)/2;
            this.Top = desktopWorkingArea.Bottom - this.ActualHeight;
          }
          break;
        case KeyboardStartupLocation.BelowTextBox:
          {
            WindowStartupLocation = WindowStartupLocation.Manual;
            Point locationFromScreen = this._tbox.PointToScreen(new Point(0, 0));
            this.Left = locationFromScreen.X;
            this.Top = locationFromScreen.Y+ _tbox.ActualHeight;
          }
          break;
        default:
          {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
          }
          break;
      }
    }

    private void _tbox_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      if ((bool)e.NewValue == false)
      {
        Close();
      }
    }

    protected override void OnClosing(CancelEventArgs e)
    {
      base.OnClosing(e);
      _tbox.IsVisibleChanged -= _tbox_IsVisibleChanged;
    }
  }
}
