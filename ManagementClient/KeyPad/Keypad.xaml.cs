﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;

namespace KeyPad
{
  /// <summary>
  /// Logica di interazione per MainWindow.xaml
  /// </summary>
  public partial class Keypad : KeyboardBase
  {
    public Keypad(TextBox owner, Window wndOwner)
      :base(owner,wndOwner)
    {
      InitializeComponent();
      this.DataContext = this;
    }

    private void button_Click(object sender, RoutedEventArgs e)
    {
      Button button = sender as Button;
      int currIndex = _tbox.CaretIndex;
      switch (button.CommandParameter.ToString())
      {
        case "ESC":
        case "RETURN":
          Close();
          break;

        case "BACK":
          if (currIndex > 0)
          {
            _tbox.Text = _tbox.Text.Remove(currIndex - 1, 1);
            _tbox.CaretIndex = currIndex - 1;
          }
          break;

        default:
          _tbox.Text = _tbox.Text.Insert(currIndex, button.Content.ToString());
          _tbox.CaretIndex = currIndex + 1;
          break;
      }
    }
    

  }
}
