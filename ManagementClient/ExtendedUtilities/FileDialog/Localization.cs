﻿using Localization;
using System;
using System.Windows.Markup;

namespace ExtendedUtilities.FileDialog
{
  public class Localization : MarkupExtension
  {
    public Localization() { }
    public string Translate { get; set; }

    public override object ProvideValue(IServiceProvider serviceProvider)
    {
      return Localize.LocalizeDefaultString(Translate);
    }
  }
}
