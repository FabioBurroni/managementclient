﻿using MaterialDesignExtensions.Controls;
using MaterialDesignExtensions.Converters;
using MaterialDesignExtensions.Model;
using MaterialDesignThemes.Wpf;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;

namespace ExtendedUtilities.FileDialog
{
  /// <summary>
  /// A dialog for selecting a file to save data into.
  /// </summary>
  public class OpenDirectoryDialog : BaseFileDialog
  {
    /// <summary>
    /// The name of the file itself without the full path.
    /// </summary>
    public static readonly DependencyProperty FilenameProperty = DependencyProperty.Register(
            nameof(Filename),
            typeof(string),
            typeof(OpenDirectoryDialog),
            new PropertyMetadata(null));

    /// <summary>
    /// The name of the file itself without the full path.
    /// </summary>
    public string Filename
    {
      get
      {
        return (string)GetValue(FilenameProperty);
      }

      set
      {
        SetValue(FilenameProperty, value);
      }
    }

    /// <summary>
    /// Forces the possible file extension of the selected file filter for new filenames.
    /// </summary>
    public static readonly DependencyProperty ForceFileExtensionOfFileFilterProperty = DependencyProperty.Register(
        nameof(ForceFileExtensionOfFileFilter),
        typeof(bool),
        typeof(SaveFileDialog),
        new PropertyMetadata(false));

    /// <summary>
    /// Forces the possible file extension of the selected file filter for new filenames.
    /// </summary>
    public bool ForceFileExtensionOfFileFilter
    {
      get
      {
        return (bool)GetValue(ForceFileExtensionOfFileFilterProperty);
      }

      set
      {
        SetValue(ForceFileExtensionOfFileFilterProperty, value);
      }
    }

    static OpenDirectoryDialog()
    {
      DefaultStyleKeyProperty.OverrideMetadata(typeof(SaveFileDialog), new FrameworkPropertyMetadata(typeof(SaveFileDialog)));
    }

    /// <summary>
    /// Creates a new <see cref="OpenFolderDialog" />.
    /// </summary>
    public OpenDirectoryDialog() : base() { }

    protected override void CancelHandler(object sender, RoutedEventArgs args)
    {
      DialogHost.CloseDialogCommand.Execute(new OpenDirectoryDialogResult(true, null), GetDialogHost());
    }

    protected override void OpenDirectoryControlFileSelectedHandler(object sender, RoutedEventArgs args)
    {
      DialogHost.CloseDialogCommand.Execute(new OpenDirectoryDialogResult(false, (args as FileSelectedEventArgs)?.FileInfo), GetDialogHost());
    }

    /// <summary>
    /// Shows a new <see cref="SaveFileDialog" />.
    /// </summary>
    /// <param name="dialogHostName">The name of the <see cref="DialogHost" /></param>
    /// <param name="args">The arguments for the dialog initialization</param>
    /// <returns></returns>
    public static async Task<OpenDirectoryDialogResult> ShowDialogAsync(string dialogHostName, OpenDirectoryDialogArguments args)
    {
      OpenDirectoryDialog dialog = InitDialog(
          args.Width,
          args.Height,
          args.CurrentDirectory,
          args.CreateNewDirectoryEnabled,
          args.ShowHiddenFilesAndDirectories,
          args.ShowSystemFilesAndDirectories,
          args.SwitchPathPartsAsButtonsEnabled,
          args.PathPartsAsButtons
      );

      return await DialogHost.Show(dialog, dialogHostName, args.OpenedHandler, args.ClosingHandler) as OpenDirectoryDialogResult;
    }

    /// <summary>
    /// Shows a new <see cref="SaveFileDialog" />.
    /// </summary>
    /// <param name="dialogHost">The <see cref="DialogHost" /></param>
    /// <param name="args">The arguments for the dialog initialization</param>
    /// <returns></returns>
    public static async Task<OpenDirectoryDialogResult> ShowDialogAsync(DialogHost dialogHost, OpenDirectoryDialogArguments args)
    {
      OpenDirectoryDialog dialog = InitDialog(
          args.Width,
          args.Height,
          args.CurrentDirectory,
          args.CreateNewDirectoryEnabled,
          args.ShowHiddenFilesAndDirectories,
          args.ShowSystemFilesAndDirectories,
          args.SwitchPathPartsAsButtonsEnabled,
          args.PathPartsAsButtons
      );

      return await dialogHost.ShowDialog(dialog, args.OpenedHandler, args.ClosingHandler) as OpenDirectoryDialogResult;
    }

    private static OpenDirectoryDialog InitDialog(double? width, double? height,
        string currentDirectory, 
        bool createNewDirectoryEnabled,
        bool showHiddenFilesAndDirectories, bool showSystemFilesAndDirectories,
        bool switchPathPartsAsButtonsEnabled, bool pathPartsAsButtons)
    {
      OpenDirectoryDialog dialog = new OpenDirectoryDialog();
      InitDialog(dialog, width, height, currentDirectory, showHiddenFilesAndDirectories, showSystemFilesAndDirectories, createNewDirectoryEnabled, switchPathPartsAsButtonsEnabled, pathPartsAsButtons);
      

      return dialog;
    }
  }

  /// <summary>
  /// Arguments to initialize a save file dialog.
  /// </summary>
  public class OpenDirectoryDialogArguments : FileDialogArguments
  {
   

    /// <summary>
    /// Creates a new <see cref="SaveFileDialogArguments" />.
    /// </summary>
    public OpenDirectoryDialogArguments() : base() { }

    /// <summary>
    /// Copy constructor
    /// </summary>
    /// <param name="args"></param>
    public OpenDirectoryDialogArguments(SaveFileDialogArguments args)
        : base(args) { }
  }

  /// <summary>
  /// The dialog result for <see cref="SaveFileDialog" />.
  /// </summary>
  public class OpenDirectoryDialogResult : FileDialogResult
  {
    /// <summary>
    /// Creates a new <see cref="SaveFileDialogResult" />.
    /// </summary>
    /// <param name="canceled">True if the dialog was canceled</param>
    /// <param name="fileInfo">The selected file</param>
    public OpenDirectoryDialogResult(bool canceled, FileInfo fileInfo) : base(canceled, fileInfo) { }
  }
}
