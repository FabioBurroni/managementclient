﻿using System.Windows;
using System.Windows.Media;

namespace ExtendedUtilities.Performance
{
  public static class RenderCapabilityWrapper
  {
    public static readonly DependencyProperty TierProperty = DependencyProperty.RegisterAttached("Tier", typeof(int), typeof(RenderCapabilityWrapper), new PropertyMetadata(RenderCapability.Tier >> 16));
    public static int GetTier(DependencyObject depObj)
    {
      return (int)TierProperty.DefaultMetadata.DefaultValue;
    }
  }
}