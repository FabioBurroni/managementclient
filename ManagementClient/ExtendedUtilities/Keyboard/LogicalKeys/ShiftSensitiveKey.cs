﻿using System.Collections.Generic;
using WindowsInput;

namespace ExtendedUtilities.Keyboard.LogicalKeys
{
    public class ShiftSensitiveKey : MultiCharacterKey
    {
        public ShiftSensitiveKey(VirtualKeyCode keyCode, IList<string> keyDisplays)
            : base(keyCode, keyDisplays)
        {
        }
    }
}