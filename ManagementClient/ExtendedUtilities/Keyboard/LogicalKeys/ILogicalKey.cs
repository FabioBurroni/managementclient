﻿using System.ComponentModel;

namespace ExtendedUtilities.Keyboard.LogicalKeys
{
    public interface ILogicalKey : INotifyPropertyChanged
    {
        string DisplayName { get; }
        void Press();
        event LogicalKeyPressedEventHandler LogicalKeyPressed;
    }
}