﻿namespace ExtendedUtilities.Keyboard.LogicalKeys
{
  public enum KeyboardType
  {
    FullScreen,

    FullScreenAdditional,

    NumFullScreen,

    NumFullScreenAdditional,

    Floating,

    FloatingAdditional,

    NumFloating,

    NumFloatingAdditional,
  }
}