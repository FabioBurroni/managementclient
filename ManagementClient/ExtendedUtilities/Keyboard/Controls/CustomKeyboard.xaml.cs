﻿using Configuration;
using Configuration.Settings.Global;
using ExtendedUtilities.Keyboard.LogicalKeys;
using ExtendedUtilities.Wpf;
using MaterialDesignThemes.Wpf;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ExtendedUtilities.Keyboard.Controls
{
  /// <summary>
  /// Interaction logic for CustomKeyboard.xaml
  /// </summary>
  public partial class CustomKeyboard : UserControl, INotifyPropertyChanged
  {
    #region Notify property changed

    public event PropertyChangedEventHandler PropertyChanged;

    // This method is called by the Set accessor of each property.
    // The CallerMemberName attribute that is applied to the optional propertyName
    // parameter causes the property name of the caller to be substituted as an argument.
    protected void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion Notify property changed

    #region Fields

    private string _text;
    private List<Language> _languages;
    private RelayCommand<CultureInfo> _languageCommand;

    #endregion Fields

    #region DEPENDENCY PROPERTY

    public static readonly DependencyProperty IsDraggingProperty = DependencyProperty.Register("IsDragging", typeof(bool), typeof(CustomKeyboard), new UIPropertyMetadata(false));
    public static readonly DependencyProperty FontSizeTextProperty = DependencyProperty.Register("FontSizeText", typeof(int), typeof(CustomKeyboard), new UIPropertyMetadata(25));
    public static readonly DependencyProperty KeyLanguageProperty = DependencyProperty.Register("KeyLanguage", typeof(string), typeof(CustomKeyboard), new UIPropertyMetadata("it-IT"));
    public static readonly DependencyProperty DeadZoneProperty = DependencyProperty.Register("DeadZone", typeof(double), typeof(CustomKeyboard), new UIPropertyMetadata(5d));
    public static readonly DependencyProperty KeyTypeProperty = DependencyProperty.Register("KeyType", typeof(KeyboardType), typeof(CustomKeyboard), new UIPropertyMetadata(KeyboardType.Floating, OnKeyTypePropertyChanged));

    #endregion DEPENDENCY PROPERTY

    #region COSTRUTTORE

    public CustomKeyboard()
    {
      InitializeComponent();
    }

    #endregion COSTRUTTORE

    #region PROPERTIES

    public ICommand LanguageCommand
    {
      get
      {
        return _languageCommand ?? (_languageCommand = new RelayCommand<CultureInfo>(SetLanguage));
      }
    }

    public int FontSizeText
    {
      get { return (int)GetValue(FontSizeTextProperty); }
      set { SetValue(FontSizeTextProperty, value); }
    }

    public string KeyLanguage
    {
      get { return (string)GetValue(KeyLanguageProperty); }
      set { SetValue(KeyLanguageProperty, value); }
    }

    public string MyText
    {
      get { return _text; }
      set
      {
        _text = value;
        NotifyPropertyChanged("MyText");
      }
    }

    public bool IsDragging
    {
      get { return (bool)GetValue(IsDraggingProperty); }
      set { SetValue(IsDraggingProperty, value); }
    }

    public KeyboardType KeyType
    {
      get { return (KeyboardType)GetValue(KeyTypeProperty); }
      set { SetValue(KeyTypeProperty, value); }
    }

    public double DeadZone
    {
      get { return (double)GetValue(DeadZoneProperty); }
      set { SetValue(DeadZoneProperty, value); }
    }

    #endregion PROPERTIES

    #region Init

    public void Init()
    {
      if (KeyType == KeyboardType.Floating ||
        KeyType == KeyboardType.NumFloating ||
        KeyType == KeyboardType.NumFloatingAdditional ||
        KeyType == KeyboardType.FloatingAdditional)
      {
        Fullscreen.Visibility = Visibility.Collapsed;
        Floating.Visibility = Visibility.Visible;
      }
      else
      {
        Fullscreen.Visibility = Visibility.Visible;
        Floating.Visibility = Visibility.Collapsed;

        FixTextBox();
      }

      #region SET LANGUAGE

      var keyLan = InputLanguageManager.Current.AvailableInputLanguages.Cast<CultureInfo>();
      _languages = new List<Language>();
      var configLan = ConfigurationManager.Parameters.Localization.Languages.ToList();

      foreach (var lan in configLan)
      {
        if (keyLan.Any(s => s.Name == lan.Name))
          _languages.Add(lan);
      }

      var defaultKey = InputLanguageManager.Current.CurrentInputLanguage;
      if (_languages.Any(s => s.Name == defaultKey.Name))
        SelectedLanguage.Source = new BitmapImage(_languages.FirstOrDefault(s => s.Name == defaultKey.Name).ImageUri);

      Languages.ItemsSource = new ObservableCollection<Language>(_languages);

      #endregion SET LANGUAGE

      if (KeyType == KeyboardType.NumFullScreen ||
          KeyType == KeyboardType.NumFullScreenAdditional)
        ChageTypeIcon.Kind = PackIconKind.Keyboard;
      else
        ChageTypeIcon.Kind = PackIconKind.Numeric;
    }

    #endregion Init

    #region EVENTI

    private void SetLanguage(CultureInfo culture)
    {
      var image = _languages.FirstOrDefault(s => s.Name == culture.Name);
      SelectedLanguage.Source = new BitmapImage(image.ImageUri);

      InputLanguageManager.Current.CurrentInputLanguage = image.Culture;

      FullScreenKeyboard.KeyLanguage = image.Name;
      KeyLanguage = image.Name;

      FixTextBox();
    }

    private static void OnKeyTypePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var keyboard = (CustomKeyboard)d;

      var keyType = (KeyboardType)e.NewValue;
      if (keyType == KeyboardType.Floating ||
          keyType == KeyboardType.NumFloating ||
          keyType == KeyboardType.FloatingAdditional ||
          keyType == KeyboardType.NumFloatingAdditional)
      {
        keyboard.Fullscreen.Visibility = Visibility.Collapsed;
        keyboard.Floating.Visibility = Visibility.Visible;
      }
      else
      {
        keyboard.Fullscreen.Visibility = Visibility.Visible;
        keyboard.Floating.Visibility = Visibility.Collapsed;

        keyboard.FixTextBox();
      }
    }

    private void FullScreen_OnEnter()
    {
      DialogHost.CloseDialogCommand.Execute(txtContent.Text, null);
    }

    private void KeyClose_Click(object sender, RoutedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(null, null);
    }

    private void ChageType_Click(object sender, RoutedEventArgs e)
    {
      switch (KeyType)
      {
        case KeyboardType.FullScreen:
          ChageTypeIcon.Kind = PackIconKind.Keyboard;
          FullScreenKeyboard.KeyType = KeyboardType.NumFullScreen;
          KeyType = KeyboardType.NumFullScreen;

          break;

        case KeyboardType.FullScreenAdditional:
          ChageTypeIcon.Kind = PackIconKind.Keyboard;
          FullScreenKeyboard.KeyType = KeyboardType.NumFullScreenAdditional;
          KeyType = KeyboardType.NumFullScreenAdditional;

          break;

        case KeyboardType.NumFullScreen:
          ChageTypeIcon.Kind = PackIconKind.Numeric;
          FullScreenKeyboard.KeyType = KeyboardType.FullScreen;
          KeyType = KeyboardType.FullScreen;

          break;

        case KeyboardType.NumFullScreenAdditional:
          ChageTypeIcon.Kind = PackIconKind.Numeric;
          FullScreenKeyboard.KeyType = KeyboardType.FullScreenAdditional;
          KeyType = KeyboardType.FullScreenAdditional;

          break;
      }

      FixTextBox();
    }

    private void FixTextBox()
    {
      txtContent.Focus();
      txtContent.CaretIndex = txtContent.Text.Length;
    }

    private void ExtendedType_Click(object sender, RoutedEventArgs e)
    {
      if ((sender as ToggleButton).IsChecked == true)
      {
        if (KeyType == KeyboardType.FullScreen)
        {
          FullScreenKeyboard.KeyType = KeyboardType.FullScreenAdditional;
          KeyType = KeyboardType.FullScreenAdditional;
        }
        else if (KeyType == KeyboardType.NumFullScreen)
        {
          FullScreenKeyboard.KeyType = KeyboardType.NumFullScreenAdditional;
          KeyType = KeyboardType.NumFullScreenAdditional;
        }
      }
      else
      {
        if (KeyType == KeyboardType.FullScreenAdditional)
        {
          FullScreenKeyboard.KeyType = KeyboardType.FullScreen;
          KeyType = KeyboardType.FullScreen;
        }
        else if (KeyType == KeyboardType.NumFullScreenAdditional)
        {
          FullScreenKeyboard.KeyType = KeyboardType.NumFullScreen;
          KeyType = KeyboardType.NumFullScreen;
        }
      }

      FixTextBox();
    }

    private void Check_Click(object sender, RoutedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(txtContent.Text, null);
    }

    #endregion EVENTI
  }
}