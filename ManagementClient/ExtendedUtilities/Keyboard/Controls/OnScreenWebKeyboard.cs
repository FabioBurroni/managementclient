﻿using Configuration;
using ExtendedUtilities.Keyboard.LogicalKeys;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using WindowsInput;

namespace ExtendedUtilities.Keyboard.Controls
{
  public class OnScreenWebKeyboard : Grid, INotifyPropertyChanged
  {
    #region Notify property changed

    public event PropertyChangedEventHandler PropertyChanged;

    // This method is called by the Set accessor of each property.
    // The CallerMemberName attribute that is applied to the optional propertyName
    // parameter causes the property name of the caller to be substituted as an argument.
    protected void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion Notify property changed

    #region DEPENDENCY PROPERTY

    public static readonly DependencyProperty AreAnimationsEnabledProperty = DependencyProperty.Register("AreAnimationsEnabled", typeof(bool), typeof(OnScreenWebKeyboard), new UIPropertyMetadata(true, OnAreAnimationsEnabledPropertyChanged));
    public static readonly DependencyProperty FontSizeTextProperty = DependencyProperty.Register("FontSizeText", typeof(int), typeof(OnScreenWebKeyboard), new UIPropertyMetadata(25, OnFontSizeTextPropertyChanged));
    public static readonly DependencyProperty KeyLanguageProperty = DependencyProperty.Register("KeyLanguage", typeof(string), typeof(OnScreenWebKeyboard), new UIPropertyMetadata("it-IT", OnLanguagePropertyChanged));
    public static readonly DependencyProperty KeyTypeProperty = DependencyProperty.Register("KeyType", typeof(KeyboardType), typeof(OnScreenWebKeyboard), new UIPropertyMetadata(KeyboardType.Floating, OnKeyTypePropertyChanged));

    #endregion DEPENDENCY PROPERTY

    #region Fields

    private ObservableCollection<OnScreenKeyboardSection> _sections;
    private List<ModifierKeyBase> _modifierKeys;
    private List<ILogicalKey> _allLogicalKeys;
    private List<OnScreenKey> _allOnScreenKeys;

    private string _textFocused;

    #endregion Fields

    #region PROPERTIES

    public string FocusedText
    {
      get { return _textFocused; }
      set
      {
        _textFocused = value;
        NotifyPropertyChanged("FocusedText");
      }
    }

    public KeyboardType KeyType
    {
      get { return (KeyboardType)GetValue(KeyTypeProperty); }
      set { SetValue(KeyTypeProperty, value); }
    }

    public int FontSizeText
    {
      get { return (int)GetValue(FontSizeTextProperty); }
      set { SetValue(FontSizeTextProperty, value); }
    }

    public string KeyLanguage
    {
      get { return (string)GetValue(KeyLanguageProperty); }
      set { SetValue(KeyLanguageProperty, value); }
    }

    public bool AreAnimationsEnabled
    {
      get { return (bool)GetValue(AreAnimationsEnabledProperty); }
      set { SetValue(AreAnimationsEnabledProperty, value); }
    }

    #endregion PROPERTIES

    #region Init

    public override void BeginInit()
    {
      try
      {
        var current = InputLanguageManager.Current.CurrentInputLanguage.Name;
        var language = ConfigurationManager.Parameters.Localization.Languages.FirstOrDefault(s => s.Name == current);

        ReCreate(language.Name, KeyType);
      }
      catch (Exception)
      {
        SetValue(FocusManager.IsFocusScopeProperty, true);
        _modifierKeys = new List<ModifierKeyBase>();
        _allLogicalKeys = new List<ILogicalKey>();
        _allOnScreenKeys = new List<OnScreenKey>();

        ColumnDefinitions.Clear();
        Children.Clear();

        _allOnScreenKeys.ForEach(x => x.OnScreenKeyPress += OnScreenKeyPress);

        _allOnScreenKeys.ForEach(x => x.SetBinding(OnScreenKey.FontSizeTextProperty, new Binding("FontSizeText") { Source = FontSizeText }));

        SynchroniseModifierKeyState();
      }
      finally
      {
        base.BeginInit();
      }
    }

    #endregion Init

    #region EVENTI

    public delegate void OnEnterHandler();

    public event OnEnterHandler OnEnter;

    private static void OnFontSizeTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var keyboard = (OnScreenWebKeyboard)d;
      keyboard._allOnScreenKeys.ToList().ForEach(x => x.FontSizeText = (int)e.NewValue);
    }

    private static void OnLanguagePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var keyboard = (OnScreenWebKeyboard)d;
      keyboard.ReCreate(e.NewValue.ToString(), keyboard.KeyType);
    }

    private static void OnKeyTypePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var keyboard = (OnScreenWebKeyboard)d;
      keyboard.ReCreate(keyboard.KeyLanguage, (KeyboardType)e.NewValue);
    }

    private static void OnAreAnimationsEnabledPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var keyboard = (OnScreenWebKeyboard)d;
      keyboard._allOnScreenKeys.ToList().ForEach(x => x.AreAnimationsEnabled = (bool)e.NewValue);
    }

    private void OnScreenKeyPress(DependencyObject sender, OnScreenKeyEventArgs e)
    {
      if (e.OnScreenKey.Key is ModifierKeyBase)
      {
        var modifierKey = (ModifierKeyBase)e.OnScreenKey.Key;
        if (modifierKey.KeyCode == VirtualKeyCode.SHIFT)
        {
          HandleShiftKeyPressed(modifierKey);
        }
        else if (modifierKey.KeyCode == VirtualKeyCode.CAPITAL)
        {
          HandleCapsLockKeyPressed(modifierKey);
        }
        else if (modifierKey.KeyCode == VirtualKeyCode.NUMLOCK)
        {
          HandleNumLockKeyPressed(modifierKey);
        }
      }
      else
      {
        ResetInstantaneousModifierKeys();
      }

      _modifierKeys.OfType<InstantaneousModifierKey>().ToList().ForEach(x => x.SynchroniseKeyState());

      if (e.OnScreenKey.Key is VirtualKey)
      {
        var virtualKey = (VirtualKey)e.OnScreenKey.Key;
        if (virtualKey.KeyCode == VirtualKeyCode.RETURN)
        {
          OnEnter?.Invoke();
        }
      }
    }

    private void HandleShiftKeyPressed(ModifierKeyBase shiftKey)
    {
      _allLogicalKeys.OfType<CaseSensitiveKey>().ToList().ForEach(x => x.SelectedIndex =
                                                                        InputSimulator.IsTogglingKeyInEffect(VirtualKeyCode.CAPITAL) ^ shiftKey.IsInEffect ? 1 : 0);
      _allLogicalKeys.OfType<ShiftSensitiveKey>().ToList().ForEach(x => x.SelectedIndex = shiftKey.IsInEffect ? 1 : 0);
    }

    private void HandleCapsLockKeyPressed(ModifierKeyBase capsLockKey)
    {
      _allLogicalKeys.OfType<CaseSensitiveKey>().ToList().ForEach(x => x.SelectedIndex =
                                                                        capsLockKey.IsInEffect ^ InputSimulator.IsKeyDownAsync(VirtualKeyCode.SHIFT) ? 1 : 0);
    }

    private void HandleNumLockKeyPressed(ModifierKeyBase numLockKey)
    {
      _allLogicalKeys.OfType<NumLockSensitiveKey>().ToList().ForEach(x => x.SelectedIndex = numLockKey.IsInEffect ? 1 : 0);
    }

    #endregion EVENTI

    #region METODI

    private ObservableCollection<OnScreenKey> GetPrimaryKey(string language)
    {
      var keyboardLayout = ConfigurationManager.Parameters.Keyboards.FirstOrDefault(s => s.Name == language);

      var mainKeys = new ObservableCollection<OnScreenKey>();

      if (keyboardLayout is null)
      {
        SnackbarTools.MainSnackbar.ShowMessageFail("Keyboard not found", 5);
        return mainKeys;
      }

      foreach (var row in keyboardLayout.Rows)
      {
        var count = 0;
        foreach (var key in row.Keys)
        {
          if (key.Length == 0)
            key.Length = 1;

          if (!row.SpecialKey && !row.Numpad)
          {
            if (key.Code == VirtualKeyCode.CAPITAL)
            {
              var newKey = new OnScreenKey { GridRow = row.Id, GridColumn = count, Key = new TogglingModifierKey(key.KeyDisplay0, key.Code), GridWidth = new GridLength(key.Length, GridUnitType.Star), FontSizeText = FontSizeText };
              mainKeys.Add(newKey);
            }
            else if (key.Code == VirtualKeyCode.SHIFT)
            {
              var newKey = new OnScreenKey { GridRow = row.Id, GridColumn = count, Key = new InstantaneousModifierKey(key.KeyDisplay0, key.Code), GridWidth = new GridLength(key.Length, GridUnitType.Star), FontSizeText = FontSizeText };
              mainKeys.Add(newKey);
            }
            else if (!string.IsNullOrEmpty(key.KeyDisplay0) && string.IsNullOrEmpty(key.KeyDisplay1))
            {
              var newKey = new OnScreenKey { GridRow = row.Id, GridColumn = count, Key = new VirtualKey(key.Code, key.KeyDisplay0), GridWidth = new GridLength(key.Length, GridUnitType.Star), FontSizeText = FontSizeText };
              mainKeys.Add(newKey);
            }
            else if (key.KeyDisplay0.ToLower() != key.KeyDisplay1.ToLower())
            {
              var newKey = new OnScreenKey { GridRow = row.Id, GridColumn = count, Key = new ShiftSensitiveKey(key.Code, new List<string> { key.KeyDisplay0, key.KeyDisplay1 }), GridWidth = new GridLength(key.Length, GridUnitType.Star), FontSizeText = FontSizeText };
              mainKeys.Add(newKey);
            }
            else
            {
              var newKey = new OnScreenKey { GridRow = row.Id, GridColumn = count, Key = new CaseSensitiveKey(key.Code, new List<string> { key.KeyDisplay0, key.KeyDisplay1 }), GridWidth = new GridLength(key.Length, GridUnitType.Star), FontSizeText = FontSizeText };
              mainKeys.Add(newKey);
            }

            count++;
          }
        }
      }

      return mainKeys;
    }

    private ObservableCollection<OnScreenKey> GetAdditionalKey(string language)
    {
      var keyboardLayout = ConfigurationManager.Parameters.Keyboards.FirstOrDefault(s => s.Name == language);

      var specialKeys = new ObservableCollection<OnScreenKey>();

      if (keyboardLayout is null)
      {
        SnackbarTools.MainSnackbar.ShowMessageFail("Keyboard not found", 5);
        return specialKeys;
      }

      foreach (var row in keyboardLayout.Rows)
      {
        var count2 = 0;
        foreach (var key in row.Keys)
        {
          if (key.Length == 0)
            key.Length = 1;

          if (row.SpecialKey && !row.Numpad)
          {
            if (key.Code == VirtualKeyCode.CAPITAL)
            {
              var newKey = new OnScreenKey { GridRow = row.Id, GridColumn = count2, Key = new TogglingModifierKey(key.KeyDisplay0, key.Code), GridWidth = new GridLength(key.Length, GridUnitType.Star), FontSizeText = FontSizeText };
              specialKeys.Add(newKey);
            }
            else if (key.Code == VirtualKeyCode.SHIFT)
            {
              var newKey = new OnScreenKey { GridRow = row.Id, GridColumn = count2, Key = new InstantaneousModifierKey(key.KeyDisplay0, key.Code), GridWidth = new GridLength(key.Length, GridUnitType.Star), FontSizeText = FontSizeText };
              specialKeys.Add(newKey);
            }
            else if (key.ModifierCode == VirtualKeyCode.CONTROL)
            {
              var newKey = new OnScreenKey { GridRow = row.Id, GridColumn = count2, Key = new ChordKey(key.KeyDisplay0, key.ModifierCode, key.Code), GridWidth = new GridLength(key.Length, GridUnitType.Star), FontSizeText = FontSizeText };
              specialKeys.Add(newKey);
            }
            else if (!string.IsNullOrEmpty(key.KeyDisplay0) && string.IsNullOrEmpty(key.KeyDisplay1))
            {
              var newKey = new OnScreenKey { GridRow = row.Id, GridColumn = count2, Key = new VirtualKey(key.Code, key.KeyDisplay0), GridWidth = new GridLength(key.Length, GridUnitType.Star), FontSizeText = FontSizeText };
              specialKeys.Add(newKey);
            }
            else if (key.KeyDisplay0.ToLower() != key.KeyDisplay1.ToLower())
            {
              var newKey = new OnScreenKey { GridRow = row.Id, GridColumn = count2, Key = new ShiftSensitiveKey(key.Code, new List<string> { key.KeyDisplay0, key.KeyDisplay1 }), GridWidth = new GridLength(key.Length, GridUnitType.Star), FontSizeText = FontSizeText };
              specialKeys.Add(newKey);
            }
            else
            {
              var newKey = new OnScreenKey { GridRow = row.Id, GridColumn = count2, Key = new CaseSensitiveKey(key.Code, new List<string> { key.KeyDisplay0, key.KeyDisplay1 }), GridWidth = new GridLength(key.Length, GridUnitType.Star), FontSizeText = FontSizeText };
              specialKeys.Add(newKey);
            }
            count2++;
          }
        }
      }

      return specialKeys;
    }

    private ObservableCollection<OnScreenKey> GetNumpadKey(string language)
    {
      var keyboardLayout = ConfigurationManager.Parameters.Keyboards.FirstOrDefault(s => s.Name == language);

      var numKeys = new ObservableCollection<OnScreenKey>();

      if (keyboardLayout is null)
      {
        SnackbarTools.MainSnackbar.ShowMessageFail("Keyboard not found", 5);
        return numKeys;
      }

      foreach (var row in keyboardLayout.Rows)
      {
        var count = 0;
        foreach (var key in row.Keys)
        {
          if (key.Length == 0)
            key.Length = 1;

          if (!row.SpecialKey && row.Numpad)
          {
            var newKey = new OnScreenKey { GridRow = row.Id, GridColumn = count, Key = new VirtualKey(key.Code, key.KeyDisplay0) };
            numKeys.Add(newKey);

            count++;
          }
        }
      }

      return numKeys;
    }

    private void ReCreate(string language, KeyboardType keyboardType)
    {
      SetValue(FocusManager.IsFocusScopeProperty, true);
      _modifierKeys = new List<ModifierKeyBase>();
      _allLogicalKeys = new List<ILogicalKey>();
      _allOnScreenKeys = new List<OnScreenKey>();

      ColumnDefinitions.Clear();
      Children.Clear();

      _sections = new ObservableCollection<OnScreenKeyboardSection>();

      var mainSection = new OnScreenKeyboardSection();

      switch (keyboardType)
      {
        case KeyboardType.NumFullScreen:
        case KeyboardType.NumFloating:
          var numpad = GetNumpadKey(language);

          if (numpad is null)
            return;

          mainSection.Keys = numpad;
          mainSection.SetValue(ColumnProperty, 0);
          _sections.Add(mainSection);
          Children.Add(mainSection);

          _allLogicalKeys.AddRange(numpad.Select(x => x.Key));
          _allOnScreenKeys.AddRange(mainSection.Keys);
          break;

        case KeyboardType.Floating:
        case KeyboardType.FullScreen:
          var keyboard = GetPrimaryKey(language);

          if (keyboard is null)
            return;

          mainSection.Keys = keyboard;
          mainSection.SetValue(ColumnProperty, 0);
          _sections.Add(mainSection);
          Children.Add(mainSection);

          _allLogicalKeys.AddRange(keyboard.Select(x => x.Key));
          _allOnScreenKeys.AddRange(mainSection.Keys);
          break;

        case KeyboardType.FloatingAdditional:
        case KeyboardType.FullScreenAdditional:
          var mainKeys = GetPrimaryKey(language);

          if (mainKeys is null)
            return;

          mainSection.Keys = mainKeys;
          mainSection.SetValue(ColumnProperty, 0);
          _sections.Add(mainSection);
          ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(3, GridUnitType.Star) });
          Children.Add(mainSection);

          _allLogicalKeys.AddRange(mainKeys.Select(x => x.Key));
          _allOnScreenKeys.AddRange(mainSection.Keys);

          var specialSection = new OnScreenKeyboardSection();
          var specialKeys = GetAdditionalKey(language);

          specialSection.Keys = specialKeys;
          specialSection.SetValue(ColumnProperty, 1);
          _sections.Add(specialSection);
          ColumnDefinitions.Add(new ColumnDefinition());
          Children.Add(specialSection);

          _allLogicalKeys.AddRange(specialKeys.Select(x => x.Key));
          _allOnScreenKeys.AddRange(specialSection.Keys);

          _modifierKeys.AddRange(_allLogicalKeys.OfType<ModifierKeyBase>());
          break;

        case KeyboardType.NumFloatingAdditional:
        case KeyboardType.NumFullScreenAdditional:
          var numpadkey = GetNumpadKey(language);

          if (numpadkey is null)
            return;

          mainSection.Keys = numpadkey;
          mainSection.SetValue(ColumnProperty, 0);
          _sections.Add(mainSection);
          ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(3, GridUnitType.Star) });
          Children.Add(mainSection);

          _allLogicalKeys.AddRange(numpadkey.Select(x => x.Key));
          _allOnScreenKeys.AddRange(mainSection.Keys);

          var additionalSection = new OnScreenKeyboardSection();
          var additionalKeys = GetAdditionalKey(language);

          additionalSection.Keys = additionalKeys;
          additionalSection.SetValue(ColumnProperty, 1);
          _sections.Add(additionalSection);
          ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(2, GridUnitType.Star) });
          Children.Add(additionalSection);

          _allLogicalKeys.AddRange(additionalKeys.Select(x => x.Key));
          _allOnScreenKeys.AddRange(additionalSection.Keys);

          _modifierKeys.AddRange(_allLogicalKeys.OfType<ModifierKeyBase>());
          break;
      }

      _allOnScreenKeys.ForEach(x => x.OnScreenKeyPress += OnScreenKeyPress);

      _allOnScreenKeys.ForEach(x => x.SetBinding(OnScreenKey.FontSizeTextProperty, new Binding("FontSizeText") { Source = FontSizeText }));

      SynchroniseModifierKeyState();
    }

    private void SynchroniseModifierKeyState()
    {
      _modifierKeys.ToList().ForEach(x => x.SynchroniseKeyState());
    }

    private void ResetInstantaneousModifierKeys()
    {
      _modifierKeys.OfType<InstantaneousModifierKey>().ToList().ForEach(x => { if (x.IsInEffect) x.Press(); });
    }

    #endregion METODI
  }
}