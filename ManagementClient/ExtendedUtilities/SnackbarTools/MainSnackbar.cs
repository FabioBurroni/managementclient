﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ExtendedUtilities.SnackbarTools
{
  public static class MainSnackbar
  {
		public static Snackbar control;

		#region EVENTI INTERFACCIA
		/// <summary>
		/// Show Generic Grey info SnackBar
		/// </summary>
		/// <param name="text"> Text to display</param>
		/// <param name="second"> Duration </param>
		public static void ShowMessageInfo(string text, int second)
		{
			control.Background = Brushes.DarkSlateGray;

			control.MessageQueue?.Enqueue(
							 text,
							 null,
							 null,
							 null,
							 false,
							 true,
							 TimeSpan.FromSeconds(second));
		}

		/// <summary>
		/// Show Ok info  green SnackBar
		/// </summary>
		/// <param name="text"> Text to display</param>
		/// <param name="second"> Duration </param>
		public static void ShowMessageOk(string text, int second)
		{
			control.Background = Brushes.ForestGreen;

			control.MessageQueue?.Enqueue(
							 text,
							 null,
							 null,
							 null,
							 false,
							 true,
							 TimeSpan.FromSeconds(second));
		}


		/// <summary>
		/// Show failure info red SnackBar
		/// </summary>
		/// <param name="text"> Text to display</param>
		/// <param name="second"> Duration </param>
		public static void ShowMessageFail(string text, int second)
		{
			control.Background = Brushes.Red;

			control.MessageQueue?.Enqueue(
							 text,
							 null,
							 null,
							 null,
							 false,
							 true,
							 TimeSpan.FromSeconds(second));
		}

		#endregion

	}
}
