﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using System.Windows.Interactivity;

namespace ExtendedUtilities.Wpf
{
	/// <summary>
	/// Sets the designated property to the supplied value. TargetObject
	/// optionally designates the object on which to set the property. If
	/// TargetObject is not supplied then the property is set on the object
	/// to which the trigger is attached.
	/// </summary>
	public class SetPropertyAction : TriggerAction<FrameworkElement>
	{
		#region PropertyName DependencyProperty

		/// <summary>
		/// The property to be executed in response to the trigger.
		/// </summary>
		public string PropertyName
		{
			get { return (string)GetValue(PropertyNameProperty); }
			set { SetValue(PropertyNameProperty, value); }
		}

		public static readonly DependencyProperty PropertyNameProperty = DependencyProperty.Register("PropertyName", typeof(string), typeof(SetPropertyAction));

		#endregion

		#region PropertyValue DependencyProperty

		/// <summary>
		/// The value to set the property to.
		/// </summary>
		public object PropertyValue
		{
			get { return GetValue(PropertyValueProperty); }
			set { SetValue(PropertyValueProperty, value); }
		}

		public static readonly DependencyProperty PropertyValueProperty = DependencyProperty.Register("PropertyValue", typeof(object), typeof(SetPropertyAction));

		#endregion

		#region TargetObject DependencyProperty

		/// <summary>
		/// Specifies the object upon which to set the property.
		/// </summary>
		public object TargetObject
		{
			get { return GetValue(TargetObjectProperty); }
			set { SetValue(TargetObjectProperty, value); }
		}

		public static readonly DependencyProperty TargetObjectProperty = DependencyProperty.Register("TargetObject", typeof(object), typeof(SetPropertyAction));

		#endregion

		// Private Implementation.

		//protected override void Invoke(object parameter)
		//{
		//	object target = TargetObject ?? AssociatedObject;
		//	PropertyInfo propertyInfo = target.GetType().GetProperty(
		//		 PropertyName,
		//		 BindingFlags.Instance | BindingFlags.Public
		//		 | BindingFlags.NonPublic | BindingFlags.InvokeMethod);

		//	propertyInfo.SetValue(target, PropertyValue);
		//}

		protected override void Invoke(object parameter)
		{
			var target = TargetObject ?? AssociatedObject;

			var targetType = target.GetType();

			var property = targetType.GetProperty(PropertyName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);
			if (property == null)
				throw new ArgumentException($"Property not found: {PropertyName}");

			if (property.CanWrite == false)
				throw new ArgumentException($"Property is not settable: {PropertyName}");

			object convertedValue;

			if (PropertyValue == null)
				convertedValue = null;

			else
			{
				var valueType = PropertyValue.GetType();
				var propertyType = property.PropertyType;

				if (valueType == propertyType)
					convertedValue = PropertyValue;

				else
				{
					var propertyConverter = TypeDescriptor.GetConverter(propertyType);

					if (propertyConverter.CanConvertFrom(valueType))
						convertedValue = propertyConverter.ConvertFrom(PropertyValue);

					else if (valueType.IsSubclassOf(propertyType))
						convertedValue = PropertyValue;

					else
						throw new ArgumentException($"Cannot convert type '{valueType}' to '{propertyType}'.");
				}
			}

			property.SetValue(target, convertedValue);
		}
	}
}