﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ExtendedUtilities.Tasks
{
	#region Class CustomTask

	internal class CustomTask : ICustomTask
	{
		private readonly CancellationTokenSource _cancellationToken;
		private Task _task;

		public CustomTask(Action action, int pollInterval, bool isLongRunning)
		{
			_cancellationToken = new CancellationTokenSource();

			if (isLongRunning)
				CreateTask(action, pollInterval, TaskCreationOptions.LongRunning);
			else
				CreateTask(action, pollInterval, TaskCreationOptions.PreferFairness);
		}

		private void CreateTask(Action action, int pollInterval, TaskCreationOptions taskCreationOptions)
		{
			if (_task != null)
				return;

			_task = Task.Factory.StartNew(
			 () =>
			 {
				 while (!_cancellationToken.Token.IsCancellationRequested)
				 {
					 action();
					 _cancellationToken.Token.WaitHandle.WaitOne(pollInterval);
				 }
			 }, _cancellationToken.Token, taskCreationOptions, TaskScheduler.Default);
		}

		public void CancelTask()
		{
			_cancellationToken.Cancel();
		}
	}

	#endregion

	#region Class CustomTask<TAction>

	public class CustomTask<TAction> : ICustomTask
	{
		private readonly CancellationTokenSource _cancellationToken;
		private Task _task;

		public CustomTask(Action<TAction> action, TAction parameter, int pollInterval, bool isLongRunning)
		{
			_cancellationToken = new CancellationTokenSource();

			if (isLongRunning)
				CreateTask(action, parameter, pollInterval, TaskCreationOptions.LongRunning);
			else
				CreateTask(action, parameter, pollInterval, TaskCreationOptions.PreferFairness);
		}

		private void CreateTask(Action<TAction> action, TAction parameter, int pollInterval, TaskCreationOptions taskCreationOptions)
		{
			if (_task != null)
				return;

			_task = Task.Factory.StartNew(
			 () =>
			 {
				 while (!_cancellationToken.Token.IsCancellationRequested)
				 {
					 action(parameter);
					 _cancellationToken.Token.WaitHandle.WaitOne(pollInterval);
				 }
			 }, _cancellationToken.Token, taskCreationOptions, TaskScheduler.Default);
		}

		public void CancelTask()
		{
			_cancellationToken.Cancel();
		}
	}

	#endregion
}