﻿using System;
using System.Collections.Generic;
using Utilities.Locks;

namespace ExtendedUtilities.Tasks
{
  public class TaskManager
  {
    private static readonly Lazy<TaskManager> _instance = new Lazy<TaskManager>(() => new TaskManager());

    private readonly Dictionary<object, Dictionary<Delegate, ICustomTask>> _customTasksDictionary = new Dictionary<object, Dictionary<Delegate, ICustomTask>>();
    private readonly ReaderWriterLockSlimManager _readerWriterLock = new ReaderWriterLockSlimManager();

    // Private to prevent direct instantiation
    private TaskManager()
    {
    }

    public static TaskManager Instance
    {
      get
      {
        return _instance.Value;
      }
    }

    #region AddTask

    /// <summary>
    /// Aggiunge un task da eseguire
    /// </summary>
    /// <param name="instance">Istanza dell'oggetto che richiede il task</param>
    /// <param name="action">Azione da eseguire</param>
    /// <param name="timeout">Ogni quanto tempo viene ripetuto il task</param>
    public void AddTask(object instance, Action action, TimeSpan timeout)
    {
      AddTask(instance, action, timeout, false);
    }

    /// <summary>
    /// Aggiunge un task da eseguire
    /// </summary>
    /// <param name="instance">Istanza dell'oggetto che richiede il task</param>
    /// <param name="action">Azione da eseguire</param>
    /// <param name="millisecond">Ogni quanti millisecondi viene ripetuto il task</param>
    public void AddTask(object instance, Action action, int millisecond)
    {
      AddTask(instance, action, millisecond, false);
    }

    /// <summary>
    /// Aggiunge un task da eseguire
    /// </summary>
    /// <param name="instance">Istanza dell'oggetto che richiede il task</param>
    /// <param name="action">Azione da eseguire</param>
    /// <param name="timeout">Ogni quanto tempo viene ripetuto il task</param>
    /// <param name="isLongTask">Specifica se è un task che durerà molto, oppure sarà di breve durata</param>
    public void AddTask(object instance, Action action, TimeSpan timeout, bool isLongTask)
    {
      AddTask(instance, action, (int)timeout.TotalMilliseconds, isLongTask);
    }

    /// <summary>
    /// Aggiunge un task da eseguire
    /// </summary>
    /// <param name="instance">Istanza dell'oggetto che richiede il task</param>
    /// <param name="action">Azione da eseguire</param>
    /// <param name="millisecond">Ogni quanti millisecondi viene ripetuto il task</param>
    /// <param name="isLongTask">Specifica se è un task che durerà molto, oppure sarà di breve durata</param>
    public void AddTask(object instance, Action action, int millisecond, bool isLongTask)
    {
      if (millisecond <= 0)
        throw new ArgumentOutOfRangeException("millisecond", "Value must be greater than zero");

      //Il dizionario contiene già l'istanza e l'azione => esco
      if (IsActionContained(instance, action))
        return;

      var customTask = new CustomTask(action, millisecond, isLongTask);
      AddToDictionary(instance, action, customTask);
    }

    #endregion

    #region AddTask<TAction>

    /// <summary>
    /// Aggiunge un task parametrizzato da eseguire
    /// </summary>
    /// <typeparam name="TAction">Tipo del parametro</typeparam>
    /// <param name="instance">Istanza dell'oggetto che richiede il task</param>
    /// <param name="action">Azione da eseguire</param>
    /// <param name="parameter">Parametro da eseguire nell'azione</param>
    /// <param name="timeout">Ogni quanto tempo viene ripetuto il task</param>
    public void AddTask<TAction>(object instance, Action<TAction> action, TAction parameter, TimeSpan timeout)
    {
      AddTask<TAction>(instance, action, parameter, timeout, false);
    }

    /// <summary>
    /// Aggiunge un task parametrizzato da eseguire
    /// </summary>
    /// <typeparam name="TAction">Tipo del parametro</typeparam>
    /// <param name="instance">Istanza dell'oggetto che richiede il task</param>
    /// <param name="action">Azione da eseguire</param>
    /// <param name="parameter">Parametro da eseguire nell'azione</param>
    /// <param name="millisecond">Ogni quanti millisecondi viene ripetuto il task</param>
    public void AddTask<TAction>(object instance, Action<TAction> action, TAction parameter, int millisecond)
    {
      AddTask<TAction>(instance, action, parameter, millisecond, false);
    }

    /// <summary>
    /// Aggiunge un task parametrizzato da eseguire
    /// </summary>
    /// <typeparam name="TAction">Tipo del parametro</typeparam>
    /// <param name="instance">Istanza dell'oggetto che richiede il task</param>
    /// <param name="action">Azione da eseguire</param>
    /// <param name="parameter">Parametro da eseguire nell'azione</param>
    /// <param name="timeout">Ogni quanto tempo viene ripetuto il task</param>
    /// /// <param name="isLongTask">Specifica se è un task che durerà molto, oppure sarà di breve durata</param>
    public void AddTask<TAction>(object instance, Action<TAction> action, TAction parameter, TimeSpan timeout, bool isLongTask)
    {
      AddTask<TAction>(instance, action, parameter, (int)timeout.TotalMilliseconds, false);
    }

    /// <summary>
    /// Aggiunge un task parametrizzato da eseguire
    /// </summary>
    /// <typeparam name="TAction">Tipo del parametro</typeparam>
    /// <param name="instance">Istanza dell'oggetto che richiede il task</param>
    /// <param name="action">Azione da eseguire</param>
    /// <param name="parameter">Parametro da eseguire nell'azione</param>
    /// <param name="millisecond">Ogni quanti millisecondi viene ripetuto il task</param>
    /// /// <param name="isLongTask">Specifica se è un task che durerà molto, oppure sarà di breve durata</param>
    public void AddTask<TAction>(object instance, Action<TAction> action, TAction parameter, int millisecond, bool isLongTask)
    {
      if (millisecond <= 0)
        throw new ArgumentOutOfRangeException("millisecond", "Value must be greater than zero");

      //Il dizionario contiene già l'istanza e l'azione => esco
      if (IsActionContained(instance, action))
        return;

      var customTask = new CustomTask<TAction>(action, parameter, millisecond, isLongTask);
      AddToDictionary(instance, action, customTask);
    }

    #endregion

    #region RemoveTask

    /// <summary>
    /// Rimuove il task che sta eseguendo
    /// </summary>
    /// <param name="instance">Istanza dell'oggetto che sta eseguendo il task</param>
    /// <param name="action">Azione da rimuovere</param>
    public void RemoveTask(object instance, Action action)
    {
      //Se instanza ed azione sono contenute, rimuovo dal dizionario e cancello il task
      if (IsActionContained(instance, action))
      {
        var customTask = GetCustomTask(instance, action);
        customTask.CancelTask();

        RemoveFromDictionary(instance, action);
      }
    }

    #endregion

    #region RemoveTask<TAction>

    /// <summary>
    /// Rimuove il task parametrizzato che sta eseguendo
    /// </summary>
    /// <typeparam name="TAction">Tipo del parametro</typeparam>
    /// <param name="instance">Istanza dell'oggetto che sta eseguendo il task</param>
    /// <param name="action">Azione parametrizzata da rimuovere</param>
    public void RemoveTask<TAction>(object instance, Action<TAction> action)
    {
      if (IsActionContained(instance, action))
      {
        var customTask = _customTasksDictionary[instance][action];
        customTask.CancelTask();

        RemoveFromDictionary(instance, action);
      }
    }

    #endregion

    #region Dictionary

    private void AddToDictionary(object instance, Delegate action, ICustomTask customTask)
    {
      //Il dizionario non contiene l'instanza della classe => la aggiungo
      if (!IsInstanceContanied(instance))
      {
        using (_readerWriterLock.WriteLock())
        {
          _customTasksDictionary.Add(instance, new Dictionary<Delegate, ICustomTask> { { action, customTask } });
        }
        return;
      }

      //Il dizionario contiene l'instanza della classe => recupero il dizionario originale
      var innerDictionary = GetInnerDictionary(instance);

      //Controllo se l'azione è già contenuta => se non la contiene la aggiungo, altrimenti finisco
      if (!IsActionContained(innerDictionary, action))
      {
        using (_readerWriterLock.WriteLock())
        {
          innerDictionary.Add(action, customTask);
        }
      }
    }

    private bool IsActionContained(object instance, Delegate action)
    {
      using (_readerWriterLock.ReadLock())
      {
        if (instance == null || !_customTasksDictionary.ContainsKey(instance))
          return false;

        return _customTasksDictionary[instance].ContainsKey(action);
      }
    }

    private bool IsActionContained(IReadOnlyDictionary<Delegate, ICustomTask> innerDictionary, Delegate action)
    {
      using (_readerWriterLock.ReadLock())
      {
        return innerDictionary.ContainsKey(action);
      }
    }

    private bool IsInstanceContanied(object instance)
    {
      using (_readerWriterLock.ReadLock())
      {
        return instance != null && _customTasksDictionary.ContainsKey(instance);
      }
    }

    private bool IsInnerDictionaryEmpty(IReadOnlyCollection<KeyValuePair<Delegate, ICustomTask>> innerDictionary)
    {
      using (_readerWriterLock.ReadLock())
      {
        return innerDictionary != null && innerDictionary.Count == 0;
      }
    }

    private Dictionary<Delegate, ICustomTask> GetInnerDictionary(object instance)
    {
      using (_readerWriterLock.ReadLock())
      {
        return _customTasksDictionary[instance];
      }
    }

    private ICustomTask GetCustomTask(object instance, Delegate action)
    {
      using (_readerWriterLock.ReadLock())
      {
        return _customTasksDictionary[instance][action];
      }
    }

    private void RemoveFromDictionary(object instance, Delegate action)
    {
      //Il dizionario non contiene l'instanza della classe => nulla da rimuovere, esco
      if (!IsInstanceContanied(instance))
      {
        return;
      }

      //Il dizionario contiene l'instanza della classe => recupero il dizionario originale
      var innerDictionary = GetInnerDictionary(instance);

      //Controllo se l'azione è già contenuta => la rimuovo
      if (IsActionContained(innerDictionary, action))
      {
        using (_readerWriterLock.WriteLock())
        {
          innerDictionary.Remove(action);
        }

        //Controllo se il dizionario dei CustomTask è vuoto=> in questo caso rimuovo anche l'istanza
        if (IsInnerDictionaryEmpty(innerDictionary))
        {
          using (_readerWriterLock.WriteLock())
          {
            _customTasksDictionary.Remove(instance);
          }
        }
      }
    }

    #endregion
  }
}