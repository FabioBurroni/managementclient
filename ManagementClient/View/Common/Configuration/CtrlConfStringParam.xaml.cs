﻿using System.Windows;
using Model;
using Model.Common.Configuration;

namespace View.Common.Configuration
{
  /// <summary>
  /// Interaction logic for CtrlConf.xaml
  /// </summary>
  public partial class CtrlConfStringParam : CtrlBase, ICtrlConfParameter
  {
    public CtrlConfStringParam()
    {
      InitializeComponent();
    }

    #region TRADUZIONI

    protected override void Translate()
    {
      if (txtBlkTitle.Tag != null)
        txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      if (txtBlkDescr.Tag != null)
        txtBlkDescr.Text = Context.Instance.TranslateDefault((string)txtBlkDescr.Tag);
    }

    #endregion TRADUZIONI

    public ConfParameterClient ConfParameter
    {
      get { return (ConfParameterClient)GetValue(ConfParameterProperty); }
      set { SetValue(ConfParameterProperty, value); }
    }

    // Using a DependencyProperty as the backing store for ConfParameter.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ConfParameterProperty =
        DependencyProperty.Register("ConfParameter", typeof(ConfParameterClient), typeof(CtrlConfStringParam), new PropertyMetadata(null));

    public event OnConfirmHandler OnConfirm;
    public event OnCancelHandler OnCancel;

    private void butCancel_Click(object sender, RoutedEventArgs e)
    {
      OnCancel?.Invoke(ConfParameter);
    }

    private void butConfirm_Click(object sender, RoutedEventArgs e)
    {
      OnConfirm?.Invoke(ConfParameter);
    }

    private void butDefault_Click(object sender, RoutedEventArgs e)
    {
      ConfParameter.SetDefault();
    }

    private void CtrlBase_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
  }
}
