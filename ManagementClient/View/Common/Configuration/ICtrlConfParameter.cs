﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Model.Common.Configuration;

namespace View.Common.Configuration
{
  public delegate void OnConfirmHandler(ConfParameterClient confPar);
  public delegate void OnCancelHandler(ConfParameterClient confPar);
  public interface ICtrlConfParameter
  {
    event OnConfirmHandler OnConfirm;
    event OnCancelHandler OnCancel;

    ConfParameterClient ConfParameter { get; set; }
  }
}
