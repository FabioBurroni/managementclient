﻿ using System;
using System.Windows;
using System.Windows.Input;
using ExtendedUtilities.Wpf;

namespace View.Common.Configuration
{
  /// <summary>
  /// Interaction logic for CtrlConfirmAdminOperation.xaml
  /// </summary>
  public partial class CtrlConfirmAdminOperation : Window
  {
    #region Field

    private ICommand _confirmCommand;

    #endregion

    #region Property

    public string Username { get; private set; }
    public string Password { get; private set; }

    #region public string OptionalMessage

    /// <summary>
    /// Identifies the OptionalMessage dependency property.
    /// </summary>
    public static DependencyProperty OptionalMessageProperty =
      DependencyProperty.Register("OptionalMessage", typeof(string), typeof(CtrlConfirmAdminOperation), null);

    /// <summary>
    /// 
    /// </summary>
    public string OptionalMessage
    {
      get { return (string)GetValue(OptionalMessageProperty); }
      set { SetValue(OptionalMessageProperty, value); }
    }

    #endregion public string OptionalMessage

    #endregion

    #region Constructor

    public CtrlConfirmAdminOperation()
    {
      InitializeComponent();

      //var isPowerUser = Configurations.Instance.IsPowerUserLogged;
      var isPowerUser = true;

      //se l'utente loggato è PowerUser, mostro username e password, altrimenti no
      PowerUserRow.Height = isPowerUser ? new GridLength(0) : new GridLength(1, GridUnitType.Star);
      Height = isPowerUser ? 230 : 430;

      TxtUserName.Focus();
    }

    public CtrlConfirmAdminOperation(string optionalMessage)
      : this()
    {
      OptionalMessage = optionalMessage;
    }

    #endregion

    #region Command

    public ICommand ConfirmCommand
    {
      get
      {
        return _confirmCommand ?? (_confirmCommand = new RelayCommand<object[]>(Confirm));
      }
    }

    #endregion

    #region Private Method

    private void Confirm(object[] confirmParameters)
    {
      if (confirmParameters == null || confirmParameters.Length != 3)
        return;

      var confirm = Convert.ToBoolean(confirmParameters[0]);

      var username = confirmParameters[1].ToString();
      var password = confirmParameters[2].ToString();

      //li memorizzo solo se entrambi hanno un valore
      if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
      {
        Username = username;

        //converto in base 64 la stringa //EDIT: la metto base 64 a basso livello
        //Password = Convert.ToBase64String(Encoding.UTF8.GetBytes(password));
        Password = password;
      }

      //metto il risultato della conferma della dialog
      DialogResult = confirm;

      Close();
    }

    #endregion
  }
}