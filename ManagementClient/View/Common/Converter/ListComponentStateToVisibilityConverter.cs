﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
using Model.Custom.C200318.OrderManager;
namespace View.Common.Converter
{
  public class ListComponentStateToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      Visibility ret = Visibility.Collapsed;
      string par = parameter as string;
      if (value is C200318_State)
      {
      
        switch((C200318_State)value)
        {
          case C200318_State.LDONE:
          case C200318_State.LEDIT:
          case C200318_State.LKILL:
          case C200318_State.LUNCOMPLETE:
            break;
          case C200318_State.LEXEC:
            if (par == "pause" || par=="kill" || par=="refresh")
              ret = Visibility.Visible;
            break;
          case C200318_State.LWAIT:
            if (par == "kill" || par=="exec")
              ret = Visibility.Visible;
            break;
          case C200318_State.LPAUSE:
            if (par == "exec" || par == "kill" || par == "refresh")
              ret = Visibility.Visible;
            break;

          case C200318_State.CDONE:
          case C200318_State.CKILL:
          case C200318_State.CPAUSE:
            break;
          case C200318_State.CEXEC:
          case C200318_State.CWAIT:
            if (par == "kill")
              ret = Visibility.Visible;
            break;
        }
      }
      return ret;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
