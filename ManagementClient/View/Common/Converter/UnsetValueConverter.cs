﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace View.Common.Converter
{
	public class UnsetValueConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return DependencyProperty.UnsetValue;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}
	}
}