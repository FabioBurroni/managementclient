﻿using System;
using System.Globalization;
using System.Windows.Data;
using Model.Common;

namespace View.Common.Converter
{
  public class ResultToLocalizedStringConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      try
      {
        if ((value is Results_Enum)&&(value != null))
        {
          string strToReturn = string.Empty;

          string descrToReturn = Enum.GetName(typeof(Results_Enum), value);
          if (!string.IsNullOrEmpty(descrToReturn))
            strToReturn += Localization.Localize.LocalizeDefaultResultString(descrToReturn);

          int valNumber = (int)value;
          if (valNumber != 1)// 1 is ok... 
            strToReturn += "(" + ((int)value).ToString() + ") ";

          return strToReturn;
        }
      }
      catch(Exception Ex)
      {
        string exc = Ex.ToString();
      }

      return "";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    public static object Convert(object value)
    {
      return new ResultToLocalizedStringConverter().Convert(value, null, null, CultureInfo.CurrentCulture);
    }
  }
}
