﻿using System;
using System.Globalization;
using System.Windows.Data;
namespace View.Common.Converter
{
  public class ResultStringLocalizedStringConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is string)
      {
        string result = (string)value;
        return Localization.Localize.LocalizeDefaultResultString(result);
      }

      return "";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    public static object Convert(object value)
    {
      return new ResultStringLocalizedStringConverter().Convert(value, null, null, CultureInfo.CurrentCulture);
    }
  }
}
