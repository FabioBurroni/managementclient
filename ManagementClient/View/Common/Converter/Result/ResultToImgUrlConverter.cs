﻿using System;
using System.Globalization;
using System.Windows.Data;
using Model.Common;

namespace View.Common.Converter
{ 
  public class ResultToImgUrlConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {

      string s = "/View;component/Common/Images/";

      try
      {      

        if (value != null)
        {
          Results_Enum re = (Results_Enum)value;


          switch (re)
          {
            case Results_Enum.UNDEFINED: return s + "warning.png";
            case Results_Enum.OK: return s + "ok.png";

            case Results_Enum.DATABASE_NOT_CONNECTED: return s + "Result/database.png";
            case Results_Enum.STORED_PROCEDURE_EXCEPTION: return s + "Result/database.png";
            case Results_Enum.QUERY_EXCEPTION: return s + "Result/database.png";
            case Results_Enum.COMMUNICATION_TIMEOUT: return s + "warning_red.png";
            case Results_Enum.COMMUNICATION_COMCOUNTERS_NOT_ALIGNED: return s + "warning_red.png";
            case Results_Enum.COMMUNICATION_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.COMMUNICATION_DATA_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.PARAMETER_NUMBER_NOK: return s + "warning_red.png";
            case Results_Enum.EXCEPTION_EXECUTING_COMMAND: return s + "warning_red.png";
            case Results_Enum.POSITION_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.POSITION_HAS_NO_COMM: return s + "warning_red.png";
            case Results_Enum.NO_REACHABLE_AREA: return s + "warning_red.png";
            case Results_Enum.SHAPECONTROL_WIDTH_NOT_VALID: return s + "palletWidthError.png";
            case Results_Enum.SHAPECONTROL_DEPTH_NOT_VALID: return s + "palletDepthError.png";
            case Results_Enum.SHAPECONTROL_HEIGHT_NOT_VALID: return s + "palletHeightError.png";
            case Results_Enum.SHAPECONTROL_WEIGHT_NOT_VALID: return s + "weight_red.png";
            case Results_Enum.SHAPECONTROL_PRODUCT_NOT_VALID: return s + "Result/package_unknown.png";
            case Results_Enum.SHAPECONTROL_ARTICLE_NOT_VALID: return s + "Result/package_error.png";
            case Results_Enum.SHAPECONTROL_PALLET_NOT_VALID: return s + "Result/pallet_top_error.png";
            case Results_Enum.SHAPECONTROL_POSITION_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.SHAPECONTROL_NO_AVAILABLE_CELL: return s + "warning_red.png";
            case Results_Enum.SHAPECONTROL_NO_AVAILABLE_AREA: return s + "warning_red.png";
            case Results_Enum.SHAPECONTROL_NO_REACHABLE_AREA: return s + "warning_red.png";
            case Results_Enum.SHAPECONTROL_PALLET_IN_WH: return s + "warning_red.png";
            case Results_Enum.SHAPECONTROL_PALLET_FOR_ANTICIPATED_EXIT: return s + "warning_red.png";
            case Results_Enum.SHAPECONTROL_LABEL_NOT_VALID: return s + "Result/barcode_error.png";
            case Results_Enum.SHAPECONTROL_PALLETTYPE_NOT_VALID: return s + "Result/pallet_top_error.png";
            case Results_Enum.SHAPECONTROL_SLAT_NOT_COMPLIANT: return s + "warning_red.png";
            case Results_Enum.SHAPECONTROL_GENERIC_ERROR: return s + "warning_red.png";
            case Results_Enum.ENGINE_SOURCE_JOB_ALREADY_PRESENT: return s + "warning_red.png";
            case Results_Enum.ENGINE_SOURCE_NO_CONDITION_TO_CREATE_JOB: return s + "warning_red.png";
            case Results_Enum.ENGINE_SOURCE_ERROR: return s + "warning_red.png";
            case Results_Enum.ENGINE_SOURCE_PALLET_WITHOUT_FINALDEST: return s + "warning_red.png";
            case Results_Enum.ENGINE_SOURCE_NO_REACHABLE_AREA: return s + "warning_red.png";
            case Results_Enum.ENGINE_SOURCE_NO_AVAILABLE_CELL: return s + "warning_red.png";
            case Results_Enum.ENGINE_SOURCE_PALLET_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.ENGINE_SOURCE_PALLET_NOT_MAPPED_IN_POSITION: return s + "warning_red.png";
            case Results_Enum.ENGINE_SOURCE_POSITION_IS_DISABLED: return s + "warning_red.png";
            case Results_Enum.CELL_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.AREA_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.SOURCE_CELL_IS_NOT_EMPTY: return s + "warning_red.png";
            case Results_Enum.SOURCE_CELL_IS_EMPTY: return s + "warning_red.png";
            case Results_Enum.DESTINATION_CELL_IS_NOT_EMPTY: return s + "warning_red.png";
            case Results_Enum.DESTINATION__CELL_IS_EMPTY: return s + "warning_red.png";
            case Results_Enum.PALLET_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.SLAVE_CANNOT_MAPPED_IN_MASTER: return s + "warning_red.png";
            case Results_Enum.SLAVE_CANNOT_MAPPED_IN_POSITION: return s + "warning_red.png";
            case Results_Enum.MASTER_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.MASTER_CANNOT_EXECUTE_COMMAND: return s + "warning_red.png";
            case Results_Enum.MASTER_NOT_IN_MANUAL_STATE: return s + "warning_red.png";
            case Results_Enum.MASTER_HAS_A_SLAVE_ON_BOARD: return s + "warning_red.png";
            case Results_Enum.PALLET_GEOMETRY_NOT_COMPLIANT: return s + "warning_red.png";
            case Results_Enum.CELL_CANNOT_HOST_PALLET: return s + "warning_red.png";
            case Results_Enum.SAT_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.SAT_STATUS_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.SAT_ERROR_UPDATING_STATUS: return s + "warning_red.png";
            case Results_Enum.JOB_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.JOB_ID_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.JOB_STATUS_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.JOB_ERROR_UPDATING_STATUS: return s + "warning_red.png";
            case Results_Enum.JOB_POSSOURCE_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.JOB_POSDEST_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.JOB_JOBTYPE_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.JOB_ERROR_INSERTING: return s + "warning_red.png";
            case Results_Enum.JOB_MASTER_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.JOB_SLAVESOURCE_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.JOB_SLAVEDEST_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.JOB_ERROR_UPDATING: return s + "warning_red.png";
            case Results_Enum.JOB_PALLET_STATUS_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.JOB_PALLET_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.JOB_PALLET_ERROR_INSERTING: return s + "warning_red.png";
            case Results_Enum.JOB_PALLET_ALREADY_PRESENT: return s + "warning_red.png";
            case Results_Enum.JOB_CANNOT_CREATE_NEW_JOB: return s + "warning_red.png";
            case Results_Enum.JOB_ERROR_CREATING_NEW_JOB: return s + "warning_red.png";
            case Results_Enum.JOB_ALREADY_PRESENT: return s + "warning_red.png";
            case Results_Enum.CMD_JOB_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CMD_CMDTYPE_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CMD_PALLET_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CMD_SAT_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CMD_POSITION_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CMD_WAITSTATE_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CMD_CMDRESULT_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CMD_ERROR_INSERT: return s + "warning_red.png";
            case Results_Enum.CMD_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CMD_ERROR_UPDATING: return s + "warning_red.png";
            case Results_Enum.CMD_STATUS_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CMD_ERROR_STATUS_UPDATING: return s + "warning_red.png";
            case Results_Enum.CMD_ALREADY_ACKNOWLEDGE: return s + "warning_red.png";
            case Results_Enum.SOURCE_CELL_RESERVED: return s + "warning_red.png";
            case Results_Enum.DESTINATION_CELL_RESERVED: return s + "warning_red.png";
            case Results_Enum.LM_ARTICLE_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.LM_CANNOT_CREATE_JOB: return s + "warning_red.png";
            case Results_Enum.LM_FINALDEST_ERROR: return s + "warning_red.png";
            case Results_Enum.LM_DESTINATION_CANNOT_HOST_PALLET: return s + "warning_red.png";
            case Results_Enum.LM_NO_LCP_TO_SERVE: return s + "warning_red.png";
            case Results_Enum.LM_DESTINATION_DISABLED: return s + "warning_red.png";
            case Results_Enum.LM_NO_LIST_TO_SERVE: return s + "warning_red.png";
            case Results_Enum.LM_CMP_NOT_SCANNED: return s + "warning_red.png";
            case Results_Enum.LM_PRELOADLINE_REACHED_MAX_PALLET_IN_TRANIST: return s + "warning_red.png";
            case Results_Enum.LM_DESTINATION_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.LM_ONDA_IS_FULL: return s + "warning_red.png";
            case Results_Enum.LM_ONDA_DEST_POS_HAS_MAX_PALLET_IN_TRANSIT: return s + "warning_red.png";
            case Results_Enum.LM_WHOUTPUT_DESTINATION_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.LM_ONDA_ITEM_COMPLETE: return s + "warning_red.png";
            case Results_Enum.LM_ONDA_REACHED_MAX_ITEM_OPENED: return s + "warning_red.png";
            case Results_Enum.LM_ONDA_ITEM_NO_PALLET_SELECTABLE: return s + "warning_red.png";
            case Results_Enum.LM_ONDA_ITEM_ERROR_CREATING_JOB: return s + "warning_red.png";
            case Results_Enum.LM_ONDA_NO_ITEM_TO_SERVE: return s + "warning_red.png";
            case Results_Enum.LM_ARTICLE_TEMPORARILY_NOT_AVAILABLE: return s + "warning_red.png";
            case Results_Enum.LM_LCP_SERVED: return s + "warning_red.png";
            case Results_Enum.LM_NO_AVAILABLE_MASTER: return s + "warning_red.png";
            case Results_Enum.LM_ERROR_PRELOAD_LINE_CANNOT_BE_ASSIGNED: return s + "warning_red.png";
            case Results_Enum.LM_PRELOAD_LINE_CANNOT_HOST_PALLET: return s + "warning_red.png";
            case Results_Enum.LM_MASTER_MAX_WORKLOAD: return s + "warning_red.png";
            case Results_Enum.JM_PARKING_NO_AVAILABLE_PARK: return s + "warning_red.png";
            case Results_Enum.JM_PARKING_SAT_ALREADY_PARKED: return s + "warning_red.png";
            case Results_Enum.JM_PARKING_ERROR_CREATING_JOB: return s + "warning_red.png";
            case Results_Enum.JM_PARKING_ERROR_UNKNOWN_SOURCE_POSITION_FOR_SLAVE: return s + "warning_red.png";
            case Results_Enum.JM_PARKING_SAT_HAS_ALREADY_PARKING_JOB: return s + "warning_red.png";
            case Results_Enum.JM_PARKING_DESTINATION_HAS_ALREADY_SLAVE: return s + "warning_red.png";
            case Results_Enum.JM_PARKING_DESTINATION_HAS_ALREADY_JOB: return s + "warning_red.png";
            case Results_Enum.JM_PARKING_DESTINATION_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.JM_PARKING_SLAVE_IS_CHARGING: return s + "warning_red.png";
            case Results_Enum.JM_CHARGING_NO_AVAILABLE_CHARGER_POSITION: return s + "warning_red.png";
            case Results_Enum.JM_CHARGING_SAT_ALREADY_IN_CHARGER_POSITION: return s + "warning_red.png";
            case Results_Enum.JM_CHARGING_ERROR_CREATING_JOB: return s + "warning_red.png";
            case Results_Enum.JM_CHARGING_ERROR_UNKNOWN_SOURCE_POSITION_FOR_SLAVE: return s + "warning_red.png";
            case Results_Enum.JM_CHARGING_SAT_HAS_ALREADY_CHARGING_JOB: return s + "warning_red.png";
            case Results_Enum.JM_CHARGING_NO_SAT_ON_POSITION_CHARGER: return s + "warning_red.png";
            case Results_Enum.JM_CHARGING_CHARGER_POSITION_ALREADY_ON: return s + "warning_red.png";
            case Results_Enum.JM_CHARGING_CHARGER_POSITION_ALREADY_OFF: return s + "warning_red.png";
            case Results_Enum.SERVICE_PALLET_NO_AVAILABLE: return s + "warning_red.png";
            case Results_Enum.SERVICE_PALLET_ERROR_CREATING_NEW: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_PALLET_IN_WH: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_PALLET_IN_HANDLING: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_PALLET_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_SHAPECONTROL_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_NO_PALLET_PRESENT_IN_POSITION: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_COM_TIMEOUT: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_COM_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_PALLET_ALREDY_PRESENT: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_PALLET_PRODUCT_GROUP_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ARTICLE_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CHECKIN_PALLETTYPE_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_CREATING_PRODUCT: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_CREATING_PALLET: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_INSERTING_PRODUCT: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_UPDATATING_MAPPING: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_DATE_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_QTY_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_ARTICLE_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_QTY_EXCEED_STANDARD: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_CREATEPALLETDB: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_EVACUATION: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_PALLETINPOS: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_PALLET_ALREADY_IN_MAP: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_PRINTER: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_PRINTER_UNDEFINED: return s + "warning_red.png";
            case Results_Enum.CHECKIN_ERROR_BATCH_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.POSITION_ERROR_FRESENIUS_PRINTER: return s + "warning_red.png";
            case Results_Enum.POSITION_ERROR_ODMS_PRINTER: return s + "warning_red.png";
            case Results_Enum.POSITION_ERROR_FRESENIUS_PRINTER_UNDEFINED: return s + "warning_red.png";
            case Results_Enum.POSITION_ERROR_ODMS_PRINTER_UNDEFINED: return s + "warning_red.png";
            case Results_Enum.POSITION_ERROR_FRESENIUS_PRINTER_LIST_EMPTY: return s + "warning_red.png";
            case Results_Enum.POSITION_ERROR_ODMS_PRINTER_LIST_EMPTY: return s + "warning_red.png";
            case Results_Enum.OM_DESTINATION_POSITION_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.OM_LISTTYPE_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.OM_WAITSTATE_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.OM_KILLSTATE_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.OM_USER_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_INSERTING_ORDER: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_INSERTING_DESTINATION: return s + "warning_red.png";
            case Results_Enum.OM_CMP_WAITSTATE_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.OM_ARTICLE_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.OM_QTY_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_INSERTING_CMP: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_INSERTING_ASE: return s + "warning_red.png";
            case Results_Enum.OM_ORDER_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.OM_ORDER_NOT_IN_WAIT_SATATE: return s + "warning_red.png";
            case Results_Enum.OM_DESTINATION_1_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.OM_DESTINATION_2_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_CHANGING_DESTINATION: return s + "warning_red.png";
            case Results_Enum.OM_CHANGE_STATE_NOT_ALLOWED: return s + "warning_red.png";
            case Results_Enum.OM_CHANGE_STATE_ERROR: return s + "warning_red.png";
            case Results_Enum.OM_EXECUTION_MODALITY_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.OM_CHANGE_EXECUTION_MODALITY_ERROR: return s + "warning_red.png";
            case Results_Enum.OM_CMP_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.OM_CMP_STATE_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.OM_CMP_CHANGE_STATE_ERROR: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_ASSIGNING_PRELOAD_LINE: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_PRELOAD_LINE_HAS_ORDER_ASSIGNED: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_PRELOAD_LINE_NOT_ASSIGNED: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_SEQUENCE_NUMBER_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_INSERTING_LIST_INFO: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_UPDATING_ARTICLE: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_UPDATING_ARTICLE_INFO: return s + "warning_red.png";
            case Results_Enum.OM_ARTICLE_NOT_DELETABLE: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_DELETING_ARTICLE_PALLET_IN_SYSTEM: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_DELETING_ARTICLE_ORDERS_IN_SYSTEM: return s + "warning_red.png";
            case Results_Enum.OM_ERROR_DELETING_ARTICLE: return s + "warning_red.png";
            case Results_Enum.PL_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.PL_HAS_EXECUTING_ORDER: return s + "warning_red.png";
            case Results_Enum.PL_HAS_PALLET_IN_TRANSIT: return s + "warning_red.png";
            case Results_Enum.PL_NO_ORDER_ASSIGNED: return s + "warning_red.png";
            case Results_Enum.PL_HAS_SLAVE_ON_LANE: return s + "warning_red.png";
            case Results_Enum.PALLET_MANUAL_EXPELLED: return s + "warning_red.png";
            case Results_Enum.PALLET_MANUAL_REMOVED: return s + "warning_red.png";
            case Results_Enum.PALLET_AUTO_EXPELLED: return s + "warning_red.png";
            case Results_Enum.EXPORT_PALLET_OUT_ERROR: return s + "warning_red.png";
            case Results_Enum.EXPORT_PALLET_REJECT_ERROR: return s + "warning_red.png";
            case Results_Enum.EXPORT_PALLET_IN_ERROR: return s + "warning_red.png";
            case Results_Enum.EXPORT_ORDER_ALREADY_EXPORTED: return s + "warning_red.png";
            case Results_Enum.EXPORT_ORDER_ERROR: return s + "warning_red.png";
            case Results_Enum.SCHEDULE_ENDTIME_SMALL_STARTTIME: return s + "warning_red.png";
            case Results_Enum.SCHEDULE_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.SCHEDULE_IS_OVERLAPPED_TO_ANOTHER: return s + "warning_red.png";
            case Results_Enum.SCHEDULE_ERROR_INSERT: return s + "warning_red.png";
            case Results_Enum.LOGIN_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.LOGIN_ERROR_UPDATING_USER: return s + "warning_red.png";
            case Results_Enum.LOGIN_ERROR_INSERTING_USER: return s + "warning_red.png";
            case Results_Enum.LOGIN_USER_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.LOGIN_PROFILE_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.RESTORE_ERROR: return s + "warning_red.png";
            case Results_Enum.RESTORE_ACTIVITY_DELETED: return s + "warning_red.png";
            case Results_Enum.RESTORE_MASTER_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.RESTORE_SLAVE_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.RESTORE_ACTIVITY_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.RESTORE_MASTER_CANNOT_HOST_SLAVE: return s + "warning_red.png";
            case Results_Enum.RESTORE_MASTER_CANNOT_HOST_SLAVE_AND_PALLET: return s + "warning_red.png";
            case Results_Enum.RESTORE_MASTER_CANNOT_HOST_PALLET: return s + "warning_red.png";
            case Results_Enum.RESTORE_ERROR_SENDING_COMMAND: return s + "warning_red.png";
            case Results_Enum.RESTORE_ERROR_CANNOT_CREATE_COMMAND: return s + "warning_red.png";
            case Results_Enum.RESTORE_ERROR_CANNOT_SEND_COMMAND: return s + "warning_red.png";
            case Results_Enum.RESTORE_ERROR_POSITION_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.RESTORE_ERROR_PALLET_NOT_VALID: return s + "warning_red.png";
            case Results_Enum.RESTORE_ERROR_PLC_CONFIGURATION: return s + "warning_red.png";
            case Results_Enum.RESTORE_ERROR_MASTER_HAS_ALREADY_A_COMMAND: return s + "warning_red.png";
            case Results_Enum.RESTORE_ERROR_PALLET_NOT_FOUND: return s + "warning_red.png";
            case Results_Enum.RESTORE_ERROR_PALLET_NOT_DETECTED_BY_SENSORS: return s + "warning_red.png";
          }

          
        }
        return "";
      }
      catch(Exception ex)
      {
        string exc = ex.ToString();

        return s + "warning.png";
      }
      ;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
