﻿using System;
using System.Windows;
using System.Windows.Data;
using Model.Common.Plugin;
namespace View.Common.Converter
{
  public class PluginStatusToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      if (value is Plugin_State)
      {
        Plugin_State state = (Plugin_State)value;

        string button = (string)parameter;

        switch (state)
        {
          case Plugin_State.INITIALIZING:
            return Visibility.Hidden;

          case Plugin_State.STARTED:
            if (button == "start")
              return Visibility.Hidden;
            if (button == "stop")
              return Visibility.Visible;
            if (button == "disable")
              return Visibility.Visible;
            if (button == "enable")
              return Visibility.Hidden;
            if (button == "update")
              return Visibility.Visible;
            if (button == "unload")
              return Visibility.Visible;
            break;

          case Plugin_State.STOPPED:
            if (button == "start")
              return Visibility.Visible;
            if (button == "stop")
              return Visibility.Hidden;
            if (button == "disable")
              return Visibility.Hidden;
            if (button == "enable")
              return Visibility.Hidden;
            if (button == "update")
              return Visibility.Hidden;
            if (button == "unload")
              return Visibility.Visible;
            else return Visibility.Hidden;


          case Plugin_State.STOPPING:
            return Visibility.Hidden;

          case Plugin_State.DISABLED:
            if (button == "stop")
              return Visibility.Visible;
            if (button == "start")
              return Visibility.Hidden;
            if (button == "enable")
              return Visibility.Visible;
            if (button == "disable")
              return Visibility.Hidden;
            if (button == "update")
              return Visibility.Visible;
            if (button == "unload")
              return Visibility.Visible;
            else return Visibility.Hidden;
        }
      }
      return null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotSupportedException();
    }
  }

}
