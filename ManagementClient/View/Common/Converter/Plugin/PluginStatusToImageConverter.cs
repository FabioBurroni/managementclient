﻿using System;
using System.Windows.Data;
using Model.Common.Plugin;

namespace View.Common.Converter
{
	public class PluginStatusToImageConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (value is Plugin_State)
			{
				Plugin_State state = (Plugin_State)value;
				if (state == Plugin_State.INITIALIZING)
					return new Uri("/images/init.png", UriKind.Relative);
				if (state == Plugin_State.DISABLED)
					return new Uri("/images/disabled.png", UriKind.Relative);
				if (state == Plugin_State.STARTED)
					return new Uri("/images/play.png", UriKind.Relative);
				if (state == Plugin_State.STOPPING)
					return new Uri("/images/stopping.jpg", UriKind.Relative);
				if (state == Plugin_State.STOPPED)
					return new Uri("/images/stop_m.png", UriKind.Relative);
			}
			return null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotSupportedException();
		}
	}
}
