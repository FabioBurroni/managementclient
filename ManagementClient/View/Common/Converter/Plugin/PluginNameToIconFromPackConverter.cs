﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
  public class PluginNameToIconFromPackConverter : IValueConverter
  {
    
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      string iconname = "Gear";
      if(value!=null)
      {
        if (value is string)
        {
          if (!string.IsNullOrEmpty(value.ToString()))
          {
            string v = value.ToString();

            if (v.Contains("Administration"))
              iconname = "ShieldAccount";
            else if (v.Contains("Article"))
              iconname = "FormatListNumbered";
            else if (v.Contains("CheckIn"))
              iconname = "Gear";
            else if (v.Contains("Data"))
              iconname = "DatabaseLockOutline";
            else if (v.Contains("Exit"))
              iconname = "ExitToApp";
            else if (v.Contains("Map"))
              iconname = "Map";
            else if (v.Contains("Master"))
              iconname = "GolfCart";
            else if (v.Contains("Order"))
              iconname = "PlaylistPlay";
            else if (v.Contains("Report"))
              iconname = "TableLarge";
            else if (v.Contains("Router"))
              iconname = "MapMarkerPath";
            //else if (v.Contains("Configuration"))
            //  iconname = "Gear";

          }
        }
        
      }
      return iconname;
      //return new PackIcon { Kind = (PackIconKind)Enum.Parse(typeof(PackIconKind),iconname) };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
