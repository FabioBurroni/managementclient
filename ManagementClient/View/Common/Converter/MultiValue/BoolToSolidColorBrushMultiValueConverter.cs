﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace View.Common.Converter
{
  public class BoolToSolidColorBrushMultiValueConverter : IMultiValueConverter
  {
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      if (values == null || values.Length != 3)
        return Binding.DoNothing;

      var warning = values[0] as bool?;
      var okColor = (SolidColorBrush)values[1];
      var nokColor = (SolidColorBrush)values[2];

      if (!warning.HasValue)
        return Binding.DoNothing;

      return warning.Value ? nokColor : okColor;
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      return new[] { Binding.DoNothing };
    }
  }
}