﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
  public class BoolToStringMultiValueConverter : IMultiValueConverter
  {
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      if (values == null || values.Length != 3)
        return Binding.DoNothing;

      var value = values[0] as bool?;
      var okString = (string)values[1];
      var nokString = (string)values[2];

      if (!value.HasValue)
        return Binding.DoNothing;

      return value.Value ? okString : nokString;
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      return new[] { Binding.DoNothing };
    }
  }
}