﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using Utilities.Extensions;

namespace View.Common.Converter
{
  public class ResultToSolidColorBrushMultiValueConverter : IMultiValueConverter
  {
    //Implementazione IValueConverter
    //public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //{
    //  var result = value as string;

    //  return result.EqualsIgnoreCase("ok") ? new SolidColorBrush(Colors.Green) : new SolidColorBrush(Colors.Red);
    //}

    //public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //{
    //  return Binding.DoNothing;
    //}

    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      if (values == null || values.Length != 3)
        return Binding.DoNothing;

      var okNok = values[0] as string;
      var okColor = (SolidColorBrush)values[1];
      var nokColor = (SolidColorBrush)values[2];

      return okNok.EqualsIgnoreCase("ok") ? okColor : nokColor;
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      return new[] { Binding.DoNothing };
    }
  }
}