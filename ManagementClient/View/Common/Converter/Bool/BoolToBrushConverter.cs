﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace View.Common.Converter
{
  public class BoolToBrushConverter : IValueConverter
  {

    public SolidColorBrush TrueColor { get; set; } = new SolidColorBrush(Colors.Green);
    public SolidColorBrush FalseColor { get; set; } = new SolidColorBrush(Colors.Red);


    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is bool)
        return ((bool)value) ? TrueColor : FalseColor;
      return FalseColor;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
