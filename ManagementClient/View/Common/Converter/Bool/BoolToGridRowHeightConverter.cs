﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace View.Common.Converter
{
  [ValueConversion(typeof(bool), typeof(GridLength))]
  public class BoolToGridRowHeightConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      double height = 0;

      if (parameter != null)
        height = double.Parse(parameter.ToString());

      return (value as bool?) == true ? new GridLength(height, GridUnitType.Pixel) : new GridLength(0);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      // Don't need any convert back
      return Binding.DoNothing;
    }
  }
}