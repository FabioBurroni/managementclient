﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;

namespace View.Common.Converter
{
  public class BoolToVisibilityConverter : IValueConverter
  {
    private Visibility _trueValue = Visibility.Visible;

    public Visibility TrueValue
    {
      get { return _trueValue; }
      set { _trueValue = value; }
    }

    private Visibility _falseValue = Visibility.Collapsed;
    public Visibility FalseValue
    {
      get { return _falseValue; }
      set
      {
        _falseValue = value;
      }
    }

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is bool)
      {
        if ((bool)value)
          return TrueValue;
      }
      return FalseValue;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
