﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
using Utilities.Extensions;

namespace View.Common.Converter
{
  public class UserLevelToVisibilityConverter : IValueConverter
  {
    private string _Level = string.Empty;
    public string Level
    {
      get { return _Level; }
      set { _Level = value;}
    }

    private Visibility _trueValue = Visibility.Visible;
    public Visibility TrueValue
    {
      get { return _trueValue; }
      set { _trueValue = value; }
    }

    private Visibility _falseValue = Visibility.Collapsed;
    public Visibility FalseValue
    {
      get { return _falseValue; }
      set
      {
        _falseValue = value;
      }
    }

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value != null)
      {
        if (value is string)
        {
          if (!string.IsNullOrEmpty(value.ToString()))
          {
            string userLevel = value.ToString();            

            // Level impostato non valido... fai sempre visualizzare
            if(string.IsNullOrEmpty(Level))
              return TrueValue;

            // Level impostato a sysAdmin
            if(Level.EqualsIgnoreCase("sysAdmin"))
            {
              // ritorna trueValue solo se l'utente è sysAdmin
              return Level.EqualsIgnoreCase(userLevel) ? TrueValue : FalseValue;
            }

            // Level impostato a plantadmin
            if (Level.EqualsIgnoreCase("plantadmin"))
            {
              // ritorna trueValue solo se l'utente è plantadmin o sysAdmin
              return Level.EqualsIgnoreCase("sysAdmin") || Level.EqualsIgnoreCase(userLevel) ? TrueValue : FalseValue;
            }

            // Level impostato a advoperator
            if (Level.EqualsIgnoreCase("advoperator"))
            {
              // ritorna trueValue solo se l'utente è plantadmin o sysAdmin
              return Level.EqualsIgnoreCase("sysAdmin") || Level.EqualsIgnoreCase("plantadmin") || Level.EqualsIgnoreCase(userLevel) ? TrueValue : FalseValue;
            }

            // Level impostato a operator
            if (Level.EqualsIgnoreCase("operator"))
            {
              // ritorna trueValue solo se l'utente è plantadmin o sysAdmin
              return Level.EqualsIgnoreCase("sysAdmin") || Level.EqualsIgnoreCase("plantadmin") || Level.EqualsIgnoreCase("advoperator") || Level.EqualsIgnoreCase(userLevel) ? TrueValue : FalseValue;
            }
          }
        }
      }

      return FalseValue;

     
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
