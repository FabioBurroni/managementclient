﻿using MaterialDesignThemes.Wpf;
using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
  public class UserLevelToIconFromPackConverter : IValueConverter
  {

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value != null)
      {
        if (value is string)
        {
          if (!string.IsNullOrEmpty(value.ToString()))
          {
            string iconname = (string)value;

            if (iconname.ToLowerInvariant() == "") return PackIconKind.AccountCog;
            else if (iconname.ToLowerInvariant() == "sysadmin") return PackIconKind.ShieldAccount;
            else if (iconname.ToLowerInvariant() == "plantadmin") return PackIconKind.AccountTie;
            else if (iconname.ToLowerInvariant() == "advoperator") return PackIconKind.AccountHardHat;
            else if (iconname.ToLowerInvariant() == "operator") return PackIconKind.UserGroup;
          }
        }       
      }

      return PackIconKind.User;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    public static object Convert(string value)
    {
      return new UserLevelToIconFromPackConverter().Convert(value, null, null, CultureInfo.CurrentCulture);
    }

  }
}
