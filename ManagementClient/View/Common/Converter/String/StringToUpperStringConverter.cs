﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
  public class StringToUpperStringConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is string)
      {
        return value.ToString().ToUpperInvariant();
      }
      return "";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
