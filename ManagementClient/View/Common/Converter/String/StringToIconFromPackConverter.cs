﻿using MaterialDesignThemes.Wpf;
using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
  public class StringToIconFromPackConverter : IValueConverter
  {

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      string iconname = "Gear";
      if (value != null)
      {
        if (value is string)
        {
          if (!string.IsNullOrEmpty(value.ToString()))
            iconname = (string)value;
        }
        else
          iconname = value.ToString();
      }

      try
      {
        return (PackIconKind)Enum.Parse(typeof(PackIconKind), iconname);
      }
      catch(Exception ex)
      {
        string exc = ex.ToString();

        return "Gear";
      }
      
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    public static object Convert(string value)
    {
      return new StringToIconFromPackConverter().Convert(value, null, null, CultureInfo.CurrentCulture);
    }
  }

 
}
