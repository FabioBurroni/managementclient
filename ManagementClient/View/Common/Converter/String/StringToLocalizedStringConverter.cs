﻿using System;
using System.Windows.Data;
using System.Globalization;


namespace View.Common.Converter
{
  public class StringToLocalizedStringConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      string ret = "";
      if (value is string)
      {
        //...split character
        if (parameter != null
          && parameter is string
          && !string.IsNullOrEmpty((string)parameter)
          && !string.IsNullOrEmpty((string)value))
        {
          var strA = ((string)value).Split(((string)parameter).ToCharArray());
          if (strA.Length >= 2)
            ret = strA[0] + " " + Localization.Localize.LocalizeDefaultString((string)strA[1]);
          else
            ret = Localization.Localize.LocalizeDefaultString((string)value);
        }
        else
        {
          ret = Localization.Localize.LocalizeDefaultString((string)value);
        }
      }
      else
      {
        ret = Localization.Localize.LocalizeDefaultString(value.ToString().Replace('_',' '));
      }
      return ret;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return Binding.DoNothing;
    }
  }
}
