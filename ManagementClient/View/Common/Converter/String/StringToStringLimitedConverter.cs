﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
  public class StringToStringLimitedConverter : IValueConverter
  {

	public int MaxLength { get; set; } = 15;

	public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
	{
	  var s = value as string;
	  if (s != null && s.Length > MaxLength)
		return s?.Substring(0, MaxLength)+"..." ?? value;
	  else
		return s;
	}

	public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
	{
	  throw new NotImplementedException();
	}
  }
}