﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
  public class StringToBoolConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is string)
      {
        return ((string)value).ToLower() == "1" ? true : ((string)value).ToLower() == "true" ? true : false;
      }

      if (value is bool)
        return value;

      return false;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is bool?)
      {
        return ((bool?)value).ToString();
      }
      return "false";
    }
  }
}
