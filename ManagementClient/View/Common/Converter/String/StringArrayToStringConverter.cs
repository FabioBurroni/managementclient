﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace View.Common.Converter
{
  public class StringArrayToStringConverter : IValueConverter
  {

	public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
	{
    try
    {
			IList<string> collection = (IList<string>)value;
			return string.Join("," , collection.OrderBy(p => p, StringComparer.OrdinalIgnoreCase).ToArray());
    }
		catch(Exception ex)
			{
				string exc = ex.ToString();

				return string.Empty;
		}
	}

	public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
	{
	  throw new NotImplementedException();
	}
  }
}