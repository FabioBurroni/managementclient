﻿using System;
using System.Windows.Data;
using System.Globalization;
using Authentication;

namespace View.Common.Converter
{

  public class UserActionToBooleanConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value != null)
      {
        string userAction = value.ToString();
        return Profiler.IsActionAllowed(userAction);
      }
      return false;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      //return (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString((string)value);

      return value;
    }

  }
}
