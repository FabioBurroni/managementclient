﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace View.Common.Converter
{
  public class IntThreeLevelToColorConverter : IValueConverter
  {

    private int _MinLevel;

    public int MinLevel
    {
      get { return _MinLevel; }
      set { _MinLevel = value; }
    }

    private int _MediumLevel;

    public int MediumLevel
    {
      get { return _MediumLevel; }
      set { _MediumLevel = value; }
    }


    private SolidColorBrush _RedColor = new SolidColorBrush(Colors.Red);

    public SolidColorBrush RedColor
    {
      get { return _RedColor; }
      set { _RedColor = value; }
    }


    private SolidColorBrush _GreenColor = new SolidColorBrush(Colors.Green);

    public SolidColorBrush GreenColor
    {
      get { return _GreenColor; }
      set { _GreenColor = value; }
    }


    private SolidColorBrush _OrangeColor = new SolidColorBrush(Colors.Orange);

    public SolidColorBrush OrangeColor
    {
      get { return _OrangeColor; }
      set { _OrangeColor = value; }
    }



    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is int)
      {
        if ((int)value<MinLevel)
          return RedColor;
        if ((int)value < MediumLevel)
          return OrangeColor;
        return GreenColor;
      }
      return RedColor;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
