﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
	/// <summary>
	/// Converte in intero 0-100 in base al max value inserito come converter parameter
	/// </summary>
	public class IntToPercConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
      {
				int val = int.Parse(value.ToString());
				int par = int.Parse(parameter.ToString());

				if (par == 0)
					return 0;

				return (val / par) * 100;
			}
			catch(Exception ex)
			{
				string exc = ex.ToString();

				return 0;
      }
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}
	}
}