﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
	/// <summary>
	/// Converte in intero in un intero nullabile, quindi se il valore è 0 viene restituito null
	/// </summary>
	public class IntToNullableIntConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var nullableInt = value as int?;
			if (nullableInt == null)
				throw new NullReferenceException($"{nameof(nullableInt)} is not {typeof(int?)}");

			//se il valore è 0, allora restituisco null altrimenti il valore stesso
			return nullableInt == 0 ? null : nullableInt;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}
	}
}