﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
	public class IntHigherOrEqualParameterToBoolConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
      try
      {
				if (value is null || parameter is null)
					return false;			

				return (int.Parse(value.ToString()) >= int.Parse(parameter.ToString()));
      }
			catch(Exception ex)
      {
				string exc = ex.ToString();
				return false;
      }
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}
	}
}