﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
	public class IntLowerOrEqualToBoolConverter : IValueConverter
	{
		public int CompareValue { get; set; } = 0;

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var nullableInt = value as int?;
			return nullableInt <= CompareValue;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}
	}
}