﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
	public class NullToBoolConverter : IValueConverter
	{
		/// <summary>
		/// Restituisce True solo se il valore è diverso da null
		/// </summary>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return value != null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}
	}
}