﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
	public class NullOrEmptyToBoolConverter : IValueConverter
	{
		/// <summary>
		/// Restituisce Trye solo se il valore è diverso da null
		/// </summary>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return !string.IsNullOrEmpty(value?.ToString());
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}
	}
}