﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace View.Common.Converter
{
	public class NullToVisibilityConverter : IValueConverter
	{
		public Visibility IsNullValue { get; set; } = Visibility.Hidden;
		public Visibility IsNotNullValue { get; set; } = Visibility.Visible;
		/// <summary>
		/// Restituisce Visibility.Visible solo se il valore è booleano e vale true
		/// </summary>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return (value is null) ? IsNullValue : IsNotNullValue;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}