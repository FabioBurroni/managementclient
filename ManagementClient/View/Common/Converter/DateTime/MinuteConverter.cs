﻿using System;
using System.Globalization;
using System.Windows.Data;
namespace View.Common.Converter
{
  [ValueConversion(typeof(decimal), typeof(string))]
  public class MinuteConverter : IValueConverter
  {
    /// <summary>
    /// Converte un intero in stringa
    /// From native format to display format
    /// </summary>
    /// <param name="value"></param>
    /// <param name="targetType"></param>
    /// <param name="parameter"></param>
    /// <param name="culture"></param>
    /// <returns></returns>
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      int minute = (int)value;
      return minute.ToString().PadLeft(2, '0');
    }

    /// <summary>
    /// Converte la string in int
    /// From display format to native format
    /// </summary>
    /// <param name="value"></param>
    /// <param name="targetType"></param>
    /// <param name="parameter"></param>
    /// <param name="culture"></param>
    /// <returns></returns>
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      string str = value.ToString();
      int result = 0;

      if (int.TryParse(str, out result))
      {
        return result;
      }

      return result.ToString();

    }
  }
}
