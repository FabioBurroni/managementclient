﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace View.Common.Converter
{
  public class DateTimeToStringConverter : IValueConverter
  {
    private string _DateFormat = "HH:mm:ss dd-MM-yyyy";

    public string DateFormat
    {
      get { return _DateFormat; }
      set { _DateFormat = value; }
    }

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is DateTime)
      {
        DateTime dt = (DateTime)value;

        if (dt == DateTime.MinValue || dt == DateTime.MaxValue)
          return "";

        return dt.ToString(_DateFormat);
      }

      return "";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
