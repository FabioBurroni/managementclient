﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace View.Common.Converter
{
  public class ClientNameToImgUrlConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      string s = "/View;component/Common/Images/";

      if (value != null)
      {
        if (value is string)
        {
          if (!string.IsNullOrEmpty(value.ToString()))
          {
            string v = value.ToString().ToLowerInvariant();

            switch (v)
            {
              case "management_client": return s + "client.ico";
              case "synoptic_manager": return s + "diagnosticManager.ico";
              case "report_manager": return s + "order.png";
            }
          }
        }
      }      
      return s + "logo_cassioli.png";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
