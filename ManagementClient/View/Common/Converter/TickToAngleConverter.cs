﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows;
using System.Globalization;
using System.Windows.Controls;


namespace View.Common.Converter
{
  public class TickToAngleConverter : IMultiValueConverter
  {

    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      double tick = (double)values[0];
      ProgressBar bar = values[1] as ProgressBar;
      var ret = 359.98 * (tick / (bar.Maximum - bar.Minimum)) + 180;
      //var ret = 359.98 * (tick / (bar.Maximum - bar.Minimum));
      return ret;
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

  }
}
