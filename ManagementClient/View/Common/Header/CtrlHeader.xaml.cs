﻿using System;
using System.Windows;
using Authentication;
using Configuration;
using Localization;
using Model;
using System.Drawing;
using System.IO;
using UserControl = System.Windows.Controls.UserControl;
using View.Common.Tools;
using MaterialDesignThemes.Wpf;
using View.Common.Info;
using View.Common.Converter;
using ExtendedUtilities.SnackbarTools;
using View.Common.Languages;
using View.Common.LayoutSettings;
using ExtendedUtilities.FileDialog;
using Utilities.Extensions;

namespace View.Common.Header
{
	/// <summary>
	/// Interaction logic for CtrlHeader.xaml
	/// </summary>
	public partial class CtrlHeader : UserControl
	{
		#region Events

		public event EventHandler LogoutRequest;

		//20210127 Evento ricerca Aggiornamenti
		public delegate void CheckForUpdateHandler();
		public event CheckForUpdateHandler CheckForUpdate;

		public delegate void OnKeyboardClickHandler();
		public event OnKeyboardClickHandler OnKeyboardClick;

		#endregion

		#region Fields

		private readonly AuthenticationManager _authenticationManager = AuthenticationManager.Instance;
		private readonly ConfigurationParameters _confParameters = ConfigurationManager.Parameters;
		private readonly CultureManager _cultureManager = CultureManager.Instance;

		#endregion

		#region DP - AutomaticUpdateEnabled
		public bool AutomaticUpdateEnabled
		{
			get { return (bool)GetValue(AutomaticUpdateEnabledProperty); }
			set { SetValue(AutomaticUpdateEnabledProperty, value); }
		}

		// Using a DependencyProperty as the backing store for AutomaticUpdateEnabled.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty AutomaticUpdateEnabledProperty =
				DependencyProperty.Register("AutomaticUpdateEnabled", typeof(bool), typeof(CtrlHeader), new PropertyMetadata(false));
		#endregion

		#region DP - App Version
		public string AppVersion
		{
			get { return (string)GetValue(AppVersionProperty); }
			set { SetValue(AppVersionProperty, value); }
		}

		// Using a DependencyProperty as the backing store for AppVersion.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty AppVersionProperty =
			DependencyProperty.Register("AppVersion", typeof(string), typeof(CtrlHeader), new PropertyMetadata(""));

    #endregion
		  

		#region Constructor

		public CtrlHeader()
		{			
			InitializeComponent();
		}

		#endregion

		#region Init

		public void Init()
		{
			//setto i dati di default
			SetSessionData(null);

			InitAuthenticationManager();
			InitCultureManager();

			CtrlLanguage.Init();

			Translate();		
			
		}

		#endregion

		#region Events

		private void ButtonLogout_OnClick(object sender, RoutedEventArgs e)
		{
			LogoutRequest?.Invoke(this, new EventArgs());
		}

		private void butCheckUpdates_Click(object sender, RoutedEventArgs e)
		{
			CheckForUpdate?.Invoke();
		}

		private void popboxUsername_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
		{
			vbUsername.Visibility = Visibility.Visible;
		}

		private void popboxUsername_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			vbUsername.Visibility = Visibility.Hidden;
		}

		private async void btnTools_Click(object sender, RoutedEventArgs e)
		{
			var toolBox = new CtrlToolbox();

			//show the dialog
			var result = await DialogHost.Show(toolBox, "RootDialog");

			if (toolBox.TakeScreen)
			{
				TakeScreen();
			}
			else if (toolBox.RecordScreen)
			{
				RecordScreen();
			}
		}

		private async void ButtonAbout_Click(object sender, RoutedEventArgs e)
		{
			var info = new CtrlInfo();

			//show the dialog
			var result = await DialogHost.Show(info, "RootDialog");

		}

		private async void btnLayoutSettings_Click(object sender, RoutedEventArgs e)
		{
			var layoutSettings = new CtrlLayoutSettings();

			//show the dialog
			var result = await DialogHost.Show(layoutSettings, "RootDialog");

		}

		private void btnKeyboard_Click(object sender, RoutedEventArgs e)
		{
			OnKeyboardClick?.Invoke();
		}

		private void btnExit_Click(object sender, RoutedEventArgs e)
		{
			var window = Window.GetWindow(this);
			window.Close();
		}
		#endregion

		#region Private Methods

		private void Translate()
		{
			LabelUsername.Text = Localize.LocalizeDefaultString("Username".ToUpperInvariant());
			LabelProfile.Text = Localize.LocalizeDefaultString("Profile".ToUpperInvariant());
			TextBlockLogout.Text = Localize.LocalizeDefaultString("Logout".ToUpperInvariant());
			//butCheckUpdatesText.Text= Localize.LocalizeDefaultString("UDATES").ToUpperInvariant();

			LabelProfile.Text = Localize.LocalizeDefaultString("Profile".ToUpperInvariant());
			LabelProfile.Text = Localize.LocalizeDefaultString("Profile".ToUpperInvariant());
			LabelProfile.Text = Localize.LocalizeDefaultString("Profile".ToUpperInvariant());

		}

		private void SetSessionData(ISession session)
		{
			Dispatcher.Invoke(() =>
			{
				var isSessionValid = session?.IsActive == true;

				TxtUsername.Text = isSessionValid ? session.User.Username : "...";
				TxtUserProfile.Text = isSessionValid ? session.User.UserProfileCode : "...";
				//TxtUserIpAddress.Text = isSessionValid ? session.Source.IpAddressString : "...";

				TxtPopBoxUsername.Text = TxtUsername.Text;
				TxtPopBoxUserProfile.Text = TxtUserProfile.Text;

				if (isSessionValid)
				{
					IconUser.Kind = (PackIconKind)UserLevelToIconFromPackConverter.Convert(TxtUserProfile.Text);
					IconUserSmall.Kind = (PackIconKind)UserLevelToIconFromPackConverter.Convert(TxtUserProfile.Text);
				}					

				//il bottone di logout lo mostro se la sessione è valida e non è esplicitamente richiesto di non farlo vedere
				btnLogout.Visibility = isSessionValid && !ArgumentsConfiguration.DenyLogout ? Visibility.Visible : Visibility.Hidden;

				// solo sysAdmin accede ai tools
				btnTools.Visibility = isSessionValid && session.User.UserProfileCode.EqualsIgnoreCase("sysAdmin") ? Visibility.Visible : Visibility.Collapsed;
			});
		}

		#region Authentication Manager

		private void InitAuthenticationManager()
		{
			_authenticationManager.LoginSession += delegate (object sender, LoginSessionEventArgs e) { SetSessionData(e.Session); };
			_authenticationManager.LogoutSession += delegate (object sender, LogoutSessionEventArgs e) { SetSessionData(e.Session); };
		}

		#endregion

		#region Culture Manager

		private void InitCultureManager()
		{
			_cultureManager.CultureChanged += delegate { Translate(); };
		}




		#endregion

		#region Screenshot
		private async void TakeScreen()
		{
			Screenshot s = new Screenshot();
			s.ShowDialog();

			System.Drawing.Image image = CaptureScreen(s.Position.X, s.Position.Y, s.Width, s.Height);


			SaveFileDialogArguments dialogArgs = new SaveFileDialogArguments()
			{
				Width = 1000,
				Height = 1000,
				CurrentDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
				CreateNewDirectoryEnabled = true,
				ShowHiddenFilesAndDirectories = false,
				ShowSystemFilesAndDirectories = true,
				SwitchPathPartsAsButtonsEnabled = true,
				PathPartsAsButtons = true,

				Filename = CleanFileName($"{Localize.LocalizeDefaultString("SCREENSHOT")} {DateTime.Now.ToString(CultureManager.Instance.CultureInfo.DateTimeFormat.FullDateTimePattern)}", "_") + ".png",
				ForceFileExtensionOfFileFilter = true,
				Filters = "png files (*.png)|*.png ",
				FilterIndex = 0,
				CurrentFile = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + CleanFileName($"{Localize.LocalizeDefaultString("SCREENSHOT")} {DateTime.Now.ToString(CultureManager.Instance.CultureInfo.DateTimeFormat.FullDateTimePattern)}", "_") + ".png"
			};

			SaveFileDialogResult save = await SaveFileDialog.ShowDialogAsync("RootDialog", dialogArgs);

			if (save is null)
      {
				MainSnackbar.ShowMessageInfo("NOT SAVED".TD(), 1);
				return;
      }

			if (!save.Canceled)
			{
				if (!string.IsNullOrEmpty(save.File))
				{
					FileInfo fi = new FileInfo(save.File);

					if (fi.Extension == ".png")
					{
						image.Save(save.File);
						MainSnackbar.ShowMessageOk("SCREENSHOT COMPLETED".TD() + " " + save.File, 5);
					}
					else
					{
						MainSnackbar.ShowMessageFail("FILE EXTENSION NOT VALID".TD() + " " + save.File, 5);
					}
				}
				else
				{
					MainSnackbar.ShowMessageFail("FILE NAME NOT VALID".TD() + " " + save.File, 5);
				}
			}
			else
			{
					MainSnackbar.ShowMessageInfo("NOT SAVED".TD(), 1);
			}

		}

		private System.Drawing.Image CaptureScreen(double sourceX, double sourceY, double width, double height)
		{
			System.Drawing.Size regionSize = new System.Drawing.Size((int)width, (int)height);
			Bitmap bmp = new Bitmap(regionSize.Width, regionSize.Height);
			Graphics g = Graphics.FromImage(bmp);
			g.CopyFromScreen((int)sourceX, (int)sourceY, 0, 0, regionSize);
			return bmp;
		}

		#endregion

		#region Screen Record
		private void RecordScreen()
		{
			ScreenRecorder s = new ScreenRecorder();
			s.ShowDialog();
		}
		#endregion

		private string CleanFileName(string fileName, string replacement)
		{
			string regexSearch = new string(System.IO.Path.GetInvalidFileNameChars()) + new string(System.IO.Path.GetInvalidPathChars());
			var r = new System.Text.RegularExpressions.Regex(string.Format("[{0}]", System.Text.RegularExpressions.Regex.Escape(regexSearch)));
			return r.Replace(fileName, replacement);
		}

		
		#endregion

		

   
  }
}