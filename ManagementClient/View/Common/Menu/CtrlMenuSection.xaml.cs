﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Authentication;
using Configuration.Settings.UI;
using Localization;
using Model;
using Model.Common;
using Utilities.Extensions.MoreLinq;
using SelectionChangedEventArgs = System.Windows.Controls.SelectionChangedEventArgs;

namespace View.Common.Menu
{
	/// <summary>
	/// Interaction logic for CtrlMenuSection.xaml
	/// </summary>
	public partial class CtrlMenuSection : UserControl
	{
		public delegate void OnMenuSelectedHandler(object sender, MenuInfoItem menuitem);

		public event OnMenuSelectedHandler OnMenuSelected;

		private readonly CultureManager _cultureManager = CultureManager.Instance;

		#region Constructor

		public CtrlMenuSection()
		{
			InitializeComponent();

			//20210125 la registrazione dell'evento CultureChanged viene spostata nell InitSection
			//_cultureManager.CultureChanged += delegate { Translate(); };
		}

		#endregion

		#region Properties

		public static readonly DependencyProperty HeaderProperty = DependencyProperty.Register("Header", typeof(string), typeof(CtrlMenuSection), new PropertyMetadata(default(string)));

		public string Header
		{
			get { return (string)GetValue(HeaderProperty); }
			set { SetValue(HeaderProperty, value); }
		}

		public static readonly DependencyProperty IconProperty = DependencyProperty.Register("Icon", typeof(string), typeof(CtrlMenuSection), new PropertyMetadata(default(string)));

		public string Icon
		{
			get { return (string)GetValue(IconProperty); }
			set { SetValue(IconProperty, value); }
		}


		public ObservableCollection<MenuInfoItem> MenuItems { get; } = new ObservableCollection<MenuInfoItem>();

		#endregion

		#region Events

		private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
		{
			//if (OnMenuSelected != null)
			//{
			//  var fe = sender as FrameworkElement;
			//  OnMenuSelected(this, (MenuInfoItem)fe?.Tag);
			//}
		}

		private void ListBoxMenuItems_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (e.AddedItems.Count > 0)
			{
				var fe = e.AddedItems[0] as MenuInfoItem;
				if (fe == null)
					return;

				OnMenuSelected?.Invoke(this, fe);
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Disegna una sezione, restituisce true se almeno un elemento è stato inserito
		/// </summary>
		public bool InitSection(MenuSection section)
		{
			var buttonSections = section.GetAll().OrderBy(s => s.Position).ToList();

			//filtro i bottoni senza azione o con profilo settato
			buttonSections = buttonSections.Where(s => string.IsNullOrEmpty(s.Action) || Profiler.IsActionAllowed(s.Action)).ToList();

			foreach (var buttonSection in buttonSections)
			{
				MenuItems.Add(new MenuInfoItem(buttonSection.Name, buttonSection.Text, buttonSection.ImageUri, buttonSection.Control.Path, buttonSection.Icon, buttonSection.IconColor));
			}

			//20210125
			//return buttonSections.Any();
			var ret = buttonSections.Any();
			if (ret)
			{
				_cultureManager.CultureChanged += delegate { Translate(); };

			}
			return ret;
		}

		public IList<MenuInfoItem> AvailableMenuItems()
		{
			return MenuItems.ToArray();
		}
        public MenuInfoItem GetMenuInfoItemByControlName(string controlName)
        {
            return MenuItems.SingleOrDefault(i => i.ControlName.Equals(controlName));
        }
        public void ClearSelection()
		{
			ListBoxMenuItems.UnselectAll();
		}

		//20210125 metodo close per deregistrare l'evento CultureChanged
		public void Close()
		{
			try
			{
				_cultureManager.CultureChanged -= delegate { Translate(); };
			}
			catch
			{
			}
		}
		#endregion

		#region Private Methods

		private void Translate()
		{
			//scorro il titolo della GroupBox e tutti i menu item per tradurli
			//20210323
			//GroupBoxHeader.Header = Header.LocalizeDefaultString();
			MenuItems.ForEach(mi => mi.LocalizeText());
		}

		#endregion
	}
}