﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Configuration.Settings.UI;
using Model.Common;
using Utilities.Extensions.MoreLinq;
using appconfiguration = Configuration;

using Localization;


namespace View.Common.Menu
{
  /// <summary>
  /// Interaction logic for CtrlLeftMenu.xaml
  /// </summary>
  public partial class CtrlLeftMenu : UserControl
  {
    public delegate void OnMenuSelectedHandler(MenuInfoItem menuitem);

    public event OnMenuSelectedHandler OnMenuSelected;

    private IDictionary<MenuSection, CtrlMenuSection> _menuSections = new Dictionary<MenuSection, CtrlMenuSection>();

    public CtrlLeftMenu()
    {
      InitializeComponent();
    }

    //public ObservableCollection<MenuInfoItem> MenuItems { get; } = new ObservableCollection<MenuInfoItem>();

    public void Init()
    {
      //faccio un dizionario con chiave la sezione e come valore il controllo stesso
      _menuSections = appconfiguration::ConfigurationManager.Parameters.LeftMenu.GetAll().OrderBy(s => s.Position).ToDictionary(k => k, v => (CtrlMenuSection)null);

      foreach (var section in _menuSections.Keys.ToArray())
      {

        if (!section.IsPositionAllowed(appconfiguration::ConfigurationManager.Parameters.AuthenticationSetting.LoginSetting.Positions.ToList()))
          
          continue;

        var ctrlLeftMenu = new CtrlMenuSection();
        if (ctrlLeftMenu.InitSection(section))
        {
          ctrlLeftMenu.OnMenuSelected += CtrlLeftMenuOnMenuSelected;
          _menuSections[section] = ctrlLeftMenu;
        }
      }

      //scorro le sezioni valide
      foreach (var section in _menuSections.Where(s => s.Value != null))
      {
        //20210128 traduzione group box
        section.Value.Header = Localization.Localize.LocalizeDefaultString(section.Key.Text.ToUpperInvariant());
        section.Value.Icon = section.Key.Icon;
        MainControl.Children.Add(section.Value);

      }

      //20210323
      try
      {
        CultureManager.Instance.CultureChanged += delegate { Translate(); };
      }
      catch
      {
      }


    }

    //20210323
    private void Translate()
    {
      foreach (var section in _menuSections.Where(s => s.Value != null))
      {
        //20210128 traduzione group box
        section.Value.Header = Localization.Localize.LocalizeDefaultString(section.Key.Text);
      }
    }

    public IList<MenuInfoItem> AvailableMenuItems()
    {
      return _menuSections.Values.Where(m => m != null).SelectMany(m => m.AvailableMenuItems()).ToArray();
    }

    public MenuInfoItem GetMenuInfoItemByControlName(string controlName)
    {
      return _menuSections
          .Select(section => section.Value.GetMenuInfoItemByControlName(controlName))
          .SingleOrDefault(item => item != null);
    }

    public void Close()
    {
      foreach (var s in _menuSections.Where(s => s.Value != null))
      {
        s.Value.OnMenuSelected -= CtrlLeftMenuOnMenuSelected;
        //20210125 metodo close per deregistrare l'evento CultureChanged
        s.Value.Close();
      }

      _menuSections.Clear();
      MainControl.Children.Clear();

      //20210323
      try
      {
        CultureManager.Instance.CultureChanged -= delegate { Translate(); };
      }
      catch
      {
      }
    }

    public void ClearSelection()
    {
      _menuSections.Values.ForEach(delegate (CtrlMenuSection section) { section?.ClearSelection(); });
    }

    private void CtrlLeftMenuOnMenuSelected(object sender, MenuInfoItem menuItem)
    {
      OnMenuSelected?.Invoke(menuItem);

      //resetto la selezione della listbox nei controlli che non contengono il bottone di scelta effettuato
      var ctrlMenuSection = sender as CtrlMenuSection;
      if (ctrlMenuSection == null)
        return;

      _menuSections.Values.ForEach(delegate (CtrlMenuSection section) { if (section != ctrlMenuSection) section?.ClearSelection(); });
    }
  }
}