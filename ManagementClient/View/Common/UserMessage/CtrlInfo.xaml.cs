﻿using System;
using System.Windows;

namespace View.Common.UserMessage
{
  /// <summary>
  /// Interaction logic for CtrlCustomTooltip.xaml
  /// </summary>
  public partial class CtrlInfo : CtrlBase
  {
    #region DP - Title
    public string Title
    {
      get { return (string)GetValue(TitleProperty); }
      set { SetValue(TitleProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty TitleProperty =
       DependencyProperty.Register("Title", typeof(string), typeof(CtrlInfo), new PropertyMetadata(""));
    #endregion

    #region DP - ImageUri
    public string ImageUri
    {
      get { return (string)GetValue(ImageUriProperty); }
      set { SetValue(ImageUriProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ImageUriProperty =
       DependencyProperty.Register("ImageUri", typeof(string), typeof(CtrlInfo), new PropertyMetadata(""));
    #endregion

    #region DP - Description
    public string Description
    {
      get { return (string)GetValue(DescriptionProperty); }
      set { SetValue(DescriptionProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty DescriptionProperty =
       DependencyProperty.Register("Description", typeof(string), typeof(CtrlInfo), new PropertyMetadata(""));
    #endregion

    #region COSTRUTTORE
    public CtrlInfo()
    {
      Title = "";
      ImageUri = "";
      Description = "";

      InitializeComponent();
    }

    
    #endregion


  }


}
