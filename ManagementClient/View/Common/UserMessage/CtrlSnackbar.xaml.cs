﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace View.Common.UserMessage
{
	/// <summary>
	/// Interaction logic for CtrlSnackbar.xaml
	/// </summary>
	public partial class CtrlSnackbar : UserControl
  {
		#region costruttore

		public CtrlSnackbar()
		{
			InitializeComponent();
		}
		#endregion


		#region EVENTI INTERFACCIA
		/// <summary>
		/// Show Generic Grey info SnackBar
		/// </summary>
		/// <param name="text"> Text to display</param>
		/// <param name="second"> Duration </param>
		public void ShowMessageInfo(string text, int second)
    {
			control.Background = Brushes.DarkSlateGray;

			control.MessageQueue?.Enqueue(
							 text,
							 null,
							 null,
							 null,
							 false,
							 true,
							 TimeSpan.FromSeconds(second));
		}

		/// <summary>
		/// Show Ok info  green SnackBar
		/// </summary>
		/// <param name="text"> Text to display</param>
		/// <param name="second"> Duration </param>
		public void ShowMessageOk(string text, int second)
		{
			control.Background = Brushes.ForestGreen;

			control.MessageQueue?.Enqueue(
							 text,
							 null,
							 null,
							 null,
							 false,
							 true,
							 TimeSpan.FromSeconds(second));
		}

		/// <summary>
		/// Show Ok info  green SnackBar
		/// </summary>
		/// <param name="text"> Text to display</param>
		/// <param name="second"> Duration </param>
		public void ShowMessageOkAndOpenFile(string text, int second, string actionContent, string filePath)
		{
			Action<string> openFileCommand;
			openFileCommand = OpenFile;

			control.Background = Brushes.ForestGreen;
			var style = this.FindResource("SnackBarButton") as Style;
			control.ActionButtonStyle = style;

			control.MessageQueue?.Enqueue(
							 text,
							 actionContent,
							 openFileCommand,
							 filePath,
							 false,
							 true,
							 TimeSpan.FromSeconds(second));
		}

		private void OpenFile(string filePath)
    {
      try
      {
				if (File.Exists(filePath))
					System.Diagnostics.Process.Start("explorer.exe", filePath);
			}
			catch(Exception) { }
		}

		/// <summary>
		/// Show failure info red SnackBar
		/// </summary>
		/// <param name="text"> Text to display</param>
		/// <param name="second"> Duration </param>
		public void ShowMessageFail(string text, int second)
		{
			control.Background = Brushes.Red;

			control.MessageQueue?.Enqueue(
							 text,
							 null,
							 null,
							 null,
							 false,
							 true,
							 TimeSpan.FromSeconds(second));
		}

		#endregion


	}
}
