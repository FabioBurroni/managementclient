﻿using System;
using System.Windows.Controls;
using System.Windows.Media;

namespace View.Common.UserMessage
{
	/// <summary>
	/// Interaction logic for CtrlSnackbar.xaml
	/// </summary>
	public partial class CtrlSnackbarServer : UserControl
  {
		#region costruttore

		public CtrlSnackbarServer()
		{
			InitializeComponent();
		}
		#endregion


		#region EVENTI INTERFACCIA

		/// <summary>
		/// Show Generic Grey info SnackBar
		/// </summary>
		/// <param name="text"> Text to display</param>
		/// <param name="second"> Duration </param>
		public void ShowMessageInfo(string text, int second)
    {
			control.Background = Brushes.DarkSlateGray;

			control.MessageQueue?.Enqueue(
							 text,
							 null,
							 null,
							 null,
							 false,
							 true,
							 TimeSpan.FromSeconds(second));
		}

		/// <summary>
		/// Show Generic Grey info SnackBar
		/// </summary>
		/// <param name="text"> Text to display</param>
		/// <param name="second"> Duration </param>
		public void ShowServerMessage(string text, int second)
		{
			control.Background = Brushes.DodgerBlue;

			control.MessageQueue?.Enqueue(
							 text,
							 null,
							 null,
							 null,
							 false,
							 true,
							 TimeSpan.FromSeconds(second));
		}

		/// <summary>
		/// Show Ok info  green SnackBar
		/// </summary>
		/// <param name="text"> Text to display</param>
		/// <param name="second"> Duration </param>
		public void ShowMessageOk(string text, int second)
		{
			control.Background = Brushes.ForestGreen;

			control.MessageQueue?.Enqueue(
							 text,
							 null,
							 null,
							 null,
							 false,
							 true,
							 TimeSpan.FromSeconds(second));
		}


		/// <summary>
		/// Show failure info red SnackBar
		/// </summary>
		/// <param name="text"> Text to display</param>
		/// <param name="second"> Duration </param>
		public void ShowMessageFail(string text, int second)
		{
			control.Background = Brushes.Red;

			control.MessageQueue?.Enqueue(
							 text,
							 null,
							 null,
							 null,
							 false,
							 true,
							 TimeSpan.FromSeconds(second));
		}

		#endregion


	}
}
