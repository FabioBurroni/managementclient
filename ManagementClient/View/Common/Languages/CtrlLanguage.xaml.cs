﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Configuration;
using Configuration.Settings.Global;
using ExtendedUtilities.Wpf;
using Localization;
using Utilities.Atomic;
using Utilities.Extensions;

namespace View.Common.Languages
{
	/// <summary>
	/// Interaction logic for CtrlLanguage.xaml
	/// </summary>
	public partial class CtrlLanguage : UserControl
	{
		#region Fields

		private readonly ThreadSafeSingleShotGuard _singleShotGuard = new ThreadSafeSingleShotGuard();

		private readonly CultureManager _cultureManager = CultureManager.Instance;

		private RelayCommand<Language> _languageCommand;

		private List<Language> _languages;

		#endregion

		#region Constructor

		public CtrlLanguage()
		{
			InitializeComponent();
		}

		#endregion

		#region Properties

		public static readonly DependencyProperty LanguagesProperty = DependencyProperty.Register("Languages", typeof(ObservableCollection<Language>), typeof(CtrlLanguage), new PropertyMetadata(default(ObservableCollection<Language>)));

		public ObservableCollection<Language> Languages
		{
			get { return (ObservableCollection<Language>)GetValue(LanguagesProperty); }
			set { SetValue(LanguagesProperty, value); }
		}

		#endregion

		#region Methods

		/// <summary>
		/// Inizializza la lingua
		/// </summary>
		public void Init()
		{
			if (!_singleShotGuard.CheckAndSetFirstCall)
				return;

			_cultureManager.CultureChanged += delegate { Translate(); };

			_languages = ConfigurationManager.Parameters.Localization.Languages.ToList();

			//non la possibità di far scattare OnPropertyChange perchè ho un ViewModel, quindi risetto la sorgente per far ricaricare i dati
			Languages = new ObservableCollection<Language>(_languages);
			ComboBoxLanguages.SelectedIndex = _languages.FindIndex(l => l.Name.EqualsIgnoreCase(ConfigurationManager.Parameters.Localization.DefaultLanguage.Name));
		}

		private void Translate()
		{
			TextBlockLanguage.Text = Localize.LocalizeDefaultString("Language").ToUpperInvariant();
		}

		private void SetLanguage(Language newlLanguage)
		{
			_cultureManager.Culture = newlLanguage.Name;
		}

		#endregion

		#region Command

		/// <summary>
		/// Comando per ricevere il cambio di lingua effettuato
		/// </summary>
		public ICommand LanguageCommand
		{
			get
			{
				return _languageCommand ?? (_languageCommand = new RelayCommand<Language>(SetLanguage));
			}
		}

		#endregion
	}
}