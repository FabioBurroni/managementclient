﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace View.Common.Languages
{
  public static class TranslateExtension
  {

    /// <summary>
    /// Esegue la traduzione di default
    /// </summary>
    /// <param name="voc"></param>
    /// <returns></returns>
    public static string TD(this string voc)
    {
      return Model.Context.Instance.TranslateDefault(voc);
    }

  }
}
