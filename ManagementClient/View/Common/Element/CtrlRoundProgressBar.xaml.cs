﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace View.Common.Element
{
  /// <summary>
  /// Interaction logic for CtrlRoundProgressBar.xaml
  /// </summary>
  public partial class CtrlRoundProgressBar : UserControl
  {
    public CtrlRoundProgressBar()
    {
      InitializeComponent();
    }

    public int Maximum
    {
      get { return (int)GetValue(MaximumProperty); }
      set { SetValue(MaximumProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Maximum.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty MaximumProperty =
        DependencyProperty.Register("Maximum", typeof(int), typeof(CtrlRoundProgressBar), new PropertyMetadata(100));


    public int Minimum
    {
      get { return (int)GetValue(MinimumProperty); }
      set { SetValue(MinimumProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Minimum.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty MinimumProperty =
        DependencyProperty.Register("Minimum", typeof(int), typeof(CtrlRoundProgressBar), new PropertyMetadata(0));

    public int Value
    {
      get { return (int)GetValue(ValueProperty); }
      set { SetValue(ValueProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ValueProperty =
        DependencyProperty.Register("Value", typeof(int), typeof(CtrlRoundProgressBar), new PropertyMetadata(0));


    public Brush ForeColor
    {
      get { return (Brush)GetValue(ForeColorProperty); }
      set { SetValue(ForeColorProperty, value); }
    }

    // Using a DependencyProperty as the backing store for ForeColor.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ForeColorProperty =
       DependencyProperty.Register("ForeColor", typeof(Brush), typeof(CtrlRoundProgressBar), new PropertyMetadata(Brushes.Blue));

    public Brush CountForeColor
    {
      get { return (Brush)GetValue(CountForeColorProperty); }
      set { SetValue(CountForeColorProperty, value); }
    }

    // Using a DependencyProperty as the backing store for CountForeColor.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty CountForeColorProperty =
        DependencyProperty.Register("CountForeColor", typeof(Brush), typeof(CtrlRoundProgressBar), new PropertyMetadata(Brushes.Black));

    public string PostAppend
    {
      get { return (string)GetValue(PostAppendProperty); }
      set { SetValue(PostAppendProperty, value); }
    }

    // Using a DependencyProperty as the backing store for PostAppend.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty PostAppendProperty =
        DependencyProperty.Register("PostAppend", typeof(string), typeof(CtrlRoundProgressBar), new PropertyMetadata(""));



  }
}
