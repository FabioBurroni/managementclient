﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using Authentication;
using Configuration;
using Localization;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Common.Bcr;
using Utilities.Extensions;
using XmlCommunicationManager.XmlClient.Event;
using appconfiguration = Configuration;


namespace View.Common.Footer
{
	/// <summary>
	/// Interaction logic for CtrlFooter.xaml
	/// </summary>
	public partial class CtrlFooter : UserControl, INotifyPropertyChanged
	{
		#region Notify property changed

		public event PropertyChangedEventHandler PropertyChanged;

		// This method is called by the Set accessor of each property.
		// The CallerMemberName attribute that is applied to the optional propertyName
		// parameter causes the property name of the caller to be substituted as an argument.
		protected void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion Notify property changed

		#region Fields

		private readonly Context _context = Context.Instance;
		private readonly CultureManager _cultureManager = CultureManager.Instance;
		private readonly AuthenticationManager _authenticationManager = AuthenticationManager.Instance;
		private readonly ConfigurationParameters _confParameters = ConfigurationManager.Parameters;
		private readonly BcrManager _bcrManager = Context.Instance.BcrManager;
		private bool _barcodeConnected;
		private string _comPort;
		private bool _hmsConnected;

		private DispatcherTimer switchingTimer = new DispatcherTimer();

		public delegate void OnKeyboardClickHandler();
		public event OnKeyboardClickHandler OnKeyboardClick;

    private bool keyboardVisibility;

    public bool KeyboardVisibility
		{
      get { return keyboardVisibility; }
      set { keyboardVisibility = value;
				NotifyPropertyChanged("KeyboardVisibility");
			}
    }




		#endregion

		#region AUTOMATIC UPDATE


		//20210514 Evento ricerca Aggiornamenti
		public delegate void CheckForUpdateHandler();
		public event CheckForUpdateHandler CheckForUpdate;



		public bool AutomaticUpdateEnabled
		{
			get { return (bool)GetValue(AutomaticUpdateEnabledProperty); }
			set { SetValue(AutomaticUpdateEnabledProperty, value); }
		}

		// Using a DependencyProperty as the backing store for AutomaticUpdateEnabled.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty AutomaticUpdateEnabledProperty =
				DependencyProperty.Register("AutomaticUpdateEnabled", typeof(bool), typeof(CtrlFooter), new PropertyMetadata(false));

		public bool AutomaticUpdateAvailable
		{
			get { return (bool)GetValue(AutomaticUpdateAvailableProperty); }
			set { SetValue(AutomaticUpdateAvailableProperty, value); }
		}

		// Using a DependencyProperty as the backing store for AutomaticUpdateAvailable.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty AutomaticUpdateAvailableProperty =
				DependencyProperty.Register("AutomaticUpdateAvailable", typeof(bool), typeof(CtrlFooter), new PropertyMetadata(false));

		private void butCheckUpdates_Click(object sender, RoutedEventArgs e)
		{
			CheckForUpdate?.Invoke();
		}
		#endregion

		#region Constructor

		public CtrlFooter()
		{
			InitializeComponent();

			switchingTimer.Stop();
			switchingTimer.Tick += new EventHandler(SwitchTransitioner);
			switchingTimer.Interval = new TimeSpan(0, 0, 5);
		}

		#endregion

		#region Init

		public void Init()
		{
			//setto i dati di default
			SetSessionData(null);

			InitBcr();

			_context.XmlClient.OnConnectedChanged += XmlClientOnConnectedChanged;
			_hmsConnected = _context.XmlClient.Connected;

			TxtXmlClientIp.Text = appconfiguration::ConfigurationManager.Parameters.XmlClientConnectionSetting.IpAddress;
			TxtXmlServerIp.Text = appconfiguration::ConfigurationManager.Parameters.XmlServerConnectionSetting.IpAddress;

			_authenticationManager.LoginSession += delegate (object sender, LoginSessionEventArgs e) { SetSessionData(e.Session); };
			_authenticationManager.LogoutSession += delegate (object sender, LogoutSessionEventArgs e) { SetSessionData(e.Session); };

			_cultureManager.CultureChanged += delegate { Translate(); };

			Translate();

			switchingTimer.Start();
		}

		#endregion

		#region Events

		private void SwitchTransitioner(object sender, EventArgs e)
		{
			if (transitioner.SelectedIndex == 0)
				transitioner.SelectedIndex = 1;
			else
				transitioner.SelectedIndex = 0;
		}

    private void XmlClientOnConnectedChanged(object sender, ConnectedChangedEventArgs e)
		{
			Dispatcher.Invoke(() =>
			{
				_hmsConnected = e.Connected;
				SetServerStatus();
			});
		}

		private void btnBcr_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
		{
			TextBlockBcrStatus.Visibility = Visibility.Visible;
		}

		private void btnBcr_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			TextBlockBcrStatus.Visibility = Visibility.Collapsed;
		}

		private void btnKeyboard_Click(object sender, RoutedEventArgs e)
		{
			OnKeyboardClick?.Invoke();
		}

		#endregion

		#region Private Methods

		private void SetServerStatus()
		{
			//TxtServerStatus.Text = Localize.LocalizeDefaultString(_hmsConnected ? "Connected" : "Disconnected").ToUpperInvariant();
			//TxtServerStatus.Background = _hmsConnected ? new SolidColorBrush(Colors.Green) : new SolidColorBrush(Colors.Red);

			IconServerStatus.Foreground = _hmsConnected ? new SolidColorBrush(Colors.Green) : new SolidColorBrush(Colors.Red);
			IconServerStatus.Kind = _hmsConnected ? PackIconKind.Check : PackIconKind.CancelBold;
		}

		private void SetSessionData(ISession session)
		{
			Dispatcher.Invoke(() =>
			{
				var positions = session?.PositionCollection ?? new string[0];

				var isSessionValid = session?.IsActive == true;
				var isLoggedInPos = positions.Any();

				//mostro le etichette solo se la sessione è valida e sono loggato in una posizione
				BorderPosition.Visibility = isSessionValid && isLoggedInPos ? Visibility.Visible : Visibility.Hidden;
				TxtPosition.Text = string.Join(", ", positions).ToUpperInvariant();
			});
		}

		#region BCR

		private void InitBcr()
		{
			_bcrManager.SerialBcrChanged += BcrManagerOnSerialBcrChanged;

			ManageBcr();
		}

		private void BcrManagerOnSerialBcrChanged(object sender, SerialBcrChangedEventArgs e)
		{
			Dispatcher.Invoke(() =>
			{
				//setto i dati a video
				ManageBcr(e.Port);
			});
		}

		private void ManageBcr(string serialPortName = null)
		{
			if (_confParameters.BcrSetting?.SerialBcrSetting.Enabled != true)
			{
				//il bcr non è abilitato, lo nascondo
				//ControlBcr.Visibility = Visibility.Hidden;
				IconBcrStatus.Visibility = Visibility.Visible;
				return;
			}

			_barcodeConnected = !string.IsNullOrEmpty(serialPortName);
			_comPort = serialPortName ?? "";

			SetupBcrOnScreen();
		}

		private void SetupBcrOnScreen()
		{
			if (_confParameters.BcrSetting?.SerialBcrSetting.Enabled != true)
			{
				//il bcr non è abilitato, lo nascondo
				//ControlBcr.Visibility = Visibility.Hidden;
				IconBcrStatus.Visibility = Visibility.Visible;
				return;
			}

			//TextBlockBcr.Text = Localize.LocalizeDefaultString("Barcode Reader").ToUpperInvariant();

			//var status = _barcodeConnected ? $"{Localize.LocalizeDefaultString("Connected")} ({_comPort})" : Localize.LocalizeDefaultString("Not Connected");
			var status = Localize.LocalizeDefaultString(_barcodeConnected ? "Connected".ToUpperInvariant() : "Not Connected".ToUpperInvariant());
			if (_barcodeConnected)
				status = $"{status} ({_comPort})";
			TextBlockBcrStatus.Text = status.ToUpperInvariant();

			var color = _barcodeConnected ? new SolidColorBrush(Colors.LimeGreen) : new SolidColorBrush(Colors.Red);
			//TextBlockBcr.Foreground = color;
			TextBlockBcrStatus.Foreground = color;
			IconBcrStatus.Foreground = color;

			//il bcr è abilitato, lo mostro
			//ControlBcr.Visibility = Visibility.Visible;
			IconBcrStatus.Visibility = Visibility.Visible;
		}

		#endregion

		private void Translate()
		{
			TxtPositionLabel.Text = Localize.LocalizeDefaultString("Position".ToUpperInvariant());
			TxtClientIpLabel.Text = Localize.LocalizeDefaultString("Client IP".ToUpperInvariant());
			TxtServerIpLabel.Text = Localize.LocalizeDefaultString("Server IP".ToUpperInvariant());
			TxtServerStatus.Text = Localize.LocalizeDefaultString("Server Status".ToUpperInvariant()).ToUpperInvariant();

			butCheckUpdates.ToolTip = Localize.LocalizeDefaultString("UPDATE");

			SetServerStatus();

			SetupBcrOnScreen();

			SetupKeyboardVisibility();
		}

    #region Keyboard
		private void SetupKeyboardVisibility()
    {
			// Check Layout Settings for keyboards
			var toolSettings = ConfigurationManager.Parameters.Layout.Where(x => x.Title == "Tools").FirstOrDefault();
			KeyboardVisibility = toolSettings.Settings.Where(x => x.Name == "KeyboardShortcutOnFooter").FirstOrDefault().Value.ConvertToBool();


		}
		#endregion

		#endregion
	}
}