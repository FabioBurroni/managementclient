﻿using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using Configuration;
using System.IO;
using System.Reflection;
using System.Diagnostics;

namespace View.Common.Info
{
  
  public partial class CtrlInfo : CtrlBase
  {
    #region PUBLIC PROPERTIES

    private string applicationFolder;
    public string ApplicationFolder
    {
      get { return applicationFolder; }
      set
      {
        applicationFolder = value;
        NotifyPropertyChanged("ApplicationFolder");
      }
    }

    private string applicationName;
    public string ApplicationName
    {
      get { return applicationName; }
      set 
      { 
        applicationName = value;
        NotifyPropertyChanged("ApplicationName");
      }
    }

    private string applicationPath;
    public string ApplicationPath
    {
      get { return applicationPath; }
      set
      {
        applicationPath = value;
        NotifyPropertyChanged("ApplicationPath");
      }
    }

    private string applicationVersion;
    public string ApplicationVersion
    {
      get { return applicationVersion; }
      set
      {
        applicationVersion = value;
        NotifyPropertyChanged("ApplicationVersion");
      }
    }

    private FileInfo applicationFileInfo;
    public FileInfo ApplicationFileInfo
    {
      get { return applicationFileInfo; }
      set
      {
        applicationFileInfo = value;
        NotifyPropertyChanged("ApplicationFileInfo");
      }
    }

    private string developer;

    public string Developer
    {
      get { return developer; }
      set
      {
        developer = value;
        NotifyPropertyChanged("Developer");
      }
    }

    private string authenticationPath;
    public string AuthenticationPath
    {
      get { return authenticationPath; }
      set
      {
        authenticationPath = value;
        NotifyPropertyChanged("AuthenticationPath");
      }
    }

    private string authenticationVersion;
    public string AuthenticationVersion
    {
      get { return authenticationVersion; }
      set
      {
        authenticationVersion = value;
        NotifyPropertyChanged("AuthenticationVersion");
      }
    }

    private FileInfo authenticationFileInfo;
    public FileInfo AuthenticationFileInfo
    {
      get { return authenticationFileInfo; }
      set
      {
        authenticationFileInfo = value;
        NotifyPropertyChanged("AuthenticationFileInfo");
      }
    }

    private string configurationPath;
    public string ConfigurationPath
    {
      get { return configurationPath; }
      set
      {
        configurationPath = value;
        NotifyPropertyChanged("ConfigurationPath");
      }
    }

    private string configurationVersion;
    public string ConfigurationVersion
    {
      get { return configurationVersion; }
      set
      {
        configurationVersion = value;
        NotifyPropertyChanged("ConfigurationVersion");
      }
    }

    private FileInfo configurationFileInfo;
    public FileInfo ConfigurationFileInfo
    {
      get { return configurationFileInfo; }
      set
      {
        configurationFileInfo = value;
        NotifyPropertyChanged("ConfigurationFileInfo");
      }
    }

    private string extendedUtilitiesPath;
    public string ExtendedUtilitiesPath
    {
      get { return extendedUtilitiesPath; }
      set
      {
        extendedUtilitiesPath = value;
        NotifyPropertyChanged("ExtendedUtilitiesPath");
      }
    }

    private string extendedUtilitiesVersion;
    public string ExtendedUtilitiesVersion
    {
      get { return extendedUtilitiesVersion; }
      set
      {
        extendedUtilitiesVersion = value;
        NotifyPropertyChanged("ExtendedUtilitiesVersion");
      }
    }

    private FileInfo extendedUtilitiesFileInfo;
    public FileInfo ExtendedUtilitiesFileInfo
    {
      get { return extendedUtilitiesFileInfo; }
      set
      {
        extendedUtilitiesFileInfo = value;
        NotifyPropertyChanged("ExtendedUtilitiesFileInfo");
      }
    }

    private string keyPadPath;
    public string KeyPadPath
    {
      get { return keyPadPath; }
      set
      {
        keyPadPath = value;
        NotifyPropertyChanged("KeyPadPath");
      }
    }

    private string keyPadVersion;
    public string KeyPadVersion
    {
      get { return keyPadVersion; }
      set
      {
        keyPadVersion = value;
        NotifyPropertyChanged("KeyPadVersion");
      }
    }

    private FileInfo keyPadFileInfo;
    public FileInfo KeyPadFileInfo
    {
      get { return keyPadFileInfo; }
      set
      {
        keyPadFileInfo = value;
        NotifyPropertyChanged("KeyPadFileInfo");
      }
    }

    private string localizationPath;
    public string LocalizationPath
    {
      get { return localizationPath; }
      set
      {
        localizationPath = value;
        NotifyPropertyChanged("LocalizationPath");
      }
    }

    private string localizationVersion;
    public string LocalizationVersion
    {
      get { return localizationVersion; }
      set
      {
        localizationVersion = value;
        NotifyPropertyChanged("LocalizationVersion");
      }
    }

    private FileInfo localizationFileInfo;
    public FileInfo LocalizationFileInfo
    {
      get { return localizationFileInfo; }
      set
      {
        localizationFileInfo = value;
        NotifyPropertyChanged("LocalizationFileInfo");
      }
    }

    private string modelPath;
    public string ModelPath
    {
      get { return modelPath; }
      set
      {
        modelPath = value;
        NotifyPropertyChanged("ModelPath");
      }
    }

    private string modelVersion;
    public string ModelVersion
    {
      get { return modelVersion; }
      set
      {
        modelVersion = value;
        NotifyPropertyChanged("ModelVersion");
      }
    }

    private FileInfo modelFileInfo;
    public FileInfo ModelFileInfo
    {
      get { return modelFileInfo; }
      set
      {
        modelFileInfo = value;
        NotifyPropertyChanged("ModelFileInfo");
      }
    }


    private string utilitiesPath;
    public string UtilitiesPath
    {
      get { return utilitiesPath; }
      set
      {
        utilitiesPath = value;
        NotifyPropertyChanged("UtilitiesPath");
      }
    }

    private string utilitiesVersion;
    public string UtilitiesVersion
    {
      get { return utilitiesVersion; }
      set
      {
        utilitiesVersion = value;
        NotifyPropertyChanged("UtilitiesVersion");
      }
    }

    private FileInfo utilitiesFileInfo;
    public FileInfo UtilitiesFileInfo
    {
      get { return utilitiesFileInfo; }
      set
      {
        utilitiesFileInfo = value;
        NotifyPropertyChanged("UtilitiesFileInfo");
      }
    }


    private string viewPath;
    public string ViewPath
    {
      get { return viewPath; }
      set
      {
        viewPath = value;
        NotifyPropertyChanged("ViewPath");
      }
    }

    private string viewVersion;
    public string ViewVersion
    {
      get { return viewVersion; }
      set
      {
        viewVersion = value;
        NotifyPropertyChanged("ViewVersion");
      }
    }

    private FileInfo viewFileInfo;
    public FileInfo ViewFileInfo
    {
      get { return viewFileInfo; }
      set
      {
        viewFileInfo = value;
        NotifyPropertyChanged("ViewFileInfo");
      }
    }
    #endregion

    #region CONSTRUCTOR
    public CtrlInfo()
    {
      
      InitializeComponent();

      ApplicationFolder = ConfigurationManager.MainDirectory;
      Developer = "Cassioli s.r.l";
      
      ApplicationName = "ManagementClient";
      
      ApplicationPath = Path.Combine(ConfigurationManager.MainDirectory, ApplicationName + ".exe");
      ApplicationVersion = Assembly.LoadFrom(ApplicationPath).GetName().Version.ToString();
      ApplicationFileInfo = new FileInfo(ApplicationPath);

      AuthenticationPath = Path.Combine(ConfigurationManager.MainDirectory, "Authentication" + ".dll");
      AuthenticationVersion = Assembly.LoadFrom(AuthenticationPath).GetName().Version.ToString();
      AuthenticationFileInfo = new FileInfo(AuthenticationPath);

      ConfigurationPath = Path.Combine(ConfigurationManager.MainDirectory, "Configuration" + ".dll");
      ConfigurationVersion = Assembly.LoadFrom(ConfigurationPath).GetName().Version.ToString();
      ConfigurationFileInfo = new FileInfo(ConfigurationPath);

      ExtendedUtilitiesPath = Path.Combine(ConfigurationManager.MainDirectory, "ExtendedUtilities" + ".dll");
      ExtendedUtilitiesVersion = Assembly.LoadFrom(ExtendedUtilitiesPath).GetName().Version.ToString();
      ExtendedUtilitiesFileInfo = new FileInfo(ExtendedUtilitiesPath);

      KeyPadPath = Path.Combine(ConfigurationManager.MainDirectory, "KeyPad" + ".dll");
      KeyPadVersion = Assembly.LoadFrom(KeyPadPath).GetName().Version.ToString();
      KeyPadFileInfo = new FileInfo(KeyPadPath);

      LocalizationPath = Path.Combine(ConfigurationManager.MainDirectory, "Localization" + ".dll");
      LocalizationVersion = Assembly.LoadFrom(LocalizationPath).GetName().Version.ToString();
      LocalizationFileInfo = new FileInfo(LocalizationPath);

      ModelPath = Path.Combine(ConfigurationManager.MainDirectory, "Model" + ".dll");
      ModelVersion = Assembly.LoadFrom(ModelPath).GetName().Version.ToString();
      ModelFileInfo = new FileInfo(ModelPath);

      UtilitiesPath = Path.Combine(ConfigurationManager.MainDirectory, "Utilities" + ".dll");
      UtilitiesVersion = Assembly.LoadFrom(UtilitiesPath).GetName().Version.ToString();
      UtilitiesFileInfo = new FileInfo(UtilitiesPath);

      ViewPath = Path.Combine(ConfigurationManager.MainDirectory, "View" + ".dll");
      ViewVersion = Assembly.LoadFrom(ViewPath).GetName().Version.ToString();
      ViewFileInfo = new FileInfo(ViewPath);

    }



    #endregion

    private void btnOpenProjectFolder_Click(object sender, RoutedEventArgs e)
    {
      OpenFolder(ApplicationFolder);
    }

    private void OpenFolder(string folderPath)
    {
      if (Directory.Exists(folderPath))
      {
        ProcessStartInfo startInfo = new ProcessStartInfo
        {
          Arguments = folderPath,
          FileName = "explorer.exe"
        };
        Process.Start(startInfo);
      }
      else
      {
       // MainSnackbar.ShowMessageFail(folderPath + " " + "NOT EXISTS".TD(), 2);
      }
    }
  }
}
