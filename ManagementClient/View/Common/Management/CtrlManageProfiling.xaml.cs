﻿using Authentication.GUI.Management;
using Authentication.Profiling;
using MaterialDesignThemes.Wpf;
using Model;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using View.Common.Languages;

namespace View.Common.Management
{
	/// <summary>
	/// Interaction logic for CtrlManageProfiling.xaml
	/// </summary>
	public partial class CtrlManageProfiling : CtrlBase
	{

		private bool _isLoading;
		public bool IsLoading
		{
			get { return _isLoading; }
			set
			{
				_isLoading = value;
				NotifyPropertyChanged();
			}
		}

		private string selectedUserLevelCode;
		public string SelectedUserLevelCode
		{
			get { return selectedUserLevelCode; }
			set
			{
				selectedUserLevelCode = value;
				NotifyPropertyChanged("SelectedUserLevelCode");
				NotifyPropertyChanged("SelectedUserLevelName");
				UpdateProfileOptionList();
			}
		}

		public string SelectedUserLevelName
		{
			get { return SelectedUserLevelCode.TD(); }
		}

		private string selectedApplicationCode;
		public string SelectedApplicationCode
		{
			get { return selectedApplicationCode; }
			set
			{
				selectedApplicationCode = value;
				NotifyPropertyChanged("SelectedApplicationCode");
				NotifyPropertyChanged("SelectedApplicationName");
				UpdateProfileOptionList();
			}
		}

		public string SelectedApplicationName
		{
			get { return SelectedApplicationCode.Replace("_", " ").ToUpperInvariant(); }
		}

		public ObservableCollectionFast<UserStructure> ProfileOptionList { get; set; } = new ObservableCollectionFast<UserStructure>();

		private ManageProfiling ManageProfiling;

		private int authenticationLevel = 0;
		public int AuthenticationLevel
		{
			get { return authenticationLevel; }
			set
			{
				authenticationLevel = value;
				NotifyPropertyChanged("AuthenticationLevel");
			}
		}

		#region Constructor

		public CtrlManageProfiling()
		{
			SelectedUserLevelCode = "SysAdmin";
			SelectedApplicationCode = "Management_Client";

			InitializeComponent();
						
		}

		#endregion

		#region Translate

		protected override void Translate()
		{
			if (!IsInitialized)
				return;

			//HEADER
			txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
			txtBlkDescription.Text = Context.Instance.TranslateDefault((string)txtBlkDescription.Tag);
			butRefresh.ToolTip = Context.Instance.TranslateDefault((string)butRefresh.Tag);

			//left bar user level popup box
			popupBoxUser.ToolTip = Context.Instance.TranslateDefault((string)popupBoxUser.Tag);
			//btnSysAdminSelection button 
			btnSysAdminSelection.ToolTip = Context.Instance.TranslateDefault((string)btnSysAdminSelection.Tag);
			txtBlkSysAdminSelection.Text = Context.Instance.TranslateDefault((string)txtBlkSysAdminSelection.Tag);
			//btnPlantAdminSelection button
			btnPlantAdminSelection.ToolTip = Context.Instance.TranslateDefault((string)btnPlantAdminSelection.Tag);
			txtBlkPlantAdminSelection.Text = Context.Instance.TranslateDefault((string)txtBlkPlantAdminSelection.Tag);
			//btnAdvOperatorSelection button
			btnAdvOperatorSelection.ToolTip = Context.Instance.TranslateDefault((string)btnAdvOperatorSelection.Tag);
			txtBlkAdvOperatorSelection.Text = Context.Instance.TranslateDefault((string)txtBlkAdvOperatorSelection.Tag);
			//btnOperatorSelection button
			btnOperatorSelection.ToolTip = Context.Instance.TranslateDefault((string)btnOperatorSelection.Tag);
			txtBlkOperatorSelection.Text = Context.Instance.TranslateDefault((string)txtBlkOperatorSelection.Tag);

			//left bar client popup box
			popupBoxClient.ToolTip = Context.Instance.TranslateDefault((string)popupBoxClient.Tag);
			//btnManagementClient button 
			btnManagementClient.ToolTip = Context.Instance.TranslateDefault((string)btnManagementClient.Tag);
			txtBlkManagementClient.Text = Context.Instance.TranslateDefault((string)txtBlkManagementClient.Tag);
			//btnDiagnosticManager button
			btnDiagnosticManager.ToolTip = Context.Instance.TranslateDefault((string)btnDiagnosticManager.Tag);
			txtBlkDiagnosticManagerSelection.Text = Context.Instance.TranslateDefault((string)txtBlkDiagnosticManagerSelection.Tag);
			//btnReportManager button
			btnReportManager.ToolTip = Context.Instance.TranslateDefault((string)btnReportManager.Tag);
			txtBlkReportManagerSelection.Text = Context.Instance.TranslateDefault((string)txtBlkReportManagerSelection.Tag);

			//save button
			btnSave.ToolTip = Context.Instance.TranslateDefault((string)btnSave.Tag);

			// central
			HintAssist.SetHint(txtBoxSelectedUserLevel, Localization.Localize.LocalizeDefaultString((string)txtBoxSelectedUserLevel.Tag));
			HintAssist.SetHint(txtBoxSelectedApplication, Localization.Localize.LocalizeDefaultString((string)txtBoxSelectedApplication.Tag));

			//grid
			colAction.Header = Localization.Localize.LocalizeDefaultString("ACTION");
			colDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
			colStandard.Header = Localization.Localize.LocalizeDefaultString("STANDARD");
			colEnabled.Header = Localization.Localize.LocalizeDefaultString("ENABLED");
			colOptionalData.Header = Localization.Localize.LocalizeDefaultString("OPTIONAL DATA");

			if (dgProfilingL.ItemsSource != null)
				CollectionViewSource.GetDefaultView(dgProfilingL.ItemsSource).Refresh();
		}

		#endregion

		#region Control Event


		private void CtrlBase_Loaded(object sender, System.Windows.RoutedEventArgs e)
		{
			Translate();

			SelectedUserLevelCode = "SysAdmin";
			SelectedApplicationCode = "Management_Client";

			IsLoading = true;

			ManageProfiling = new ManageProfiling(Context.XmlClient);
			ManageProfiling.OnListUpdated += ManageProfiling_OnListUpdated;
			ManageProfiling.OnNewMessageToShow += ManageProfiling_OnNewMessageToShow;
			AuthenticationLevel = ManageProfiling.AuthenticationLevel;
		}

		private void CtrlBase_IsVisibleChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
		{
			if (!IsVisible)
			{
				Closing();
			}
		}

		public override void Closing()
		{
			ManageProfiling.Close();
			IsLoading = false;
			base.Closing();
		}
		#endregion
		

		#region Manage profiling event Methods

		private void ManageProfiling_OnNewMessageToShow(string message, int type)
    {
      switch (type)
      {
				case 0:
					LocalSnackbar.ShowMessageInfo(message, 1);
					break;
				case 1:
					LocalSnackbar.ShowMessageOk(message, 1);
					break;
				case 2:
					LocalSnackbar.ShowMessageFail(message, 2);
					break;
      }
			
		}

    private void ManageProfiling_OnListUpdated()
    {
			UpdateProfileOptionList();
		}
		   
		
    #endregion

    #region UI EVENT

    private void btnSave_Click(object sender, System.Windows.RoutedEventArgs e)
    {
			if (ManageProfiling is null)
				return;

			IsLoading = true;

			List<UserStructure> list = new List<UserStructure>();
			foreach (var c in ProfileOptionList)
				list.Add(new UserStructure(c.UserAction, c.UserClient, c.UserProfile, c.Enabled, c.OptionalData));

			ManageProfiling.SaveChanges(list);
    }

		private void butRefresh_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			if (ManageProfiling is null)
				return;
			IsLoading = true;
			ManageProfiling.Reload();
		}

    private void btnManagementClient_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
    {
			imgManagementClient.Opacity = 1;

		}

    private void btnManagementClient_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
    {
			imgManagementClient.Opacity = 0.5;
		}

    private void btnDiagnosticManager_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
    {
			imgDiagnosticManager.Opacity = 1;
		}

    private void btnDiagnosticManager_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
    {
			imgDiagnosticManager.Opacity = 0.5;
		}

		private void btnReportManager_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
		{
			imgReportManager.Opacity = 1;
		}

		private void btnReportManager_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			imgReportManager.Opacity = 0.5;
		}

		private void btnSysAdminSelection_Click(object sender, System.Windows.RoutedEventArgs e)
    {
			SelectedUserLevelCode = "SysAdmin";
		}

    private void btnPlantAdminSelection_Click(object sender, System.Windows.RoutedEventArgs e)
    {
			SelectedUserLevelCode = "PlantAdmin";
		}

    private void btnAdvOperatorSelection_Click(object sender, System.Windows.RoutedEventArgs e)
    {
			SelectedUserLevelCode = "AdvOperator";
		}

    private void btnOperatorSelection_Click(object sender, System.Windows.RoutedEventArgs e)
    {
			SelectedUserLevelCode = "Operator";
		}

    private void btnManagementClient_Click(object sender, System.Windows.RoutedEventArgs e)
    {
			SelectedApplicationCode = "Management_Client";
		}

    private void btnDiagnosticManager_Click(object sender, System.Windows.RoutedEventArgs e)
    {
			SelectedApplicationCode = "Synoptic_Manager";
		}

		private void btnReportManager_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			SelectedApplicationCode = "Report_Manager";
		}
		#endregion

		#region private methods
		
		private void UpdateProfileOptionList()
		{
			ProfileOptionList.Clear();
			if (ManageProfiling is null)
				return;

			List<UserStructure> copy = ManageProfiling.ProfileOptionList.Where(x => x.UserProfileCode.ToLowerInvariant() == SelectedUserLevelCode.ToLowerInvariant() && x.UserClientCode.ToLowerInvariant() == SelectedApplicationCode.ToLowerInvariant()).ToList();

			foreach (var c in copy)
				ProfileOptionList.Add(new UserStructure(c.UserAction, c.UserClient, c.UserProfile, c.Enabled, c.OptionalData));

			IsLoading = false;
		}
    #endregion


	}
}