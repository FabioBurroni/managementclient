﻿using Authentication;
using Authentication.GUI.Management;
using Authentication.Profiling;
using Configuration;
using Localization;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using Model;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Windows.Data;
using Utilities.Extensions;
using View.Common.Languages;

namespace View.Common.Management
{
	/// <summary>
	/// Interaction logic for CtrlManageSessions.xaml
	/// </summary>
	public partial class CtrlManageSessions : CtrlBase
	{
    #region Fields
    public Timer RefreshTimer = new Timer();

		private bool autorefresh;
		private double intervalTime = 60000;
		
		private double elapsedTime;
		public double ElapsedTime
		{
			get { return elapsedTime; }
			set
			{
				elapsedTime = value;
				NotifyPropertyChanged("ElapsedTime");
				NotifyPropertyChanged("RefreshTimeInSeconds");
			}
		}

		public double RefreshTimeInSeconds => ElapsedTime / 1000;

		private bool _isLoading;
		public bool IsLoading
		{
			get { return _isLoading; }
			set
			{
				_isLoading = value;
				NotifyPropertyChanged("IsLoading");
			}
		}

		private string selectedUserLevelCode;
		public string SelectedUserLevelCode
		{
			get { return selectedUserLevelCode; }
			set
			{
				selectedUserLevelCode = value;
				NotifyPropertyChanged("SelectedUserLevelCode");
				NotifyPropertyChanged("SelectedUserLevelName");
			}
		}

		public string SelectedUserLevelName
		{
			get { return SelectedUserLevelCode.TD(); }
			set
      {
				if (value == string.Empty || value is null)
					SelectedUserLevelCode = string.Empty;

			}
		}

		private string selectedApplicationCode;
		public string SelectedApplicationCode
		{
			get { return selectedApplicationCode; }
			set
			{
				selectedApplicationCode = value;
				NotifyPropertyChanged("SelectedApplicationCode");
				NotifyPropertyChanged("SelectedApplicationName");
			}
		}

		public string SelectedApplicationName
		{
			get { return SelectedApplicationCode.Replace("_", " ").ToUpperInvariant(); }
			set
			{
				if (value == string.Empty || value is null)
					SelectedApplicationCode = string.Empty;

			}
		}

		public ObservableCollectionFast<ISession> SessionList { get; set; } = new ObservableCollectionFast<ISession>();

		private ManageSessions ManageSession;

		private int authenticationLevel = 0;
		public int AuthenticationLevel
		{
			get { return authenticationLevel; }
			set
			{
				authenticationLevel = value;
				NotifyPropertyChanged("AuthenticationLevel");
			}
		}

		private ISession selectedSession;
		public ISession SelectedSession
		{
			get { return selectedSession; }
			set
			{
				selectedSession = value;
				NotifyPropertyChanged("SelectedSession");
			}
		}

    #endregion

    #region Constructor

    public CtrlManageSessions()
		{
			SelectedUserLevelCode = string.Empty;
			SelectedApplicationCode = string.Empty;

			InitializeComponent();

			
		}


    #endregion

    #region Translate

    protected override void Translate()
		{
			if (!IsInitialized)
				return;

			//HEADER
			txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
			txtBlkDescription.Text = Context.Instance.TranslateDefault((string)txtBlkDescription.Tag);
			butRefresh.ToolTip = Context.Instance.TranslateDefault((string)butRefresh.Tag);

			//username contains hint
			HintAssist.SetHint(txtBoxUsernameContains, Localization.Localize.LocalizeDefaultString((string)txtBoxUsernameContains.Tag));

			//user level hint
			HintAssist.SetHint(txtBoxSelectedUserLevel, Localization.Localize.LocalizeDefaultString((string)txtBoxSelectedUserLevel.Tag));
			//user level popup box
			popupBoxUser.ToolTip = Context.Instance.TranslateDefault((string)popupBoxUser.Tag);
			//btnSysAdminSelection button 
			btnSysAdminSelection.ToolTip = Context.Instance.TranslateDefault((string)btnSysAdminSelection.Tag);
			txtBlkSysAdminSelection.Text = Context.Instance.TranslateDefault((string)txtBlkSysAdminSelection.Tag);
			//btnPlantAdminSelection button
			btnPlantAdminSelection.ToolTip = Context.Instance.TranslateDefault((string)btnPlantAdminSelection.Tag);
			txtBlkPlantAdminSelection.Text = Context.Instance.TranslateDefault((string)txtBlkPlantAdminSelection.Tag);
			//btnAdvOperatorSelection button
			btnAdvOperatorSelection.ToolTip = Context.Instance.TranslateDefault((string)btnAdvOperatorSelection.Tag);
			txtBlkAdvOperatorSelection.Text = Context.Instance.TranslateDefault((string)txtBlkAdvOperatorSelection.Tag);
			//btnOperatorSelection button
			btnOperatorSelection.ToolTip = Context.Instance.TranslateDefault((string)btnOperatorSelection.Tag);
			txtBlkOperatorSelection.Text = Context.Instance.TranslateDefault((string)txtBlkOperatorSelection.Tag);

			// selected application hint
			HintAssist.SetHint(txtBoxSelectedApplication, Localization.Localize.LocalizeDefaultString((string)txtBoxSelectedApplication.Tag));
			//left bar client popup box
			popupBoxClient.ToolTip = Context.Instance.TranslateDefault((string)popupBoxClient.Tag);
			//btnManagementClient button 
			btnManagementClient.ToolTip = Context.Instance.TranslateDefault((string)btnManagementClient.Tag);
			txtBlkManagementClient.Text = Context.Instance.TranslateDefault((string)txtBlkManagementClient.Tag);
			//btnDiagnosticManager button
			btnDiagnosticManager.ToolTip = Context.Instance.TranslateDefault((string)btnDiagnosticManager.Tag);
			txtBlkDiagnosticManagerSelection.Text = Context.Instance.TranslateDefault((string)txtBlkDiagnosticManagerSelection.Tag);
			//btnReportManager button
			btnReportManager.ToolTip = Context.Instance.TranslateDefault((string)btnReportManager.Tag);
			txtBlkReportManagerSelection.Text = Context.Instance.TranslateDefault((string)txtBlkReportManagerSelection.Tag);

			//source contains hint
			HintAssist.SetHint(txtBoxSourceContains, Localization.Localize.LocalizeDefaultString((string)txtBoxSourceContains.Tag));
			
			//position contains hint
			HintAssist.SetHint(txtBoxPositionContains, Localization.Localize.LocalizeDefaultString((string)txtBoxPositionContains.Tag));

			//CANCEL BUTTON TOOLTIP
			btnCancel.ToolTip = Context.Instance.TranslateDefault((string)btnCancel.Tag);

			//btnSave_Click button tooltip
			btnSearch.ToolTip = Context.Instance.TranslateDefault((string)btnSearch.Tag);

			////grid
			colUsername.Header = Localization.Localize.LocalizeDefaultString("USER");
			colProfileCode.Header = Localization.Localize.LocalizeDefaultString("LEVEL");
			colClientCode.Header = Localization.Localize.LocalizeDefaultString("CLIENT");
			colSourceType.Header = Localization.Localize.LocalizeDefaultString("SOURCE");
			colIPAddress.Header = Localization.Localize.LocalizeDefaultString("IP ADDRESS");
			colPortNumber.Header = Localization.Localize.LocalizeDefaultString("PORT NUMBER");
			colLastActivity.Header = Localization.Localize.LocalizeDefaultString("LAST ACTIVITY");
			colRootToken.Header = Localization.Localize.LocalizeDefaultString("ROOT TOKEN");
			colChildToken.Header = Localization.Localize.LocalizeDefaultString("CHILD TOKEN");
			colPosition.Header = Localization.Localize.LocalizeDefaultString("POSITION");

			if (dgSessionL.ItemsSource != null)
				CollectionViewSource.GetDefaultView(dgSessionL.ItemsSource).Refresh();
		}

		#endregion

		#region Control Event
    
		private void RefreshTimer_Elapsed(object sender, ElapsedEventArgs e)
    {
			ElapsedTime -= RefreshTimer.Interval;
			if (ElapsedTime <= 0)
      {
				ElapsedTime = intervalTime;
				if (ManageSession is null)
					return;
				IsLoading = true;
				ManageSession.Reload();
			}
    }

		private void CtrlBase_Loaded(object sender, System.Windows.RoutedEventArgs e)
		{
			Translate();
			
			IsLoading = true;

			//get parameters...
			var userManagementSettings = ConfigurationManager.Parameters.Layout.Where(x => x.Title == "User Management").FirstOrDefault();
			bool enableAutoRefresh = userManagementSettings.Settings.Where(x => x.Name == "ManageSessionsAutoRefresh").FirstOrDefault().Value.ConvertToBool();
			int durationAutoRefresh = userManagementSettings.Settings.Where(x => x.Name == "ManageSessionsAutoRefreshDuration").FirstOrDefault().Value.ConvertToInt();

			if (durationAutoRefresh >= 5)
				intervalTime = durationAutoRefresh * 1000;

			autorefresh = enableAutoRefresh;

			RefreshTimer = new Timer();
			RefreshTimer.Interval = 1000;
			RefreshTimer.AutoReset = true;
			RefreshTimer.Elapsed += RefreshTimer_Elapsed;
			ElapsedTime = intervalTime;

			if (autorefresh)
			{				
				RefreshTimer.Start();

				iconRefresh.Visibility = System.Windows.Visibility.Hidden;
				busyOverlayViewBox.Visibility = System.Windows.Visibility.Visible;
			}
			else
      {
				iconRefresh.Visibility = System.Windows.Visibility.Visible;
				busyOverlayViewBox.Visibility = System.Windows.Visibility.Hidden;
			}

			ManageSession = new ManageSessions(Context.XmlClient);
			ManageSession.OnListUpdated += ManageSession_OnListUpdated;
			ManageSession.OnNewMessageToShow += ManageSession_OnNewMessageToShow;
			AuthenticationLevel = ManageSession.AuthenticationLevel;


			if (ManageSession is null)
				return;
			ManageSession.Reload();
		}

		private void CtrlBase_IsVisibleChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
		{
			if (!IsVisible)
			{
				Closing();
			}
		}

		public override void Closing()
		{
			ElapsedTime = intervalTime;
			if (RefreshTimer != null)
			{
				RefreshTimer.Stop();
				RefreshTimer.Elapsed -= RefreshTimer_Elapsed;
				RefreshTimer = null;
			}

			ManageSession.Close();
			IsLoading = false;
			base.Closing();
		}

		#endregion


		#region Manage session event Methods

		private void ManageSession_OnNewMessageToShow(string message, int type)
		{
			switch (type)
			{
				case 0:
					LocalSnackbar.ShowMessageInfo(message, 1);
					break;
				case 1:
					LocalSnackbar.ShowMessageOk(message, 1);
					break;
				case 2:
					LocalSnackbar.ShowMessageFail(message, 2);
					break;
			}

		}

		private void ManageSession_OnListUpdated()
		{
			UpdateSessionList();
		}


		#endregion

		#region UI EVENT

		private void btnCancel_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			txtBoxUsernameContains.Text = string.Empty;
			SelectedUserLevelName = string.Empty;
			SelectedApplicationName = string.Empty;
			txtBoxSourceContains.Text = string.Empty;
			txtBoxPositionContains.Text = string.Empty;
		}

		private void btnSearch_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			if (ManageSession is null)
				return;
			IsLoading = true;
			ManageSession.Reload();
		}

		private async void btnLogout_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			if (SelectedSession != null)
			{
				ConfirmationDialogArguments dialogArgs = new ConfirmationDialogArguments
				{
					Title = "LOGOUT".TD(),
					Message = Localize.LocalizeAuthenticationString("Are You Sure To Force Logout For Selected Sessions?"),
					OkButtonLabel = "LOGOUT".TD(),
					CancelButtonLabel = "CANCEL".TD(),
					StackedButtons = false
				};

				bool result = await ConfirmationDialog.ShowDialogAsync("RootDialogWithoutExit", dialogArgs);
				if (!result)
				{
					LocalSnackbar.ShowMessageInfo("CANCELLED".TD(), 2);
					return;
				}
				List<int> logoutList = new List<int>();
				logoutList.Add(SelectedSession.SessionTokens.ChildToken);
				ManageSession.Logout(logoutList);
			}
			else
			{
				LocalSnackbar.ShowMessageFail("SELECT ELEMENT FROM LIST".TD(), 3);
			}
		}

		private void butRefresh_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			if (ManageSession is null)
				return;
			IsLoading = true;
			ManageSession.Reload();
		}

		private void butRefresh_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
		{
			iconRefresh.Visibility = System.Windows.Visibility.Visible;
			busyOverlayViewBox.Visibility = System.Windows.Visibility.Hidden;
		}

		private void butRefresh_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			if (!autorefresh)
				return;

			iconRefresh.Visibility = System.Windows.Visibility.Hidden;
			busyOverlayViewBox.Visibility = System.Windows.Visibility.Visible;
		}

		private void btnManagementClient_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
		{
			imgManagementClient.Opacity = 1;
		}

		private void btnManagementClient_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			imgManagementClient.Opacity = 0.5;
		}

		private void btnDiagnosticManager_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
		{
			imgDiagnosticManager.Opacity = 1;
		}

		private void btnDiagnosticManager_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			imgDiagnosticManager.Opacity = 0.5;
		}

		private void btnReportManager_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
		{
			imgReportManager.Opacity = 1;
		}

		private void btnReportManager_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			imgReportManager.Opacity = 0.5;
		}

		private void btnSysAdminSelection_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			SelectedUserLevelCode = "SysAdmin";
		}

		private void btnPlantAdminSelection_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			SelectedUserLevelCode = "PlantAdmin";
		}

		private void btnAdvOperatorSelection_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			SelectedUserLevelCode = "AdvOperator";
		}

		private void btnOperatorSelection_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			SelectedUserLevelCode = "Operator";
		}

		private void btnManagementClient_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			SelectedApplicationCode = "Management_Client";
		}

		private void btnDiagnosticManager_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			SelectedApplicationCode = "Synoptic_Manager";
		}

		private void btnReportManager_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			SelectedApplicationCode = "Report_Manager";
		}
		#endregion

		#region private methods

		private void UpdateSessionList()
		{
			SessionList.Clear();
			if (ManageSession is null)
				return;

			List<ISession> sessionL = new List<ISession>();
			sessionL.AddRange(ManageSession.SessionList);

			//FILTER SESSION			
			if (txtBoxUsernameContains.Text != null && txtBoxUsernameContains.Text != string.Empty)
				sessionL = sessionL.Where(x => x.User.Username.ToLowerInvariant().Contains(txtBoxUsernameContains.Text.ToLowerInvariant())).ToList();

			if (SelectedUserLevelName != null && SelectedUserLevelName != string.Empty)
				sessionL = sessionL.Where(x => x.User.UserProfileCode.ToLowerInvariant() == SelectedUserLevelName.ToLowerInvariant()).ToList();

			if (SelectedApplicationCode != null && SelectedApplicationCode != string.Empty)
				sessionL = sessionL.Where(x => x.Source.UserClientCode.ToLowerInvariant() == SelectedApplicationCode.ToLowerInvariant()).ToList();

			if (txtBoxSourceContains.Text != null && txtBoxSourceContains.Text != string.Empty)
				sessionL = sessionL.Where(x => x.User.Username.ToLowerInvariant().Contains(txtBoxSourceContains.Text.ToLowerInvariant())).ToList();

			if (txtBoxPositionContains.Text != null && txtBoxPositionContains.Text != string.Empty)
				sessionL = sessionL.Where(x => x.User.Username.ToLowerInvariant().Contains(txtBoxPositionContains.Text.ToLowerInvariant())).ToList();

			SessionList.AddRange(sessionL);

			IsLoading = false;

			LocalSnackbar.ShowMessageOk("LIST UPDATED".TD(), 3);

			ElapsedTime = intervalTime;
		}
		
		#endregion
		   
  }
}