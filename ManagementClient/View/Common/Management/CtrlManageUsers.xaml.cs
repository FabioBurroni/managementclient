﻿using Authentication.GUI.Management;
using Configuration;
using Localization;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using Model;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Windows.Data;
using Utilities.Extensions;
using View.Common.Languages;
using static Authentication.GUI.Management.ManageUsers;

namespace View.Common.Management
{
  /// <summary>
  /// Interaction logic for CtrlManageUsers.xaml
  /// </summary>
  public partial class CtrlManageUsers : CtrlBase
  {
    #region Fields

    public Timer RefreshTimer = new Timer();

    private bool autorefresh;
    private double intervalTime = 60000;

    private double elapsedTime;

    public double ElapsedTime
    {
      get { return elapsedTime; }
      set
      {
        elapsedTime = value;
        NotifyPropertyChanged("ElapsedTime");
        NotifyPropertyChanged("RefreshTimeInSeconds");
      }
    }

    public double RefreshTimeInSeconds => ElapsedTime / 1000;

    private bool _isLoading;

    public bool IsLoading
    {
      get { return _isLoading; }
      set
      {
        _isLoading = value;
        NotifyPropertyChanged("IsLoading");
      }
    }

    public ObservableCollectionFast<User> UserList { get; set; } = new ObservableCollectionFast<User>();

    private ManageUsers ManageUsers;

    private int authenticationLevel = 0;

    public int AuthenticationLevel
    {
      get { return authenticationLevel; }
      set
      {
        authenticationLevel = value;
        NotifyPropertyChanged("AuthenticationLevel");
      }
    }

    private User selectedUser;

    public User SelectedUser
    {
      get { return selectedUser; }
      set
      {
        selectedUser = value;

        if (string.IsNullOrEmpty(selectedUser?.Password))
          txtPassword.Password = "**********";
        NotifyPropertyChanged("SelectedUser");
      }
    }

    private User newUser;

    public User NewUser
    {
      get { return newUser; }
      set
      {
        newUser = value;
        NotifyPropertyChanged("NewUser");
      }
    }

    #endregion Fields

    #region Constructor

    public CtrlManageUsers()
    {
      InitializeComponent();

      NewUser = new User();
      SelectedUser = new User();
    }

    #endregion Constructor

    #region Translate

    protected override void Translate()
    {
      if (!IsInitialized)
        return;

      //HEADER
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkDescription.Text = Context.Instance.TranslateDefault((string)txtBlkDescription.Tag);
      butRefresh.ToolTip = Context.Instance.TranslateDefault((string)butRefresh.Tag);

      //username contains hint
      HintAssist.SetHint(txtBoxUsernameContains, Localization.Localize.LocalizeDefaultString((string)txtBoxUsernameContains.Tag));
      HintAssist.SetHint(txtBoxNameContains, Localization.Localize.LocalizeDefaultString((string)txtBoxNameContains.Tag));
      HintAssist.SetHint(cmbLevel, Localization.Localize.LocalizeDefaultString((string)cmbLevel.Tag));
      HintAssist.SetHint(txtBoxCompanyContains, Localization.Localize.LocalizeDefaultString((string)txtBoxCompanyContains.Tag));
      txtBlkBlocked.Text = Context.Instance.TranslateDefault((string)txtBlkBlocked.Tag);

      //CANCEL BUTTON TOOLTIP
      btnCancel.ToolTip = Context.Instance.TranslateDefault((string)btnCancel.Tag);
      btnSearch.ToolTip = Context.Instance.TranslateDefault((string)btnSearch.Tag);

      ////grid
      colUsername.Header = Localization.Localize.LocalizeDefaultString("USER");
      colName.Header = Localization.Localize.LocalizeDefaultString("NAME");
      colProfileCode.Header = Localization.Localize.LocalizeDefaultString("LEVEL");
      colCompany.Header = Localization.Localize.LocalizeDefaultString("COMPANY");
      colBlocked.Header = Localization.Localize.LocalizeDefaultString("BLOCKED");

      tiCreateUser.Text = Context.Instance.TranslateDefault((string)tiCreateUser.Tag);
      txtBlkUsernameNew.Text = Context.Instance.TranslateDefault((string)txtBlkUsernameNew.Tag);
      txtBlkFullnameNew.Text = Context.Instance.TranslateDefault((string)txtBlkFullnameNew.Tag);
      txtBlkPasswordNew.Text = Context.Instance.TranslateDefault((string)txtBlkPasswordNew.Tag);
      txtBlkCompanyNew.Text = Context.Instance.TranslateDefault((string)txtBlkCompanyNew.Tag);
      txtBlkProfileNew.Text = Context.Instance.TranslateDefault((string)txtBlkProfileNew.Tag);
      txtBlkBlockedNew.Text = Context.Instance.TranslateDefault((string)txtBlkBlockedNew.Tag);
      btnCencelNew.Content = Context.Instance.TranslateDefault((string)btnCencelNew.Tag);
      btnSaveNew.Content = Context.Instance.TranslateDefault((string)btnSaveNew.Tag);

      tiModifyUser.Text = Context.Instance.TranslateDefault((string)tiModifyUser.Tag);
      txtBlkUsername.Text = Context.Instance.TranslateDefault((string)txtBlkUsername.Tag);
      txtBlkFullname.Text = Context.Instance.TranslateDefault((string)txtBlkFullname.Tag);
      txtBlkPassword.Text = Context.Instance.TranslateDefault((string)txtBlkPassword.Tag);
      txtBlkCompany.Text = Context.Instance.TranslateDefault((string)txtBlkCompany.Tag);
      txtBlkProfile.Text = Context.Instance.TranslateDefault((string)txtBlkProfile.Tag);
      txtBlkBlocked2.Text = Context.Instance.TranslateDefault((string)txtBlkBlocked.Tag);
      btnDelete.Content = Context.Instance.TranslateDefault((string)btnDelete.Tag);
      btnModify.Content = Context.Instance.TranslateDefault((string)btnModify.Tag);

      if (dgSessionL.ItemsSource != null)
        CollectionViewSource.GetDefaultView(dgSessionL.ItemsSource).Refresh();
    }

    #endregion Translate

    #region Control Event

    private void RefreshTimer_Elapsed(object sender, ElapsedEventArgs e)
    {
      ElapsedTime -= RefreshTimer.Interval;
      if (ElapsedTime <= 0)
      {
        ElapsedTime = intervalTime;
        if (ManageUsers is null)
          return;
        IsLoading = true;
        ManageUsers.Reload();
      }
    }

    private void CtrlBase_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
      Translate();

      IsLoading = true;

      //get parameters...
      var userManagementSettings = ConfigurationManager.Parameters.Layout.Where(x => x.Title == "User Management").FirstOrDefault();
      bool enableAutoRefresh = userManagementSettings.Settings.Where(x => x.Name == "ManageSessionsAutoRefresh").FirstOrDefault().Value.ConvertToBool();
      int durationAutoRefresh = userManagementSettings.Settings.Where(x => x.Name == "ManageSessionsAutoRefreshDuration").FirstOrDefault().Value.ConvertToInt();

      if (durationAutoRefresh >= 5)
        intervalTime = durationAutoRefresh * 1000;

      autorefresh = enableAutoRefresh;

      RefreshTimer = new Timer();
      RefreshTimer.Interval = 1000;
      RefreshTimer.AutoReset = true;
      RefreshTimer.Elapsed += RefreshTimer_Elapsed;
      ElapsedTime = intervalTime;

      if (autorefresh)
      {
        RefreshTimer.Start();

        iconRefresh.Visibility = System.Windows.Visibility.Hidden;
        busyOverlayViewBox.Visibility = System.Windows.Visibility.Visible;
      }
      else
      {
        iconRefresh.Visibility = System.Windows.Visibility.Visible;
        busyOverlayViewBox.Visibility = System.Windows.Visibility.Hidden;
      }
      ManageUsers = new ManageUsers(Context.XmlClient, ConfigurationManager.Parameters.AuthenticationSetting.Customer.Company);
      ManageUsers.OnUsersUpdated += ManageUsers_OnUsersUpdated;
      ManageUsers.OnUserProfilesUpdated += ManageUsers_OnUserProfilesUpdated;
      ManageUsers.OnNewMessageToShow += ManageUsers_OnNewMessageToShow;

      if (ManageUsers is null)
        return;
      ManageUsers.Reload();
    }

    private void ManageUsers_OnNewMessageToShow(string message, int type)
    {
      switch (type)
      {
        case 0:
          LocalSnackbar.ShowMessageInfo(message, 1);
          break;

        case 1:
          LocalSnackbar.ShowMessageOk(message, 1);
          break;

        case 2:
          LocalSnackbar.ShowMessageFail(message, 2);
          break;
      }
    }

    private void ManageUsers_OnUserProfilesUpdated()
    {
      if (ManageUsers is null)
        return;

      cmbProfile.ItemsSource = ManageUsers.UserProfiles.Select(s => s.Code).ToList();
      cmbProfileNew.ItemsSource = ManageUsers.UserProfiles.Select(s => s.Code).ToList();
      cmbLevel.ItemsSource = ManageUsers.UserProfiles.Select(s => s.Code).ToList();
    }

    private void ManageUsers_OnUsersUpdated()
    {
      UpdateSessionList();
    }

    private void CtrlBase_IsVisibleChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
    {
      if (!IsVisible)
      {
        Closing();
      }
    }

    public override void Closing()
    {
      ElapsedTime = intervalTime;
      if (RefreshTimer != null)
      {
        RefreshTimer.Stop();
        RefreshTimer.Elapsed -= RefreshTimer_Elapsed;
        RefreshTimer = null;
      }

      ManageUsers.Close();
      IsLoading = false;
      base.Closing();
    }

    #endregion Control Event

    #region UI EVENT

    private void btnCancel_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      txtBoxUsernameContains.Text = string.Empty;
      txtBoxNameContains.Text = string.Empty;
      txtBoxCompanyContains.Text = string.Empty;
      btnBlocked.IsChecked = false;
      cmbLevel.SelectedItem = null;
    }

    private void btnSearch_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      if (ManageUsers is null)
        return;
      IsLoading = true;
      ManageUsers.Reload();
    }

    private void butRefresh_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      if (ManageUsers is null)
        return;
      IsLoading = true;
      ManageUsers.Reload();
    }

    private void butRefresh_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
    {
      iconRefresh.Visibility = System.Windows.Visibility.Visible;
      busyOverlayViewBox.Visibility = System.Windows.Visibility.Hidden;
    }

    private void butRefresh_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
    {
      if (!autorefresh)
        return;

      iconRefresh.Visibility = System.Windows.Visibility.Hidden;
      busyOverlayViewBox.Visibility = System.Windows.Visibility.Visible;
    }

    private void btnSaveNew_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      NewUser.Password = txtPasswordNew.Password;
      ManageUsers.CreateOrUpdateUser(NewUser, true);
    }

    private void btnCencelNew_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      btnBlockedNew.IsChecked = false;
      cmbProfileNew.SelectedItem = null;
      txtCompanyNew.Text = "";
      txtFullnameNew.Text = "";
      txtPasswordNew.Password = "";
      txtUsernameNew.Text = "";
    }

    private async void btnDelete_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      ConfirmationDialogArguments dialogArgs = new ConfirmationDialogArguments
      {
        Title = "DELETE".TD(),
        Message = Localize.LocalizeAuthenticationString("Are You Sure To Delete This User?"),
        OkButtonLabel = "YES".TD(),
        CancelButtonLabel = "NO".TD(),
        StackedButtons = false
      };

      bool result = await ConfirmationDialog.ShowDialogAsync("RootDialogWithoutExit", dialogArgs);
      if (!result)
      {
        LocalSnackbar.ShowMessageInfo("CANCELLED".TD(), 2);
        return;
      }

      ManageUsers.DeleteUser(SelectedUser.Username);
    }

    private void btnModify_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      SelectedUser.Password = txtPassword.Password;
      ManageUsers.CreateOrUpdateUser(SelectedUser, false);
    }

    #endregion UI EVENT

    #region private methods

    private void UpdateSessionList()
    {
      UserList.Clear();

      if (ManageUsers is null)
        return;

      List<User> userL = new List<User>();
      userL.AddRange(ManageUsers.Users);

      //FILTER SESSION

      if (txtBoxUsernameContains.Text != null && txtBoxUsernameContains.Text != string.Empty)
        userL = userL.Where(x => x.Username.ToLowerInvariant().Contains(txtBoxUsernameContains.Text.ToLowerInvariant())).ToList();

      if (txtBoxNameContains.Text != null && txtBoxNameContains.Text != string.Empty)
        userL = userL.Where(x => x.Fullname.ToLowerInvariant().Contains(txtBoxUsernameContains.Text.ToLowerInvariant())).ToList();

      if (txtBoxCompanyContains.Text != null && txtBoxCompanyContains.Text != string.Empty)
        userL = userL.Where(x => x.Company.ToLowerInvariant().Contains(txtBoxCompanyContains.Text.ToLowerInvariant())).ToList();

      if (cmbLevel.SelectedItem != null)
        userL = userL.Where(x => x.Profilecode.ToLowerInvariant() == cmbLevel.SelectedItem.ToString().ToLowerInvariant()).ToList();

      if (btnBlocked != null && btnBlocked.IsChecked != false)
        userL = userL.Where(x => x.Blocked == btnBlocked.IsChecked).ToList();

      UserList.AddRange(userL);

      LocalSnackbar.ShowMessageOk("LIST UPDATED".TD(), 3);

      IsLoading = false;

      ElapsedTime = intervalTime;
    }

    #endregion private methods
  }
}