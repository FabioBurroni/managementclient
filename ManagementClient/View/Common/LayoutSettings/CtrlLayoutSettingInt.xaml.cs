﻿using System.Windows;
using Model;
using Model.Common.LayoutSetting;

namespace View.Common.LayoutSettings
{
  /// <summary>
  /// Interaction logic for CtrlConf.xaml
  /// </summary>
  public partial class CtrlLayoutSettingInt : CtrlBase, ICtrlLayoutSetting
  {

    public CtrlLayoutSettingInt()
    {
      InitializeComponent();
    }

    #region TRADUZIONI

    protected override void Translate()
    {
      if (txtBlkTitle.Tag != null)
        txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      if (txtBlkDescr.Tag != null)
        txtBlkDescr.Text = Context.Instance.TranslateDefault((string)txtBlkDescr.Tag);
    }

    #endregion TRADUZIONI


    public LayoutSettingParameter ConfParameter
    {
      get { return (LayoutSettingParameter)GetValue(ConfParameterProperty); }
      set { SetValue(ConfParameterProperty, value); }
    }

    // Using a DependencyProperty as the backing store for ConfParameter.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ConfParameterProperty =
        DependencyProperty.Register("ConfParameter", typeof(LayoutSettingParameter), typeof(CtrlLayoutSettingInt), new PropertyMetadata(null));

    public event OnConfirmHandler OnConfirm;
    public event OnCancelHandler OnCancel;

    private void butCancel_Click(object sender, RoutedEventArgs e)
    {
      OnCancel?.Invoke(ConfParameter);
    }

    private void butConfirm_Click(object sender, RoutedEventArgs e)
    {
      OnConfirm?.Invoke(ConfParameter);
    }

    private void butDefault_Click(object sender, RoutedEventArgs e)
    {
      ConfParameter.SetDefault();
    }

    private void CtrlBase_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
  }
}
