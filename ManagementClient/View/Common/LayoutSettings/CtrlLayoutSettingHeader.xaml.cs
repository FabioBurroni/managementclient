﻿using System.Windows;
using Model;

namespace View.Common.LayoutSettings
{
  /// <summary>
  /// Interaction logic for CtrlConf.xaml
  /// </summary>
  public partial class CtrlLayoutSettingHeader : CtrlBase
  {
    #region Dependency properties
    public string Icon
    {
      get { return (string)GetValue(IconProperty); }
      set { SetValue(IconProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Icon.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty IconProperty =
        DependencyProperty.Register("Icon", typeof(string), typeof(CtrlLayoutSettingHeader), new PropertyMetadata(null));

    public string Title
    {
      get { return (string)GetValue(TitleProperty); }
      set { SetValue(TitleProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty TitleProperty =
        DependencyProperty.Register("Title", typeof(string), typeof(CtrlLayoutSettingHeader), new PropertyMetadata(null));

    public string Description
    {
      get { return (string)GetValue(DescriptionProperty); }
      set { SetValue(DescriptionProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty DescriptionProperty =
        DependencyProperty.Register("Description", typeof(string), typeof(CtrlLayoutSettingHeader), new PropertyMetadata(null));


    public Visibility Visible
    {
      get { return (Visibility)GetValue(VisibleProperty); }
      set { SetValue(VisibleProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Visible.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty VisibleProperty =
        DependencyProperty.Register("Visible", typeof(Visibility), typeof(CtrlLayoutSettingHeader), new PropertyMetadata(null));

    #endregion



    public CtrlLayoutSettingHeader()
    {
      InitializeComponent();

    }

    #region TRADUZIONI

    protected override void Translate()
    {
      if (txtBlkTitle.Tag != null)
        txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      if (Description != null && txtBlkTitle.Text != null)
        txtBlkTitle.ToolTip = Context.Instance.TranslateDefault((string)Description);
    }

    #endregion TRADUZIONI

    private void CtrlBase_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
  }
}
