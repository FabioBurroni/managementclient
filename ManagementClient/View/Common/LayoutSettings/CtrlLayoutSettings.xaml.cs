﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Configuration;
using Configuration.Settings.UI;
using Model.Common.LayoutSetting;
using Utilities.Extensions;

namespace View.Common.LayoutSettings
{
  
  public partial class CtrlLayoutSettings : CtrlBase
  {
    #region FIELDS
    List<LayoutSettingParameter> _paramList = new List<LayoutSettingParameter>();
    #endregion
  
    #region CONSTRUCTOR
    public CtrlLayoutSettings()
    {      
      InitializeComponent();

      Init();

    }

    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      //txtBlkTitleConfPar.Text = Context.Instance.TranslateDefault((string)txtBlkTitleConfPar.Tag);
      //txtBlkTitlePallRed.Text = Context.Instance.TranslateDefault((string)txtBlkTitlePallRed.Tag);

      //txtBlkHeaderMaintenance.Text = Context.Instance.TranslateDefault((string)txtBlkHeaderMaintenance.Tag);
      //txtBlkPalletCode.Text = Context.Instance.TranslateDefault((string)txtBlkPalletCode.Tag);
      //txtBlkDestination.Text = Context.Instance.TranslateDefault((string)txtBlkDestination.Tag);
      //butRededtinateConfirm.Content = Context.Instance.TranslateDefault((string)butRededtinateConfirm.Tag);

      //txtBlkHeaderConfig.Text = Context.Instance.TranslateDefault((string)txtBlkHeaderConfig.Tag);


      //CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI
    

    private void Init()
    {
      ConfigurationReader reader = new ConfigurationReader(ConfigurationManager.Parameters);
      reader.LoadConfiguration(ConfigurationManager.LayoutFile);

      wp.Children.Clear();

      foreach (LayoutSettingSection settingSection in ConfigurationManager.Parameters.Layout)
      {
        CtrlLayoutSettingHeader head = new CtrlLayoutSettingHeader();
        
        head.Title = settingSection.Title;
        head.Description = settingSection.Descr;
        head.Icon = settingSection.Icon;
        head.Visibility = GetVisibilityFromString(settingSection.Visible);

        if (head.Visibility != Visibility.Visible)
          continue;

        wp.Children.Add(head);

        List<LayoutSetting> Settings = settingSection.Settings;

        foreach (LayoutSetting s in Settings)
        {
          LayoutSettingParameter p;
          CtrlLayoutSettingString ctrlString = new CtrlLayoutSettingString();
          CtrlLayoutSettingBool ctrlBool = new CtrlLayoutSettingBool();
          CtrlLayoutSettingInt ctrlInt = new CtrlLayoutSettingInt();
          CtrlLayoutSettingCombo ctrlCombo = new CtrlLayoutSettingCombo();

          switch (s.Type)
        {
          case "bool":
            p = new LayoutSettingBool()
            {
              Title = s.Title,
              Name = s.Name,
              Icon = s.Icon,
              Descr = s.Descr,
              Value = s.Value.ConvertToBool(),
              OriginalValue = s.Value,
              DefaultValue = s.Default.ConvertToBool(),
              Section = settingSection.Title
            };

            ctrlBool.ConfParameter = p;
            ctrlBool.OnCancel += Ctrl_OnCancel;
            ctrlBool.OnConfirm += Ctrl_OnConfirm;
            wp.Children.Add(ctrlBool);
            break;

          case "value":
            p = new LayoutSettingInt()
            {
              Title = s.Title,
              Name = s.Name,
              Icon = s.Icon,
              Descr = s.Descr,
              Value = s.Value.ConvertToInt(),
              OriginalValue = s.Value,
              DefaultValue = s.Default.ConvertToInt(),
              Section = settingSection.Title
            };

            ctrlInt.ConfParameter = p;
            ctrlInt.OnCancel += Ctrl_OnCancel;
            ctrlInt.OnConfirm += Ctrl_OnConfirm;
            wp.Children.Add(ctrlInt);
            break;

          case "visibility":
            p = new LayoutSettingString()
            {
              Title = s.Title,
              Name = s.Name,
              Icon = s.Icon,
              Descr = s.Descr,
              //Value = GetVisibilityFromString(s.Value),
              //DefaultValue = GetVisibilityFromString(s.Default)
              Value = s.Value,
              OriginalValue = s.Value,
              DefaultValue = s.Default,
              Section = settingSection.Title
            };
            
            ctrlString.ConfParameter = p;
            ctrlString.OnCancel += Ctrl_OnCancel;
            ctrlString.OnConfirm += Ctrl_OnConfirm;
            wp.Children.Add(ctrlString);
            break;

          case "string":
            p = new LayoutSettingString()
            {
              Title = s.Title,
              Name = s.Name,
              Icon = s.Icon,
              Descr = s.Descr,
              Value = s.Value,
              OriginalValue = s.Value,
              DefaultValue = s.Default,
              Section = settingSection.Title
            };

            ctrlString.ConfParameter = p;
            ctrlString.OnCancel += Ctrl_OnCancel;
            ctrlString.OnConfirm += Ctrl_OnConfirm;
            wp.Children.Add(ctrlString);
            break;


              //case "enum":
              //  p = new LayoutSettingOption()
              //  {
              //    Title = s.Title,
              //    Name = s.Name,
              //    Icon = s.Icon,
              //    Descr = s.Descr,
              //    Value = s.Value,
              //    DefaultValue = s.Default,
             // Section = settingSection.Title
          //  };

              //  ctrlCombo.ConfParameter = p;
              //  ctrlCombo.OnCancel += Ctrl_OnCancel;
              //  ctrlCombo.OnConfirm += Ctrl_OnConfirm;
              //  wp.Children.Add(ctrlCombo);
              //  break;
          }
       
        }

        Separator sep = new Separator();
        double margin = 10;
        sep.Margin = new Thickness(0, margin, 0, margin);
        wp.Children.Add(sep);

      }

    }

    private Visibility GetVisibilityFromString(string value)
    {
      switch (value.ToLowerInvariant())
      {
        case "visible":
          return Visibility.Visible;

        case "true":
          return Visibility.Visible;

        case "hidden":
          return Visibility.Hidden;

        case "collapsed":
          return Visibility.Collapsed;

        case "false":
          return Visibility.Collapsed;
      }
      return Visibility.Collapsed;
    }

    private void GetConfigurationValue(LayoutSettingParameter confPar)
    {
      foreach (LayoutSettingSection settingSection in ConfigurationManager.Parameters.Layout)
      {
        if (confPar.Section == settingSection.Title)
        {
          List<LayoutSetting> Settings = settingSection.Settings;

          foreach (LayoutSetting s in Settings)
          {
            if (s.Name == confPar.Name)
              switch (s.Type)
              {
                case "bool":
                  confPar.Value = s.Value.ConvertToBool();
                  break;

                case "value":
                  confPar.Value = s.Value.ConvertToInt();
                  break;

                case "visibility":
                  confPar.Value = s.Value;
                  break;

                case "string":
                  confPar.Value = s.Value;
                  break;
              }
          }
        }
      }
    }
    

    #region eventi
    private void Ctrl_OnConfirm(LayoutSettingParameter confPar)
    {
      if (ConfigurationWriter.SetLayoutValue(confPar.Section, confPar.Name, confPar.SelectedValue))
      {
        confPar.OriginalValue = confPar.SelectedValue;
      }
    }

    private void Ctrl_OnCancel(LayoutSettingParameter confPar)
    {
      GetConfigurationValue(confPar);
    }




    private void CtrlBaseLoaded(object sender, RoutedEventArgs e)
    {
      
      Translate();
    }

    #endregion

    private void CtrlBase_Unloaded(object sender, RoutedEventArgs e)
    {
      wp.Children.Clear();
    }
  }
}
