﻿using System.Windows;
using Model;
using Model.Common.LayoutSetting;

namespace View.Common.LayoutSettings
{
  /// <summary>
  /// Interaction logic for CtrlConfCheckParameter.xaml
  /// </summary>
  public partial class CtrlLayoutSettingBool : CtrlBase, ICtrlLayoutSetting
  {
    public CtrlLayoutSettingBool()
    {
      InitializeComponent();

    }

    #region TRADUZIONI

    protected override void Translate()
    {
      if (txtBlkTitle.Tag != null)
        txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      if (txtBlkDescr.Tag != null)
        txtBlkDescr.Text = Context.Instance.TranslateDefault((string)txtBlkDescr.Tag);

      Yes = Context.Instance.TranslateDefault("YES");
      No = Context.Instance.TranslateDefault("NO");
    }

    #endregion TRADUZIONI

    public LayoutSettingParameter ConfParameter
    {
      get { return (LayoutSettingParameter)GetValue(ConfParameterProperty); }
      set { SetValue(ConfParameterProperty, value); }
    }

    // Using a DependencyProperty as the backing store for ConfParameter.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ConfParameterProperty =
        DependencyProperty.Register("ConfParameter", typeof(LayoutSettingParameter), typeof(CtrlLayoutSettingBool), new PropertyMetadata(null));

    private string yes = "YES";
    public string Yes
    {
      get { return yes; }
      set { yes = value;
        NotifyPropertyChanged("Yes");
      }
    }

    private string no = "NO";
    public string No
    {
      get { return no; }
      set
      {
        no = value;
        NotifyPropertyChanged("No");
      }
    }

    public event OnConfirmHandler OnConfirm;
    public event OnCancelHandler OnCancel;

    private void butCancel_Click(object sender, RoutedEventArgs e)
    {
      OnCancel?.Invoke(ConfParameter);
    }

    private void butConfirm_Click(object sender, RoutedEventArgs e)
    {
      OnConfirm?.Invoke(ConfParameter);
    }

    private void butDefault_Click(object sender, RoutedEventArgs e)
    {
      ConfParameter.SetDefault();
    }

    private void CtrlBase_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
  }
}
