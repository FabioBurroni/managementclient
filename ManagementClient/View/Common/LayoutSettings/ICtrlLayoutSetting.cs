﻿
using Model.Common.LayoutSetting;

namespace View.Common.LayoutSettings
{
  public delegate void OnConfirmHandler(LayoutSettingParameter confPar);
  public delegate void OnCancelHandler(LayoutSettingParameter confPar);
  public interface ICtrlLayoutSetting
  {
    event OnConfirmHandler OnConfirm;
    event OnCancelHandler OnCancel;

    LayoutSettingParameter ConfParameter { get; set; }
  }
}
