﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace View.Common.Datetime
{
	/// <summary>
	/// Interaction logic for CtrlCurrentDateTime.xaml
	/// </summary>
	public partial class CtrlCurrentDateTime : UserControl
	{
		private readonly DispatcherTimer _dispatcherTimer = new DispatcherTimer();

		public CtrlCurrentDateTime()
		{
			InitializeComponent();
			Update();
		}

		private void dispatcherTimer_Tick(object sender, EventArgs e)
		{
			Update();
		}

		private void Update()
		{
			var culture = Localization.CultureManager.Instance.Culture;
			if (string.IsNullOrEmpty(culture))
				culture = "it-IT";

			day.Text = DateTime.Now.ToString("f", new CultureInfo(culture));
		}

		public Brush ForeColor
		{
			get { return (Brush)GetValue(ForeColorProperty); }
			set { SetValue(ForeColorProperty, value); }
		}

		// Using a DependencyProperty as the backing store for ForeColor.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ForeColorProperty =
			 DependencyProperty.Register("ForeColor", typeof(Brush), typeof(CtrlCurrentDateTime), new PropertyMetadata(Brushes.White));

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			DispatcherTimer dispatcherTimer = new DispatcherTimer();
			dispatcherTimer.Tick += dispatcherTimer_Tick;
			dispatcherTimer.Interval = new TimeSpan(0, 0, 10);
			dispatcherTimer.Start();
		}

		private void UserControl_Unloaded(object sender, RoutedEventArgs e)
		{
			try
			{
				_dispatcherTimer.Tick -= dispatcherTimer_Tick;
				_dispatcherTimer.Stop();
			}
			catch
			{

			}
		}
	}
}