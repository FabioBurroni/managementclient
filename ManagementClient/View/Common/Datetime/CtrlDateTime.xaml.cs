﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace View.Common.Datetime
{
  /// <summary>
  /// Interaction logic for CtrlDateTime.xaml
  /// </summary>
  public partial class CtrlDateTime : UserControl
  {
    #region COSTRUTTORE
    public CtrlDateTime()
    {
      Title = "";
      ViewDateTime = new View_DateTime { SelectedDateTime = DateTime.Now };
      ViewDateTime.PropertyChanged += ViewDateTime_PropertyChanged;
      InitializeComponent();
    }

    private void ViewDateTime_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      if (e.PropertyName == "SelectedDateTime")
        SelectedDateTime = ViewDateTime.SelectedDateTime;
    }
    #endregion

    #region DP - Title
    public string Title
    {
      get { return (string)GetValue(TitleProperty); }
      set { SetValue(TitleProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty TitleProperty =
       DependencyProperty.Register("Title", typeof(string), typeof(CtrlDateTime), new PropertyMetadata(""));
    #endregion

    public DateTime SelectedDateTime
    {
      get { return (DateTime)GetValue(SelectedDateTimeProperty); }
      set { SetValue(SelectedDateTimeProperty, value); }
    }

    // Using a DependencyProperty as the backing store for SelectedDateTime.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty SelectedDateTimeProperty =
        DependencyProperty.Register("SelectedDateTime", typeof(DateTime), typeof(CtrlDateTime),
          new UIPropertyMetadata(DateTime.Now, new PropertyChangedCallback(OnCalendarChanged)));


    private static void OnCalendarChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      CtrlDateTime ctrl = d as CtrlDateTime;
      ctrl.ViewDateTime.SelectedDateTime = (DateTime)e.NewValue;
    }

    public View_DateTime ViewDateTime { get; set; }

    private void timeHH_Error(object sender, ValidationErrorEventArgs e)
    {
      if (e.Action == ValidationErrorEventAction.Added)
      {

        MessageBox.Show(e.Error.ErrorContent.ToString());
        var be = e.Error.BindingInError;

        Validation.ClearInvalid((BindingExpression)be);
      }

      else
      {
      }
    }

    private void timeHH_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Tab)
        return;

      if ((e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
      {
        if (timeHH.Text.Length >= 2)
          e.Handled = true;

      }
      else
      {
        e.Handled = true;
      }
    }

    private void timeMM_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Tab)
        return;

      if ((e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
      {
        if (timeMM.Text.Length >= 2)
          e.Handled = true;

      }
      else
      {
        e.Handled = true;
      }
    }

    private void butHHPlus_Click(object sender, RoutedEventArgs e)
    {
      SelectedDateTime = SelectedDateTime.AddHours(1);
    }
    private void butHHMinus_Click(object sender, RoutedEventArgs e)
    {
      SelectedDateTime = SelectedDateTime.AddHours(-1);
    }
    private void butMMPlus_Click(object sender, RoutedEventArgs e)
    {
      SelectedDateTime = SelectedDateTime.AddMinutes(1);
    }

    private void butMMMinus_Click(object sender, RoutedEventArgs e)
    {
      SelectedDateTime = SelectedDateTime.AddMinutes(-1);
    }

    private void timeHH_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      if (e.Delta > 0)
        SelectedDateTime = SelectedDateTime.AddHours(1);
      else
        SelectedDateTime = SelectedDateTime.AddHours(-1);
    }

    private void timeMM_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      if (e.Delta > 0)
        SelectedDateTime = SelectedDateTime.AddMinutes(1);
      else
        SelectedDateTime = SelectedDateTime.AddMinutes(-1);
    }
  }

  public class View_DateTime : Model.ModelBase
  {
    private int _Hour;
    public int Hour
    {
      get { return _Hour; }
      set
      {
        if (value >= 0 && value <= 23)
        {

          if (value != _Hour)
          {
            _Hour = value;
            NotifyPropertyChanged();
            NotifyPropertyChanged("SelectedDateTime");
          }
        }
      }
    }


    private int _Minute;
    public int Minute
    {
      get { return _Minute; }
      set
      {
        if (value >= 0 && value <= 59)
        {
          if (value != _Minute)
          {
            _Minute = value;
            NotifyPropertyChanged();
            NotifyPropertyChanged("SelectedDateTime");
          }
        }
      }
    }



    private DateTime _Date;
    public DateTime Date
    {
      get { return _Date; }
      set
      {
        if (value != _Date)
        {
          _Date = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("SelectedDateTime");
        }
      }
    }

    public DateTime SelectedDateTime
    {
      get
      {
        return new DateTime(_Date.Year, _Date.Month, _Date.Day, _Hour, _Minute, 0);
      }

      set
      {
        Date = value;
        Hour = value.Hour;
        Minute = value.Minute;
        NotifyPropertyChanged();
      }
    }
  }
}
