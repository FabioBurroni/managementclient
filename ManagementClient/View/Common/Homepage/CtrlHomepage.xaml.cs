﻿
using Configuration;
using Model;
using System;
using System.Linq;
using System.Reflection;
using System.Windows.Controls;

namespace View.Common.Homepage
{
  /// <summary>
  /// Interaction logic for CtrlHomepage.xaml
  /// </summary>
  public partial class CtrlHomepage : CtrlFunction
  {
    public CtrlHomepage()
    {
      InitializeComponent();

      try
      {
        //get parameters...
        var homepageSettings = ConfigurationManager.Parameters.Homepages;
        var viewHomepage = homepageSettings.Where(x => x.Name == "Homepage").FirstOrDefault(); ;
        var dashboard = homepageSettings.Where(x => x.Name == "Dashboard").FirstOrDefault();

        Assembly assembly = Assembly.Load(viewHomepage.Container.ToString().Split('.').FirstOrDefault().ToString());
      
        Type t = assembly.GetType(viewHomepage.Value.ToString());
        UserControl ucHome = (UserControl)Activator.CreateInstance(t);

        Homepage.Content = ucHome;

      
        t = assembly.GetType(dashboard.Value.ToString());
        UserControl ucDash = (UserControl)Activator.CreateInstance(t);

        Dashboard.Content = ucDash;
      }
      catch(Exception ex)
      {
        string exc = ex.ToString();
      }
    }
  }
}
