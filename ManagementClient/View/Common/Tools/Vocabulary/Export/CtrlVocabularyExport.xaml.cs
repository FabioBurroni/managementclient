﻿using Localization;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using System;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using System.Windows;
using View.Common.Languages;

namespace View.Common.Tools
{
  public partial class CtrlVocabularyExport : CtrlBase
  {
    #region PUBLIC PROPERTIES

    private bool isVocabularyToExport =  true;
    public bool IsVocabularyToExport
    {
      get { return isVocabularyToExport; }
      set
      {
        isVocabularyToExport = value;
        NotifyPropertyChanged("IsVocabularyToExport");
      }
    }

    private bool isCustomVocabularyToExport = true;
    public bool IsCustomVocabularyToExport
    {
      get { return isCustomVocabularyToExport; }
      set
      {
        isCustomVocabularyToExport = value;
        NotifyPropertyChanged("IsCustomVocabularyToExport");
      }
    }

    #endregion PUBLIC PROPERTIES

    #region CONSTRUCTOR

    public CtrlVocabularyExport()
    {     
      InitializeComponent();
    }

    #endregion CONSTRUCTOR

    #region Control Event

    private void butExport_Click(object sender, RoutedEventArgs e)
    {
      Export();
    }

    private void butZipExport_Click(object sender, RoutedEventArgs e)
    {
      ExportZip();
      
    }

    public string GetTmpDirectory()
    {
      string tmpDirectory;

      do
      {
        tmpDirectory = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()));
      } while (Directory.Exists(tmpDirectory));

      tmpDirectory = Path.Combine(tmpDirectory, "Vocabulary");
      Directory.CreateDirectory(tmpDirectory);
      return tmpDirectory;
    }

   
    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    #endregion Control Event

    #region PRIVATE METHODS

    private static async Task<OpenDirectoryDialogResult> SelectFolderDialogs(string title, DialogHost dialogHost)
    {
      OpenDirectoryDialogArguments dialogArgs = new OpenDirectoryDialogArguments()
      {
        //CurrentDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
        CreateNewDirectoryEnabled = true,
        ShowHiddenFilesAndDirectories = true,
        ShowSystemFilesAndDirectories = true,
        SwitchPathPartsAsButtonsEnabled = true,
        PathPartsAsButtons = true,
        
        };

      return await OpenDirectoryDialog.ShowDialogAsync(dialogHost.Identifier.ToString(), dialogArgs);

     

     
    }

    #region Export

    private async void ExportZip()
    {
      try
      {
        string tmpDir = GetTmpDirectory();

        if (IsVocabularyToExport)
        {
          File.Copy(Localize.VocabularyFilePath, Path.Combine(tmpDir, Localize.VocabularyFileName));
        }

        if (IsCustomVocabularyToExport)
        {
          File.Copy(Localize.CustomVocabularyFilePath, Path.Combine(tmpDir, Localize.CustomVocabularyFileName));
        }

        OpenDirectoryDialogResult oddr = await SelectFolderDialogs("ZIP EXPORT", localVocabularyExportDialogHost);
        if (!oddr.Canceled)
        {
          if (!string.IsNullOrEmpty(oddr.Directory))
          {
            ZipFile.CreateFromDirectory(tmpDir, Path.Combine(oddr.Directory,"Vocabulary.zip"));
          }
          else
          {
            LocalSnackbar.ShowMessageFail("EXPORT FAIL".TD() + " - Directory NOT VALID", 3);
          }
        }
      
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();
      }
    }

    private async void Export()
    {
      try
      {
         OpenDirectoryDialogResult oddr = await SelectFolderDialogs("ZIP EXPORT", localVocabularyExportDialogHost);
        if (!oddr.Canceled)
        {
          if (!string.IsNullOrEmpty(oddr.Directory))
          {
            if (IsVocabularyToExport)
            {
              File.Copy(Localize.VocabularyFilePath, Path.Combine(oddr.Directory, Localize.VocabularyFileName));
            }

            if (IsCustomVocabularyToExport)
            {
              File.Copy(Localize.CustomVocabularyFilePath, Path.Combine(oddr.Directory, Localize.CustomVocabularyFileName));
            }
          }
          else
          {
            LocalSnackbar.ShowMessageFail("EXPORT FAIL".TD() + " - Directory NOT VALID", 3);
          }
        }

      }
      catch (Exception ex)
      {
        string exc = ex.ToString();
      }
    }

    #endregion Export


    

    #endregion PRIVATE METHODS
  }
}