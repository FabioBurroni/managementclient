﻿using System.Windows;
using MaterialDesignThemes.Wpf;
using Configuration;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using ExtendedUtilities.SnackbarTools;
using View.Common.Languages;
using Localization;
using System;
using System.ComponentModel;

namespace View.Common.Tools
{

  /// <summary>
  /// Interaction logic for WinVocabularyManager.xaml
  /// </summary>
  public partial class WinVocabularyManager : Window, INotifyPropertyChanged
  {
    #region Notify property changed

    public event PropertyChangedEventHandler PropertyChanged;

    // This method is called by the Set accessor of each property.
    // The CallerMemberName attribute that is applied to the optional propertyName
    // parameter causes the property name of the caller to be substituted as an argument.
    protected void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion Notify property changed

    #region Properties

    #region Vocabulary Folder

    private string vocabularyFolderName = "Vocabulary";
    public string VocabularyFolderName
    {
      get { return vocabularyFolderName; }
      set
      {
        vocabularyFolderName = value;
        NotifyPropertyChanged("VocabularyFoldeName");
      }
    }

    private string vocabularyFolder;
    public string VocabularyFolder
    {
      get { return vocabularyFolder; }
      set
      {
        vocabularyFolder = value;
        NotifyPropertyChanged("VocabularyFolder");
      }
    }

    #endregion

    #region Vocabulary.xml
    private string vocabularyFileName = "Vocabulary.xml";
    public string VocabularyFileName
    {
      get { return vocabularyFileName; }
      set
      {
        vocabularyFileName = value;
        NotifyPropertyChanged("VocabularyFileName");
      }
    }

    private string vocabularyFilePath;
    public string VocabularyFilePath
    {
      get { return vocabularyFilePath; }
      set
      {
        vocabularyFilePath = value;
        NotifyPropertyChanged("VocabularyFilePath");
      }
    }
    #endregion

    #region Vocabulary_alarm_service.xml
    private string vocabularyAlarmFileName = "Vocabulary_alarm_service.xml";
    public string VocabularyAlarmFileName
    {
      get { return vocabularyAlarmFileName; }
      set
      {
        vocabularyAlarmFileName = value;
        NotifyPropertyChanged("VocabularyAlarmFileName");
      }
    }

    private string vocabularyAlarmFilePath;
    public string VocabularyAlarmFilePath
    {
      get { return vocabularyAlarmFilePath; }
      set
      {
        vocabularyAlarmFilePath = value;
        NotifyPropertyChanged("VocabularyAlarmFilePath");
      }
    }
    #endregion

    #region Vocabulary_authentication.xml
    private string vocabularyAuthenticationFileName = "Vocabulary_authentication.xml";
    public string VocabularyAuthenticationFileName
    {
      get { return vocabularyAuthenticationFileName; }
      set
      {
        vocabularyAuthenticationFileName = value;
        NotifyPropertyChanged("VocabularyAuthenticationFileName");
      }
    }

    private string vocabularyAuthenticationFilePath;
    public string VocabularyAuthenticationFilePath
    {
      get { return vocabularyAuthenticationFilePath; }
      set
      {
        vocabularyAuthenticationFilePath = value;
        NotifyPropertyChanged("VocabularyAuthenticationFilePath");
      }
    }
    #endregion

    #region Vocabulary_Result.xml
    private string vocabularyResultFileName = "Vocabulary_Result.xml";
    public string VocabularyResultFileName
    {
      get { return vocabularyResultFileName; }
      set
      {
        vocabularyResultFileName = value;
        NotifyPropertyChanged("VocabularyResultFileName");
      }
    }

    private string vocabularyResultFilePath;
    public string VocabularyResultFilePath
    {
      get { return vocabularyResultFilePath; }
      set
      {
        vocabularyResultFilePath = value;
        NotifyPropertyChanged("VocabularyResultFilePath");
      }
    }
    #endregion

    #region Custom_Vocabulary.xml
    private string customVocabularyFileName = "Vocabulary.xml";
    public string CustomVocabularyFileName
    {
      get { return customVocabularyFileName; }
      set
      {
        customVocabularyFileName = value;
        NotifyPropertyChanged("CustomVocabularyFileName");
      }
    } 
    
    private string customVocabularyFilePath;
    public string CustomVocabularyFilePath
    {
      get { return customVocabularyFilePath; }
      set
      {
        customVocabularyFilePath = value;
        NotifyPropertyChanged("CustomVocabularyFilePath");
      }
    }
    #endregion

    #region Custom_Vocabulary_alarm_service.xml
    private string customVocabularyAlarmFileName = "Vocabulary_alarm_service.xml";
    public string CustomVocabularyAlarmFileName
    {
      get { return customVocabularyAlarmFileName; }
      set
      {
        customVocabularyAlarmFileName = value;
        NotifyPropertyChanged("CustomVocabularyAlarmFileName");
      }
    }

    private string customVocabularyAlarmFilePath;
    public string CustomVocabularyAlarmFilePath
    {
      get { return customVocabularyAlarmFilePath; }
      set
      {
        customVocabularyAlarmFilePath = value;
        NotifyPropertyChanged("CustomVocabularyAlarmFilePath");
      }
    }
    #endregion

    #region Custom_Vocabulary_Result.xml
    private string customVocabularyResultFileName = "VocabularyResult.xml";
    public string CustomVocabularyResultFileName
    {
      get { return customVocabularyResultFileName; }
      set
      {
        customVocabularyResultFileName = value;
        NotifyPropertyChanged("CustomVocabularyResultFileName");
      }
    }

    private string customVocabularyResultFilePath;
    public string CustomVocabularyResultFilePath
    {
      get { return customVocabularyResultFilePath; }
      set
      {
        customVocabularyResultFilePath = value;
        NotifyPropertyChanged("CustomVocabularyResultFilePath");
      }
    }
    #endregion


    #endregion

    #region CONSTRUCTOR
    public WinVocabularyManager()
    {
      VocabularyFolder = Path.Combine(ConfigurationManager.MainDirectory, VocabularyFolderName);

      VocabularyFileName = Localize.VocabularyFileName;
      VocabularyFilePath = Localize.VocabularyFilePath;

      VocabularyAlarmFileName = Localize.VocabularyAlarmFileName;
      VocabularyAlarmFilePath = Localize.VocabularyAlarmFilePath;

      VocabularyAuthenticationFileName = Localize.VocabularyAuthenticationFileName;
      VocabularyAuthenticationFilePath = Localize.VocabularyAuthenticationFilePath;

      VocabularyResultFileName = Localize.VocabularyResultFileName;
      VocabularyResultFilePath = Localize.VocabularyResultFilePath;

      CustomVocabularyFileName = Localize.CustomVocabularyFileName;
      CustomVocabularyFilePath = Localize.CustomVocabularyFilePath;

      CustomVocabularyAlarmFileName = Localize.CustomVocabularyAlarmFileName;
      CustomVocabularyAlarmFilePath = Localize.CustomVocabularyAlarmFilePath;

      CustomVocabularyResultFileName = Localize.CustomVocabularyResultFileName;
      CustomVocabularyResultFilePath = Localize.CustomVocabularyResultFilePath;

      InitializeComponent();

      Translate();
    }

    #endregion

    #region Translate
    private void Translate()
    {

    }

    #endregion

    #region PRIVATE METHODS

    private void OpenFolder(string folderPath)
    {
      if (Directory.Exists(folderPath))
      {
        ProcessStartInfo startInfo = new ProcessStartInfo
        {
          Arguments = folderPath,
          FileName = "explorer.exe"
        };
        Process.Start(startInfo);
      }
      else
      {
        LocalSnackbar.ShowMessageFail(folderPath + " " + "NOT EXISTS".TD(), 2);
      }
    }

    private void OpenFile(string filePath)
    {
      if (File.Exists(filePath))
      {
        System.Diagnostics.Process.Start(filePath);
      }
      else
      {
        LocalSnackbar.ShowMessageFail(filePath + " " + "NOT EXISTS".TD(), 2);
      }
    }

    private void CopyPath(string path)
    {
      if (Directory.Exists(path) || File.Exists(path))
      {
        Clipboard.SetText(path);

        LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        MainSnackbar.ShowMessageFail("sono la main!!", 10);
      }
      else
      {
        LocalSnackbar.ShowMessageFail(Clipboard.GetText() + " " + "COPY FAILED".TD(), 1);
      }
    }

    private void CopyFile(string filePath)
    {
      if (File.Exists(filePath))
      {
        DataObject objData = new DataObject();
        string[] filename = new string[1];
        filename[0] = filePath;
        objData.SetData(DataFormats.FileDrop, filename, true);
        Clipboard.SetDataObject(objData, true);

        LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
      }
      else
      {
        LocalSnackbar.ShowMessageFail(Clipboard.GetText() + " " + "COPY FAILED".TD(), 1);
      }

    }

    #endregion

    #region Window Event

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    #region Folder and file events

    #region VOCABULARY FOLDER

    private void VocabularyFolderCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(VocabularyFolder);
    }

    private void VocabularyFolderOpenDirectory_Click(object sender, RoutedEventArgs e)
    {
      OpenFolder(VocabularyFolder);
    }

    #endregion
    
    #region DEFAULT VOCABULARY FILE
    private void VocabularyFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(VocabularyFilePath);
    }

    private void VocabularyFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(VocabularyFilePath);
    }

    private void VocabularyFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(VocabularyFilePath);
    }

    #endregion

    #region DEFAULT ALARM VOCABULARY FILE
    private void VocabularyAlarmFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(VocabularyAlarmFilePath);
    }

    private void VocabularyAlarmFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(VocabularyAlarmFilePath);
    }

    private void VocabularyAlarmFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(VocabularyAlarmFilePath);
    }

    #endregion

    #region DEFAULT AUTHENTICATION VOCABULARY FILE
    private void VocabularyAuthenticationFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(VocabularyAuthenticationFilePath);
    }

    private void VocabularyAuthenticationFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(VocabularyAuthenticationFilePath);
    }

    private void VocabularyAuthenticationFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(VocabularyAuthenticationFilePath);
    }

    #endregion

    #region DEFAULT RESULT VOCABULARY FILE
    private void VocabularyResultFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(VocabularyResultFilePath);
    }

    private void VocabularyResultFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(VocabularyResultFilePath);
    }

    private void VocabularyResultFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(VocabularyResultFilePath);
    }

    #endregion

    #region CUSTOM VOCABULARY FOLDER
    private void CustomVocabularyFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(CustomVocabularyFilePath);
    }

    private void CustomVocabularyFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(CustomVocabularyFilePath);
    }

    private void CustomVocabularyFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(CustomVocabularyFilePath);
    }
    #endregion

    #region CUSTOM ALARM VOCABULARY FILE
    private void CustomVocabularyAlarmFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(CustomVocabularyAlarmFilePath);
    }

    private void CustomVocabularyAlarmFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(CustomVocabularyAlarmFilePath);
    }

    private void CustomVocabularyAlarmFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(CustomVocabularyAlarmFilePath);
    }

    #endregion

    #region CUSTOM RESULT VOCABULARY FILE
    private void CustomVocabularyResultFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(CustomVocabularyResultFilePath);
    }

    private void CustomVocabularyResultFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(CustomVocabularyResultFilePath);
    }

    private void CustomVocabularyResultFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(CustomVocabularyResultFilePath);
    }

    #endregion
    #endregion

    private async void btnCompare_Click(object sender, RoutedEventArgs e)
    {
      var ctrl = new CtrlVocabularyCompare();

      //show the dialog
      var result = await DialogHost.Show(ctrl, vocabularyManagerDialogHost.Identifier.ToString());
    }

    private async void btnEditor_Click(object sender, RoutedEventArgs e)
    {
      var ctrl = new CtrlVocabularyEditor();

      //show the dialog
      var result = await DialogHost.Show(ctrl, vocabularyManagerDialogHost.Identifier.ToString());
    }

    private async void btnExport_Click(object sender, RoutedEventArgs e)
    {
      var ctrl = new CtrlVocabularyExport();

      //show the dialog
      var result = await DialogHost.Show(ctrl, vocabularyManagerDialogHost.Identifier.ToString());
    }
    #endregion
  }
}
