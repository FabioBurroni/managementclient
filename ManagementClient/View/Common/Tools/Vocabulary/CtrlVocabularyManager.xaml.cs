﻿using System.Windows;
using MaterialDesignThemes.Wpf;
using Configuration;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using ExtendedUtilities.SnackbarTools;
using View.Common.Languages;
using Localization;
using System;

namespace View.Common.Tools
{

  public partial class CtrlVocabularyManager : CtrlBase
  {
    #region Properties

    private string vocabularyFolderName = "Vocabulary";
    public string VocabularyFolderName
    {
      get { return vocabularyFolderName; }
      set
      {
        vocabularyFolderName = value;
        NotifyPropertyChanged("VocabularyFoldeName");
      }
    }

    private string vocabularyFolder;
    public string VocabularyFolder
    {
      get { return vocabularyFolder; }
      set
      {
        vocabularyFolder = value;
        NotifyPropertyChanged("VocabularyFolder");
      }
    }

    private string vocabularyFileName = "Vocabulary.xml";
    public string VocabularyFileName
    {
      get { return vocabularyFileName; }
      set
      {
        vocabularyFileName = value;
        NotifyPropertyChanged("VocabularyFileName");
      }
    }

    private string vocabularyFilePath;
    public string VocabularyFilePath
    {
      get { return vocabularyFilePath; }
      set
      {
        vocabularyFilePath = value;
        NotifyPropertyChanged("VocabularyFilePath");
      }
    }

    private string customVocabularyFileName = "Vocabulary.xml";
    public string CustomVocabularyFileName
    {
      get { return customVocabularyFileName; }
      set
      {
        customVocabularyFileName = value;
        NotifyPropertyChanged("CustomVocabularyFileName");
      }
    }

    private string customVocabularyFilePath;
    public string CustomVocabularyFilePath
    {
      get { return customVocabularyFilePath; }
      set
      {
        customVocabularyFilePath = value;
        NotifyPropertyChanged("CustomVocabularyFilePath");
      }
    }

    #endregion


    #region CONSTRUCTOR
    public CtrlVocabularyManager()
    {

      VocabularyFolder = Path.Combine(ConfigurationManager.MainDirectory, VocabularyFolderName);

      VocabularyFileName = Localize.VocabularyFileName;
      VocabularyFilePath = Localize.VocabularyFilePath;

      CustomVocabularyFileName = Localize.CustomVocabularyFileName;
      CustomVocabularyFilePath = Localize.CustomVocabularyFilePath;

      InitializeComponent();
    }


    #endregion

    #region PRIVATE METHODS

    private void OpenFolder(string folderPath)
    {
      if (Directory.Exists(folderPath))
      {
        ProcessStartInfo startInfo = new ProcessStartInfo
        {
          Arguments = folderPath,
          FileName = "explorer.exe"
        };
        Process.Start(startInfo);
      }
      else
      {
        LocalSnackbar.ShowMessageFail(folderPath + " " + "NOT EXISTS".TD(), 2);
      }
    }

    private void OpenFile(string filePath)
    {
      if (File.Exists(filePath))
      {
        System.Diagnostics.Process.Start(filePath);
      }
      else
      {
        LocalSnackbar.ShowMessageFail(filePath + " " + "NOT EXISTS".TD(), 2);
      }
    }

    private void CopyPath(string path)
    {
      if (Directory.Exists(path) || File.Exists(path))
      {
        Clipboard.SetText(path);

        LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        MainSnackbar.ShowMessageFail("sono la main!!",10);
      }
      else
      {
        LocalSnackbar.ShowMessageFail(Clipboard.GetText() + " " + "COPY FAILED".TD(), 1);
      }
    }

    private void CopyFile(string filePath)
    {
      if (File.Exists(filePath))
      {
        DataObject objData = new DataObject();
        string[] filename = new string[1];
        filename[0] = filePath;
        objData.SetData(DataFormats.FileDrop, filename, true);
        Clipboard.SetDataObject(objData, true);

        LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
      }
      else
      {
        LocalSnackbar.ShowMessageFail(Clipboard.GetText() + " " + "COPY FAILED".TD(), 1);
      }

    }

    #endregion
    
    #region Control Event

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }


    #endregion


    #region Folder and file events
    private void VocabularyFolderCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(VocabularyFolder);
    }

    private void VocabularyFolderOpenDirectory_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFolder(VocabularyFolder);
    }
    private void VocabularyFolderOpenDirectory_Click(object sender, RoutedEventArgs e)
    {
      OpenFolder(VocabularyFolder);
    }

    private void VocabularyFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(VocabularyFilePath);
    }

    private void VocabularyFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(VocabularyFilePath);
    }

    private void VocabularyFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(VocabularyFilePath);
    }

    private void CustomVocabularyFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(CustomVocabularyFilePath);
    }

    private void CustomVocabularyFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(CustomVocabularyFilePath);
    }

    private void CustomVocabularyFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(CustomVocabularyFilePath);
    }
    #endregion

    private async void btnCompare_Click(object sender, RoutedEventArgs e)
    {
      var ctrl = new CtrlVocabularyCompare();

      //show the dialog
      var result = await DialogHost.Show(ctrl, localVocabularyManagerDialogHost.Identifier.ToString());
    }

    private async void btnEditor_Click(object sender, RoutedEventArgs e)
    {
      var ctrl = new CtrlVocabularyEditor();

      //show the dialog
      var result = await DialogHost.Show(ctrl, localVocabularyManagerDialogHost.Identifier.ToString());
    }

    private async void btnExport_Click(object sender, RoutedEventArgs e)
    {
      var ctrl = new CtrlVocabularyExport();

      //show the dialog
      var result = await DialogHost.Show(ctrl, localVocabularyManagerDialogHost.Identifier.ToString());
    }
  }
}
