﻿using MaterialDesignThemes.Wpf;
using System.Collections.Generic;
using System.Windows;

namespace View.Common.Tools
{
  public partial class CtrlVocabularyConfirmationDialog : CtrlBase
  {

    #region PUBLIC PROPERTIES

    public int ReturnValue; // 0 = butClose, 1= left, 2= center, 3 = right
    #endregion PUBLIC PROPERTIES

    #region CONSTRUCTOR

    public CtrlVocabularyConfirmationDialog(
      string title, 
      List<UIElement> _contentToShow, 
      string but1Text, string but2Text, string but3Text, string but4Text, string butCloseText)
    {     
      InitializeComponent();
      
      but1.Visibility = !string.IsNullOrEmpty(but1Text) ? Visibility.Visible : Visibility.Hidden;
      but2.Visibility = !string.IsNullOrEmpty(but2Text) ? Visibility.Visible : Visibility.Hidden;
      but3.Visibility = !string.IsNullOrEmpty(but3Text) ? Visibility.Visible : Visibility.Hidden;
      but4.Visibility = !string.IsNullOrEmpty(but4Text) ? Visibility.Visible : Visibility.Hidden;
      butClose.Visibility = !string.IsNullOrEmpty(butCloseText) ? Visibility.Visible : Visibility.Hidden;

      but1.Content = but1Text;
      but2.Content = but2Text;
      but3.Content = but3Text;
      but4.Content = but4Text;
      butClose.Content = butCloseText;

      txtBlkTitle.Text = title;
      foreach (var child in _contentToShow)
        spMain.Children.Add(child);

    }

    
  

    #endregion CONSTRUCTOR

    #region Control Event  

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      ReturnValue = 0;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    private void but1_Click(object sender, RoutedEventArgs e)
    {
      ReturnValue = 1;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    private void but2_Click(object sender, RoutedEventArgs e)
    {
      ReturnValue = 2;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    private void but3_Click(object sender, RoutedEventArgs e)
    {
      ReturnValue = 3;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    private void but4_Click(object sender, RoutedEventArgs e)
    {
      ReturnValue = 4;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }
    #endregion Control Event
  }
}