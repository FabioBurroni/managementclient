﻿using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Common.VocabularyManager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Xml;
using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;
using View.Common.Languages;

namespace View.Common.Tools
{
  public partial class CtrlVocabularyEditor : CtrlBase
  {
 
    #region ## VARIABILI ##
    
    private XmlDocument xDoc = new XmlDocument();
    private List<Item> itemlist = new List<Item>();
    private ObservableCollectionFast<string> FileLanguages = new ObservableCollectionFast<string>();
    private ObservableCollectionFast<string> CultureCodes = new ObservableCollectionFast<string>()
    {
      "sq-AL",
      "es-AR",
      "pt-BR",
      "cs-CZ",
      "da-DK",
      "ar-EG",
      "fr-FR",
      "de-DE",
      "el-GR",
      "bn-IN",
      "it-IT",
      "ko-KR",
      "en-US",
      "ru-RU",
      "tr-TR",
      "uk-UA",
      "ro-RO",
    };
    private bool unico = true;
    private DataTable dt = new DataTable();

    private int version;

    public int Version
    {
      get { return version; }
      set 
      { 
        version = value;
        NotifyPropertyChanged("Version");  
      }
    }


    private DataView _GridSource;
    public DataView GridSource
    {
      get { return _GridSource; }
      set
      {
        _GridSource = value;
        NotifyPropertyChanged("GridSource");
      }
    }

    private bool doubleClickAutoTranslate;
    public bool DoubleClickAutoTranslate
    {
      get { return doubleClickAutoTranslate; }
      set
      {
        doubleClickAutoTranslate = value;
        NotifyPropertyChanged("DoubleClickAutoTranslate");
      }
    }

    #endregion PUBLIC PROPERTIES

    #region CONSTRUCTOR

    public CtrlVocabularyEditor()
    {
      InitializeComponent();

      CultureCodes = SortLanguages(CultureCodes);

    }

    private ObservableCollectionFast<string> SortLanguages (ObservableCollectionFast<string> list)
    {
      ObservableCollectionFast<string> sortedResult = new ObservableCollectionFast<string>();
      List<string> listSorted = list.ToList();
      listSorted.Sort();
      foreach(string s in listSorted)
      {
        sortedResult.Add(s);
      }
      return sortedResult;

    }

    #endregion CONSTRUCTOR

    #region Control Event
    private void dgMain_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {

      try
      {
        if (dgMain.CurrentItem != null && dgMain.CurrentItem.ToString() != "{NewItemPlaceholder}")
        {
          int row = dgMain.SelectedIndex;
          int col = dgMain.CurrentCell.Column.DisplayIndex;

          if (DoubleClickAutoTranslate)
           OnStartEdit(row, col);
        }
      }
      catch (System.Exception)
      {
        LocalSnackbar.ShowMessageFail("FAIL".TD(), 3);
      }
    }
    private void dgMain_CellEditEnding(object sender, System.Windows.Controls.DataGridCellEditEndingEventArgs e)
    {
      try
      {
        comVar.CurrentData = (DataView)dgMain.ItemsSource;
        comVar.row = dgMain.SelectedIndex;
        comVar.col = 1;
      }
      catch (System.Exception) 
      {
        LocalSnackbar.ShowMessageFail("FAIL".TD(), 3);
      }
    }

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    private void btnExport_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        if (GridSource != null)
        {
          Update();

          XmlDocument doc = new XmlDocument();
          XmlNode vocabulary = doc.CreateElement("vocabulary");

          XmlAttribute attributeVersion = doc.CreateAttribute("version");
          attributeVersion.Value = Version.ToString();
          vocabulary.Attributes.Append(attributeVersion);

          doc.AppendChild(vocabulary);

          bool celleVuote = false;

          foreach (DataRowView row in GridSource)
          {
            XmlNode item = doc.CreateElement("item");
            XmlAttribute ID = doc.CreateAttribute("ID");

            ID.Value = row.Row[0].ToString();
            item.Attributes.Append(ID);

            XmlNode def = doc.CreateElement("default");
            def.InnerXml = "<![CDATA[" + row.Row[1].ToString() + "]]>";
            item.AppendChild(def);
            for (int i = 0; i < FileLanguages.Count; i++)
            {
              XmlNode lang = doc.CreateElement(FileLanguages[i]);
              lang.InnerXml = "<![CDATA[" + row[i+2].ToString() + "]]>";
              item.AppendChild(lang);

              if (row[i + 1].ToString() == "")
              {
                celleVuote = true;
              }
            }
            vocabulary.AppendChild(item);
          }

          System.Windows.Forms.SaveFileDialog saveFile = new System.Windows.Forms.SaveFileDialog();
          if (celleVuote)
          {
            DialogResult dialog = System.Windows.Forms.MessageBox.Show("Sono presenti celle vuote, vuoi esportare ugualmente il file? ","ERROR".TD() , MessageBoxButtons.YesNoCancel);

            if (dialog == DialogResult.Yes)
            {
              saveFile.DefaultExt = ".xml";
              saveFile.AddExtension = true;
              string filename = "Vocabulary_" + DateTime.Now.ToString();
              saveFile.FileName = filename.Replace(" ", "_").Replace("/", "-").Replace(":", "-");
              saveFile.Filter = "Xml document (*.xml)|*.xml|Text files (*.txt)|*.txt|All files (*.*)|*.*";
              saveFile.ShowDialog();

              if (saveFile.FileName != "")
              {
                doc.Save(saveFile.FileName);

                GridSource = null;
                itemlist = new List<Item>();
                FileLanguages = new ObservableCollectionFast<string>();
              }
            }
          }
          else
          {
            saveFile.DefaultExt = ".xml";
            saveFile.AddExtension = true;
            string filename = "Vocabulary_" + DateTime.Now.ToString();
            saveFile.FileName = filename.Replace(" ", "_").Replace("/", "-").Replace(":", "-");
            saveFile.Filter = "Xml document (*.xml)|*.xml|Text files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFile.ShowDialog();

            if (saveFile.FileName != "")
            {
              doc.Save(saveFile.FileName);

              GridSource = null;
              itemlist = new List<Item>();
              FileLanguages = new ObservableCollectionFast<string>();
            }
          }

          celleVuote = false;
        }
        else
          LocalSnackbar.ShowMessageFail("FAIL".TD(), 3);
      }
      catch (Exception)
      {
      }
    }

    private void btnUpdate_Click(object sender, RoutedEventArgs e)
    {
      if (GridSource != null)
      {
        Update();
      }
    }

    private void Update()
    {
      if (GridSource != null)
      {
        UpdateItem();
        GridSource = null;
        insCol();
        insDati();
      }
    }

    private void btnClear_Click(object sender, RoutedEventArgs e)
    {
      if (GridSource != null)
      {
        DialogResult dialog = System.Windows.Forms.MessageBox.Show("Clear", "Sei sicuro di voler cancellare i dati=", MessageBoxButtons.YesNo);

        if (dialog == DialogResult.Yes)
        {
          GridSource = null;
          itemlist = new List<Item>();
          FileLanguages = new ObservableCollectionFast<string>();
        }
      }
    }

    private void btnDeleteColumn_Click(object sender, RoutedEventArgs e)
    {
      if (GridSource != null)
      {
        deleteLanguages();

      }
      else

        LocalSnackbar.ShowMessageFail("FAIL".TD(), 3);
    }

    public async void deleteLanguages()
    {
      CtrlVocabularyLanguageSelector ctrl = new CtrlVocabularyLanguageSelector(FileLanguages);

      ctrl.Title = "DELETE LANGUAGES".TD();
      ctrl.ConfirmationQuestion = "Confirm deleting selected language?".TD();

      //show the dialog
      var result = await DialogHost.Show(ctrl, localVocabularyEditorDialogHost.Identifier.ToString());

      if (!string.IsNullOrEmpty(ctrl.LanguageSelected))
      {
        UpdateItem();
        
        FileLanguages.Remove(ctrl.LanguageSelected);// viene tolto dalle lingue del file
        CultureCodes.Add(ctrl.LanguageSelected);// ritorna tra i disponibili

        GridSource = null;

        CultureCodes = SortLanguages(CultureCodes);

        insCol();
        insDati();
      }

    }


    private void btnAddColumn_Click(object sender, RoutedEventArgs e)
    {
      if (GridSource != null)
      {
        addLanguages();

      }
      else

        LocalSnackbar.ShowMessageFail("FAIL".TD(), 3);

    }
    public async void addLanguages()
    {
      CtrlVocabularyLanguageSelector ctrl = new CtrlVocabularyLanguageSelector(CultureCodes);

      ctrl.Title = "ADD LANGUAGES".TD();
      ctrl.ConfirmationQuestion = "Confirm add selected language?".TD();

      //show the dialog
      var result = await DialogHost.Show(ctrl, localVocabularyEditorDialogHost.Identifier.ToString());

      if (!string.IsNullOrEmpty(ctrl.LanguageSelected))
      {
        UpdateItem();
        
        FileLanguages.Add(ctrl.LanguageSelected);// viene aggiunto alle lingue del file
        CultureCodes.Remove(ctrl.LanguageSelected);// viene tolta dalle disponibili

        GridSource = null;

        FileLanguages = SortLanguages(FileLanguages);

        insCol();
        insDati();
      }

    }

    private async void btnAddFile_Click(object sender, RoutedEventArgs e)
    {
      System.Windows.Forms.OpenFileDialog fileD = new System.Windows.Forms.OpenFileDialog();  
      fileD.Filter = "XML Document (*.xml;)|*.xml;|" + "Xml,Txt,Log (*.xml;*.txt;*.log;)|*.xml;*.txt;*.log;|" + "All files (*.*)|*.*";
      fileD.ShowDialog();

      if (fileD.FileName != "" && (fileD.FileName.Contains(".txt") || fileD.FileName.Contains(".log") || fileD.FileName.Contains(".xml")))
      {
        try
        {
          xDoc.Load(fileD.FileName);
          Update();

          getColonne();
          getDati();

          insCol();
          await insDati();

        }
        catch (System.Exception ex)
        { 
          LocalSnackbar.ShowMessageFail("FAIL".TD() + " - " + ex.ToString(), 3);
        }
      }
      else if (fileD.FileName != "")
        LocalSnackbar.ShowMessageFail("FAIL".TD(), 3);

      Update();

    }

    #endregion Control Event

    #region PRIVATE METHODS
    public void OnStartEdit(int row, int col)
    {
      if (row < GridSource.Count)
      {
        if (col > 1 && GridSource[row].Row[col].ToString() == "" && GridSource[row].Row[1].ToString() != "")
        {
          Process myProcess = new Process();
          myProcess.StartInfo.UseShellExecute = true;
          myProcess.StartInfo.FileName = "https://translate.google.it/?hl=it&sl=auto&tl=" + FileLanguages[col - 2][0] + FileLanguages[col - 2][1] + "&text=" + GridSource[row].Row[1].ToString() + "&op=translatea";
          myProcess.Start();
        }
      }
    }
    private static async Task<OpenDirectoryDialogResult> SelectFolderDialogs(string title, DialogHost dialogHost)
    {
      OpenDirectoryDialogArguments dialogArgs = new OpenDirectoryDialogArguments()
      {
        //CurrentDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
        CreateNewDirectoryEnabled = true,
        ShowHiddenFilesAndDirectories = true,
        ShowSystemFilesAndDirectories = true,
        SwitchPathPartsAsButtonsEnabled = true,
        PathPartsAsButtons = true,

      };

      return await OpenDirectoryDialog.ShowDialogAsync(dialogHost.Identifier.ToString(), dialogArgs);




    }

    public void UpdateItem()
    {
      if (GridSource != null)
      {
        itemlist.Clear();

        for (int i = 0; i < GridSource.Count; i++)
        {
          Item _item = new Item
          {
            id = i + 1,
            defValue = GridSource[i].Row[1].ToString()
          };
          for (int k = 0; k < FileLanguages.Count; k++)
          {
            _item.lang.Add(new Tag(FileLanguages[k], GridSource[i].Row[k + 2].ToString()));
          }
          itemlist.Add(_item);
        }
      }
    }
    public  void insCol()
    {
      dt = new DataTable();

      DataColumn dcID = new DataColumn("ID", typeof(int))
      {
        Unique = true,
        AutoIncrement = true,
        AutoIncrementSeed = 1,
        ReadOnly = true,
      };
      dt.Columns.Add(dcID);

      DataColumn dcDef = new DataColumn("Default", typeof(string))
      {
        Unique = true,
        AllowDBNull = false,
      };
      dt.Columns.Add(dcDef);
      foreach (string sLang in FileLanguages)
      {
        DataColumn dc = new DataColumn(sLang, typeof(string));
        try
        {
          dt.Columns.Add(dc);
        }
        catch (Exception) { }
      }
    }
    public async Task<int> insDati()
    {
      int res = 1;

      foreach (Item item in itemlist)
      {
        DataRow dr = dt.NewRow();
        dr[1] = item.defValue;

        foreach (Tag tag in item.lang)
          if (FileLanguages.Contains(tag.lang))
            dr[tag.lang] = tag.value;

        try
        {
          dt.Rows.Add(dr);
        }
        catch (Exception exc)
        {
          try
          {
            if (res > 0)
            {
              
              foreach (DataRow row in dt.Rows)
              {
                if (row.ItemArray[1].ToString().EqualsIgnoreCase(dr.ItemArray[1].ToString()))
                {
                  if (res != 4)
                  res = await dialogResultDup(row, dr);
                  int n = item.lang.Count;

                  if (res == 0)
                  {
                    return 1;//do nothing, exit
                  }
                  if (res == 1)
                  {
                    ;//do nothing, keep original
                  }
                  else if (res == 2)// Keep all original
                  {
                    res = 0;
                  }
                  else if (res >= 3)// replace
                  {
                    for (int i = 1; i < row.ItemArray.Length; i++)
                    {
                      row[i] = "";
                    }

                    row[1] = item.defValue;

                    foreach (Tag tag in item.lang)
                      row[tag.lang] = tag.value;
                  }

                }
              }
            }
          }
          catch (Exception ex)
          {

          }
        }
      }
      GridSource = dt.DefaultView;

      return 1;
    }

    public void getDati()
    {
      Version = Convert.ToInt32(xDoc.DocumentElement.GetAttribute("version")?.ToString());

      foreach (XmlNode padre in xDoc.DocumentElement.ChildNodes)
      {
        if (padre.HasChildNodes)
        {
          Item _item = new Item
          {
            defValue = padre.ChildNodes[0].InnerText.ToUpper(),
            id = int.Parse(padre.Attributes[0].Value)
          };
          for (int i = 1; i < padre.ChildNodes.Count; i++)
          {
            if (_item.id == 0)
              _item.lang.Add(new Tag(padre.ChildNodes[i].Name, ""));
            else
              _item.lang.Add(new Tag(padre.ChildNodes[i].Name, padre.ChildNodes[i].InnerText));
          }

          itemlist.Add(_item);
        }
      }
      // TO FORCE ALPHABETIC NAME SORTING 
      //var listOrdered = itemlist.OrderBy(x => x.defValue).ToList();
      //itemlist.Clear();
      //itemlist.AddRange(listOrdered.ToList());
      //int id = 1;
      //itemlist.ForEach(x => x.id = id++);

    }
    public void getColonne()
    {
      foreach (XmlNode padre in xDoc.DocumentElement.ChildNodes)
      {
        foreach (XmlNode figlio in padre.ChildNodes)
        {
          for (int i = 0; i < FileLanguages.Count; i++)
          {
            if (figlio.Name == FileLanguages[i])
            {
              unico = false;
              break;
            }
          }
          if (unico && figlio.Name.ToLower() != "default")
          {
            FileLanguages.Add(figlio.Name);
            CultureCodes.Remove(figlio.Name);
          }
          else
            unico = true;
        }
      }

      FileLanguages = SortLanguages(FileLanguages);
      }

    public void dialogError(string e, string a = "")
    {
      System.Windows.Forms.MessageBox.Show("Error!", e + a, MessageBoxButtons.OK);
    }
    public bool dialogResultExport()
    {
      DialogResult dialog  = System.Windows.Forms.MessageBox.Show("Error!", "Sono presenti celle vuote, vuoi esportare ugualmente il file? ", MessageBoxButtons.YesNoCancel);

      if (dialog == DialogResult.Yes)
      {
        return true;
      }
      else if (dialog == DialogResult.No)
      {
        return false;
      }
      else
      {
        return false;
      }
    }
    
    public async Task<int> dialogResultDup(DataRow a, DataRow b)
    {
      List<UIElement> sp = new List<UIElement>();

      TextBlock txtBlkOriginal = new TextBlock();
      txtBlkOriginal.Text = "ORIGINAL ID".TD();
      sp.Add(txtBlkOriginal);

      TextBlock txtBlkOriginalData = new TextBlock();
      foreach (var col in a.ItemArray)
      {
        txtBlkOriginalData.Text += " - " + col;
      }
      sp.Add(txtBlkOriginalData);

      TextBlock txtBlkDuplicate = new TextBlock();
      txtBlkDuplicate.Text = "DUPLICATE ID".TD();
      sp.Add(txtBlkDuplicate);

      TextBlock txtBlkDuplicateData = new TextBlock();
      foreach (var col in b.ItemArray)
      {
        txtBlkDuplicateData.Text += " - " + col;
      }
      sp.Add(txtBlkDuplicateData);


      CtrlVocabularyConfirmationDialog dialog = new CtrlVocabularyConfirmationDialog(
        "DUPLICATE".TD(),
        sp,
        "KEEP".TD(),
        "KEEP ALL".TD(),
        "REPLACE".TD(),
        "REPLACE ALL".TD(),
        "CANCEL".TD()
        );

      //show the dialog
     await DialogHost.Show(dialog, localVocabularyEditorDialogHost.Identifier.ToString());

     return dialog.ReturnValue;
      
      
    }

    #endregion 
    
  }
}