﻿using Configuration;
using MaterialDesignThemes.Wpf;
using Model;
using System;
using System.Windows;
using System.Windows.Controls;

namespace View.Common.Tools
{
  public static class ResourcesHelper
  {
    public static T Get<T>(string resourceName) where T : class
    {
      return Application.Current.TryFindResource(resourceName) as T;
    }
  }
  public partial class CtrlVocabularyLanguageSelector : CtrlBase
  {

    #region PUBLIC PROPERTIES

    private string title =  string.Empty;
    public string Title
    {
      get { return title; }
      set
      {
        title = value;
        NotifyPropertyChanged("Title");
      }
    }

    private string confirmationQuestion = string.Empty;
    public string ConfirmationQuestion
    {
      get { return confirmationQuestion; }
      set
      {
        confirmationQuestion = value;
        NotifyPropertyChanged("ConfirmationQuestion");
      }
    }

    public ObservableCollectionFast<string> Languages = new ObservableCollectionFast<string>();

    private string languageSelected = string.Empty;
    public string LanguageSelected
    {
      get { return languageSelected; }
      set
      {
        languageSelected = value;
        NotifyPropertyChanged("LanguageSelected");
      }
    }



    #endregion PUBLIC PROPERTIES

    #region CONSTRUCTOR

    public CtrlVocabularyLanguageSelector(ObservableCollectionFast<string> languages)
    {     
      InitializeComponent();

      Languages = languages;
      InitAvailableLanguages();
    }

    private void InitAvailableLanguages()
    {
      if (Languages != null)
      {
        foreach (string s in Languages)
        {
         ;
          try
          {
            Image img = new Image()
            {
              Source = new System.Windows.Media.Imaging.BitmapImage(new Uri("pack://application:,,,/View;component/common/images/CountryCode/"+ s + ".png", UriKind.RelativeOrAbsolute))
            };
            
            Button btn = new Button();
            btn.Content = img;
            btn.Tag = s;
            btn.ToolTip = s;
            btn.Height = 100.0;
            btn.Width = 100.0;
            btn.Style = ResourcesHelper.Get<Style>("MaterialDesignFlatButton");
            btn.Click += butConfirmSelection_Click;
            wpMain.Children.Add(btn);
          }
          catch(Exception ex)
          {

          }
          
        }
      }         
    }
  

    #endregion CONSTRUCTOR

    #region Control Event

    private void butConfirmSelection_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is string)
      {
        try
        {
          string langSelected = b.Tag.ToString();
          if(!string.IsNullOrEmpty(langSelected))
            ConfirmSelection(langSelected);
        }
        catch
        {

        }
      }

     
    }

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    #endregion Control Event

    #region PRIVATE METHODS

    

    #region Confirm Selection

    private void ConfirmSelection(string selectedLanguage)
    {
      LanguageSelected = selectedLanguage;
      DialogHost.CloseDialogCommand.Execute(true, null);

    }

    #endregion Confirm Selection




    #endregion PRIVATE METHODS
  }
}