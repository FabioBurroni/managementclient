﻿using ExtendedUtilities.Wpf;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Common.VocabularyManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml;
using Utilities.Extensions.MoreLinq;
using View.Common.Languages;

namespace View.Common.Tools
{
  public partial class CtrlVocabularyCompare : CtrlBase
  {
    #region ## VARIABILI ##

    private XmlDocument xDoc = new XmlDocument();

    private ObservableCollectionFast<string> FileLanguages = new ObservableCollectionFast<string>();
    private int version;

    public int Version
    {
      get { return version; }
      set
      {
        version = value;
        NotifyPropertyChanged("Version");
      }
    }

    private ObservableCollectionFast<string> CultureCodes = new ObservableCollectionFast<string>()
    {
      "sq-AL",
      "es-AR",
      "pt-BR",
      "cs-CZ",
      "da-DK",
      "ar-EG",
      "fr-FR",
      "de-DE",
      "el-GR",
      "bn-IN",
      "it-IT",
      "ko-KR",
      "en-US",
      "ru-RU",
      "tr-TR",
      "uk-UA",
      "ro-RO",
    };

    private ObservableCollectionFast<Item> _GridSource1 = new ObservableCollectionFast<Item>();
    private ObservableCollectionFast<Item> _GridSource2 = new ObservableCollectionFast<Item>();
    private Item _SelectedItem1 = new Item();
    private Item _SelectedItem2 = new Item();

    private List<Item> ResetGrid1 = new List<Item>();
    private List<Item> ResetGrid2 = new List<Item>();

    private List<Item> LastChangeGrid1 = new List<Item>();
    private List<Item> LastChangeGrid2 = new List<Item>();

    private List<Item> FullList = new List<Item>();

    public ObservableCollectionFast<Item> GridSource1
    {
      get { return _GridSource1; }
      set
      {
        _GridSource1 = value;
        NotifyPropertyChanged("GridSource1");
      }
    }

    public ObservableCollectionFast<Item> GridSource2
    {
      get { return _GridSource2; }
      set
      {
        _GridSource2 = value;
        NotifyPropertyChanged("GridSource2");
      }
    }

    public Item SelectedItem1
    {
      get { return _SelectedItem1; }
      set
      {
        _SelectedItem1 = value;
        NotifyPropertyChanged("SelectedItem1");
      }
    }

    public Item SelectedItem2
    {
      get { return _SelectedItem2; }
      set
      {
        _SelectedItem2 = value;
        NotifyPropertyChanged("SelectedItem2");
      }
    }

    #endregion ## VARIABILI ##

    #region CONSTRUCTOR

    public CtrlVocabularyCompare()
    {
      InitializeComponent();

      CultureCodes = SortLanguages(CultureCodes);

      GridSource1 = new ObservableCollectionFast<Item>();

      GridSource2 = new ObservableCollectionFast<Item>();
    }

    #endregion CONSTRUCTOR

    #region Control Event

    /// <summary>
    /// Sincronizza gli offset verticali e orizzontali
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void scrollView_ScrollChanged(object sender, ScrollChangedEventArgs e)
    {
      if (e.VerticalOffset != 0.0f)
      {
        try
        {
          ScrollViewer sv1 = dataGrid1.GetType().InvokeMember("InternalScrollHost", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetProperty, null, dataGrid1, null) as ScrollViewer;
          ScrollViewer sv2 = dataGrid2.GetType().InvokeMember("InternalScrollHost", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetProperty, null, dataGrid2, null) as ScrollViewer;

          sv1.ScrollToVerticalOffset(e.VerticalOffset);
          sv2.ScrollToVerticalOffset(e.VerticalOffset);
        }
        catch (Exception ex) { ex.ToString(); }
      }
    }

    /// <summary>
    /// copia i dati della DG di destra in quella di sinistra
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CopyToLeft_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        var SelectedList = dataGrid1.SelectedItems.Count > 0 ? dataGrid1.SelectedItems : dataGrid2.SelectedItems;

        foreach (Item item in SelectedList)
        {
          int index = GridSource1.IndexOf(item) >= 0 ? GridSource1.IndexOf(item) : GridSource2.IndexOf(item);

          GridSource1[index].defValue = GridSource2[index].defValue;

          GridSource1[index].lang.Clear();
          GridSource1[index].lang.AddRange(GridSource2[index].lang.ToList());
        }
      }
      catch (Exception ex)
      {
        LocalSnackbar.ShowMessageFail(ex.Message.TD(), 3);
      }
      Update();
    }

    /// <summary>
    /// copia i dati della DG sinistra in quella di destra
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CopyToRight_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        var SelectedList = dataGrid1.SelectedItems.Count > 0 ? dataGrid1.SelectedItems : dataGrid2.SelectedItems;

        foreach (Item item in SelectedList)
        {
          int index = GridSource1.IndexOf(item) >= 0 ? GridSource1.IndexOf(item) : GridSource2.IndexOf(item);

          GridSource2[index].defValue = GridSource1[index].defValue;

          GridSource2[index].lang.Clear();
          GridSource2[index].lang.AddRange(GridSource1[index].lang.ToList());
        }
        Update();
      }
      catch (Exception ex)
      {
        LocalSnackbar.ShowMessageFail(ex.Message.TD(), 3);
      }
    }

    /// <summary>
    /// elimina la riga su tutte e due le DG
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void DelRow_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        var SelectedList = dataGrid1.SelectedItems.Count > 0 ? dataGrid1.SelectedItems : dataGrid2.SelectedItems;

        foreach (Item item in SelectedList)
        {
          int index = GridSource1.IndexOf(item) >= 0 ? GridSource1.IndexOf(item) : GridSource2.IndexOf(item);

          GridSource1[index].defValue = "";

          GridSource1[index].lang.Clear();
          for (int i = 0; i < FileLanguages.Count; i++)
            GridSource1[index].lang.Add(new Tag(FileLanguages[i], ""));

          GridSource2[index].defValue = "";

          GridSource2[index].lang.Clear();
          for (int i = 0; i < FileLanguages.Count; i++)
            GridSource2[index].lang.Add(new Tag(FileLanguages[i], ""));
        }
        Update();
      }
      catch (Exception ex)
      {
        LocalSnackbar.ShowMessageFail(ex.Message.TD(), 3);
      }
    }

    /// <summary>
    /// elimina la riga solo sulla datagrid attuale
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void DelSingleRow_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        var SelectedList = dataGrid1.SelectedItems.Count > 0 ? dataGrid1.SelectedItems : dataGrid2.SelectedItems;

        foreach (Item item in SelectedList)
        {
          int index = GridSource1.IndexOf(item) >= 0 ? GridSource1.IndexOf(item) : GridSource2.IndexOf(item);
          var source = GridSource1.IndexOf(item) >= 0 ? GridSource1 : GridSource2;

          source[index].defValue = "";

          source[index].lang.Clear();
          for (int i = 0; i < FileLanguages.Count; i++)
            source[index].lang.Add(new Tag(FileLanguages[i], ""));
        }
        Update();
      }
      catch (Exception ex)
      {
        LocalSnackbar.ShowMessageFail(ex.Message.TD(), 3);
      }
    }

    private void btnReset1_Click(object sender, RoutedEventArgs e)
    {
      GridSource1.Clear();
      GridSource1.AddRange(ResetGrid1);
      Update();
    }

    private void btnReset2_Click(object sender, RoutedEventArgs e)
    {
      GridSource2.Clear();
      GridSource2.AddRange(ResetGrid2);
      Update();
    }

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    private void btnUpdate_Click(object sender, RoutedEventArgs e)
    {
      Update();
    }

    private void btnClear_Click(object sender, RoutedEventArgs e)
    {
      if (GridSource1 != null || GridSource2 != null)
      {
        DialogResult dialog = System.Windows.Forms.MessageBox.Show("Clear", "Sei sicuro di voler cancellare i dati=", MessageBoxButtons.YesNo);

        if (dialog == DialogResult.Yes)
        {
          GridSource1.Clear();
          GridSource2.Clear();

          ResetGrid1.Clear();
          ResetGrid2.Clear();

          LastChangeGrid1.Clear();
          LastChangeGrid2.Clear();

          dataGrid1.Columns.Clear();
          dataGrid2.Columns.Clear();

          FullList.Clear();

          FileLanguages.Clear();

          Add1Text.Text = "";
          Add2Text.Text = "";
        }
      }
    }

    private void btnAddFile_Click(object sender, RoutedEventArgs e)
    {
      System.Windows.Forms.OpenFileDialog fileD = new System.Windows.Forms.OpenFileDialog();
      fileD.Filter = "XML Document (*.xml;)|*.xml;|" + "Xml,Txt,Log (*.xml;*.txt;*.log;)|*.xml;*.txt;*.log;|" + "All files (*.*)|*.*";
      fileD.ShowDialog();

      if (fileD.FileName != "" && (fileD.FileName.Contains(".txt") || fileD.FileName.Contains(".log") || fileD.FileName.Contains(".xml")))
      {
        try
        {
          xDoc.Load(fileD.FileName);

          var FileName = fileD.FileName.Split('\\');

          getColonne();
          getDatiFile(GridSource1);

          Update();

          Add1Text.Text = "...\\" + FileName[FileName.Length - 4] + "\\" + FileName[FileName.Length - 3] + "\\" + FileName[FileName.Length - 2] + "\\" + FileName[FileName.Length - 1];
          ResetGrid1.Clear();
          ForListAddRange(ResetGrid1, GridSource1.ToList().FindAll(x => x.defValue != ""));
        }
        catch (Exception ex)
        {
          GridSource1.Clear();
          Update();
          LocalSnackbar.ShowMessageFail("FAIL".TD() + " - " + ex.ToString(), 3);
        }
      }
      else if (fileD.FileName != "")
      {
        GridSource1.Clear();
        Update();
        LocalSnackbar.ShowMessageFail("FAIL".TD(), 3);
      }
    }

    private void btnAddFile2_Click(object sender, RoutedEventArgs e)
    {
      System.Windows.Forms.OpenFileDialog fileD = new System.Windows.Forms.OpenFileDialog();
      fileD.Filter = "XML Document (*.xml;)|*.xml;|" + "Xml,Txt,Log (*.xml;*.txt;*.log;)|*.xml;*.txt;*.log;|" + "All files (*.*)|*.*";
      fileD.ShowDialog();

      if (fileD.FileName != "" && (fileD.FileName.Contains(".txt") || fileD.FileName.Contains(".log") || fileD.FileName.Contains(".xml")))
      {
        try
        {
          xDoc.Load(fileD.FileName);

          var FileName = fileD.FileName.Split('\\');

          getColonne();
          getDatiFile(GridSource2);

          Update();
          Add2Text.Text = "...\\" + FileName[FileName.Length - 4] + "\\" + FileName[FileName.Length - 3] + "\\" + FileName[FileName.Length - 2] + "\\" + FileName[FileName.Length - 1];
          ResetGrid2.Clear();
          ForListAddRange(ResetGrid2, GridSource2.ToList().FindAll(x => x.defValue != ""));
        }
        catch (System.Exception ex)
        {
          GridSource2.Clear();
          Update();
          LocalSnackbar.ShowMessageFail("FAIL".TD() + " - " + ex.ToString(), 3);
        }
      }
      else if (fileD.FileName != "")
      {
        GridSource2.Clear();
        Update();
        LocalSnackbar.ShowMessageFail("FAIL".TD(), 3);
      }
    }

    private void btnExport1_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        if (GridSource1 != null && Update())
        {
          XmlDocument doc = new XmlDocument();
          XmlNode vocabulary = doc.CreateElement("vocabulary");

          XmlAttribute attributeVersion = doc.CreateAttribute("version");
          attributeVersion.Value = Version.ToString();
          vocabulary.Attributes.Append(attributeVersion);

          doc.AppendChild(vocabulary);

          int ValueID = 1;

          foreach (var _item in GridSource1.ToList().FindAll(x => x.defValue != ""))
          {
            XmlNode item = doc.CreateElement("item");
            XmlAttribute ID = doc.CreateAttribute("ID");

            ID.Value = ValueID.ToString();
            item.Attributes.Append(ID);

            XmlNode def = doc.CreateElement("default");
            def.InnerXml = "<![CDATA[" + _item.defValue + "]]>";
            item.AppendChild(def);

            for (int i = 0; i < FileLanguages.Count; i++)
            {
              try
              {
                XmlNode lang = doc.CreateElement(_item.lang[i].lang);
                lang.InnerXml = "<![CDATA[" + _item.lang[i].value + "]]>";
                item.AppendChild(lang);
              }
              catch (Exception)
              {
                XmlNode lang = doc.CreateElement(_item.lang[i].lang);
                lang.InnerXml = "<![CDATA[]]>";
                item.AppendChild(lang);
              }
            }

            vocabulary.AppendChild(item);
            ValueID++;
          }

          System.Windows.Forms.SaveFileDialog saveFile = new System.Windows.Forms.SaveFileDialog();

          saveFile.DefaultExt = ".xml";
          saveFile.AddExtension = true;
          string filename = "Vocabulary_" + DateTime.Now.ToString();
          saveFile.FileName = filename.Replace(" ", "_").Replace("/", "-").Replace(":", "-");
          saveFile.Filter = "Xml document (*.xml)|*.xml|Text files (*.txt)|*.txt|All files (*.*)|*.*";
          saveFile.ShowDialog();

          if (saveFile.FileName != "")
          {
            doc.Save(saveFile.FileName);
          }
        }
      }
      catch (Exception ex)
      {
        LocalSnackbar.ShowMessageFail(ex.Message.TD(), 3);
      }
    }

    private void btnExport2_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        if (GridSource2 != null && Update())
        {
          XmlDocument doc = new XmlDocument();
          XmlNode vocabulary = doc.CreateElement("vocabulary");

          XmlAttribute attributeVersion = doc.CreateAttribute("version");
          attributeVersion.Value = Version.ToString();
          vocabulary.Attributes.Append(attributeVersion);

          doc.AppendChild(vocabulary);

          int ValueID = 1;

          foreach (var _item in GridSource2.ToList().FindAll(x => x.defValue != ""))
          {
            XmlNode item = doc.CreateElement("item");
            XmlAttribute ID = doc.CreateAttribute("ID");

            ID.Value = ValueID.ToString();
            item.Attributes.Append(ID);

            XmlNode def = doc.CreateElement("default");
            def.InnerXml = "<![CDATA[" + _item.defValue + "]]>";
            item.AppendChild(def);

            for (int i = 0; i < FileLanguages.Count; i++)
            {
              try
              {
                XmlNode lang = doc.CreateElement(_item.lang[i].lang);
                lang.InnerXml = "<![CDATA[" + _item.lang[i].value + "]]>";
                item.AppendChild(lang);
              }
              catch (Exception)
              {
                XmlNode lang = doc.CreateElement(_item.lang[i].lang);
                lang.InnerXml = "<![CDATA[]]>";
                item.AppendChild(lang);
              }
            }

            vocabulary.AppendChild(item);
            ValueID++;
          }

          System.Windows.Forms.SaveFileDialog saveFile = new System.Windows.Forms.SaveFileDialog();

          saveFile.DefaultExt = ".xml";
          saveFile.AddExtension = true;
          string filename = "Vocabulary_" + DateTime.Now.ToString();
          saveFile.FileName = filename.Replace(" ", "_").Replace("/", "-").Replace(":", "-");
          saveFile.Filter = "Xml document (*.xml)|*.xml|Text files (*.txt)|*.txt|All files (*.*)|*.*";
          saveFile.ShowDialog();

          if (saveFile.FileName != "")
          {
            doc.Save(saveFile.FileName);
          }
        }
      }
      catch (Exception ex)
      {
        LocalSnackbar.ShowMessageFail(ex.Message.TD(), 3);
      }
    }

    #endregion Control Event

    #region PRIVATE METHODS

    public void getDatiFile(ObservableCollectionFast<Item> _GridSource)
    {
      int newVersion = Convert.ToInt32(xDoc.DocumentElement.GetAttribute("version")?.ToString());
      Version = Version < newVersion ? newVersion : Version;

      _GridSource.Clear();

      foreach (XmlNode item in xDoc.DocumentElement.ChildNodes)
      {
        if (item.HasChildNodes)
        {
          Item _item = new Item
          {
            defValue = item.ChildNodes[0].InnerText.ToUpper(),
            id = int.Parse(item.Attributes[0].Value)
          };

          for (int i = 0; i < FileLanguages.Count; i++)
          {
            try
            {
              var tag = _item.id != 0 ? new Tag(FileLanguages[i], item[FileLanguages[i]].InnerText) : new Tag(FileLanguages[i], "");
              _item.lang.Add(tag);
            }
            catch (Exception)
            {
              var tag = new Tag(FileLanguages[i], "");
              _item.lang.Add(tag);
            }
          }

          if (!_GridSource.ToList().Exists(x => x.defValue == _item.defValue))
          {
            _GridSource.Add(_item);
          }
          else
          {
            throw new Exception("DUPLICATE DEFAULT VALUE");
          }
        }
      }

      Update();
    }

    public bool Update()
    {
      FullList.Clear();

      var currentGrid1 = new List<Item>();
      var currentGrid2 = new List<Item>();

      ForListAddRange(currentGrid1, GridSource1);
      ForListAddRange(currentGrid2, GridSource2);

      ForListAddRange(FullList, GridSource1.ToList().FindAll(q => q.defValue != ""));
      ForListAddRange(FullList, GridSource2.ToList().FindAll(q => q.defValue != "" && !FullList.Exists(x => x.defValue.ToUpper() == q.defValue.ToUpper())));

      FullList = FullList.OrderBy(q => q.defValue).ToList();

      GridSource1.Clear();
      GridSource2.Clear();

      int index = 1;
      try
      {
        foreach (var item in FullList)
        {
          int res = Convert.ToInt32(currentGrid1.ToList().Exists(q => q.defValue.ToUpper() == item.defValue.ToUpper())) + Convert.ToInt32(currentGrid2.ToList().Exists(q => q.defValue.ToUpper() == item.defValue.ToUpper()));

          if (currentGrid1.ToList().FindAll(q => q.defValue == item.defValue).Count > 1 || currentGrid2.ToList().FindAll(q => q.defValue == item.defValue).Count > 1)
          {
            var x1 = currentGrid1.ToList().FindAll(q => q.defValue == item.defValue);
            var x2 = currentGrid2.ToList().FindAll(q => q.defValue == item.defValue);
            throw new Exception("DUPLICATE DEFAULT VALUE!");
          }

          switch (res)
          {
            case 0:
              {
                var EmptyTagList = new List<Tag>();

                for (int i = 0; i < FileLanguages.Count; i++)
                  EmptyTagList.Add(new Tag(FileLanguages[i], ""));

                GridSource1.Add(new Item() { id = index, defValue = "", lang = EmptyTagList });
                GridSource2.Add(new Item() { id = index, defValue = "", lang = EmptyTagList });
              }
              break;

            case 1:
              {
                ObservableCollectionFast<Item> source1 = currentGrid1.ToList().Exists(q => q.defValue == item.defValue) ? GridSource1 : GridSource2;
                ObservableCollectionFast<Item> source2 = currentGrid1.ToList().Exists(q => q.defValue == item.defValue) ? GridSource2 : GridSource1;

                var EmptyTagList = new List<Tag>();

                var MyItem = currentGrid1.ToList().Exists(q => q.defValue == item.defValue) ? currentGrid1.ToList().Find(q => q.defValue == item.defValue) : currentGrid2.ToList().Find(q => q.defValue == item.defValue);

                var tl = new List<Tag>();

                for (int i = 0; i < FileLanguages.Count; i++)
                {
                  var tag = MyItem.lang.Find(x => x.lang == FileLanguages[i]);
                  string value = tag != null ? tag.value : "";
                  tl.Add(new Tag(FileLanguages[i], value));

                  EmptyTagList.Add(new Tag(FileLanguages[i], ""));
                }
                source1.Add(new Item() { id = index, defValue = MyItem.defValue.ToUpper(), lang = tl });
                source2.Add(new Item() { id = index, defValue = "", lang = EmptyTagList });
              }
              break;

            case 2:
              {
                var MyItem1 = currentGrid1.ToList().Find(q => q.defValue.ToUpper() == item.defValue.ToUpper());
                var MyItem2 = currentGrid2.ToList().Find(q => q.defValue.ToUpper() == item.defValue.ToUpper());

                var tl1 = new List<Tag>();
                var tl2 = new List<Tag>();

                for (int i = 0; i < FileLanguages.Count; i++)
                {
                  var tag1 = MyItem1.lang.Find(x => x.lang == FileLanguages[i]);
                  string value1 = tag1 != null ? tag1.value : "";
                  tl1.Add(new Tag(FileLanguages[i], value1));

                  var tag2 = MyItem2.lang.Find(x => x.lang == FileLanguages[i]);
                  var value2 = tag2 != null ? tag2.value : "";
                  tl2.Add(new Tag(FileLanguages[i], value2));
                }

                GridSource1.Add(new Item() { id = index, defValue = MyItem1.defValue.ToUpper(), lang = tl1 });
                GridSource2.Add(new Item() { id = index, defValue = MyItem2.defValue.ToUpper(), lang = tl2 });
              }
              break;
          }
          index++;
        }

        LastChangeGrid1.Clear();
        LastChangeGrid2.Clear();

        ForListAddRange(LastChangeGrid1, GridSource1.ToList().FindAll(x => x.defValue != ""));
        ForListAddRange(LastChangeGrid2, GridSource2.ToList().FindAll(x => x.defValue != ""));

        return true;
      }
      catch (Exception ex)
      {
        GridSource1.Clear();
        GridSource2.Clear();

        ForObAddRange(GridSource1, LastChangeGrid1);
        ForObAddRange(GridSource2, LastChangeGrid2);

        Update();

        LocalSnackbar.ShowMessageFail(ex.Message.TD(), 3);

        return false;
      }
    }

    public void getColonne()
    {
      dataGrid1.Columns.Clear();
      dataGrid2.Columns.Clear();

      foreach (XmlNode Item in xDoc.DocumentElement.ChildNodes)
      {
        foreach (XmlNode Tag in Item.ChildNodes)
        {
          if (!FileLanguages.ToList().Exists(x => x == Tag.Name) && Tag.Name != "default")
          {
            FileLanguages.Add(Tag.Name);
            CultureCodes.Remove(Tag.Name);
          }
        }
      }

      FileLanguages = SortLanguages(FileLanguages);

      AddCol(FileLanguages.ToList());
    }

    private void AddCol(List<string> list)
    {
      dataGrid1.Columns.Add(new System.Windows.Controls.DataGridTextColumn() { Header = "ID", Binding = new System.Windows.Data.Binding("id") });
      dataGrid1.Columns.Add(new System.Windows.Controls.DataGridTextColumn() { Header = "Default", Binding = new System.Windows.Data.Binding("defValue") { Mode = System.Windows.Data.BindingMode.TwoWay } });

      dataGrid2.Columns.Add(new System.Windows.Controls.DataGridTextColumn() { Header = "ID", Binding = new System.Windows.Data.Binding("id") });
      dataGrid2.Columns.Add(new System.Windows.Controls.DataGridTextColumn() { Header = "Default", Binding = new System.Windows.Data.Binding("defValue") { Mode = System.Windows.Data.BindingMode.TwoWay } });

      for (int i = 0; i < list.Count; i++)
      {
        if (true)
        {
        }
        dataGrid1.Columns.Add(new System.Windows.Controls.DataGridTextColumn() { Header = list[i], Binding = new System.Windows.Data.Binding("lang[" + i + "].value") { Mode = System.Windows.Data.BindingMode.TwoWay, /*UpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged*/ } });
        dataGrid2.Columns.Add(new System.Windows.Controls.DataGridTextColumn() { Header = list[i], Binding = new System.Windows.Data.Binding("lang[" + i + "].value") { Mode = System.Windows.Data.BindingMode.TwoWay, /*UpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged*/ } });
      }
    }

    private ObservableCollectionFast<string> SortLanguages(ObservableCollectionFast<string> list)
    {
      ObservableCollectionFast<string> sortedResult = new ObservableCollectionFast<string>();
      List<string> listSorted = list.ToList();
      listSorted.Sort();
      foreach (string s in listSorted)
      {
        sortedResult.Add(s);
      }
      return sortedResult;
    }

    public void ForListAddRange(List<Item> oc, IEnumerable<Item> collection)
    {
      if (collection == null)
      {
        throw new ArgumentNullException("collection");
      }
      foreach (Item item in collection)
      {
        var _item = new Item();
        _item.id = item.id;
        _item.defValue = item.defValue;
        _item.lang = new List<Tag>();
        foreach (var tag in item.lang)
        {
          _item.lang.Add(new Tag(tag.lang, tag.value));
        }
        oc.Add(_item);
      }
    }

    public void ForObAddRange(ObservableCollectionFast<Item> oc, IEnumerable<Item> collection)
    {
      if (collection == null)
      {
        throw new ArgumentNullException("collection");
      }
      foreach (Item item in collection)
      {
        var _item = new Item();
        _item.id = item.id;
        _item.defValue = item.defValue;
        _item.lang = new List<Tag>();
        foreach (var tag in item.lang)
        {
          _item.lang.Add(new Tag(tag.lang, tag.value));
        }
        oc.Add(_item);
      }
    }

    #endregion PRIVATE METHODS
  }
}