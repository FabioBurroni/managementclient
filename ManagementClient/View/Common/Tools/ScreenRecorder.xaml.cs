﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using SharpAvi.Codecs;
using System.Windows.Input;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using SharpAvi.Output;
using System.Windows.Interop;
using SharpAvi;
using Localization;

namespace View.Common.Tools
{
  /// <summary>
  /// Interaction logic for Screenshot.xaml
  /// </summary>
  public partial class ScreenRecorder : Window
  {
    public ScreenRecorder()
    {
      InitializeComponent();

      Translate();

      recordingTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
      recordingTimer.Tick += recordingTimer_Tick;
      DataContext = this;

      InitDefaultSettings();

      WindowMoveBehavior.Attach(this);
    }

    private void Translate()
    {
      txtBlkTitle.Text = Localize.LocalizeDefaultString(txtBlkTitle.Tag.ToString().ToUpperInvariant());
      btnSettings.ToolTip = Localize.LocalizeDefaultString("SETTINGS");
      btnRecord.ToolTip = Localize.LocalizeDefaultString("RECORD");
      btnStop.ToolTip = Localize.LocalizeDefaultString("STOP");
      btnLast.ToolTip = Localize.LocalizeDefaultString("Open Last Recording".ToUpperInvariant());

      txtBlkClose.Text = Localize.LocalizeDefaultString(txtBlkClose.Tag.ToString().ToUpperInvariant());
    }

    #region Recording

    private readonly DispatcherTimer recordingTimer;
    private readonly Stopwatch recordingStopwatch = new Stopwatch();
    private Recorder recorder;
    private string lastFileName;

    private static readonly DependencyPropertyKey IsRecordingPropertyKey =
        DependencyProperty.RegisterReadOnly("IsRecording", typeof(bool), typeof(ScreenRecorder), new PropertyMetadata(false));
    public static readonly DependencyProperty IsRecordingProperty = IsRecordingPropertyKey.DependencyProperty;

    public bool IsRecording
    {
      get { return (bool)GetValue(IsRecordingProperty); }
      private set { SetValue(IsRecordingPropertyKey, value); }
    }

    private static readonly DependencyPropertyKey ElapsedPropertyKey =
        DependencyProperty.RegisterReadOnly("Elapsed", typeof(string), typeof(ScreenRecorder), new PropertyMetadata(string.Empty));
    public static readonly DependencyProperty ElapsedProperty = ElapsedPropertyKey.DependencyProperty;

    public string Elapsed
    {
      get { return (string)GetValue(ElapsedProperty); }
      private set { SetValue(ElapsedPropertyKey, value); }
    }

    private static readonly DependencyPropertyKey HasLastScreencastPropertyKey =
        DependencyProperty.RegisterReadOnly("HasLastScreencast", typeof(bool), typeof(ScreenRecorder), new PropertyMetadata(false));
    public static readonly DependencyProperty HasLastScreencastProperty = HasLastScreencastPropertyKey.DependencyProperty;

    public bool HasLastScreencast
    {
      get { return (bool)GetValue(HasLastScreencastProperty); }
      private set { SetValue(HasLastScreencastPropertyKey, value); }
    }

    private void StartRecording()
    {
      if (IsRecording)
        throw new InvalidOperationException("Already recording.");

      if (minimizeOnStart)
        WindowState = WindowState.Minimized;

      Elapsed = "00:00";
      HasLastScreencast = false;
      IsRecording = true;

      recordingStopwatch.Reset();
      recordingTimer.Start();

      lastFileName = System.IO.Path.Combine(outputFolder, DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".avi");
        recorder = new Recorder(lastFileName, encoder, encodingQuality);

      recordingStopwatch.Start();
    }

    private void StopRecording()
    {
      if (!IsRecording)
        throw new InvalidOperationException("Not recording.");

      try
      {
        if (recorder != null)
        {
          recorder.Dispose();
          recorder = null;
        }
      }
      finally
      {
        recordingTimer.Stop();
        recordingStopwatch.Stop();

        IsRecording = false;
        HasLastScreencast = true;

        WindowState = WindowState.Normal;
      }
    }

    private void recordingTimer_Tick(object sender, EventArgs e)
    {
      var elapsed = recordingStopwatch.Elapsed;
      Elapsed = string.Format(
          "{0:00}:{1:00}",
          Math.Floor(elapsed.TotalMinutes),
          elapsed.Seconds);
    }

    #endregion


    #region Settings

    private string outputFolder;
    private FourCC encoder;
    private int encodingQuality;
    private bool minimizeOnStart;

    private void InitDefaultSettings()
    {
      var exePath = new Uri(System.Reflection.Assembly.GetEntryAssembly().Location).LocalPath;
      outputFolder = System.IO.Path.GetDirectoryName(exePath);

      encoder = KnownFourCCs.Codecs.MotionJpeg;
      encodingQuality = 70;

      minimizeOnStart = true;
    }

    private void ShowSettingsDialog()
    {
      var dlg = new ScreenRecorderSettingsWindow()
      {
        Owner = this,
        Folder = outputFolder,
        Encoder = encoder,
        Quality = encodingQuality,
        MinimizeOnStart = minimizeOnStart
      };

      if (dlg.ShowDialog() == true)
      {
        outputFolder = dlg.Folder;
        encoder = dlg.Encoder;
        encodingQuality = dlg.Quality;
        minimizeOnStart = dlg.MinimizeOnStart;
      }
    }

    #endregion


    private void StartRecording_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        StartRecording();
      }
      catch (Exception ex)
      {
        MessageBox.Show("Error starting recording\r\n" + ex.ToString());
        StopRecording();
      }
    }

    private void StopRecording_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        StopRecording();
      }
      catch (Exception ex)
      {
        MessageBox.Show("Error stopping recording\r\n" + ex.ToString());
      }
    }

    private void GoToLastScreencast_Click(object sender, RoutedEventArgs e)
    {
      Process.Start("explorer.exe", string.Format("/select, \"{0}\"", lastFileName));
    }

    private void Settings_Click(object sender, RoutedEventArgs e)
    {
      ShowSettingsDialog();
    }

    private void Exit_Click(object sender, RoutedEventArgs e)
    {
      if (IsRecording)
        StopRecording();

      Close();
    }
  }

  internal static class WindowMoveBehavior
  {
    private static readonly DependencyProperty MoveOriginProperty =
        DependencyProperty.RegisterAttached("MoveOrigin", typeof(System.Windows.Point), typeof(WindowMoveBehavior));

    public static void Attach(Window window)
    {
      window.Closed += window_Closed;
      window.MouseLeftButtonDown += window_MouseLeftButtonDown;
      window.MouseLeftButtonUp += window_MouseLeftButtonUp;
      window.MouseMove += window_MouseMove;
    }

    private static void window_Closed(object sender, EventArgs e)
    {
      var window = (Window)sender;
      window.Closed -= window_Closed;
      window.MouseLeftButtonDown -= window_MouseLeftButtonDown;
      window.MouseLeftButtonUp -= window_MouseLeftButtonUp;
      window.MouseMove -= window_MouseMove;
    }

    private static void window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      var window = (Window)sender;
      window.SetValue(MoveOriginProperty, e.GetPosition(window));
      window.CaptureMouse();
    }

    private static void window_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
      var window = (Window)sender;
      if (window.IsMouseCaptured)
      {
        window.ReleaseMouseCapture();
      }
    }

    private static void window_MouseMove(object sender, MouseEventArgs e)
    {
      var window = (Window)sender;
      if (window.IsMouseCaptured)
      {
        var offset = e.GetPosition(window) - (System.Windows.Point)window.GetValue(MoveOriginProperty);
        window.Left += offset.X;
        window.Top += offset.Y;
      }
    }
  }

  internal class Recorder : IDisposable
  {
    private readonly int screenWidth;
    private readonly int screenHeight;
    private readonly AviWriter writer;
    private readonly IAviVideoStream videoStream;
    private readonly Thread screenThread;
    private readonly ManualResetEvent stopThread = new ManualResetEvent(false);
    private readonly AutoResetEvent videoFrameWritten = new AutoResetEvent(false);
    private readonly AutoResetEvent audioBlockWritten = new AutoResetEvent(false);

    public Recorder(string fileName, FourCC codec, int quality)
    {
      System.Windows.Media.Matrix toDevice;
      using (var source = new HwndSource(new HwndSourceParameters()))
      {
        toDevice = source.CompositionTarget.TransformToDevice;
      }

      screenWidth = (int)Math.Round(SystemParameters.PrimaryScreenWidth * toDevice.M11);
      screenHeight = (int)Math.Round(SystemParameters.PrimaryScreenHeight * toDevice.M22);

      // Create AVI writer and specify FPS
      writer = new AviWriter(fileName)
      {
        FramesPerSecond = 10,
        EmitIndex1 = true,
      };

      // Create video stream
      videoStream = CreateVideoStream(codec, quality);
      // Set only name. Other properties were when creating stream, 
      // either explicitly by arguments or implicitly by the encoder used
      videoStream.Name = "Screencast";

      screenThread = new Thread(RecordScreen)
      {
        Name = typeof(Recorder).Name + ".RecordScreen",
        IsBackground = true
      };

      videoFrameWritten.Set();
      screenThread.Start();
    }

    private IAviVideoStream CreateVideoStream(FourCC codec, int quality)
    {
      // Select encoder type based on FOURCC of codec
      if (codec == KnownFourCCs.Codecs.Uncompressed)
      {
        return writer.AddUncompressedVideoStream(screenWidth, screenHeight);
      }
      else if (codec == KnownFourCCs.Codecs.MotionJpeg)
      {
        return writer.AddMotionJpegVideoStream(screenWidth, screenHeight, quality);
      }
      else
      {
        return writer.AddMpeg4VideoStream(screenWidth, screenHeight, (double)writer.FramesPerSecond,
            // It seems that all tested MPEG-4 VfW codecs ignore the quality affecting parameters passed through VfW API
            // They only respect the settings from their own configuration dialogs, and Mpeg4VideoEncoder currently has no support for this
            quality: quality,
            codec: codec,
            // Most of VfW codecs expect single-threaded use, so we wrap this encoder to special wrapper
            // Thus all calls to the encoder (including its instantiation) will be invoked on a single thread although encoding (and writing) is performed asynchronously
            forceSingleThreadedAccess: true);
      }
    }
        
    public void Dispose()
    {
      stopThread.Set();
      screenThread.Join();
     
      // Close writer: the remaining data is written to a file and file is closed
      writer.Close();

      stopThread.Close();
    }

    private void RecordScreen()
    {
      var stopwatch = new Stopwatch();
      var buffer = new byte[screenWidth * screenHeight * 4];

      Task videoWriteTask = null;

      var isFirstFrame = true;
      var shotsTaken = 0;
      var timeTillNextFrame = TimeSpan.Zero;
      stopwatch.Start();

      while (!stopThread.WaitOne(timeTillNextFrame))
      {
        GetScreenshot(buffer);
        shotsTaken++;

        // Wait for the previous frame is written
        if (!isFirstFrame)
        { 
          videoWriteTask.Wait();

          videoFrameWritten.Set();
        }

        // Start asynchronous (encoding and) writing of the new frame
        videoWriteTask = videoStream.WriteFrameAsync(true, buffer, 0, buffer.Length);

        timeTillNextFrame = TimeSpan.FromSeconds(shotsTaken / (double)writer.FramesPerSecond - stopwatch.Elapsed.TotalSeconds);
        if (timeTillNextFrame < TimeSpan.Zero)
          timeTillNextFrame = TimeSpan.Zero;

        isFirstFrame = false;
      }

      stopwatch.Stop();

      // Wait for the last frame is written
      if (!isFirstFrame)
      {
        videoWriteTask.Wait();
      }
    }

    private void GetScreenshot(byte[] buffer)
    {
      using (var bitmap = new Bitmap(screenWidth, screenHeight))
      using (var graphics = Graphics.FromImage(bitmap))
      {
        graphics.CopyFromScreen(0, 0, 0, 0, new System.Drawing.Size(screenWidth, screenHeight));
        var bits = bitmap.LockBits(new Rectangle(0, 0, screenWidth, screenHeight), ImageLockMode.ReadOnly, PixelFormat.Format32bppRgb);
        Marshal.Copy(bits.Scan0, buffer, 0, buffer.Length);
        bitmap.UnlockBits(bits);

        // Should also capture the mouse cursor here, but skipping for simplicity
        // For those who are interested, look at http://www.codeproject.com/Articles/12850/Capturing-the-Desktop-Screen-with-the-Mouse-Cursor
      }
    }

    
  }

}
