﻿using System.Windows;
using System.Windows.Controls;
using MaterialDesignThemes.Wpf;

namespace View.Common.Tools
{
  
  public partial class CtrlToolbox : UserControl
  {
    #region PUBLIC PROPERTIES

    public bool TakeScreen { get; set; }
    public bool RecordScreen { get; set; }
    #endregion

    #region CONSTRUCTOR
    public CtrlToolbox()
    {
      InitializeComponent();
     
    }


    #endregion

    #region Control Event

  
    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    private void btnScreenshot_Click(object sender, RoutedEventArgs e)
    {
      TakeScreen = true;
      DialogHost.CloseDialogCommand.Execute(true, null);
    } 

    private async void btnProjectFolder_Click(object sender, RoutedEventArgs e)
    {
      var ctrl = new CtrlProjectFolder();

      //show the dialog
      var result = await DialogHost.Show(ctrl, localDialogHost.Identifier.ToString());
    }

    private void btnScreenRecorder_Click(object sender, RoutedEventArgs e)
    {
      RecordScreen = true;
      DialogHost.CloseDialogCommand.Execute(true, null);

    }
    #endregion

    private async void btnVocabularyManager_Click(object sender, RoutedEventArgs e)
    {
      //var ctrl = new CtrlVocabularyManager();

      //show the dialog
      //var result = await DialogHost.Show(ctrl, localDialogHost.Identifier.ToString());

      var winVocabularyManager = new WinVocabularyManager();

      winVocabularyManager.Show();

    }
  }
}
