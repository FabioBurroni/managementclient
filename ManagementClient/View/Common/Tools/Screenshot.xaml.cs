﻿using Localization;
using System.Windows;
using System.Windows.Input;

namespace View.Common.Tools
{
  /// <summary>
  /// Interaction logic for Screenshot.xaml
  /// </summary>
  public partial class Screenshot : Window
  {

    public Point Position { get; set; }

    public Screenshot()
    {
      InitializeComponent();
      btnScreenShot.Content = Localize.LocalizeDefaultString("PRESS TO TAKE SCREENSHOT".ToUpperInvariant());
      txtBlkInfo.Text = Localize.LocalizeDefaultString(txtBlkInfo.Tag.ToString().ToUpperInvariant());
      txtBlkInfoExtend.Text = Localize.LocalizeDefaultString(txtBlkInfoExtend.Tag.ToString().ToUpperInvariant());

    }
    private void btnScreenShot_Click(object sender, RoutedEventArgs e)
    {
      Position = PointToScreen(new Point(0, 0));
     
      this.Close();

    }

    private void Window_MouseDown(object sender, MouseButtonEventArgs e)
    {
      if (e.ChangedButton == MouseButton.Left)
      {
        this.DragMove();

        spExtendInfo.Visibility = Visibility.Hidden;
        spDragInfo.Visibility = Visibility.Hidden;
      }
    }

  }
}
