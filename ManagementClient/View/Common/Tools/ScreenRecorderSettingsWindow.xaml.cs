﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using Localization;
using SharpAvi;
using SharpAvi.Codecs;

namespace View.Common.Tools
{
  /// <summary>
  /// Interaction logic for SettingsWindow.xaml
  /// </summary>
  public partial class ScreenRecorderSettingsWindow : Window, System.Windows.Forms.IWin32Window
  {
    public ScreenRecorderSettingsWindow()
    {
      InitializeComponent();

      Translate();

      InitAvailableCodecs();

      DataContext = this;

      WindowMoveBehavior.Attach(this);
    }
    
    private void Translate()
    {
      txtBlkTitle.Text = Localize.LocalizeDefaultString(txtBlkTitle.Tag.ToString());
      txtBlkFolder.Text = Localize.LocalizeDefaultString(txtBlkFolder.Tag.ToString());
      txtBlkEncoder.Text = Localize.LocalizeDefaultString(txtBlkEncoder.Tag.ToString());
      //txtBlk64Bit.Text = Localize.LocalizeDefaultString(txtBlk64Bit.Tag.ToString());
      txtBlkQuality.Text = Localize.LocalizeDefaultString(txtBlkQuality.Tag.ToString());
      chkBoxMinimize.Content = Localize.LocalizeDefaultString("Minimize on capture start"); 
      btnCancel.Content = Localize.LocalizeDefaultString("CANCEL");
      btnOk.Content = Localize.LocalizeDefaultString("OK");
    }

    private void InitAvailableCodecs()
    {
      var codecs = new List<CodecInfo>();
      codecs.Add(new CodecInfo(KnownFourCCs.Codecs.Uncompressed, "(none)"));
      codecs.Add(new CodecInfo(KnownFourCCs.Codecs.MotionJpeg, "Motion JPEG"));
      codecs.AddRange(Mpeg4VideoEncoderVcm.GetAvailableCodecs());
      AvailableCodecs = codecs;
    }

    

    public static readonly DependencyProperty FolderProperty =
        DependencyProperty.Register("Folder", typeof(string), typeof(ScreenRecorderSettingsWindow));

    public string Folder
    {
      get { return (string)GetValue(FolderProperty); }
      set { SetValue(FolderProperty, value); }
    }

    public static readonly DependencyProperty EncoderProperty =
        DependencyProperty.Register("Encoder", typeof(FourCC), typeof(ScreenRecorderSettingsWindow));

    public FourCC Encoder
    {
      get { return (FourCC)GetValue(EncoderProperty); }
      set { SetValue(EncoderProperty, value); }
    }

    public static readonly DependencyProperty QualityProperty =
        DependencyProperty.Register("Quality", typeof(int), typeof(ScreenRecorderSettingsWindow));

    public int Quality
    {
      get { return (int)GetValue(QualityProperty); }
      set { SetValue(QualityProperty, value); }
    }

    public static readonly DependencyProperty SelectedAudioSourceIndexProperty =
        DependencyProperty.Register("SelectedAudioSourceIndex", typeof(int), typeof(ScreenRecorderSettingsWindow));

    public int SelectedAudioSourceIndex
    {
      get { return (int)GetValue(SelectedAudioSourceIndexProperty); }
      set { SetValue(SelectedAudioSourceIndexProperty, value); }
    }

    public static readonly DependencyProperty UseStereoProperty =
        DependencyProperty.Register("UseStereo", typeof(bool), typeof(ScreenRecorderSettingsWindow),
                                    new PropertyMetadata(false));

    public static readonly DependencyProperty MinimizeOnStartProperty =
        DependencyProperty.Register("MinimizeOnStart", typeof(bool), typeof(ScreenRecorderSettingsWindow));

    public bool MinimizeOnStart
    {
      get { return (bool)GetValue(MinimizeOnStartProperty); }
      set { SetValue(MinimizeOnStartProperty, value); }
    }

    public IEnumerable<CodecInfo> AvailableCodecs
    {
      get;
      private set;
    }

    public IEnumerable<KeyValuePair<int, string>> AvailableAudioSources
    {
      get;
      private set;
    }

   
    public int MaximumAudioQuality
    {
      get { return Mp3AudioEncoderLame.SupportedBitRates.Length - 1; }
    }

    public bool Is64BitProcess
    {
      get { return IntPtr.Size == 8; }
    }


    private void OK_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = true;
    }

    private void BrowseFolder_Click(object sender, RoutedEventArgs e)
    {
      var dlg = new FolderBrowserDialog()
      {
        SelectedPath = Folder,
        ShowNewFolderButton = true,
        Description = "Select folder for screencasts"
      };

      if (dlg.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
      {
        Folder = dlg.SelectedPath;
      }
    }

    IntPtr System.Windows.Forms.IWin32Window.Handle
    {
      get { return new WindowInteropHelper(this).Handle; }
    }
  }

}
