﻿using System.Windows;
using MaterialDesignThemes.Wpf;
using Configuration;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using ExtendedUtilities.SnackbarTools;
using View.Common.Languages;

namespace View.Common.Tools
{

  public partial class CtrlProjectFolder : CtrlBase
  {   
    private string mainParentFolder;
    public string MainParentFolder
    {
      get { return mainParentFolder; }
      set
      {
        mainParentFolder = value;
        NotifyPropertyChanged("MainParentFolder");
      }
    }

    private string projectFolder;
    public string ProjectFolder
    {
      get { return projectFolder; }
      set
      {
        projectFolder = value;
        NotifyPropertyChanged("ProjectFolder");
      }
    }

    private string mainConfFolder;
    public string MainConfFolder
    {
      get { return mainConfFolder; }
      set
      {
        mainConfFolder = value;
        NotifyPropertyChanged("MainConfFolder");
      }
    }

    private string mainConfFile;
    public string MainConfFile
    {
      get { return mainConfFile; }
      set
      {
        mainConfFile = value;
        NotifyPropertyChanged("MainConfFile");
      }
    }

    private string confFolder;
    public string ConfFolder
    {
      get { return confFolder; }
      set
      {
        confFolder = value;
        NotifyPropertyChanged("ConfFolder");
      }
    }

    private string confApplicationFile;
    public string ConfApplicationFile
    {
      get { return confApplicationFile; }
      set
      {
        confApplicationFile = value;
        NotifyPropertyChanged("ConfApplicationFile");
      }
    }

    private string confConnectionsFile;
    public string ConfConnectionsFile
    {
      get { return confConnectionsFile; }
      set
      {
        confConnectionsFile = value;
        NotifyPropertyChanged("ConfConnectionsFile");
      }
    }

    private string confPositionFile;
    public string ConfPositionFile
    {
      get { return confPositionFile; }
      set
      {
        confPositionFile = value;
        NotifyPropertyChanged("ConfPositionFile");
      }
    }

    private string logFolderName = "Log";
    public string LogFolderName
    {
      get { return logFolderName; }
      set
      {
        logFolderName = value;
        NotifyPropertyChanged("LogFolderName");
      }
    } 

    private string logFolder;
    public string LogFolder
    {
      get { return logFolder; }
      set
      {
        logFolder = value;
        NotifyPropertyChanged("LogFolder");
      }
    }

    private string screenshotFolderName = "Screenshot";
    public string ScreenshotFolderName
    {
      get { return screenshotFolderName; }
      set
      {
        screenshotFolderName = value;
        NotifyPropertyChanged("ScreenshotFolderName");
      }
    } 

    private string screenshotFolder;
    public string ScreenshotFolder
    {
      get { return screenshotFolder; }
      set
      {
        screenshotFolder = value;
        NotifyPropertyChanged("ScreenshotFolder");
      }
    }

    private string vocabularyFolderName = "Vocabulary";
    public string VocabularyFolderName
    {
      get { return vocabularyFolderName; }
      set
      {
        vocabularyFolderName = value;
        NotifyPropertyChanged("VocabularyFolderName");
      }
    } 

    private string vocabularyFolder;
    public string VocabularyFolder
    {
      get { return vocabularyFolder; }
      set
      {
        vocabularyFolder = value;
        NotifyPropertyChanged("VocabularyFolder");
      }
    }

    private string vocabularyFileName  = "vocabulary.xml";
    public string VocabularyFileName
    {
      get { return vocabularyFileName; }
      set
      {
        vocabularyFileName = value;
        NotifyPropertyChanged("VocabularyFileName");
      }
    } 

    private string vocabularyFile;
    public string VocabularyFile
    {
      get { return vocabularyFile; }
      set
      {
        vocabularyFile = value;
        NotifyPropertyChanged("VocabularyFile");
      }
    }

    private string vocabularyAlarmFileName  = "vocabulary_alarm_service.xml";
    public string VocabularyAlarmFileName
    {
      get { return vocabularyAlarmFileName; }
      set
      {
        vocabularyAlarmFileName = value;
        NotifyPropertyChanged("VocabularyAlarmFileName");
      }
    } 

    private string vocabularyAlarmFile;
    public string VocabularyAlarmFile
    {
      get { return vocabularyAlarmFile; }
      set
      {
        vocabularyAlarmFile = value;
        NotifyPropertyChanged("VocabularyAlarmFile");
      }
    }

    private string vocabularyAuthenticationFileName = "vocabulary_authentication.xml";
    public string VocabularyAuthenticationFileName
    {
      get { return vocabularyAuthenticationFileName; }
      set
      {
        vocabularyAuthenticationFileName = value;
        NotifyPropertyChanged("VocabularyAuthenticationFileName");
      }
    } 

    private string vocabularyAuthenticationFile;
    public string VocabularyAuthenticationFile
    {
      get { return vocabularyAuthenticationFile; }
      set
      {
        vocabularyAuthenticationFile = value;
        NotifyPropertyChanged("VocabularyAuthenticationFile");
      }
    }

    private string managementClientFileName = "ManagementClient.exe";
    public string ManagementClientFileName
    {
      get { return managementClientFileName; }
      set
      {
        managementClientFileName = value;
        NotifyPropertyChanged("ManagementClientFileName");
      }
    }

    private string managementClientFile;
    public string ManagementClientFile
    {
      get { return managementClientFile; }
      set
      {
        managementClientFile = value;
        NotifyPropertyChanged("ManagementClientFile");
      }
    }

    private string managementClientFileVersion;
    public string ManagementClientFileVersion
    {
      get { return managementClientFileVersion; }
      set
      {
        managementClientFileVersion = value;
        NotifyPropertyChanged("ManagementClientFileVersion");
      }
    }

    private string authenticationFileName = "Authentication.dll";
    public string AuthenticationFileName
    {
      get { return authenticationFileName; }
      set
      {
        authenticationFileName = value;
        NotifyPropertyChanged("AuthenticationFileName");
      }
    }

    private string authenticationFile;
    public string AuthenticationFile
    {
      get { return authenticationFile; }
      set
      {
        authenticationFile = value;
        NotifyPropertyChanged("AuthenticationFile");
      }
    }

    private string authenticationFileVersion;
    public string AuthenticationFileVersion
    {
      get { return authenticationFileVersion; }
      set
      {
        authenticationFileVersion = value;
        NotifyPropertyChanged("AuthenticationFileVersion");
      }
    }

    private string configurationFileName = "Configuration.dll";
    public string ConfigurationFileName
    {
      get { return configurationFileName; }
      set
      {
        configurationFileName = value;
        NotifyPropertyChanged("ConfigurationFileName");
      }
    }

    private string configurationFile;
    public string ConfigurationFile
    {
      get { return configurationFile; }
      set
      {
        configurationFile = value;
        NotifyPropertyChanged("ConfigurationFile");
      }
    }

    private string configurationFileVersion;
    public string ConfigurationFileVersion
    {
      get { return configurationFileVersion; }
      set
      {
        configurationFileVersion = value;
        NotifyPropertyChanged("ConfigurationFileVersion");
      }
    }

    private string extendedUtilitiesFileName = "ExtendedUtilities.dll";
    public string ExtendedUtilitiesFileName
    {
      get { return extendedUtilitiesFileName; }
      set
      {
        extendedUtilitiesFileName = value;
        NotifyPropertyChanged("ExtendedUtilitiesFileName");
      }
    }

    private string extendedUtilitiesFile;
    public string ExtendedUtilitiesFile
    {
      get { return extendedUtilitiesFile; }
      set
      {
        extendedUtilitiesFile = value;
        NotifyPropertyChanged("ExtendedUtilitiesFile");
      }
    }

    private string extendedUtilitiesFileVersion;
    public string ExtendedUtilitiesFileVersion
    {
      get { return extendedUtilitiesFileVersion; }
      set
      {
        extendedUtilitiesFileVersion = value;
        NotifyPropertyChanged("ExtendedUtilitiesFileVersion");
      }
    }

    private string keyPadFileName = "KeyPad.dll";
    public string KeyPadFileName
    {
      get { return keyPadFileName; }
      set
      {
        keyPadFileName = value;
        NotifyPropertyChanged("KeyPadFileName");
      }
    } 

    private string keyPadFile;
    public string KeyPadFile
    {
      get { return keyPadFile; }
      set
      {
        keyPadFile = value;
        NotifyPropertyChanged("KeyPadFile");
      }
    }

    private string keyPadFileVersion;
    public string KeyPadFileVersion
    {
      get { return keyPadFileVersion; }
      set
      {
        keyPadFileVersion = value;
        NotifyPropertyChanged("KeyPadFileVersion");
      }
    }

    private string localizationFileName = "Localization.dll";
    public string LocalizationFileName
    {
      get { return localizationFileName; }
      set
      {
        localizationFileName = value;
        NotifyPropertyChanged("LocalizationFileName");
      }
    }

    private string localizationFile;
    public string LocalizationFile
    {
      get { return localizationFile; }
      set
      {
        localizationFile = value;
        NotifyPropertyChanged("LocalizationFile");
      }
    }

    private string localizationFileVersion;
    public string LocalizationFileVersion
    {
      get { return localizationFileVersion; }
      set
      {
        localizationFileVersion = value;
        NotifyPropertyChanged("LocalizationFileVersion");
      }
    }

    private string modelFileName = "Model.dll";
    public string ModelFileName
    {
      get { return modelFileName; }
      set
      {
        modelFileName = value;
        NotifyPropertyChanged("ModelFileName");
      }
    }

    private string modelFile;
    public string ModelFile
    {
      get { return modelFile; }
      set
      {
        modelFile = value;
        NotifyPropertyChanged("ModelFile");
      }
    }

    private string modelFileVersion;
    public string ModelFileVersion
    {
      get { return modelFileVersion; }
      set
      {
        modelFileVersion = value;
        NotifyPropertyChanged("ModelFileVersion");
      }
    }

    private string utilitiesFileName = "Utilities.dll";
    public string UtilitiesFileName
    {
      get { return utilitiesFileName; }
      set
      {
        utilitiesFileName = value;
        NotifyPropertyChanged("UtilitiesFileName");
      }
    }

    private string utilitiesFile;
    public string UtilitiesFile
    {
      get { return utilitiesFile; }
      set
      {
        utilitiesFile = value;
        NotifyPropertyChanged("UtilitiesFile");
      }
    }

    private string utilitiesFileVersion;
    public string UtilitiesFileVersion
    {
      get { return utilitiesFileVersion; }
      set
      {
        utilitiesFileVersion = value;
        NotifyPropertyChanged("UtilitiesFileVersion");
      }
    }

    private string viewFileName = "View.dll";
    public string ViewFileName
    {
      get { return viewFileName; }
      set
      {
        viewFileName = value;
        NotifyPropertyChanged("");
      }
    }

    private string viewFile;
    public string ViewFile
    {
      get { return viewFile; }
      set
      {
        viewFile = value;
        NotifyPropertyChanged("ViewFile");
      }
    }

    private string viewFileVersion;
    public string ViewFileVersion
    {
      get { return viewFileVersion; }
      set
      {
        viewFileVersion = value;
        NotifyPropertyChanged("ViewFileVersion");
      }
    }


    #region CONSTRUCTOR
    public CtrlProjectFolder()
    {

      MainParentFolder = ConfigurationManager.MainParentDirectory;

      #region main

      ProjectFolder = ConfigurationManager.MainDirectory;

      #region mainConf
      MainConfFolder = ConfigurationManager.MainConfDirectory;

      MainConfFile = ConfigurationManager.MainConfFile;
      #endregion

      #region Conf
      ConfFolder = ConfigurationManager.AppConfDirectory;

      ConfApplicationFile = ConfigurationManager.ApplicationFile;

      ConfConnectionsFile = ConfigurationManager.ConnectionsFile;

      ConfPositionFile = ConfigurationManager.PositionFile;
      #endregion

      #region Log
      LogFolder = Path.Combine(ConfigurationManager.MainDirectory, LogFolderName);
      #endregion

      #region screenshot
      ScreenshotFolder = Path.Combine(ConfigurationManager.MainDirectory, ScreenshotFolderName);
      #endregion

      #region vocabulary

      VocabularyFolder = Path.Combine(ConfigurationManager.MainDirectory, VocabularyFolderName);

      VocabularyFile = Path.Combine(VocabularyFolder, VocabularyFileName);

      VocabularyAlarmFile = Path.Combine(VocabularyFolder, VocabularyAlarmFileName);

      VocabularyAuthenticationFile = Path.Combine(VocabularyFolder, VocabularyAuthenticationFileName);

      #endregion

      ManagementClientFile = Path.Combine(ConfigurationManager.MainDirectory, ManagementClientFileName);
      ManagementClientFileVersion = Assembly.LoadFrom(ManagementClientFile).GetName().Version.ToString();
      AuthenticationFile = Path.Combine(ConfigurationManager.MainDirectory, AuthenticationFileName);

      AuthenticationFileVersion = Assembly.LoadFrom(AuthenticationFile).GetName().Version.ToString();
      ConfigurationFile = Path.Combine(ConfigurationManager.MainDirectory, ConfigurationFileName);
      ConfigurationFileVersion = Assembly.LoadFrom(ConfigurationFile).GetName().Version.ToString();

      ExtendedUtilitiesFile = Path.Combine(ConfigurationManager.MainDirectory, ExtendedUtilitiesFileName);
      ExtendedUtilitiesFileVersion = Assembly.LoadFrom(ExtendedUtilitiesFile).GetName().Version.ToString();
      KeyPadFile = Path.Combine(ConfigurationManager.MainDirectory, KeyPadFileName);

      KeyPadFileVersion = Assembly.LoadFrom(KeyPadFile).GetName().Version.ToString();
      LocalizationFile = Path.Combine(ConfigurationManager.MainDirectory, LocalizationFileName);
      LocalizationFileVersion = Assembly.LoadFrom(LocalizationFile).GetName().Version.ToString();

      ModelFile = Path.Combine(ConfigurationManager.MainDirectory, ModelFileName);
      ModelFileVersion = Assembly.LoadFrom(ModelFile).GetName().Version.ToString();
      UtilitiesFile = Path.Combine(ConfigurationManager.MainDirectory, UtilitiesFileName);

      UtilitiesFileVersion = Assembly.LoadFrom(UtilitiesFile).GetName().Version.ToString();
      ViewFile = Path.Combine(ConfigurationManager.MainDirectory, ViewFileName);
      ViewFileVersion = Assembly.LoadFrom(ViewFile).GetName().Version.ToString();

      #endregion

      InitializeComponent();
    }


    #endregion

    #region PRIVATE METHODS

    private void OpenFolder(string folderPath)
    {
      if (Directory.Exists(folderPath))
      {
        ProcessStartInfo startInfo = new ProcessStartInfo
        {
          Arguments = folderPath,
          FileName = "explorer.exe"
        };
        Process.Start(startInfo);
      }
      else
      {
        LocalSnackbar.ShowMessageFail(folderPath + " " + "NOT EXISTS".TD(), 2);
      }
    }

    private void OpenFile(string filePath)
    {
      if (File.Exists(filePath))
      {
        System.Diagnostics.Process.Start(filePath);
      }
      else
      {
        LocalSnackbar.ShowMessageFail(filePath + " " + "NOT EXISTS".TD(), 2);
      }
    }

    private void CopyPath(string path)
    {
      if (Directory.Exists(path) || File.Exists(path))
      {
        Clipboard.SetText(path);

        LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        //MainSnackbar.ShowMessageFail("sono la main!!",10);
      }
      else
      {
        LocalSnackbar.ShowMessageFail(Clipboard.GetText() + " " + "COPY FAILED".TD(), 1);
      }
    }

    private void CopyFile(string filePath)
    {
      if (File.Exists(filePath))
      {
        DataObject objData = new DataObject();
        string[] filename = new string[1];
        filename[0] = filePath;
        objData.SetData(DataFormats.FileDrop, filename, true);
        Clipboard.SetDataObject(objData, true);

        LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
      }
      else
      {
        LocalSnackbar.ShowMessageFail(Clipboard.GetText() + " " + "COPY FAILED".TD(), 1);
      }

    }

    #endregion
    
    #region Control Event

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }


    #endregion


    #region Folder and file events
    
    #region main parent
    private void MainParentFolderCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(MainParentFolder);
    }

    private void MainParentFolderOpenDirectory_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFolder(MainParentFolder);
    }

    #endregion

    #region main
    private void MainFolderCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(ProjectFolder);
    }

    private void MainFolderOpenDirectory_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFolder(ProjectFolder);
    }
    #endregion

    #region Main CONF
    private void MainConfFolderCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(MainConfFolder);
    }

    private void MainConfFolderOpenDirectory_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFolder(MainConfFolder);
    }

    private void MainConfFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(MainConfFile);
    }

    private void MainConfFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(MainConfFile);
    }

    private void MainConfFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(MainConfFile);
    }

    #endregion

    #region custom conf
    private void ConfFolderCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(ConfFolder);
    }

    private void ConfFolderOpenDirectory_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFolder(ConfFolder);
    }

    #region Application Configuration
    private void ConfApplicationFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(ConfApplicationFile);
    }

    private void ConfApplicationFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(ConfApplicationFile);
    }

    private void ConfApplicationFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(ConfApplicationFile);
    }
    #endregion

    #region Connections Configuration
    private void ConfConnectionsFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(ConfConnectionsFile);
    }

    private void ConfConnectionsFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(ConfConnectionsFile);
    }

    private void ConfConnectionsFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(ConfConnectionsFile);
    }
    #endregion

    #region Position Configuration
    private void ConfPositionFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(ConfPositionFile);
    }

    private void ConfPositionFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(ConfPositionFile);
    }

    private void ConfPositionFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(ConfPositionFile);
    }
    #endregion

    #endregion

    #region Log

    private void LogCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(LogFolder);
    }

    private void LogOpenDirectory_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFolder(LogFolder);
    }

    #endregion

    #region Vocabulary
    private void VocabularyFolderCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(VocabularyFolder);
    }

    private void VocabularyFolderOpenDirectory_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFolder(VocabularyFolder);
    }

    #region Vocabulary File
    private void VocabularyFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(VocabularyFile);
    }

    private void VocabularyFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(VocabularyFile);
    }

    private void VocabularyFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(VocabularyFile);
    }
    #endregion

    #region Vocabulary Alarm
    private void VocabularyAlarmFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(VocabularyAlarmFile);
    }

    private void VocabularyAlarmFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(VocabularyAlarmFile);
    }

    private void VocabularyAlarmFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(VocabularyAlarmFile);
    }
    #endregion

    #region Position Configuration
    private void VocabularyAuthenticationFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(VocabularyAuthenticationFile);
    }

    private void VocabularyAuthenticationFileOpen_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFile(VocabularyAuthenticationFile);
    }

    private void VocabularyAuthenticationFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(VocabularyAuthenticationFile);
    }
    #endregion

    #endregion

    #region screenshot
    private void ScreenshotCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(ScreenshotFolder);
    }

    private void ScreenshotOpenDirectory_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      OpenFolder(ScreenshotFolder);
    }
    #endregion

    #region Management Client File
    private void ManagementClientFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(ManagementClientFile);
    }

    private void ManagementClientFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(ManagementClientFile);
    }
    #endregion

    #region Authentication File
    private void AuthenticationFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(AuthenticationFile);
    }

    private void AuthenticationFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(AuthenticationFile);
    }
    #endregion

    #region Configuration File
    private void ConfigurationFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(ConfigurationFile);
    }

    private void ConfigurationFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(ConfigurationFile);
    }
    #endregion

    #region Extended Utilities File
    private void ExtendedUtilitiesFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(ExtendedUtilitiesFile);
    }

    private void ExtendedUtilitiesFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(ExtendedUtilitiesFile);
    }
    #endregion

    #region KeyPad File
    private void KeyPadFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(KeyPadFile);
    }

    private void KeyPadFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(KeyPadFile);
    }
    #endregion

    #region Localization File
    private void LocalizationFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(LocalizationFile);
    }
      
    private void LocalizationFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(LocalizationFile);
    }
    #endregion

    #region Model File
    private void ModelFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(ModelFile);
    }

    private void ModelFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(ModelFile);
    }
    #endregion

    #region Utilities File
    private void UtilitiesFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(UtilitiesFile);
    }

    private void UtilitiesFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(UtilitiesFile);
    }
    #endregion

    #region View File
    private void ViewFileCopyPath_Click(object sender, RoutedEventArgs e)
    {
      CopyPath(ViewFile);
    }

    private void ViewFileCopy_Click(object sender, RoutedEventArgs e)
    {
      CopyFile(ViewFile);
    }

    #endregion

    #endregion

   
  }
}
