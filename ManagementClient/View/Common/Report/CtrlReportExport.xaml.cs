﻿using Localization;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using Model.Common.Report;
using Model.Export;
using Model.Export.Csv;
using Model.Export.Excel;
using Model.Export.Pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using View.Common.Languages;

namespace View.Common.Report
{
  public partial class CtrlReportExport : CtrlBase
  {
    #region PUBLIC PROPERTIES

    public Export_Configuration Configuration { get; set; } = null;

    public string ReportName { get; set; } = "Report";

    public IList<IExportable> List { get; set; }

    public Type T { get; set; }

    public ExportResult Result { get; set; }

    public string FileCreatedName { get; set; } = "";

    private Model.Export.Pdf.Header pdfHeader;

    public Model.Export.Pdf.Header PdfHeader
    {
      get { return pdfHeader; }
      set
      {
        pdfHeader = value;
        NotifyPropertyChanged("PdfHeader");
      }
    }

    private Model.Export.Pdf.Footer pdfFooter;

    public Model.Export.Pdf.Footer PdfFooter
    {
      get { return pdfFooter; }
      set
      {
        pdfFooter = value;
        NotifyPropertyChanged("PdfFooter");
      }
    }

    #endregion PUBLIC PROPERTIES

    #region CONSTRUCTOR

    public CtrlReportExport(string name, IList<IExportable> list, Type t)
    {
      ReportName = name;
      List = list;
      T = t;

      //init report configuration
      Configuration = Export_Configuration.ReportConfiguration_Read(ReportName);
      if (Configuration == null)
      {
        Configuration = new Export_Configuration(ReportName, true, List.FirstOrDefault());
      }

      //aggiorno il localizzato
      Configuration.Columns.ForEach(c => c.Description = c.Name.TD());
      Configuration.Columns.ForEach(c => c.Color = Color.FromRgb(254, 222, 179));

      //Export_Configuration.ReportConfiguration_Save(ReportName, Configuration);
      // or
      Configuration.Save(ReportName);

      PdfHeader = new Model.Export.Pdf.Header();
      PdfHeader.DateTime = DateTime.Now;
      PdfFooter = new Model.Export.Pdf.Footer();

      var cassioliLogo = Environment.CurrentDirectory + "\\Images\\LogoCassioli.png";

      if(File.Exists(cassioliLogo))
        pdfHeader.Logo1Path = cassioliLogo;

      InitializeComponent();

      // Sort it
      Sort();
    }

    #endregion CONSTRUCTOR

    #region Control Event

    private async void butLogo1Import_Click(object sender, RoutedEventArgs e)
    {
      OpenFileDialogArguments dialogArgs = new OpenFileDialogArguments()
      {
        Width = 1000,
        Height = 1000,
        CurrentDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
        CreateNewDirectoryEnabled = false,
        ShowHiddenFilesAndDirectories = false,
        ShowSystemFilesAndDirectories = false,
        SwitchPathPartsAsButtonsEnabled = false,
        PathPartsAsButtons = false,
        Filters = $@"{Localize.LocalizeDefaultString("Image files")} (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png",
        FilterIndex = 0,
      };

      OpenFileDialogResult result = await OpenFileDialog.ShowDialogAsync(localDialogHost.Identifier.ToString(), dialogArgs);

      if (!String.IsNullOrEmpty(result.File))
      {
        PdfHeader.Logo1Path = result.File;
        NotifyPropertyChanged("PdfHeader");
      }
    }

    private async void butLogo2Import_Click(object sender, RoutedEventArgs e)
    {
      OpenFileDialogArguments dialogArgs = new OpenFileDialogArguments()
      {
        Width = 1000,
        Height = 1000,
        CurrentDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
        CreateNewDirectoryEnabled = false,
        ShowHiddenFilesAndDirectories = false,
        ShowSystemFilesAndDirectories = false,
        SwitchPathPartsAsButtonsEnabled = false,
        PathPartsAsButtons = false,
        Filters = $@"{Localize.LocalizeDefaultString("Image files")} (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png",
        FilterIndex = 0,
      };

      OpenFileDialogResult result = await OpenFileDialog.ShowDialogAsync(localDialogHost.Identifier.ToString(), dialogArgs);

      if (!String.IsNullOrEmpty(result.File))
      {
        PdfHeader.Logo2Path = result.File;
        NotifyPropertyChanged("PdfHeader");
      }
    }

    private void btnSaveConfig_Click(object sender, RoutedEventArgs e)
    {
      if (Configuration.Save(ReportName))
      {
        //LocalSnackbar.ShowMessageOk(ReportName + " " + "CONFIGURATION SAVED".TD(), 3);
        LocalSnackbar.ShowMessageOk(ReportName + " " + "CONFIGURATION SAVED".TD(), 3);
      }
      else
      {
        LocalSnackbar.ShowMessageFail(ReportName + " " + "SAVE CONFIGURATION FAIL".TD(), 3);
      }
    }

    private void btnResetConfig_Click(object sender, RoutedEventArgs e)
    {
      Configuration = new Export_Configuration(ReportName, true, List.FirstOrDefault());

      if (Configuration != null)
      {
        //aggiorno il localizzato
        Configuration.Columns.ForEach(c => c.Description = c.Name.TD());

        if (Configuration.Save(ReportName))
        {
          // Sort it
          Sort();

          dgColumns.GetBindingExpression(DataGrid.ItemsSourceProperty).UpdateSource();

          LocalSnackbar.ShowMessageOk(ReportName + " " + "CONFIGURATION RESET TO DEFAULT".TD(), 3);
        }
        else
        {
          LocalSnackbar.ShowMessageFail(ReportName + " " + "CONFIGURATION RESET TO DEFAULT FAIL".TD(), 3);
        }
      }
      else
      {
        LocalSnackbar.ShowMessageFail(ReportName + " " + "CONFIGURATION RESET TO DEFAULT FAIL".TD(), 3);
      }
    }

    private void butMoveDown_Click(object sender, RoutedEventArgs e)
    {
      var b = sender as Button;
      var column = b.Tag as Export_Column;
      if (column != null)
      {
        if (column.Position < Configuration.Columns.Count)
        {
          int newPos = column.Position + 1;
          Configuration.Columns.FirstOrDefault(c => c.Position == newPos).Position--;
          column.Position++;
        }
      }
      Sort();
    }

    private void butMoveUp_Click(object sender, RoutedEventArgs e)
    {
      var b = sender as Button;
      var column = b.Tag as Export_Column;
      if (column != null)
      {
        if (column.Position > 1)
        {
          int newPos = column.Position - 1;
          Configuration.Columns.FirstOrDefault(c => c.Position == newPos).Position++;
          column.Position--;
        }
      }
      Sort();
      //dgColumns.Items.Refresh();
    }

    private void butPdfExport_Click(object sender, RoutedEventArgs e)
    {
      ExportPdf();
    }

    private void butExcelExport_Click(object sender, RoutedEventArgs e)
    {
      ExportExcel();
    }

    private void butCsvExport_Click(object sender, RoutedEventArgs e)
    {
      ExportCsv();
    }

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    #endregion Control Event

    #region PRIVATE METHODS

    private void Sort()
    {
      ICollectionView view = CollectionViewSource.GetDefaultView(dgColumns.ItemsSource);
      view.SortDescriptions.Add(new SortDescription("Position", ListSortDirection.Ascending));

      CollectionViewSource.GetDefaultView(dgColumns.ItemsSource).Refresh();
    }

    #region Export

    private async void ExportPdf()
    {
      var pdf = new PdfFormat();

      if (List.Count > 0)
      {
        //..retrieve columns
        var columns = Configuration.Columns.Where(c => c.Selected).OrderBy(c => c.Position).ToList();
        if (columns.Count > 0)
        {
          var headers = columns.Select(c => c.Description).ToArray();
          var sizes = columns.Select(c => c.Size).ToArray();
          var colors = columns.Select(c => c.Color).ToArray();

          //Type myType = typeof(T);
          System.Reflection.PropertyInfo[] info = columns.Select(c => T.GetProperty(c.Property)).ToArray();

          var main = new Main()
          {
            Headers = headers,
            Sizes = sizes,
            Info = info,
            ColorHeader = colors,
            RepeteHeader = chkHeader.IsChecked.Value
          };

          if (PdfHeader.IsEnabled)
          {
            SetHeaderElementPosition(((ComboBoxItem)cmbHeaderLeft.SelectedItem)?.Tag?.ToString(), Position.Left);
            SetHeaderElementPosition(((ComboBoxItem)cmbHeaderCenter.SelectedItem)?.Tag?.ToString(), Position.Center);
            SetHeaderElementPosition(((ComboBoxItem)cmbHeaderRight.SelectedItem)?.Tag?.ToString(), Position.Right);
          }

          if (PdfFooter.IsEnabled)
          {
            SetFooterElementPosition(((ComboBoxItem)cmbFooterLeft.SelectedItem)?.Tag?.ToString(), Position.Left);
            SetFooterElementPosition(((ComboBoxItem)cmbFooterRight.SelectedItem)?.Tag?.ToString(), Position.Right);
          }

          pdf.Title = Configuration.Title.TD();
          pdf.Landscape = Configuration.Landscape;
          pdf.Main = main;
          pdf.Header = PdfHeader;
          pdf.Footer = PdfFooter;

          try
          {
            Result = await PdfExportation.Export(localDialogHost, List, pdf);

            FileCreatedName = PdfExportation.FileCreatedName;

            Complete();
          }
          catch (Exception ex)
          {
            throw ex;
          }
        }
      }
      else

        Result = ExportResult.EMPTY_LIST;
    }

    private void SetHeaderElementPosition(string tag, Position value)
    {
      switch (tag)
      {
        case "LOGO CASSIOLI":
          PdfHeader.Logo1Position = value;
          break;

        case "LOGO CUSTOMER":
          PdfHeader.Logo2Position = value;
          break;

        case "CONTENT":
          PdfHeader.ContentPosition = value;
          break;
      }
    }

    private void SetFooterElementPosition(string tag, Position value)
    {
      switch (tag)
      {
        case "FILENAME":
          PdfFooter.FileNamePosition = value;
          break;

        case "PAGENUMBER":
          PdfFooter.PageNumberPosition = value;
          break;
      }
    }

    private async void ExportExcel()
    {
      if (List == null)
      {
        Result = ExportResult.EMPTY_LIST;
        return;
      }
      try
      {
        if (List.Count > 0)
        {
          Result = await ExcelExportation.ExportToExcel(List, localDialogHost, Configuration.Title.TD());

          FileCreatedName = ExcelExportation.FileCreatedName;

          Complete();
        }
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();
      }
    }

    private async void ExportCsv()
    {
      if (List == null)
      {
        Result = ExportResult.EMPTY_LIST;
        return;
      }
      try
      {
        if (List.Count > 0)
        {
          Result = await CsvExportation.Export(List, localDialogHost, Configuration.Title.TD());

          FileCreatedName = CsvExportation.FileCreatedName;

          Complete();
        }
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();
      }
    }

    #endregion Export

    private void Complete()
    {
      switch (Result)
      {
        case ExportResult.UNDEFINED:
          break;

        case ExportResult.OK:
          DialogHost.CloseDialogCommand.Execute(true, null);
          break;

        case ExportResult.FAIL:
          LocalSnackbar.ShowMessageFail("EXPORT FAIL".TD() + " " + FileCreatedName, 3);
          break;

        case ExportResult.FILE_EXTENSION_NOT_VALID:
          LocalSnackbar.ShowMessageFail("EXPORT FAIL".TD() + " " + FileCreatedName, 3);
          break;

        case ExportResult.CANCELED:
          LocalSnackbar.ShowMessageInfo("EXPORT CANCELED", 3);
          break;

        case ExportResult.FILE_NAME_NOT_VALID:
          LocalSnackbar.ShowMessageFail("EXPORT FAIL".TD() + " " + FileCreatedName, 3);
          break;

        case ExportResult.CONFIGURATION_NOT_VALID:
          LocalSnackbar.ShowMessageFail("EXPORT FAIL".TD(), 3);
          break;

        case ExportResult.FILE_IMPOSSIBILE_TO_OVERWRITE:
          LocalSnackbar.ShowMessageFail("EXPORT FAIL".TD() + " " + FileCreatedName, 3);
          break;

        case ExportResult.EMPTY_LIST:
          LocalSnackbar.ShowMessageFail("EMPTY LIST".TD(), 3);
          break;

        case ExportResult.TYPE_NOT_VALID:
          LocalSnackbar.ShowMessageFail("TYPE NOT VALID".TD(), 3);
          break;

        default:
          break;
      }
    }

    #endregion PRIVATE METHODS
  }
}