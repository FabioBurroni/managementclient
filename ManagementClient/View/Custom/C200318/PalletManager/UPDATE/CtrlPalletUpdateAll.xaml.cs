﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200318;
using Model.Custom.C200318.PalletManager;
using View.Common.Languages;

namespace View.Custom.C200318.PalletManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletUpdateAll.xaml
  /// </summary>
  public partial class CtrlPalletUpdateAll : CtrlBaseC200318
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public int StartIndex { get; set; } = _indexStartValue;
    public int MaxItems { get; set; } = _maxItemsStartValue;

    public C200318_PalletManagerAllFilter Filter { get; set; } = new C200318_PalletManagerAllFilter();

    public ObservableCollectionFast<C200318_PalletManagerAll> PalletL { get; set; } = new ObservableCollectionFast<C200318_PalletManagerAll>();

    private C200318_PalletManagerAll _PalletSelected;

    public C200318_PalletManagerAll PalletSelected
    {
      get { return _PalletSelected; }
      set 
      { 
        _PalletSelected = value;
        NotifyPropertyChanged("PalletSelected");
      }
    }


    #endregion


    #region COSTRUTTORE
    public CtrlPalletUpdateAll()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colLotCode.Header = Localization.Localize.LocalizeDefaultString("LOT CODE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colArticleDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colDateBorn.Header = Localization.Localize.LocalizeDefaultString("DATE BORN");
      colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
      colPosition.Header = Localization.Localize.LocalizeDefaultString("POSITION");
      colProductionDate.Header = Localization.Localize.LocalizeDefaultString("PRODUCTION DATE");

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butClose.ToolTip = Localization.Localize.LocalizeDefaultString("CLOSE");

      CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE
    private void Cmd_PM_Pallet_GetAll()
    {
      busyOverlay.IsBusy = true;
      PalletL.Clear();
      CommandManagerC200318.PM_Pallet_GetAll(this, Filter);
    }
    private void PM_Pallet_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      var palL = dwr.Data as List<C200318_PalletManagerAll>;
      if (palL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (palL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        palL = palL.Take(Filter.MaxItems).ToList();

        PalletL.AddRange(palL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

   #endregion

    #region Eventi controllo
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();

      Cmd_PM_Pallet_GetAll();
    }

    #endregion


    #region Eventi Ricerca
    
    private void ctrlFilter_OnSearch()
    {
      Cmd_PM_Pallet_GetAll();
    }
    #endregion

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_PM_Pallet_GetAll();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_PM_Pallet_GetAll();
    }

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_PalletManagerAll)
      {
        try
        {
          Clipboard.SetText((((C200318_PalletManagerAll)b.Tag)).PalletCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyLot_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_PalletManagerAll)
      {
        try
        {
          Clipboard.SetText((((C200318_PalletManagerAll)b.Tag)).LotCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyArticle_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_PalletManagerAll)
      {
        try
        {
          Clipboard.SetText((((C200318_PalletManagerAll)b.Tag)).ArticleCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

   
    private void dgPallet_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      PalletSelected = null;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }
  }

}