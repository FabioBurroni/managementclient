﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200318;
using Model.Custom.C200318.ArticleManager;
using Model.Custom.C200318.PalletManager;
using View.Common.Languages;
using View.Custom.C200318.Converter;

namespace View.Custom.C200318.PalletManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletManager.xaml
  /// </summary>
  public partial class CtrlPalletRegistration : CtrlBaseC200318
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public int StartIndex { get; set; } = _indexStartValue;
    public int MaxItems { get; set; } = _maxItemsStartValue;

    #endregion


    private C200318_PalletInsert pallet;

    public C200318_PalletInsert Pallet
    {
      get { return pallet; }
      set { pallet = value;
        NotifyPropertyChanged("Pallet");
      }
    }


    private string posCode;

    public string PosCode
    {
      get { return posCode; }
      set
      {
        posCode = value;
        NotifyPropertyChanged("PosCode");
      }
    }



    #region COSTRUTTORE
    public CtrlPalletRegistration()
    {
      InitializeComponent();

      Pallet = new C200318_PalletInsert();

      iconResultArticle.Visibility = Visibility.Hidden;
      iconResultLot.Visibility = Visibility.Hidden;
      iconResultQuantity.Visibility = Visibility.Hidden;
      iconResultExpiryDate.Visibility = Visibility.Hidden;
      iconResultLotLength.Visibility = Visibility.Hidden;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      // ricarichiamo gli helper e gli hint assist
      Pallet = new C200318_PalletInsert();

      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkDescription.Text = Context.Instance.TranslateDefault((string)txtBlkDescription.Tag);

      txtBlkArticleCode.Text = Context.Instance.TranslateDefault((string)txtBlkArticleCode.Tag);   
      btnSearchArticle.ToolTip = Context.Instance.TranslateDefault((string)btnSearchArticle.Tag);

      txtBlkLotCode.Text = Context.Instance.TranslateDefault((string)txtBlkLotCode.Tag);
      txtBoxLotCodeHint.Text = Context.Instance.TranslateDefault((string)txtBoxLotCodeHint.Tag);

      txtBlkQuantity.Text = Context.Instance.TranslateDefault((string)txtBlkQuantity.Tag);
      txtBlkExpiryDate.Text = Context.Instance.TranslateDefault((string)txtBlkExpiryDate.Tag);
      txtBlkLotLength.Text = Context.Instance.TranslateDefault((string)txtBlkLotLength.Tag);

      txtBlkRegister.Text = Context.Instance.TranslateDefault((string)txtBlkRegister.Tag);

      btnRegister.ToolTip = Context.Instance.TranslateDefault((string)btnRegister.Tag);

      

    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE

    private void Cmd_PM_Get_CheckInCodes()
    {
      CommandManagerC200318.PM_Get_CheckInCodes(this, Context.Instance.Positions);
    }

    private void PM_Get_CheckInCodes(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var positionEntryList = dwr.Data as List<string>;
      PosCode = positionEntryList.FirstOrDefault();
    }

    private void Cmd_PM_Pallet_Register()
    {
      if (PosCode != null)
        CommandManagerC200318.PM_Pallet_Register(this, Pallet);
      else
        Snackbar.ShowMessageFail("CLIENT IS NOT ASSOCIATED TO CHECKIN POSITION".TD(), 3);
      
    }

    private async void PM_Pallet_Register(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C200318_PalletInsertResult InsertResult = (C200318_PalletInsertResult)dwr.Data;

      if (InsertResult.Result == C200318_CustomResult.OK)
      {
        var view = new CtrlPalletPrintConfirmation("PALLET CREATED".TD(), InsertResult.Code);

        //show the dialog
        await DialogHost.Show(view, "RootDialog");

        C200318_CustomResult PrintResult = view.Result;
        if (PrintResult == C200318_CustomResult.OK)
        {
          Snackbar.ShowMessageOk("PRINT".TD() + " : " + InsertResult.Code , 3);

        }
        else
        {
          Snackbar.ShowMessageFail("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(PrintResult), 3);
        }
      
        Pallet = new C200318_PalletInsert();
      }
      else
      {
        Snackbar.ShowMessageFail("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(InsertResult.Result), 3);

      }
      
      
    }


    private void Cmd_PM_Article_CheckPresence()
    {
      CommandManagerC200318.PM_Article_CheckPresence(this, Pallet.ArticleCode);
    }

    private void PM_Article_CheckPresence(IList<string> commandMethodParameters, IModel model)
    {
      iconResultArticle.Visibility = Visibility.Visible;
      iconResultArticle.Kind = PackIconKind.QuestionMark;
      iconResultArticle.Foreground = Brushes.DarkRed;

      String txtBlkHelper = "ARTICLE NOT FOUND".TD();
      HintAssist.SetHelperText(txtBoxArticleCode, txtBlkHelper);
    
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C200318_Article Result = (C200318_Article)dwr.Data;

      iconResultArticle.Visibility = Visibility.Visible;
      iconResultArticle.Kind = PackIconKind.Check;
      iconResultArticle.Foreground = Brushes.Green;

      txtBlkHelper = Result.Descr;
      HintAssist.SetHelperText(txtBoxArticleCode, txtBlkHelper);

    }




    #endregion

    #region Eventi

    #region Eventi controllo
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();

      if (PosCode == null)
        Cmd_PM_Get_CheckInCodes();
    }

    private void CtrlBaseC200318_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    #endregion


    
    
    #endregion

    #region Eventi Article 
   

    private void txtBoxArticleCode_LostFocus(object sender, RoutedEventArgs e)
    {
      Pallet.ArticleCode = txtBoxArticleCode.Text;

      if (txtBoxArticleCode_Validate())
        Cmd_PM_Article_CheckPresence();
    }

    private void txtBoxArticleCode_TextChanged(object sender, TextChangedEventArgs e)
    {
      Pallet.ArticleCode = txtBoxArticleCode.Text;
      txtBoxArticleCode_Validate();
    }


    private bool txtBoxArticleCode_Validate()
    {
      bool isValidSintax = false;

      txtBoxArticleCode.Background = Brushes.Transparent;

      //prepare helper elements
      iconResultArticle.Visibility = Visibility.Hidden;
      string txtBlkHelper = string.Empty;

      // check empty
      if (String.IsNullOrWhiteSpace(Pallet.ArticleCode) || String.IsNullOrEmpty(Pallet.ArticleCode))
      {
        iconResultArticle.Visibility = Visibility.Visible;
        iconResultArticle.Kind = PackIconKind.Cancel;
        iconResultArticle.Foreground = Brushes.OrangeRed;

      }

      // check length over limit
      else if (Pallet.ArticleCode.Length > 10)
      {
        iconResultArticle.Visibility = Visibility.Visible;
        iconResultArticle.Kind = PackIconKind.ErrorOutline;
        iconResultArticle.Foreground = Brushes.Red;

        txtBlkHelper = "ARTICLE CODE MAX LENGTH IS 10".TD();
        //txtBlkHelper.Foreground = Brushes.Red;
      }

      //check valid length
      else if (Pallet.ArticleCode.Length <= 10)
      {
        // la validazione è fatta dal lost focus
        isValidSintax = true;

        //iconResultArticle.Visibility = Visibility.Visible;
        //iconResultArticle.Kind = PackIconKind.Check;
        //iconResultArticle.Foreground = Brushes.Green;

        //txtBlkHelper.Text = "VALID".TD();
        //txtBlkHelper.Foreground = Brushes.Red;
      }

      //set helper
      HintAssist.SetHelperText(txtBoxArticleCode, txtBlkHelper);

      return isValidSintax;
    }

    private async void btnSearchArticle_Click(object sender, RoutedEventArgs e)
    {
      // open article ctrl
      // prepare the view
      var view = new CtrlPalletArticles();

      //show the dialog
      await DialogHost.Show(view, "RootDialog");

      C200318_Article ArticleSelected = view.ArticleSelected;

      txtBoxArticleCode.Text = ArticleSelected?.Code;

      if (ArticleSelected!= null)
      {
        Pallet.ArticleCode = ArticleSelected.Code;
        if (Pallet.ArticleCode != "")
          Cmd_PM_Article_CheckPresence();
        else
        {
          txtBoxArticleCode.Background = Brushes.Transparent;
        }
      }

    }

    #endregion

    #region Eventi LotCode

    
    private void txtBoxLotCode_LostFocus(object sender, RoutedEventArgs e)
    {
      Pallet.LotCode = txtBoxLotCode.Text;
      txtBoxLotCode_Validate();
    }

    private void txtBoxLotCode_TextChanged(object sender, TextChangedEventArgs e)
    {
      Pallet.LotCode = txtBoxLotCode.Text;
      txtBoxLotCode_Validate();
    }

    private bool txtBoxLotCode_Validate()
    {
      txtBoxLotCode.Background = Brushes.Transparent;
      bool isValidSintax = false;

      //prepare helper elements
      iconResultLot.Visibility = Visibility.Hidden;
      string txtBlkHelper = string.Empty;

      // check empty
      if (String.IsNullOrWhiteSpace(Pallet.LotCode) || String.IsNullOrEmpty(Pallet.LotCode))
      {

        iconResultLot.Visibility = Visibility.Visible;
        iconResultLot.Kind = PackIconKind.Cancel;
        iconResultLot.Foreground = Brushes.OrangeRed;

      }

      // check length over limit
      else if (Pallet.LotCode.Length > 10)
      {
        iconResultLot.Visibility = Visibility.Visible;
        iconResultLot.Kind = PackIconKind.ErrorOutline;
        iconResultLot.Foreground = Brushes.Red;

        txtBlkHelper = "LOT CODE MAX LENGTH IS 10".TD();
        //txtBlkHelper.Foreground = Brushes.Red;
      }

      //check valid length
      else if (Pallet.LotCode.Length <= 10)
      {
        iconResultLot.Visibility = Visibility.Visible;
        iconResultLot.Kind = PackIconKind.Check;
        iconResultLot.Foreground = Brushes.Green;

        isValidSintax = true;
      }

      //set helper
      HintAssist.SetHelperText(txtBoxLotCode, txtBlkHelper);

      return isValidSintax;
    }

    #endregion

    #region Eventi Quantity
    private void txtBoxQuantity_LostFocus(object sender, RoutedEventArgs e)
    {
      try
      {
        Pallet.Quantity = int.Parse(txtBoxQuantity.Text);
        txtBoxQuantity_Validate();
      }
      catch(Exception ex)
      {
        string exc = ex.ToString();

        iconResultQuantity.Visibility = Visibility.Visible;
        iconResultQuantity.Kind = PackIconKind.QuestionMark;
        iconResultQuantity.Foreground = Brushes.DarkRed;

        string txtBlkHelper = "INSERT VALID NUMBER".TD();
        HintAssist.SetHelperText(txtBoxQuantity, txtBlkHelper);
      }

    }

    private void txtBoxQuantity_TextChanged(object sender, TextChangedEventArgs e)
    {
      try
      {
        Pallet.Quantity = int.Parse(txtBoxQuantity.Text);
        txtBoxQuantity_Validate();
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        iconResultQuantity.Visibility = Visibility.Visible;
        iconResultQuantity.Kind = PackIconKind.QuestionMark;
        iconResultQuantity.Foreground = Brushes.DarkRed;

        string txtBlkHelper = "INSERT VALID NUMBER".TD();
        HintAssist.SetHelperText(txtBoxQuantity, txtBlkHelper);
      }
    }


    private bool txtBoxQuantity_Validate()
    {
      txtBoxQuantity.Background = Brushes.Transparent;
      bool isValidSintax = false;

      //prepare helper elements
      iconResultQuantity.Visibility = Visibility.Hidden;
      string txtBlkHelper = string.Empty;

      // check empty
      if (String.IsNullOrWhiteSpace(txtBoxQuantity.Text) || String.IsNullOrEmpty(txtBoxQuantity.Text))
      {
        iconResultQuantity.Visibility = Visibility.Visible;
        iconResultQuantity.Kind = PackIconKind.Cancel;
        iconResultQuantity.Foreground = Brushes.OrangeRed;

      }

      // check quantity under limit
      else if (Pallet.Quantity <= 0)
      {
        iconResultQuantity.Visibility = Visibility.Visible;
        iconResultQuantity.Kind = PackIconKind.ErrorOutline;
        iconResultQuantity.Foreground = Brushes.Red;

        txtBlkHelper = "QUANTITY MUST BE POSITIVE".TD();
      }

      //check valid length
      else if (Pallet.Quantity <= int.MaxValue)
      {
        iconResultQuantity.Visibility = Visibility.Visible;
        iconResultQuantity.Kind = PackIconKind.Check;
        iconResultQuantity.Foreground = Brushes.Green;

        isValidSintax = true;
      }

      //set helper
      HintAssist.SetHelperText(txtBoxQuantity, txtBlkHelper);

      return isValidSintax;
    }

    #endregion

    #region Eventi ExpiryDate
    private void ExpiryDate_LostFocus(object sender, RoutedEventArgs e)
    {
      // to validate date time, try to parse it
      try
      {
        Pallet.ExpiryDate = DateTime.Parse(dpExpiryDate.Text);
        txtBoxExpiryDate_Validate();
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        iconResultExpiryDate.Visibility = Visibility.Visible;
        iconResultExpiryDate.Kind = PackIconKind.QuestionMark;
        iconResultExpiryDate.Foreground = Brushes.DarkRed;

        string txtBlkHelper = "INSERT VALID DATE".TD();
        HintAssist.SetHelperText(dpExpiryDate, txtBlkHelper);
      }

    }
    private void ExpiryDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
    {
      // to validate date time, try to parse it
      try
      {
        Pallet.ExpiryDate = DateTime.Parse(dpExpiryDate.Text);
        txtBoxExpiryDate_Validate();
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        iconResultExpiryDate.Visibility = Visibility.Visible;
        iconResultExpiryDate.Kind = PackIconKind.QuestionMark;
        iconResultExpiryDate.Foreground = Brushes.DarkRed;

        string txtBlkHelper = "INSERT VALID DATE".TD();
        HintAssist.SetHelperText(dpExpiryDate, txtBlkHelper);
      }
    }

    private bool txtBoxExpiryDate_Validate()
    {
      dpExpiryDate.Background = Brushes.Transparent;
      bool isValidSintax = false;

      //prepare helper elements
      iconResultExpiryDate.Visibility = Visibility.Hidden;
      string txtBlkHelper = string.Empty;

      // check empty
      if (String.IsNullOrWhiteSpace(dpExpiryDate.Text) || String.IsNullOrEmpty(dpExpiryDate.Text))
      {
        iconResultExpiryDate.Visibility = Visibility.Visible;
        iconResultExpiryDate.Kind = PackIconKind.Cancel;
        iconResultExpiryDate.Foreground = Brushes.OrangeRed;

      }

      // check quantity under limit
      else if (Pallet.ExpiryDate <= DateTime.MinValue)
      {
        iconResultExpiryDate.Visibility = Visibility.Visible;
        iconResultExpiryDate.Kind = PackIconKind.ErrorOutline;
        iconResultExpiryDate.Foreground = Brushes.Red;

        txtBlkHelper = "DATE NOT VALID".TD();
      }

      //check valid length
      else if (Pallet.ExpiryDate <= DateTime.MaxValue)
      {
        iconResultExpiryDate.Visibility = Visibility.Visible;
        iconResultExpiryDate.Kind = PackIconKind.Check;
        iconResultExpiryDate.Foreground = Brushes.Green;

        isValidSintax = true;
      }

      //set helper
      HintAssist.SetHelperText(txtBoxQuantity, txtBlkHelper);

      return isValidSintax;
    }


    #endregion

    #region Eventi LotLength

    private void txtBoxLotLength_LostFocus(object sender, RoutedEventArgs e)
    {
      try
      {
        Pallet.LotLength = int.Parse(txtBoxLotLength.Text);
        txtBoxLotLength_Validate();
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        iconResultLotLength.Visibility = Visibility.Visible;
        iconResultLotLength.Kind = PackIconKind.QuestionMark;
        iconResultLotLength.Foreground = Brushes.DarkRed;

        string txtBlkHelper = "INSERT VALID NUMBER".TD();
        HintAssist.SetHelperText(txtBoxLotLength, txtBlkHelper);
      }
    }

    private void txtBoxLotLength_TextChanged(object sender, TextChangedEventArgs e)
    {
      try
      {
        Pallet.LotLength = int.Parse(txtBoxLotLength.Text);
        txtBoxLotLength_Validate();
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        iconResultLotLength.Visibility = Visibility.Visible;
        iconResultLotLength.Kind = PackIconKind.QuestionMark;
        iconResultLotLength.Foreground = Brushes.DarkRed;

        string txtBlkHelper = "INSERT VALID NUMBER".TD();
        HintAssist.SetHelperText(txtBoxLotLength, txtBlkHelper);
      }
    }

    private bool txtBoxLotLength_Validate()
    {
      txtBoxLotLength.Background = Brushes.Transparent;
      bool isValidSintax = false;

      //prepare helper elements
      iconResultLotLength.Visibility = Visibility.Hidden;
      string txtBlkHelper = string.Empty;

      // check empty
      if (String.IsNullOrWhiteSpace(txtBoxLotLength.Text) || String.IsNullOrEmpty(txtBoxLotLength.Text))
      {
        iconResultLotLength.Visibility = Visibility.Visible;
        iconResultLotLength.Kind = PackIconKind.Cancel;
        iconResultLotLength.Foreground = Brushes.OrangeRed;

      }

      // check quantity under limit
      else if (Pallet.LotLength <= 0)
      {
        iconResultLotLength.Visibility = Visibility.Visible;
        iconResultLotLength.Kind = PackIconKind.ErrorOutline;
        iconResultLotLength.Foreground = Brushes.Red;

        txtBlkHelper = "QUANTITY MUST BE POSITIVE".TD();
      }

      //check valid length
      else if (Pallet.LotLength <= int.MaxValue)
      {
        iconResultLotLength.Visibility = Visibility.Visible;
        iconResultLotLength.Kind = PackIconKind.Check;
        iconResultLotLength.Foreground = Brushes.Green;

        isValidSintax = true;
      }

      //set helper
      HintAssist.SetHelperText(txtBoxLotLength, txtBlkHelper);

      return isValidSintax;
    }
    #endregion

    private async void btnRegister_Click(object sender, RoutedEventArgs e)
    {
      #region article check
      if (String.IsNullOrWhiteSpace(Pallet.ArticleCode) || String.IsNullOrEmpty(Pallet.ArticleCode))
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "INSERT PALLET RESULT".TD(),
          Message = "PLEASE INSERT VALID ARTICLE CODE".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        return;
      }

      if (Pallet.ArticleCode.Length > 10)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "INSERT PALLET RESULT".TD(),
          Message = "ARTICLE CODE LENGTH MUST BE FROM 1 TO 10 CHARACTERS".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        return;
      }
      #endregion

      #region lot code check
      if (String.IsNullOrWhiteSpace(Pallet.LotCode) || String.IsNullOrEmpty(Pallet.LotCode))
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "CREATE NEW PALLET".TD(),
          Message = "PLEASE INSERT VALID LOT CODE".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        return;
      }

      if (Pallet.LotCode.Length > 10)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "INSERT PALLET RESULT".TD(),
          Message = "LOT CODE LENGTH MUST BE FROM 1 TO 10 CHARACTERS".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        return;
      }
      #endregion

      #region qty check
      if (Pallet.Quantity <= 0)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "INSERT PALLET RESULT".TD(),
          Message = "QUANTITY MUST BE POSITIVE".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        return;
      }
      #endregion

      #region expiry date check
      // to validate date time, try to parse it
      //if (!DateTime.TryParse(Pallet.ExpiryDate.ToString("dd/mm/yyyy"), out DateTime theDate))
      //{
      //  AlertDialogArguments alertDialogArgs = new AlertDialogArguments
      //  {
      //    Title = "INSERT PALLET RESULT".TD(),
      //    Message = "INSERT VALID EXPIRY DATE".TD(),
      //    OkButtonLabel = "OK".TD()
      //  };

      //  await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

      //  return;
      //}
      #endregion

      #region lot length check
      if (Pallet.LotLength <= 0)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "INSERT PALLET RESULT".TD(),
          Message = "LOT LENGTH MUST BE POSITIVE".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        return;
      }
      #endregion


      ConfirmationDialogArguments dialogArgs = new ConfirmationDialogArguments
      {
        Title = "CREATE PALLET CONFIRM".TD(),
        Message = "Are you sure?".TD(),
        OkButtonLabel = "YES".TD(),
        CancelButtonLabel = "CANCEL".TD(),
        StackedButtons = false
      };

      bool result = await ConfirmationDialog.ShowDialogAsync("RootDialog", dialogArgs);

      if (result)
        Cmd_PM_Pallet_Register();
    }
  }
}
