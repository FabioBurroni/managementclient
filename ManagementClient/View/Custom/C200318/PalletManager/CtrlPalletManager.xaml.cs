﻿using System;
using System.Windows;
using Model.Custom.C200318;


namespace View.Custom.C200318.PalletManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletManager.xaml
  /// </summary>
  public partial class CtrlPalletManager : CtrlBaseC200318
  {
    public C200318_UserLogged UserLogged { get; set; } = new C200318_UserLogged();

    #region Costruttore
    public CtrlPalletManager()
    {
      InitializeComponent();
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
      Authentication.AuthenticationManager.Instance.LoginSession += Instance_LoginSession;

    }
    #endregion

    #region TRADUZIONI
    protected override void Translate()
    {
      tiCreateNew.Text = Localization.Localize.LocalizeDefaultString((string)tiCreateNew.Tag);
      tiEdit.Text = Localization.Localize.LocalizeDefaultString((string)tiEdit.Tag);
      tiPrint.Text = Localization.Localize.LocalizeDefaultString((string)tiPrint.Tag);
    }
    #endregion TRADUZIONI

    #region Eventi controllo
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    #endregion

    private void Instance_LoginSession(object sender, Authentication.LoginSessionEventArgs sessionArgs)
    {
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
    }

    private void CtrlBaseC200318_Unloaded(object sender, RoutedEventArgs e)
    {
      try
      {
        Authentication.AuthenticationManager.Instance.LoginSession -= Instance_LoginSession;
      }
      catch (Exception)
      {

      }
    }
  }
}
