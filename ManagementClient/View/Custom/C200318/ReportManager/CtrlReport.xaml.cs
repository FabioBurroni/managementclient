﻿using System;
using System.Windows;
using Model.Custom.C200318;


namespace View.Custom.C200318.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlReport.xaml
  /// </summary>
  public partial class CtrlReport : CtrlBaseC200318
  {
    public C200318_UserLogged UserLogged { get; set; } = new C200318_UserLogged();

    #region Costruttore
    public CtrlReport()
    {
      InitializeComponent();
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
      Authentication.AuthenticationManager.Instance.LoginSession += Instance_LoginSession;

    }
    #endregion

    #region TRADUZIONI
    protected override void Translate()
    {
      //DATABASE
      tiDatabase.Text = Localization.Localize.LocalizeDefaultString((string)tiDatabase.Tag);
      tiArticleDatabase.Text = Localization.Localize.LocalizeDefaultString((string)tiArticleDatabase.Tag);
      tiLotDatabase.Text = Localization.Localize.LocalizeDefaultString((string)tiLotDatabase.Tag);
      tiPalletAll.Text = Localization.Localize.LocalizeDefaultString((string)tiPalletAll.Tag);

      //GIACENZE
      tiSTOCK.Text = Localization.Localize.LocalizeDefaultString((string)tiSTOCK.Tag);
      tiStockPallet.Text = Localization.Localize.LocalizeDefaultString((string)tiStockPallet.Tag);
      tiStockLotArticle.Text = Localization.Localize.LocalizeDefaultString("LOT")+"/"+ Localization.Localize.LocalizeDefaultString("ARTICLE");
      tiStockByCell.Text = Localization.Localize.LocalizeDefaultString((string)tiStockByCell.Tag);
            
      tiPalletReject.Text = Localization.Localize.LocalizeDefaultString((string)tiPalletReject.Tag);
      tiPalletIn.Text = Localization.Localize.LocalizeDefaultString((string)tiPalletIn.Tag);
      tiPalletOut.Text = Localization.Localize.LocalizeDefaultString((string)tiPalletOut.Tag);
     

      //DISTRIBUTION  
      tiDistribution.Text = Localization.Localize.LocalizeDefaultString((string)tiDistribution.Tag);
      tiDistributionLotArticle.Text = Localization.Localize.LocalizeDefaultString((string)tiDistributionLotArticle.Tag);
      tiPalletFlow.Text = Localization.Localize.LocalizeDefaultString((string)tiPalletFlow.Tag);
    }
    #endregion TRADUZIONI

    #region Eventi controllo
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    #endregion

    private void Instance_LoginSession(object sender, Authentication.LoginSessionEventArgs sessionArgs)
    {
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
    }

    private void CtrlBaseC200318_Unloaded(object sender, RoutedEventArgs e)
    {
      try
      {
        Authentication.AuthenticationManager.Instance.LoginSession -= Instance_LoginSession;
      }
      catch (Exception)
      {

      }
    }
  }
}
