﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200318;
using Model.Custom.C200318.ArticleManager;
using Model.Export;
using View.Common.Languages;
using View.Common.Report;
using Model.Custom.C200318.Report;

namespace View.Custom.C200318.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlAnagraficaLotti.xaml
  /// </summary>
  public partial class CtrlAnagraficaLotti : CtrlBaseC200318
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public int StartIndex { get; set; } = _indexStartValue;
    public int MaxItems { get; set; } = _maxItemsStartValue;

    public C200318_LotFilter Filter { get; set; } = new C200318_LotFilter();

    public ObservableCollectionFast<C200318_Lot> LotL { get; set; } = new ObservableCollectionFast<C200318_Lot>();
    #endregion

   

   
    #region COSTRUTTORE
    public CtrlAnagraficaLotti()
    {
      

      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlLotFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colLotCode.Header = Localization.Localize.LocalizeDefaultString("LOT CODE");
      colDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colDateBorn.Header = Localization.Localize.LocalizeDefaultString("DATE");
      
      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");

      CollectionViewSource.GetDefaultView(dgLotL.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    private void Cmd_RM_Lot_GetAll()
    {
      busyOverlay.IsBusy = true;
      LotL.Clear();
      CommandManagerC200318.RM_Lot_GetAll(this, Filter);
    }
    private void RM_Lot_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;

      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var artL = dwr.Data as List<C200318_Lot>;
      if (artL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (artL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        artL = artL.Take(Filter.MaxItems).ToList();
        LotL.AddRange(artL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

    #endregion


    #region Eventi Ricerca

    private void ctrlLotFilter_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;
      ctrlLotFilter.SearchHighlight = false;

      Cmd_RM_Lot_GetAll();
    }

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_RM_Lot_GetAll();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_RM_Lot_GetAll();
    }

   

    #endregion

    #region Eventi controllo
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();
    }

    private void CtrlBaseC200318_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_Lot)
      {
        try
        {
          Clipboard.SetText((((C200318_Lot)b.Tag)).Code);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }


    #endregion

  

    private async void butExport_Click(object sender, RoutedEventArgs e)
    {
      var view = new CtrlReportExport("Lot", LotL.Select(g => new Exportable_Lot(g)).Cast<IExportable>().ToArray(),typeof(Exportable_Lot));

      //show the dialog
      var result = await DialogHost.Show(view, "RootDialog");

      if (view.Result == ExportResult.OK)
        LocalSnackbar.ShowMessageOkAndOpenFile("EXPORT COMPLETED".TD() + " " + view.FileCreatedName, 5, "OPEN", view.FileCreatedName);

    }

    
  }


  public class Exportable_Lot : IExportable
  {

    public Exportable_Lot(C200318_Lot lotIn)
    {
      Code = lotIn.Code;
      Description = lotIn.Descr;
      Date = lotIn.DateBorn.ToString();      
    }

    [Exportation(ColumnIndex = 0, ColumnName = "CODE")]
    public string Code { get; set; }

    [Exportation(ColumnIndex = 1, ColumnName = "DESCRIPTION")]
    public string Description { get; set; }

    [Exportation(ColumnIndex = 2, ColumnName = "DATE")]
    public string Date { get; set; }


   
  }

}
