﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Wpf;
using Model;
using Model.Custom.C200318.Report;
using View.Common.Languages;

namespace View.Custom.C200318.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlDistributionPalletFlow.xaml
  /// </summary>
  public partial class CtrlDistributionPalletFlow : CtrlBaseC200318
  {

    #region Public Properties
    public C200318_PalletFlowFilter Filter { get; set; } = new C200318_PalletFlowFilter();

    private C200318_PalletFlow palletFlow = new C200318_PalletFlow();  

    public C200318_PalletFlow PalletFlow
    {
      get { return palletFlow; }
      set { palletFlow = value; }
    }

    #endregion

    #region COSTRUTTORE
    public CtrlDistributionPalletFlow()
    {
      InitializeComponent();
      Filter = ctrlPalletFlowFilterHorizontal.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);

      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      axisY.Title = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLETS");
      //axisX.Title = Localization.Localize.LocalizeDefaultString("PALLET");

    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE

    private void Cmd_RM_Pallet_Flow()
    {
      busyOverlay.IsBusy = true;

      PalletFlow.listIn.Clear();
      PalletFlow.listOut.Clear();
      PalletFlow.listReject.Clear();

      CommandManagerC200318.RM_Pallet_Flow(this, Filter);
    }
    private void RM_Pallet_Flow(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      var pF = dwr.Data as C200318_PalletFlow;
      if (pF != null)
      {
        if (pF.listIn != null)
          PalletFlow.listIn.AddRange(pF.listIn.OrderBy(x=>x.DateBorn));
        if (pF.listOut != null)
          PalletFlow.listOut.AddRange(pF.listOut.OrderBy(x => x.DateBorn));
        if (pF.listReject != null)
          PalletFlow.listReject.AddRange(pF.listReject.OrderBy(x => x.DateBorn));

        RefreshChart();
      }
    }

    #endregion

    #region Eventi controllo
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    private void CtrlBaseC200318_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    #endregion


    #region Eventi Ricerca
    private void ctrlPalletFlowFilterHorizontal_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;
      ctrlPalletFlowFilterHorizontal.SearchHighlight = false;

      Cmd_RM_Pallet_Flow();
    }
    #endregion

    
    private void RefreshChart()
    {
      SeriesCollection.Clear();
      Labels.Clear();

      ChartValues<int> palInCountL = new ChartValues<int>();
      ChartValues<int> palOutCountL = new ChartValues<int>();
      ChartValues<int> palRejCountL = new ChartValues<int>();

      TimeSpan x_interval = TimeSpan.FromHours(1);

      DateTime x_min = DateTime.MinValue;
      DateTime x_max = DateTime.MaxValue;

      // if it has been filtered by date range
      if (Filter.DateRangeSelected)
      {
        x_min = Filter.DateBornMin;
        x_max = Filter.DateBornMax;
      }
      else
      {
        // min value of dateborn of each pallet list
        if (PalletFlow != null)
        {
          if ((PalletFlow.listIn.Count > 0) && (PalletFlow.listIn.Count > 0) && (PalletFlow.listIn.Count > 0))
          {

            List<DateTime> minPalL = new List<DateTime>()
            {
              PalletFlow.listIn.Min(pal => pal.DateBorn),
              PalletFlow.listOut.Min(pal => pal.DateBorn),
              PalletFlow.listReject.Min(pal => pal.DateBorn),
            };

            x_min = minPalL.Min();

            List<DateTime> maxPalL = new List<DateTime>()
            {
              PalletFlow.listIn.Max(pal => pal.DateBorn),
              PalletFlow.listOut.Max(pal => pal.DateBorn),
              PalletFlow.listReject.Max(pal => pal.DateBorn),
            };

            x_max = minPalL.Max();
          }
          else
          {
            x_min = Filter.DateBornMin;
            x_max = Filter.DateBornMax;
          }
        }
      }

      TimeSpan range = x_max - x_min;


      x_interval = TimeSpan.FromMinutes(1);

      if (range.TotalHours > 1)
      {
        x_interval = TimeSpan.FromHours(1);

        if (range.TotalDays > 1)
        {
          x_interval = TimeSpan.FromDays(1);

          if (range.TotalDays > 21)
          {
            x_interval = TimeSpan.FromDays(7);

            if (range.TotalDays > 120)
            {
              x_interval = TimeSpan.FromDays(30);

              if (range.TotalDays > 365)
              {
                x_interval = TimeSpan.FromDays(365);
              }
            }
          }
        }
      }


      DateTime x_prev = x_min;
      DateTime x_act = x_min;     

      while (x_act < x_max)
      {
        // x(t) and x(t-1)
        x_prev = x_act;
        x_act += x_interval;

        palInCountL.Add(PalletFlow.listIn.Where(pal => pal.DateBorn >= x_prev && pal.DateBorn < x_act).Count());
        palOutCountL.Add(PalletFlow.listOut.Where(pal => pal.DateBorn >= x_prev && pal.DateBorn < x_act).Count());
        palRejCountL.Add(PalletFlow.listReject.Where(pal => pal.DateBorn >= x_prev && pal.DateBorn < x_act).Count());
        Labels.Add(x_act.ToString());
      }
      
      LineSeries palletInLineSeries = new LineSeries { Title = "PALLET IN".TD(), Values = palInCountL, Stroke = Brushes.Green, Fill = new SolidColorBrush() { Opacity = 0.15, Color = Brushes.Green.Color }, LineSmoothness = (0.1d),};
      SeriesCollection.Add(palletInLineSeries);

      LineSeries palletOutLineSeries = new LineSeries { Title = "PALLET OUT".TD(), Values = palOutCountL, Stroke = Brushes.Blue, Fill = new SolidColorBrush() { Opacity = 0.15, Color = Brushes.Blue.Color }, LineSmoothness = (0.1d), };
      SeriesCollection.Add(palletOutLineSeries);

      LineSeries palletRejLineSeries = new LineSeries { Title = "PALLET REJECT".TD(), Values = palRejCountL, Stroke = Brushes.Red, Fill = new SolidColorBrush() { Opacity = 0.15, Color = Brushes.Red.Color }, LineSmoothness = (0.1d), };
      SeriesCollection.Add(palletRejLineSeries);
      palletFlowChart.UpdateLayout();

      //Formatter = value => new DateTime((long)value).ToString("dd.MM.yyyy");
      Formatter = value => value.ToString("N");
    }

    private SeriesCollection seriesCollection = new SeriesCollection { };

    public SeriesCollection SeriesCollection
    {
      get { return seriesCollection; }
      set { seriesCollection = value;
        NotifyPropertyChanged("SeriesCollection");
      }
    }

    public ObservableCollectionFast<string> Labels { get; set; } = new ObservableCollectionFast<string>();
    public Func<double, string> Formatter { get; set; }

   
  }
  }