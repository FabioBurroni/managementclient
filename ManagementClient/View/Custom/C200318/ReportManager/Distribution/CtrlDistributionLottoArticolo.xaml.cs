﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using LiveCharts;
using LiveCharts.Wpf;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200318.Report;
using Model.Export;
using Utilities.Extensions.MoreLinq;
using View.Common.Languages;
using View.Common.Report;

namespace View.Custom.C200318.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlDistributionLottoArticolo.xaml
  /// </summary>
  public partial class CtrlDistributionLottoArticolo : CtrlBaseC200318
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public C200318_StockLotArticleFilter Filter { get; set; } = new C200318_StockLotArticleFilter();

    public ObservableCollectionFast<C200318_DistributionLotArticle> GiacL { get; set; } = new ObservableCollectionFast<C200318_DistributionLotArticle>();

    #endregion

    #region COSTRUTTORE
    public CtrlDistributionLottoArticolo()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlDistributionLotArticleFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);

      colLotCode.Header = Localization.Localize.LocalizeDefaultString("LOT CODE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colArticleDescription.Header = Localization.Localize.LocalizeDefaultString("ARTICLE DESCRIPTION");
      colStockTotale.Header = Localization.Localize.LocalizeDefaultString("TOTAL STOCK");
      colStockInEmergenziale.Header = Localization.Localize.LocalizeDefaultString("EMERGENCIAL STOCK");


      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      axisX.Title = Localization.Localize.LocalizeDefaultString("LOT") + " - " + Localization.Localize.LocalizeDefaultString("ARTICLE");
      axisY.Title = Localization.Localize.LocalizeDefaultString("PALLET");

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      txtBlkClearSelection.Text = Context.Instance.TranslateDefault((string)txtBlkClearSelection.Tag);
      txtBlkSelectAll.Text = Context.Instance.TranslateDefault((string)txtBlkSelectAll.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      
      CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    private void Cmd_RM_Stock_LotArticle()
    {
      busyOverlay.IsBusy = true;
      GiacL.Clear();
      CommandManagerC200318.RM_Stock_LotArticle(this, Filter);
    }
    private void RM_Stock_LotArticle(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      var giacArtL = dwr.Data as List<C200318_StockLotArticle>;
      if (giacArtL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (giacArtL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        giacArtL = giacArtL.Take(Filter.MaxItems).ToList();

        foreach(var giac in giacArtL)
        {
          GiacL.Add(new C200318_DistributionLotArticle()
          {
            LotCode = giac.LotCode,
            ArticleCode = giac.ArticleCode,
            ArticleDescr = giac.ArticleDescr,
            MinQty = giac.MinQty,
            MaxQty = giac.MaxQty,
            StockTotale = giac.StockTotale,
            StockInEmergenziale = giac.StockInEmergenziale,
            IsSelected = false
          }); 
        }

        RefreshChart();
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

    #endregion

    #region Eventi controllo
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    private void CtrlBaseC200318_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    #endregion


    #region Eventi Ricerca
    private void ctrlDistributionLotArticleFilter_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;
      ctrlDistributionLotArticleFilter.SearchHighlight = false;

      Cmd_RM_Stock_LotArticle();
    }
    #endregion

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_RM_Stock_LotArticle();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_RM_Stock_LotArticle();
    }

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_StockLotArticle)
      {
        try
        {
          Clipboard.SetText((((C200318_StockLotArticle)b.Tag)).ArticleCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyLot_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_StockLotArticle)
      {
        try
        {
          Clipboard.SetText((((C200318_StockLotArticle)b.Tag)).LotCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void RefreshChart()
    {
      SeriesCollection.Clear();
      Labels.Clear();

      ChartValues<double> totalStockValuesL = new ChartValues<double>();
      ChartValues<double> emergencialStockValuesL = new ChartValues<double>();

      GiacL.ForEach(x =>
      { if (x.IsSelected)
        {
          totalStockValuesL.Add(x.StockTotale);
          emergencialStockValuesL.Add(x.StockInEmergenziale);
          Labels.Add(x.LotCode + " - " + x.ArticleCode);
        }
      }); ;

      ColumnSeries totalStockColumnSeries = new ColumnSeries { Title = "TOTAL STOCK".TD(), Values = totalStockValuesL };
      SeriesCollection.Add(totalStockColumnSeries);
      
      ColumnSeries emergencialStockColumnSeries = new ColumnSeries { Title = "EMERGENCIAL STOCK".TD(), Values = emergencialStockValuesL };
      SeriesCollection.Add(emergencialStockColumnSeries);

      Formatter = value => value.ToString("N");
    }

    private SeriesCollection seriesCollection = new SeriesCollection { };

    public SeriesCollection SeriesCollection
    {
      get { return seriesCollection; }
      set { seriesCollection = value;
        NotifyPropertyChanged("SeriesCollection");
      }
    }

    public ObservableCollectionFast<string> Labels { get; set; } = new ObservableCollectionFast<string>();
    public Func<double, string> Formatter { get; set; }

    private void CheckBox_Click(object sender, RoutedEventArgs e)
    {
      RefreshChart();
    }

    private void btnSelectAll_Click(object sender, RoutedEventArgs e)
    {
      if (GiacL != null)
      {
        foreach (var giac in GiacL)
        {
          giac.IsSelected = true;
        }
        CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();

        RefreshChart();
      }
    }

    private void btnClearSelection_Click(object sender, RoutedEventArgs e)
    {
      if (GiacL != null)
      {
        foreach (var giac in GiacL)
        {
          giac.IsSelected = false;
        }
        CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();

        RefreshChart();
      }
    }
  }
  }