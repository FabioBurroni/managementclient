﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200318.Report;
using Model.Export;
using View.Common.Languages;
using View.Common.Report;

namespace View.Custom.C200318.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletOut.xaml
  /// </summary>
  public partial class CtrlPalletOut : CtrlBaseC200318
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public C200318_PalletOutFilter Filter { get; set; } = new C200318_PalletOutFilter();

    public ObservableCollectionFast<C200318_PalletOut> GiacL { get; set; } = new ObservableCollectionFast<C200318_PalletOut>();
    #endregion

    #region COSTRUTTORE
    public CtrlPalletOut()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlPalletOutFilterHorizontal.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colLotCode.Header = Localization.Localize.LocalizeDefaultString("LOT CODE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colData.Header = Localization.Localize.LocalizeDefaultString("DATE");
      colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
      colPosition.Header = Localization.Localize.LocalizeDefaultString("POSITION");
      colCellCode.Header = Localization.Localize.LocalizeDefaultString("CELL CODE");
      colOrderCode.Header = Localization.Localize.LocalizeDefaultString("ORDER CODE");

      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");

      CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE
    private void Cmd_RM_Pallet()
    {
      busyOverlay.IsBusy = true;
      GiacL.Clear();
      CommandManagerC200318.RM_PalletOut(this,Filter);
    }
    private void RM_PalletOut(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      var giacL = dwr.Data as List<C200318_PalletOut>;
      if(giacL!=null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (giacL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        giacL = giacL.Take(Filter.MaxItems).ToList();
        GiacL.AddRange(giacL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }
       
    #endregion

    #region Eventi controllo
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    private void CtrlBaseC200318_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    #endregion


    #region Eventi Ricerca
    private void ctrlPalletOutFilterHorizontal_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;

      Cmd_RM_Pallet();

    }
    #endregion

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_RM_Pallet();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_RM_Pallet();
    }

    private void butCopyLot_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_PalletReject)
      {
        try
        {
          Clipboard.SetText((((C200318_PalletReject)b.Tag)).LotCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyArticle_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_PalletOut)
      {
        try
        {
          Clipboard.SetText((((C200318_PalletOut)b.Tag)).ArticleCode);
        }
        catch
        {

        }
      }
    }
    private void butCopyPalletCode_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_PalletOut)
      {
        try
        {
          Clipboard.SetText((((C200318_PalletOut)b.Tag)).PalletCode);
        }
        catch
        {

        }
      }
    }

    private void butCopyOrder_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_PalletOut)
      {
        try
        {
          Clipboard.SetText((((C200318_PalletOut)b.Tag)).OrderCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }

    }

    private void butCopyPosition_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_PalletOut)
      {
        try
        {
          Clipboard.SetText((((C200318_PalletOut)b.Tag)).Position);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }

    }

    private async void butExport_Click(object sender, RoutedEventArgs e)
    {
      var view = new CtrlReportExport("PalletOut", GiacL.Select(g => new Exportable_PalletOut(g)).Cast<IExportable>().ToArray(), typeof(Exportable_PalletOut));

      //show the dialog
      var result = await DialogHost.Show(view, "RootDialog");

      if (view.Result == ExportResult.OK)
        LocalSnackbar.ShowMessageOkAndOpenFile("EXPORT COMPLETED".TD() + " " + view.FileCreatedName, 5, "OPEN", view.FileCreatedName);

    }


  }

  public class Exportable_PalletOut : IExportable
  {

    public Exportable_PalletOut(C200318_PalletOut palletOut)
    {
      PalletCode = palletOut.PalletCode;
      LotCode = palletOut.LotCode;
      ArticleCode = palletOut.ArticleCode;
      Date = palletOut.DateBorn.ToString();
      Quantity = palletOut.Qty.ToString();
      Position = palletOut.Position.ToString();
      CellCode = palletOut.CellCode;
      OrderCode = palletOut.OrderCode.ToString();
    }

    [Exportation(ColumnIndex = 0, ColumnName = "PALLET CODE")]
    public string PalletCode { get; set; }

    [Exportation(ColumnIndex = 1, ColumnName = "LOT CODE")]
    public string LotCode { get; set; }

    [Exportation(ColumnIndex = 2, ColumnName = "ARTICLE CODE")]
    public string ArticleCode { get; set; }

    [Exportation(ColumnIndex = 3, ColumnName = "QUANTITY")]
    public string Quantity { get; set; }

    [Exportation(ColumnIndex = 4, ColumnName = "DATE")]
    public string Date { get; set; }

    [Exportation(ColumnIndex = 5, ColumnName = "POSITION")]
    public string Position { get; set; }

    [Exportation(ColumnIndex = 6, ColumnName = "CELL CODE")]
    public string CellCode { get; set; }

    [Exportation(ColumnIndex = 7, ColumnName = "ORDER CODE")]
    public string OrderCode { get; set; }

    
  }

}
