﻿
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using Model.Custom.C200318.Report;

namespace View.Custom.C200318.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlStockFilter.xaml
  /// </summary>
  public partial class CtrlStockByCellFilterHorizontal : CtrlBaseC200318
  {

    #region EVENTI
    public delegate void OnSearchHandler();
    public event OnSearchHandler OnSearch;
    #endregion

    #region Fields
    private const int _indexStartValue = 0;
    #endregion

    #region PUBLIC PROPERTIES
    public C200318_StockByCellFilter Filter { get; set; } = new C200318_StockByCellFilter();

    #region Press to search highlight

    private bool searchHighlight = true;

    public bool SearchHighlight
    {
      get { return searchHighlight; }
      set
      {
        searchHighlight = value;
        NotifyPropertyChanged("SearchHighlight");
      }
    }

    #endregion
    #endregion

    #region Constructor
    public CtrlStockByCellFilterHorizontal()
    {
      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {

      HintAssist.SetHint(cbMaxItems, Localization.Localize.LocalizeDefaultString((string)cbMaxItems.Tag));
      HintAssist.SetHint(txtBoxLotCode, Localization.Localize.LocalizeDefaultString((string)txtBoxLotCode.Tag));
      HintAssist.SetHint(txtBoxArticleCode, Localization.Localize.LocalizeDefaultString((string)txtBoxArticleCode.Tag));
      HintAssist.SetHint(txtBoxArticleDescr, Localization.Localize.LocalizeDefaultString((string)txtBoxArticleDescr.Tag));
      HintAssist.SetHint(cbWorkModality, Localization.Localize.LocalizeDefaultString((string)cbWorkModality.Tag));

      btnCancel.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnCancel.Tag);
      btnSearch.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnSearch.Tag);
    }

    #endregion TRADUZIONI

    #region Events
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
        return;

      Translate();

    }
    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
    }

    private void btnSearch_Click(object sender, RoutedEventArgs e)
    {
      if (cbMaxItems.SelectedItem != null)
      {
        Filter.MaxItems = (int)cbMaxItems.SelectedItem;
      }
      Filter.Index = _indexStartValue;
      Search();
    }
    #endregion

    #region private Methods
    private void Search()
    {
      OnSearch?.Invoke();
    }

    #endregion

    private void TextBox_KeyDown(object sender, KeyEventArgs e)
    {
      if(e.Key==Key.Return)
        Search();
    }
  }
}
