﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200318.Report;
using Model.Export;
using View.Common.Languages;
using View.Common.Report;

namespace View.Custom.C200318.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlStockPallet.xaml
  /// </summary>
  public partial class CtrlStockPallet : CtrlBaseC200318
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public C200318_StockPalletFilter Filter { get; set; } = new C200318_StockPalletFilter();

    public ObservableCollectionFast<C200318_StockPallet> GiacL { get; set; } = new ObservableCollectionFast<C200318_StockPallet>();
    #endregion

    #region COSTRUTTORE
    public CtrlStockPallet()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlStockFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
      colPosition.Header = Localization.Localize.LocalizeDefaultString("POSITION");
      colProductionDate.Header = Localization.Localize.LocalizeDefaultString("PRODUCTION DATE"); 
      colLotCode.Header = Localization.Localize.LocalizeDefaultString("LOT CODE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colArticleDescription.Header = Localization.Localize.LocalizeDefaultString("ARTICLE DESCRIPTION");
      colQty.Header = Localization.Localize.LocalizeDefaultString("PRODUCT QUANTITY");

      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");

      CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE
    private void Cmd_RM_Stock_Pallet_ByArea()
    {
      busyOverlay.IsBusy = true;
      GiacL.Clear();
      CommandManagerC200318.RM_Stock_Pallet_ByWorkModality(this,Filter);
    }
    private void RM_Stock_Pallet_ByWorkModality(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var giacL = dwr.Data as List<C200318_StockPallet>;
      if(giacL!=null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (giacL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        giacL = giacL.Take(Filter.MaxItems).ToList();
        GiacL.AddRange(giacL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

     #endregion

    #region Eventi controllo
      private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    private void CtrlBaseC200318_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    #endregion


    #region Eventi Ricerca
    private void ctrlStockFilter_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;
      ctrlStockFilter.SearchHighlight = false;

      Cmd_RM_Stock_Pallet_ByArea();
    }
    #endregion

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_RM_Stock_Pallet_ByArea();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_RM_Stock_Pallet_ByArea();
    }

    private void butCopyArticle_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_StockPallet)
      {
        try
        {
          Clipboard.SetText((((C200318_StockPallet)b.Tag)).ArticleCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyPalletCode_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_StockPallet)
      {
        try
        {
          Clipboard.SetText((((C200318_StockPallet)b.Tag)).Code);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }
    private void butCopyLot_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_StockPallet)
      {
        try
        {
          Clipboard.SetText((((C200318_StockPallet)b.Tag)).LotCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private async void butExport_Click(object sender, RoutedEventArgs e)
    {
      var view = new CtrlReportExport("PalletStock", GiacL.Select(g => new Exportable_PalletStock(g)).Cast<IExportable>().ToArray(), typeof(Exportable_PalletStock));

      //show the dialog
      var result = await DialogHost.Show(view, "RootDialog");

      if (view.Result == ExportResult.OK)
        LocalSnackbar.ShowMessageOkAndOpenFile("EXPORT COMPLETED".TD() + " " + view.FileCreatedName, 5, "OPEN", view.FileCreatedName);
    }

  }

  public class Exportable_PalletStock : IExportable
  {

    public Exportable_PalletStock(C200318_StockPallet palIn)
    {
      PalletCode = palIn.Code;
      Position = palIn.MapperEntityCode;
      ProductionDate = palIn.DateBorn.ToString();
      LotCode = palIn.LotCode.ToString();
      ArticleCode = palIn.ArticleCode.ToString();
      ArticleDescription = palIn.ArticleDescr.ToString();
      ProductQuantity = palIn.Qty.ToString();
    }

    [Exportation(ColumnIndex = 0, ColumnName = "PALLET CODE")]
    public string PalletCode { get; set; }

    [Exportation(ColumnIndex = 1, ColumnName = "POSITION")]
    public string Position { get; set; }

    [Exportation(ColumnIndex = 2, ColumnName = "PRODUCTION DATE")]
    public string ProductionDate { get; set; }

    [Exportation(ColumnIndex = 3, ColumnName = "LOT CODE")]
    public string LotCode { get; set; }

    [Exportation(ColumnIndex = 4, ColumnName = "ARTICLE CODE")]
    public string ArticleCode { get; set; }

    [Exportation(ColumnIndex = 5, ColumnName = "ARTICLE DESCRIPTION")]
    public string ArticleDescription { get; set; }

    [Exportation(ColumnIndex = 6, ColumnName = "PRODUCT QUANTITY")]
    public string ProductQuantity { get; set; }


   

  }
}
