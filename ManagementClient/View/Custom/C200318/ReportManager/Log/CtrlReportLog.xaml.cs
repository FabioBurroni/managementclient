﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Custom.C200318.Report;

using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200318.Report.Log;
using Model.Export;
using View.Common.Report;
using View.Common.Languages;
using Utilities.Extensions;

namespace View.Custom.C200318.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlReportLog.xaml
  /// </summary>
  public partial class CtrlReportLog : CtrlBaseC200318
  {
    #region CTOR
    public CtrlReportLog()
    {
      LogFilter.Index = _indexStartValue;
      LogFilter.MaxItems = _maxItemsStartValue;

      InitializeComponent();
    }
    #endregion

    #region TRANSLATE
    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colData.Header = Localization.Localize.LocalizeDefaultString("DATE");
      colDescription.Header= Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colParameters.Header= Localization.Localize.LocalizeDefaultString("PARAMETERS");
      colUser.Header= Localization.Localize.LocalizeDefaultString("USER");
      colCommand.Header = Localization.Localize.LocalizeDefaultString("COMMAND");

      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");
      txtblkDateRange.Text = Localization.Localize.LocalizeDefaultString((string)txtblkDateRange.Tag);

      ctrlDateStart.Title = Localization.Localize.LocalizeDefaultString((string)ctrlDateStart.Tag);
      ctrlDateEnd.Title = Localization.Localize.LocalizeDefaultString((string)ctrlDateEnd.Tag);

      CollectionViewSource.GetDefaultView(dgvLog.ItemsSource).Refresh();
    }

    #endregion

    #region PUBLIC PROPERTIES
    public ObservableCollectionFast<C200318_LogXmlCommand> LogL { get; set; } = new ObservableCollectionFast<C200318_LogXmlCommand>();

    public C200318_LogFilter LogFilter { get; set; } = new C200318_LogFilter();
    #endregion

    #region COMMAND RESPONSE
    private void Cmd_RM_LogXmlCommand()
    {
      LogL.Clear();
      //_realCommandL.Clear();
      CommandManagerC200318.RM_LogXmlCommand(this, LogFilter);
    }

    private void RM_LogXmlCommand(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var logL = dwr.Data as List<C200318_LogXmlCommand>;
      if (logL != null)
      {
        logL.ForEach(log => LogDecodeDescr(log));
        LogL.AddRange(logL);
      }

      LogFilter.TotalItems = LogL.Count;

    }


    #endregion

    #region PRIVATE METHODS
    List<string> _realCommandL = new List<string>();
    private void LogDecodeDescr(C200318_LogXmlCommand command)
    {
      string realCommand = string.Empty;
      List<string> parameterL = new List<string>();

      #region PLUGIN COMMAND
      if(command.Command.ToLower().Contains("plugin"))
      {
        int iS = command.Command.IndexOf("(") + 1;
        int iE = command.Command.IndexOf(",");

        if(iS>0 && iE>0 && iE>iS)
        {
          realCommand = command.Command.Substring(iS, iE - iS);
          if (command.Command.Contains(","))
          {
            var tmpParL = command.Command.Split(',').ToList().Skip(1).ToList();
            foreach (var p in tmpParL)
            {
              parameterL.Add(p.Replace(')', ' ').Trim());
            }
          }
        }
        else
        {
          //...plugin command without parameters
          iE = command.Command.IndexOf(")");
          if(iS > 0 && iE > 0 && iE > iS)
          {
            realCommand = command.Command.Substring(iS, iE - iS);
          }
        }
        //Console.WriteLine(parameterL);
      }
      #endregion

      #region NO PLUGIN COMMAND
      else
      {

        int iS = command.Command.IndexOf(":") + 1;
        int iE = command.Command.IndexOf("(");

        if(iS>0 && iE>0 && iE>iS)
        {
          realCommand = command.Command.Substring(iS, iE- iS);
          int iFrom = command.Command.IndexOf('(') + 1;
          int iTo = command.Command.IndexOf(')');

          if(iFrom>0 && iTo>0 && iTo>iFrom)
          {
            var parameterS = command.Command.Substring(iFrom, iTo - iFrom);
            parameterL = parameterS.Split(',').ToList();
          }
          //Console.WriteLine(parameterL);
        }
      }

      #endregion

      if(!_realCommandL.Contains(realCommand))
      {
        _realCommandL.Add(realCommand);
        Console.WriteLine(realCommand);
      }

      switch (realCommand)
      {
        /*
         

         */
        case "OM_Order_UpdateState":
          command.Description = "CHANGE ORDER STATUS".TD();
          command.Parameters = $" ORDER ID:".TD() + $"{parameterL[0]}" +
            $" STATUS:".TD() + $"{parameterL[1]}";
          break;
        case "DFG_DefragStartingCondition_Set":
          command.Description = Context.Instance.TranslateDefault("SET DEFRAG STARTING CONDITION");
          break;
        case "DFG_DefragStatus_Stop":
          command.Description = Context.Instance.TranslateDefault("STOP DEFRAG PROCEDURE");
          break;
        case "sat_Conf_SetProperty":
          command.Description = Context.Instance.TranslateDefault("CHANGE CONFIGURATION PARAMETER");
          break;
        case "sat_SetWhCellStatus":
          command.Description = "CHANGE STATUS OF LANE".TD();
          command.Parameters=" LANE:".TD() + $"{parameterL[0]}" +
            " STATUS:".TD() + $"{parameterL[1]}";
            
          break;
        case "sat_MaintenanceTransferPallet":
          command.Description = "TRANSFER PALLET BETWEEN LANES".TD();
          command.Parameters =
            " FROM:".TD() + $"{parameterL[0]}" +
            " TO:".TD() + $"{parameterL[1]}" +
            " NUMBER OF PALLETS:".TD() + $"{parameterL[2]}";
          break;
        case "sat_SetPalletPosHandling":
          command.Description = "MAPPING PALLET IN POSITION".TD();
          command.Parameters =
            " FROM:".TD() + $"{parameterL[0]}" +
            " TO:".TD() + $"{parameterL[1]}" +
            " PALLET CODE:".TD() + $"{parameterL[2]}";
          break;
        case "sat_SetPalletPosWH":
          command.Description = "MAPPING PALLET IN LANE".TD();
          command.Parameters =
            " FROM:".TD() + $"{parameterL[0]}" +
            " TO:".TD() + $"{parameterL[1]}" +
            " PALLET CODE:".TD() + $"{parameterL[2]}";
          break;
        case "sat_ExpellPallets":
          command.Description = "EXPELL PALLETS".TD();
          command.Parameters =
            " FROM:".TD() + $"{parameterL[0]}" +
            " TO:".TD() + $"{parameterL[1]}" +
            " NUMBER OF PALLETS:".TD() + $"{parameterL[2]}";
          break;
        case "sat_ExpellPallet":
          command.Description = "PALLET EXPELLED".TD();
          command.Parameters =
            " PALLET CODE:".TD() + $"{parameterL[0]}" +
            " TO:".TD() + $"{parameterL[1]}"; 
          break;
        case "sat_PalletInfo":
          command.Description = Context.Instance.TranslateDefault("REQUEST INFO FOR PALLET");
          
          break;
        case "sat_SatMasterChangeStatus":
          command.Description = "CHANGE STATUS OF MASTER".TD();
          command.Parameters =
            " MASTER:".TD() + $"{parameterL[0]}" +
            " STATUS :".TD() + $"{parameterL[1]}";
          break;
        case "sat_MapSlaveOnMaster":
          command.Description = "MAPPING SATELLITE ON MASTER".TD();
          command.Parameters =
            " SATELLITE:".TD() + $"{parameterL[0]}" +
            " MASTER:".TD() + $"{parameterL[1]}";
          break;
        case "sat_ResetSatelliteMaster":
          command.Description = "RESET COMMUNICATION OF MASTER";
          command.Parameters =
          " MASTER:".TD() + $"{parameterL[0]}";
          break;
        case "sat_NotificationAck":
          command.Description = Context.Instance.TranslateDefault("ACKWNOLEDGE OF NOTIFICATION");
          
          break;
        case "sat_Maintenance_MasterMoving":
          command.Description = "MOVE MASTER".TD();
          command.Parameters =
            " DESTINATION:".TD() + $"{parameterL[1]}";
          break;
        case "sat_SatSlaveChangeStatus":
          command.Description = "CHANGE STATUS OF SATELLITE".TD();
          command.Parameters =
            " SATELLITE:".TD() + $"{parameterL[0]}" +
            " STATUS:".TD() + $"{parameterL[1]}";
          break;
        case "sat_MapSatelliteOnPosition":
          command.Description = "MAPPING MASTER OR SATELLITE  ON LANE".TD();
          command.Parameters =
            ":".TD() + $"{parameterL[0]}" +
            " DESTINATION:".TD() + $"{parameterL[1]}";
          break;
        case "sat_JobDelete":
          command.Description = "JOB DELETED".TD();
          command.Parameters =
            " JOB ID:".TD() + $"{parameterL[0]}";
          break;
        case "sat_RestoreMasterDelete":
          command.Description = "RESTORE ACTIVITY DELETED".TD();
          command.Parameters =
            " MASTER:".TD() + $"{parameterL[0]}";
          break;
        case "sat_Charge":
          command.Description = "SATELLITE RECHARGE FORCED".TD();
          command.Parameters =
            " SATELLITE:".TD() + $"{parameterL[0]}";
          break;
        case "sat_PositionCharging_SetCharge":
          command.Description = "BATTERY CHARGER STATUS FORCED".TD();
          command.Parameters =
            " CHARGER:".TD() + $"{parameterL[0]}" +
            " STATUS:".TD() + $"{((parameterL[1].ConvertTo<bool>()) ? "ON".TD() : "OFF".TD())}";
          break;
        case "sat_ResetPosition":
          command.Description = "POSITION COMMUNICATION RESET".TD();
          command.Parameters =
            " POSITION CODE:".TD() + $"{parameterL[0]}";
          break;
        case "sat_SetPositionStatus":
          command.Description = "CHANGE POSITION STATUS".TD();
          command.Parameters =
            " POSITION CODE:".TD() + $"{parameterL[0]}" + 
            " STATUS:".TD() + $"{parameterL[1]}";
          break;
        case "sat_Status":
          command.Description = "CREATE JOB FOR SATELLITE STATUS".TD();
          command.Parameters =
          " SATELLITE:".TD() + $"{parameterL[0]}";
          break;
        case "sat_ParkOnPosition":
          command.Description = "PARK SATELLITE ON LANE".TD();
          command.Parameters =
            " SATELLITE:".TD() + $"{parameterL[0]}" +
            " DESTINATION:".TD() + $"{parameterL[1]}";
          break;
        case "sat_RestoreMaster":
          command.Description = "MANUAL RESTORE OF MASTER".TD();
          command.Parameters =
            " MASTER:".TD() + $"{parameterL[0]}" +
            " ACTION:".TD() + $"{parameterL[1]}";
          if(parameterL.Count==3)
            command.Parameters += " INFO:".TD() + $"{parameterL[2]}";
          break;
        default:
          command.Description = "";
          break;
      }
    }
    #endregion

    #region PAGINATION
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (LogFilter.Index > _indexStartValue)
        LogFilter.Index--;
      Cmd_RM_LogXmlCommand();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      LogFilter.Index++;
      Cmd_RM_LogXmlCommand();
    }
    #endregion

    #region EVENTS

    private async void butExport_Click(object sender, RoutedEventArgs e)
    {
      var view = new CtrlReportExport("ReportLog", LogL.Select(g => new Exportable_LogXmlCommand(g)).Cast<IExportable>().ToArray(), typeof(Exportable_LogXmlCommand));
      //show the dialog
      var result = await DialogHost.Show(view, "RootDialog");
      if (view.Result == ExportResult.OK)
        LocalSnackbar.ShowMessageOkAndOpenFile("EXPORT COMPLETED".TD() + " " + view.FileCreatedName, 5, "OPEN", view.FileCreatedName);

    }

    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      LogFilter.Reset();

    }

    private void btnSearch_Click(object sender, RoutedEventArgs e)
    {
      Cmd_RM_LogXmlCommand();
    }

    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      _realCommandL.Clear();
    }
    #endregion
  }

  public class Exportable_LogXmlCommand : IExportable
  {

    public Exportable_LogXmlCommand(C200318_LogXmlCommand log)
    {
      DateBorn = log.DateBorn.ConvertToDateTimeFormatString();
      Description = log.Description;
      User = log.User;
      Command = log.Command;


    }

    [Exportation(ColumnIndex = 0, ColumnName = "DATE")]
    public string DateBorn { get; set; }

    [Exportation(ColumnIndex = 1, ColumnName = "DESCRIPTION")]
    public string Description{ get; set; }

    [Exportation(ColumnIndex = 2, ColumnName = "USER")]
    public string User { get; set; }

    [Exportation(ColumnIndex = 3, ColumnName = "COMMAND")]
    public string Command { get; set; }

  }
}

