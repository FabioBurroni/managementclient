﻿using System;
using System.Windows.Data;
using System.Globalization;

using Model.Custom.C200318.OrderManager;

namespace View.Custom.C200318.Converter
{
  public class OrderStateToStringConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is C200318_State)
      {
        switch((C200318_State)value)
        {
          case C200318_State.CDONE:
            return "COMPLETED";
          case C200318_State.CEXEC:
            return "EXECUTING";
          case C200318_State.CKILL:
            return "KILLED";
          case C200318_State.CPAUSE:
            return "PAUSED";
          case C200318_State.CWAIT:
            return "WAITING";
          case C200318_State.LDONE:
            return "COMPLETED";
          case C200318_State.LEDIT:
            return "EDITING";
          case C200318_State.LEXEC:
            return "EXECUTING";
          case C200318_State.LKILL:
            return "KILLED";
          case C200318_State.LPAUSE:
            return "PAUSED";
          case C200318_State.LUNCOMPLETE:
            return "NOT COMPLETED";
          case C200318_State.LWAIT:
            return "WAITING";
          case C200318_State.SERVICE:
            return "SERVICE";
        }
      }
      return "";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
