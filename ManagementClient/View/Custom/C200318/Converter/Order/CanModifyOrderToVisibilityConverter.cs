﻿using System;
using System.Windows;
using System.Globalization;
using System.Windows.Data;
using Model.Custom.C200318.OrderManager;

namespace View.Custom.C200318.Converter
{
  public class CanModifyOrderToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is C200318_State)
      {
        if (((C200318_State)value) == C200318_State.LWAIT)
          return Visibility.Visible;
      }
      return Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
