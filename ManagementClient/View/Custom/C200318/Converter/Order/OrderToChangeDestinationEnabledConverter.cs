﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Model.Custom.C200318.OrderManager;

namespace View.Custom.C200318.Converter
{
  public class OrderToChangeDestinationEnabledConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value!=null && value is C200318_Order)
      {
        return ((C200318_Order)value).State == C200318_State.LWAIT;
      }

      return false;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
