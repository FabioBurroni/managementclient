﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

using Model;
using Model.Custom.C200318;
using Model.Custom.C200318.OrderManager;

namespace View.Custom.C200318.Converter
{
  public class CmpListToQtyConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (!(parameter is string))
        return 0;

      if(value is ObservableCollectionFast<C200318_OrderCmp>)
      {
        ObservableCollectionFast<C200318_OrderCmp> cmpL = value as ObservableCollectionFast<C200318_OrderCmp>;

        if ((string)parameter == "qty_requested")
          return cmpL.Sum(cmp => cmp.QtyRequested);
        if ((string)parameter == "qty_delivered")
          return cmpL.Sum(cmp => cmp.QtyDelivered);
        if ((string)parameter == "qty_intransit")
          return cmpL.Sum(cmp => cmp.NumPalletInTransit);
        if ((string)parameter == "qty_remaining")
          return cmpL.Sum(cmp => cmp.QtyRemaining);
      }
      return 0;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
