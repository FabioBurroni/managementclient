﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Model.Custom.C200318;
using Model.Custom.C200318.OrderManager;


namespace View.Custom.C200318.Converter
{
  public class OrderStateToColorConverter : IValueConverter
  {

    public Brush StateWait { get; set; } = Brushes.Orange;
    public Brush StateDone{ get; set; } = Brushes.Green;
    public Brush StateExec{ get; set; } = Brushes.Blue;
    public Brush StatePause{ get; set; } = Brushes.Blue;
    public Brush StateKill{ get; set; } = Brushes.Red;
    public Brush StateDefault{ get; set; } = Brushes.Black;


    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is C200318_State)
      {
        var state = (C200318_State)value;
        switch (state)
        {
          case C200318_State.LWAIT:
            return StateWait;
          case C200318_State.LEXEC:
            return StateExec;
          case C200318_State.LPAUSE:
            return StatePause;
          case C200318_State.LKILL:
            return StateKill;
          case C200318_State.LDONE:
            return StateDone;
          case C200318_State.LUNCOMPLETE:
            return StateDefault;
          default:
            return StateDefault;
        }
      }
      return StateDefault;

    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
