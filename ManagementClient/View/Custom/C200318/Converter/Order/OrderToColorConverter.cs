﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Model.Custom.C200318;

namespace View.Custom.C200318.Converter
{
  public class OrderToColorConverter : IValueConverter
  {

    private bool _IsBackground;
    public bool IsBackground
    {
      get { return _IsBackground; }
      set
      {
        if (value != _IsBackground)
        {
          _IsBackground = value;
        }
      }
    }

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is C200318_ListColor)
      {
        switch ((C200318_ListColor)value)
        {
          case C200318_ListColor.AZURE:
            return IsBackground ? Brushes.Black : Brushes.Aquamarine;
          case C200318_ListColor.BLUE:
            return IsBackground ? Brushes.White : Brushes.Blue;
          case C200318_ListColor.BROWN:
            return IsBackground ? Brushes.White : Brushes.Brown;
          case C200318_ListColor.GRAY:
            return IsBackground ? Brushes.White : Brushes.Gray;
          case C200318_ListColor.GREEN:
            return IsBackground ? Brushes.White : Brushes.Green;
          case C200318_ListColor.ORANGE:
            return IsBackground ? Brushes.White : Brushes.Orange;
          case C200318_ListColor.PINK:
            return IsBackground ? Brushes.Black : Brushes.Pink;
          case C200318_ListColor.RED:
            return IsBackground ? Brushes.White : Brushes.Red;
          case C200318_ListColor.YELLOW:
            return IsBackground ? Brushes.Black : Brushes.Yellow;
          case C200318_ListColor.WHITE:
            return IsBackground ? Brushes.Black : Brushes.White;
          default:
            return IsBackground ? Brushes.Gray : Brushes.Azure;
        }
      }

      return Brushes.White;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
