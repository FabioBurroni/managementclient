﻿using System;
using System.Globalization;
using System.Windows.Data;
using Model.Custom.C200318;

namespace View.Custom.C200318.Converter
{
  public class CustomResultToImgUrlConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {


    if (value is C200318_CustomResult)
      {
        C200318_CustomResult re = (C200318_CustomResult)value;

        string s = "/View;component/Common/Images/";

        switch (re)
        {
          case C200318_CustomResult.UNDEFINED: return s + "warning.png";
          case C200318_CustomResult.OK: return s + "ok.png";
          case C200318_CustomResult.DATABASE_NOT_CONNECTED: return s + "Result/database.png";
          case C200318_CustomResult.STORED_PROCEDURE_EXCEPTION: return s + "Result/database.png";
          case C200318_CustomResult.QUERY_EXCEPTION: return s + "Result/database.png";
          case C200318_CustomResult.ERROR_READING_DATA_FROM_PLC: return s + "warning_red.png";
          case C200318_CustomResult.ERROR_DATA_FROM_PLC_NOT_VALID: return s + "warning_red.png";
          case C200318_CustomResult.UNTRANSLATED_ERROR: return s + "Result/question.png";
          case C200318_CustomResult.CONFIGURATION_ERROR: return s + "Result/parameters.png";
          case C200318_CustomResult.PARAMETERS_ERROR: return s + "Result/parameters.png";
          case C200318_CustomResult.QRCODE_NOT_READ: return s + "Result/barcode_error.png";
          case C200318_CustomResult.QRCODE_WRONG_FORMAT: return s + "Result/barcode_error.png";
          case C200318_CustomResult.QRCODE_PALLETCODE_NOT_VALID: return s + "Result/barcode_error.png";
          case C200318_CustomResult.QRCODE_ARTICLECODE_NOT_VALID: return s + "Result/barcode_error.png";
          case C200318_CustomResult.QRCODE_LOTCODE_NOT_VALID: return s + "Result/barcode_error.png";
          case C200318_CustomResult.QRCODE_QTY_NOT_VALID: return s + "Result/barcode_error.png";
          case C200318_CustomResult.QRCODE_EXPIRYDATE_NOT_VALID: return s + "Result/barcode_error.png";
          case C200318_CustomResult.QRCODE_LOTLENGTH_NOT_VALID: return s + "Result/barcode_error.png";
          case C200318_CustomResult.LOT_NOT_VALID: return s + "order.png";
          case C200318_CustomResult.LOT_ERROR_INSERT: return s + "order.png";
          case C200318_CustomResult.PALLETTYPE_NOT_VALID: return s + "Result/pallet_top_error.png";
          case C200318_CustomResult.ARTICLE_NOT_VALID: return s + "Result/package_unknown.png";
          case C200318_CustomResult.ARTICLE_NOT_PRESENT: return s + "Result/package_unknown.png";
          case C200318_CustomResult.ARTICLETYPE_NOT_PRESENT: return s + "Result/package_unknown.png";
          case C200318_CustomResult.ARTICLE_ERROR_INSERT: return s + "Result/package_error.png";
          case C200318_CustomResult.ARTICLE_ERROR_UPDATE: return s + "Result/package_error.png";
          case C200318_CustomResult.ARTICLE_TYPECUSTOM_NOT_PRESENT: return s + "Result/package_error.png";
          case C200318_CustomResult.ARTICLE_UNIT_NOT_PRESENT: return s + "Result/package_error.png";
          case C200318_CustomResult.ARTICLE_DESCRIPTION_NOT_VALID: return s + "Result/package_error.png";
          case C200318_CustomResult.PALLET_NOT_VALID: return s + "Result/pallet_top_error.png";
          case C200318_CustomResult.PALLET_NOT_FOUND: return s + "Result/pallet_top_error.png";
          case C200318_CustomResult.PALLET_INSERT_ERROR: return s + "Result/pallet_top_error.png";
          case C200318_CustomResult.PALLET_ALREADY_PRESENT_IN_WH:  return s + "Result/pallet_full_exclamation.png";
          case C200318_CustomResult.PALLET_ALREADY_PRESENT_IN_HANDLING: return s + "Result/pallet_full_exclamation.png";
          case C200318_CustomResult.PALLET_DESTINED_TO_REJECT: return s + "Result/pallet_full_exclamation.png";
          case C200318_CustomResult.PALLET_ALREADY_MAPPED_IN_POSITION: return s + "warnResult/pallet_full_exclamationing_red.png";
          case C200318_CustomResult.PALLET_SERVICE_ERROR_CREATING: return s + "Result/pallet_full_exclamation.png";
          case C200318_CustomResult.PALLET_WITHOUT_DESTINATION: return s + "Result/pallet_full_question.png";
          case C200318_CustomResult.PALLET_DESTINED_TO_EXIT: return s + "Result/pallet_full_exclamation.png";
          case C200318_CustomResult.PALLET_TRACKING_ERROR: return s + "Result/pallet_full_question.png";
          case C200318_CustomResult.PALLET_ALREADY_PRESENT_IN_DB: return s + "Result/pallet_full_question.png";
          case C200318_CustomResult.PALLET_ALREADY_RESERVED: return s + "Result/pallet_full_question.png";
          case C200318_CustomResult.PALLETCUSTOM_INSERT_ERROR: return s + "Result/pallet_full_exclamation.png";
          case C200318_CustomResult.PALLETCUSTOM_NOT_FOUND: return s + "Result/pallet_full_exclamation.png";
          case C200318_CustomResult.PALLETCUSTOM_UPDATE_ERROR: return s + "Result/pallet_full_exclamation.png";
          case C200318_CustomResult.PRODUCT_INSERT_ERROR: return s + "Result/question.png";
          case C200318_CustomResult.MAPPING_INSERT_ERROR: return s + "Result/question.png";
          case C200318_CustomResult.POSITION_NOT_FOUND: return s + "Result/question.png";
          case C200318_CustomResult.MAPPING_PALLET_ERROR: return s + "Result/question.png";
          case C200318_CustomResult.SHAPECONTROL_OVERFLOW_WIDTH_RIGHT: return s + "Result/pallet_width_right_error.png";
          case C200318_CustomResult.SHAPECONTROL_OVERFLOW_WIDTH_LEFT: return s + "Result/pallet_width_left_error.png";
          case C200318_CustomResult.SHAPECONTROL_OVERFLOW_WIDTH_RIGHTLEFT: return s + "Result/pallet_width_right_left_error.png";
          case C200318_CustomResult.SHAPECONTROL_OVERFLOW_DEPTH_REAR: return s + "Result/pallet_depth_rear_error.png";
          case C200318_CustomResult.SHAPECONTROL_OVERFLOW_DEPTH_FRONT: return s + "Result/pallet_depth_front_error.png";
          case C200318_CustomResult.SHAPECONTROL_OVERFLOW_DEPTH_REARFRONT: return s + "Result/pallet_depth_front_rear_error.png";
          case C200318_CustomResult.SHAPECONTROL_OVERFLOW_HEIGHT: return s + "palletHeightError.png";
          case C200318_CustomResult.SHAPECONTROL_OVERFLOW_WEIGHT: return s + "weight_red.png";
          case C200318_CustomResult.SHAPECONTROL_NO_AVAILABLE_AREA: return s + "warning_red.png";
          case C200318_CustomResult.SHAPECONTROL_NO_REACHABLE_AREA: return s + "warning_red.png";
          case C200318_CustomResult.SHAPECONTROL_NO_AVAILABLE_CELL: return s + "warning_red.png";
          case C200318_CustomResult.SHAPECONTROL_PALLET_NOT_VALID: return s + "Result/pallet_top_error.png";
          case C200318_CustomResult.SHAPECONTROL_ARTICLE_NOT_VALID: return s + "Result/package_unknown.png";
          case C200318_CustomResult.SHAPECONTROL_ARTICLE_NOT_ACCEPTED: return s + "Result/package_error.png";
          case C200318_CustomResult.SHAPECONTROL_LABEL_NOT_VALID: return s + "Result/barcode_error.png";
          case C200318_CustomResult.SHAPECONTROL_LABEL_NOT_READ: return s + "Result/barcode_error.png";
          case C200318_CustomResult.SHAPECONTROL_PALLETTYPE_NOT_VALID: return s + "Result/pallet_top_error.png";
          case C200318_CustomResult.SHAPECONTROL_PALLET_IN_WH: return s + "warning_red.png";
          case C200318_CustomResult.SHAPECONTROL_GENERIC_ERROR: return s + "warning_red.png";
          case C200318_CustomResult.SHAPECONTROL_ERROR_READING_DATA_FROM_PLC: return s + "warning_red.png";
          case C200318_CustomResult.SHAPECONTROL_BAR_CODE_NOT_READ: return s + "Result/barcode_error.png";
          case C200318_CustomResult.SHAPECONTROL_BAR_CODE_NOT_VALID: return s + "Result/barcode_error.png";
          case C200318_CustomResult.SHAPECONTROL_ROUTING_SWITCHING: return s + "warning_red.png";
          case C200318_CustomResult.SHAPECONTROL_ROUTE_NOT_AVAILABLE: return s + "warning_red.png";
          case C200318_CustomResult.SHAPECONTROL_SLATS_NOK: return s + "warning_red.png";
          case C200318_CustomResult.RESERVED_CELL_ERROR: return s + "warning_red.png";
          case C200318_CustomResult.FINALDEST_ERROR: return s + "warning_red.png";
          case C200318_CustomResult.JOB_ERROR_CREATE: return s + "warning_red.png";
          case C200318_CustomResult.JOB_CANNOT_CREATE: return s + "warning_red.png";
          case C200318_CustomResult.ORDER_NUM_ROWS_NOT_VALID: return s + "warning_red.png";
          case C200318_CustomResult.ORDER_ERROR_UPDATE: return s + "warning_red.png";
          case C200318_CustomResult.ORDER_NOT_VALID: return s + "warning_red.png";
          case C200318_CustomResult.ORDERTYPE_NOT_VALID: return s + "warning_red.png";
          case C200318_CustomResult.ORDERSTATE_NOT_VALID: return s + "warning_red.png";
          case C200318_CustomResult.ORDER_CODE_NOT_VALID: return s + "warning_red.png";
          case C200318_CustomResult.ORDER_ALREADY_PRESENT: return s + "warning_red.png";
          case C200318_CustomResult.ORDER_ERROR_INSERT: return s + "warning_red.png";
          case C200318_CustomResult.ORDER_QTY_NOT_VALID: return s + "warning_red.png";
          case C200318_CustomResult.ORDERCMP_ERROR_INSERT: return s + "warning_red.png";
          case C200318_CustomResult.ORDER_NOT_EDITABLE: return s + "warning_red.png";
          case C200318_CustomResult.ORDER_DESTINATION_NOT_VALID: return s + "warning_red.png";
          case C200318_CustomResult.ORDER_ERROR_DESTINATION_UPDATE: return s + "warning_red.png";
          case C200318_CustomResult.ORDER_COMPONENT_ERROR_UPDATE: return s + "warning_red.png";
          case C200318_CustomResult.PRINTER_ERROR_UPDATE: return s + "warning_red.png";
          case C200318_CustomResult.PRINTER_ERROR_INSERT: return s + "warning_red.png";
          case C200318_CustomResult.PRINTER_ERROR_DELETE: return s + "warning_red.png";
          case C200318_CustomResult.PRINTER_NOT_VALID: return s + "warning_red.png";
          case C200318_CustomResult.PRINTER_ERROR: return s + "warning_red.png";
          case C200318_CustomResult.LABEL_ERROR_UPDATE: return s + "warning_red.png";
          case C200318_CustomResult.LABEL_ERROR_INSERT: return s + "warning_red.png";
          case C200318_CustomResult.LABEL_ERROR_DELETE: return s + "warning_red.png";
          case C200318_CustomResult.LABEL_NOT_VALID: return s + "warning_red.png";
          case C200318_CustomResult.LABEL_FIELD_ERROR_UPDATE: return s + "warning_red.png";
          case C200318_CustomResult.LABEL_FIELD_ERROR_INSERT: return s + "warning_red.png";
          case C200318_CustomResult.LABEL_FIELD_ERROR_DELETE: return s + "warning_red.png";
          case C200318_CustomResult.LABEL_FIELD_NOT_VALID: return s + "warning_red.png";
          case C200318_CustomResult.COMMUNICATION_TIMEOUT: return s + "warning_red.png";
          case C200318_CustomResult.COMMUNICATION_RESET_RUNNING: return s + "warning_red.png";
          case C200318_CustomResult.ROUTER_IS_SWITCHING: return s + "warning_red.png";
          case C200318_CustomResult.ROUTER_SWITCH_NOT_ALLOWED: return s + "warning_red.png";
          case C200318_CustomResult.CHECKIN_PALLET_NOT_PRESENT: return s + "warning_red.png";
          case C200318_CustomResult.BCRCHECK_PALLET_IN_WH: return s + "warning_red.png";
          case C200318_CustomResult.BCRCHECK_GENERIC_ERROR: return s + "warning_red.png";
          case C200318_CustomResult.BCRCHECK_ERROR_READING_DATA_FROM_PLC: return s + "warning_red.png";
          case C200318_CustomResult.BCRCHECK_BAR_CODE_NOT_READ: return s + "warning_red.png";
          case C200318_CustomResult.BCRCHECK_BAR_CODE_NOT_VALID: return s + "warning_red.png";
          case C200318_CustomResult.BCRCHECK_BAR_CODE_NOT_MATCH_PALLET_ID: return s + "warning_red.png";
          case C200318_CustomResult.CELL_MANUAL_SAT_ERROR_INSERT: return s + "warning_red.png";
          case C200318_CustomResult.CELL_MANUAL_SAT_ERROR_INSERT_ALREADY_PRESENT: return s + "warning_red.png";
          case C200318_CustomResult.CELL_MANUAL_SAT_ERROR_INSERT_NOT_FOUND: return s + "warning_red.png";
          case C200318_CustomResult.CELL_MANUAL_SAT_ERROR_DELETE: return s + "warning_red.png";
          case C200318_CustomResult.CELL_MANUAL_SAT_ERROR_DELETE_NOT_PRESENT: return s + "warning_red.png";
          case C200318_CustomResult.CELL_MANUAL_SAT_ERROR_DELETE_NOT_FOUND: return s + "warning_red.png";
          case C200318_CustomResult.CELL_MANUAL_SAT_NOT_VALID: return s + "warning_red.png";
          case C200318_CustomResult.WORKMODALITY_SWITCH_MASTER_NOT_MANUAL: return s + "warning_red.png";
          case C200318_CustomResult.WORKMODALITY_SWITCH_MODE_EQUAL_CURRENT: return s + "warning_red.png";
          case C200318_CustomResult.WORKMODALITY_SWITCH_MODE_ALREADY_REQUESTED: return s + "warning_red.png";
          case C200318_CustomResult.WORKMODALITY_SWITCH_MODE_SWITCHING: return s + "warning_red.png";
          case C200318_CustomResult.WORKMODALITY_SWITCH_MODE_DONE: return s + "warning_red.png";
        }
      }
      return "";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
