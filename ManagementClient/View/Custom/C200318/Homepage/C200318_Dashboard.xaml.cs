﻿
using System;

namespace View.Custom.C200318.Homepage
{
  /// <summary>
  /// Interaction logic for C200318_Dashboard.xaml
  /// </summary>
  public partial class C200318_Dashboard : CtrlBaseC200318
  {
    public C200318_Dashboard()
    {
      InitializeComponent();

      prova.LabelFormatter = Formatter;
    }

    public Func<double, string> Formatter = value => (value + "%").ToString();
  }
}
