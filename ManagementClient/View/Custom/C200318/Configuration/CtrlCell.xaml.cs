﻿using Model.Custom.C200318.OrderManager;
using Model;
using System.Windows;
using System.Windows.Media;

namespace View.Custom.C200318.Configuration
{
  public partial class CtrlCell : CtrlBaseC200318
  {
    public static readonly DependencyProperty CellProperty =
        DependencyProperty.Register("Cell", typeof(C200318_Cell), typeof(CtrlCell), new UIPropertyMetadata(null, CellPropertyChanged));

    public delegate void OnClickHandler(C200318_Cell cell);

    public event OnClickHandler OnClick;

    public CtrlCell()
    {
      InitializeComponent();
    }

    public C200318_Cell Cell
    {
      get { return (C200318_Cell)GetValue(CellProperty); }
      set { SetValue(CellProperty, value); }
    }

    private static void CellPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var ctrlCell = (CtrlCell)d;
      var newValue = (C200318_Cell)e.NewValue;

      ctrlCell.controlButton.Background = newValue.PalletCount > 0 ? Brushes.Green : Brushes.WhiteSmoke;
      ctrlCell.controlButton.Foreground = newValue.PalletCount > 0 ? Brushes.WhiteSmoke : Brushes.Black;

      if (ctrlCell.Cell.IsDisabled)
      {
        ctrlCell.viewBoxEnabled.Visibility = Visibility.Visible;
        ctrlCell.grdButton.Visibility = Visibility.Hidden;
      }
      else
      {
        ctrlCell.viewBoxEnabled.Visibility = Visibility.Hidden;
        ctrlCell.grdButton.Visibility = Visibility.Visible;
      }
    }

    private void Grid_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
    {
      viewBoxEnabled.Visibility = Visibility.Hidden;
      grdButton.Visibility = Visibility.Visible;
    }

    private void Grid_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
    {
      if (Cell.IsDisabled)
      {
        viewBoxEnabled.Visibility = Visibility.Visible;
        grdButton.Visibility = Visibility.Hidden;
      }
      else
      {
        viewBoxEnabled.Visibility = Visibility.Hidden;
        grdButton.Visibility = Visibility.Visible;
      }
    }

    private void ControlCell_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    #region LOCALIZATION

    protected override void Translate()
    {
      txtArticleCode.Text = Context.Instance.TranslateDefault((string)txtArticleCode.Tag) + ": ";
      txtCellCode.Text = Context.Instance.TranslateDefault((string)txtCellCode.Tag) + ": ";

      txtArticleCodeToolTip.Text = Context.Instance.TranslateDefault((string)txtArticleCodeToolTip.Tag) + ": ";
      txtArticleDescr.Text = Context.Instance.TranslateDefault((string)txtArticleDescr.Tag) + ": ";
      txtCodeToolTip.Text = Context.Instance.TranslateDefault((string)txtCodeToolTip.Tag) + ": ";
      txtIsAvailableForManualSatFetch.Text = Context.Instance.TranslateDefault((string)txtIsAvailableForManualSatFetch.Tag) + ": ";
      txtIsDisabled.Text = Context.Instance.TranslateDefault((string)txtIsDisabled.Tag) + ": ";
      txtIsLocked.Text = Context.Instance.TranslateDefault((string)txtIsLocked.Tag) + ": ";
      txtLotCode.Text = Context.Instance.TranslateDefault((string)txtLotCode.Tag) + ": ";
      txtPalletCount.Text = Context.Instance.TranslateDefault((string)txtPalletCount.Tag) + ": ";
      txtX.Text = Context.Instance.TranslateDefault((string)txtX.Tag) + ": ";
      txtY.Text = Context.Instance.TranslateDefault((string)txtY.Tag) + ": ";
      txtZ.Text = Context.Instance.TranslateDefault((string)txtZ.Tag) + ": ";
    }

    #endregion LOCALIZATION

    private void controlButton_Click(object sender, RoutedEventArgs e)
    {
      OnClick?.Invoke(Cell);
    }
  }
}
