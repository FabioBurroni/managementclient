﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Utilities.Extensions;
using Model;
using Model.Common.Configuration;
using Model.Custom.C200318;
using View.Common.Configuration;
using MaterialDesignThemes.Wpf;
using MaterialDesignExtensions.Controls;

namespace View.Custom.C200318.Configuration
{
  /// <summary>
  /// Interaction logic for CtrlConfigurations.xaml
  /// </summary>
  public partial class CtrlConfigurations : CtrlBaseC200318
  {

    #region Costruttore
    public CtrlConfigurations()
    {
      InitializeComponent();


      Init();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitleConfPar.Text = Context.Instance.TranslateDefault((string)txtBlkTitleConfPar.Tag);
      txtBlkTitlePallRed.Text = Context.Instance.TranslateDefault((string)txtBlkTitlePallRed.Tag);

      txtBlkHeaderMaintenance.Text = Context.Instance.TranslateDefault((string)txtBlkHeaderMaintenance.Tag);
      txtBlkPalletCode.Text = Context.Instance.TranslateDefault((string)txtBlkPalletCode.Tag);
      txtBlkDestination.Text = Context.Instance.TranslateDefault((string)txtBlkDestination.Tag);
      butRededtinateConfirm.Content = Context.Instance.TranslateDefault((string)butRededtinateConfirm.Tag);

      txtCellConfiguration.Text = Context.Instance.TranslateDefault((string)txtCellConfiguration.Tag);

      //txtBlkHeaderConfig.Text = Context.Instance.TranslateDefault((string)txtBlkHeaderConfig.Tag);


      //CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region FIELDS
    List<ConfParameterClient> _paramList = new List<ConfParameterClient>(); 
    #endregion

    private void Init()
    {

      #region P3_ShapeCtrlGoOnIfNoAreaAvailable
      ConfParameterClient p = new ConfParameterBool()
      {
        Title = "SHAPE CONTROL",
        Name = "P3_ShapeCtrlGoOnIfNoAreaAvailable",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "P3 " + "SHAPECONTROL GO ON IF NO AREA IS AVAILABLE",
        Value = false,
        DefaultValue = "True"
      };
      _paramList.Add(p);
      #endregion

      #region SelectWhCell_LogDb
      p = new ConfParameterBool()
      {
        Title = "SELECT WAREHOUSE CELL LOG DB",
        Name = "SelectWhCell_LogDb",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "LOG IN DATABASE DATA ABOUT SELECT WH CELL",
        Value = true,
        DefaultValue = "true"
      };
      _paramList.Add(p);
      #endregion

      #region SelectWhCell_LogFile
      p = new ConfParameterBool()
      {
        Title = "SELECT WAREHOUSE CELL LOG FILE",
        Name = "SelectWhCell_LogFile",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "LOG IN FILE DETAILS ABOUT SELECT WH CELL",
        Value = true,
        DefaultValue = "true"
      };
      _paramList.Add(p);
      #endregion

      #region LogOrderManager
      p = new ConfParameterBool()
      {
        Title = "LOG ORDER MANAGER",
        Name = "LogOrderManager",
        Icon = PackIconKind.ViewList.ToString(),
        Descr = "LOG ORDER MANAGER",
        Value = false,
        DefaultValue = "False"
      };
      _paramList.Add(p);
      #endregion

      #region Master_MaxWorkLoad
      p = new ConfParameterInt()
      {
        Title = "MASTER MAX WORK LOAD",
        Name = "Master_MaxWorkLoad",
        Icon = PackIconKind.Truck.ToString(),

        Descr = "MASTER MAX WORK LOAD",
        Value = 5,
        DefaultValue = "5"
      };
      _paramList.Add(p);
      #endregion

      //...26/04/2022
      #region AutomaticPrinter_GoOnIfNok
      p = new ConfParameterBool()
      {
        Title = "AUTOMATIC PRINTER",
        Name = "AutomaticPrinter_GoOnIfNok",
        Icon = PackIconKind.ArrowRightBoldBox.ToString(),
        Descr = "Pallet go on if printer result is not OK",
        Value = false,
        DefaultValue = "False"
      };
      _paramList.Add(p);
      #endregion

      //...02/05/2022
      #region PlantMonitorAutoUpdate
      p = new ConfParameterBool()
      {
        Title = "PLANT MONITOR",
        Name = "PlantMonitorAutoUpdate",
        Icon = PackIconKind.Monitor.ToString(),
        Descr = "Update plant monitors on timer",
        Value = false,
        DefaultValue = "False"
      };
      _paramList.Add(p);
      #endregion

      //...ORDER MANAGER
      #region OrderManager_MaxPalletsForJob
      p = new ConfParameterInt()
      {
        Title = "ORDER MANAGER MAX PALLETS FOR JOB",
        Name = "OrderManager_MaxPalletsForJob",
        Icon = PackIconKind.Package.ToString(),

        Descr = "MAX PALLETS FOR JOB",
        Value = 1,
        DefaultValue = "2"
      };
      _paramList.Add(p);
      #endregion

      #region OrderManager_MaxPalletsForManualFetch
      p = new ConfParameterInt()
      {
        Title = "ORDER MANAGER MAX PALLETS FOR MANUAL SAT FETCH",
        Name = "OrderManager_MaxPalletsForManualFetch",
        Icon = PackIconKind.Package.ToString(),

        Descr = "MAX PALLETS FOR MANUAL SAT FETCH",
        Value = 1,
        DefaultValue = "2"
      };
      _paramList.Add(p);
      #endregion

      #region OrderManager_DestinationAssignement
      p = new ConfParameterBool()
      {
        Title = "ORDER AUTOMATIC DESTINATION ASSIGNEMENT",
        Name = "OrderManager_DestinationAssignement",
        Icon = PackIconKind.Truck.ToString(),

        Descr = "AUTOMATIC DESTINATION ASSIGNEMENT FOR WAITING ORDERS.",
        Value = true,
        DefaultValue = true
      };
      _paramList.Add(p);
      #endregion

      #region MASTERMANAGER LONGCHARGE REFRESH SECONDS
      p = new ConfParameterInt()
      {
        Title = "MASTERMANAGER LONGCHARGE REFRESH SECONDS",
        Name = "MasterManager_LongChargeRefreshSeconds",
        Icon = PackIconKind.Battery.ToString(),

        Descr = "REFRESH SECONDS",
        Value = 20,
        DefaultValue = 20
      };
      _paramList.Add(p);
      #endregion

      #region MASTER MANAGER REQUEST OPENING GATE REFRESH SECONDS
      p = new ConfParameterInt()
      {
        Title = "MASTER MANAGER REQUEST OPENING GATE REFRESH SECONDS",
        Name = "MasterManager_RequestOpeningGateRefreshSeconds",
        Icon = PackIconKind.DoorOpen.ToString(),

        Descr = "REFRESH SECONDS",
        Value = 20,
        DefaultValue = 20
      };
      _paramList.Add(p);
      #endregion

      #region TestMode
      p = new ConfParameterBool()
      {
        Title = "TEST MODALITY",
        Name = "TestMode",
        Icon = PackIconKind.TestTube.ToString(),

        Descr = "WORK MODALITY",
        Value = false,
        DefaultValue = "false"
      };
      _paramList.Add(p);
      #endregion


      //20210827 fabio
      //ChargerManager_InCharge_RefreshTimeS  60
      //ChargerManager_ToCharge_RefreshTimeS  20
      //ChargerManager_InCharge_WaitTimeS 60
      //ChargerManager_InCharge_AcquisitionTimeM  5
      //ChargerManager_MinTimeBeforeAbortS  300
      //ChargerManager_Battery_LogStatus True
      //ChargerManager_InCharge_Log True
      //ChargerManager_ToCharge_Log True
      //ChargerManager_InPark_RefreshTimeS  60
      //ChargerManager_Parking_MaxInactivityTimeS 30
      //ChargerManager_InPark_AcquisitionTimeM  5
      //ChargerManager_InPark_Log True

      #region ChargerManager_InPark_RefreshTimeS
      p = new ConfParameterInt()
      {
        Title = "ChargerManager_InPark_RefreshTimeS",
        Name = "ChargerManager_InPark_RefreshTimeS",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "REFRESH SECONDS OF MONITORING SATELLITES IN PARK",
        Value = 60,
        DefaultValue = 60
      };
      _paramList.Add(p);
      #endregion

      #region ChargerManager_InPark_AcquisitionTimeM
      p = new ConfParameterInt()
      {
        Title = "ChargerManager_InPark_AcquisitionTimeM",
        Name = "ChargerManager_InPark_AcquisitionTimeM",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "WHEN SATELLITE IS PARKED BATTERY STATUS IS LOGGED IN DATABASE EVERY X MINUTES",
        Value = 5,
        DefaultValue = 5
      };
      _paramList.Add(p);
      #endregion

      #region ChargerManager_InPark_Log
      p = new ConfParameterBool()
      {
        Title = "ChargerManager_InPark_Log",
        Name = "ChargerManager_InPark_Log",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "IF TRUE IN PARK MONITORING PROCEDURE IS LOGGED IN FILE",
        Value = true,
        DefaultValue = true
      };
      _paramList.Add(p);
      #endregion

      #region ChargerManager_InCharge_RefreshTimeS
      p = new ConfParameterInt()
      {
        Title = "ChargerManager_InCharge_RefreshTimeS",
        Name = "ChargerManager_InCharge_RefreshTimeS",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "REFRESH SECONDS OF MONITORING SATELLITES IN CHARGE",
        Value = 60,
        DefaultValue = 60
      };
      _paramList.Add(p);
      #endregion

      #region ChargerManager_InCharge_WaitTimeS
      p = new ConfParameterInt()
      {
        Title = "ChargerManager_InCharge_WaitTimeS",
        Name = "ChargerManager_InCharge_WaitTimeS",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "WAIT TIME BEFORE READ BATTERY STATUS FIRST TIME",
        Value = 60,
        DefaultValue = 60
      };
      _paramList.Add(p);
      #endregion

      #region ChargerManager_InCharge_AcquisitionTimeM
      p = new ConfParameterInt()
      {
        Title = "ChargerManager_InCharge_AcquisitionTimeM",
        Name = "ChargerManager_InCharge_AcquisitionTimeM",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "WHEN SATELLITE IS IN CHARGING BATTERY STATUS IS LOGGED IN DATABASE EVERY X MINUTES",
        Value = 5,
        DefaultValue = 5
      };
      _paramList.Add(p);
      #endregion

      #region ChargerManager_InCharge_Log
      p = new ConfParameterBool()
      {
        Title = "ChargerManager_InCharge_Log",
        Name = "ChargerManager_InCharge_Log",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "IF TRUE IN CHARGE MONITORING PROCEDURE IS LOGGED IN FILE",
        Value = true,
        DefaultValue = true
      };
      _paramList.Add(p);
      #endregion

      #region ChargerManager_ToCharge_RefreshTimeS
      p = new ConfParameterInt()
      {
        Title = "ChargerManager_ToCharge_RefreshTimeS",
        Name = "ChargerManager_ToCharge_RefreshTimeS",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "REFRESH SECONDS OF MONITORING SATELLITES TO CHARGE",
        Value = 20,
        DefaultValue = 20
      };
      _paramList.Add(p);
      #endregion

      #region ChargerManager_MinTimeBeforeAbortS
      p = new ConfParameterInt()
      {
        Title = "ChargerManager_MinTimeBeforeAbortS",
        Name = "ChargerManager_MinTimeBeforeAbortS",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "MIN TIME IN SECONDS TO BE ELAPSED BEFORE ABORT A CHARGE PROCEDURE",
        Value = 300,
        DefaultValue = 300
      };
      _paramList.Add(p);
      #endregion

      #region ChargerManager_Battery_LogStatus
      p = new ConfParameterBool()
      {
        Title = "CHARGE MANAGER LOG BATTERY STATUS",
        Name = "ChargerManager_Battery_LogStatus",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "IF TRUE THE STATUS OF BATTERY IS LOGGED IN FILE WHEN CHARGER IS ON",
        Value = true,
        DefaultValue = true
      };
      _paramList.Add(p);
      #endregion

      #region ChargerManager_ToCharge_Log
      p = new ConfParameterBool()
      {
        Title = "ChargerManager_ToCharge_Log",
        Name = "ChargerManager_ToCharge_Log",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "IF TRUE TO CHARGE MONITORING PROCEDURE IS LOGGED IN FILE",
        Value = true,
        DefaultValue = true
      };
      _paramList.Add(p);
      #endregion

      #region ChargerManager_Parking_MaxInactivityTimeS
      p = new ConfParameterInt()
      {
        Title = "ChargerManager_Parking_MaxInactivityTimeS",
        Name = "ChargerManager_Parking_MaxInactivityTimeS",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "MAX INACTIVITY TIME BEFORE TO PARK THE SATELLITES",
        Value = 60,
        DefaultValue = 60
      };
      _paramList.Add(p);
      #endregion

      //20230519
      #region ChargerManager_MaxWaitTimeBeforeChargeOnMasterM
      p = new ConfParameterInt()
      {
        Title = "ChargerManager Max Wait Time Before Charge On Master M",
        Name = "ChargerManager_MaxWaitTimeBeforeChargeOnMasterM",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "MAX WAIT TIME IN MINUTE BEFORE TO CHARGE SATELLITE ON MASTER",
        Value = 10,
        DefaultValue = 10
      };
      _paramList.Add(p);
      #endregion

      //20230519
      #region ChargerManager_Version
      p = new ConfParameterInt()
      {
        Title = "Charger Manager Version",
        Name = "ChargerManager_Version",
        Icon = PackIconKind.Battery.ToString(),
        Descr = "CHARGE MANAGER ALGORITHM VERSION 1 OR 2",
        Value = 1,
        DefaultValue = 1
      };
      _paramList.Add(p);
      #endregion

      #region WEB CAM
      p = new ConfParameterBool()
      {
        Title = "LOG WEB CAM PLUGIN",
        Name = "WebCam_Log",
        Icon = PackIconKind.WebCamera.ToString(),
        Descr = "LOG WEB CAM PLUGIN",
        Value = false,
        DefaultValue = "True"
      };
      _paramList.Add(p);

      p = new ConfParameterInt()
      {
        Title = "WEB CAM PLUGIN INACTIVITY TIME",
        Name = "WebCam_InictivityTimeS",
        Icon = PackIconKind.WebCamera.ToString(),

        Descr = "WEB CAM PLUGIN INACTIVITY TIME",
        Value = 20,
        DefaultValue = "20"
      };
      _paramList.Add(p);

      #endregion


      foreach (var parameter in _paramList)
      {
        if (parameter.ParamType == typeof(string))
        {
          CtrlConfStringParam ctrl = new CtrlConfStringParam();
          ctrl.ConfParameter = parameter;
          ctrl.OnCancel += Ctrl_OnCancel;
          ctrl.OnConfirm += Ctrl_OnConfirm;
          wp.Children.Add(ctrl);
        }
        else if (parameter.ParamType == typeof(int))
        {
          CtrlConfIntParam ctrl = new CtrlConfIntParam();
          ctrl.ConfParameter = parameter;
          ctrl.OnCancel += Ctrl_OnCancel;
          ctrl.OnConfirm += Ctrl_OnConfirm;
          wp.Children.Add(ctrl);
        }
        else if (parameter.ParamType == typeof(bool))
        {
          CtrlConfCheckParameter ctrl = new CtrlConfCheckParameter();
          ctrl.ConfParameter = parameter;
          ctrl.OnCancel += Ctrl_OnCancel;
          ctrl.OnConfirm += Ctrl_OnConfirm;
          wp.Children.Add(ctrl);
        }
        else if (parameter.ParamType.IsEnum)
        {
          CtrlConfComboParameter ctrl = new CtrlConfComboParameter();
          ctrl.ConfParameter = parameter;
          ctrl.OnCancel += Ctrl_OnCancel;
          ctrl.OnConfirm += Ctrl_OnConfirm;
          wp.Children.Add(ctrl);
        }
      }

    }


    #region COMANDI E RISPOSTE
    private void Cmd_MM_RedestinatePallet(string palletCode, string positionCode)
    {
      CommandManagerC200318.MM_RedestinatePallet(this, palletCode, positionCode);
    }
    private void MM_RedestinatePallet(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      C200318_CustomResult cr = dwr.Result.ConvertTo<C200318_CustomResult>();
      MessageBox.Show(cr.ToString());


    }


    private void Cmd_CO_Configuration_GetAll()
    {
      CommandManagerC200318.CO_Configuration_GetAll(this);
    }
    private void CO_Configuration_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var listNew = dwr.Data as List<ConfParameterServer>;
      if (listNew != null)
      {
        foreach (var pNew in listNew)
        {
          var confParameter = _paramList.FirstOrDefault(p => p.Name == pNew.Name);
          if (confParameter != null)
          {
            confParameter.Update(pNew);
          }
        }
      }
    }

    private void Cmd_CO_Configuration_Get(string shippingLaneCode)
    {
      CommandManagerC200318.CO_Configuration_Get(this, shippingLaneCode);
    }
    private void CO_Configuration_Get(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var pNew = dwr.Data as ConfParameterServer;
      if (pNew != null)
      {
        var confParameter = _paramList.FirstOrDefault(p => p.Name == pNew.Name);
        if (confParameter != null)
        {
          confParameter.Update(pNew);
        }
      }
    }

    private void Cmd_CO_Configuration_Set(string parameterName, string parameterValue)
    {
      CommandManagerC200318.CO_Configuration_Set(this, parameterName, parameterValue);
    }

    private void CO_Configuration_Set(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      C200318_CustomResult result = dwr.Result.ConvertTo<C200318_CustomResult>();
      if (result == C200318_CustomResult.OK)
      {

      }
      else
      {

      }

      MessageBox.Show(result.ToString());

      Cmd_CO_Configuration_Get(commandMethodParameters[1]);
    }

    #endregion

    #region eventi
    private void Ctrl_OnConfirm(ConfParameterClient confPar)
    {
      Cmd_CO_Configuration_Set(confPar.Name, confPar.SelectedValue);
    }

    private void Ctrl_OnCancel(ConfParameterClient confPar)
    {
      Cmd_CO_Configuration_Get(confPar.Name);

    }

    private void CtrlBaseLoaded(object sender, RoutedEventArgs e)
    {
      Cmd_CO_Configuration_GetAll();
    }


    public override void Closing()
    {

    }
    #endregion

    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      Cmd_CO_Configuration_GetAll();
      Translate();
    }


    #region REDESTINATE PALLET
    private async void butRededtinateConfirm_Click(object sender, RoutedEventArgs e)
    {
      string palletCode = txtRedestinatePalletCode.Text;
      if(string.IsNullOrEmpty(palletCode))
      {
        AlertDialogArguments dialogArgs = new AlertDialogArguments
        {
          Title = "Error on redestination request",
          Message = "Specify Pallet Code",
          OkButtonLabel = "GOT IT"
        };

        await AlertDialog.ShowDialogAsync("RootDialog", dialogArgs);

        return;
      }

      var si = cbRedestinateDestination.SelectedItem;
      if(si==null)
      {
        AlertDialogArguments dialogArgs = new AlertDialogArguments
        {
          Title = "Error on redestination request",
          Message = "Specify Pallet Destination",
          OkButtonLabel = "GOT IT"
        };

        await AlertDialog.ShowDialogAsync("RootDialog", dialogArgs);

        return;
      }

      var cbi = si as System.Windows.Controls.ComboBoxItem;
      string positionCode = cbi.Content.ToString();

      if(string.IsNullOrEmpty(positionCode))
      {
        AlertDialogArguments dialogArgs = new AlertDialogArguments
        {
          Title = "Error on redestination request",
          Message = "Specify Pallet Destination",
          OkButtonLabel = "GOT IT"
        };

        await AlertDialog.ShowDialogAsync("RootDialog", dialogArgs);

        return;
      }

      Cmd_MM_RedestinatePallet(palletCode, positionCode);

      


    }
    #endregion

    private void tiConfParammeters_Loaded(object sender, RoutedEventArgs e)
    {
      Cmd_CO_Configuration_GetAll();
    }
  }
}
