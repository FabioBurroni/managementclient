﻿using Model;
using Model.Custom.C200318;
using Model.Custom.C200318.OrderManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Utilities.Extensions;

namespace View.Custom.C200318.Configuration
{
  public partial class CtrlCellConfiguration : CtrlBaseC200318
  {
    private List<C200318_Cell> _cells = new List<C200318_Cell>();
    private C200318_Cell _cell = new C200318_Cell();
    private int _lastMax = 0;
    private int _lastMin = 0;

    public List<C200318_Cell> Cells
    {
      get { return _cells; }
      set
      {
        _cells = value;
        NotifyPropertyChanged("Cells");
      }
    }

    public C200318_Cell Cell
    {
      get { return _cell; }
      set
      {
        _cell = value;
        NotifyPropertyChanged("Cell");
      }
    }

    public CtrlCellConfiguration()
    {
      InitializeComponent();
    }

    #region LOCALIZATION

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtSide1.Text = Context.Instance.TranslateDefault((string)txtSide1.Tag) + " 1";
      txtSide2.Text = Context.Instance.TranslateDefault((string)txtSide2.Tag) + " 2";

      txtArticleCode.Text = Context.Instance.TranslateDefault((string)txtArticleCode.Tag) + ": ";
      txtArticleDescr.Text = Context.Instance.TranslateDefault((string)txtArticleDescr.Tag) + ": ";
      txtCode.Text = Context.Instance.TranslateDefault((string)txtCode.Tag) + ": ";
      txtIsAvailableForManualSatFetch.Text = Context.Instance.TranslateDefault((string)txtIsAvailableForManualSatFetch.Tag) + ": ";
      txtIsDisabled.Text = Context.Instance.TranslateDefault((string)txtIsDisabled.Tag) + ": ";
      txtIsLocked.Text = Context.Instance.TranslateDefault((string)txtIsLocked.Tag) + ": ";
      txtLotCode.Text = Context.Instance.TranslateDefault((string)txtLotCode.Tag) + ": ";
      txtPalletCount.Text = Context.Instance.TranslateDefault((string)txtPalletCount.Tag) + ": ";
      txtX.Text = Context.Instance.TranslateDefault((string)txtX.Tag) + ": ";
      txtY.Text = Context.Instance.TranslateDefault((string)txtY.Tag) + ": ";
      txtZ.Text = Context.Instance.TranslateDefault((string)txtZ.Tag) + ": ";

      cmdDisabe.Content = Context.Instance.TranslateDefault((string)cmdDisabe.Tag);
      cmdEnabe.Content = Context.Instance.TranslateDefault((string)cmdEnabe.Tag);
    }

    #endregion LOCALIZATION

    void CtrlCell_OnClick(C200318_Cell cell)
    {
      Cell = cell;
    }

    private void GenerateTable(List<C200318_Cell> _cells, int element)
    {
      if (_cells is null ||
        !_cells.Any())
      {
        return;
      }

      grdCell.Children.Clear();
      grdCell.ColumnDefinitions.Clear();
      grdCell.RowDefinitions.Clear();

      var minX = _cells.Min(s => s.X);
      var maxX = _cells.Max(s => s.X);
      var minY = _cells.Min(s => s.Y);
      var maxY = _cells.Max(s => s.Y);

      var countX = 0;
      var countY = 0;
      var count = Cells.Max(s => s.Y);

      while (countY != Cells.Max(s => s.Y) * 2 + 2)
      {
        grdCell.RowDefinitions.Add(new RowDefinition());
        countY++;
      }

      while (countX != element * 2 + 2)
      {
        grdCell.ColumnDefinitions.Add(new ColumnDefinition());
        countX++;
      }

      var rowCount = grdCell.RowDefinitions.Count();
      var columnCount = grdCell.ColumnDefinitions.Count();

      for (countY = 0; countY <= rowCount; countY++)
      {
        if (countY % 2 == 0)
        {
          GridSplitter gridSplitter = new GridSplitter();
          gridSplitter.IsEnabled = false;
          gridSplitter.Height = 2.0;
          gridSplitter.Width = Double.NaN;
          gridSplitter.Background = Brushes.Black;
          gridSplitter.HorizontalAlignment = HorizontalAlignment.Stretch;
          gridSplitter.VerticalAlignment = VerticalAlignment.Bottom;
          gridSplitter.SetValue(Grid.RowProperty, countY);
          gridSplitter.SetValue(Grid.ColumnSpanProperty, columnCount);
          grdCell.Children.Add(gridSplitter);
        }
        else if (count >= minY)
        {
          Label text = new Label();
          text.Content = count;
          text.SetValue(Grid.ColumnProperty, 1);
          text.HorizontalAlignment = HorizontalAlignment.Center;
          text.VerticalAlignment = VerticalAlignment.Center;
          text.SetValue(Grid.RowProperty, countY);
          grdCell.Children.Add(text);
          count--;
        }
      }

      count = minX;

      for (countX = 0; countX <= columnCount; countX++)
      {
        if (countX % 2 == 0)
        {
            GridSplitter gridSplitter = new GridSplitter();
            gridSplitter.IsEnabled = false;
            gridSplitter.Width = 2.0;
            gridSplitter.Height = Double.NaN;
            gridSplitter.Background = Brushes.Black;
            gridSplitter.HorizontalAlignment = HorizontalAlignment.Right;
            gridSplitter.VerticalAlignment = VerticalAlignment.Stretch;
            gridSplitter.SetValue(Grid.ColumnProperty, countX);
            gridSplitter.SetValue(Grid.RowSpanProperty, rowCount);
            grdCell.Children.Add(gridSplitter);
        }
        else if (count <= maxX && countX != 1)
        {
          Label text = new Label();
          text.Content = count;
          text.HorizontalAlignment = HorizontalAlignment.Center;
          text.VerticalAlignment = VerticalAlignment.Center;
          text.SetValue(Grid.ColumnProperty, countX);
          text.SetValue(Grid.RowProperty, rowCount);
          grdCell.Children.Add(text);
          count++;
        }
      }

      foreach (var cell in _cells)
      {
        CtrlCell ctrlCell = new CtrlCell();
        ctrlCell.Cell = cell;
        ctrlCell.OnClick += CtrlCell_OnClick;
        ctrlCell.Height = Double.NaN;
        ctrlCell.Width = Double.NaN;

        var x = ((cell.X - minX) * 2) + 3;
        var y = rowCount - cell.Y * 2 - 1;

        ctrlCell.SetValue(Grid.RowProperty, y);
        ctrlCell.SetValue(Grid.ColumnProperty, x);

        grdCell.Children.Add(ctrlCell);
      }
    }

    private void butRefresh_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      Cell = null;
      Cells = null;

      Cmd_OM_GetAllCellForManualSatFetch();
    }

    private void CtrlBaseC200318_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
      Translate();

      ChangeSide.IsChecked = true;

      Cell = null;
      Cells = null;
    }

    private void Cmd_OM_GetAllCellForManualSatFetch()
    {
      CommandManagerC200318.OM_GetAllCellForManualSatFetch(this);
    }

    private void OM_GetAllCellForManualSatFetch(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var cells = dwr.Data as List<C200318_Cell>;
      if (cells != null)
      {
        var side = ChangeSide.IsChecked.Value ? 2 : 1;
        var filter = cells.Where(s => s.Z == side).ToList();

        Cells = filter.OrderByDescending(s => s.Y).ThenBy(s => s.X).ToList();
      }

      if (_cells is null ||
        !_cells.Any())
      {
        return;
      }

      var minX = _cells.Min(s => s.X);
      var maxX = _cells.Max(s => s.X);


      if (_lastMin == 0 && _lastMax == 0)
      {
        _lastMax = minX + 4;
        _lastMin = minX;
        GenerateTable(Cells.Where(s => s.X >= minX && s.X <= minX + 4).ToList(), _lastMax);
        btnPlus.IsEnabled = true;
        btnMinus.IsEnabled = false;
      }
      else
      {
        GenerateTable(Cells.Where(s => s.X >= _lastMin && s.X <= _lastMax).ToList(), _lastMax - _lastMin + 1);

        if (_lastMax == maxX)
          btnPlus.IsEnabled = false;
        else
          btnPlus.IsEnabled = true;

        if (_lastMin == minX)
          btnMinus.IsEnabled = false;
        else
          btnMinus.IsEnabled = true;
      }
    }

    private void ChangeSide_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      Cell = null;
      Cells = null;

      _lastMin = _lastMax = 0;

      Cmd_OM_GetAllCellForManualSatFetch();
    }

    private void cmdDisabe_Click(object sender, RoutedEventArgs e)
    {
      Cmd_OM_RemoveCellForManualSatFetch();
    }

    private void cmdEnabe_Click(object sender, RoutedEventArgs e)
    {
      Cmd_OM_AddCellForManualSatFetch();
    }

    private void Cmd_OM_RemoveCellForManualSatFetch()
    {
      CommandManagerC200318.OM_RemoveCellForManualSatFetch(this, Cell.Code);
    }

    private void OM_RemoveCellForManualSatFetch(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      if(dwr.Data != null)
      {
        C200318_CustomResult data = dwr.Data.ConvertTo<C200318_CustomResult>();

        switch (data)
        {
          case C200318_CustomResult.CELL_MANUAL_SAT_ERROR_DELETE:
          case C200318_CustomResult.CELL_MANUAL_SAT_ERROR_DELETE_NOT_FOUND:
          case C200318_CustomResult.CELL_MANUAL_SAT_ERROR_DELETE_NOT_PRESENT:
            ExtendedUtilities.SnackbarTools.MainSnackbar.ShowMessageFail(data.ToString(), 5);
            break;

            case C200318_CustomResult.OK:
            ExtendedUtilities.SnackbarTools.MainSnackbar.ShowMessageOk("Prelievo manuale disabilitato per cella " + Cell.Code, 5);

            var children = grdCell.Children as UIElementCollection;
            var updatedChildren = children.OfType<CtrlCell>().SingleOrDefault(s => s.Cell == Cell).Cell.IsAvailableForManualSatFetch = false;
            Cell.IsAvailableForManualSatFetch = false;
            break;
        }
      }
    }

    private void Cmd_OM_AddCellForManualSatFetch()
    {
      CommandManagerC200318.OM_AddCellForManualSatFetch(this, Cell.Code);
    }

    private void OM_AddCellForManualSatFetch(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      if (dwr.Data != null)
      {
        C200318_CustomResult data = dwr.Data.ConvertTo<C200318_CustomResult>();

        switch (data)
        {
          case C200318_CustomResult.CELL_MANUAL_SAT_ERROR_INSERT:
          case C200318_CustomResult.CELL_MANUAL_SAT_ERROR_INSERT_ALREADY_PRESENT:
          case C200318_CustomResult.CELL_MANUAL_SAT_ERROR_INSERT_NOT_FOUND:
            ExtendedUtilities.SnackbarTools.MainSnackbar.ShowMessageFail(data.ToString(), 5);
            break;
          case C200318_CustomResult.OK:
            ExtendedUtilities.SnackbarTools.MainSnackbar.ShowMessageOk("Prelievo manuale abilitato per cella " + Cell.Code, 5);

            var children = grdCell.Children as UIElementCollection;
            var updatedChildren = children.OfType<CtrlCell>().SingleOrDefault(s => s.Cell == Cell).Cell.IsAvailableForManualSatFetch = true;
            Cell.IsAvailableForManualSatFetch = true;
            break;
        }
      }
    }

    private void btnPlus_Click(object sender, RoutedEventArgs e)
    {
      btnMinus.IsEnabled = true;

      Cell = null;
      var maxX = _cells.Max(s => s.X);

      if(_lastMax + 5 > maxX)
      {
        var elemnts = Cells.Where(s => s.X > maxX - 5 && s.X <= maxX).ToList();
        _lastMax = maxX;
        _lastMin = maxX - 4;
        GenerateTable(elemnts, 5);
      }
      else
      {
        if (maxX - (_lastMax + 5)  == 1)
        {
          var elemnts = Cells.Where(s => s.X > _lastMax && s.X <= _lastMax + 6).ToList();
          _lastMin = _lastMax + 1;
          _lastMax += 6;
          GenerateTable(elemnts, 6);
        }
        else
        {
          var elemnts = Cells.Where(s => s.X > _lastMax && s.X <= _lastMax + 5).ToList();
          _lastMin = _lastMax + 1;
          _lastMax += 5;
          GenerateTable(elemnts, 5);
        }
      }

      if (_lastMax == maxX)
        btnPlus.IsEnabled = false;
      else
        btnPlus.IsEnabled = true;
    }

    private void btnMinus_Click(object sender, RoutedEventArgs e)
    {
      btnPlus.IsEnabled = true;
      Cell = null;
      var minX = _cells.Min(s => s.X);

      if (_lastMin - 5 < minX)
      {
        var elemnts = Cells.Where(s => s.X >= minX && s.X <= minX + 4).ToList();
        _lastMin = minX;
        _lastMax = minX + 4;
        GenerateTable(elemnts, minX + 4);
      }
      else
      {
        if (minX - (_lastMin - 5) == 1)
        {
          var elemnts = Cells.Where(s => s.X >= _lastMin - 6 && s.X < _lastMin).ToList();
          _lastMax = _lastMin - 1;
          _lastMin -= 6;
          GenerateTable(elemnts, 6);
        }
        else
        {
          var elemnts = Cells.Where(s => s.X >= _lastMin - 5 && s.X < _lastMin).ToList();
          _lastMax = _lastMin - 1;
          _lastMin -= 5;
          GenerateTable(elemnts, 5);
        }
      }

      if (_lastMin == minX)
        btnMinus.IsEnabled = false;
      else
        btnMinus.IsEnabled = true;
    }
  }
}