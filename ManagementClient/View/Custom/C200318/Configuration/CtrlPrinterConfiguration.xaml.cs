﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Model;
using Model.Custom.C200318;
using Model.Custom.C200318.Printer;
using Utilities.Extensions;
using View.Common.Languages;
using View.Custom.C200153.Converter;

namespace View.Custom.C200318.Configuration
{
  /// <summary>
  /// Interaction logic for CtrlPalletTracking.xaml
  /// </summary>
  public partial class CtrlPrinterConfiguration : CtrlBaseC200318
  {
    #region COSTRUTTORE

    public CtrlPrinterConfiguration()
    {      
      InitializeComponent();

    }

    #endregion COSTRUTTORE

    #region LOCALIZATION

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkDescriptionHeader.Text = Context.Instance.TranslateDefault((string)txtBlkDescriptionHeader.Tag);
      //...refresh printers button tooltip
     // txtBlkPrinterRefreshButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkPrinterRefreshButtonToolTip.Tag);
      txtBlkPositionT.Text = Context.Instance.TranslateDefault((string)txtBlkPositionT.Tag);
      txtBlkDescriptionT.Text = Context.Instance.TranslateDefault((string)txtBlkDescriptionT.Tag);
      txtBlkIpAddressT.Text = Context.Instance.TranslateDefault((string)txtBlkIpAddressT.Tag);
      txtBlkPortNumberT.Text = Context.Instance.TranslateDefault((string)txtBlkPortNumberT.Tag);
      txtBlkTimeoutT.Text = Context.Instance.TranslateDefault((string)txtBlkTimeoutT.Tag);
      txtBlkDeleteT.Text = Context.Instance.TranslateDefault((string)txtBlkDeleteT.Tag);
      //...Right side
      txtBlkPositionCode.Text = Context.Instance.TranslateDefault((string)txtBlkPositionCode.Tag);
      txtBlkDescription.Text = Context.Instance.TranslateDefault((string)txtBlkDescription.Tag);
      txtBlkIpAddress.Text = Context.Instance.TranslateDefault((string)txtBlkIpAddress.Tag);
      txtBlkPortNumber.Text = Context.Instance.TranslateDefault((string)txtBlkPortNumber.Tag);
      txtBlkTimeout.Text = Context.Instance.TranslateDefault((string)txtBlkTimeout.Tag);
      //...Buttons
      txtBlkNewButton.Text = Context.Instance.TranslateDefault((string)txtBlkNewButton.Tag);
      //...new printer button tooltip
      txtBlkPrinterNewButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkPrinterNewButtonToolTip.Tag);
      txtBlkClear.Text = Context.Instance.TranslateDefault((string)txtBlkClear.Tag);
      //...clear button tooltip
      txtBlkPrinterClearButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkPrinterClearButtonToolTip.Tag);
      txtBlkConfirm.Text = Context.Instance.TranslateDefault((string)txtBlkConfirm.Tag);
      //...confirm button tooltip
      txtBlkPrinterConfirmButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkPrinterConfirmButtonToolTip.Tag);
    }

    #endregion

    #region PUBLIC PROPERTIES
    public ObservableCollectionFast<C200318_Printer> PrinterL { get; set; } = new ObservableCollectionFast<C200318_Printer>();
    #endregion

    #region DP - PrinterSelected
    public C200318_Printer PrinterSelected
    {
      get { return (C200318_Printer)GetValue(PrinterSelectedProperty); }
      set { SetValue(PrinterSelectedProperty, value); }
    }

    // Using a DependencyProperty as the backing store for PrinterSelected.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty PrinterSelectedProperty =
        DependencyProperty.Register("PrinterSelected", typeof(C200318_Printer)
          , typeof(CtrlPrinterConfiguration), new PropertyMetadata(null));
    #endregion

    #region COMANDI E RISPOSTE
    private void Cmd_CO_Printer_GetAll()
    {
      PrinterL.Clear();
      CommandManagerC200318.CO_Printer_GetAll(this);
    }
    private void CO_Printer_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var list = dwr.Data as List<C200318_Printer>;
      if (list != null)
      {
        list.ForEach(p => PrinterL.Add(p));
      }
    }

    private void Cmd_CO_Printer_Get(string positionCode)
    {
      CommandManagerC200318.CO_Printer_Get(this, positionCode);
    }
    private void CO_Printer_Get(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var p = dwr.Data as C200318_Printer;
      if (p != null)
      {
        var oldPr = PrinterL.FirstOrDefault(pr => pr.PositionCode.EqualsIgnoreCase(p.PositionCode));
        if (oldPr != null)
          oldPr.Update(p);
        else
          PrinterL.Add(p);
      }
    }

    private void Cmd_CO_Printer_Delete(string positionCode)
    {
      CommandManagerC200318.CO_Printer_Delete(this, positionCode);
    }
    private void CO_Printer_Delete(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Result == null) return;
      string positionCode = commandMethodParameters[1];
      C200318_CustomResult result = dwr.Result.ConvertTo<C200318_CustomResult>();
      if (result != C200318_CustomResult.OK)
      {

        LocalSnackbar.ShowMessageOk(positionCode.ToString() + "DELETED".TD(), 3);
      }
      else
      {
        LocalSnackbar.ShowMessageFail("RESULT".TD() + ": " + CustomResultToLocalizedStringConverter.Convert(result), 3);
      }
      Cmd_CO_Printer_GetAll();
    }

    private void Cmd_CO_Printer_InsertOrUpdate(C200318_Printer printer)
    {
      CommandManagerC200318.CO_Printer_InsertOrUpdate(this, printer.PositionCode, printer.IpAddress, printer.PortNumber, printer.Description, printer.Timeout);
    }
    private void CO_Printer_InsertOrUpdate(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Result == null) return;
      string positionCode = commandMethodParameters[1];
      C200318_CustomResult result = dwr.Result.ConvertTo<C200318_CustomResult>();
      if (result != C200318_CustomResult.OK)
      {
        LocalSnackbar.ShowMessageOk(positionCode.ToString() + "UPDATED".TD(), 3);

        Cmd_CO_Printer_GetAll();
      }
      else
      {
        Cmd_CO_Printer_Get(commandMethodParameters[1]);
      }
    }

    #endregion

    #region EVENTS
    private void butPrinterRefresh_Click(object sender, RoutedEventArgs e)
    {
      Cmd_CO_Printer_GetAll();
    }

    private void butPrinterNew_Click(object sender, RoutedEventArgs e)
    {
      PrinterSelected = new C200318_Printer();
    }

    private void butPrinterClear_Click(object sender, RoutedEventArgs e)
    {
      PrinterSelected?.Clear();
    }

    private void butPrinterConfirm_Click(object sender, RoutedEventArgs e)
    {
      if (PrinterSelected == null)
      {
          LocalSnackbar.ShowMessageInfo("PLEASE SELECT PRINTER".TD(), 3);
     
        return;
      }
      Cmd_CO_Printer_InsertOrUpdate(PrinterSelected);
    }

    private void butPrinterDelete_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b == null)
        return;
      var p = b.Tag as C200318_Printer;
      if (p != null)
        Cmd_CO_Printer_Delete(p.PositionCode);
    }

    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();

      Cmd_CO_Printer_GetAll();
    }
    #endregion


  }
}