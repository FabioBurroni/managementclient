﻿

namespace View.Custom.C200318.TrafficBoard
{
  /// <summary>
  /// Interaction logic for CtrlReport.xaml
  /// </summary>
  public partial class CtrlTrafficBoard : CtrlBaseC200318
  {



    public CtrlTrafficBoard()
    {
      InitializeComponent();
    }



    /// <summary>
    /// Implemented IsSelected to propagate to the child control the status of IsSelected
    /// </summary>
    public override bool IsSelected
    {
      get
      {
        return base.IsSelected;
      }
      set
      {
        ctrlTrafficTask.IsSelected = value;
        base.IsSelected = value;
      }
    }


    #region COMANDI E RISPOSTE

    #endregion COMANDI E RISPOSTE


  }
}