﻿using System.Windows;
using Model.Custom.C200318;

namespace View.Custom.C200318.TrafficBoard
{
  /// <summary>
  /// Interaction logic for CtrlTaskFilter.xaml
  /// </summary>
  public partial class CtrlTrafficBoardTaskFilter : CtrlBaseC200318
  {
    public C200318_TrafficBoardTaskFilter Filter
    {
      get { return (C200318_TrafficBoardTaskFilter)GetValue(FilterProperty); }
      set { SetValue(FilterProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Filter.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty FilterProperty =
        DependencyProperty.Register("Filter", typeof(C200318_TrafficBoardTaskFilter), typeof(CtrlTrafficBoardTaskFilter), new PropertyMetadata(null));

    public CtrlTrafficBoardTaskFilter()
    {
      InitializeComponent();
    }

    protected override void Translate()
    {
      
    }

    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    
    private void butClear_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
    }

    private void butSend_Click(object sender, RoutedEventArgs e)
    {
      Filter.FireOnSearch();
    }
  }
}