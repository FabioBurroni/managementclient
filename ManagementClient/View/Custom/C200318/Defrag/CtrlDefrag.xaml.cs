﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using View.Common.Languages;

using Model;
using Model.Custom.C200318;
using Model.Custom.C200318.Defrag;

using Utilities.Extensions;

using MaterialDesignExtensions.Controls;
using Authentication;
using System.Collections.ObjectModel;

namespace View.Custom.C200318.Defrag
{
  /// <summary>
  /// Interaction logic for CtrlDefrag.xaml
  /// </summary>
  public partial class CtrlDefrag : CtrlBaseC200318
  {
    private readonly AuthenticationManager _authenticationManager = AuthenticationManager.Instance;
    
    #region CTOR
    public CtrlDefrag()
    {
      //DicDefragStatusEnum_Init();
      InitializeComponent();
      CanShowDefragParameter = _authenticationManager?.Session?.User.Username.EqualsIgnoreCase("cassioli") ?? false;
      Translate();
    }
    #endregion

    #region ENUMERATIONS MANAGEMENT
    /*
    public Dictionary<C200318_DefragStatusEnum, string> DicDefragStatusEnum { get; set; } = new Dictionary<C200318_DefragStatusEnum, string>();

    private void DicDefragStatusEnum_Init()
    {
      var enumValues = Enum.GetValues(typeof(C200318_DefragStatusEnum));
      foreach (var enumVal in enumValues)
      {
        DicDefragStatusEnum.Add((C200318_DefragStatusEnum)enumVal, Context.Instance.TranslateDefault(enumVal.ToString()));
      }
    }
    */

    #endregion

    #region DP - ShowDefragParameter

    public bool ShowDefragParameter
    {
      get { return (bool)GetValue(ShowDefragParameterProperty); }
      set { SetValue(ShowDefragParameterProperty, value); }
    }

    // Using a DependencyProperty as the backing store for ShowDefragParameter.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ShowDefragParameterProperty =
        DependencyProperty.Register("ShowDefragParameter", typeof(bool), typeof(CtrlDefrag)
          , new PropertyMetadata(false));
    #endregion

    #region CanShowDefragParameter
    public bool CanShowDefragParameter
    {
      get { return (bool)GetValue(CanShowDefragParameterProperty); }
      set { SetValue(CanShowDefragParameterProperty, value); }
    }

    // Using a DependencyProperty as the backing store for CanShowDefragParameter.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty CanShowDefragParameterProperty =
        DependencyProperty.Register("CanShowDefragParameter", typeof(bool), typeof(CtrlDefrag), new PropertyMetadata(false)); 
    #endregion

    #region PUBLIC PROPERTY
    public C200318_DefragStartingCondition DefragStartingConditionServer { get; set; } = new C200318_DefragStartingCondition();
    public C200318_DefragStartingCondition DefragStartingConditionLocal{ get; set; } = new C200318_DefragStartingCondition();
    public C200318_DefragStatus DefragStatusServer { get; set; } = new C200318_DefragStatus();
    public C200318_DefragStatus DefragStatusLocal { get; set; } = new C200318_DefragStatus();
    public ObservableCollectionFast<C200318_DefragStep> DefragStepL { get; set; } = new ObservableCollectionFast<C200318_DefragStep>();
    public ObservableCollectionFast<C200318_DefragParameter> DefragParameterL { get; set; } = new ObservableCollectionFast<C200318_DefragParameter>();
    #endregion

    #region TRANSLATION
    protected override void Translate()
    {
      labDefragStatusDateEnd.Text = Context.Instance.TranslateDefault((string)labDefragStatusDateEnd.Tag);
      labDefragStatusDateStart.Text = Context.Instance.TranslateDefault((string)labDefragStatusDateStart.Tag);
      labDefragStep.Text = Context.Instance.TranslateDefault((string)labDefragStep.Tag);
      labStartingCondition.Text = Context.Instance.TranslateDefault((string)labStartingCondition.Tag);
      labStatus.Text = Context.Instance.TranslateDefault((string)labStatus.Tag);
      labTitle.Text = Context.Instance.TranslateDefault((string)labTitle.Tag);
      

      gbHeader_StartType.Header = Context.Instance.TranslateDefault((string)gbHeader_StartType.Tag);
      gbHeader_Refresh.Header = Context.Instance.TranslateDefault((string)gbHeader_Refresh.Tag);
      gbHeader_ManualStart.Header = Context.Instance.TranslateDefault((string)gbHeader_ManualStart.Tag);
      gbHeader_EnabledStatus.Header = Context.Instance.TranslateDefault((string)gbHeader_EnabledStatus.Tag);
      gbHeader_DefragStatusResult.Header = Context.Instance.TranslateDefault((string)gbHeader_DefragStatusResult.Tag);
      gbHeader_DefragStatus.Header = Context.Instance.TranslateDefault((string)gbHeader_DefragStatus.Tag);
      gbHeader_Calendar.Header = Context.Instance.TranslateDefault((string)gbHeader_Calendar.Tag);
      gbHeader_DefragCurrentStatus.Header = Context.Instance.TranslateDefault((string)gbHeader_DefragCurrentStatus.Tag);
      gbHeader_UpdateConfiguration.Header = Context.Instance.TranslateDefault((string)gbHeader_UpdateConfiguration.Tag);

      butRefresh.Content = Context.Instance.TranslateDefault((string)butRefresh.Tag);
      butManualStart.ToolTip= Context.Instance.TranslateDefault((string)butManualStart.Tag);
      butManualStop.ToolTip= Context.Instance.TranslateDefault((string)butManualStop.Tag);

      butDefragDisable.Content = Context.Instance.TranslateDefault((string)butDefragDisable.Tag);
      butDefragEnable.Content = Context.Instance.TranslateDefault((string)butDefragEnable.Tag);
      butDefragStartingConditionCancel.Content = Context.Instance.TranslateDefault((string)butDefragStartingConditionCancel.Tag);
      butDefragStartingConditionUpdate.Content = Context.Instance.TranslateDefault((string)butDefragStartingConditionUpdate.Tag);
      
      butDefragParameterShow.ToolTip = Context.Instance.TranslateDefault((string)butDefragParameterShow.Tag);
      butParameterViewClose.Content = Context.Instance.TranslateDefault((string)butParameterViewClose.Tag);
      butParameterRefresh.Content = Context.Instance.TranslateDefault((string)butParameterRefresh.Tag);

      colDefragStep_DateBorn.Header = Context.Instance.TranslateDefault("DATE BORN"); 
      colDefragStep_DateEnd.Header = Context.Instance.TranslateDefault("DATE END"); 
      colDefragStep_FromLane.Header = Context.Instance.TranslateDefault("FROM LANE"); 
      colDefragStep_NumPalletTransferred.Header = Context.Instance.TranslateDefault("NUM PALLET TRANSFERRED");
      colDefragStep_PalletAmount.Header = Context.Instance.TranslateDefault("PALLET AMOUNT");
      colDefragStep_StartingCondition.Header = Context.Instance.TranslateDefault("STARTING CONDITION");
      colDefragStep_Status.Header = Context.Instance.TranslateDefault("STATUS");
      colDefragStep_ToLane.Header = Context.Instance.TranslateDefault("TO LANE");

      colParameterDescription.Header = Context.Instance.TranslateDefault("PARAMETER DESCRIPTION");
      colParameterName.Header = Context.Instance.TranslateDefault("PARAMETER NAME");
      colParameterValue.Header = Context.Instance.TranslateDefault("PARAMETER VALUE");
    }
    #endregion

    #region COMANDI E RISPOSTE

    #region DFG_DefragStartingCondition_Get

    private void Cmd_DFG_DefragStartingCondition_Get()
    {
      CommandManagerC200318.DFG_DefragStartingCondition_Get(this);
    }

    private void DFG_DefragStartingCondition_Get(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var dfsc = dwr.Data as C200318_DefragStartingCondition;
      DefragStartingConditionServer.Update(dfsc);
      DefragStartingConditionLocal.Update(dfsc);
    }
    #endregion

    #region DFG_DefragStatus_Get
    private void Cmd_DFG_DefragStatus_Get()
    {
      CommandManagerC200318.DFG_DefragStatus_Get(this);
    }
    private void DFG_DefragStatus_Get(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var dfgs = dwr.Data as C200318_DefragStatus;
      DefragStatus_Update(dfgs);
    }

    #endregion

    #region DFG_DefragStatus_Set
    private void Cmd_DFG_DefragStatus_Set(C200318_DefragStatus dfgs)
    {
      CommandManagerC200318.DFG_DefragStatus_Set(this, dfgs);
    }
    private async void DFG_DefragStatus_Set(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (string.IsNullOrEmpty(dwr.Result)) return;

      C200318_DefragResultEnum result = dwr.Result.ConvertTo<C200318_DefragResultEnum>();
      if (result != C200318_DefragResultEnum.OK)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "DEFRAG UPDATE STATUS".TD(),
          Message = "ERROR UPDATING".TD() + $" [{result.ToString().TD()}]",
          OkButtonLabel = "OK".TD()
        };
        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);
        return;
      }
    }
    #endregion

    #region DFG_DefragStatus_Start
    private void Cmd_DFG_DefragStatus_Start()
    {
      CommandManagerC200318.DFG_DefragStatus_Start(this);
    }
    private async void DFG_DefragStatus_Start(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (string.IsNullOrEmpty(dwr.Result)) return;

      Refresh();


      C200318_DefragResultEnum result = dwr.Result.ConvertTo<C200318_DefragResultEnum>();
      if (result != C200318_DefragResultEnum.OK)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "DEFRAG UPDATE STATUS".TD(),
          Message = "ERROR STARTING".TD() + $" [{result.ToString().TD()}]",
          OkButtonLabel = "OK".TD()
        };
        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);
        return;
      }
    }
    #endregion

    #region DFG_DefragStatus_Stop
    private void Cmd_DFG_DefragStatus_Stop()
    {
      CommandManagerC200318.DFG_DefragStatus_Stop(this);
    }
    private async void DFG_DefragStatus_Stop(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (string.IsNullOrEmpty(dwr.Result)) return;

      Refresh();

      C200318_DefragResultEnum result = dwr.Result.ConvertTo<C200318_DefragResultEnum>();
      if (result != C200318_DefragResultEnum.OK)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "DEFRAG UPDATE STATUS".TD(),
          Message = "ERROR STOPPING".TD() + $" [{result.ToString().TD()}]",
          OkButtonLabel = "OK".TD()
        };
        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);
        return;
      }
    }
    #endregion

    #region DFG_DefragStartingCondition_Set
    private void Cmd_DFG_DefragStartingCondition_Set(C200318_DefragStartingCondition dfgsc)
    {
      CommandManagerC200318.DFG_DefragStartingCondition_Set(this, dfgsc);
    }
    private async void DFG_DefragStartingCondition_Set(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (string.IsNullOrEmpty(dwr.Result)) return;

      C200318_DefragResultEnum result = dwr.Result.ConvertTo<C200318_DefragResultEnum>();
      if (result != C200318_DefragResultEnum.OK)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "DEFRAG UPDATE STARTING CONDITION".TD(),
          Message = "ERROR UPDATING".TD() + $" [{result.ToString().TD()}]",
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);
        return;
      }

      Refresh();

    }

    #endregion

    #region DFG_DefragStepList_Get
    private void Cmd_DFG_DefragStepList_Get()
    {
      DefragStepL.Clear();
      CommandManagerC200318.DFG_DefragStepList_Get(this);
    }
    private void DFG_DefragStepList_Get(IList<string> commandMethodParameters, IModel model)
    {

      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;

      List<C200318_DefragStep> dfgStepL = dwr.Data as List<C200318_DefragStep>;
      if (dfgStepL == null) return;
      DefragStepL.AddRange(dfgStepL.OrderByDescending(dfgs => dfgs.Id));
      //dfgStepL.OrderByDescending(dfgs=>dfgs.Id).ToList().ForEach(dfgs => DefragStepL.Add(dfgs));

    }
    #endregion

    #region DFG_DefragParameters_Get
    private void Cmd_DFG_DefragParameters_Get()
    {
      DefragParameterL.Clear();

      CommandManagerC200318.DFG_DefragParameters_Get(this);
    }
    private void DFG_DefragParameters_Get(IList<string> commandMethodParameters, IModel model)
    {

      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;

      List<C200318_DefragParameter> dfgParametersL = dwr.Data as List<C200318_DefragParameter>;
      if (dfgParametersL == null) return;
      DefragParameterL.AddRange(dfgParametersL);
    }
    #endregion

    #region DFG_DefragParameter_Set
    private void Cmd_DFG_DefragParameter_Set(C200318_DefragParameter dfgp)
    {
      CommandManagerC200318.DFG_DefragParameter_Set(this,dfgp);
    }
    private async void DFG_DefragParameter_Set(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (string.IsNullOrEmpty(dwr.Result)) return;

      C200318_DefragResultEnum result = dwr.Result.ConvertTo<C200318_DefragResultEnum>();
      if (result != C200318_DefragResultEnum.OK)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "DEFRAG UPDATE PARAMETER".TD(),
          Message = "ERROR UPDATING".TD() + $" [{result.ToString().TD()}]",
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);
        return;
      }

      Cmd_DFG_DefragParameters_Get();
    }
    #endregion

    #endregion

    #region PRIVATE METHODS
    private void DefragStatus_Update(C200318_DefragStatus dfgs)
    {
      DefragStatusServer.Update(dfgs);
      DefragStatusServer.ResultTranslated = Context.Instance.TranslateDefault(dfgs.Result.ToString());
      DefragStatusLocal.Update(dfgs);
      DefragStatusLocal.ResultTranslated = Context.Instance.TranslateDefault(dfgs.Result.ToString());
    }


    private void Refresh()
    {
      Cmd_DFG_DefragStartingCondition_Get();
      Cmd_DFG_DefragStatus_Get();
      Cmd_DFG_DefragStepList_Get();
    }
    #endregion

    #region EVENTS
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      Refresh();
    }

    private void butDefragEnable_Click(object sender, RoutedEventArgs e)
    {
      DefragStartingConditionLocal.Enabled = true;
    }

    private void butDefragDisable_Click(object sender, RoutedEventArgs e)
    {
      DefragStartingConditionLocal.Enabled= false;
    }

    private void butDefragStartingConditionUpdate_Click(object sender, RoutedEventArgs e)
    {
      Cmd_DFG_DefragStartingCondition_Set(DefragStartingConditionLocal);
    }

    private void butDefragStartingConditionCancel_Click(object sender, RoutedEventArgs e)
    {
      DefragStartingConditionLocal.Update(DefragStartingConditionServer);
    }

    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      Refresh();
    }

    private void butManualStart_Click(object sender, RoutedEventArgs e)
    {
      Cmd_DFG_DefragStatus_Start();
    }

    private void butManualStop_Click(object sender, RoutedEventArgs e)
    {
      Cmd_DFG_DefragStatus_Stop();
    }

    private void butParameterUpdate_Click(object sender, RoutedEventArgs e)
    {
      var button = sender as Button;
      if (button == null)
        return;

      C200318_DefragParameter parameter = button.Tag as C200318_DefragParameter;

      if (parameter == null)
        return;

      Cmd_DFG_DefragParameter_Set(parameter);
    }

    private void butParameterViewClose_Click(object sender, RoutedEventArgs e)
    {
      ShowDefragParameter=false;
    }

    private void butDefragParameterShow_Click(object sender, RoutedEventArgs e)
    {
      ShowDefragParameter = true;
      Cmd_DFG_DefragParameters_Get();

    }

    private void dgParameters_Loaded(object sender, RoutedEventArgs e)
    {
    }

    private void butParameterRefresh_Click(object sender, RoutedEventArgs e)
    {
      Cmd_DFG_DefragParameters_Get();
    }
    #endregion
  }
}
