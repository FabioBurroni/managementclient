﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Model;
using Model.Custom.C200318;
using Model.Custom.C200318.OrderManager;
using View.Common.Languages;

namespace View.Custom.C200318.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderManager.xaml
  /// </summary>
  public partial class CtrlOrderManager : CtrlBaseC200318
  {
    private bool isEmergencial;
    public bool IsEmergencial
    {
      get { return isEmergencial; }
      set
      {
        isEmergencial = value;

        txtActualModeValue.Text = isEmergencial ? "EMERGENCIAL".TD() : "AUTOMATIC".TD();
        bEmergency.Background = isEmergencial ? Brushes.IndianRed : Brushes.White;
        txtActualModeValue.Foreground = isEmergencial ? Brushes.White : Brushes.Black;

        

        NotifyPropertyChanged("IsEmergencial");
      }
    }

    #region CONSTRUCTOR
    public CtrlOrderManager()
    {
      InitializeComponent();

    } 
    #endregion
        
    private void CtrlOrderCreate_OnOrderLoad(string orderCode)
    {
      ctrlOrderViewer.LoadOrderByCode(orderCode);
      tiOrdersViewer.IsSelected = true;
    }

    #region TRADUZIONI

    protected override void Translate()
    {
      txtTabHeaderOrderBuilder.Text = Context.Instance.TranslateDefault((string)txtTabHeaderOrderBuilder.Tag);
      txtTabHeaderOrderManager.Text = Context.Instance.TranslateDefault((string)txtTabHeaderOrderManager.Tag);
      txtTabHeaderWorkModality.Text = Context.Instance.TranslateDefault((string)txtTabHeaderWorkModality.Tag);

      txtActualModeValue.Text = IsEmergencial ? "EMERGENCIAL".TD() : "AUTOMATIC".TD();
    }
    #endregion
    #region COMANDI E RISPOSTE

    #region OM_OrderDestination_GetAll
    private void Cmd_OM_OrderDestination_GetAll()
    {
      CommandManagerC200318.OM_OrderDestination_GetAll(this);
    }

    private void OM_OrderDestination_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var list = dwr.Data as List<C200318_ListDestination>;
      if (C200318_ModelContext.Instance.ListDestinasions.Count == 0)
      {
        C200318_ModelContext.Instance.ListDestinasions.AddRange(list);
      }
    }
    #endregion    

   
    #endregion

    #region EVENTI COTROLLO
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      if (C200318_ModelContext.Instance.ListDestinasions.Count == 0)
        Cmd_OM_OrderDestination_GetAll();

      Translate();
    }

    private void CtrlBaseC200318_Unloaded(object sender, RoutedEventArgs e)
    {
      //try
      //{
      //  //Authentication.AuthenticationManager.Instance.LoginSession -= Instance_LoginSession;
      //}
      //catch (Exception)
      //{

      //}
    }

    #endregion


    private void CtrlOrderWorkModalityManager_OnWorkModalityUpdate(C200318_WorkModality workModality)
    {
      if (workModality == C200318_WorkModality.EMERGENCIAL)
        IsEmergencial = true;
      else
        IsEmergencial = false;
    }
  }
}
