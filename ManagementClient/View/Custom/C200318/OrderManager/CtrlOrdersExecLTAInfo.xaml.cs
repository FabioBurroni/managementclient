﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200318.OrderManager;
using View.Common.Languages;

namespace View.Custom.C200318.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderLTA.xaml
  /// </summary>
  public partial class CtrlOrdersExecLTAInfo : CtrlBaseC200318
  {
    public C200318_WorkModalitySwitchRequestOption Result { get; set; }

    ObservableCollectionFast<C200318_Order> OrderList = new ObservableCollectionFast<C200318_Order>();

    private C200318_WorkModality workModalityCurrent;
    public C200318_WorkModality WorkModalityCurrent
    {
      get { return workModalityCurrent; }
      set
      {
        workModalityCurrent = value;

        NotifyPropertyChanged("WorkModalityCurrent");
      }
    }

    private string subTitle;
    public string SubTitle
    {
      get { return subTitle; }
      set
      {
        subTitle = value;

        NotifyPropertyChanged("SubTitle");
      }
    }

    #region CONSTRUCTOR
    public CtrlOrdersExecLTAInfo(ObservableCollectionFast<C200318_Order> orderList, C200318_WorkModality workModality)
    {
      if (orderList != null)
        OrderList = orderList;

      WorkModalityCurrent = workModality;

      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);

      txtBlkCancel.Text = Context.Instance.TranslateDefault((string)txtBlkCancel.Tag);
      txtBlkConfirm.Text = Context.Instance.TranslateDefault((string)txtBlkConfirm.Tag);

    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {

      Translate();

      foreach (var kvp in WordDic)
      {
        kvp.Value.Localized = kvp.Value.DefaultVal.TD();
      }

      PrepareOrderList();
    }
    #endregion

    #region TRADUZIONI
    public Dictionary<string, Localization.Word> WordDic { get; set; } = new Dictionary<string, Localization.Word>();

    private void WordDicAdd(string key, string defaultVal)
    {
      if (WordDic.ContainsKey(key))
        return;
      WordDic.Add(key, new Localization.Word(key, defaultVal, defaultVal.TD()));
    }
   
    #endregion

    #region EVENTI
 
    private void butCancel_Click(object sender, RoutedEventArgs e)
    {
      CloseWithResult(C200318_WorkModalitySwitchRequestOption.CANCELED);
    }

    private void butConfirm_Click(object sender, RoutedEventArgs e)
    {
      CloseWithResult(C200318_WorkModalitySwitchRequestOption.OK);
    }
    #endregion

    #region Private Methods
    void PrepareOrderList()
    {
      if (OrderList == null)
        return;

      // subtitle
      if (WorkModalityCurrent == C200318_WorkModality.EMERGENCIAL)
        SubTitle = "EMERGENCIAL".TD() + " -> " + "AUTOMATIC".TD();
      else
        SubTitle = "AUTOMATIC".TD() + " -> " + "EMERGENCIAL".TD();

      if (OrderList.Count > 0)
      {
        var q = new TextBlock();
        q.Text = "Orders in execution will be stopped".TD() + ":";
        q.FontSize = 20;
        q.Foreground = Brushes.Red;
        spContent.Children.Add(q);

        foreach (var ord in OrderList)
        {
          //create user control foreach order
          CtrlOrderExecLTAInfo order = new CtrlOrderExecLTAInfo();
          order.Order = ord;
          spContent.Children.Add(order);
        }
      }

    }
    
    void CloseWithResult(C200318_WorkModalitySwitchRequestOption res)
    {
      Result = res;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }
    #endregion

  }
}
