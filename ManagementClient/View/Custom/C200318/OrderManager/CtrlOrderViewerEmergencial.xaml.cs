﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Utilities.Extensions;
using Model;
using Model.Common;
using Model.Custom.C200318;
using Model.Custom.C200318.OrderManager;
using View.Common.Languages;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;

namespace View.Custom.C200318.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderViewer.xaml
  /// </summary>
  public partial class CtrlOrderViewerEmergencial : CtrlBaseC200318
  {
    #region Public Properties
    //20200504
    public UserLogged UserLogged { get; set; } = new UserLogged();
    #endregion

    #region Fields
    private int _indexStartValue = 0; 
    #endregion

    #region Contructor
    public CtrlOrderViewerEmergencial()
    {
      InitializeComponent();
      
      if (DesignTimeHelper.IsInDesignMode)
        return;


      Filter = new C200318_OrderFilter();
      ctrlOrders.OrderL = OrderL;
      //20200504
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
      Authentication.AuthenticationManager.Instance.LoginSession += Instance_LoginSession;

      //OrderDestinationL.AddRange(C200318_ModelContext.Instance.ListDestinasions);

    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
     
      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
    }

    #endregion TRADUZIONI
    
    #region Evento Login
    //20200504
    private void Instance_LoginSession(object sender, Authentication.LoginSessionEventArgs sessionArgs)
    {
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
    }
    #endregion

    #region PUBLIC PROPERTIES
    public ObservableCollectionFast<C200318_ListDestination> OrderDestinationL { get; set; } = new ObservableCollectionFast<C200318_ListDestination>();
    public C200318_OrderFilter Filter { get; set; } = new C200318_OrderFilter();
    public ObservableCollectionFast<C200318_Order> OrderL { get; set; } = new ObservableCollectionFast<C200318_Order>();
    #endregion

    #region COMANDI E RISPOSTE

    #region OM_OrderComponent_Kill
    private void Cmd_OM_OrderComponent_Kill(int order_id, int cmp_id)
    {
      CommandManagerC200318.OM_OrderComponent_Kill(this, order_id,cmp_id);
    }

    private async void OM_OrderComponent_Kill(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data is C200318_CustomResult)
      {
        var result = (C200318_CustomResult)dwr.Data;
        if (result == C200318_CustomResult.OK)
        {
          int orderId = commandMethodParameters[1].ConvertTo<int>();
          if (orderId != 0)
          {
            Cmd_OM_Order_Get("", orderId);
          }
        }
        else
        {
          AlertDialogArguments alertDialogArgs = new AlertDialogArguments
          {
            Title = "ORDER COMPONENT".TD(),
            Message = "ERROR CANCELLING".TD(),
            OkButtonLabel = "OK".TD()
          };

          await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        }
      }
    }
    #endregion

    #region OM_OrderDestination_GetAll
    private void Cmd_OM_OrderDestination_GetAll()
    {
      CommandManagerC200318.OM_OrderDestination_GetAll(this);
    }

    private void OM_OrderDestination_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var list = dwr.Data as List<C200318_ListDestination>;

      if(OrderDestinationL.Count==0)
        OrderDestinationL.AddRange(list);
    }
    #endregion

    #region OM_Order_Get
    private void Cmd_OM_Order_Get(string orderCode, int orderId)
    {
      CommandManagerC200318.OM_Order_Get(this, orderCode, orderId);
    }

    private void OM_Order_Get(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var orderNew = dwr.Data as C200318_Order;
      if (orderNew != null)
      {
        var orderOld = OrderL.FirstOrDefault(ord => ord.Code == orderNew.Code);
        if (orderOld != null)
        {
          orderOld.Update(orderNew);
          orderOld = orderNew;
          ctrlOrders.OrderSelected = orderOld;
        }
        else
        {
          OrderL.Add(orderNew);
        }
      }
    }
    #endregion

    #region OM_Order_UpdateState
    private void Cmd_OM_Order_UpdateState(C200318_Order order, C200318_State newState)
    {
      //ShowWait(true, "PALLET", "ATTENDERE RECUPERO INFORMAZIONI");
      CommandManagerC200318.OM_Order_UpdateState(this, order.Id, newState);
    }
    private void OM_Order_UpdateState(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      if (dwr.Data is C200318_CustomResult)
      {
        var result = (C200318_CustomResult)dwr.Data;
        if (result != C200318_CustomResult.OK)
        {
          MessageBox.Show("SI E' VERIFICATO UN ERRORE! " + result.ToString());
        }
        //MessageBox.Show(result.ToString());
      }

      Cmd_OM_Order_Get("", commandMethodParameters[1].ConvertToInt());

    }
    #endregion

    #region OM_Order_Update
    private void Cmd_OM_Order_Update(C200318_Order order, bool urgente, int priority, bool completamentoManuale)
    {
      //ShowWait(true, "PALLET", "ATTENDERE RECUPERO INFORMAZIONI");
      if (order == null)
        return;

      CommandManagerC200318.OM_Order_Update(this, order.Id, priority, urgente, completamentoManuale);
    }
    private void OM_Order_Update(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      if (dwr.Data is C200318_CustomResult)
      {
        var result = (C200318_CustomResult)dwr.Data;
        MessageBox.Show(result.ToString());
      }

      Cmd_OM_Order_Get("", commandMethodParameters[1].ConvertToInt());


    }
    #endregion

    #region OM_Order_GetAll_Paged
    private void Cmd_OM_Order_GetAll_Paged()
    {
      //ShowWait(true, "PALLET", "ATTENDERE RECUPERO INFORMAZIONI");
      OrderL.Clear();
      CommandManagerC200318.OM_Order_GetAll_Paged(this, Filter);
    }
    private void OM_Order_GetAll_Paged(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var orderL = dwr.Data as List<C200318_Order>;
      if (orderL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (orderL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        orderL = orderL.Take(Filter.MaxItems).ToList();
        OrderL.AddRange(orderL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }
    #endregion

    #region OM_OrderCell_PalletInTransit

    private void Cmd_OM_OrderCell_PalletInTransit(string orderCode, string cellCode)
    {
      CommandManagerC200318.OM_OrderCell_PalletInTransit(this, orderCode, cellCode);
    }

    private async void OM_OrderCell_PalletInTransit(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      if (dwr.Data is List<C200318_PalletInTransit>)
      {
        var palL = dwr.Data as List<C200318_PalletInTransit>;

        // prepare the view
        var view = new CtrlPalletInTransitForCell(palL);
        view.OnPalletComplete += WinPalletInTransitForCell_OnPalletComplete;
        view.MinHeight = 700;
        view.MinWidth = 1200;

        //show the dialog
        await DialogHost.Show(view, "RootDialogWithoutExit");

        //OK, request new config....
        //if (view.Result == C200318_WorkModalitySwitchRequestOption.OK)
        //  Cmd_OM_WorkModalitySet(WorkModalityToRequest);
      }
    }

    private void WinPalletInTransitForCell_OnPalletComplete(C200318_PalletInTransit Pallet)
    {
      CommandManagerC200318.OM_Emergencial_CheckOut(this, Pallet.Code);
    }

    private void OM_Emergencial_CheckOut(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      if (dwr.Data is C200318_CustomResult)
      {
        var result = (C200318_CustomResult)dwr.Data;

        if (result == C200318_CustomResult.OK)
          LocalSnackbar.ShowMessageOk("RESULT".TD() + ": " + result.ToString(),5);
        else
          LocalSnackbar.ShowMessageFail("RESULT".TD() + ": " + result.ToString(), 5);

        if (ctrlOrders.OrderSelected != null)
          Cmd_OM_Order_Get("", ctrlOrders.OrderSelected.Id);
      }

    }

    #endregion

  
    #endregion

    #region PUBLIC METHODS
    public void LoadOrderByCode(string orderCode)
    {
      OrderL.Clear();
      Cmd_OM_Order_Get(orderCode,0);
    }
    public void LoadOrderById(int orderId)
    {
      OrderL.Clear();

      Cmd_OM_Order_Get("",orderId);
    }
    #endregion

    #region EVENTI

    #region Eventi Ricerca
    
    private void CtrlOrderFilter_OnSearch()
    {
      Cmd_OM_Order_GetAll_Paged();
    }
    #endregion

    #region Eventi Paginazione
    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_OM_Order_GetAll_Paged();
    }
    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_OM_Order_GetAll_Paged();
    }
    #endregion

    #region Evento Cambio Stato Ordine
    private void ctrlOrders_OnOrderChangeState(C200318_Order order, C200318_State newState)
    {
      Cmd_OM_Order_UpdateState(order, newState);
    }
    #endregion

    #region EVENTO REFRESH
    private void ctrlOrders_OnOrderRefresh(C200318_Order order)
    {
      if (order != null)
        Cmd_OM_Order_Get("", order.Id);
      else
        Cmd_OM_Order_GetAll_Paged();
    }

    private void ctrlOrders_OnOrderRefreshByState(C200318_State state)
    {
      Filter.Reset();
      Filter.StateSelected_Set(state);
      Cmd_OM_Order_GetAll_Paged();
    }
    #endregion

    #region Evento richiesta Info Componente
    private void CtrlCell_OnInfo(string orderCode, string cellCode)
    {
      if ((orderCode != null)&&(cellCode != null))
      {
        Cmd_OM_OrderCell_PalletInTransit(orderCode,cellCode);
      }
    } 
    #endregion

    #region Evento Loaded Unloaded
    private void CtrlBaseC200318_Unloaded(object sender, RoutedEventArgs e)
    {
      try
      {
        Authentication.AuthenticationManager.Instance.LoginSession -= Instance_LoginSession;
      }
      catch (Exception)
      {

      }
    }

   

    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();

      if (Utilities.Extensions.DesignTimeHelper.IsInDesignMode)
        return;

      //if (C200318_ModelContext.Instance.ListDestinasions.Count == 0)
        Cmd_OM_OrderDestination_GetAll();

    }
    #endregion

    
   
    #region EVENTO KILL COMPONMENTE
    private async void CtrlCmps_OnComponentKill(C200318_Order ord, C200318_OrderCmp cmp)
    {
      ConfirmationDialogArguments dialogArgs = new ConfirmationDialogArguments
      {
        Title = "CANCEL ORDER COMPONENT".TD(),
        Message = "Are you sure?".TD(),
        OkButtonLabel = "YES".TD(),
        CancelButtonLabel = "CANCEL".TD(),
        StackedButtons = false
      };

      bool result = await ConfirmationDialog.ShowDialogAsync("RootDialog", dialogArgs);

      if (result)
      {
        Cmd_OM_OrderComponent_Kill(ord.Id, cmp.Id);
      }
    }
    #endregion

    #endregion

   
  }

 
}
