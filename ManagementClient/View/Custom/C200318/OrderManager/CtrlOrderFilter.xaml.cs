﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200318;
using Model.Custom.C200318.OrderManager;


namespace View.Custom.C200318.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderFilter.xaml
  /// </summary>
  public partial class CtrlOrderFilter : CtrlBaseC200318
  {
    #region EVENTI
    public delegate void OnSearchOrderHandler();
    public event OnSearchOrderHandler OnSearch;
    #endregion

    #region Fields
    private const int _indexStartValue = 0;

    #endregion

    #region PUBLIC PROPERTIES
    public C200318_OrderFilter Filter { get; set; } = new C200318_OrderFilter();
    #endregion

    #region COSTRUTTORE
    public CtrlOrderFilter()
    {
      InitializeComponent();
      Filter.Reset();
    }
    #endregion
    #region TRADUZIONI

    protected override void Translate()
    {
      HintAssist.SetHint(cbOrderState, Localization.Localize.LocalizeDefaultString((string)cbOrderState.Tag).ToLower());
      HintAssist.SetHint(cbMaxItems, Localization.Localize.LocalizeDefaultString((string)cbMaxItems.Tag).ToLower());
      HintAssist.SetHint(cbOrderType, Localization.Localize.LocalizeDefaultString((string)cbOrderType.Tag).ToLower());
      HintAssist.SetHint(txtBoxOrderCode, Localization.Localize.LocalizeDefaultString((string)txtBoxOrderCode.Tag).ToLower());

      txtBlkPriority.Text = Context.Instance.TranslateDefault((string)txtBlkPriority.Tag);
      txtBlkSearch.Text = Context.Instance.TranslateDefault((string)txtBlkSearch.Tag);
      txtBlkQuickSearch.Text = Context.Instance.TranslateDefault((string)txtBlkQuickSearch.Tag);

      butFilterByStateDone.ToolTip = Localization.Localize.LocalizeDefaultString((string)butFilterByStateDone.Tag);
      butFilterByStateExec.ToolTip = Localization.Localize.LocalizeDefaultString((string)butFilterByStateExec.Tag);
      butFilterByStateKill.ToolTip = Localization.Localize.LocalizeDefaultString((string)butFilterByStateKill.Tag);
      butFilterByStatePause.ToolTip = Localization.Localize.LocalizeDefaultString((string)butFilterByStatePause.Tag);
      btnCancel.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnCancel.Tag);
      btnSearch.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnSearch.Tag);

      btnCancel.Content = Localization.Localize.LocalizeDefaultString((string)btnCancel.Tag);
      btnSearch.Content = Localization.Localize.LocalizeDefaultString((string)btnSearch.Tag);

      var selOrderTypeIndex = cbOrderType.SelectedIndex;
      cbOrderType.SelectedItem = null;
      CollectionViewSource.GetDefaultView(cbOrderType.ItemsSource).Refresh();
      cbOrderType.SelectedIndex = selOrderTypeIndex;

      var selOrderStateIndex = cbOrderState.SelectedIndex;
      cbOrderState.SelectedItem = null;
      CollectionViewSource.GetDefaultView(cbOrderState.ItemsSource).Refresh();
      cbOrderState.SelectedIndex = selOrderStateIndex;
    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
    #endregion

    #region EVENTI

    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
    }

    private void btnSearch_Click(object sender, RoutedEventArgs e)
    {
      if (cbMaxItems.SelectedItem != null)
      {
        Filter.MaxItems = (int)cbMaxItems.SelectedItem;
      }
      Filter.Index = _indexStartValue;
      Search();
    }
   

    private void butFilterByStateExec_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
      Filter.StateSelected_Set(C200318_State.LEXEC);
      Search();
    }

    private void butFilterByStatePause_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
      Filter.StateSelected_Set(C200318_State.LPAUSE);
      Search();
    }

    private void butFilterByStateDone_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
      Filter.StateSelected_Set(C200318_State.LDONE);
      Search();
    }

    private void butFilterByStateKill_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
      Filter.StateSelected_Set(C200318_State.LKILL);
      Search();
    }

    private void butFilterByStateWait_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
      Filter.StateSelected_Set(C200318_State.LWAIT);
      Search();
    }
    #endregion

    #region private Methods
    private void Search()
    {
      OnSearch?.Invoke();
    }
    #endregion
  }
}
