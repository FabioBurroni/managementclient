﻿
using System.Collections.Generic;
using System.Windows;
using Model.Custom.C200318.OrderManager;
using Model;
using System.Windows.Controls;
using System.Linq;

namespace View.Custom.C200318.OrderManager
{
  /// <summary>
  /// Interaction logic for WinPalletInTransitForCmp.xaml
  /// </summary>
  public partial class WinPalletInTransitForCmp : Window
  {
    /// <summary>
    /// Delegate for Complete Pallet
    /// </summary>
    /// <param name="PalletInTransit"></param>
    public delegate void OnPalletCompleteRequest(C200318_PalletInTransit Pallet);
    /// <summary>
    /// Event for Kill Component
    /// </summary>
    public event OnPalletCompleteRequest OnPalletComplete;


    public ObservableCollectionFast<C200318_PalletInTransit> PalletInTransit { get; set; } = new ObservableCollectionFast<C200318_PalletInTransit>();

    public WinPalletInTransitForCmp(List<C200318_PalletInTransit> palletInTransit)
    {
      InitializeComponent();
      
      PalletInTransit.AddRange(palletInTransit);

      if (palletInTransit.Count > 0)
      {
        if (palletInTransit.FirstOrDefault().WorkModality == C200318_WorkModality.EMERGENCIAL)
          colComplete.Visibility = Visibility.Visible;
        else
          colComplete.Visibility = Visibility.Collapsed;
      }

      Translate();
    }

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {

    }
    private void Translate()
    {
      Title = Localization.Localize.LocalizeDefaultString("NO PALLET FOR ORDER ITEM");
      txtBlkNoPallet.Text = Context.Instance.TranslateDefault((string)txtBlkNoPallet.Tag);
      txtBlkPalletAssigned.Text = Context.Instance.TranslateDefault((string)txtBlkPalletAssigned.Tag);
      butClose.ToolTip = Context.Instance.TranslateDefault((string)butClose.Tag);

      colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
      colPositionCode.Header = Localization.Localize.LocalizeDefaultString("POSITION CODE");
    }

    
    private void butComplete_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_PalletInTransit)
      {
        C200318_PalletInTransit pal = b.Tag as C200318_PalletInTransit;

        OnPalletComplete?.Invoke(pal);
      }
    }
  }
}
