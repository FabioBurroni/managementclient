﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200318.OrderManager;
using View.Common.Languages;

namespace View.Custom.C200318.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderCellEmergencialFetch.xaml
  /// </summary>
  public partial class CtrlOrderCellEmergencialFetch : CtrlBaseC200318
  {

    /// <summary>
    /// Delegato per richiesta fetch pallet assegnati ad ordine
    /// </summary>
    /// <param name="cmp"></param>
    public delegate void OnFetchHandler(string orderCode, string cellCode);
    /// <summary>
    /// Evento per richiesta info pallet assegnati ad ordine
    /// </summary>
    /// <param name="cmp"></param>
    public event OnFetchHandler OnFetch;

    public C200318_Order Order
    {
      get { return (C200318_Order)GetValue(OrderProperty); }
      set { SetValue(OrderProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Order.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty OrderProperty =
        DependencyProperty.Register("Order", typeof(C200318_Order), typeof(CtrlOrderCellEmergencialFetch), new PropertyMetadata(null, PalletInTransitLPropertyChanged));

    private static void PalletInTransitLPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var ctrl = (CtrlOrderCellEmergencialFetch)d;

      ctrl.Cells = new List<C200318_CellPalletInTransit>();

      List<C200318_CellPalletInTransit> cells = new List<C200318_CellPalletInTransit>();

      if (ctrl.Order != null)
      {
        if (ctrl.Order.PalletInTransitL.Count > 0)
        {
          var groupPalletInTransitforCell =
              ctrl.Order.PalletInTransitL.GroupBy(x => x.PositionCode);


          foreach (var cellGroup in groupPalletInTransitforCell)
          {
            var c = new C200318_CellPalletInTransit();
            c.Code = cellGroup.Key;
            c.Order = ctrl.Order;

            foreach (var pall in cellGroup)
              c.PalletInTransitL.Add(pall);

            cells.Add(c);
          }

          ctrl.Cells = cells;
        }
       
      }
      
    }

    private List<C200318_CellPalletInTransit> _Cells;

    public List<C200318_CellPalletInTransit> Cells
    {
      get { return _Cells; }
      set { _Cells = value;
        NotifyPropertyChanged("Cells"); 
                  }
    }


    #region CONSTRUCTOR
    public CtrlOrderCellEmergencialFetch()
    {
    
      WordDicAdd("PALLET_IN_TRANSIT", "PALLET IN TRANSIT");
      WordDicAdd("PALLET_IN_TRANSIT_COUNT", "PALLET IN TRANSIT COUNT");
      WordDicAdd("COPY", "COPY");
      WordDicAdd("PALLET_CODE", "PALLET CODE");
      WordDicAdd("ARTICLE_CODE", "ARTICLE CODE");
      WordDicAdd("LOT_CODE", "LOT CODE");
      WordDicAdd("ARTICLE_DESCR", "ARTICLE DESCRIPTION");
      WordDicAdd("POSITION_CODE", "POSITION CODE");
      WordDicAdd("DESTINATION", "DESTINATION");

      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkNumberOfPallets.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfPallets.Tag);

      colCellCode.Header = Localization.Localize.LocalizeDefaultString("CELL CODE");
      colX.Header = Localization.Localize.LocalizeDefaultString("COLUMN");
      colY.Header = Localization.Localize.LocalizeDefaultString("FLOOR");
      colZ.Header = Localization.Localize.LocalizeDefaultString("SIDE");
      colFetch.Header = Localization.Localize.LocalizeDefaultString("FETCH");

      

      if (dgCells.ItemsSource != null)
        CollectionViewSource.GetDefaultView(dgCells.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {

      Translate();

      foreach (var kvp in WordDic)
      {
        kvp.Value.Localized = kvp.Value.DefaultVal.TD();
      }
    }
    #endregion

    #region TRADUZIONI
    public Dictionary<string, Localization.Word> WordDic { get; set; } = new Dictionary<string, Localization.Word>();

    private void WordDicAdd(string key, string defaultVal)
    {
      if (WordDic.ContainsKey(key))
        return;
      WordDic.Add(key, new Localization.Word(key, defaultVal, defaultVal.TD()));
    }
   
    #endregion

    #region EVENTI
   
    private void butCopyCellCode_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        Button b = sender as Button;
        if (b == null)
          return;
        Clipboard.SetText(b.Tag as string);

        LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);


      }
      catch
      {
      }
    }

    private void butFetchMenu_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200318_CellPalletInTransit)
      {
        C200318_CellPalletInTransit cell = b.Tag as C200318_CellPalletInTransit;
        OnFetch?.Invoke(cell.Order.Code, cell.Code);
      }
    }
    
    #endregion
  }
}
