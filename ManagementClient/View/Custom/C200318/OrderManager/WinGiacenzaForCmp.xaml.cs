﻿
using System.Windows;
using Model;
using Model.Custom.C200318.OrderManager;
namespace View.Custom.C200318.OrderManager
{
  /// <summary>
  /// Interaction logic for WinStockForCmp.xaml
  /// </summary>
  public partial class WinStockForCmp : Window
  {
    public string Cell { get; set; }
    public string NumberOfPallets { get; set; }

    public C200318_ArticleLotStockByCell Stock { get; set; } = new C200318_ArticleLotStockByCell();
    public WinStockForCmp(C200318_ArticleLotStockByCell stock)
    {
      
      if(stock != null)
      {
        Stock = stock;
      }
      InitializeComponent();

      Translate();

    }

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void Translate()
    {
      Title = Localization.Localize.LocalizeDefaultString("STOCK FOR ORDER ITEM");
      //txtBlkStockFor.Text = Context.Instance.TranslateDefault((string)txtBlkStockFor.Tag);

      Cell = Localization.Localize.LocalizeDefaultString("CELL");
      NumberOfPallets = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLETS");

      colCellCode.Header = Cell;
      colNumPallet.Header = NumberOfPallets;
    }
  }
}
