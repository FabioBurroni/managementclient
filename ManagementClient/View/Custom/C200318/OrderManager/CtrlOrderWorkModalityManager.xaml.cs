﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200318;
using Model.Custom.C200318.OrderManager;
using Utilities.Extensions;
using View.Common.Languages;
using View.Custom.C200318.Converter;

namespace View.Custom.C200318.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderWorkModalityManager.xaml
  /// </summary>
  public partial class CtrlOrderWorkModalityManager : CtrlBaseC200318
  {
    #region Events
    public delegate void OnWorkModalityUpdateHandler(C200318_WorkModality workModality);
    public event OnWorkModalityUpdateHandler OnWorkModalityUpdate;

    #endregion

    #region private field
    Timer switchingTimer = new Timer();
    #endregion

    #region public Properties
    public C200318_OrderFilter Filter { get; set; } = new C200318_OrderFilter();
    public ObservableCollectionFast<C200318_Order> OrderL { get; set; } = new ObservableCollectionFast<C200318_Order>();

    private bool _isSelectionValid;

    public bool IsSelectionValid
    {
      get { return _isSelectionValid; }
      set
      {
        _isSelectionValid = value;
        NotifyPropertyChanged();
      }
    }

    private C200318_WorkModalityConfiguration workModality;
    public C200318_WorkModalityConfiguration WorkModality
    {
      get { return workModality; }
      set
      {
        workModality = value;
                
        OnWorkModalityUpdate?.Invoke(workModality.Current);

        NotifyPropertyChanged("WorkModality");
      }
    }

    private C200318_WorkModality workModalityToRequest;
    public C200318_WorkModality WorkModalityToRequest
    {
      get { return workModalityToRequest; }
      set
      {
        workModalityToRequest = value;

        NotifyPropertyChanged("WorkModalityToRequest");
      }
    }
    #endregion

  
    public CtrlOrderWorkModalityManager()
    {
      InitializeComponent();
      WorkModality = new C200318_WorkModalityConfiguration();
      WorkModalityToRequest = C200318_WorkModality.NORMAL;

      switchingTimer.AutoReset = false;
      switchingTimer.Stop();
      switchingTimer.Elapsed += new ElapsedEventHandler(WorkModalityGet);
      switchingTimer.Interval = 10000;
    }

    #region Translation
    protected override void Translate()
    {
      txtBlkTitle.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkTitle.Tag);

      // 1
      txtBlkCurrentConfiguration.FirstLevelTitle = "CURRENT CONFIGURATION".TD();
      txtBlkCurrentConfigurationLastUpdate.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkCurrentConfigurationLastUpdate.Tag);
      btnChangeMode.Continue = Localization.Localize.LocalizeDefaultString((string)btnChangeMode.Tag);

      //2
      txtBlkStepperHeader2.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkStepperHeader2.Tag);
      HintAssist.SetHint(txtBoxActualConfiguration, Localization.Localize.LocalizeDefaultString((string)txtBoxActualConfiguration.Tag));
      txtBlkCurrentConfigurationLastUpdateSwitch.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkCurrentConfigurationLastUpdateSwitch.Tag);
      HintAssist.SetHint(cmbBoxNewConfiguration, Localization.Localize.LocalizeDefaultString((string)cmbBoxNewConfiguration.Tag));
      txtBlkRequestedConfigurationLastUpdateSwitch.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkRequestedConfigurationLastUpdateSwitch.Tag);
      btnCANCEL_step2_Switch.Content = Localization.Localize.LocalizeDefaultString((string)btnCANCEL_step2_Switch.Tag);
      btnCANCEL_step2_Switch.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnCANCEL_step2_Switch.Tag);
      btnSET_step2_Switch.Content = Localization.Localize.LocalizeDefaultString((string)btnSET_step2_Switch.Tag);
      btnSET_step2_Switch.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnSET_step2_Switch.Tag);
      btnFORCE_step2_Switch.Content = Localization.Localize.LocalizeDefaultString((string)btnFORCE_step2_Switch.Tag);
      btnFORCE_step2_Switch.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnFORCE_step2_Switch.Tag);

      //3
      txtBlkStepperHeader3.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkStepperHeader3.Tag);
      HintAssist.SetHint(txtBoxActualConfigurationSwitching, Localization.Localize.LocalizeDefaultString((string)txtBoxActualConfigurationSwitching.Tag));
      txtBoxActualConfigurationSwitching.Text = Localization.Localize.LocalizeDefaultString((string)txtBoxActualConfigurationSwitching.Tag);
      txtBlkCurrentConfigurationLastUpdateSwitching.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkCurrentConfigurationLastUpdateSwitching.Tag);
      HintAssist.SetHint(txtBlkRequestedConfiguration, Localization.Localize.LocalizeDefaultString((string)txtBlkRequestedConfiguration.Tag));
      txtBlkRequestedConfiguration.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkRequestedConfiguration.Tag);
      txtBlkRequestedConfigurationLastUpdateSwitching.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkRequestedConfiguration.Tag);
      btnFORCE_step3_Switching.Content = Localization.Localize.LocalizeDefaultString((string)btnFORCE_step3_Switching.Tag);
      btnFORCE_step3_Switching.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnFORCE_step3_Switching.Tag);

    }
    #endregion

    private void WorkModalityGet(Object myObject, ElapsedEventArgs myEventArgs)
    {
      Cmd_OM_WorkModalityGet();
    }

    #region XML COMMANDS
    #region OM_Order_GetAll_Paged
    private void Cmd_OM_Order_GetAll_Paged()
    {
      OrderL.Clear();

      Filter.Reset();
      C200318_State state = C200318_State.LEXEC;
      Filter.StateSelected_Set(state);

      CommandManagerC200318.OM_Order_GetAll_Paged(this, Filter);
    }
    private async void OM_Order_GetAll_Paged(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
     
      var orderL = dwr.Data as List<C200318_Order>;
      if (orderL != null)
      {
        orderL = orderL.Take(Filter.MaxItems).ToList();
        OrderL.AddRange(orderL);
      }

      // prepare the view
      var view = new CtrlOrdersExecLTAInfo(OrderL, WorkModality.Current);

      //show the dialog
      await DialogHost.Show(view, "RootDialogWithoutExit");

      //OK, request new config....
      if (view.Result == C200318_WorkModalitySwitchRequestOption.OK)
        Cmd_OM_WorkModalitySet(WorkModalityToRequest);

    }
    #endregion

    private void Cmd_OM_WorkModalityGet()
    {
      CommandManagerC200318.OM_WorkModalityGet(this);
    }
    private void OM_WorkModalityGet(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;

      switchingTimer.Stop();

      var rc = dwr.Data as C200318_WorkModalityConfiguration;
      //WorkModality.Update(rc);
      if (rc != null)
        WorkModality = rc;
      //NotifyPropertyChanged("WorkModality");

      if (WorkModality.IsSwitching)
      {
        switchingTimer.Start();

        stepper.IsLinear = false;
        stepper.SelectedIndex = step3_Switching.TabIndex;
        stepper.IsLinear = true;
        LocalSnackbar.ShowMessageInfo("WORK MODALITY IS SWITCHING".TD(), 3);
      }
      else if  (stepper.SelectedIndex == step3_Switching.TabIndex)
      {
        stepper.IsLinear = false;
        stepper.SelectedIndex = step1_Current.TabIndex;
        stepper.IsLinear = true;
        LocalSnackbar.ShowMessageInfo("WORK MODALITY IS SWITCHED".TD(), 3);
      }
    }
    
    private void Cmd_OM_WorkModalityForce(C200318_WorkModality rc)
    {
      CommandManagerC200318.OM_WorkModalityForce(this, rc);
    }
    private void OM_WorkModalityForce(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;

      C200318_CustomResult Result = (C200318_CustomResult)dwr.Data;

      LocalSnackbar.ShowMessageOk("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(Result), 2);

      if (Result == C200318_CustomResult.WORKMODALITY_SWITCH_MODE_DONE)
      {
        stepper.IsLinear = false;
        stepper.SelectedIndex = step1_Current.TabIndex;
        stepper.IsLinear = true;
      }

      Cmd_OM_WorkModalityGet();

    }
   
    private void Cmd_OM_WorkModalitySet(C200318_WorkModality rc)
    {
      CommandManagerC200318.OM_WorkModalitySet(this, rc);
    }
    private void OM_WorkModalitySet(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C200318_CustomResult Result = (C200318_CustomResult)dwr.Data;


      LocalSnackbar.ShowMessageOk("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(Result), 2);

      if (Result == C200318_CustomResult.WORKMODALITY_SWITCH_MODE_SWITCHING)
      {
        stepper.IsLinear = false;
        stepper.SelectedIndex = step3_Switching.TabIndex;
        stepper.IsLinear = true;
      }
      else if (Result == C200318_CustomResult.WORKMODALITY_SWITCH_MODE_DONE)
      {
        stepper.IsLinear = false;
        stepper.SelectedIndex = step1_Current.TabIndex;
        stepper.IsLinear = true;
      }

      Cmd_OM_WorkModalityGet();
    }

    #endregion

    #region Event button
    private void butForce_Click(object sender, RoutedEventArgs e)
    {
      Cmd_OM_WorkModalityForce(WorkModalityToRequest);
    }

    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      Cmd_OM_WorkModalityGet();
    }

    private void butSet_Click(object sender, RoutedEventArgs e)
    {
      Cmd_OM_WorkModalitySet(WorkModalityToRequest);
    }

    private void cbNewConf_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      var cbi = cmbBoxNewConfiguration.SelectedItem as ComboBoxItem;
      if (cbi != null)
      {
        WorkModalityToRequest = cbi.Content.ToString().ConvertTo<C200318_WorkModality>();
      }

      IsSelectionValid = (WorkModalityToRequest != WorkModality.Current);
    }

    #endregion

    #region Event User Control
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();

      Cmd_OM_WorkModalityGet();
    }

    private void CtrlBaseC200318_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      if (!IsVisible)
        switchingTimer.Stop();
    }

    private void stepper_Loaded(object sender, RoutedEventArgs e)
    {
      Cmd_OM_WorkModalityGet();
    }
    #endregion

    #region Switch actions
    private void btnSET_step2_Switch_Click(object sender, RoutedEventArgs e)
    {
      //check if current configuration 
      if (WorkModalityToRequest != WorkModality.Current)
      {
        //already switching?
        if (!WorkModality.IsSwitching)
        {
          //OK, request new config....
          // Cmd_OM_WorkModalitySet(WorkModalityToRequest);

          //request all order executing before switching confirmation
          Cmd_OM_Order_GetAll_Paged();

        }
        else
        {
          //already switching....update values - snackbar error
          Cmd_OM_WorkModalityGet();
        }
      }
      else
      {
        // selected configuration not valid - snackbar red
        Cmd_OM_WorkModalityGet();
      }
    }

    private void btnCANCEL_step2_Switch_Click(object sender, RoutedEventArgs e)
    {
      stepper.IsLinear = false;
      stepper.SelectedIndex = step1_Current.TabIndex;
      stepper.IsLinear = true;
    }

    private void btnFORCE_step2_Switch_Click(object sender, RoutedEventArgs e)
    {
      //OK, force new config....
      Cmd_OM_WorkModalityForce(WorkModalityToRequest);
    }

    private void btnFORCE_step3_Switching_Click(object sender, RoutedEventArgs e)
    {
      //OK, force new config....
      Cmd_OM_WorkModalityForce(WorkModalityToRequest);
    }

    #endregion

    private void StepButtonBar_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      if (!WorkModality.IsSwitching)
      {
        if (WorkModality.Current == C200318_WorkModality.EMERGENCIAL)
          WorkModalityToRequest = C200318_WorkModality.NORMAL;
        else if (WorkModality.Current == C200318_WorkModality.NORMAL)
          WorkModalityToRequest = C200318_WorkModality.EMERGENCIAL;
      }
    }
  }

}
