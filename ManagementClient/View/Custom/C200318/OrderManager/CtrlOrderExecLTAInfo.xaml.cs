﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200318.OrderManager;
using View.Common.Languages;

namespace View.Custom.C200318.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderExecLTAInfo.xaml
  /// </summary>
  public partial class CtrlOrderExecLTAInfo : CtrlBaseC200318
  {
    public C200318_WorkModalitySwitchRequestOption Result { get; set; }



    public C200318_Order Order
    {
      get { return (C200318_Order)GetValue(OrderProperty); }
      set { SetValue(OrderProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Order.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty OrderProperty =
        DependencyProperty.Register("Order", typeof(C200318_Order), typeof(CtrlOrderExecLTAInfo), new PropertyMetadata(null));



    #region CONSTRUCTOR
    public CtrlOrderExecLTAInfo()
    {
    
      WordDicAdd("PALLET_IN_TRANSIT", "PALLET IN TRANSIT");
      WordDicAdd("PALLET_IN_TRANSIT_COUNT", "PALLET IN TRANSIT COUNT");
      WordDicAdd("COPY", "COPY");
      WordDicAdd("PALLET_CODE", "PALLET CODE");
      WordDicAdd("ARTICLE_CODE", "ARTICLE CODE");
      WordDicAdd("LOT_CODE", "LOT CODE");
      WordDicAdd("ARTICLE_DESCR", "ARTICLE DESCRIPTION");
      WordDicAdd("POSITION_CODE", "POSITION CODE");
      WordDicAdd("DESTINATION", "DESTINATION");

      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkNumberOfPallets.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfPallets.Tag);

      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colCopy.Header = Localization.Localize.LocalizeDefaultString("COPY");
      colDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colLotCode.Header = Localization.Localize.LocalizeDefaultString("LOT CODE");
      colDestination.Header = Localization.Localize.LocalizeDefaultString("DESTINATION");
      colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
      colPosition.Header = Localization.Localize.LocalizeDefaultString("POSITION");

      if (dgOrders.ItemsSource != null)
        CollectionViewSource.GetDefaultView(dgOrders.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {

      Translate();

      foreach (var kvp in WordDic)
      {
        kvp.Value.Localized = kvp.Value.DefaultVal.TD();
      }
    }
    #endregion

    #region TRADUZIONI
    public Dictionary<string, Localization.Word> WordDic { get; set; } = new Dictionary<string, Localization.Word>();

    private void WordDicAdd(string key, string defaultVal)
    {
      if (WordDic.ContainsKey(key))
        return;
      WordDic.Add(key, new Localization.Word(key, defaultVal, defaultVal.TD()));
    }
   
    #endregion

    #region EVENTI
    private void butCopyPalletCode_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        Button b = sender as Button;
        if (b == null)
          return;
        Clipboard.SetText(b.Tag as string);

      }
      catch
      {
      }
    }

    #endregion

  

  }
}
