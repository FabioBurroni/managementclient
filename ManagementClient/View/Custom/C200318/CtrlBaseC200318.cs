﻿using Configuration;
using XmlCommunicationManager.XmlClient.Event;
using Utilities.Extensions;
using Model.Custom.C200318;
using Model;
using View.Common;

namespace View.Custom.C200318
{
	public class CtrlBaseC200318 : CtrlBase
	{
		
		#region Constructor

		public CtrlBaseC200318()
		{
			// Check for design mode. 
			if (DesignTimeHelper.IsInDesignMode)
				return;

			InitCommandManager();
		}

    #endregion
    

    #region Private Properties

    protected CommandManagerC200318 CommandManagerC200318 { get; private set; }

		#endregion

		#region Init CommandManager

		private void InitCommandManager()
		{
      CommandManagerC200318 = new CommandManagerC200318(Context.XmlClient, ConfigurationManager.Parameters.CommandPreambleSetting.GetPreamble("custom"));
		}

		#endregion

		#region Override

		protected override void XmlClientOnException(object sender, ExceptionEventArgs e)
		{
			if (ManageXmlClientException(CommandManagerC200318, e))
				return;

			base.XmlClientOnException(sender, e);
		}

		protected override void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs e)
		{
			if (ManageXmlClientReply(CommandManagerC200318, e))
				return;

			base.XmlClientOnRetrieveReplyFromServer(sender, e);
		}

		#endregion

  
  }
}