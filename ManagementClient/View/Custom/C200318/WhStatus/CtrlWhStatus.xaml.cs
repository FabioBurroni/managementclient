﻿using System;
using System.Windows;
using Model;
using Model.Custom.C200318.WhStatus;

namespace View.Custom.C200318.WhStatus
{
  /// <summary>
  /// Interaction logic for CtrlWhStatus.xaml
  /// </summary>
  public partial class CtrlWhStatus : CtrlBaseC200318
  {
    public string Title
    {
      get { return (string)GetValue(TitleProperty); }
      set { SetValue(TitleProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty TitleProperty =
        DependencyProperty.Register("Title", typeof(string), typeof(CtrlWhStatus), new PropertyMetadata(""));

    public Func<double, string> Formatter = value => (value + "%").ToString();

    #region DP - WhStatus
    public C200318_WhStatus WhStatus
    {
      get { return (C200318_WhStatus)GetValue(WhStatusProperty); }
      set { SetValue(WhStatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for WhStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty WhStatusProperty =
        DependencyProperty.Register("WhStatus", typeof(C200318_WhStatus), typeof(CtrlWhStatus), new PropertyMetadata(null));

    #endregion

    #region DP - MasterStatus
    public C200318_MasterStatus MasterStatus
    {
      get { return (C200318_MasterStatus)GetValue(MasterStatusProperty); }
      set { SetValue(MasterStatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for MasterStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty MasterStatusProperty =
        DependencyProperty.Register("MasterStatus", typeof(C200318_MasterStatus), typeof(CtrlWhStatus), new PropertyMetadata(null));
    #endregion

    #region DP - Satellite1Status
    public C200318_SlaveStatus Slave1Status
    {
      get { return (C200318_SlaveStatus)GetValue(Slave1StatusProperty); }
      set { SetValue(Slave1StatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for SatelliteStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty Slave1StatusProperty =
        DependencyProperty.Register("Slave1Status", typeof(C200318_SlaveStatus), typeof(CtrlWhStatus), new PropertyMetadata(null));
    #endregion

    #region DP - Satellite2Status
    public C200318_SlaveStatus Slave2Status
    {
      get { return (C200318_SlaveStatus)GetValue(Slave2StatusProperty); }
      set { SetValue(Slave2StatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for SatelliteStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty Slave2StatusProperty =
        DependencyProperty.Register("Slave2Status", typeof(C200318_SlaveStatus), typeof(CtrlWhStatus), new PropertyMetadata(null));
    #endregion

    #region COSTRUTTORE
    public CtrlWhStatus()
    {
      InitializeComponent();

      gaugeBlkBlocked.LabelFormatter = Formatter;
      gaugeBlkCompleted.LabelFormatter = Formatter;
      gaugeBlkEmpty.LabelFormatter = Formatter;
      gaugeBlkFilling.LabelFormatter = Formatter;
      gaugeBlkNotCompleted.LabelFormatter = Formatter;
    }
    #endregion
    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkBlocked.Text = Context.Instance.TranslateDefault((string)txtBlkBlocked.Tag);
      txtBlkCompleted.Text = Context.Instance.TranslateDefault((string)txtBlkCompleted.Tag);
      txtBlkEmpty.Text = Context.Instance.TranslateDefault((string)txtBlkEmpty.Tag);
      txtBlkMaster.Text = Context.Instance.TranslateDefault((string)txtBlkMaster.Tag);
      txtBlkNotCompleted.Text = Context.Instance.TranslateDefault((string)txtBlkNotCompleted.Tag);
      txtBlkSlave1.Text = Context.Instance.TranslateDefault((string)txtBlkSlave1.Tag);
      txtBlkSlave2.Text = Context.Instance.TranslateDefault((string)txtBlkSlave2.Tag);
      txtBlkFilling.Text = Context.Instance.TranslateDefault((string)txtBlkFilling.Tag);
    }
    #endregion

    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
  }
}
