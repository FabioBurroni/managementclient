﻿using System;
using System.Linq;
using Model.Attributes;
using Model;
using XmlCommunicationManager.Common.Xml;
using System.Collections.Generic;
using System.Windows;
using Model.Custom.C200318;
using Configuration;
using Utilities.Extensions;
using System.Windows.Threading;

namespace View.Custom.C200318.OutPosition
{
  /// <summary>
  /// Interaction logic for CtrlExitPosition.xaml
  /// </summary>
  public partial class CtrlOutPosition : CtrlBaseC200318
  {
    private bool refreshAvailable;

    public bool RefreshAvailable
    {
      get { return refreshAvailable; }
      set { refreshAvailable = value;
        NotifyPropertyChanged("RefreshAvailable");
      }
    }


    #region DP - TooManyPalletInPos

    public bool TooManyPalletInPos
    {
      get { return (bool)GetValue(TooManyPalletInPosProperty); }
      set { SetValue(TooManyPalletInPosProperty, value); }
    }

    // Using a DependencyProperty as the backing store for ToManyPalletInPos.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty TooManyPalletInPosProperty =
        DependencyProperty.Register("TooManyPalletInPos", typeof(bool), typeof(CtrlOutPosition), new PropertyMetadata(false));

    #endregion DP - TooManyPalletInPos

    #region DP - PalletInPos

    public C200318_PositionOut_Pallet PalletInPos
    {
      get { return (C200318_PositionOut_Pallet)GetValue(PalletInPosProperty); }
      set { SetValue(PalletInPosProperty, value); }
    }

    // Using a DependencyProperty as the backing store for PalletInPos.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty PalletInPosProperty =
        DependencyProperty.Register("PalletInPos", typeof(C200318_PositionOut_Pallet), typeof(CtrlOutPosition), new PropertyMetadata(null));

    #endregion DP - PalletInPos

    #region DP - PosCode

    public string PosCode
    {
      get { return (string)GetValue(PosCodeProperty); }
      set { SetValue(PosCodeProperty, value); }
    }

    // Using a DependencyProperty as the backing store for PosCode.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty PosCodeProperty = DependencyProperty.Register("PosCode", typeof(string), typeof(CtrlOutPosition), new PropertyMetadata(null));

    #endregion DP - PosCode

    #region Costruttore

    public CtrlOutPosition()
    {
      InitializeComponent();
      Cmd_EP_Get_ExitPositionCodes();
            //...Timer will update the output display every 5 seconds
            int interval = 5;//seconds
            var timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, interval);
            timer.Start();
      Translate();
    }

    #endregion Costruttore

    #region TRADUZIONI

    protected override void Translate()
    {
      labTitle.Text = Context.Instance.TranslateDefault((string)labTitle.Tag);
      //txtBlkBoxCode.Text = Context.Instance.TranslateDefault((string)txtBlkBoxCode.Tag);
      icon_code_1.ToolTip = Context.Instance.TranslateDefault((string)icon_code_1.Tag);
      icon_weight_1.ToolTip = Context.Instance.TranslateDefault((string)icon_weight_1.Tag);
      icon_article_1.ToolTip = Context.Instance.TranslateDefault((string)icon_article_1.Tag);
      iconCustomResult.ToolTip = Context.Instance.TranslateDefault((string)iconCustomResult.Tag);
      iconRejectResult.ToolTip = Context.Instance.TranslateDefault((string)iconRejectResult.Tag);

      txtBlkLotCode.Text = Context.Instance.TranslateDefault((string)txtBlkLotCode.Tag);
      txtBlkLotLength.Text = Context.Instance.TranslateDefault((string)txtBlkLotLength.Tag);
      txtBlkOrderCode.Text = Context.Instance.TranslateDefault((string)txtBlkOrderCode.Tag);
      txtBlkDestination.Text = Context.Instance.TranslateDefault((string)txtBlkDestination.Tag);
      txtBlkArticleCode.Text = Context.Instance.TranslateDefault((string)txtBlkArticleCode.Tag);
      txtBlkQuantityRequested.Text = Context.Instance.TranslateDefault((string)txtBlkQuantityRequested.Tag);
      txtBlkQuantityDelivered.Text = Context.Instance.TranslateDefault((string)txtBlkQuantityDelivered.Tag);
      txtBlkOrderLotCode.Text = Context.Instance.TranslateDefault((string)txtBlkOrderLotCode.Tag);
      txtBlkArticleDescription.Text = Context.Instance.TranslateDefault((string)txtBlkArticleDescription.Tag);
      txtBlkTooManyPalletInPosition.Text = Context.Instance.TranslateDefault((string)txtBlkTooManyPalletInPosition.Tag);
      txtBlkNoPalletInPosition.Text = Context.Instance.TranslateDefault((string)txtBlkNoPalletInPosition.Tag);
      txtBlkNoAssociatedOrder.Text = Context.Instance.TranslateDefault((string)txtBlkNoAssociatedOrder.Tag);
    }

    #endregion TRADUZIONI

    #region NOTIFICHE

    /// <summary>
    /// Cambio pallet in posizione
    /// </summary>
    /// <param name="xmlcmd"></param>
    [XmlNotification(BringToFront = false)]
    public void Notify_Pallet_OnPosition_Changed(XmlCommand xmlcmd)
    {
      Console.WriteLine(xmlcmd);

      //get parameters...
      var exitPositionPanelSettings = ConfigurationManager.Parameters.Layout.Where(x => x.Title == "Exit Position Panel").FirstOrDefault();
      bool enableAutoRefresh = exitPositionPanelSettings.Settings.Where(x => x.Name == "ExitPositionAutoRefresh").FirstOrDefault().Value.ConvertToBool();

      if (enableAutoRefresh || PalletInPos is null)
        Cmd_EP_Get_PalletInPos();
      else
        RefreshAvailable = true;      
    }

    #endregion NOTIFICHE

    #region COMANDI E RISPOSTE

    private void Cmd_EP_Get_ExitPositionCodes()
    {
      CommandManagerC200318.EP_Get_ExitPositionCodes(this, Context.Instance.Positions);
    }

    private void EP_Get_ExitPositionCodes(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var positionExitList = dwr.Data as List<string>;
      PosCode = positionExitList.FirstOrDefault();
      Refresh();
    }

    private void Cmd_EP_Get_PalletInPos()
    {
      if (PosCode != null)
      {
        CommandManagerC200318.EP_Get_PalletInPos(this, PosCode);
      }
    }

    private void EP_Get_PalletInPos(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var list = dwr.Data as List<C200318_PositionOut_Pallet>;
      TooManyPalletInPos = list.Count > 1;
           
      PalletInPos = list.FirstOrDefault();

      iconCustomResult.Visibility = Visibility.Visible;
      txtBlkCustomResult.Visibility = Visibility.Visible;
      imgCustomResult.Visibility = Visibility.Visible;

      iconRejectResult.Visibility = Visibility.Hidden;
      txtBlkRejectResult.Visibility = Visibility.Hidden;
      imgRejectResult.Visibility = Visibility.Hidden;

      imgServiceResult.Visibility = Visibility.Hidden;

      if (PalletInPos != null)
      {
        if (PalletInPos.IsService)
        {
          iconCustomResult.Visibility = Visibility.Hidden;
          txtBlkCustomResult.Visibility = Visibility.Hidden;
          imgCustomResult.Visibility = Visibility.Hidden;

          iconRejectResult.Visibility = Visibility.Hidden;
          txtBlkRejectResult.Visibility = Visibility.Hidden;
          imgRejectResult.Visibility = Visibility.Hidden;

          imgServiceResult.Visibility = Visibility.Visible;
        }
        else
        {
          if(PalletInPos.IsReject)
          {
            iconCustomResult.Visibility   = Visibility.Hidden;
            txtBlkCustomResult.Visibility = Visibility.Hidden;
            imgCustomResult.Visibility    = Visibility.Hidden;

            iconRejectResult.Visibility   = Visibility.Visible;
            txtBlkRejectResult.Visibility = Visibility.Visible;
            imgRejectResult.Visibility    = Visibility.Visible;
          }
          else
          {
            if(PalletInPos.IsCustomReject)
            {
              iconCustomResult.Visibility = Visibility.Visible;
              txtBlkCustomResult.Visibility = Visibility.Visible;
              imgCustomResult.Visibility = Visibility.Visible;

              iconRejectResult.Visibility = Visibility.Hidden;
              txtBlkRejectResult.Visibility = Visibility.Hidden;
              imgRejectResult.Visibility = Visibility.Hidden;
            }
          }



        }

      }

      RefreshAvailable = false;
    }

    #endregion COMANDI E RISPOSTE

    private void Refresh()
    {
      Cmd_EP_Get_PalletInPos();
    }

    private void timer_Tick(object sender, EventArgs e)
    {
        if (PosCode == null)
            Cmd_EP_Get_ExitPositionCodes();
        Refresh();
    }

    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();

      if (PosCode == null)
        Cmd_EP_Get_ExitPositionCodes();
      else
        Refresh();
    }

    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      Refresh();
    }
  }
}