﻿using System.Windows;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Common;
using Model.Custom.C200318.ArticleManager;
using View.Common.Languages;

namespace View.Custom.C200318.SKUManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderLTA.xaml
  /// </summary>
  public partial class CtrlSKUManagerInsert : CtrlBaseC200318
  {
    public ShowDialogResults Result { get; set; }

    private C200318_Article _Article;

    public C200318_Article Article
    {
      get { return _Article; }
      set
      {
        _Article = value;
        NotifyPropertyChanged("Article");

      }
    }

    #region CONSTRUCTOR
    public CtrlSKUManagerInsert()
    {
      Article = new C200318_Article();

      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkSubTitle.Text = Context.Instance.TranslateDefault((string)txtBlkSubTitle.Tag);

      txtBlkArticleCode.Text = Context.Instance.TranslateDefault((string)txtBlkArticleCode.Tag);
      txtBlkArticleDescription.Text = Context.Instance.TranslateDefault((string)txtBlkArticleDescription.Tag);
      txtBlkUnity.Text = Context.Instance.TranslateDefault((string)txtBlkUnity.Tag);
      txtBlkType.Text = Context.Instance.TranslateDefault((string)txtBlkType.Tag);

      txtBlkCancel.Text = Context.Instance.TranslateDefault((string)txtBlkCancel.Tag);
      txtBlkConfirm.Text = Context.Instance.TranslateDefault((string)txtBlkConfirm.Tag);

    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC200318_Loaded(object sender, RoutedEventArgs e)
    {

      Translate();

    }
    #endregion

  

    #region EVENTI
 
    private void butCancel_Click(object sender, RoutedEventArgs e)
    {
      CloseWithResult(ShowDialogResults.CANCELED);
    }

    private async void butConfirm_Click(object sender, RoutedEventArgs e)
    {
      if (Article.Code == "" || Article.Code is null)
      {
        LocalSnackbar.ShowMessageFail("PLEASE INPUT A VALID ARTICLE CODE".TD(), 3);
      }
      else if (Article.Descr == "" || Article.Descr is null)
      {
        LocalSnackbar.ShowMessageFail("PLEASE INPUT A VALID ARTICLE DESCRIPTION".TD(), 3);
      }
      else if (Article.Unit == "" || Article.Unit is null)
      {
        LocalSnackbar.ShowMessageFail("PLEASE INPUT A VALID ARTICLE UNIT".TD(), 3);        
      }
      else if (Article.Type == "" || Article.Type is null)
      {
        LocalSnackbar.ShowMessageFail("PLEASE INPUT A VALID ARTICLE TYPE".TD(), 3);

      }
      else
      {
        ConfirmationDialogArguments dialogArgs = new ConfirmationDialogArguments
        {
          Title = "UPDATE ARTICLE CONFIRM".TD(),
          Message = "Are you sure?".TD(),
          OkButtonLabel = "YES".TD(),
          CancelButtonLabel = "CANCEL".TD(),
          StackedButtons = false
        };

        bool result = await ConfirmationDialog.ShowDialogAsync("RootDialog", dialogArgs); 
        
        if (result)
          CloseWithResult(ShowDialogResults.OK);
        else
          CloseWithResult(ShowDialogResults.CANCELED);
      }

    }
    #endregion

    #region Private Methods
   
    
    void CloseWithResult(ShowDialogResults res)
    {
      Result = res;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }
    #endregion

  }
}
