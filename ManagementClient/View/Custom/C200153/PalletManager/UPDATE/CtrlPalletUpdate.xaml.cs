﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.ArticleManager;
using Model.Custom.C200153.Depositor;
using Model.Custom.C200153.PalletManager;
using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;
using View.Common.Languages;
using View.Custom.C200153.Converter;

namespace View.Custom.C200153.PalletManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletUpdate.xaml
  /// </summary>
  public partial class CtrlPalletUpdate : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public int StartIndex { get; set; } = _indexStartValue;
    public int MaxItems { get; set; } = _maxItemsStartValue;

    #endregion


    private C200153_PalletInsert _Pallet;

    public C200153_PalletInsert Pallet
    {
      get { return _Pallet; }
      set {
        _Pallet = value;
        NotifyPropertyChanged("Pallet");
      }
    }


    private string posCode;

    public string PosCode
    {
      get { return posCode; }
      set
      {
        posCode = value;
        NotifyPropertyChanged("PosCode");
      }
    }



    #region COSTRUTTORE
    public CtrlPalletUpdate()
    {
      InitializeComponent();

      Pallet = new C200153_PalletInsert();

      iconResultPallet.Visibility = Visibility.Hidden;
      iconResultArticle.Visibility = Visibility.Hidden;
      iconResultBatch.Visibility = Visibility.Hidden;
      iconResultDepositor.Visibility = Visibility.Hidden;
      iconResultExpiryDate.Visibility = Visibility.Hidden;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      // ricarichiamo gli helper e gli hint assist
      Pallet = new C200153_PalletInsert();

      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkDescription.Text = Context.Instance.TranslateDefault((string)txtBlkDescription.Tag);

      txtBlkPalletCode.Text = Context.Instance.TranslateDefault((string)txtBlkPalletCode.Tag);   
      btnSearchPallet.ToolTip = Context.Instance.TranslateDefault((string)btnSearchPallet.Tag);

      txtBlkArticleCode.Text = Context.Instance.TranslateDefault((string)txtBlkArticleCode.Tag);
      btnSearchArticle.ToolTip = Context.Instance.TranslateDefault((string)btnSearchArticle.Tag);
      txtBlkBatchCode.Text = Context.Instance.TranslateDefault((string)txtBlkBatchCode.Tag);
      txtBoxBatchCodeHint.Text = Context.Instance.TranslateDefault((string)txtBoxBatchCodeHint.Tag);
      txtBlkDepositorCode.Text = Context.Instance.TranslateDefault((string)txtBlkDepositorCode.Tag);
      txtBoxDepositorCodeHint.Text = Context.Instance.TranslateDefault((string)txtBoxDepositorCodeHint.Tag);
      txtBlkExpiryDate.Text = Context.Instance.TranslateDefault((string)txtBlkExpiryDate.Tag);

      txtBlkIsWrapped.Text = Context.Instance.TranslateDefault((string)txtBlkIsWrapped.Tag);
      txtBlkLabeled.Text = Context.Instance.TranslateDefault((string)txtBlkLabeled.Tag);

      txtBlkRegister.Text = Context.Instance.TranslateDefault((string)txtBlkRegister.Tag);

      btnRegister.ToolTip = Context.Instance.TranslateDefault((string)btnRegister.Tag);

    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE

    private void Cmd_PM_Get_CheckInCodes()
    {
      CommandManagerC200153.PM_Get_CheckInCodes(this, Context.Instance.Positions);
    }

    private void PM_Get_CheckInCodes(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var positionEntryList = dwr.Data as List<string>;
      PosCode = positionEntryList.FirstOrDefault();
    }

    private void Cmd_PM_Pallet_Update()
    {
      if (PosCode != null)
        CommandManagerC200153.PM_Pallet_Update(this, Pallet);
      else
        Snackbar.ShowMessageFail("CLIENT IS NOT ASSOCIATED TO CHECKIN POSITION".TD(), 3);
      
    }

    private async void PM_Pallet_Update(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Result == null) return;
      C200153_CustomResult result = dwr.Result.ConvertTo<C200153_CustomResult>();

      if (dwr.Data == null)
      {
        Snackbar.ShowMessageFail("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(result), 3);
        return;
      }

      string palletCode = dwr.Data.ToString();

      if (result == C200153_CustomResult.OK)
      {
        Pallet.Code = palletCode;

        var view = new CtrlPalletCheckInConfirmation("PALLET UPDATED".TD(), palletCode);

        //show the dialog
        await DialogHost.Show(view, "RootDialog");

        C200153_CustomResult ConfirmationResult = view.Result;
        if (ConfirmationResult == C200153_CustomResult.OK)
        {
          Cmd_PM_Pallet_CheckIn();
        }
        else
        {
          //Snackbar.ShowMessageFail("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(ConfirmationResult), 3);
          Pallet = new C200153_PalletInsert();
        }
      
      }
      else
      {
        Snackbar.ShowMessageFail("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(result), 3);

      }
      
      
    }

    private void Cmd_PM_Pallet_CheckIn()
    {
      CommandManagerC200153.PM_Pallet_CheckIn(this, Context.Instance.Position, Pallet.Code);
    }
    private void PM_Pallet_CheckIn(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null)
      {
        Snackbar.ShowMessageFail("CHECK IN".TD() + " " + Pallet.Code + " : " + CustomResultToLocalizedStringConverter.Convert(C200153_CustomResult.UNDEFINED), 3);
      }
      else if (dwr.Data == null)
      {
        Snackbar.ShowMessageFail("CHECK IN".TD() + " " + Pallet.Code + " : " + CustomResultToLocalizedStringConverter.Convert(C200153_CustomResult.UNDEFINED), 3);
      }
      else
      {
        C200153_CustomResult CheckInResult = (C200153_CustomResult)dwr.Data;

        if (CheckInResult == C200153_CustomResult.OK)
          Snackbar.ShowMessageOk("CHECK IN".TD() + " " + Pallet.Code + " : " + CustomResultToLocalizedStringConverter.Convert(CheckInResult), 3);
        else
          Snackbar.ShowMessageFail("CHECK IN".TD() + " " + Pallet.Code + " : " + CustomResultToLocalizedStringConverter.Convert(CheckInResult), 3);
      }

      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    private void Cmd_PM_Check_Pallet()
    {
      CommandManagerC200153.PM_Check_Pallet(this, Pallet.Code);
    }

    private void PM_Check_Pallet(IList<string> commandMethodParameters, IModel model)
    {
      iconResultPallet.Visibility = Visibility.Visible;
      iconResultPallet.Kind = PackIconKind.QuestionMark;
      iconResultPallet.Foreground = Brushes.DarkRed;

      String txtBlkHelper = string.Empty;
      HintAssist.SetHelperText(txtBoxPalletCode, txtBlkHelper);
    
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Result == null) return;

      // get result
      C200153_CustomResult result = dwr.Result.ConvertTo<C200153_CustomResult>();
      txtBlkHelper = CustomResultToLocalizedStringConverter.Convert(result).ToString(); 
      HintAssist.SetHelperText(txtBoxPalletCode, txtBlkHelper);

      iconResultPallet.Visibility = Visibility.Visible;
      iconResultPallet.Kind = PackIconKind.ErrorOutline;
      iconResultPallet.Foreground = Brushes.Red;

      if (dwr.Data == null) return;

      C200153_PalletInsert pallet = (C200153_PalletInsert)dwr.Data;

      if (result == C200153_CustomResult.PALLET_ALREADY_PRESENT_IN_DB )
      {
        iconResultPallet.Visibility = Visibility.Visible;
        iconResultPallet.Kind = PackIconKind.Check;
        iconResultPallet.Foreground = Brushes.Green;

        txtBlkHelper = String.Empty;
        HintAssist.SetHelperText(txtBoxPalletCode, txtBlkHelper);

        Pallet = pallet;

        btnRegister.IsEnabled = true;
      }

    }


    private void Cmd_PM_Article_CheckPresence()
    {
      CommandManagerC200153.PM_Article_CheckPresence(this, Pallet.ArticleCode);
    }

    private void PM_Article_CheckPresence(IList<string> commandMethodParameters, IModel model)
    {
      iconResultArticle.Visibility = Visibility.Visible;
      iconResultArticle.Kind = PackIconKind.QuestionMark;
      iconResultArticle.Foreground = Brushes.DarkRed;

      String txtBlkHelper = "ARTICLE NOT FOUND".TD();
      HintAssist.SetHelperText(txtBoxArticleCode, txtBlkHelper);

      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C200153_Article Result = (C200153_Article)dwr.Data;

      iconResultArticle.Visibility = Visibility.Visible;
      iconResultArticle.Kind = PackIconKind.Check;
      iconResultArticle.Foreground = Brushes.Green;

      txtBlkHelper = Result.Descr;
      HintAssist.SetHelperText(txtBoxArticleCode, txtBlkHelper);

    }

    private void Cmd_PM_Depositor_CheckPresence()
    {
      CommandManagerC200153.PM_Depositor_CheckPresence(this, Pallet.DepositorCode);
    }

    private void PM_Depositor_CheckPresence(IList<string> commandMethodParameters, IModel model)
    {
      iconResultDepositor.Visibility = Visibility.Visible;
      iconResultDepositor.Kind = PackIconKind.QuestionMark;
      iconResultDepositor.Foreground = Brushes.DarkRed;

      String txtBlkHelper = "DEPOSITOR NOT FOUND".TD();
      HintAssist.SetHelperText(txtBoxDepositorCode, txtBlkHelper);

      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C200153_Depositor Result = (C200153_Depositor)dwr.Data;

      iconResultDepositor.Visibility = Visibility.Visible;
      iconResultDepositor.Kind = PackIconKind.Check;
      iconResultDepositor.Foreground = Brushes.Green;

      txtBlkHelper = Result.Address;
      HintAssist.SetHelperText(txtBoxDepositorCode, txtBlkHelper);

    }



    #endregion

    #region Eventi

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();

      if (PosCode == null)
        Cmd_PM_Get_CheckInCodes();
    }

    private void CtrlBaseC200153_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    #endregion

    #endregion

    #region Eventi Pallet code 


    private void txtBoxPalletCode_LostFocus(object sender, RoutedEventArgs e)
    {
      Pallet.Code = txtBoxPalletCode.Text;

      if (txtBoxPalletCode_Validate())
        Cmd_PM_Check_Pallet();
    }

    private void txtBoxPalletCode_TextChanged(object sender, TextChangedEventArgs e)
    {
      Pallet.Code = txtBoxPalletCode.Text;
      txtBoxPalletCode_Validate();
    }


    private bool txtBoxPalletCode_Validate()
    {
      btnRegister.IsEnabled = false;
      bool isValidSintax = false;
      int code;

      txtBoxPalletCode.Background = Brushes.Transparent;
      //prepare helper elements
      iconResultPallet.Visibility = Visibility.Hidden;
      string txtBlkHelper = string.Empty;

      // check empty
      if (String.IsNullOrWhiteSpace(Pallet.Code) || String.IsNullOrEmpty(Pallet.Code))
      {
        iconResultPallet.Visibility = Visibility.Visible;
        iconResultPallet.Kind = PackIconKind.Cancel;
        iconResultPallet.Foreground = Brushes.OrangeRed;

      }
      // check pallet is a number 
      else if (!int.TryParse(Pallet.Code.Replace("0",""), out code))
      {
        iconResultPallet.Visibility = Visibility.Visible;
        iconResultPallet.Kind = PackIconKind.ErrorOutline;
        iconResultPallet.Foreground = Brushes.Red;

        txtBlkHelper = "CODE MUST BE A NUMBER".TD();
        //txtBlkHelper.Foreground = Brushes.Red;
      }
      // check number in cassioli range 
      //else if (code > 59999 || code < 10001)
      //{
      //  iconResultPallet.Visibility = Visibility.Visible;
      //  iconResultPallet.Kind = PackIconKind.ErrorOutline;
      //  iconResultPallet.Foreground = Brushes.Red;

      //  txtBlkHelper = "CODE RANGE IS FROM 10001 TO 59999".TD();
      //  //txtBlkHelper.Foreground = Brushes.Red;
      //}

      //check valid length
      else 
      {
        // la validazione è fatta dal lost focus
        isValidSintax = true;

        //iconResultPallet.Visibility = Visibility.Visible;
        //iconResultPallet.Kind = PackIconKind.Check;
        //iconResultPallet.Foreground = Brushes.Green;

        //txtBlkHelper.Text = "VALID".TD();
        //txtBlkHelper.Foreground = Brushes.Red;
      }

      //set helper
      HintAssist.SetHelperText(txtBoxPalletCode, txtBlkHelper);

      return isValidSintax;
    }

    private async void btnSearchPallet_Click(object sender, RoutedEventArgs e)
    {
      // open article ctrl
      // prepare the view
      var view = new CtrlPalletUpdateAll();

      //show the dialog
      await DialogHost.Show(view, "RootDialog");


      if (view.PalletSelected != null)
      {

        C200153_PalletInsert PalletSelected = new C200153_PalletInsert()
        {
          Code = view.PalletSelected.PalletCode,
          ArticleCode = view.PalletSelected.ArticleCode,
          PalletTypeId = view.PalletSelected.PalletTypeId,
          ExpiryDate = view.PalletSelected.ProductionDate,
          BatchCode = view.PalletSelected.BatchCode,
          DepositorCode = view.PalletSelected.DepositorCode,
          IsWrapped = view.PalletSelected.IsWrapped,
          IsLabeled = view.PalletSelected.IsLabeled
        }

          ;

        txtBoxPalletCode.Text = PalletSelected?.Code;

        Pallet.Code = PalletSelected.Code;
        if (txtBoxPalletCode_Validate())
          Cmd_PM_Check_Pallet();
        else
        {
          txtBoxPalletCode.Background = Brushes.Transparent;
        }
      }

    }

    #endregion

    #region Eventi Article 

    private void txtBoxArticleCode_LostFocus(object sender, RoutedEventArgs e)
    {
      Pallet.ArticleCode = txtBoxArticleCode.Text;

      if (txtBoxArticleCode_Validate())
        Cmd_PM_Article_CheckPresence();
    }

    private void txtBoxArticleCode_TextChanged(object sender, TextChangedEventArgs e)
    {
      Pallet.ArticleCode = txtBoxArticleCode.Text;
      txtBoxArticleCode_Validate();
    }


    private bool txtBoxArticleCode_Validate()
    {
      bool isValidSintax = false;

      txtBoxArticleCode.Background = Brushes.Transparent;

      //prepare helper elements
      iconResultArticle.Visibility = Visibility.Hidden;
      string txtBlkHelper = string.Empty;

      // check empty
      if (String.IsNullOrWhiteSpace(Pallet.ArticleCode) || String.IsNullOrEmpty(Pallet.ArticleCode))
      {
        iconResultArticle.Visibility = Visibility.Visible;
        iconResultArticle.Kind = PackIconKind.Cancel;
        iconResultArticle.Foreground = Brushes.OrangeRed;

      }

      // check length over limit
      else if (Pallet.ArticleCode.Length > 10)
      {
        iconResultArticle.Visibility = Visibility.Visible;
        iconResultArticle.Kind = PackIconKind.ErrorOutline;
        iconResultArticle.Foreground = Brushes.Red;

        txtBlkHelper = "ARTICLE CODE MAX LENGTH IS 10".TD();
        //txtBlkHelper.Foreground = Brushes.Red;
      }

      //check valid length
      else if (Pallet.ArticleCode.Length <= 10)
      {
        // la validazione è fatta dal lost focus
        isValidSintax = true;

        //iconResultArticle.Visibility = Visibility.Visible;
        //iconResultArticle.Kind = PackIconKind.Check;
        //iconResultArticle.Foreground = Brushes.Green;

        //txtBlkHelper.Text = "VALID".TD();
        //txtBlkHelper.Foreground = Brushes.Red;
      }

      //set helper
      HintAssist.SetHelperText(txtBoxArticleCode, txtBlkHelper);

      return isValidSintax;
    }

    private async void btnSearchArticle_Click(object sender, RoutedEventArgs e)
    {
      // open article ctrl
      // prepare the view
      var view = new CtrlPalletArticles();

      //show the dialog
      await DialogHost.Show(view, "RootDialog");

      C200153_Article ArticleSelected = view.ArticleSelected;

      txtBoxArticleCode.Text = ArticleSelected?.Code;

      if (ArticleSelected != null)
      {
        Pallet.ArticleCode = ArticleSelected.Code;
        if (!Pallet.ArticleCode.IsNullOrEmpty())
          Cmd_PM_Article_CheckPresence();
        else
        {
          txtBoxArticleCode.Background = Brushes.Transparent;
        }
      }

    }

    #endregion

    #region Eventi BatchCode

    private void txtBoxBatchCode_LostFocus(object sender, RoutedEventArgs e)
    {
      Pallet.BatchCode = txtBoxBatchCode.Text;
      txtBoxBatchCode_Validate();
    }

    private void txtBoxBatchCode_TextChanged(object sender, TextChangedEventArgs e)
    {
      Pallet.BatchCode = txtBoxBatchCode.Text;
      txtBoxBatchCode_Validate();
    }

    private bool txtBoxBatchCode_Validate()
    {
      txtBoxBatchCode.Background = Brushes.Transparent;
      bool isValidSintax = false;

      //prepare helper elements
      iconResultBatch.Visibility = Visibility.Hidden;
      string txtBlkHelper = string.Empty;

      // check empty
      if (String.IsNullOrWhiteSpace(Pallet.BatchCode) || String.IsNullOrEmpty(Pallet.BatchCode))
      {

        iconResultBatch.Visibility = Visibility.Visible;
        iconResultBatch.Kind = PackIconKind.Cancel;
        iconResultBatch.Foreground = Brushes.OrangeRed;

      }

      //// check length over limit
      //else if (Pallet.BatchCode.Length > 10)
      //{
      //  iconResultBatch.Visibility = Visibility.Visible;
      //  iconResultBatch.Kind = PackIconKind.ErrorOutline;
      //  iconResultBatch.Foreground = Brushes.Red;

      //  txtBlkHelper = "BATCH CODE MAX LENGTH IS 10".TD();
      //  //txtBlkHelper.Foreground = Brushes.Red;
      //}

      //check valid length
      else if (Pallet.BatchCode.Length <= 30)
      {
        iconResultBatch.Visibility = Visibility.Visible;
        iconResultBatch.Kind = PackIconKind.Check;
        iconResultBatch.Foreground = Brushes.Green;

        isValidSintax = true;
      }

      //set helper
      HintAssist.SetHelperText(txtBoxBatchCode, txtBlkHelper);

      return isValidSintax;
    }

    #endregion

    #region Eventi BatchLength

    private void txtBoxBatchLength_LostFocus(object sender, RoutedEventArgs e)
    {
      try
      {
        Pallet.BatchLength = int.Parse(txtBoxBatchLength.Text);
        txtBoxBatchLength_Validate();
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        iconResultBatchLength.Visibility = Visibility.Visible;
        iconResultBatchLength.Kind = PackIconKind.QuestionMark;
        iconResultBatchLength.Foreground = Brushes.DarkRed;

        string txtBlkHelper = "INSERT VALID NUMBER".TD();
        HintAssist.SetHelperText(txtBoxBatchLength, txtBlkHelper);
      }
    }

    private void txtBoxBatchLength_TextChanged(object sender, TextChangedEventArgs e)
    {
      try
      {
        Pallet.BatchLength = int.Parse(txtBoxBatchLength.Text);
        txtBoxBatchLength_Validate();
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        iconResultBatchLength.Visibility = Visibility.Visible;
        iconResultBatchLength.Kind = PackIconKind.QuestionMark;
        iconResultBatchLength.Foreground = Brushes.DarkRed;

        string txtBlkHelper = "INSERT VALID NUMBER".TD();
        HintAssist.SetHelperText(txtBoxBatchLength, txtBlkHelper);
      }
    }

    private bool txtBoxBatchLength_Validate()
    {
      txtBoxBatchLength.Background = Brushes.Transparent;
      bool isValidSintax = false;

      //prepare helper elements
      iconResultBatchLength.Visibility = Visibility.Hidden;
      string txtBlkHelper = string.Empty;

      // check empty
      if (String.IsNullOrWhiteSpace(txtBoxBatchLength.Text) || String.IsNullOrEmpty(txtBoxBatchLength.Text))
      {
        iconResultBatchLength.Visibility = Visibility.Visible;
        iconResultBatchLength.Kind = PackIconKind.Cancel;
        iconResultBatchLength.Foreground = Brushes.OrangeRed;

      }

      // check quantity under limit
      else if (Pallet.BatchLength <= 0)
      {
        iconResultBatchLength.Visibility = Visibility.Visible;
        iconResultBatchLength.Kind = PackIconKind.ErrorOutline;
        iconResultBatchLength.Foreground = Brushes.Red;

        txtBlkHelper = "QUANTITY MUST BE POSITIVE".TD();
      }

      //check valid length
      else if (Pallet.BatchLength <= int.MaxValue)
      {
        iconResultBatchLength.Visibility = Visibility.Visible;
        iconResultBatchLength.Kind = PackIconKind.Check;
        iconResultBatchLength.Foreground = Brushes.Green;

        isValidSintax = true;
      }

      //set helper
      HintAssist.SetHelperText(txtBoxBatchLength, txtBlkHelper);

      return isValidSintax;
    }
    #endregion

    #region Eventi ProductionDate
    private void ProductionDate_LostFocus(object sender, RoutedEventArgs e)
    {
      // to validate date time, try to parse it
      try
      {
        Pallet.ProductionDate = DateTime.Parse(dpProductionDate.Text);
        txtBoxProductionDate_Validate();
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        iconResultProductionDate.Visibility = Visibility.Visible;
        iconResultProductionDate.Kind = PackIconKind.QuestionMark;
        iconResultProductionDate.Foreground = Brushes.DarkRed;

        string txtBlkHelper = "INSERT VALID DATE".TD();
        HintAssist.SetHelperText(dpProductionDate, txtBlkHelper);
      }

    }
    private void ProductionDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
    {
      // to validate date time, try to parse it
      try
      {
        Pallet.ProductionDate = DateTime.Parse(dpProductionDate.Text);
        txtBoxProductionDate_Validate();
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        iconResultProductionDate.Visibility = Visibility.Visible;
        iconResultProductionDate.Kind = PackIconKind.QuestionMark;
        iconResultProductionDate.Foreground = Brushes.DarkRed;

        string txtBlkHelper = "INSERT VALID DATE".TD();
        HintAssist.SetHelperText(dpProductionDate, txtBlkHelper);
      }
    }

    private bool txtBoxProductionDate_Validate()
    {
      dpProductionDate.Background = Brushes.Transparent;
      bool isValidSintax = false;

      //prepare helper elements
      iconResultProductionDate.Visibility = Visibility.Hidden;
      string txtBlkHelper = string.Empty;

      // check empty
      if (String.IsNullOrWhiteSpace(dpProductionDate.Text) || String.IsNullOrEmpty(dpProductionDate.Text))
      {
        iconResultProductionDate.Visibility = Visibility.Visible;
        iconResultProductionDate.Kind = PackIconKind.Cancel;
        iconResultProductionDate.Foreground = Brushes.OrangeRed;

      }

      // check quantity under limit
      else if (Pallet.ProductionDate <= DateTime.MinValue)
      {
        iconResultProductionDate.Visibility = Visibility.Visible;
        iconResultProductionDate.Kind = PackIconKind.ErrorOutline;
        iconResultProductionDate.Foreground = Brushes.Red;

        txtBlkHelper = "DATE NOT VALID".TD();
      }

      //check valid length
      else if (Pallet.ProductionDate <= DateTime.MaxValue)
      {
        iconResultProductionDate.Visibility = Visibility.Visible;
        iconResultProductionDate.Kind = PackIconKind.Check;
        iconResultProductionDate.Foreground = Brushes.Green;

        isValidSintax = true;
      }

      //set helper
      HintAssist.SetHelperText(dpProductionDate, txtBlkHelper);

      return isValidSintax;
    }


    #endregion

    #region Eventi ExpiryDate
    private void ExpiryDate_LostFocus(object sender, RoutedEventArgs e)
    {
      // to validate date time, try to parse it
      try
      {
        Pallet.ExpiryDate = DateTime.Parse(dpExpiryDate.Text);
        txtBoxExpiryDate_Validate();
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        iconResultExpiryDate.Visibility = Visibility.Visible;
        iconResultExpiryDate.Kind = PackIconKind.QuestionMark;
        iconResultExpiryDate.Foreground = Brushes.DarkRed;

        string txtBlkHelper = "INSERT VALID DATE".TD();
        HintAssist.SetHelperText(dpExpiryDate, txtBlkHelper);
      }

    }
    private void ExpiryDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
    {
      // to validate date time, try to parse it
      try
      {
        Pallet.ExpiryDate = DateTime.Parse(dpExpiryDate.Text);
        txtBoxExpiryDate_Validate();
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        iconResultExpiryDate.Visibility = Visibility.Visible;
        iconResultExpiryDate.Kind = PackIconKind.QuestionMark;
        iconResultExpiryDate.Foreground = Brushes.DarkRed;

        string txtBlkHelper = "INSERT VALID DATE".TD();
        HintAssist.SetHelperText(dpExpiryDate, txtBlkHelper);
      }
    }

    private bool txtBoxExpiryDate_Validate()
    {
      dpExpiryDate.Background = Brushes.Transparent;
      bool isValidSintax = false;

      //prepare helper elements
      iconResultExpiryDate.Visibility = Visibility.Hidden;
      string txtBlkHelper = string.Empty;

      // check empty
      if (String.IsNullOrWhiteSpace(dpExpiryDate.Text) || String.IsNullOrEmpty(dpExpiryDate.Text))
      {
        iconResultExpiryDate.Visibility = Visibility.Visible;
        iconResultExpiryDate.Kind = PackIconKind.Cancel;
        iconResultExpiryDate.Foreground = Brushes.OrangeRed;

      }

      // check quantity under limit
      else if (Pallet.ExpiryDate <= DateTime.MinValue)
      {
        iconResultExpiryDate.Visibility = Visibility.Visible;
        iconResultExpiryDate.Kind = PackIconKind.ErrorOutline;
        iconResultExpiryDate.Foreground = Brushes.Red;

        txtBlkHelper = "DATE NOT VALID".TD();
      }

      //check valid length
      else if (Pallet.ExpiryDate <= DateTime.MaxValue)
      {
        iconResultExpiryDate.Visibility = Visibility.Visible;
        iconResultExpiryDate.Kind = PackIconKind.Check;
        iconResultExpiryDate.Foreground = Brushes.Green;

        isValidSintax = true;
      }

      //set helper
      HintAssist.SetHelperText(dpExpiryDate, txtBlkHelper);

      return isValidSintax;
    }


    #endregion

    #region Eventi Depositor 

    private void txtBoxDepositorCode_LostFocus(object sender, RoutedEventArgs e)
    {
      Pallet.DepositorCode = txtBoxDepositorCode.Text;

      if (txtBoxDepositorCode_Validate())
        Cmd_PM_Depositor_CheckPresence();
    }

    private void txtBoxDepositorCode_TextChanged(object sender, TextChangedEventArgs e)
    {
      Pallet.DepositorCode = txtBoxDepositorCode.Text;
      txtBoxDepositorCode_Validate();
    }


    private bool txtBoxDepositorCode_Validate()
    {
      bool isValidSintax = false;

      txtBoxDepositorCode.Background = Brushes.Transparent;

      //prepare helper elements
      iconResultDepositor.Visibility = Visibility.Hidden;
      string txtBlkHelper = string.Empty;

      // check empty
      if (String.IsNullOrWhiteSpace(Pallet.DepositorCode) || String.IsNullOrEmpty(Pallet.DepositorCode))
      {
        iconResultDepositor.Visibility = Visibility.Visible;
        iconResultDepositor.Kind = PackIconKind.Cancel;
        iconResultDepositor.Foreground = Brushes.OrangeRed;

      }

      // check length over limit
      else if (Pallet.DepositorCode.Length > 50)
      {
        iconResultDepositor.Visibility = Visibility.Visible;
        iconResultDepositor.Kind = PackIconKind.ErrorOutline;
        iconResultDepositor.Foreground = Brushes.Red;

        txtBlkHelper = "DEPOSITOR CODE MAX LENGTH IS 50".TD();
        //txtBlkHelper.Foreground = Brushes.Red;
      }

      //check valid length
      else if (Pallet.DepositorCode.Length <= 50)
      {
        // la validazione è fatta dal lost focus
        isValidSintax = true;

        //iconResultDepositor.Visibility = Visibility.Visible;
        //iconResultDepositor.Kind = PackIconKind.Check;
        //iconResultDepositor.Foreground = Brushes.Green;

        //txtBlkHelper.Text = "VALID".TD();
        //txtBlkHelper.Foreground = Brushes.Red;
      }

      //set helper
      HintAssist.SetHelperText(txtBoxDepositorCode, txtBlkHelper);

      return isValidSintax;
    }

    private async void btnSearchDepositor_Click(object sender, RoutedEventArgs e)
    {
      // open Depositor ctrl
      // prepare the view
      var view = new CtrlPalletDepositors();

      //show the dialog
      await DialogHost.Show(view, "RootDialog");

      C200153_Depositor DepositorSelected = view.DepositorSelected;

      txtBoxDepositorCode.Text = DepositorSelected?.Code;

      if (DepositorSelected != null)
      {
        Pallet.DepositorCode = DepositorSelected.Code;
        if (!Pallet.DepositorCode.IsNullOrEmpty())
          Cmd_PM_Depositor_CheckPresence();
        else
        {
          txtBoxDepositorCode.Background = Brushes.Transparent;
        }
      }

    }

    #endregion

    private async void btnRegister_Click(object sender, RoutedEventArgs e)
    {
      #region article check
      if (String.IsNullOrWhiteSpace(Pallet.ArticleCode) || String.IsNullOrEmpty(Pallet.ArticleCode))
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "UPDATE PALLET RESULT".TD(),
          Message = "PLEASE INSERT VALID ARTICLE CODE".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        return;
      }

      if (Pallet.ArticleCode.Length > 10)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "UPDATE PALLET RESULT".TD(),
          Message = "ARTICLE CODE LENGTH MUST BE FROM 1 TO 10 CHARACTERS".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        return;
      }
      #endregion

      #region batch code check
      if (String.IsNullOrWhiteSpace(Pallet.BatchCode) || String.IsNullOrEmpty(Pallet.BatchCode))
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "UPDATE PALLET RESULT".TD(),
          Message = "PLEASE INSERT VALID BATCH CODE".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        return;
      }

      if (Pallet.BatchCode.Length > 50)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "UPDATE PALLET RESULT".TD(),
          Message = "BATCH CODE LENGTH MUST BE FROM 1 TO 50 CHARACTERS".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        return;
      }
      #endregion

      #region batch length check
      if (Pallet.BatchLength <= 0)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "UPDATE PALLET RESULT".TD(),
          Message = "PLEASE INSERT VALID BATCH LENGTH".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        return;
      }
      #endregion

      #region production date check
      // to validate date time, try to parse it
      //if (!DateTime.TryParse(Pallet.ExpiryDate.ToString("dd/mm/yyyy"), out DateTime theDate))
      //{
      //  AlertDialogArguments alertDialogArgs = new AlertDialogArguments
      //  {
      //    Title = "INSERT PALLET RESULT".TD(),
      //    Message = "INSERT VALID EXPIRY DATE".TD(),
      //    OkButtonLabel = "OK".TD()
      //  };

      //  await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

      //  return;
      //}
      #endregion

      #region expiry date check
      // to validate date time, try to parse it
      //if (!DateTime.TryParse(Pallet.ExpiryDate.ToString("dd/mm/yyyy"), out DateTime theDate))
      //{
      //  AlertDialogArguments alertDialogArgs = new AlertDialogArguments
      //  {
      //    Title = "UPDATE PALLET RESULT".TD(),
      //    Message = "INSERT VALID EXPIRY DATE".TD(),
      //    OkButtonLabel = "OK".TD()
      //  };

      //  await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

      //  return;
      //}
      #endregion

      #region depositor code check
      if (String.IsNullOrWhiteSpace(Pallet.DepositorCode) || String.IsNullOrEmpty(Pallet.DepositorCode))
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "UPDATE PALLET RESULT".TD(),
          Message = "PLEASE INSERT VALID DEPOSITOR CODE".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        return;
      }

      if (Pallet.DepositorCode.Length > 50)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "UPDATE PALLET RESULT".TD(),
          Message = "DEPOSITOR CODE LENGTH MUST BE FROM 1 TO 50 CHARACTERS".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        return;
      }
      #endregion



      ConfirmationDialogArguments dialogArgs = new ConfirmationDialogArguments
      {
        Title = "UPDATE PALLET CONFIRM".TD(),
        Message = "Are you sure?".TD(),
        OkButtonLabel = "YES".TD(),
        CancelButtonLabel = "CANCEL".TD(),
        StackedButtons = false
      };

      bool result = await ConfirmationDialog.ShowDialogAsync("RootDialog", dialogArgs);

      if (result)
        Cmd_PM_Pallet_Update();
    }
  }
}
