﻿
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using Model.Custom.C200153.PalletManager;

namespace View.Custom.C200153.PalletManager
{
  /// <summary>
  /// Interaction logic for CtrlStockFilter.xaml
  /// </summary>
  public partial class CtrlPalletUpdateAllFilterHorizontal : CtrlBaseC200153
  {

    #region EVENTI
    public delegate void OnSearchHandler();
    public event OnSearchHandler OnSearch;
    #endregion

    #region Fields
    private const int _indexStartValue = 0;
    #endregion

    #region PUBLIC PROPERTIES
    public C200153_PalletManagerAllFilter Filter { get; set; } = new C200153_PalletManagerAllFilter();

   
    #endregion

    #region Constructor
    public CtrlPalletUpdateAllFilterHorizontal()
    {
      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      HintAssist.SetHint(cbMaxItems, Localization.Localize.LocalizeDefaultString((string)cbMaxItems.Tag));      
      HintAssist.SetHint(txtBoxPalletCode, Localization.Localize.LocalizeDefaultString((string)txtBoxPalletCode.Tag));
      HintAssist.SetHint(txtBoxArticleCode, Localization.Localize.LocalizeDefaultString((string)txtBoxArticleCode.Tag));
      HintAssist.SetHint(txtBoxArticleDescr, Localization.Localize.LocalizeDefaultString((string)txtBoxArticleDescr.Tag));
      HintAssist.SetHint(txtBoxBatchCode, Localization.Localize.LocalizeDefaultString((string)txtBoxBatchCode.Tag));
      HintAssist.SetHint(txtBoxDepositorCode, Localization.Localize.LocalizeDefaultString((string)txtBoxDepositorCode.Tag));

      //HintAssist.SetHint(cbPalletType, Localization.Localize.LocalizeDefaultString((string)cbPalletType.Tag));
      //CollectionViewSource.GetDefaultView(cbPalletType.ItemsSource).Refresh();

      btnCancel.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnCancel.Tag);
      btnSearch.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnSearch.Tag);
    }

    #endregion TRADUZIONI

    #region Events
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
        return;

      Translate();

    }
    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
    }

    private void btnSearch_Click(object sender, RoutedEventArgs e)
    {
      if (cbMaxItems.SelectedItem != null)
      {
        Filter.MaxItems = (int)cbMaxItems.SelectedItem;
      }
      Filter.Index = _indexStartValue;
      Search();
    }
    #endregion

    #region private Methods
    private void Search()
    {
      OnSearch?.Invoke();
    }

    #endregion

    private void TextBox_KeyDown(object sender, KeyEventArgs e)
    {
      if(e.Key==Key.Return)
        Search();
    }
  }
}
