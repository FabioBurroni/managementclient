﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.PalletManager;
using View.Common.Languages;
using View.Custom.C200153.Converter;

namespace View.Custom.C200153.PalletManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletPrint.xaml
  /// </summary>
  public partial class CtrlPalletPrint : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public C200153_PalletPrintFilter Filter { get; set; } = new C200153_PalletPrintFilter();

    public ObservableCollectionFast<C200153_PalletManagerAll> PalletL { get; set; } = new ObservableCollectionFast<C200153_PalletManagerAll>();
    #endregion

    #region COSTRUTTORE
    public CtrlPalletPrint()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colBatchCode.Header = Localization.Localize.LocalizeDefaultString("BATCH CODE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colArticleDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colDateBorn.Header = Localization.Localize.LocalizeDefaultString("DATE BORN");
      colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
      colPosition.Header = Localization.Localize.LocalizeDefaultString("POSITION");
      colProductionDate.Header = Localization.Localize.LocalizeDefaultString("PRODUCTION DATE");
      colExpiryDate.Header = Localization.Localize.LocalizeDefaultString("EXPIRY DATE");
      colDepositorCode.Header = Localization.Localize.LocalizeDefaultString("DEPOSITOR");
      colPrint.Header = Localization.Localize.LocalizeDefaultString("PRINT");
      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
     
      CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE
    private void Cmd_PM_Pallet_Print_GetAll()
    {
      busyOverlay.IsBusy = true;
      PalletL.Clear();
      CommandManagerC200153.PM_Pallet_Print_GetAll(this, Filter);
    }
    private void PM_Pallet_Print_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      var palL = dwr.Data as List<C200153_PalletManagerAll>;
      if (palL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (palL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        palL = palL.Take(Filter.MaxItems).ToList();

        PalletL.AddRange(palL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

   #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    private void CtrlBaseC200153_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    #endregion


    #region Eventi Ricerca
    
    private void ctrlArticleFilter_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;
      ctrlFilter.SearchHighlight = false;

      Cmd_PM_Pallet_Print_GetAll();

    }
    #endregion

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_PM_Pallet_Print_GetAll();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_PM_Pallet_Print_GetAll();
    }

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletManagerAll)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletManagerAll)b.Tag)).PalletCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyBatch_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletManagerAll)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletManagerAll)b.Tag)).BatchCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyArticle_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletManagerAll)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletManagerAll)b.Tag)).ArticleCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyDepositor_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletManagerAll)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletManagerAll)b.Tag)).DepositorCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private async void butPrint_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletManagerAll)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletManagerAll)b.Tag)).PalletCode);

          string palCode = ((C200153_PalletManagerAll)b.Tag).PalletCode;

          if (palCode != null)
          {
            CtrlPalletPrintConfirmation view = new CtrlPalletPrintConfirmation("PRINT PALLET LABEL".TD(), palCode);

            //show the dialog
            await DialogHost.Show(view, "RootDialog");

            C200153_CustomResult PrintResult = view.Result;
            if (PrintResult == C200153_CustomResult.OK)
            {
              LocalSnackbar.ShowMessageOk("PRINT".TD() + " : " + palCode, 3);

            }
            else
            {
              LocalSnackbar.ShowMessageFail("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(PrintResult), 3);
            }
          }
        }
        catch
        {

        }
      }
    }

  }

 
}