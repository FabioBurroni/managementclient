﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200153;
using Utilities.Extensions;

namespace View.Custom.C200153.PalletManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletPrintConfirmation.xaml
  /// </summary>
  public partial class CtrlPalletPrintConfirmation : CtrlBaseC200153
  {

    #region Public Properties
    private string _Title = string.Empty;

    public string Title
    {
      get { return _Title; }
      set
      {
        _Title = value;
        NotifyPropertyChanged("Title");
      }
    }

    private string _PalletCode = string.Empty;

    public string PalletCode
    {
      get { return _PalletCode; }
      set { 
        _PalletCode = value;
        NotifyPropertyChanged("PalletCode");
      }
    }

    private C200153_CustomResult _Result;

    public C200153_CustomResult Result
    {
      get { return _Result; }
      set
      {
        _Result = value;
        NotifyPropertyChanged("Result");
      }
    }

    #endregion

    #region COSTRUTTORE
    public CtrlPalletPrintConfirmation(string title,string palletCode)
    {
    
      InitializeComponent();

      Title = title;
      PalletCode = palletCode;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
     
      butPrint.ToolTip = Localization.Localize.LocalizeDefaultString("PRINt");
      butClose.ToolTip = Localization.Localize.LocalizeDefaultString("CLOSE");
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    private void Cmd_PM_Pallet_Print()
    {      
      CommandManagerC200153.PM_Print(this, Context.Instance.Position, PalletCode);
    }
    private void PM_Print(IList<string> commandMethodParameters, IModel model)
    {      
      var dwr = model as DataWrapperResult;
      if (dwr == null)
      {
        Result = C200153_CustomResult.PRINTER_NOT_VALID;
      }
      else if (dwr.Result == null)
      {
        Result = C200153_CustomResult.PRINTER_NOT_VALID;
      }
      else
      {
        Result = dwr.Result.ConvertTo<C200153_CustomResult>();
      }

      DialogHost.CloseDialogCommand.Execute(true, null);
    }

   
    #endregion


    #region Eventi Ricerca

    

    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();
    }

    private void butPrint_Click(object sender, RoutedEventArgs e)
    {
      if (!string.IsNullOrEmpty(PalletCode))
        Cmd_PM_Pallet_Print();

    }


    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    #endregion

  }



}
