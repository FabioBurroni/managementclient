﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200153;
using Utilities.Extensions;

namespace View.Custom.C200153.PalletManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletCheckInConfirmation.xaml
  /// </summary>
  public partial class CtrlPalletCheckInConfirmation : CtrlBaseC200153
  {

    #region Public Properties
    private string _Title = string.Empty;

    public string Title
    {
      get { return _Title; }
      set
      {
        _Title = value;
        NotifyPropertyChanged("Title");
      }
    }

    private string _PalletCode = string.Empty;

    public string PalletCode
    {
      get { return _PalletCode; }
      set { 
        _PalletCode = value;
        NotifyPropertyChanged("PalletCode");
      }
    }

    private C200153_CustomResult _Result = C200153_CustomResult.UNDEFINED;

    public C200153_CustomResult Result
    {
      get { return _Result; }
      set
      {
        _Result = value;
        NotifyPropertyChanged("Result");
      }
    }

    #endregion

    #region COSTRUTTORE
    public CtrlPalletCheckInConfirmation(string title,string palletCode)
    {
    
      InitializeComponent();

      Title = title;
      PalletCode = palletCode;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
     
      butConfirm.ToolTip = Localization.Localize.LocalizeDefaultString("CONFIRM");
      butClose.ToolTip = Localization.Localize.LocalizeDefaultString("CLOSE");
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
   

   
    #endregion


    #region Eventi Ricerca

    

    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();
    }

    private void butConfirm_Click(object sender, RoutedEventArgs e)
    {
      Result = C200153_CustomResult.OK;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }


    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    #endregion

  }



}
