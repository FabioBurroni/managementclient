﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.ArticleManager;
using Model.Custom.C200153.Depositor;
using Model.Custom.C200153.PalletManager;
using View.Common.Languages;

namespace View.Custom.C200153.PalletManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletArticles.xaml
  /// </summary>
  public partial class CtrlPalletDepositors : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public int StartIndex { get; set; } = _indexStartValue;
    public int MaxItems { get; set; } = _maxItemsStartValue;

    public C200153_PalletDepositorFilter Filter { get; set; } = new C200153_PalletDepositorFilter();

    public ObservableCollectionFast<C200153_Depositor> DepL { get; set; } = new ObservableCollectionFast<C200153_Depositor>();
    #endregion

    #region Article selected dp


    public C200153_Depositor DepositorSelected
    {
      get { return (C200153_Depositor)GetValue(DepositorSelectedProperty); }
      set { SetValue(DepositorSelectedProperty, value); }
    }

    // Using a DependencyProperty as the backing store for OrderSelected.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty DepositorSelectedProperty =
        DependencyProperty.Register("DepositorSelected", typeof(C200153_Depositor), typeof(CtrlPalletDepositors)
          , new UIPropertyMetadata(null));

    #endregion

    #region COSTRUTTORE
    public CtrlPalletDepositors()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlDepositorFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);

      colDepositorCode.Header = Localization.Localize.LocalizeDefaultString("CODE");
      colAddress.Header = Localization.Localize.LocalizeDefaultString("ADDRESS");

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butClose.ToolTip = Localization.Localize.LocalizeDefaultString("CLOSE");

      CollectionViewSource.GetDefaultView(dgDepositorL.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    private void Cmd_PM_Depositor_GetAll()
    {
      busyOverlay.IsBusy = true;
      DepL.Clear();
      CommandManagerC200153.PM_Depositor_GetAll(this, Filter);
    }
    private void PM_Depositor_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;

      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var depL = dwr.Data as List<C200153_Depositor>;
      if (depL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (depL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        depL = depL.Take(Filter.MaxItems).ToList();
        DepL.AddRange(depL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

   
    #endregion


    #region Eventi Ricerca

    private void ctrlDepositorFilter_OnSearch()
    {
      Cmd_PM_Depositor_GetAll();
    }

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_PM_Depositor_GetAll();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_PM_Depositor_GetAll();
    }

   

    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();

      Cmd_PM_Depositor_GetAll();
    }

  

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_Depositor)
      {
        try
        {
          Clipboard.SetText((((C200153_Depositor)b.Tag)).Code);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }


    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      DepositorSelected = null;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    private void dgDepositorL_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }
    #endregion

  }



}
