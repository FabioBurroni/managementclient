﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.ArticleManager;
using Model.Custom.C200153.PalletManager;
using View.Common.Languages;

namespace View.Custom.C200153.PalletManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletArticles.xaml
  /// </summary>
  public partial class CtrlPalletArticles : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public int StartIndex { get; set; } = _indexStartValue;
    public int MaxItems { get; set; } = _maxItemsStartValue;

    public C200153_PalletArticleFilter Filter { get; set; } = new C200153_PalletArticleFilter();

    public ObservableCollectionFast<C200153_Article> ArticleL { get; set; } = new ObservableCollectionFast<C200153_Article>();
    #endregion

    #region Article selected dp


    public C200153_Article ArticleSelected
    {
      get { return (C200153_Article)GetValue(ArticleSelectedProperty); }
      set { SetValue(ArticleSelectedProperty, value); }
    }

    // Using a DependencyProperty as the backing store for OrderSelected.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ArticleSelectedProperty =
        DependencyProperty.Register("ArticleSelected", typeof(C200153_Article), typeof(CtrlPalletArticles)
          , new UIPropertyMetadata(null));

    #endregion

    #region COSTRUTTORE
    public CtrlPalletArticles()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlArticleFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colArtCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      //colStockWindow.Header = Localization.Localize.LocalizeDefaultString("STOCK WINDOW");
      //colFifoWindow.Header = Localization.Localize.LocalizeDefaultString("FIFO WINDOW");
      colTurnover.Header = Localization.Localize.LocalizeDefaultString("TURNOVER");
      colWrappingProgram.Header = Localization.Localize.LocalizeDefaultString("WRAPPING PROGRAM");
      
      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butClose.ToolTip = Localization.Localize.LocalizeDefaultString("CLOSE");

      CollectionViewSource.GetDefaultView(dgArticleL.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    private void Cmd_PM_Article_GetAll()
    {
      busyOverlay.IsBusy = true;
      ArticleL.Clear();
      CommandManagerC200153.PM_Article_GetAll(this, Filter);
    }
    private void PM_Article_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;

      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var artL = dwr.Data as List<C200153_Article>;
      if (artL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (artL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        artL = artL.Take(Filter.MaxItems).ToList();
        ArticleL.AddRange(artL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

   
    #endregion


    #region Eventi Ricerca

    private void ctrlArticleFilter_OnSearch()
    {
      Cmd_PM_Article_GetAll();
    }

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_PM_Article_GetAll();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_PM_Article_GetAll();
    }

   

    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();

      Cmd_PM_Article_GetAll();
    }

  

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_Article)
      {
        try
        {
          Clipboard.SetText((((C200153_Article)b.Tag)).Code);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }


    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      ArticleSelected = null;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    private void dgArticleL_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }
    #endregion

  }



}
