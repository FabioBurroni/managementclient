﻿using Model.Custom.C200153.Report;
using System;
using System.Globalization;
using System.Windows.Data;
using Utilities.Extensions;

namespace View.Custom.C200153.Converter
{
  public class GiacToValueConverter : IValueConverter
  {

    private string _warehouse = "AWH1";
    public string WAREHOUSE
    {
      get { return _warehouse; }
      set
      {
        if (value != _warehouse)
        {
          _warehouse = value;
        }
      }
    }

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
       if (value is C200153_StockByArea)
      {
        C200153_StockByArea giac = (C200153_StockByArea)value; 

        if (giac.AreaCode.EqualsIgnoreCase(WAREHOUSE))
          return giac.NumPallet;
        else 
          return 0;
      }

      return 0;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
