﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using Model.Custom.C200153.Router;

namespace View.Custom.C200153.Converter
{
  public class ConvRouterConfigToImgUrl : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {


    if (value is C200153_RouterConfigurationEnum)
      {
        C200153_RouterConfigurationEnum re = (C200153_RouterConfigurationEnum)value;

        string s = "/View;component/Custom/C200153/Images/Router/";

        switch (re)
        {
          case C200153_RouterConfigurationEnum.DEFAULT: return s + "router_default.png";
          case C200153_RouterConfigurationEnum.BACKUP1: return s + "router_backup1.png";
          case C200153_RouterConfigurationEnum.BACKUP2: return s + "router_backup2.png";
          case C200153_RouterConfigurationEnum.BACKUP3: return s + "router_backup3.png";
          case C200153_RouterConfigurationEnum.BACKUP4: return s + "router_backup4.png";
        }
      }
      return "";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
