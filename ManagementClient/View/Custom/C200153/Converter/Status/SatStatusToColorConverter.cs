﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using Model.Custom.C200153.WhStatus;

namespace View.Custom.C200153.Converter
{
  public class SatStatusToColorConverter : IValueConverter
  {

    public bool IsForeground { get; set; } = true;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is C200153_SatelliteStatusEnum)
      {
        var status = (C200153_SatelliteStatusEnum)value;

        switch (status)
        {
          case C200153_SatelliteStatusEnum.PLAY:
            return IsForeground ? Brushes.White : Brushes.Green;
          case C200153_SatelliteStatusEnum.STOP:
            return IsForeground ? Brushes.Yellow: Brushes.Red;
          case C200153_SatelliteStatusEnum.ERROR:
            return IsForeground ? Brushes.Yellow : Brushes.Red;
          case C200153_SatelliteStatusEnum.RESTORE:
            return IsForeground ? Brushes.Yellow : Brushes.Red;
          case C200153_SatelliteStatusEnum.RESET:
            return IsForeground ? Brushes.Yellow : Brushes.Red;
          case C200153_SatelliteStatusEnum.CHARGING:
            return IsForeground ? Brushes.Yellow : Brushes.Orange;
          case C200153_SatelliteStatusEnum.PARKING:
            return IsForeground ? Brushes.White: Brushes.Blue;
          case C200153_SatelliteStatusEnum.MANUAL:
            return IsForeground ? Brushes.White : Brushes.Gray;
          default:
            return IsForeground ? Brushes.White : Brushes.Gray;
        }
      }
        return IsForeground ? Brushes.White : Brushes.Gray;

    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
