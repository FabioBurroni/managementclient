﻿using System;
using System.Globalization;
using System.Windows.Data;
using Model.Custom.C200153;

namespace View.Custom.C200153.Converter
{
  public class CustomResultToImgUrlConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {


    if (value is C200153_CustomResult)
      {
        C200153_CustomResult re = (C200153_CustomResult)value;

        string s = "/View;component/Common/Images/";

        switch (re)
        {
          //Generic 0
          case C200153_CustomResult.UNDEFINED: return s + "warning.png";
          case C200153_CustomResult.OK: return s + "ok.png";
          case C200153_CustomResult.DATABASE_NOT_CONNECTED: return s + "Result/database.png";
          case C200153_CustomResult.STORED_PROCEDURE_EXCEPTION: return s + "Result/database.png";
          case C200153_CustomResult.QUERY_EXCEPTION: return s + "Result/database.png";
          case C200153_CustomResult.ERROR_READING_DATA_FROM_PLC: return s + "Result/question.png";
          case C200153_CustomResult.ERROR_DATA_FROM_PLC_NOT_VALID: return s + "Result/question.png";
          case C200153_CustomResult.UNTRANSLATED_ERROR: return s + "Result/question.png";
          case C200153_CustomResult.CONFIGURATION_ERROR: return s + "Result/parameters.png";
          case C200153_CustomResult.PARAMETERS_ERROR: return s + "Result/parameters.png";

          //BarCode 10  
          case C200153_CustomResult.BAR_CODE_NOT_VALID : return s + "Result/barcode_error.png";
          case C200153_CustomResult.BAR_CODE_NOT_READ: return s + "Result/barcode_error.png";
          case C200153_CustomResult.BAR_CODE_READER_DISABLED: return s + "Result/barcode_error.png";

          //Lotto 20
          case C200153_CustomResult.LOT_NOT_VALID: return s + "order.png";

          //PalletType30
          case C200153_CustomResult.PALLETTYPE_NOT_VALID: return s + "Result/pallet_top_error.png";

          //Article 40
          case C200153_CustomResult.ARTICLE_NOT_VALID: return s + "Result/package_unknown.png";
          case C200153_CustomResult.ARTICLE_NOT_PRESENT: return s + "Result/package_unknown.png";
          case C200153_CustomResult.ARTICLETYPE_NOT_PRESENT: return s + "Result/package_unknown.png";
          case C200153_CustomResult.ARTICLE_ERROR_INSERT: return s + "Result/package_error.png";
          case C200153_CustomResult.ARTICLE_ERROR_UPDATE: return s + "Result/package_error.png";
          case C200153_CustomResult.ARTICLE_WRAPPINGPROGRAM_NOT_VALID: return s + "Result/package_error.png";
          case C200153_CustomResult.ARTICLE_DESCRIPTION_NOT_VALID: return s + "Result/package_error.png";

          //Pallet 50
          case C200153_CustomResult.PALLET_NOT_VALID: return s + "Result/pallet_top_error.png";
          case C200153_CustomResult.PALLET_NOT_FOUND: return s + "Result/pallet_top_error.png";
          case C200153_CustomResult.PALLET_INSERT_ERROR: return s + "Result/pallet_top_error.png";
          case C200153_CustomResult.PALLET_ALREADY_PRESENT_IN_WH: return s + "Result/pallet_full_exclamation.png";
          case C200153_CustomResult.PALLET_ALREADY_PRESENT_IN_HANDLING: return s + "Result/pallet_full_exclamation.png";
          case C200153_CustomResult.PALLET_CODE_CREATION_PROCEDURE_ERROR: return s + "Result/pallet_full_question.png";
          case C200153_CustomResult.PALLET_CODE_CREATION_COMPANY_CODE_NOT_VALID: return s + "Result/pallet_full_question.png";
          case C200153_CustomResult.PALLET_CODE_CREATION_NOT_IN_RANGE: return s + "Result/pallet_full_question.png";
          case C200153_CustomResult.PALLET_DESTINED_TO_REJECT: return s + "Result/pallet_full_exclamation.png";
          case C200153_CustomResult.PALLET_ALREADY_MAPPED_IN_POSITION: return s + "warnResult/pallet_full_exclamationing_red.png";
          case C200153_CustomResult.PALLET_SERVICE_ERROR_CREATING: return s + "Result/pallet_full_exclamation.png";
          case C200153_CustomResult.PALLET_WITHOUT_DESTINATION: return s + "Result/pallet_full_question.png";
          case C200153_CustomResult.PALLET_DESTINED_TO_EXIT: return s + "Result/pallet_full_exclamation.png";
          case C200153_CustomResult.PALLET_TRACKING_ERROR: return s + "Result/pallet_full_question.png";
          case C200153_CustomResult.PALLET_ALREADY_PRESENT_IN_DB: return s + "Result/pallet_full_question.png";
          case C200153_CustomResult.PALLET_ALREADY_RESERVED: return s + "Result/pallet_full_question.png";
          case C200153_CustomResult.PALLET_UPDATE_ERROR: return s + "Result/pallet_full_exclamation.png";
          case C200153_CustomResult.PALLET_ALREADY_PRESENT: return s + "Result/pallet_full_exclamation.png";

          //CustomPallet 70
          case C200153_CustomResult.PALLETCUSTOM_INSERT_ERROR: return s + "Result/pallet_full_exclamation.png";
          case C200153_CustomResult.PALLETCUSTOM_NOT_FOUND: return s + "Result/pallet_full_exclamation.png";
          case C200153_CustomResult.PALLETCUSTOM_UPDATE_ERROR: return s + "Result/pallet_full_exclamation.png";

          //Product
          case C200153_CustomResult.PRODUCT_INSERT_ERROR: return s + "Result/question.png";
          //case C200153_CustomResult.PRODUCT_UPDATE_ERRORR: return s + "Result/question.png";
          //case C200153_CustomResult.PRODUCT_NOT_FOUNDR: return s + "Result/question.png";

     
          //BATCH 90
          case C200153_CustomResult.BATCH_NOT_VALID: return s + "Result/pallet_top_error.png";

          //DEPOSITOR
          case C200153_CustomResult.DEPOSITOR_ERROR_DELETE: return s + "Result/package_unknown.png";
          case C200153_CustomResult.DEPOSITOR_ERROR_DELETING_DEPOSITOR_PALLET_IN_SYSTEM: return s + "Result/package_unknown.png";
          case C200153_CustomResult.DEPOSITOR_NOT_DELETABLE: return s + "Result/package_unknown.png";
          case C200153_CustomResult.DEPOSITOR_NOT_VALID: return s + "Result/package_unknown.png";
          case C200153_CustomResult.DEPOSITOR_NOT_PRESENT: return s + "Result/package_unknown.png";
          case C200153_CustomResult.DEPOSITOR_ERROR_INSERT: return s + "Result/package_unknown.png";
          case C200153_CustomResult.DEPOSITOR_ERROR_UPDATE: return s + "Result/package_unknown.png";
          case C200153_CustomResult.DEPOSITOR_ADDRESS_NOT_VALID: return s + "Result/package_unknown.png";

          //Mapping 100
          case C200153_CustomResult.MAPPING_INSERT_ERROR: return s + "Result/question.png";
          case C200153_CustomResult.POSITION_NOT_FOUND: return s + "Result/question.png";
          case C200153_CustomResult.MAPPING_PALLET_ERROR: return s + "Result/question.png";


          //ShapeControl
          case C200153_CustomResult.SHAPECONTROL_OVERFLOW_WIDTH_RIGHT: return s + "Result/pallet_width_right_error.png";
          case C200153_CustomResult.SHAPECONTROL_OVERFLOW_WIDTH_LEFT: return s + "Result/pallet_width_left_error.png";
          case C200153_CustomResult.SHAPECONTROL_OVERFLOW_WIDTH_RIGHTLEFT: return s + "Result/pallet_width_right_left_error.png";
          case C200153_CustomResult.SHAPECONTROL_OVERFLOW_DEPTH_REAR: return s + "Result/pallet_depth_rear_error.png";
          case C200153_CustomResult.SHAPECONTROL_OVERFLOW_DEPTH_FRONT: return s + "Result/pallet_depth_front_error.png";
          case C200153_CustomResult.SHAPECONTROL_OVERFLOW_DEPTH_REARFRONT: return s + "Result/pallet_depth_front_rear_error.png";
          case C200153_CustomResult.SHAPECONTROL_OVERFLOW_HEIGHT: return s + "palletHeightError.png";
          case C200153_CustomResult.SHAPECONTROL_OVERFLOW_WEIGHT: return s + "weight_red.png";
          case C200153_CustomResult.SHAPECONTROL_NO_AVAILABLE_AREA: return s + "warning_red.png";
          case C200153_CustomResult.SHAPECONTROL_NO_REACHABLE_AREA: return s + "warning_red.png";
          case C200153_CustomResult.SHAPECONTROL_NO_AVAILABLE_CELL: return s + "warning_red.png";
          case C200153_CustomResult.SHAPECONTROL_PALLET_NOT_VALID: return s + "Result/pallet_top_error.png";
          case C200153_CustomResult.SHAPECONTROL_ARTICLE_NOT_VALID: return s + "Result/package_unknown.png";
          case C200153_CustomResult.SHAPECONTROL_ARTICLE_NOT_ACCEPTED: return s + "Result/package_error.png";
          case C200153_CustomResult.SHAPECONTROL_LABEL_NOT_VALID: return s + "Result/barcode_error.png";
          case C200153_CustomResult.SHAPECONTROL_LABEL_NOT_READ: return s + "Result/barcode_error.png";
          case C200153_CustomResult.SHAPECONTROL_PALLETTYPE_NOT_VALID: return s + "Result/pallet_top_error.png";
          case C200153_CustomResult.SHAPECONTROL_PALLET_IN_WH: return s + "warning_red.png";
          case C200153_CustomResult.SHAPECONTROL_GENERIC_ERROR: return s + "warning_red.png";
          case C200153_CustomResult.SHAPECONTROL_ERROR_READING_DATA_FROM_PLC: return s + "warning_red.png";
          case C200153_CustomResult.SHAPECONTROL_BAR_CODE_NOT_READ: return s + "Result/barcode_error.png";
          case C200153_CustomResult.SHAPECONTROL_BAR_CODE_NOT_VALID: return s + "Result/barcode_error.png";
          case C200153_CustomResult.SHAPECONTROL_ROUTING_SWITCHING: return s + "warning_red.png";
          case C200153_CustomResult.SHAPECONTROL_ROUTE_NOT_AVAILABLE: return s + "warning_red.png";
          case C200153_CustomResult.SHAPECONTROL_SLATS_NOK: return s + "warning_red.png";

          // Reserved cell
          case C200153_CustomResult.RESERVED_CELL_ERROR: return s + "warning_red.png";

          // final dest
          case C200153_CustomResult.FINALDEST_ERROR: return s + "warning_red.png";

            //Job
          case C200153_CustomResult.JOB_ERROR_CREATE: return s + "warning_red.png";
          case C200153_CustomResult.JOB_CANNOT_CREATE: return s + "warning_red.png";

          //ORDERS
          case C200153_CustomResult.ORDER_NUM_ROWS_NOT_VALID: return s + "warning_red.png";
          case C200153_CustomResult.ORDER_ERROR_UPDATE: return s + "warning_red.png";
          case C200153_CustomResult.ORDER_NOT_VALID: return s + "warning_red.png";
          case C200153_CustomResult.ORDERTYPE_NOT_VALID: return s + "warning_red.png";
          case C200153_CustomResult.ORDERSTATE_NOT_VALID: return s + "warning_red.png";
          case C200153_CustomResult.ORDER_CODE_NOT_VALID: return s + "warning_red.png";
          case C200153_CustomResult.ORDER_ALREADY_PRESENT: return s + "warning_red.png";
          case C200153_CustomResult.ORDER_ERROR_INSERT: return s + "warning_red.png";
          case C200153_CustomResult.ORDER_QTY_NOT_VALID: return s + "warning_red.png";
          case C200153_CustomResult.ORDERCMP_ERROR_INSERT: return s + "warning_red.png";
          case C200153_CustomResult.ORDER_NOT_EDITABLE: return s + "warning_red.png";
          case C200153_CustomResult.ORDER_DESTINATION_NOT_VALID: return s + "warning_red.png";
          case C200153_CustomResult.ORDER_ERROR_DESTINATION_UPDATE: return s + "warning_red.png";
          case C200153_CustomResult.ORDER_COMPONENT_ERROR_UPDATE: return s + "warning_red.png";



          //...printer
          case C200153_CustomResult.PRINTER_ERROR_UPDATE: return s + "warning_red.png";
          case C200153_CustomResult.PRINTER_ERROR_INSERT: return s + "warning_red.png";
          case C200153_CustomResult.PRINTER_ERROR_DELETE: return s + "warning_red.png";
          case C200153_CustomResult.PRINTER_NOT_VALID: return s + "warning_red.png";
          case C200153_CustomResult.PRINTER_ERROR: return s + "warning_red.png";

          //...label
          case C200153_CustomResult.LABEL_ERROR_UPDATE: return s + "warning_red.png";
          case C200153_CustomResult.LABEL_ERROR_INSERT: return s + "warning_red.png";
          case C200153_CustomResult.LABEL_ERROR_DELETE: return s + "warning_red.png";
          case C200153_CustomResult.LABEL_NOT_VALID: return s + "warning_red.png";
          case C200153_CustomResult.LABEL_FIELD_ERROR_UPDATE: return s + "warning_red.png";
          case C200153_CustomResult.LABEL_FIELD_ERROR_INSERT: return s + "warning_red.png";
          case C200153_CustomResult.LABEL_FIELD_ERROR_DELETE: return s + "warning_red.png";
          case C200153_CustomResult.LABEL_FIELD_NOT_VALID: return s + "warning_red.png";


          //...communications
          case C200153_CustomResult.COMMUNICATION_TIMEOUT: return s + "warning_red.png";
          case C200153_CustomResult.COMMUNICATION_RESET_RUNNING: return s + "warning_red.png";

          // ROUTER
          case C200153_CustomResult.ROUTER_IS_SWITCHING: return s + "warning_red.png";
          case C200153_CustomResult.ROUTER_SWITCH_NOT_ALLOWED: return s + "warning_red.png";
          case C200153_CustomResult.ROUTER_NOT_FOUND: return s + "warning_red.png";

          //CHECKIN
          case C200153_CustomResult.CHECKIN_PALLET_NOT_PRESENT: return s + "warning_red.png";
        }
      }
      return "";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
