﻿using Model.Custom.C200153;
using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Custom.C200153.Converter
{
  public class CustomResultToLocalizedStringConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      try
      {
        if ((value is C200153_CustomResult) &&(value != null))
        {
          string strToReturn = string.Empty;
          
          string descrToReturn = Enum.GetName(typeof(C200153_CustomResult), value);
          if (!string.IsNullOrEmpty(descrToReturn))
            strToReturn += Localization.Localize.LocalizeDefaultResultString(descrToReturn);

          int valNumber = (int)value;
          if (valNumber != 1)// 1 is ok... 
            strToReturn += "(" + ((int)value).ToString() + ") ";

          return strToReturn; 
        }
      }
      catch(Exception Ex)
      {
        string exc = Ex.ToString();
      }

      return "";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    public static object Convert(object value)
    {
      return new CustomResultToLocalizedStringConverter().Convert(value, null, null, CultureInfo.CurrentCulture);
    }
  }
}
