﻿using Model.Custom.C200153.Report;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using Utilities.Extensions;

namespace View.Custom.C200153.Converter
{
  public class IntRangeGiacToBrushConverter : IValueConverter
  {
    private string _warehouse = "AWH1";
    public string WAREHOUSE
    {
      get { return _warehouse; }
      set
      {
        if (value != _warehouse)
        {
          _warehouse = value;
        }
      }
    }

    private Brush _DefaultColor = Brushes.Black;
    public Brush DefaultColor
    {
      get { return _DefaultColor; }
      set
      {
        if (value != _DefaultColor)
        {
          _DefaultColor = value;
        }
      }
    }

    private Brush _Color1 = Brushes.Red;
    public Brush Color1
    {
      get { return _Color1; }
      set
      {
        if (value != _Color1)
        {
          _Color1 = value;
          
        }
      }
    }



    private Brush _Color2 = Brushes.Orange;
    public Brush Color2
    {
      get { return _Color2; }
      set
      {
        if (value != _Color2)
        {
          _Color2 = value;
        }
      }
    }


    private Brush _Color3 = Brushes.DarkGreen;
    public Brush Color3
    {
      get { return _Color3; }
      set
      {
        if (value != _Color3)
        {
          _Color3 = value;
        }
      }
    }


    

   

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
       if (value is C200153_StockByArea)
      {
        C200153_StockByArea giac = (C200153_StockByArea)value;

        if (giac.AreaCode.EqualsIgnoreCase(WAREHOUSE))
          if (giac.NumPallet <= giac.MinQty)
            return _Color1;
          else if (giac.NumPallet > giac.MinQty && giac.NumPallet <= giac.MaxQty)
            return _Color2;
          else if (giac.NumPallet > giac.MinQty)
            return _Color3;
          else
          return _DefaultColor;

       
      }

      return _DefaultColor;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
