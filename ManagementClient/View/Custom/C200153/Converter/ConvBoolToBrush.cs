﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows;

namespace View.Custom.C200153.Converter
{
  public class ConvBoolToBrush : IValueConverter
  {

    public SolidColorBrush TrueColor { get; set; } = new SolidColorBrush(Colors.Green);
    public SolidColorBrush FalseColor { get; set; } = new SolidColorBrush(Colors.Red);


    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is bool)
        return ((bool)value) ? TrueColor : FalseColor;
      return FalseColor;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
