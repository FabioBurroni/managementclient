﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using System.Globalization;

using System.Windows.Media;

namespace View.Custom.C200153.Converter
{
  public class ConverterArticleToColor : IValueConverter
  {
    private List<Brush> brushes = new List<Brush>() {
       Brushes.LightBlue
      , Brushes.AntiqueWhite
      , Brushes.LightCoral
      , Brushes.Azure
      , Brushes.LightCyan
      , Brushes.LightGoldenrodYellow
      , Brushes.LightGray
      , Brushes.LightGreen
      , Brushes.AliceBlue
      , Brushes.Aquamarine
      , Brushes.LightPink
      , Brushes.LightSalmon
      , Brushes.LightSteelBlue
      , Brushes.LightYellow
      , Brushes.Yellow
      , Brushes.Orange
      , Brushes.Aqua
      , Brushes.Gold
      , Brushes.Violet
      , Brushes.Purple };
    private Dictionary<string, Brush> _dicArtToBrush = new Dictionary<string, Brush>();
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      Brush ret = Brushes.Transparent;
      if (value is string)
      {
        string artCode = value as string;
        if (_dicArtToBrush.ContainsKey(artCode))
          return _dicArtToBrush[artCode];
        else
        {
          ret = brushes.FirstOrDefault(b => !_dicArtToBrush.Values.Contains(b));
          if (ret != null)
          {
            _dicArtToBrush.Add(artCode, ret);
          }
          else
          {
            ret = brushes.FirstOrDefault();
          }
        }

      }
      return ret;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    public void Reset()
    {
      _dicArtToBrush.Clear();
    }
  }
}
