﻿using System;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model.Custom.C200153.OrderManager;

namespace View.Custom.C200153.Converter
{
  public class OrderStateToIconConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      try
      {
        if (value != null)
        {
          C200153_State val = 0;
          if (!(value is C200153_State))
          {
           if(!(Enum.TryParse(value.ToString(),out val)))
           {
                return PackIconKind.About;
            }


          }
          else
          {
            val = (C200153_State)value;
          }

          switch (val)
          {
            case C200153_State.CDONE:
              return PackIconKind.HandOkay;//new Uri("/images/ok.png", UriKind.Relative);
            case C200153_State.CEXEC:
              return PackIconKind.Play;//new Uri("/images/play.png", UriKind.Relative);
            case C200153_State.CKILL:
              return PackIconKind.CancelBold;//new Uri("/images/nok.png", UriKind.Relative);
            case C200153_State.CPAUSE:
              return PackIconKind.Pause;//new Uri("/images/Pausa.gif", UriKind.Relative);
            case C200153_State.CWAIT:
              return PackIconKind.Hourglass;//new Uri("/images/wait.gif", UriKind.Relative);
            case C200153_State.LDONE:
              return PackIconKind.HandOkay;//new Uri("/images/ok.png", UriKind.Relative);
            case C200153_State.LEDIT:
              return PackIconKind.ModeEdit;//new Uri("/images/order.png", UriKind.Relative);
            case C200153_State.LEXEC:
              return PackIconKind.Play;//new Uri("/images/play.png", UriKind.Relative);
            case C200153_State.LKILL:
              return PackIconKind.CancelBold;//new Uri("/images/nok.png", UriKind.Relative);
            case C200153_State.LPAUSE:
              return PackIconKind.Pause;//new Uri("/images/Pausa.gif", UriKind.Relative);
            case C200153_State.LUNCOMPLETE:
              return PackIconKind.CallMissed;//new Uri("/images/tick.gif", UriKind.Relative);
            case C200153_State.LWAIT:
              return PackIconKind.Hourglass;//new Uri("/images/wait.gif", UriKind.Relative);
            case C200153_State.SERVICE:
              return PackIconKind.Warning;//new Uri("/images/warning.gif", UriKind.Relative);
          }
          return PackIconKind.AboutVariant;///???
        }
        return PackIconKind.About;///???
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        return PackIconKind.About;///???
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotSupportedException();
    }
  }
}
