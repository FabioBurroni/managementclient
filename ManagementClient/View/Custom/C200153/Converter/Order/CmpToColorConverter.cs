﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Model.Custom.C200153;
using Model.Custom.C200153.OrderManager;


namespace View.Custom.C200153.Converter
{
  public class CmpToColorConverter : IMultiValueConverter
  {

    public Brush StateWait { get; set; } = Brushes.Orange;
    public Brush StateDone{ get; set; } = Brushes.Green;
    public Brush StateExecNearToEnd{ get; set; } = Brushes.Purple;
    public Brush StateExec{ get; set; } = Brushes.Blue;
    public Brush StateKill{ get; set; } = Brushes.Red;
    public Brush StateDefault{ get; set; } = Brushes.Black;


    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var cmp = value as C200153_OrderCmp;
      if(cmp!=null)
      {
        switch (cmp.State)
        {
          case C200153_State.CWAIT:
            return StateWait;
          case C200153_State.CEXEC:
            if(cmp.QtyRemaining<=0)
              return StateExecNearToEnd;
            else
              return StateExec;
          case C200153_State.CKILL:
            return StateKill;
          case C200153_State.CDONE:
            return StateDone;
          default:
            return StateDefault;
        }
      }
      return StateDefault;

    }

    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      if (values != null && values.Length == 2)
      {
        var cmp = values[0] as C200153_OrderCmp;

        if (cmp != null)
        {
          switch (cmp.State)
          {
            case C200153_State.CWAIT:
              return StateWait;
            case C200153_State.CEXEC:
              if (cmp.QtyRemaining <= 0)
                return StateExecNearToEnd;
              else
                return StateExec;
            case C200153_State.CKILL:
              return StateKill;
            case C200153_State.CDONE:
              return StateDone;
            default:
              return StateDefault;
          }
        }
      }
      return StateDefault;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
