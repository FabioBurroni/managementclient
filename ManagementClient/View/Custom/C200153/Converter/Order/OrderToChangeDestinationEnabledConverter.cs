﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Model.Custom.C200153.OrderManager;

namespace View.Custom.C200153.Converter
{
  public class OrderToChangeDestinationEnabledConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value!=null && value is C200153_Order)
      {
        return ((C200153_Order)value).State == C200153_State.LWAIT;
      }

      return false;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
