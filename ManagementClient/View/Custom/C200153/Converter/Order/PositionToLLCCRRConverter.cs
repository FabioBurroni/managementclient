﻿using System;
using System.Linq;
using System.Windows.Data;
using System.Globalization;

namespace View.Custom.C200153.Converter
{
  public class PositionToLLCCRRConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is string && parameter is string)
      {
        if ((value != null) && (parameter != null))
        {

          string val = (string)value;
          string par = (string)parameter;

          var v = val.Split('-');

          if (v.Count() == 4)
          {

            if (par.ToLowerInvariant().Equals("column"))
            {
              int col;
              Int32.TryParse(v[1], out col);

              return col.ToString();
            }
            else if (par.ToLowerInvariant().Equals("floor"))
            {
              int col;
              Int32.TryParse(v[2], out col);

              return col.ToString();
            }
            else if (par.ToLowerInvariant().Equals("side"))
            {
              int col;
              Int32.TryParse(v[3], out col);

              return col.ToString();
            }
            else
            {
              return "";

            }
          }
          else
          {
            return "";
          }
        }
        else
        {
          return "";

        }

      }
      return "";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
