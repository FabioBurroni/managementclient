﻿using System;
using System.Windows.Data;
using Model.Custom.C200153.OrderManager;

namespace View.Custom.C200153.Converter
{
  public class OrderStateToImageConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      if (value is C200153_State)
      {
        switch ((C200153_State)value)
        {
          case C200153_State.CDONE:
            return new Uri("/images/ok.png", UriKind.Relative);
          case C200153_State.CEXEC:
            return new Uri("/images/play.png", UriKind.Relative);
          case C200153_State.CKILL:
            return new Uri("/images/nok.png", UriKind.Relative);
          case C200153_State.CPAUSE:
            return new Uri("/images/Pausa.gif", UriKind.Relative);
            case C200153_State.CWAIT:
            return new Uri("/images/wait.gif", UriKind.Relative);
          case C200153_State.LDONE:
            return new Uri("/images/ok.png", UriKind.Relative);
          case C200153_State.LEDIT:
            return new Uri("/images/order.png", UriKind.Relative);
          case C200153_State.LEXEC:
            return new Uri("/images/play.png", UriKind.Relative);
          case C200153_State.LKILL:
            return new Uri("/images/nok.png", UriKind.Relative);
          case C200153_State.LPAUSE:
            return new Uri("/images/Pausa.gif", UriKind.Relative);
          case C200153_State.LUNCOMPLETE:
            return new Uri("/images/tick.gif", UriKind.Relative);
          case C200153_State.LWAIT:
            return new Uri("/images/wait.gif", UriKind.Relative);
          case C200153_State.SERVICE:
            return new Uri("/images/warning.gif", UriKind.Relative);
        }
      }
      return null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotSupportedException();
    }
  }
}
