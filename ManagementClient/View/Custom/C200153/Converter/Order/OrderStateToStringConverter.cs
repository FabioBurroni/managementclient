﻿using System;
using System.Windows.Data;
using System.Globalization;

using Model.Custom.C200153.OrderManager;

namespace View.Custom.C200153.Converter
{
  public class OrderStateToStringConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is C200153_State)
      {
        switch((C200153_State)value)
        {
          case C200153_State.CDONE:
            return "COMPLETED";
          case C200153_State.CEXEC:
            return "EXECUTING";
          case C200153_State.CKILL:
            return "KILLED";
          case C200153_State.CPAUSE:
            return "PAUSED";
          case C200153_State.CWAIT:
            return "WAITING";
          case C200153_State.LDONE:
            return "COMPLETED";
          case C200153_State.LEDIT:
            return "EDITING";
          case C200153_State.LEXEC:
            return "EXECUTING";
          case C200153_State.LKILL:
            return "KILLED";
          case C200153_State.LPAUSE:
            return "PAUSED";
          case C200153_State.LUNCOMPLETE:
            return "NOT COMPLETED";
          case C200153_State.LWAIT:
            return "WAITING";
          case C200153_State.SERVICE:
            return "SERVICE";
        }
      }
      return "";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
