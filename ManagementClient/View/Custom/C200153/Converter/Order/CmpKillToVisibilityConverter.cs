﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Model.Custom.C200153.OrderManager;

namespace View.Custom.C200153.Converter
{
  public class CmpKillToVisibilityConverter : IMultiValueConverter
  {
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      if (values == null)
        return Visibility.Hidden;
      if (values.Length != 3)
        return Visibility.Hidden;
      if (!(values[0] is C200153_Order))
        return Visibility.Hidden;
      if (!(values[1] is C200153_OrderCmp))
        return Visibility.Hidden;

      var ord = values[0] as C200153_Order;
      var cmp = values[1] as C200153_OrderCmp;

      if(ord.State==C200153_State.LEXEC || ord.State == C200153_State.LPAUSE || ord.State == C200153_State.LWAIT)
      {
        if(cmp.State == C200153_State.CWAIT || cmp.State == C200153_State.CEXEC)
        {
          return Visibility.Visible;
        }
      }

      return Visibility.Hidden;


    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
