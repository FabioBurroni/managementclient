﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
using Model.Custom.C200153.OrderManager;
namespace View.Custom.C200153.Converter
{
  public class CmpStateToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      Visibility ret = Visibility.Collapsed;
      string par = parameter as string;
      if (value is C200153_State)
      {
      
        switch((C200153_State)value)
        {
          case C200153_State.LDONE:
          case C200153_State.LEDIT:
          case C200153_State.LKILL:
          case C200153_State.LUNCOMPLETE:
            break;
          case C200153_State.LEXEC:
            if (par == "pause" || par=="kill" || par=="refresh")
              ret = Visibility.Visible;
            break;
          case C200153_State.LWAIT:
            if (par == "kill" || par=="exec")
              ret = Visibility.Visible;
            break;
          case C200153_State.LPAUSE:
            if (par == "exec" || par == "kill" || par == "refresh")
              ret = Visibility.Visible;
            break;

          case C200153_State.CDONE:
          case C200153_State.CKILL:
          case C200153_State.CPAUSE:
            break;
          case C200153_State.CEXEC:
          case C200153_State.CWAIT:
            if (par == "kill")
              ret = Visibility.Visible;
            break;
        }
      }
      return ret;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
