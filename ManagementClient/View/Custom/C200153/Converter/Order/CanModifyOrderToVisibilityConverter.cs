﻿using System;
using System.Windows;
using System.Globalization;
using System.Windows.Data;
using Model.Custom.C200153.OrderManager;

namespace View.Custom.C200153.Converter
{
  public class CanModifyOrderToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is C200153_State)
      {
        if (((C200153_State)value) == C200153_State.LWAIT)
          return Visibility.Visible;
      }
      return Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
