﻿using System;
using System.Windows;
using Model.Custom.C200153;


namespace View.Custom.C200153.DepositorManager
{
  /// <summary>
  /// Interaction logic for CtrlDepositor.xaml
  /// </summary>
  public partial class CtrlDepositor : CtrlBaseC200153
  {
    public C200153_UserLogged UserLogged { get; set; } = new C200153_UserLogged();

    #region Costruttore
    public CtrlDepositor()
    {
      InitializeComponent();
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
      Authentication.AuthenticationManager.Instance.LoginSession += Instance_LoginSession;

    }
    #endregion

    #region TRADUZIONI
    protected override void Translate()
    {
      tiManage.Text = Localization.Localize.LocalizeDefaultString((string)tiManage.Tag);
      tiImport.Text = Localization.Localize.LocalizeDefaultString((string)tiImport.Tag);
    }
    #endregion TRADUZIONI

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    #endregion

    private void Instance_LoginSession(object sender, Authentication.LoginSessionEventArgs sessionArgs)
    {
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
    }

    private void CtrlBaseC200153_Unloaded(object sender, RoutedEventArgs e)
    {
      try
      {
        Authentication.AuthenticationManager.Instance.LoginSession -= Instance_LoginSession;
      }
      catch (Exception)
      {

      }
    }
  }
}
