﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using LINQtoCSV;
using System.Windows;
using System.Windows.Data;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.Depositor;
using System.Windows.Forms;
using MaterialDesignExtensions.Controls;
using View.Common.Languages;
using MaterialDesignThemes.Wpf;

namespace View.Custom.C200153.DepositorManager
{
  /// <summary>
  /// Interaction logic for CtrlDepositorManager.xaml
  /// </summary>
  public partial class CtrlDepositorImport : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private int i = 0;

    
    #endregion

    #region Public Properties
    public ObservableCollectionFast<C200153_DepositorImport> DepositorL
    {
      get { return (ObservableCollectionFast<C200153_DepositorImport>)GetValue(DepositorLProperty); }
      set { SetValue(DepositorLProperty, value); }
    }

    // Using a DependencyProperty as the backing store for string.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty DepositorLProperty =
        DependencyProperty.Register("DepositorL", typeof(ObservableCollectionFast<C200153_DepositorImport>), typeof(CtrlDepositorImport)
          , new UIPropertyMetadata(null));


    #endregion

    #region FilePath

    public string FilePath
    {
      get { return (string)GetValue(FilePathProperty); }
      set { SetValue(FilePathProperty, value); }
    }

    // Using a DependencyProperty as the backing store for string.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty FilePathProperty =
        DependencyProperty.Register("FilePath", typeof(string), typeof(CtrlDepositorImport)
          , new UIPropertyMetadata(null));

    #endregion

    #region COSTRUTTORE
    public CtrlDepositorImport()
    {           
      InitializeComponent();
      FilePath = Localization.Localize.LocalizeDefaultString("OPEN CSV FILE");

      DepositorL = new ObservableCollectionFast<C200153_DepositorImport>();

    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkDescription.Text = Context.Instance.TranslateDefault((string)txtBlkDescription.Tag);
      HintAssist.SetHint(txtBoxSelectFile, Localization.Localize.LocalizeDefaultString((string)txtBoxSelectFile.Tag));

      colDepositorCode.Header = Localization.Localize.LocalizeDefaultString("DEPOSITOR CODE");
      colDepositorAddress.Header = Localization.Localize.LocalizeDefaultString("DEPOSITOR ADDRESS");
   
      txtBlkConfirm.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkConfirm.Tag);
      txtBlkClearSelection.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkClearSelection.Tag);
      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      txtBlkSelectAll.Text = Context.Instance.TranslateDefault((string)txtBlkSelectAll.Tag);
      txtBoxSelectFile.Text = Context.Instance.TranslateDefault((string)txtBoxSelectFile.Tag);

      btnConfirm.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnConfirm.Tag);
      btnCancel.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnCancel.Tag);
      btnClearSelection.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnClearSelection.Tag);
      btnOpenFile.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnOpenFile.Tag);
      btnSelectAll.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnSelectAll.Tag);
     
      CollectionViewSource.GetDefaultView(dgDepositorL.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    
    private void Cmd_DM_Depositor_Import(C200153_DepositorImport dep)
    {
      while (!DepositorL.ElementAt(i).Selected && (i < DepositorL.Count()-1))
           i++;

      if (DepositorL.ElementAt(i).Selected)
      {
        C200153_Depositor d = new C200153_Depositor();
        d.Code = dep.Code;        
        d.Address = dep.Address?? "ND";
        CommandManagerC200153.DM_Depositor_Import(this, d);
       }
      
    }
    private void DM_Depositor_Import(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C200153_CustomResult Result = (C200153_CustomResult)dwr.Data;

      DepositorL.ElementAt(i).Result = Result;
      DepositorL.ElementAt(i).Valid = (DepositorL.ElementAt(i).Result == C200153_CustomResult.OK);
      CollectionViewSource.GetDefaultView(dgDepositorL.ItemsSource).Refresh();

      i++;
      if (i < DepositorL.Count())
      {
        Cmd_DM_Depositor_Import(DepositorL.ElementAt(i));
      }

    }

    #endregion
     

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();
    }
      
    #endregion

    #region Private methods 

    private async void ReadDepositorFromCsv()
    {
      DepositorL.Clear();

      try
      {
        CsvFileDescription inputFileDescription = new CsvFileDescription
        {
          SeparatorChar = ';',
          FirstLineHasColumnNames = true,
          EnforceCsvColumnAttribute = true,
          IgnoreUnknownColumns = true,
          MaximumNbrExceptions = 50
        };
        CsvContext cc = new CsvContext();
        IEnumerable<C200153_DepositorImport> depositors = cc.Read<C200153_DepositorImport>(FilePath, inputFileDescription);

        if (depositors is null) return;

        foreach (C200153_DepositorImport dep in depositors)
        {

          if (dep != null)
          {
            C200153_DepositorImport depImp = dep;
          
            // valid - controlla che i campi siano validi
            depImp.Valid = true;

            DepositorL.Add(depImp);
          }
          

        }

      }
      catch (AggregatedException ae)
      {
        // Process all exceptions generated while processing the file
        List<Exception> innerExceptionsList =
            (List<Exception>)ae.Data["InnerExceptionsList"];
        foreach (Exception e in innerExceptionsList)
        {
          AlertDialogArguments alertDialogArgs = new AlertDialogArguments
          {
            Title = "ERROR READING FROM CSV".TD(),
            Message = e.Message.TD(),
            OkButtonLabel = "OK".TD()
          };

          await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);
          
        }
      }
      catch (DuplicateFieldIndexException dfie)
      {
        // name of the class used with the Read method - in this case "Product"
        string typeName = Convert.ToString(dfie.Data["TypeName"]);
        // Names of the two fields or properties that have the same FieldIndex
        string fieldName = Convert.ToString(dfie.Data["FieldName"]);
        string fieldName2 = Convert.ToString(dfie.Data["FieldName2"]);
        // Actual FieldIndex that the two fields have in common
        int commonFieldIndex = Convert.ToInt32(dfie.Data["Index"]);
        // Do some processing with this information
        // .........
        // Inform user of error situation
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "ERROR READING FROM CSV".TD(),
          Message = dfie.Message.TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

      }
      catch (Exception e)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "ERROR READING FROM CSV".TD(),
          Message = e.Message.TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);
      }

    }

    #endregion

    private async void btnConfirm_Click(object sender, RoutedEventArgs e)
    {
      if(DepositorL.Where(x=> x.Selected==true).Count()==0)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "INSERT DEPOSITOR NOT POSSIBLE".TD(),
          Message = "PLEASE SELECT AT LEAST ONE DEPOSITOR FROM LIST".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);
                
          return;
      }

    
      ConfirmationDialogArguments dialogArgs = new ConfirmationDialogArguments
      {
        Title = "INSERT DEPOSITOR CONFIRM".TD(),
        Message = "Are you sure?".TD(),
        OkButtonLabel = "YES".TD(),
        CancelButtonLabel = "CANCEL".TD(),
        StackedButtons = false
      };

      bool result = await ConfirmationDialog.ShowDialogAsync("RootDialog", dialogArgs);

      if (!result)
        return;

      i = 0;

      if (DepositorL.Count>0)
        Cmd_DM_Depositor_Import(DepositorL.ElementAt(i));
       
            
    }

    private async void btnOpenFile_Click(object sender, RoutedEventArgs e)
    {
      using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
      {
       // openFileDialog.InitialDirectory = "c:\\";
        openFileDialog.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
        openFileDialog.FilterIndex = 2;
        openFileDialog.RestoreDirectory = true;

        if (openFileDialog.ShowDialog() == DialogResult.OK)
        {
          //Get the path of specified file
          FilePath = openFileDialog.FileName;
        }
      }

      if (File.Exists(FilePath))
      {

        ReadDepositorFromCsv();
      }
      else
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "FILE NOT SELECTED".TD(),
          Message = "PLEASE SELECT A VALID CSV FILE".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);
        
      }

    }

    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      FilePath = Localization.Localize.LocalizeDefaultString("OPEN CSV FILE");
      DepositorL.Clear();
    }

    private void btnSelectAll_Click(object sender, RoutedEventArgs e)
    {
      if (DepositorL != null)
      {
        foreach (var art in DepositorL)
        {
          art.Selected = true;
        }
        CollectionViewSource.GetDefaultView(dgDepositorL.ItemsSource).Refresh();

      }
    }    

    private void btnClearSelection_Click(object sender, RoutedEventArgs e)
    {
      if (DepositorL != null)
      {
        foreach (var art in DepositorL)
        {
          art.Selected = false;
        }
        CollectionViewSource.GetDefaultView(dgDepositorL.ItemsSource).Refresh();

      }
    }

  
  }
}
