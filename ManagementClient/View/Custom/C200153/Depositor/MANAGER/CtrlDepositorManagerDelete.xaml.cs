﻿using System.Windows;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Common;
using Model.Custom.C200153.Depositor;
using View.Common.Languages;

namespace View.Custom.C200153.DepositorManager
{
  /// <summary>
  /// Interaction logic for CtrlDepositorManagerDelete.xaml
  /// </summary>
  public partial class CtrlDepositorManagerDelete : CtrlBaseC200153
  {
    public ShowDialogResults Result { get; set; }

    private C200153_Depositor _Depositor;

    public C200153_Depositor Depositor
    {
      get { return _Depositor; }
      set
      {
        _Depositor = value;
        NotifyPropertyChanged("Depositor");

      }
    }

    #region CONSTRUCTOR
    public CtrlDepositorManagerDelete(C200153_Depositor depositor)
    {
      Depositor = depositor;

      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkSubTitle.Text = Context.Instance.TranslateDefault((string)txtBlkSubTitle.Tag);

      txtBlkDepositorCode.Text = Context.Instance.TranslateDefault((string)txtBlkDepositorCode.Tag);
      txtBlkDepositorAddress.Text = Context.Instance.TranslateDefault((string)txtBlkDepositorAddress.Tag);

      txtBlkCancel.Text = Context.Instance.TranslateDefault((string)txtBlkCancel.Tag);
      txtBlkConfirm.Text = Context.Instance.TranslateDefault((string)txtBlkConfirm.Tag);

    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {

      Translate();

    }
    #endregion

  

    #region EVENTI
 
    private void butCancel_Click(object sender, RoutedEventArgs e)
    {
      CloseWithResult(ShowDialogResults.CANCELED);
    }

    private async void butConfirm_Click(object sender, RoutedEventArgs e)
    {      
      ConfirmationDialogArguments dialogArgs = new ConfirmationDialogArguments
      {
        Title = "DELETE DEPOSITOR CONFIRM".TD(),
        Message = "Are you sure?".TD(),
        OkButtonLabel = "YES".TD(),
        CancelButtonLabel = "CANCEL".TD(),
        StackedButtons = false
      };

      bool result = await ConfirmationDialog.ShowDialogAsync("RootDialog", dialogArgs); 
        
      if (result)
      {
        Depositor.IsDeleted = true;
        CloseWithResult(ShowDialogResults.OK);
      }
      else
        CloseWithResult(ShowDialogResults.CANCELED);
    }
    #endregion

    #region Private Methods
   
    
    void CloseWithResult(ShowDialogResults res)
    {
      Result = res;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }
    #endregion

  }
}
