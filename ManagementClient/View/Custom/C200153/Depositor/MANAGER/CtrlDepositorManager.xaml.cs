﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Common;
using Model.Custom.C200153;
using Model.Custom.C200153.Depositor;
using Model.Custom.C200153.Report;
using View.Common.Converter;
using View.Common.Languages;
using View.Common.UserMessage;
using View.Custom.C200153.Converter;

namespace View.Custom.C200153.DepositorManager
{
  /// <summary>
  /// Interaction logic for CtrlDepositorManager.xaml
  /// </summary>
  public partial class CtrlDepositorManager : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public int StartIndex { get; set; } = _indexStartValue;
    public int MaxItems { get; set; } = _maxItemsStartValue;

    public C200153_DepositorFilter Filter { get; set; } = new C200153_DepositorFilter();

    public ObservableCollectionFast<C200153_Depositor> DepositorL { get; set; } = new ObservableCollectionFast<C200153_Depositor>();
    #endregion

    private C200153_Depositor DepositorEdit;

    private C200153_Depositor _DepositorSelected;

    public C200153_Depositor DepositorSelected
    {
      get { return _DepositorSelected; }
      set 
      { 
        _DepositorSelected = value;
        NotifyPropertyChanged("DepositorSelected");
      }
    }


    #region COSTRUTTORE
    public CtrlDepositorManager()
    {

      DepositorEdit = new C200153_Depositor();
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlDepositorFilter.Filter;

      
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkDescription.Text = Context.Instance.TranslateDefault((string)txtBlkDescription.Tag);

      colDepositorCode.Header = Localization.Localize.LocalizeDefaultString("DEPOSITOR CODE");
      colDepositorAddress.Header = Localization.Localize.LocalizeDefaultString("DEPOSITOR ADDRESS");


      txtBlkAddNew.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkAddNew.Tag);
      btnAddNew.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnAddNew.Tag);

      txtBlkEdit.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkEdit.Tag);
      btnEdit.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnEdit.Tag);

      txtBlkDelete.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkDelete.Tag);
      btnDelete.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnDelete.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");

      CollectionViewSource.GetDefaultView(dgDepositorL.ItemsSource).Refresh();

    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    private void Cmd_DM_Depositor_GetAll()
    {
      DepositorL.Clear();
      btnEdit.IsEnabled = false;
      btnDelete.IsEnabled = false;

      CommandManagerC200153.DM_Depositor_GetAll(this, Filter);
    }
    private void DM_Depositor_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var artL = dwr.Data as List<C200153_Depositor>;
      if (artL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (artL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        artL = artL.Take(Filter.MaxItems).ToList();
        DepositorL.AddRange(artL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

    private void Cmd_DM_Depositor_Update(C200153_Depositor art)
    {
      CommandManagerC200153.DM_Depositor_Update(this, art);
    }
    private void DM_Depositor_Update(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C200153_CustomResult Result = (C200153_CustomResult)dwr.Data;

      if (Result == C200153_CustomResult.OK)
      {
        LocalSnackbar.ShowMessageOk("DEPOSITOR UPDATED".TD(), 2);
      }
      else
      {
        LocalSnackbar.ShowMessageFail("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(Result), 2);
      }

      Cmd_DM_Depositor_GetAll();
    }


    private void Cmd_DM_Depositor_Delete(C200153_Depositor art)
    {
      CommandManagerC200153.DM_Depositor_Delete(this, art);
    }
    private void DM_Depositor_Delete(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C200153_DepositorDeleteResults Result = (C200153_DepositorDeleteResults)dwr.Data;

      if (Result.DeletableResult == Results_Enum.OK)
      {
        if (Result.DeleteResult == C200153_CustomResult.OK)
        {
          LocalSnackbar.ShowMessageOk("ARTICLE DELETED".TD(), 2);
        }
        else
        {
          LocalSnackbar.ShowMessageFail("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(Result.DeleteResult), 2);
        }
      }
      else
      {
        LocalSnackbar.ShowMessageFail("RESULT".TD() + " : " + ResultToLocalizedStringConverter.Convert(Result.DeletableResult), 2);
      }

      Cmd_DM_Depositor_GetAll();
    }

    #endregion


    #region Eventi Ricerca

    private void ctrlDepositorFilter_OnSearch()
    {
      Cmd_DM_Depositor_GetAll();
    }

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_DM_Depositor_GetAll();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_DM_Depositor_GetAll();
    }

   

    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();

      btnEdit.IsEnabled = false;
      btnDelete.IsEnabled = false;
    }

    private void CtrlBaseC200153_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_Depositor)
      {
        try
        {
          Clipboard.SetText((((C200153_Depositor)b.Tag)).Code);
        }
        catch
        {

        }
      }
    }

    #endregion

    private void dgDepositorL_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (DepositorSelected != null)
      {
        btnEdit.IsEnabled = true;
        DepositorEdit = new C200153_Depositor()
            {
              Code = DepositorSelected.Code,
              Address = DepositorSelected.Address,
              IsDeleted = DepositorSelected.IsDeleted,
        };

        btnDelete.IsEnabled = !DepositorEdit.IsDeleted; ;
      }
      else
      {
        btnEdit.IsEnabled = false;
        btnDelete.IsEnabled = false;
      }     

    }

    private async void btnAddNew_Click(object sender, RoutedEventArgs e)
    {
      // prepare the view
      var view = new CtrlDepositorManagerInsert();

      //show the dialog
      await DialogHost.Show(view, "RootDialogWithoutExit");

      //OK, request update....
      if (view.Result == Model.Common.ShowDialogResults.OK)
        Cmd_DM_Depositor_Update(view.Depositor);

    }

    private async void btnEdit_Click(object sender, RoutedEventArgs e)
    {
      if(DepositorEdit != null)
      {
        if (!DepositorEdit.IsDeleted)
        {
          // prepare the view
          var view = new CtrlDepositorManagerEdit(DepositorEdit);

        //show the dialog
        await DialogHost.Show(view, "RootDialogWithoutExit");

        //OK, request update....
        if (view.Result == Model.Common.ShowDialogResults.OK)
          Cmd_DM_Depositor_Update(view.Depositor);

        }
        else
        {
          LocalSnackbar.ShowMessageFail("DEPOSITOR IS MARKED AS DELETED".TD(), 3);
        }
      }
      else
      {
        LocalSnackbar.ShowMessageFail("PLEASE SELECT ARTICLE TO EDIT".TD(),3);
      }
      
    }

    private async void btnDelete_Click(object sender, RoutedEventArgs e)
    {
      if (DepositorEdit != null)
      {
        if (!DepositorEdit.IsDeleted)
        {
          // prepare the view
          var view = new CtrlDepositorManagerDelete(DepositorEdit);

          //show the dialog
          await DialogHost.Show(view, "RootDialogWithoutExit");

          //OK, request update....
          if (view.Result == Model.Common.ShowDialogResults.OK)
            Cmd_DM_Depositor_Delete(view.Depositor);
        }
        else
        {
          LocalSnackbar.ShowMessageFail("DEPOSITOR IS MARKED AS DELETED".TD(), 3);
        }
      }
      else
      {
        LocalSnackbar.ShowMessageFail("PLEASE SELECT ARTICLE TO DELETE".TD(), 3);
      }

    }
  }
}
