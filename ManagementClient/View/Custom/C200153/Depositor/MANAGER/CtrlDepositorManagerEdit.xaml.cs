﻿using System.Windows;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Common;
using Model.Custom.C200153.Depositor;
using View.Common.Languages;

namespace View.Custom.C200153.DepositorManager
{
  /// <summary>
  /// Interaction logic for CtrlDepositorManagerEdit.xaml
  /// </summary>
  public partial class CtrlDepositorManagerEdit : CtrlBaseC200153
  {
    public ShowDialogResults Result { get; set; }

    private C200153_Depositor _Depositor;

    public C200153_Depositor Depositor
    {
      get { return _Depositor; }
      set
      {
        _Depositor = value;
        NotifyPropertyChanged("Depositor");

      }
    }

    #region CONSTRUCTOR
    public CtrlDepositorManagerEdit(C200153_Depositor depositor)
    {
      Depositor = depositor;

      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkSubTitle.Text = Context.Instance.TranslateDefault((string)txtBlkSubTitle.Tag);

      txtBlkDepositorCode.Text = Context.Instance.TranslateDefault((string)txtBlkDepositorCode.Tag);
      txtBlkDepositorAddress.Text = Context.Instance.TranslateDefault((string)txtBlkDepositorAddress.Tag);

      txtBlkCancel.Text = Context.Instance.TranslateDefault((string)txtBlkCancel.Tag);
      txtBlkConfirm.Text = Context.Instance.TranslateDefault((string)txtBlkConfirm.Tag);

    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {

      Translate();

    }
    #endregion



    #region EVENTI
    

    private void butCancel_Click(object sender, RoutedEventArgs e)
    {
      CloseWithResult(ShowDialogResults.CANCELED);
    }

    private async void butConfirm_Click(object sender, RoutedEventArgs e)
    {
      if (string.IsNullOrEmpty(Depositor.Code))
      {
        LocalSnackbar.ShowMessageFail("PLEASE INPUT A VALID DEPOSITOR CODE".TD(), 3);
      }
      else if (string.IsNullOrEmpty(Depositor.Address))
      {
        LocalSnackbar.ShowMessageFail("PLEASE INPUT A VALID DEPOSITOR DESCRIPTION".TD(), 3);
      }
      else
      {
        ConfirmationDialogArguments dialogArgs = new ConfirmationDialogArguments
        {
          Title = "UPDATE DEPOSITOR CONFIRM".TD(),
          Message = "Are you sure?".TD(),
          OkButtonLabel = "YES".TD(),
          CancelButtonLabel = "CANCEL".TD(),
          StackedButtons = false
        };

        bool result = await ConfirmationDialog.ShowDialogAsync("RootDialog", dialogArgs); 
        
        if (result)
          CloseWithResult(ShowDialogResults.OK);
        else
          CloseWithResult(ShowDialogResults.CANCELED);
      }

    }
    #endregion

    #region Private Methods
   
    
    void CloseWithResult(ShowDialogResults res)
    {
      Result = res;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }
    #endregion

  }
}
