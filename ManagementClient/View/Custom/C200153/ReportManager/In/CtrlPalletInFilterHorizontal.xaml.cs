﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
using Model.Custom.C200153;
using Model.Custom.C200153.Report;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletInFilterHorizontal.xaml
  /// </summary>
  public partial class CtrlPalletInFilterHorizontal : CtrlBaseC200153
  {
    #region EVENTI
    public delegate void OnSearchHandler();
    public event OnSearchHandler OnSearch;
    #endregion

    #region Fields
    private const int _indexStartValue = 0;
    #endregion

    #region PUBLIC PROPERTIES
    public C200153_PalletInFilter Filter { get; set; } = new C200153_PalletInFilter();
    #endregion
    
    #region Constructor
    public CtrlPalletInFilterHorizontal()
    {
      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      HintAssist.SetHint(cbMaxItems, Localization.Localize.LocalizeDefaultString((string)cbMaxItems.Tag));
      HintAssist.SetHint(txtBoxBatchCode, Localization.Localize.LocalizeDefaultString((string)txtBoxBatchCode.Tag));
      HintAssist.SetHint(txtBoxArticleCode, Localization.Localize.LocalizeDefaultString((string)txtBoxArticleCode.Tag));
      HintAssist.SetHint(txtBoxPosition, Localization.Localize.LocalizeDefaultString((string)txtBoxPosition.Tag));
      HintAssist.SetHint(txtBoxDepositorCode, Localization.Localize.LocalizeDefaultString((string)txtBoxDepositorCode.Tag));

      btnCancel.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnCancel.Tag);
      btnSearch.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnSearch.Tag);
    }

    #endregion TRADUZIONI

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    #endregion

    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
    }

    private void btnSearch_Click(object sender, RoutedEventArgs e)
    {
      if (cbMaxItems.SelectedItem != null)
      {
        Filter.MaxItems = (int)cbMaxItems.SelectedItem;
      }
      Filter.Index = _indexStartValue;
      Search();
    }

    #region private Methods
    private void Search()
    {
      OnSearch?.Invoke();
    }
    #endregion

    private void TextBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Return)
        Search();

    }
  }
}
