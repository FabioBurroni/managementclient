﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using LiveCharts;
using Model;
using Model.Custom.C200153.Report;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlDistributionFlowWhIn.xaml
  /// </summary>
  public partial class CtrlDistributionFlowWhIn : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public int StartIndex { get; set; } = _indexStartValue;
    public int MaxItems { get; set; } = _maxItemsStartValue;

    public C200153_Distribution_Storic_Filter Filter { get; set; } = new C200153_Distribution_Storic_Filter();

    public ObservableCollectionFast<C200153_Distribution_StoricFlowWhIn> StoricFlowWhInL { get; set; } = new ObservableCollectionFast<C200153_Distribution_StoricFlowWhIn>();
    public ObservableCollectionFast<C200153_Distribution_StoricFlowArticleWhIn> StoricFlowArticleWhInL { get; set; } = new ObservableCollectionFast<C200153_Distribution_StoricFlowArticleWhIn>();
    public ObservableCollectionFast<C200153_Distribution_StoricFlowWhIn> StoricFlowWhInMainAreaL { get; set; } = new ObservableCollectionFast<C200153_Distribution_StoricFlowWhIn>();

    #endregion

    #region COSTRUTTORE
    public CtrlDistributionFlowWhIn()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlDistributionFlowWhInFilterHorizontal.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      //txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      //chartStoricFlowWhInLocalArea.Title = Context.Instance.TranslateDefault("DISTRIBUTION BY LOCAL AREA");
      //chartStoricFlowWhInMainArea.Title = Context.Instance.TranslateDefault("DISTRIBUTION BY MAIN AREA");
      //chartStoricFlowArticleWhInLocalArea.Title = Context.Instance.TranslateDefault("DISTRIBUTION ARTICLE BY LOCAL AREA");

      txtStoricFlowWhInLocalArea.Text = Context.Instance.TranslateDefault("DISTRIBUTION BY LOCAL AREA");
      txtStoricFlowWhInMainArea.Text = Context.Instance.TranslateDefault("DISTRIBUTION BY MAIN AREA");
      txtStoricFlowArticleWhInLocalArea.Text = Context.Instance.TranslateDefault("DISTRIBUTION ARTICLE BY LOCAL AREA");

      colAreaCode_Pallet.Header = Localization.Localize.LocalizeDefaultString("AREA CODE");
      colAreaCode_PalletMain.Header = Localization.Localize.LocalizeDefaultString("AREA CODE");
      colAreaCode_Article.Header = Localization.Localize.LocalizeDefaultString("AREA CODE");

      colArticleCode_Article.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      
      colFloor_Pallet.Header = Localization.Localize.LocalizeDefaultString("FLOOR");
      colFloor_Article.Header = Localization.Localize.LocalizeDefaultString("FLOOR");

      colNumPallet_Pallet.Header = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLET");
      colNumPallet_PalletMain.Header = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLET");
      colNumPallet_Article.Header = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLET");

      colSelect_Article.Header = Localization.Localize.LocalizeDefaultString("SELECT");

      CollectionViewSource.GetDefaultView(dgDistributionArticleLocalArea.ItemsSource).Refresh();
      CollectionViewSource.GetDefaultView(dgStoricFlowWhIn.ItemsSource).Refresh();
      CollectionViewSource.GetDefaultView(dgStoricFlowWhInMainArea.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE
    private void CMD_RM_Distribution_StoricFlowWhIn()
    {

      CommandManagerC200153.RM_Distribution_StoricFlowWhIn(this,Filter);
    }
  
    private void RM_Distribution_StoricFlowWhIn(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var listLocalArea = dwr.Data as List<C200153_Distribution_StoricFlowWhIn>;
      if (listLocalArea != null)
      {
        //...DISTRIBUZIONE AREA LOCAL
        listLocalArea = listLocalArea.OrderBy(rec => rec.AreaCodeFull).ToList();
        StoricFlowWhInL.AddRange(listLocalArea);

        //...dati per grafico
        List<KeyValuePair<string, int>> dataLocalArea = new List<KeyValuePair<string, int>>();
        dataLocalArea.AddRange(listLocalArea.Select(element => new KeyValuePair<string, int>(element.AreaCodeFull, element.NumPallet)));
        //ColumnSeries csLocalArea = new ColumnSeries();
        //csLocalArea.Title = Filter.StartDate.ToString("HH:mm:ss");
        //csLocalArea.DependentValuePath = "Value";
        //csLocalArea.IndependentValuePath = "Key";
        //csLocalArea.ItemsSource = dataLocalArea;
        //chartStoricFlowWhInLocalArea.Series.Add(csLocalArea);

        foreach (var l in dataLocalArea)
        {
            ccDistributionStoricFlowWhInLocalArea.Series.Add(new LiveCharts.Wpf.ColumnSeries
            {
              Title = l.Key,
              Values = new ChartValues<double> { l.Value }
            });
        }

        //...DISTRIBUZIONE AREA MAIN
        //...raggruppamento per area Main
        var listMainArea = listLocalArea.GroupBy(g => g.AreaCode).Select(gr => new C200153_Distribution_StoricFlowWhIn() { AreaCode = gr.Key, NumPallet = gr.ToList().Sum(s => s.NumPallet) }).ToList();
        StoricFlowWhInMainAreaL.AddRange(listMainArea);
        //...dati per grafico
        List<KeyValuePair<string, int>> dataMainArea = new List<KeyValuePair<string, int>>();
        dataMainArea.AddRange(listMainArea.Select(element => new KeyValuePair<string, int>(element.AreaCode, element.NumPallet)));
        //ColumnSeries csMainArea = new ColumnSeries();
        //csMainArea.Title = Filter.StartDate.ToString("HH:mm:ss");
        //csMainArea.DependentValuePath = "Value";
        //csMainArea.IndependentValuePath = "Key";
        //csMainArea.ItemsSource = dataMainArea;
        //chartStoricFlowWhInMainArea.Series.Add(csMainArea);

        foreach (var l in dataMainArea)
        {
            ccStoricFlowWhInMainArea.Series.Add(new LiveCharts.Wpf.ColumnSeries
            {
              Title = l.Key,
              Values = new ChartValues<double> { l.Value }
            });
        }


          CommandManagerC200153.RM_Distribution_StoricFlowArticleWhIn(this, Filter);
      }

    }

    private void RM_Distribution_StoricFlowArticleWhIn(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var listLocalAreaArticle = dwr.Data as List<C200153_Distribution_StoricFlowArticleWhIn>;
      if (listLocalAreaArticle != null)
      {
        //...DISTRIBUZIONE article AREA LOCAL
        StoricFlowArticleWhInL.AddRange(listLocalAreaArticle);
       
      }

    }
    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    #endregion


    #region Eventi Ricerca
    private void ctrlDistributionFlowWhInFilterHorizontal_OnSearch()
    {
      StoricFlowWhInL.Clear();
      StoricFlowWhInMainAreaL.Clear();
      StoricFlowArticleWhInL.Clear();
      //chartStoricFlowWhInLocalArea.Series.Clear();
      //chartStoricFlowWhInMainArea.Series.Clear();
      //chartStoricFlowArticleWhInLocalArea.Series.Clear();

      ccDistributionStoricFlowWhInLocalArea.Series.Clear();
      ccStoricFlowWhInMainArea.Series.Clear();
      ccStoricFlowArticleWhInLocalArea.Series.Clear();

      CMD_RM_Distribution_StoricFlowWhIn();

    }


    private void dgStoricFlowWhIn_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
    {
      //ViewDistributionArticleByMainArea.Clear();

      //DataGrid dg = sender as DataGrid;
      //var item = dg.SelectedItem;
      //if (item != null)
      //{
      //  C200153_DistributionArticleByMainArea da = item as C200153_DistributionArticleByMainArea;
      //  var article = da.ArticleCode;
      //  var list = DistributionArticleByMainArea.Where(dad => dad.ArticleCode == article).ToList();
      //  foreach (var l in list)
      //  {
      //    ViewDistributionArticleByMainArea.Add(new C200153_DistributionArticleView() { Area = l.AreaCode, Percent = l.Percent });
      //  }
      //}
    }


    #endregion

    private void butRight_Click(object sender, RoutedEventArgs e)
    {
      //chartStoricFlowArticleWhInLocalArea.Series.Clear();
      ccStoricFlowArticleWhInLocalArea.Series.Clear();

      var listLocalAreaArticle = StoricFlowArticleWhInL.Where(x=> x.Selected);

      if (listLocalAreaArticle == null)
        return;
      //...dati per grafico
       List<KeyValuePair<string, int>> dataArticleLocalArea = new List<KeyValuePair<string, int>>();
      dataArticleLocalArea.AddRange(listLocalAreaArticle.Select(element => new KeyValuePair<string, int>(element.AreaCodeFull + "_" + element.ArticleCode, element.NumPallet)));
      //ColumnSeries csArticleLocalArea = new ColumnSeries();
      //csArticleLocalArea.Title = Filter.StartDate.ToString("HH:mm:ss");
      //csArticleLocalArea.DependentValuePath = "Value";
      //csArticleLocalArea.IndependentValuePath = "Key";
      //csArticleLocalArea.ItemsSource = dataArticleLocalArea;
      //chartStoricFlowArticleWhInLocalArea.Series.Add(csArticleLocalArea);

      foreach (var l in dataArticleLocalArea)
      {
          ccStoricFlowArticleWhInLocalArea.Series.Add(new LiveCharts.Wpf.ColumnSeries
          {
            Title = l.Key,
            Values = new ChartValues<double> { l.Value }
          });
      }

    }
  }
}
