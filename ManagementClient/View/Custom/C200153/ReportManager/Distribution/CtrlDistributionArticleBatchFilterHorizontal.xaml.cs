﻿using System.Windows;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using Model.Custom.C200153.Report;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletInFilterHorizontal.xaml
  /// </summary>
  public partial class CtrlDistributionArticleBatchFilterHorizontal : CtrlBaseC200153
  {
    #region EVENTI
    public delegate void OnSearchHandler();
    public event OnSearchHandler OnSearch;
    #endregion

    #region Fields
    private const int _indexStartValue = 0;
    #endregion

    #region PUBLIC PROPERTIES
    public C200153_DistributionArticleBatchFilter Filter { get; set; } = new C200153_DistributionArticleBatchFilter();
    #endregion
    
    #region Constructor
    public CtrlDistributionArticleBatchFilterHorizontal()
    {
      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      HintAssist.SetHint(cbMaxItems, Localization.Localize.LocalizeDefaultString((string)cbMaxItems.Tag));
      HintAssist.SetHint(txtBoxBatchCode, Localization.Localize.LocalizeDefaultString((string)txtBoxBatchCode.Tag));
      HintAssist.SetHint(txtBoxArticleCode, Localization.Localize.LocalizeDefaultString((string)txtBoxArticleCode.Tag));

      btnCancel.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnCancel.Tag);
      btnSearch.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnSearch.Tag);
    }

    #endregion TRADUZIONI

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    #endregion

    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
    }

    private void btnSearch_Click(object sender, RoutedEventArgs e)
    {
      if (cbMaxItems.SelectedItem != null)
      {
        Filter.MaxItems = (int)cbMaxItems.SelectedItem;
      }
      Filter.Index = _indexStartValue;
      Search();
    }

    #region private Methods
    private void Search()
    {
      OnSearch?.Invoke();
    }
    #endregion

    private void TextBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Return)
        Search();

    }
  }
}
