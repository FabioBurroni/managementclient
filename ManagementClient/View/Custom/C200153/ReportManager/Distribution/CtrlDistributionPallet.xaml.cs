﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using LiveCharts;
using LiveCharts.Wpf;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.Report;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlDistributionPallet.xaml
  /// </summary>
  public partial class CtrlDistributionPallet : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
  
    public ObservableCollectionFast<C200153_DistributionPallet> DistributionPalletLocalArea { get; set; } = new ObservableCollectionFast<C200153_DistributionPallet>();
    public ObservableCollectionFast<C200153_DistributionPallet> DistributionPalletMainArea { get; set; } = new ObservableCollectionFast<C200153_DistributionPallet>();

    #endregion

    #region COSTRUTTORE
    public CtrlDistributionPallet()
    {     
      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkAreaLocal.Text = Context.Instance.TranslateDefault((string)txtBlkAreaLocal.Tag);
      txtBlkAreaMain.Text = Context.Instance.TranslateDefault((string)txtBlkAreaMain.Tag);

      colLocAreaCode.Header = Localization.Localize.LocalizeDefaultString("AREA CODE");
      colLocAreaFloorNumber.Header = Localization.Localize.LocalizeDefaultString("FLOOR NUMBER");
      colLocAreaNumPallet.Header = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLET");
      colLocAreaPercent.Header = Localization.Localize.LocalizeDefaultString("PERCENT");

      colMainAreaCode.Header = Localization.Localize.LocalizeDefaultString("AREA CODE");
      colMainAreaNumPallet.Header = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLET");
      colMainAreaPercent.Header = Localization.Localize.LocalizeDefaultString("PERCENT");

      butRefresh.ToolTip = Context.Instance.TranslateDefault((string)butRefresh.Tag);

      CollectionViewSource.GetDefaultView(dgLocalArea.ItemsSource).Refresh();
      CollectionViewSource.GetDefaultView(dgMainArea.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE
    private void Cmd_RM_Distribution_Pallet_GetLocal()
    {

      CommandManagerC200153.RM_Distribution_Pallet_GetLocal(this);
    }
    private void RM_Distribution_Pallet_GetLocal(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var locL = dwr.Data as List<C200153_DistributionPallet>;
      if(locL != null)
      {
        int totalPallet = locL.Sum(da => da.NumPallet);
        locL.ForEach(d => d.TotalPallet = totalPallet);

        DistributionPalletLocalArea.AddRange(locL.OrderBy(d => d.AreaCodeFull));

        foreach (var value in DistributionPalletLocalArea)
        {
          pieChartDistributionPalletLocalArea.Series.Add(new PieSeries { Title = value.AreaCode, StrokeThickness = 0, Values = new ChartValues<double> { Math.Round(value.Percent, 2) } });
        }

        var distMainArea = locL.GroupBy(da => da.AreaCode, da => da.NumPallet, (k, v) => new C200153_DistributionPallet() { AreaCode = k, NumPallet = v.Sum() }).ToList();
        distMainArea.ForEach(da => da.TotalPallet = totalPallet);
        DistributionPalletMainArea.AddRange(distMainArea);

        foreach (var value in DistributionPalletMainArea)
        {
          pieChartDistributionPalletMainArea.Series.Add(new PieSeries { Title = value.AreaCode, StrokeThickness = 0, Values = new ChartValues<double> { Math.Round(value.Percent, 2) } });
        }
      }
      
    }

    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    #endregion


    #region Eventi Ricerca
    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      DistributionPalletLocalArea.Clear();
      DistributionPalletMainArea.Clear();
      Cmd_RM_Distribution_Pallet_GetLocal();

      pieChartDistributionPalletMainArea.Series.Clear();
      pieChartDistributionPalletLocalArea.Series.Clear();

    }
    #endregion

  }
}
