﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.Report;
using Utilities.Extensions.MoreLinq;
using View.Common.Languages;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlDistributionPalletFlowFilterHorizontal.xaml
  /// </summary>
  public partial class CtrlDistributionPalletFlowFilterHorizontal : CtrlBaseC200153
  {
    #region EVENTI
    public delegate void OnSearchHandler();
    public event OnSearchHandler OnSearch;
    #endregion

    #region Fields
    private const int _indexStartValue = 0;
    #endregion

    #region PUBLIC PROPERTIES
    public C200153_DistributionPalletFlowFilter Filter { get; set; } = new C200153_DistributionPalletFlowFilter();

    public ObservableCollectionFast<C200153_Position> InputPositionL = new ObservableCollectionFast<C200153_Position>();

    public ObservableCollectionFast<C200153_Position> OutputPositionL = new ObservableCollectionFast<C200153_Position>();

    public ObservableCollectionFast<C200153_Position> RejectPositionL = new ObservableCollectionFast<C200153_Position>();

    #region Press to search highlight

    private bool searchHighlight = true;

    public bool SearchHighlight
    {
      get { return searchHighlight; }
      set
      {
        searchHighlight = value;
        NotifyPropertyChanged("SearchHighlight");
      }
    }

    #endregion
    #endregion

    #region Constructor
    public CtrlDistributionPalletFlowFilterHorizontal()
    {
      InitializeComponent();

      Cmd_RM_InputPositions_GetAll();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      HintAssist.SetHint(txtBoxBatchCode, Localization.Localize.LocalizeDefaultString((string)txtBoxBatchCode.Tag));
      HintAssist.SetHint(txtBoxArticleCode, Localization.Localize.LocalizeDefaultString((string)txtBoxArticleCode.Tag));
      HintAssist.SetHint(txtBoxInputPosition, Localization.Localize.LocalizeDefaultString((string)txtBoxInputPosition.Tag));
      HintAssist.SetHint(txtBoxOutputPosition, Localization.Localize.LocalizeDefaultString((string)txtBoxOutputPosition.Tag));
      HintAssist.SetHint(txtBoxRejectPosition, Localization.Localize.LocalizeDefaultString((string)txtBoxRejectPosition.Tag));

      btnCancel.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnCancel.Tag);
      btnSearch.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnSearch.Tag);

      ctrlDateStart.Title= Localization.Localize.LocalizeDefaultString((string)ctrlDateStart.Tag);
      ctrlDateEnd.Title= Localization.Localize.LocalizeDefaultString((string)ctrlDateEnd.Tag);

      UpdatePositionList(spInputPositions, InputPositionL, "INPUT");
      UpdatePositionList(spOutputPositions, OutputPositionL, "OUTPUT");
      UpdatePositionList(spRejectPositions, RejectPositionL, "REJECT");
    }

    #endregion TRADUZIONI

    #region Comandi e Risposte
    private void Cmd_RM_InputPositions_GetAll()
    {
      InputPositionL.Clear();

      CommandManagerC200153.RM_InputPositions_GetAll(this);
    }
    private void RM_InputPositions_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var posL = dwr.Data as List<C200153_Position>;
      if (posL != null)
      {
        InputPositionL.AddRange(posL);
      }

      UpdatePositionList(spInputPositions, InputPositionL, "INPUT");

      Cmd_RM_ExitPositions_GetAll();

    }


    private void Cmd_RM_ExitPositions_GetAll()
    {
      RejectPositionL.Clear();

      CommandManagerC200153.RM_ExitPositions_GetAll(this);
    }
    private void RM_ExitPositions_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var posL = dwr.Data as List<C200153_Position>;
      if (posL != null)
      {
        RejectPositionL.AddRange(posL);
      }

      UpdatePositionList(spRejectPositions, RejectPositionL, "REJECT");

      Cmd_RM_ListDestinationPositions_GetAll();
    }

    private void Cmd_RM_ListDestinationPositions_GetAll()
    {
      OutputPositionL.Clear();

      CommandManagerC200153.RM_ListDestinationPositions_GetAll(this);
    }
    private void RM_ListDestinationPositions_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var posL = dwr.Data as List<C200153_Position>;
      if (posL != null)
      {
        OutputPositionL.AddRange(posL);
      }

      UpdatePositionList(spOutputPositions, OutputPositionL, "OUTPUT");

    }
    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    #endregion

    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
    }

    private void btnSearch_Click(object sender, RoutedEventArgs e)
    {
      Search();
    }

    #region private Methods
    private void Search()
    {
      OnSearch?.Invoke();
    }
    #endregion

    private void TextBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Return)
        Search();

    }

    private void UpdatePositionList(StackPanel spPositions, ObservableCollectionFast<C200153_Position> PositionL, string type)
    {
      //Clear buttons stack panel
      spPositions.Children.Clear();

      foreach (var pos in PositionL)
      {
        Button btn = new Button();
        btn.Name = "btn" + pos.Code;
        btn.Tag = pos;
        btn.ToolTip = "PRESS TO SELECT".TD();
        btn.Width = 100;
        btn.Height = 100;
        btn.Padding = new Thickness(10);
        btn.Margin = new Thickness(10, 0, 10, 0);
        if (type == "INPUT")
          btn.Click += BtnInput_Click;
        else if (type == "INPUT")
          btn.Click += BtnOutput_Click;
        else if (type == "REJECT")
          btn.Click += BtnReject_Click;

        btn.Style = this.FindResource("Custom_MaterialDesignFloatingActionButton") as Style;

        //costruisco il content
        TextBlock txtBlkPositionCode = new TextBlock();
        txtBlkPositionCode.Foreground = Brushes.White;
        txtBlkPositionCode.HorizontalAlignment = HorizontalAlignment.Center;
        txtBlkPositionCode.Text = pos.Code;


        TextBlock txtBlkPositionDescr = new TextBlock();
        txtBlkPositionDescr.Foreground = Brushes.White;
        txtBlkPositionDescr.HorizontalAlignment = HorizontalAlignment.Center;
        txtBlkPositionDescr.Text = pos.Description.TD();

        //add descr to viewbox
        Viewbox vbDescr = new Viewbox();
        vbDescr.Child = txtBlkPositionDescr;

        // add text code and descr to stack panel
        StackPanel spBtnContent = new StackPanel();
        spBtnContent.HorizontalAlignment = HorizontalAlignment.Center;
        spBtnContent.VerticalAlignment = VerticalAlignment.Center;
        spBtnContent.Children.Add(txtBlkPositionCode);
        spBtnContent.Children.Add(vbDescr);

        //add stack panel to viewbox
        Viewbox vb = new Viewbox();
        vb.Child = spBtnContent;

        // Add viewbox content to button
        btn.Content = vb;

        //aggiungo button allo stack panel già dichiarato nello Xaml
        spPositions.Children.Add(btn);

      }

    }

    private void BtnInput_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_Position)
      {
        try
        {
          Filter.InputPosition.Code = ((C200153_Position)b.Tag).Code;
          Filter.InputPosition.Description = ((C200153_Position)b.Tag).Description;
        }
        catch
        {

        }
      }

    }

    private void BtnOutput_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_Position)
      {
        try
        {
          Filter.OutputPosition.Code = ((C200153_Position)b.Tag).Code;
          Filter.OutputPosition.Description = ((C200153_Position)b.Tag).Description;
        }
        catch
        {

        }
      }

    }

    private void BtnReject_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_Position)
      {
        try
        {
          Filter.RejectPosition.Code = ((C200153_Position)b.Tag).Code;
          Filter.RejectPosition.Description = ((C200153_Position)b.Tag).Description;
        }
        catch
        {

        }
      }

    }

    private void txtBoxOutputPosition_TextChanged(object sender, TextChangedEventArgs e)
    {
      TextBox b = sender as TextBox;

      if (b.Text.IsNullOrEmpty())
        Filter.OutputPosition = new C200153_Position();
    }

    private void txtBoxInputPosition_TextChanged(object sender, TextChangedEventArgs e)
    {
      TextBox b = sender as TextBox;

      if (b.Text.IsNullOrEmpty())
        Filter.InputPosition = new C200153_Position();
    }

    private void txtBoxRejectPosition_TextChanged(object sender, TextChangedEventArgs e)
    {
      TextBox b = sender as TextBox;

      if (b.Text.IsNullOrEmpty())
        Filter.RejectPosition = new C200153_Position();
    }
  }
}
