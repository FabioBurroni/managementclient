﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using LiveCharts;
using LiveCharts.Wpf;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.Report;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlDistributionArticle.xaml
  /// </summary>
  public partial class CtrlDistributionArticleBatchByMainAndLocalArea : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties

    public ObservableCollectionFast<C200153_DistributionArticleBatchByLocalArea> DistributionLocal { get; set; } = new ObservableCollectionFast<C200153_DistributionArticleBatchByLocalArea>();
    public ObservableCollectionFast<C200153_DistributionArticleBatchView> ViewDistributionLocal { get; set; } = new ObservableCollectionFast<C200153_DistributionArticleBatchView>();
    public ObservableCollectionFast<C200153_DistributionArticleBatchByMainArea> DistributionMain { get; set; } = new ObservableCollectionFast<C200153_DistributionArticleBatchByMainArea>();
    public ObservableCollectionFast<C200153_DistributionArticleBatchView> ViewDistributionMain { get; set; } = new ObservableCollectionFast<C200153_DistributionArticleBatchView>();


    public int StartIndex { get; set; } = _indexStartValue;
    public int MaxItems { get; set; } = _maxItemsStartValue;

    public C200153_DistributionArticleBatchFilter Filter { get; set; } = new C200153_DistributionArticleBatchFilter();
    #endregion

    #region COSTRUTTORE
    public CtrlDistributionArticleBatchByMainAndLocalArea()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlDistributionArticleBatchFilterHorizontal.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtDistributionLocal.Text = Context.Instance.TranslateDefault("DISTRIBUTION BY LOCAL AREA");
      txtDistributionMain.Text = Context.Instance.TranslateDefault("DISTRIBUTION BY MAIN AREA");

      colAreaCode.Header = Localization.Localize.LocalizeDefaultString("AREA CODE");
      colAreaCode_Art.Header = Localization.Localize.LocalizeDefaultString("AREA CODE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE");
      colArticleCode_Art.Header = Localization.Localize.LocalizeDefaultString("ARTICLE");
      colBatchCodeMain.Header = Localization.Localize.LocalizeDefaultString("BATCH");
      colBatchCodeLocal.Header = Localization.Localize.LocalizeDefaultString("BATCH");
      colFloor.Header = Localization.Localize.LocalizeDefaultString("FLOOR");
      colNumPallet.Header = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLET");
      colNumPallet_Art.Header = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLET");
      colPercent.Header = Localization.Localize.LocalizeDefaultString("PERCENT");
      colPercent_Art.Header = Localization.Localize.LocalizeDefaultString("PERCENT");
      colTotalPallet.Header = Localization.Localize.LocalizeDefaultString("TOTAL PALLET");
      colTotalPallet_Art.Header = Localization.Localize.LocalizeDefaultString("TOTAL PALLET");
      CollectionViewSource.GetDefaultView(dgDistributionLocal.ItemsSource).Refresh();
      CollectionViewSource.GetDefaultView(dgDistributionMain.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE
    private void Cmd_RM_Distribution_ArticleBatch_ByMainArea()
    {
      CommandManagerC200153.RM_Distribution_ArticleBatch_ByMainArea(this, Filter);
    }
    private void RM_Distribution_ArticleBatch_ByMainArea(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var mainL = dwr.Data as List<C200153_DistributionArticleBatchByMainArea>;
      if (mainL != null)
      {
        //var gr = mainL.GroupBy(da => (da.ArticleCode, da.BatchCode));  //TUPLE
        var gr = mainL.GroupBy(da => new { da.ArticleCode, da.BatchCode });
     
        mainL.ForEach(d => d.TotalPallet = gr.FirstOrDefault(g => g.Key.ArticleCode == d.ArticleCode && g.Key.BatchCode == d.BatchCode ).Sum(p => p.NumPallet));
        DistributionMain.AddRange(mainL);

      }

    }

    private void Cmd_RM_Distribution_ArticleBatch_ByLocalArea(string article, string batch)
    {
      C200153_DistributionArticleBatchFilter localSearchFilter = new C200153_DistributionArticleBatchFilter();
      localSearchFilter.ArticleCode = article;
      localSearchFilter.BatchCode = batch;

      CommandManagerC200153.RM_Distribution_ArticleBatch_ByLocalArea(this, localSearchFilter);
    }

    private void RM_Distribution_ArticleBatch_ByLocalArea(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var locL = dwr.Data as List<C200153_DistributionArticleBatchByLocalArea>;
      if(locL != null)
      {
        var gr = locL.GroupBy(da => new { da.ArticleCode, da.BatchCode });
        locL.ForEach(d => d.TotalPallet = gr.FirstOrDefault(g => g.Key.ArticleCode == d.ArticleCode && g.Key.BatchCode == d.BatchCode).Sum(p => p.NumPallet));
        DistributionLocal.AddRange(locL);

        UpdateLocalView();

      }
      
    }
    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    #endregion


    #region Eventi Ricerca
   
    private void UpdateLocalView()
    {
      ViewDistributionLocal.Clear();
      ccDistributionLocal.Series.Clear();

      if (DistributionLocal != null)
      {
        foreach (var l in DistributionLocal)
        {
          ViewDistributionLocal.Add(new C200153_DistributionArticleBatchView() { Area = l.AreaCode + "-" + l.Floor.ToString(), Percent = Math.Round(l.Percent, 2) });

          ccDistributionLocal.Series.Add(new ColumnSeries
          {
            Title = l.AreaCode + "-" + l.Floor.ToString(),
            Values = new ChartValues<double> { l.Percent }
          });
        }
      }
    }


    #endregion

    private void ctrlDistributionArticleBatchFilterHorizontal_OnSearch()
    {
      DistributionMain.Clear();
      ViewDistributionMain.Clear();
      pieChartDistributionMain.Series.Clear();

      DistributionLocal.Clear();
      ViewDistributionLocal.Clear();
      ccDistributionLocal.Series.Clear();

      Cmd_RM_Distribution_ArticleBatch_ByMainArea();
    }

    private void dgDistributionMain_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
    {
      ViewDistributionMain.Clear();
      pieChartDistributionMain.Series.Clear();

      DataGrid dg = sender as DataGrid;
      var item = dg.SelectedItem;
      if (item != null)
      {
        C200153_DistributionArticleBatchByMainArea da = item as C200153_DistributionArticleBatchByMainArea;
        var article = da.ArticleCode;
        var batch = da.BatchCode;
        var list = DistributionMain.Where(dad => dad.ArticleCode == article && dad.BatchCode == batch).ToList();
        foreach (var l in list)
        {
          ViewDistributionMain.Add(new C200153_DistributionArticleBatchView() { Area = l.AreaCode, Percent = l.Percent });

          pieChartDistributionMain.Series.Add(new PieSeries { Title = l.AreaCode, StrokeThickness = 0, Values = new ChartValues<double> { Math.Round(l.Percent, 2) } });
        }

        DistributionLocal.Clear();
        ViewDistributionLocal.Clear();
        ccDistributionLocal.Series.Clear();

        Cmd_RM_Distribution_ArticleBatch_ByLocalArea(article, batch);
      }
    }

  }
}
