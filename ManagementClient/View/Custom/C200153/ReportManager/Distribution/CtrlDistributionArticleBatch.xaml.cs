﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using LiveCharts;
using LiveCharts.Wpf;
using MaterialDesignExtensions.Controls;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.Report;
using View.Common.Languages;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlDistributionArticleBatch.xaml
  /// </summary>
  public partial class CtrlDistributionArticleBatch : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    #endregion

    #region Public Properties

    public ObservableCollectionFast<C200153_DistributionArticleBatchByMainArea> DistributionArticleByMainArea { get; set; } = new ObservableCollectionFast<C200153_DistributionArticleBatchByMainArea>();
    public ObservableCollectionFast<C200153_DistributionArticleBatchView> ViewDistributionArticleByMainArea { get; set; } = new ObservableCollectionFast<C200153_DistributionArticleBatchView>();

    #endregion

    #region COSTRUTTORE
    public CtrlDistributionArticleBatch()
    {     
      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      //chDistribution.Title = Context.Instance.TranslateDefault("DISTRIBUTION");
      //chDistribution2.Title = Context.Instance.TranslateDefault("DISTRIBUTION");

      txtDistribution.Text = Context.Instance.TranslateDefault("DISTRIBUTION");
      txtDistribution2.Text = Context.Instance.TranslateDefault("DISTRIBUTION");

      colAreaCode.Header = Localization.Localize.LocalizeDefaultString("AREA CODE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colNumPallet.Header = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLET");
      colPercent.Header = Localization.Localize.LocalizeDefaultString("PERCENT");
      colTotalPallet.Header = Localization.Localize.LocalizeDefaultString("TOTAL PALLET");
      
      butRefresh.ToolTip = Context.Instance.TranslateDefault((string)butRefresh.Tag);
      CollectionViewSource.GetDefaultView(dgDistributionArticleMainArea.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE
    private void Cmd_RM_Distribution_Article_Get()
    {

      //CommandManagerC200153.RM_Distribution_Articles_Batch_ByMainArea(this);
    }
  
    private void RM_Distribution_Articles_Batch_ByMainArea(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var mainL = dwr.Data as List<C200153_DistributionArticleBatchByMainArea>;
      if (mainL != null)
      {
        var gr = mainL.GroupBy(da => da.ArticleCode);
        mainL.ForEach(d => d.TotalPallet = gr.FirstOrDefault(g => g.Key == d.ArticleCode).Sum(p => p.NumPallet));
        DistributionArticleByMainArea.AddRange(mainL);

      }

    }
    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    #endregion


    #region Eventi Ricerca
    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      DistributionArticleByMainArea.Clear();
      ViewDistributionArticleByMainArea.Clear();

      pieChartDistribution.Series.Clear();
      ccDistribution.Series.Clear();

      Cmd_RM_Distribution_Article_Get();

    }
     

    private async void dgDistributionArticleMainArea_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
    {
      ViewDistributionArticleByMainArea.Clear();
      pieChartDistribution.Series.Clear();
      ccDistribution.Series.Clear();

      try
      {

        DataGrid dg = sender as DataGrid;
        var item = dg.SelectedItem;
        if (item != null)
        {
          C200153_DistributionArticleBatchByMainArea da = item as C200153_DistributionArticleBatchByMainArea;
          var article = da.ArticleCode;
          var list = DistributionArticleByMainArea.Where(dad => dad.ArticleCode == article).ToList();
          foreach (var l in list)
          {
            ViewDistributionArticleByMainArea.Add(new C200153_DistributionArticleBatchView() { Area = l.AreaCode, Percent = l.Percent });
            pieChartDistribution.Series.Add(new PieSeries { Title = l.AreaCode, StrokeThickness = 0, Values = new ChartValues<double> { Math.Round(l.Percent, 2) } });

              ccDistribution.Series.Add(new ColumnSeries
              {
                Title = l.AreaCode,
                Values = new ChartValues<double> { Math.Round(l.Percent, 2) }
              });
          }
        }
      }
      catch(Exception ex)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "EXCEPTION".TD(),
          Message = ex.ToString().TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

      }
    }

    #endregion

  }
}
