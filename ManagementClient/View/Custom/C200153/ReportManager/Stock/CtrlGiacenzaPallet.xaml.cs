﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200153.Report;
using Model.Export;
using View.Common.Languages;
using View.Common.Report;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlStockPallet.xaml
  /// </summary>
  public partial class CtrlStockPallet : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public C200153_StockPalletFilter Filter { get; set; } = new C200153_StockPalletFilter();

    public ObservableCollectionFast<C200153_StockPallet> GiacL { get; set; } = new ObservableCollectionFast<C200153_StockPallet>();
    #endregion

    #region COSTRUTTORE
    public CtrlStockPallet()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlStockFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
      colPosition.Header = Localization.Localize.LocalizeDefaultString("POSITION");
      colDateBorn.Header = Localization.Localize.LocalizeDefaultString("DATE BORN"); 
      colBatchCode.Header = Localization.Localize.LocalizeDefaultString("BATCH CODE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colArticleDescription.Header = Localization.Localize.LocalizeDefaultString("ARTICLE DESCRIPTION");
      colDepositor.Header = Localization.Localize.LocalizeDefaultString("DEPOSITOR");
      colProductionDate.Header = Localization.Localize.LocalizeDefaultString("PRODUCTION DATE");
      colExpiryDate.Header = Localization.Localize.LocalizeDefaultString("EXPIRY DATE");

      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");

      CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE
    private void Cmd_RM_Stock_Pallet_ByArea()
    {
      busyOverlay.IsBusy = true;
      GiacL.Clear();
      CommandManagerC200153.RM_Stock_Pallet_ByArea(this,Filter);
    }
    private void RM_Stock_Pallet_ByArea(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var giacL = dwr.Data as List<C200153_StockPallet>;
      if(giacL!=null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (giacL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        giacL = giacL.Take(Filter.MaxItems).ToList();
        GiacL.AddRange(giacL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

     #endregion

    #region Eventi controllo
      private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    private void CtrlBaseC200153_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    #endregion


    #region Eventi Ricerca
    private void ctrlStockFilter_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;
      ctrlStockFilter.SearchHighlight = false;

      Cmd_RM_Stock_Pallet_ByArea();
    }
    #endregion

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_RM_Stock_Pallet_ByArea();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_RM_Stock_Pallet_ByArea();
    }

    private void butCopyArticle_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_StockPallet)
      {
        try
        {
          Clipboard.SetText((((C200153_StockPallet)b.Tag)).ArticleCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyPalletCode_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_StockPallet)
      {
        try
        {
          Clipboard.SetText((((C200153_StockPallet)b.Tag)).Code);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }
    private void butCopyBatch_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_StockPallet)
      {
        try
        {
          Clipboard.SetText((((C200153_StockPallet)b.Tag)).BatchCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private async void butExport_Click(object sender, RoutedEventArgs e)
    {
      var view = new CtrlReportExport("PalletStock", GiacL.Select(g => new Exportable_PalletStock(g)).Cast<IExportable>().ToArray(), typeof(Exportable_PalletStock));

      //show the dialog
      var result = await DialogHost.Show(view, "RootDialog");

      if (view.Result == ExportResult.OK)
        LocalSnackbar.ShowMessageOkAndOpenFile("EXPORT COMPLETED".TD() + " " + view.FileCreatedName, 5, "OPEN", view.FileCreatedName);
    }

  }

  public class Exportable_PalletStock : IExportable
  {

    public Exportable_PalletStock(C200153_StockPallet palIn)
    {
      PalletCode = palIn.Code;
      DateBorn = palIn.DateBorn.ToString();
      Position = palIn.MapperEntityCode; 
      BatchCode = palIn.BatchCode.ToString();
      ArticleCode = palIn.ArticleCode.ToString();
      ArticleDescription = palIn.ArticleDescr.ToString();
      Depositor = palIn.Depositor.ToString();
      ProductionDate = palIn.ProductionDate.ToString();
      ExpiryDate = palIn.ExpiryDate.ToString();
    }

    [Exportation(ColumnIndex = 0, ColumnName = "PALLET CODE")]
    public string PalletCode { get; set; }

    [Exportation(ColumnIndex = 1, ColumnName = "DATE BORN")]
    public string DateBorn { get; set; }

    [Exportation(ColumnIndex = 2, ColumnName = "POSITION")]
    public string Position { get; set; }

    [Exportation(ColumnIndex = 3, ColumnName = "BATCH CODE")]
    public string BatchCode { get; set; }

    [Exportation(ColumnIndex = 4, ColumnName = "ARTICLE CODE")]
    public string ArticleCode { get; set; }

    [Exportation(ColumnIndex = 5, ColumnName = "ARTICLE DESCRIPTION")]
    public string ArticleDescription { get; set; }

    [Exportation(ColumnIndex = 6, ColumnName = "DEPOSITOR")]
    public string Depositor { get; set; }

    [Exportation(ColumnIndex = 7, ColumnName = "PRODUCTION DATE")]
    public string ProductionDate { get; set; }

    [Exportation(ColumnIndex = 8, ColumnName = "EXPIRY DATE")]
    public string ExpiryDate { get; set; }


  }
}
