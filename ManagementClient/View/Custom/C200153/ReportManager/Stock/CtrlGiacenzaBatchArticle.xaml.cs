﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200153.Report;
using Model.Export;
using Utilities.Extensions;
using View.Common.Languages;
using View.Common.Report;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlStockLottoArticolo.xaml
  /// </summary>
  public partial class CtrlStockBatchArticle : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public C200153_StockArticleBatchFilter Filter { get; set; } = new C200153_StockArticleBatchFilter();

     public ObservableCollectionFast<C200153_StockArticleBatch> GiacL { get; set; } = new ObservableCollectionFast<C200153_StockArticleBatch>();

    #endregion

    #region COSTRUTTORE
    public CtrlStockBatchArticle()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlStockFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag); 
      
      colBatchCode.Header = Localization.Localize.LocalizeDefaultString("BATCH CODE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colArticleDescription.Header = Localization.Localize.LocalizeDefaultString("ARTICLE DESCRIPTION");
      colDepositor.Header = Localization.Localize.LocalizeDefaultString("DEPOSITOR");
      colAWH1.Header = Localization.Localize.LocalizeDefaultString("AWH1");
      colAWH2.Header = Localization.Localize.LocalizeDefaultString("AWH2");


      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");

      CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    private void Cmd_RM_Stock_LotArticle()
    {
      busyOverlay.IsBusy = true;
      GiacL.Clear();
      CommandManagerC200153.RM_Stock_BatchArticle(this, Filter);
    }
    private void RM_Stock_BatchArticle(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      var giacArtL = dwr.Data as List<C200153_StockArticleBatch>;
      if (giacArtL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (giacArtL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        giacArtL = giacArtL.Take(Filter.MaxItems).ToList();
        GiacL.AddRange(giacArtL);

      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    private void CtrlBaseC200153_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    #endregion


    #region Eventi Ricerca
    private void ctrlStockFilter_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;
      ctrlStockFilter.SearchHighlight = false;

      Cmd_RM_Stock_LotArticle();
    }
    #endregion

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_RM_Stock_LotArticle();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_RM_Stock_LotArticle();
    }

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_StockArticleBatch)
      {
        try
        {
          Clipboard.SetText((((C200153_StockArticleBatch)b.Tag)).ArticleCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyBatch_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_StockArticleBatch)
      {
        try
        {
          Clipboard.SetText((((C200153_StockArticleBatch)b.Tag)).BatchCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private async void butExport_Click(object sender, RoutedEventArgs e)
    {
      var view = new CtrlReportExport("ArticleStock", GiacL.Select(g => new Exportable_ArticleStock(g)).Cast<IExportable>().ToArray(), typeof(Exportable_ArticleStock));

      //show the dialog
      var result = await DialogHost.Show(view, "RootDialog");

      if (view.Result == ExportResult.OK)
        LocalSnackbar.ShowMessageOkAndOpenFile("EXPORT COMPLETED".TD() + " " + view.FileCreatedName, 5, "OPEN", view.FileCreatedName);
    }
  }

  public class Exportable_ArticleStock : IExportable
  {

    public Exportable_ArticleStock(C200153_StockArticleBatch artIn)
    {
      BatchCode = artIn.BatchCode;
      ArticleCode = artIn.ArticleCode;
      ArticleDescription = artIn.ArticleDescr;
      Depositor = artIn.DepositorCode;
      StockAWH1 = artIn.StockByArea.Where(x => x.AreaCode.EqualsIgnoreCase("AWH1")).ToString();
      StockAWH1 = artIn.StockByArea.Where(x => x.AreaCode.EqualsIgnoreCase("AWH2")).ToString();
    }

    [Exportation(ColumnIndex = 0, ColumnName = "BATCH CODE")]
    public string BatchCode { get; set; }

    [Exportation(ColumnIndex = 1, ColumnName = "ARTICLE CODE")]
    public string ArticleCode { get; set; }

    [Exportation(ColumnIndex = 2, ColumnName = "DESCRIPTION")]
    public string ArticleDescription { get; set; }

    [Exportation(ColumnIndex = 3, ColumnName = "DEPOSITOR")]
    public string Depositor { get; set; }

    [Exportation(ColumnIndex = 4, ColumnName = "AWH1")]
    public string StockAWH1 { get; set; }

    [Exportation(ColumnIndex = 5, ColumnName = "AWH2")]
    public string StockAWH2 { get; set; }

  }
}