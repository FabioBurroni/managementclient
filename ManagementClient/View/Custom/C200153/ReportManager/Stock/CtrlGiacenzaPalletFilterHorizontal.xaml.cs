﻿
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using Model.Custom.C200153.Report;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlStockFilter.xaml
  /// </summary>
  public partial class CtrlStockPalletFilterHorizontal : CtrlBaseC200153
  {

    #region EVENTI
    public delegate void OnSearchHandler();
    public event OnSearchHandler OnSearch;
    #endregion

    #region Fields
    private const int _indexStartValue = 0;
    #endregion

    #region PUBLIC PROPERTIES
    public C200153_StockPalletFilter Filter { get; set; } = new C200153_StockPalletFilter();

    #region Press to search highlight

    private bool searchHighlight = true;

    public bool SearchHighlight
    {
      get { return searchHighlight; }
      set
      {
        searchHighlight = value;
        NotifyPropertyChanged("SearchHighlight");
      }
    }

    #endregion
    #endregion

    #region Constructor
    public CtrlStockPalletFilterHorizontal()
    {
      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {

      HintAssist.SetHint(cbMaxItems, Localization.Localize.LocalizeDefaultString((string)cbMaxItems.Tag));
      //HintAssist.SetHint(txtBoxPalletCode, Localization.Localize.LocalizeDefaultString((string)txtBoxPalletCode.Tag));
      HintAssist.SetHint(txtBoxArticleCode, Localization.Localize.LocalizeDefaultString((string)txtBoxArticleCode.Tag));
      HintAssist.SetHint(txtBoxBatchCode, Localization.Localize.LocalizeDefaultString((string)txtBoxBatchCode.Tag));
      HintAssist.SetHint(txtBoxDepositorCode, Localization.Localize.LocalizeDefaultString((string)txtBoxDepositorCode.Tag));
      HintAssist.SetHint(cbWhPreference, Localization.Localize.LocalizeDefaultString((string)cbWhPreference.Tag));

      txtBlkAvailable.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkAvailable.Tag);
      txtBlkReserved.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkReserved.Tag);

      btnCancel.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnCancel.Tag);
      btnSearch.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnSearch.Tag);
    }

    #endregion TRADUZIONI

    #region Events
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
        return;

      Translate();

    }
    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
    }

    private void btnSearch_Click(object sender, RoutedEventArgs e)
    {
      if (cbMaxItems.SelectedItem != null)
      {
        Filter.MaxItems = (int)cbMaxItems.SelectedItem;
      }
      Filter.Index = _indexStartValue;
      Search();
    }
    #endregion

    #region private Methods
    private void Search()
    {
      OnSearch?.Invoke();
    }

    #endregion

    private void TextBox_KeyDown(object sender, KeyEventArgs e)
    {
      if(e.Key==Key.Return)
        Search();
    }
  }
}
