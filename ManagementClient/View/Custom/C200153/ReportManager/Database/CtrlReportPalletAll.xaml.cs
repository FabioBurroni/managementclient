﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200153.Report;
using Model.Export;
using View.Common.Languages;
using View.Common.Report;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for C200153_PalletAll.xaml
  /// </summary>
  public partial class CtrlReportPalletAll : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public C200153_PalletAllFilter Filter { get; set; } = new C200153_PalletAllFilter();

    public ObservableCollectionFast<C200153_PalletAll> PalletL { get; set; } = new ObservableCollectionFast<C200153_PalletAll>();
    #endregion

    #region COSTRUTTORE
    public CtrlReportPalletAll()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colArticleDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colDateBorn.Header = Localization.Localize.LocalizeDefaultString("DATE BORN");
      colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
      colPosition.Header = Localization.Localize.LocalizeDefaultString("POSITION");
      colProductionDate.Header = Localization.Localize.LocalizeDefaultString("PRODUCTION DATE");
      colExpiryDate.Header = Localization.Localize.LocalizeDefaultString("EXPIRY DATE");
      colBatchCode.Header = Localization.Localize.LocalizeDefaultString("BATCH");
      colDepositorCode.Header = Localization.Localize.LocalizeDefaultString("DEPOSITOR");

      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");

      CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE
    private void Cmd_RM_Pallet_GetAll()
    {
      busyOverlay.IsBusy = true;
      PalletL.Clear();
      CommandManagerC200153.RM_Pallet_GetAll(this, Filter);
    }
    private void RM_Pallet_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      var palL = dwr.Data as List<C200153_PalletAll>;
      if (palL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (palL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        palL = palL.Take(Filter.MaxItems).ToList();

        PalletL.AddRange(palL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

   #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    private void CtrlBaseC200153_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    #endregion


    #region Eventi Ricerca
    
    private void ctrlArticleFilter_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;
      ctrlFilter.SearchHighlight = false;

      Cmd_RM_Pallet_GetAll();

    }
    #endregion

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_RM_Pallet_GetAll();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_RM_Pallet_GetAll();
    }

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletAll)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletAll)b.Tag)).PalletCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyBatch_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletAll)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletAll)b.Tag)).BatchCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyArticle_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletAll)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletAll)b.Tag)).ArticleCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyDepositor_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletAll)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletAll)b.Tag)).DepositorCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private async void butExport_Click(object sender, RoutedEventArgs e)
    {
      var view = new CtrlReportExport("PalletAll", PalletL.Select(g => new Exportable_PalletAll(g)).Cast<IExportable>().ToArray(), typeof(Exportable_PalletAll));

      //show the dialog
      var result = await DialogHost.Show(view, "RootDialog");

      if (view.Result == ExportResult.OK)
        LocalSnackbar.ShowMessageOkAndOpenFile("EXPORT COMPLETED".TD() + " " + view.FileCreatedName, 5, "OPEN", view.FileCreatedName);

    }

    
  }

  public class Exportable_PalletAll : IExportable
  {

    public Exportable_PalletAll(C200153_PalletAll pal)
    {
      PalletCode = pal.PalletCode;
      DateBorn = pal.DateBorn.ToString();
      ArticleCode = pal.ArticleCode;
      Description = pal.ArticleDescr;
      ProductionDate = pal.ProductionDate.ToString();
      ExpiryDate = pal.ProductionDate.ToString();
      Position = pal.PositionCode;
      BatchCode = pal.BatchCode;
      DepositorCode = pal.DepositorCode;
    }

    [Exportation(ColumnIndex = 0, ColumnName = "PALLET CODE")]
    public string PalletCode { get; set; }

    [Exportation(ColumnIndex = 1, ColumnName = "DATE BORN")]
    public string DateBorn { get; set; }    

    [Exportation(ColumnIndex = 2, ColumnName = "ARTICLE CODE")]
    public string ArticleCode { get; set; }

    [Exportation(ColumnIndex = 3, ColumnName = "DESCRIPTION")]
    public string Description { get; set; }

    [Exportation(ColumnIndex = 4, ColumnName = "PRODUCTION DATE")]
    public string ProductionDate { get; set; }

    [Exportation(ColumnIndex = 5, ColumnName = "EXPIRY DATE")]
    public string ExpiryDate { get; set; }

    [Exportation(ColumnIndex = 6, ColumnName = "POSITION")]
    public string Position { get; set; }

    [Exportation(ColumnIndex = 7, ColumnName = "BATCH")]
    public string BatchCode { get; set; }

    [Exportation(ColumnIndex = 8, ColumnName = "DEPOSITOR")]
    public string DepositorCode { get; set; }

  }
}