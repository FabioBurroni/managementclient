﻿using System.Windows;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using Model.Custom.C200153;
using Model.Custom.C200153.Report;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlArticleFilterHorizontal.xaml
  /// </summary>
  public partial class CtrlDepositorFilterHorizontal : CtrlBaseC200153
  {
    #region EVENTI
    public delegate void OnSearchHandler();
    public event OnSearchHandler OnSearch;
    #endregion

    #region Fields
    private const int _indexStartValue = 0;
    #endregion

    #region PUBLIC PROPERTIES
    public C200153_DepositorFilter  Filter { get; set; } = new C200153_DepositorFilter();
    #endregion

    #region Press to search highlight

    private bool searchHighlight = true;

    public bool SearchHighlight
    {
      get { return searchHighlight; }
      set { searchHighlight = value;
        NotifyPropertyChanged("SearchHighlight");
      }
    }

    #endregion

    #region Constructor
    public CtrlDepositorFilterHorizontal()
    {
      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      HintAssist.SetHint(txtBoxDepositorCode, Localization.Localize.LocalizeDefaultString((string)txtBoxDepositorCode.Tag));
      HintAssist.SetHint(txtBoxDepositorAddress, Localization.Localize.LocalizeDefaultString((string)txtBoxDepositorAddress.Tag));
      HintAssist.SetHint(cbMaxItems, Localization.Localize.LocalizeDefaultString((string)cbMaxItems.Tag));
     
      btnCancel.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnCancel.Tag);
      btnSearch.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnSearch.Tag);      
    }

    #endregion TRADUZIONI

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    #endregion

    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
    }

    private void btnSearch_Click(object sender, RoutedEventArgs e)
    {
      if (cbMaxItems.SelectedItem != null)
      {
        Filter.MaxItems = (int)cbMaxItems.SelectedItem;
      }
      Filter.Index = _indexStartValue;
      Search();
    }

    #region private Methods
    private void Search()
    {
      OnSearch?.Invoke();
    }
    #endregion

    private void TextBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Return)
        Search();

    }
  }
}
