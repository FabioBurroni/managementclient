﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.Depositor;
using Model.Custom.C200153.Report;
using Model.Export;
using View.Common.Languages;
using View.Common.Report;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlAnagraficaDepositor.xaml
  /// </summary>
  public partial class CtrlAnagraficaDepositor : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
   
    public C200153_DepositorFilter Filter { get; set; } = new C200153_DepositorFilter();

    public ObservableCollectionFast<C200153_Depositor> DepL { get; set; } = new ObservableCollectionFast<C200153_Depositor>();
    #endregion
   
    #region COSTRUTTORE
    public CtrlAnagraficaDepositor()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlDepositorFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colDepositorCode.Header = Localization.Localize.LocalizeDefaultString("CODE");
      colAddress.Header = Localization.Localize.LocalizeDefaultString("ADDRESS");
      
      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");

      CollectionViewSource.GetDefaultView(dgDepL.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    private void Cmd_RM_Depositor_GetAll()
    {
      busyOverlay.IsBusy = true;
      DepL.Clear();
      CommandManagerC200153.RM_Depositor_GetAll(this, Filter);
    }
    private void RM_Depositor_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;

      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var depL = dwr.Data as List<C200153_Depositor>;
      if (depL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (depL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        depL = depL.Take(Filter.MaxItems).ToList();
        DepL.AddRange(depL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

    #endregion


    #region Eventi Ricerca

    private void ctrlDepositorFilter_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;
      ctrlDepositorFilter.SearchHighlight = false;

      Cmd_RM_Depositor_GetAll();
    }

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_RM_Depositor_GetAll();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_RM_Depositor_GetAll();
    }

   

    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();
    }

    private void CtrlBaseC200153_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_Depositor)
      {
        try
        {
          Clipboard.SetText((((C200153_Depositor)b.Tag)).Code);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }


    #endregion

  

    private async void butExport_Click(object sender, RoutedEventArgs e)
    {
      var view = new CtrlReportExport("Depositor", DepL.Select(g => new Exportable_Depositor(g)).Cast<IExportable>().ToArray(),typeof(Exportable_Depositor));

      //show the dialog
      var result = await DialogHost.Show(view, "RootDialog");

      if (view.Result == ExportResult.OK)
        LocalSnackbar.ShowMessageOkAndOpenFile("EXPORT COMPLETED".TD() + " " + view.FileCreatedName, 5, "OPEN", view.FileCreatedName);

    }

 
  }


  public class Exportable_Depositor : IExportable
  {

    public Exportable_Depositor(C200153_Depositor depIn)
    {
      Code = depIn.Code;
      Address = depIn.Address;    
    }

    [Exportation(ColumnIndex = 0, ColumnName = "CODE")]
    public string Code { get; set; }

    [Exportation(ColumnIndex = 1, ColumnName = "ADDRESS")]
    public string Address { get; set; }

   
  }

}
