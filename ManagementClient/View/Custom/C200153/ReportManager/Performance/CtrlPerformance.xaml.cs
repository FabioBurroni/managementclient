﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model;
using Model.Custom.C200153;
using Utilities.Extensions;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlPerformance.xaml
  /// </summary>
  public partial class CtrlPerformance : CtrlBaseC200153
  {
    #region COSTRUTTORE
    public CtrlPerformance()
    {
      InitializeComponent();
    }
    #endregion

    #region STORICO PRESTAZIONI

    #region PUBLIC PROPERTIES
    public DateTime HistoryPerformanceSelectedDate { get; set; } = DateTime.Now;
    public ObservableCollectionFast<C200153_Performance> HistoryPerformanceList { get; set; } = new ObservableCollectionFast<C200153_Performance>();
    #endregion

    #region COMANDI E RISPOSTE
    private void Cmd_RM_Performance(DateTime dt)
    {
      HistoryPerformanceList.Clear();
      CommandManagerC200153.RM_Performance(this, dt);
    }
    private void RM_Performance(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var perfL = dwr.Data as List<C200153_Performance>;
      if (perfL != null)
      {
        HistoryPerformanceList.AddRange(perfL);
      }
    }
    #endregion

    #region EVENTI

    private void dgStoricPerformance_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
    {
      var items = dgStoricPerformance.SelectedItems;
      if (items.Count == 0) return;
      var totMin = (items.OfType<C200153_Performance>().ToList().Last().DateEnd - items.OfType<C200153_Performance>().ToList().First().DateStart).TotalMinutes;
      var totalInputs = items.OfType<C200153_Performance>().ToList().Sum(p => p.Input);
      var totOutputs = items.OfType<C200153_Performance>().ToList().Sum(p => p.Output);

      txtStoricTotalTimeM.Text = totMin.ToString();
      txtStoricTotalInput.Text = totalInputs.ToString();
      txtStoricAverageInput.Text = (totalInputs / totMin).ToString("0.00");
      
      txtStoricTotalOutput.Text = totOutputs.ToString();
      txtStoricAverageOutput.Text = (totOutputs / totMin).ToString("0.00");

      Console.WriteLine();
    }
    private void butHistoryReport_Click(object sender, RoutedEventArgs e)
    {
      Cmd_RM_Performance(HistoryPerformanceSelectedDate);
    }

    private void butHistoryExport_Click(object sender, RoutedEventArgs e)
    {
      string formattedColumns = "{0,-22} {1,-22} {2,-10} {3,-10} {4,-10} {5,-10} {6,-10}";

      List<string> lines = new List<string>();

      string dateI = "Data Inizio";
      string dateF = "Data Fine";
      string Ingresso = "Ingresso";
      string Magazzino = "Magazzino";
      string Spedizioni = "Spedizioni";
      string Uscita = "Uscita";
      string Reject = "Rifiutati";

      lines.Add(String.Format(formattedColumns, dateI, dateF, Ingresso, Magazzino, Spedizioni, Uscita, Reject));

      foreach (var pr in HistoryPerformanceList)
      {
        lines.Add(String.Format(formattedColumns, pr.DateStart.ConvertToDateTimeFormatString(), pr.DateEnd.ConvertToDateTimeFormatString(), pr.Input, pr.Warehouse, pr.Output, pr.Exit, pr.Reject));
      }

      ExportToFile(lines);
    }
    #endregion


    #endregion

    #region PRESTAZIONI PER INTERVALLO
    public View_Performance ViewPerformance { get; set; } = new View_Performance();
    #region COMANDI E RISPOSTE
    private void Cmd_RM_Performance(DateTime dateStart, DateTime dateEnd)
    {
      CommandManagerC200153.RM_PerformanceByInterval(this, dateStart, dateEnd);
    }
    private void RM_PerformanceByInterval(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var perfL = dwr.Data as List<C200153_Performance>;
      if (perfL != null)
      {
        //ViewPerformance.PerformanceRecordList.AddRange(perfL);
        ViewPerformance.Add(perfL);
      }
    }
    #endregion

    #region EVENTI
    private void dgPerformance_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
    {
      if(dgPerformance.SelectedItems!=null)
        ViewPerformance.Selecteds = dgPerformance.SelectedItems.OfType<C200153_Performance>().ToList();
    }

    private void butPerformanceReport_Click(object sender, RoutedEventArgs e)
    {
      Cmd_RM_Performance(ViewPerformance.DateStart, ViewPerformance.DateEnd);
    }
    
    private void butPerformanceExport_Click(object sender, RoutedEventArgs e)
    {
      string formattedColumns = "{0,-22} {1,-22} {2,-10} {3,-10} {4,-10} {5,-10} {6,-10}";

      List<string> lines = new List<string>();

      string dateI = "Data Inizio";
      string dateF = "Data Fine";
      string Ingresso = "Ingresso";
      string Magazzino = "Magazzino";
      string Spedizioni = "Spedizioni";
      string Uscita = "Uscita";
      string Reject = "Rifiutati";

      lines.Add(String.Format(formattedColumns, dateI, dateF, Ingresso, Magazzino, Spedizioni, Uscita, Reject));

      foreach (var pr in ViewPerformance.PerformanceRecordList)
      {
        lines.Add(String.Format(formattedColumns, pr.DateStart.ConvertToDateTimeFormatString(), pr.DateEnd.ConvertToDateTimeFormatString(), pr.Input, pr.Warehouse, pr.Output, pr.Exit, pr.Reject));
      }

      ExportToFile(lines);
    }
    
    #endregion

    #endregion

    #region PRIVATE METHODS
    private void ExportToFile(List<string> lines)
    {
      if (lines.Count == 0) return;

      System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
      sfd.Filter = "Txt files (*.txt)|*.txt|All files (*.*)|*.*";
      sfd.ShowDialog();
      if (!string.IsNullOrEmpty(sfd.FileName))
      {
        System.IO.File.WriteAllLines(sfd.FileName, lines);
      }
    }

    #endregion

    #region View_Performance
    public class View_Performance : ModelBase
    {
      public View_Performance()
      {
        CommandAddMinute = new CommandPeformanceAddMinute(this);
        CommandSetMinute = new CommandPeformanceSetMinute(this);
        CommandClear = new CommandPeformanceClear(this);
        CommandDeleteRecord = new CommandPeformanceDeleteRecord(this);
      }

      #region PUBLIC PROPERTIES
      public ObservableCollectionFast<C200153_Performance> PerformanceRecordList { get; set; } = new ObservableCollectionFast<C200153_Performance>();
      public CommandPeformanceSetMinute CommandSetMinute { get; set; }
      public CommandPeformanceAddMinute CommandAddMinute { get; set; }
      public CommandPeformanceClear  CommandClear { get; set; }
      public CommandPeformanceDeleteRecord CommandDeleteRecord { get; set; }

      private C200153_Performance _Selected;
      public C200153_Performance Selected
      {
        get { return _Selected; }
        set
        {
          if (value != _Selected)
          {
            _Selected = value;
            NotifyPropertyChanged();
          }
        }
      }


      private List<C200153_Performance> _Selecteds;

      public List<C200153_Performance> Selecteds
      {
        get { return _Selecteds; }
        set
        {
          if (value != _Selecteds)
          {
            _Selecteds = value;
            NotifyPropertyChanged();
            Calculate();
          }
        }
      }
      #endregion

      public void Add(List<C200153_Performance> perfList)
      {
        if (perfList.Count == 0)
          return;

        var tmp = PerformanceRecordList.ToList();
        tmp.AddRange(perfList);
        PerformanceRecordList.Clear();
        PerformanceRecordList.AddRange(tmp.OrderBy(p=>p.DateStart));

        MinX = PerformanceRecordList.Min(p => p.DateStart);
        MaxX = PerformanceRecordList.Max(p => p.DateStart);
      }

      private DateTime _MinX;
      public DateTime MinX
      {
        get { return _MinX; }
        set
        {
          if (value != _MinX)
          {
            _MinX = value;
            NotifyPropertyChanged();
          }
        }
      }



      private DateTime _MaxX;
      public DateTime MaxX
      {
        get { return _MaxX; }
        set
        {
          if (value != _MaxX)
          {
            _MaxX = value;
            NotifyPropertyChanged();
          }
        }
      }

      private void Calculate()
      {
        if (_Selecteds.Count == 0) return;

        TotalMinute = (_Selecteds.Last().DateEnd - _Selecteds.First().DateStart).TotalMinutes;
        TotalInput = _Selecteds.Sum(p => p.Input);
        TotalOutput = _Selecteds.Sum(p => p.Output);
        AverageInput = TotalMinute > 0 ? (TotalInput/TotalMinute) : 0;
        AverageOutput= TotalMinute  > 0 ? (TotalOutput/TotalMinute ) : 0;
      }


      private double _AverageInput;
      public double AverageInput
      {
        get { return _AverageInput; }
        set
        {
          if (value != _AverageInput)
          {
            _AverageInput = value;
            NotifyPropertyChanged();
          }
        }
      }


      private double _AverageOutput;
      public double AverageOutput
      {
        get { return _AverageOutput; }
        set
        {
          if (value != _AverageOutput)
          {
            _AverageOutput = value;
            NotifyPropertyChanged();
          }
        }
      }

      private double _TotalMinute;
      public double TotalMinute
      {
        get { return _TotalMinute; }
        set
        {
          if (value != _TotalMinute)
          {
            _TotalMinute = value;
            NotifyPropertyChanged();
          }
        }
      }

      private int _TotalInput;
      public int TotalInput
      {
        get { return _TotalInput; }
        set
        {
          if (value != _TotalInput)
          {
            _TotalInput = value;
            NotifyPropertyChanged();
          }
        }
      }


      private int _TotalOutput;
      public int TotalOutput
      {
        get { return _TotalOutput; }
        set
        {
          if (value != _TotalOutput)
          {
            _TotalOutput = value;
            NotifyPropertyChanged();
          }
        }
      }


      private DateTime _DateStart = DateTime.Now;
      public DateTime DateStart
      {
        get { return _DateStart; }
        set
        {
          if (value != _DateStart)
          {
            _DateStart = value;
            NotifyPropertyChanged();
          }
        }
      }



      private DateTime _DateEnd = DateTime.Now;
      public DateTime DateEnd
      {
        get { return _DateEnd; }
        set
        {
          if (value != _DateEnd)
          {
            _DateEnd = value;
            NotifyPropertyChanged();
          }
        }
      }
    }
    #endregion


    #region CommandPeformanceAddMinute
    public class CommandPeformanceAddMinute : ICommand
    {
      View_Performance _vp;
      public CommandPeformanceAddMinute(View_Performance vp)
      {
        _vp = vp;
      }

      #region ICommand Interface
      public event EventHandler CanExecuteChanged;

      public bool CanExecute(object parameter)
      {
        return true;
      }

      public void Execute(object parameter)
      {
        if (parameter == null)
          return;
        int minute = 0;
        if (int.TryParse(parameter.ToString(), out minute))
        {
          _vp.DateStart = _vp.DateStart.AddMinutes(minute);
          _vp.DateEnd = _vp.DateEnd.AddMinutes(minute);
        }
      }
      #endregion
    } 
    #endregion


    #region CommandPeformanceSetMinute
    public class CommandPeformanceSetMinute : ICommand
    {
      View_Performance _vp;
      public CommandPeformanceSetMinute(View_Performance vp)
      {
        _vp = vp;
      }

      #region ICommand Interface
      public event EventHandler CanExecuteChanged;

      public bool CanExecute(object parameter)
      {
        return true;
      }

      public void Execute(object parameter)
      {
        if (parameter == null)
          return;
        int minute = 0;
        if (int.TryParse(parameter.ToString(), out minute))
        {
          _vp.DateEnd = _vp.DateStart.AddMinutes(minute);
        }
      }
      #endregion
    }
    #endregion


    #region CommandPeformanceDeleteRecord
    public class CommandPeformanceDeleteRecord : ICommand
    {
      View_Performance _vp;
      public CommandPeformanceDeleteRecord(View_Performance vp)
      {
        _vp = vp;
      }

      #region ICommand Interface
      public event EventHandler CanExecuteChanged;

      public bool CanExecute(object parameter)
      {
        return true;
      }

      public void Execute(object parameter)
      {
        C200153_Performance per = parameter as C200153_Performance;
        if (per != null)
          _vp.PerformanceRecordList.Remove(per);

      }
      #endregion
    }
    #endregion


    #region CommandPeformanceClear
    public class CommandPeformanceClear : ICommand
    {
      View_Performance _vp;
      public CommandPeformanceClear(View_Performance vp)
      {
        _vp = vp;
      }

      #region ICommand Interface
      public event EventHandler CanExecuteChanged;

      public bool CanExecute(object parameter)
      {
        return true;
      }

      public void Execute(object parameter)
      {
        _vp.PerformanceRecordList.Clear();
      }
      #endregion
    }

    #endregion

    
  }
}

