﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.Report;
using Model.Export;
using View.Common.Languages;
using View.Common.Report;

namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlStockPallet.xaml
  /// </summary>
  public partial class CtrlPalletReject : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public C200153_PalletRejectFilter Filter { get; set; } = new C200153_PalletRejectFilter();

    public ObservableCollectionFast<C200153_PalletReject> GiacL { get; set; } = new ObservableCollectionFast<C200153_PalletReject>();
    #endregion

    #region COSTRUTTORE
    public CtrlPalletReject()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlPalletRejectFilterHorizontal.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colData.Header = Localization.Localize.LocalizeDefaultString("DATE");
      colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
      colReason.Header = Localization.Localize.LocalizeDefaultString("REASON");
      colPosition.Header = Localization.Localize.LocalizeDefaultString("EXIT POSITION");

      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");

      CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE
    private void Cmd_RM_PalletReject()
    {
      busyOverlay.IsBusy = true;
      GiacL.Clear();
      CommandManagerC200153.RM_PalletReject(this,Filter);
    }
    private void RM_PalletReject(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      var giacL = dwr.Data as List<C200153_PalletReject>;
      if(giacL!=null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (giacL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        giacL = giacL.Take(Filter.MaxItems).ToList();
        GiacL.AddRange(giacL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }
       
    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    private void CtrlBaseC200153_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    #endregion


    #region Eventi Ricerca
    private void ctrlPalletRejectFilterHorizontal_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;

      Cmd_RM_PalletReject();

    }
    #endregion

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_RM_PalletReject();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_RM_PalletReject();
    }

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletReject)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletReject)b.Tag)).PalletCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyArticle_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletReject)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletReject)b.Tag)).ArticleCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }
    private void butCopyPosition_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletReject)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletReject)b.Tag)).Position);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyBatch_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletReject)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletReject)b.Tag)).BatchCode);
          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyDepositor_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletReject)
      {
        try
        {
          Clipboard.SetText((((C200153_PalletReject)b.Tag)).DepositorCode);
          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private async void butExport_Click(object sender, RoutedEventArgs e)
    {
      var view = new CtrlReportExport("PalletReject", GiacL.Select(g => new Exportable_PalletReject(g)).Cast<IExportable>().ToArray(), typeof(Exportable_PalletReject));

      //show the dialog
      var result = await DialogHost.Show(view, "RootDialog");

      if (view.Result == ExportResult.OK)
        LocalSnackbar.ShowMessageOkAndOpenFile("EXPORT COMPLETED".TD() + " " + view.FileCreatedName, 5, "OPEN", view.FileCreatedName);

    }

  }

  public class Exportable_PalletReject : IExportable
  {

    public Exportable_PalletReject(C200153_PalletReject palletReject)
    {
      Date = palletReject.DateBorn.ToString();
      PalletCode = palletReject.PalletCode;
      ArticleCode = palletReject.ArticleCode;
      BatchCode = palletReject.BatchCode;
      Depositor = palletReject.DepositorCode;
      Position = palletReject.Position.ToString();
      Reason = palletReject.Reason;
      CellCode = palletReject.CellCode;
      ProductionDate = palletReject.ProductionDate.ToString();
      ExpiryDate = palletReject.ExpiryDate.ToString();
      IsLabeled = palletReject.IsLabeled.ToString();
      IsWrapped = palletReject.IsWrapped.ToString();

    }

    [Exportation(ColumnIndex = 0, ColumnName = "DATE")]
    public string Date { get; set; }

    [Exportation(ColumnIndex = 1, ColumnName = "PALLET CODE")]
    public string PalletCode { get; set; }

    [Exportation(ColumnIndex = 2, ColumnName = "ARTICLE CODE")]
    public string ArticleCode { get; set; }

    [Exportation(ColumnIndex = 3, ColumnName = "BATCH CODE")]
    public string BatchCode { get; set; }

    [Exportation(ColumnIndex = 4, ColumnName = "DEPOSITOR")]
    public string Depositor { get; set; }
    
    [Exportation(ColumnIndex = 5, ColumnName = "POSITION")]
    public string Position { get; set; }

    [Exportation(ColumnIndex = 6, ColumnName = "REASON")]
    public string Reason { get; set; }

    [Exportation(ColumnIndex = 7, ColumnName = "CELL CODE")]
    public string CellCode { get; set; }

    [Exportation(ColumnIndex = 8, ColumnName = "PRODUCTION DATE")]
    public string ProductionDate { get; set; }

    [Exportation(ColumnIndex = 9, ColumnName = "EXPIRY DATE")]
    public string ExpiryDate { get; set; }

    [Exportation(ColumnIndex = 10, ColumnName = "LABEL")]
    public string IsLabeled { get; set; }

    [Exportation(ColumnIndex = 11, ColumnName = "WRAP")]
    public string IsWrapped { get; set; }
  }

}
