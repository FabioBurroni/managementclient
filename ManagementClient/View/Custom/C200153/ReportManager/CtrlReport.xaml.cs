﻿using System;
using System.Windows;
using Model.Custom.C200153;


namespace View.Custom.C200153.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlReport.xaml
  /// </summary>
  public partial class CtrlReport : CtrlBaseC200153
  {
    public C200153_UserLogged UserLogged { get; set; } = new C200153_UserLogged();

    #region Costruttore
    public CtrlReport()
    {
      InitializeComponent();
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
      Authentication.AuthenticationManager.Instance.LoginSession += Instance_LoginSession;

    }
    #endregion

    #region TRADUZIONI
    protected override void Translate()
    {
      tiDatabase.Text = Localization.Localize.LocalizeDefaultString((string)tiDatabase.Tag);
      tiSTOCK.Text = Localization.Localize.LocalizeDefaultString((string)tiSTOCK.Tag);
      tiPallet.Text = Localization.Localize.LocalizeDefaultString((string)tiPallet.Tag);
      tiBatchArticle.Text = Localization.Localize.LocalizeDefaultString("BATCH") + " " + Localization.Localize.LocalizeDefaultString("ARTICLE");
      tiStockByCell.Text = Localization.Localize.LocalizeDefaultString((string)tiStockByCell.Tag);
      tiDistribution.Text = Localization.Localize.LocalizeDefaultString((string)tiDistribution.Tag);
      tiDistributionPalletFlow.Text = Localization.Localize.LocalizeDefaultString((string)tiDistributionPalletFlow.Tag);
      tiPalletDistribution.Text = Localization.Localize.LocalizeDefaultString((string)tiPalletDistribution.Tag);
      tiArticleBatchDistribution.Text = Localization.Localize.LocalizeDefaultString("BATCH") + " " + Localization.Localize.LocalizeDefaultString("ARTICLE");
      tiWharehouseInputFlow.Text = Localization.Localize.LocalizeDefaultString((string)tiWharehouseInputFlow.Tag);
      tiPalletReject.Text = Localization.Localize.LocalizeDefaultString((string)tiPalletReject.Tag);
      tiPalletIn.Text = Localization.Localize.LocalizeDefaultString((string)tiPalletIn.Tag);
      tiPalletOut.Text = Localization.Localize.LocalizeDefaultString((string)tiPalletOut.Tag);
      tiPalletAll.Text = Localization.Localize.LocalizeDefaultString((string)tiPalletAll.Tag);

    }
    #endregion TRADUZIONI

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    #endregion

    private void Instance_LoginSession(object sender, Authentication.LoginSessionEventArgs sessionArgs)
    {
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
    }

    private void CtrlBaseC200153_Unloaded(object sender, RoutedEventArgs e)
    {
      try
      {
        Authentication.AuthenticationManager.Instance.LoginSession -= Instance_LoginSession;
      }
      catch (Exception)
      {

      }
    }
  }
}
