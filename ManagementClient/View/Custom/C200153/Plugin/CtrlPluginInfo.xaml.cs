﻿using System.Windows;
using System.Windows.Controls;
using Model.Custom.C200153.Plugin;

namespace View.Custom.C200153.Plugin
{
    /// <summary>
    /// Interaction logic for CtrlPluginInfo.xaml
    /// </summary>
    public partial class CtrlPluginInfo : UserControl
    {
        #region delegati ed eventi

        public delegate void OnPluginActionHandler(PluginInfo pluginInfo, PluginAction action);
        public event OnPluginActionHandler OnPluginAction;

        #endregion

        #region costruttore

        public CtrlPluginInfo()
        {
            InitializeComponent();
        }

        #endregion

        #region EVENTI INTERFACCIA

        private void butPluginDisable_Click(object sender, RoutedEventArgs e)
        {
            if (OnPluginAction != null)
                OnPluginAction(this.DataContext as PluginInfo, PluginAction.DISABLE);
        }

        private void butPluginEnable_Click(object sender, RoutedEventArgs e)
        {
            if (OnPluginAction != null)
                OnPluginAction(this.DataContext as PluginInfo, PluginAction.ENABLE);
        }

        private void butPluginStop_Click(object sender, RoutedEventArgs e)
        {
            if (OnPluginAction != null)
                OnPluginAction(this.DataContext as PluginInfo, PluginAction.STOP);
        }

        private void butPluginUnload_Click(object sender, RoutedEventArgs e)
        {
            if (OnPluginAction != null)
                OnPluginAction(this.DataContext as PluginInfo, PluginAction.UNLOAD);
        }

        private void butPluginStart_Click(object sender, RoutedEventArgs e)
        {
            if (OnPluginAction != null)
                OnPluginAction(this.DataContext as PluginInfo, PluginAction.START);
        }

        private void butPluginUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (OnPluginAction != null)
                OnPluginAction(this.DataContext as PluginInfo, PluginAction.UPDATE);
        }

        private void butAddPlugin_Click(object sender, RoutedEventArgs e)
        {
            if (OnPluginAction != null)
                OnPluginAction(this.DataContext as PluginInfo, PluginAction.ADD);
        }

        private void butCheckPlugin_Click(object sender, RoutedEventArgs e)
        {
            if (OnPluginAction != null)
                OnPluginAction(this.DataContext as PluginInfo, PluginAction.CHECK);
        }

        private void butXmlCommandsGet_Click(object sender, RoutedEventArgs e)
        {
            if (OnPluginAction != null)
                OnPluginAction(this.DataContext as PluginInfo, PluginAction.XMLCOMMAND);
        }

        #endregion
    }

    public enum PluginAction
    {
        START,
        STOP,
        ENABLE,
        DISABLE,
        UNLOAD,
        ADD,
        CHECK,
        UPDATE,
        XMLCOMMAND,
    }
}