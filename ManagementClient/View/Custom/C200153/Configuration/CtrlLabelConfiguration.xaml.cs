﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.Label;
using Utilities.Extensions;
using System;
using System.IO;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using View.Common.Languages;
using View.Custom.C200153.Converter;

namespace View.Custom.C200153.Configuration
{
  /// <summary>
  /// Interaction logic for CtrlLabelConfiguration.xaml
  /// </summary>
  public partial class CtrlLabelConfiguration : CtrlBaseC200153
  {
    #region COSTRUTTORE

    public CtrlLabelConfiguration()
    {      
      InitializeComponent();

    }

    #endregion COSTRUTTORE

    #region LOCALIZATION
    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkDescriptionHeader.Text = Context.Instance.TranslateDefault((string)txtBlkDescriptionHeader.Tag);
      txtBlkLabel.Text = Context.Instance.TranslateDefault((string)txtBlkLabel.Tag);
      //...refresh button tooltip
      //txtBlkRefreshButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkRefreshButtonToolTip.Tag);
      txtBlkCodeT.Text = Context.Instance.TranslateDefault((string)txtBlkCodeT.Tag);
      txtBlkDeleteT.Text = Context.Instance.TranslateDefault((string)txtBlkDeleteT.Tag);
      txtBlkCode.Text = Context.Instance.TranslateDefault((string)txtBlkCode.Tag);
      txtBlkNew.Text = Context.Instance.TranslateDefault((string)txtBlkNew.Tag);
      txtBlkNewLabelButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkNewLabelButtonToolTip.Tag);
      txtBlkClear.Text = Context.Instance.TranslateDefault((string)txtBlkClear.Tag);
      txtBlkClearButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkClearButtonToolTip.Tag);
      txtBlkConfirm.Text = Context.Instance.TranslateDefault((string)txtBlkConfirm.Tag);
      txtBlkConfirmButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkConfirmButtonToolTip.Tag);
      txtBlkPreview.Text = Context.Instance.TranslateDefault((string)txtBlkPreview.Tag);
      txtBlkPreviewButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkPreviewButtonToolTip.Tag);
      txtBlkPrint.Text = Context.Instance.TranslateDefault((string)txtBlkPrint.Tag);
      txtBlkPrintButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkPrintButtonToolTip.Tag);
      txtBlkWidth.Text = Context.Instance.TranslateDefault((string)txtBlkWidth.Tag);
      txtBlkHeight.Text = Context.Instance.TranslateDefault((string)txtBlkHeight.Tag);
      txtBlkPrevWidth.Text = Context.Instance.TranslateDefault((string)txtBlkPrevWidth.Tag);
      txtBlkPrevHeight.Text = Context.Instance.TranslateDefault((string)txtBlkPrevHeight.Tag);

      txtBlkLabelField.Text = Context.Instance.TranslateDefault((string)txtBlkLabelField.Tag);
      txtBlkLabelFieldsRefreshButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkLabelFieldsRefreshButtonToolTip.Tag);
      txtBlkFieldT.Text = Context.Instance.TranslateDefault((string)txtBlkFieldT.Tag);
      txtBlkFieldDescriptionT.Text = Context.Instance.TranslateDefault((string)txtBlkFieldDescriptionT.Tag);
      txtBlkFieldTranslatedT.Text = Context.Instance.TranslateDefault((string)txtBlkFieldTranslatedT.Tag);
      txtBlkDeleteFieldT.Text = Context.Instance.TranslateDefault((string)txtBlkDeleteFieldT.Tag);
      txtBlkField.Text = Context.Instance.TranslateDefault((string)txtBlkField.Tag);
      txtBlkFieldDescription.Text = Context.Instance.TranslateDefault((string)txtBlkFieldDescription.Tag);
      txtBlkFieldTranslated.Text = Context.Instance.TranslateDefault((string)txtBlkFieldTranslated.Tag);
      txtBlkFieldEnglish.Text = Context.Instance.TranslateDefault((string)txtBlkFieldEnglish.Tag);
      txtBlkNewField.Text = Context.Instance.TranslateDefault((string)txtBlkNewField.Tag);
      txtBlkNewFiledButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkNewFiledButtonToolTip.Tag);
      txtBlkClearField.Text = Context.Instance.TranslateDefault((string)txtBlkClearField.Tag);
      txtBlkClearFieldButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkClearFieldButtonToolTip.Tag);
      txtBlkConfirmField.Text = Context.Instance.TranslateDefault((string)txtBlkConfirmField.Tag);
      txtBlkConfirmFieldButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkConfirmFieldButtonToolTip.Tag);
    }
    #endregion
    #region LABEL

    #region PUBLIC PROPERTIES
    public ObservableCollectionFast<C200153_Label> LabelL { get; set; } = new ObservableCollectionFast<C200153_Label>();
    #endregion

    #region DP - LabelSelected
    public C200153_Label LabelSelected
    {
      get { return (C200153_Label)GetValue(LabelSelectedProperty); }
      set { SetValue(LabelSelectedProperty, value); }
    }

    // Using a DependencyProperty as the backing store for LabelSelected.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty LabelSelectedProperty =
        DependencyProperty.Register("LabelSelected", typeof(C200153_Label), typeof(CtrlLabelConfiguration), new PropertyMetadata(null));
    #endregion

    #region COMANDI E RISPOSTE
    private void Cmd_CO_PrintRaw(string labelCode)
    {
      CommandManagerC200153.CO_PrintRaw(this, Context.Instance.Position, labelCode);
    }
    private void CO_PrintRaw(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Result == null) return;
    }

    private void Cmd_CO_Label_GetAll()
    {
      LabelL.Clear();
      CommandManagerC200153.CO_Label_GetAll(this);
    }
    private void CO_Label_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var list = dwr.Data as List<C200153_Label>;
      if (list != null)
      {
        list.ForEach(p => LabelL.Add(p));
      }
    }

    private void Cmd_CO_Label_Get(string code)
    {
      CommandManagerC200153.CO_Label_Get(this, code);
    }
    private void CO_Label_Get(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var p = dwr.Data as C200153_Label;
      if (p != null)
      {
        var oldPr = LabelL.FirstOrDefault(pr => pr.Code.EqualsIgnoreCase(p.Code));
        if (oldPr != null)
          oldPr.Update(p);
        else
          LabelL.Add(p);
      }
    }

    private void Cmd_CO_Label_Delete(string Code)
    {
      CommandManagerC200153.CO_Label_Delete(this, Code);
    }
    private void CO_Label_Delete(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Result == null) return;
      string Code = commandMethodParameters[1];
      C200153_CustomResult result = dwr.Result.ConvertTo<C200153_CustomResult>();
      if (result != C200153_CustomResult.OK)
      {
        //MessageBox.Show(result.ToString());
        //CustomMessageBox.Show("", result.ToString(),
        //            MessageBoxButton.OK, MessageBoxImage.Error);
      }
      else
      {

      }
      Cmd_CO_Label_GetAll();
    }

    private void Cmd_CO_Label_InsertOrUpdate(C200153_Label Label)
    {
      CommandManagerC200153.CO_Label_InsertOrUpdate(this, Label.Code, Label.Label);
    }
    private void CO_Label_InsertOrUpdate(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Result == null) return;
      string code = commandMethodParameters[1];
      C200153_CustomResult result = dwr.Result.ConvertTo<C200153_CustomResult>();
      if (result != C200153_CustomResult.OK)
      {
        //MessageBox.Show(result.ToString());
        //CustomMessageBox.Show("", result.ToString(),
        //            MessageBoxButton.OK, MessageBoxImage.Error);
      }
      Cmd_CO_Label_GetAll();
    }
    #endregion

    #region EVENTS

    private void butLabeRefresh_Click(object sender, RoutedEventArgs e)
    {
      Cmd_CO_Label_GetAll();
    }

    private void butNew_Click(object sender, RoutedEventArgs e)
    {
      LabelSelected = new C200153_Label();
    }

    private void butClear_Click(object sender, RoutedEventArgs e)
    {
      LabelSelected?.Clear();

    }

    private void butConfirm_Click(object sender, RoutedEventArgs e)
    {
      Cmd_CO_Label_InsertOrUpdate(LabelSelected);
    }

    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      Cmd_CO_Label_GetAll();
    }

    private void butDelete_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b == null)
        return;
      var p = b.Tag as C200153_Label;
      if (p != null)
        Cmd_CO_Label_Delete(p.Code);
    }
    private void butPrint_Click(object sender, RoutedEventArgs e)
    {
      if (LabelSelected == null)
        return;

      Cmd_CO_PrintRaw(LabelSelected.Code);
    }
    #endregion

    #region LABEL PRVIEW

    private int _LabelHeigth = 15;
    public int LabelHeigth
    {
      get { return _LabelHeigth; }
      set
      {
        if (value != _LabelHeigth)
        {
          _LabelHeigth = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _LabelWidth = 15;
    public int LabelWidth
    {
      get { return _LabelWidth; }
      set
      {
        if (value != _LabelWidth)
        {
          _LabelWidth = value;
          NotifyPropertyChanged();
        }
      }
    }


    private double _PreviewWidth = 300;
    public double PreviewWidth
    {
      get { return _PreviewWidth; }
      set
      {
        if (value != _PreviewWidth)
        {
          _PreviewWidth = value;
          NotifyPropertyChanged();
        }
      }
    }


    private double _PreviewHeight = 600;
    public double PreviewHeight
    {
      get { return _PreviewHeight; }
      set
      {
        if (value != _PreviewHeight)
        {
          _PreviewHeight = value;
          NotifyPropertyChanged();
        }
      }
    }


    private ComboBoxItem _Density;
    public ComboBoxItem Density
    {
      get { return _Density; }
      set
      {
        if (value != _Density)
        {
          _Density = value;
          NotifyPropertyChanged();
        }
      }
    }


    /*
     * As a shared service, the Labelary API incorporates a number of usage limits which ensure that no single user can negatively impact the workloads of other users:

        Maximum 5 requests per second per client. Additional requests result in a HTTP 429 (Too Many Requests) error.
        Maximum 50 labels per request. Additional labels result in a HTTP 413 (Payload Too Large) error.
        Maximum label size of 15 x 15 inches. Larger labels result in a HTTP 400 (Bad Request) error.
        Maximum embedded object size of 5MB, for e.g. ~DU and ~DY. Larger embedded objects result in a HTTP 400 (Bad Request) error.
        Maximum embedded image dimensions of 2,000 x 2,000 pixels, for e.g. ~DG and ~DY. Larger embedded images result in a HTTP 400 (Bad Request) error.
        The image conversion service (image → ZPL) also has the following limits:

        Maximum input image file size of 200 KB. Larger files result in a HTTP 400 (Bad Request) error.
        Maximum input image dimensions of 2,000 x 2,000 pixels. Larger image sizes result in a HTTP 400 (Bad Request) error.
        Note that these limits may be changed as needed in order to ensure smooth operation of the service for all users.

        If these limits are too restrictive for your intended use, you may want to consider licensing Labelary for private on-premise use.
     */

    private void butPreview_Click(object sender, RoutedEventArgs e)
    {
      if (LabelSelected == null)
        return;

      labelImage.Source = null;

      var density = ((ComboBoxItem)Density).Content.ToString();

      byte[] zpl = Encoding.UTF8.GetBytes(LabelSelected.Label);

      string url = $"http://api.labelary.com/v1/printers/{density}dpmm/labels/{LabelWidth}x{LabelHeigth}/0/";
      // adjust print density (12dpmm), label width (4 inches), label height (6 inches), and label index (0) as necessary
      //var request = (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/12dpmm/labels/4x6/0/");
      var request = (HttpWebRequest)WebRequest.Create(url);
      request.Method = "POST";
      //request.Accept = "application/pdf"; // omit this line to get PNG images back
      request.ContentType = "application/x-www-form-urlencoded";
      request.ContentLength = zpl.Length;

      var requestStream = request.GetRequestStream();
      requestStream.Write(zpl, 0, zpl.Length);
      requestStream.Close();

      try
      {
        var response = (HttpWebResponse)request.GetResponse();
        var responseStream = response.GetResponseStream();
        //fileName = AppDomain.CurrentDomain.BaseDirectory + "\\" + "label.png";
        //var fileStream = File.Create(fileName); // change file name for PNG images

        MemoryStream memStream = new MemoryStream();
        responseStream.CopyTo(memStream);
        labelImage.Source = BitmapFrame.Create(memStream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);

        memStream.Close();
        //responseStream.CopyTo(fileStream);
        responseStream.Close();
        //fileStream.Close();
      }
      catch (WebException ex)
      {
        Console.WriteLine("Error: {0}", ex.Status);
      }


      try
      {
        //labelImage.Source = new BitmapImage(new Uri(fileName));
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();
      }
    }
    #endregion

    #endregion

    #region LABEL FIELDS
    #region PUBLIC PROPERTIES
    public ObservableCollectionFast<C200153_LabelFieldTranslator> LabelFieldL { get; set; } = new ObservableCollectionFast<C200153_LabelFieldTranslator>();
    #endregion

    #region DP - LabelFieldSelected
    public C200153_LabelFieldTranslator LabelFieldSelected
    {
      get { return (C200153_LabelFieldTranslator)GetValue(LabelFieldSelectedProperty); }
      set { SetValue(LabelFieldSelectedProperty, value); }
    }

    // Using a DependencyProperty as the backing store for LabelSelected.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty LabelFieldSelectedProperty =
        DependencyProperty.Register("LabelFieldSelected", typeof(C200153_LabelFieldTranslator), typeof(CtrlLabelConfiguration), new PropertyMetadata(null));
    #endregion

    #region COMANDI E RISPOSTE

    private void Cmd_CO_LabelField_GetAll()
    {
      LabelFieldL.Clear();
      CommandManagerC200153.CO_LabelField_GetAll(this);
    }
    private void CO_LabelField_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var list = dwr.Data as List<C200153_LabelFieldTranslator>;
      if (list != null)
      {
        list.ForEach(p => LabelFieldL.Add(p));
      }
    }

    private void Cmd_CO_LabelField_Get(string code)
    {
      CommandManagerC200153.CO_LabelField_Get(this, code);
    }
    private void CO_LabelField_Get(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var p = dwr.Data as C200153_LabelFieldTranslator;
      if (p != null)
      {
        var oldPr = LabelFieldL.FirstOrDefault(pr => pr.Field.EqualsIgnoreCase(p.Field));
        if (oldPr != null)
          oldPr.Update(p);
        else
          LabelFieldL.Add(p);
      }
    }

    private void Cmd_CO_LabelField_Delete(string field)
    {
      CommandManagerC200153.CO_LabelField_Delete(this, field);
    }
    private void CO_LabelField_Delete(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Result == null) return;
      string Code = commandMethodParameters[1];
      C200153_CustomResult result = dwr.Result.ConvertTo<C200153_CustomResult>();
      if (result != C200153_CustomResult.OK)
      {
        LocalSnackbar.ShowMessageOk(Code.ToString() + "DELETED".TD(), 3);
      }
      else
      {
        LocalSnackbar.ShowMessageFail("RESULT".TD() + ": " + CustomResultToLocalizedStringConverter.Convert(result), 3);
      }
      Cmd_CO_LabelField_GetAll();
    }

    private void Cmd_CO_LabelField_InsertOrUpdate(C200153_LabelFieldTranslator ft)
    {
      CommandManagerC200153.CO_LabelField_InsertOrUpdate(this, ft.Field, ft.FieldTranslated, ft.FieldEnglish, ft.Description);
    }
    private void CO_LabelField_InsertOrUpdate(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Result == null) return;
      string code = commandMethodParameters[1];
      C200153_CustomResult result = dwr.Result.ConvertTo<C200153_CustomResult>();
      if (result != C200153_CustomResult.OK)
      {
        LocalSnackbar.ShowMessageOk(code.ToString() + "UPDATED".TD(), 3);
      }
      Cmd_CO_LabelField_GetAll();
    }



    #endregion

    #endregion

    #region EVENTS
    private void butFieldDelete_Click(object sender, RoutedEventArgs e)
    {
      if (LabelFieldSelected != null)
        Cmd_CO_LabelField_Delete(LabelFieldSelected.Field);
    }

    private void butFieldNew_Click(object sender, RoutedEventArgs e)
    {
      LabelFieldSelected = new C200153_LabelFieldTranslator();
    }

    private void butFieldClear_Click(object sender, RoutedEventArgs e)
    {
      LabelFieldSelected?.Clear();
    }

    private void butFieldConfirm_Click(object sender, RoutedEventArgs e)
    {
      if (LabelFieldSelected != null)
      {
        Cmd_CO_LabelField_InsertOrUpdate(LabelFieldSelected);
      }
      else
      {
        LabelFieldSelected = new C200153_LabelFieldTranslator();
      }
    }

    private void butLabelFieldRefresh_Click(object sender, RoutedEventArgs e)
    {
      Cmd_CO_LabelField_GetAll();
    }


    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Cmd_CO_Label_GetAll();
      Cmd_CO_LabelField_GetAll();
    }


    private void butDelete_MouseEnter(object sender, MouseEventArgs e)
    {
      (sender as Button).ToolTip = Context.Instance.TranslateDefault("Delete label");

    }

    private void butFieldDelete_MouseEnter(object sender, MouseEventArgs e)
    {
      (sender as Button).ToolTip = Context.Instance.TranslateDefault("Delete field form the label");
    }
    #endregion



  }
}