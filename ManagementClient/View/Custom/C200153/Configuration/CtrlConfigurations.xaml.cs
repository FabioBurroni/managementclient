﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Utilities.Extensions;
using Model;
using Model.Common.Configuration;
using Model.Custom.C200153;
using View.Common.Configuration;
using MaterialDesignThemes.Wpf;
using MaterialDesignExtensions.Controls;

namespace View.Custom.C200153.Configuration
{
  /// <summary>
  /// Interaction logic for CtrlConfigurations.xaml
  /// </summary>
  public partial class CtrlConfigurations : CtrlBaseC200153
  {

    #region Costruttore
    public CtrlConfigurations()
    {
      InitializeComponent();
      Init();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitleConfPar.Text = Context.Instance.TranslateDefault((string)txtBlkTitleConfPar.Tag);
      txtBlkTitlePallRed.Text = Context.Instance.TranslateDefault((string)txtBlkTitlePallRed.Tag);

      txtBlkHeaderMaintenance.Text = Context.Instance.TranslateDefault((string)txtBlkHeaderMaintenance.Tag);
      txtBlkPalletCode.Text = Context.Instance.TranslateDefault((string)txtBlkPalletCode.Tag);
      txtBlkDestination.Text = Context.Instance.TranslateDefault((string)txtBlkDestination.Tag);
      butRededtinateConfirm.Content = Context.Instance.TranslateDefault((string)butRededtinateConfirm.Tag);

      txtBlkLabel.Text = Context.Instance.TranslateDefault((string)txtBlkLabel.Tag);
      txtBlkPrinter.Text = Context.Instance.TranslateDefault((string)txtBlkPrinter.Tag);

      //txtBlkHeaderConfig.Text = Context.Instance.TranslateDefault((string)txtBlkHeaderConfig.Tag);


      //CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region FIELDS
    List<ConfParameterClient> _paramList = new List<ConfParameterClient>(); 
    #endregion

    private void Init()
    {
      #region P10_ShapeCtrlGoOnIfNoAreaAvailable
      ConfParameterClient p = new ConfParameterBool()
      {
        Title = "SHAPE CONTROL",
        Name = "P10_ShapeCtrlGoOnIfNoAreaAvailable",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "P10 " + "SHAPECONTROL GO ON IF NO AREA IS AVAILABLE",
        Value = false,
        DefaultValue = "True"
      };
      _paramList.Add(p);
      #endregion

      #region P74_ShapeCtrlGoOnIfNoAreaAvailable
      p = new ConfParameterBool()
      {
        Title = "SHAPE CONTROL",
        Name = "P74_ShapeCtrlGoOnIfNoAreaAvailable",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "P74 " + "SHAPECONTROL GO ON IF NO AREA IS AVAILABLE",
        Value = false,
        DefaultValue = "True"
      };
      _paramList.Add(p);
      #endregion

      #region P2010_ShapeCtrlGoOnIfNoAreaAvailable
      p = new ConfParameterBool()
      {
        Title = "SHAPE CONTROL",
        Name = "P2010_ShapeCtrlGoOnIfNoAreaAvailable",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "P2010 " + "SHAPECONTROL GO ON IF NO AREA IS AVAILABLE",
        Value = false,
        DefaultValue = "True"
      };
      _paramList.Add(p);
      #endregion

      #region P2074_ShapeCtrlGoOnIfNoAreaAvailable
      p = new ConfParameterBool()
      {
        Title = "SHAPE CONTROL",
        Name = "P2074_ShapeCtrlGoOnIfNoAreaAvailable",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "P2074 " + "SHAPECONTROL GO ON IF NO AREA IS AVAILABLE",
        Value = false,
        DefaultValue = "True"
      };
      _paramList.Add(p);
      #endregion

      #region SelectWhCell_LogDb
      p = new ConfParameterBool()
      {
        Title = "SELECT WAREHOUSE CELL LOG DB",
        Name = "SelectWhCell_LogDb",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "LOG IN DATABASE DATA ABOUT SELECT WH CELL",
        Value = true,
        DefaultValue = "true"
      };
      _paramList.Add(p);
      #endregion

      #region SelectWhCell_LogFile
      p = new ConfParameterBool()
      {
        Title = "SELECT WAREHOUSE CELL LOG FILE",
        Name = "SelectWhCell_LogFile",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "LOG IN FILE DETAILS ABOUT SELECT WH CELL",
        Value = true,
        DefaultValue = "true"
      };
      _paramList.Add(p);
      #endregion


      #region TestMode
      p = new ConfParameterBool()
      {
        Title = "TEST MODALITY",
        Name = "TestMode",
        Icon = PackIconKind.TestTube.ToString(),

        Descr = "WORK MODALITY",
        Value = false,
        DefaultValue = "false"
      };
      _paramList.Add(p);
      #endregion

      //...ORDER MANAGER
      #region OrderManager_MaxPalletsForJob
      p = new ConfParameterInt()
      {
        Title = "ORDER MANAGER MAX PALLETS FOR JOB",
        Name = "OrderManager_MaxPalletsForJob",
        Icon = PackIconKind.Package.ToString(),

        Descr = "MAX PALLETS FOR JOB",
        Value = 1,
        DefaultValue = "2"
      };
      _paramList.Add(p);
      #endregion

      #region OrderManager_DestinationAssignement
      p = new ConfParameterBool()
      {
        Title = "ORDER AUTOMATIC DESTINATION ASSIGNEMENT",
        Name = "OrderManager_DestinationAssignement",
        Icon = PackIconKind.Truck.ToString(),

        Descr = "AUTOMATIC DESTINATION ASSIGNEMENT FOR WAITING ORDERS.",
        Value = 1,
        DefaultValue = "2"
      };
      _paramList.Add(p);
      #endregion

      #region LogOrderManager
      p = new ConfParameterBool()
      {
        Title = "LOG ORDER MANAGER",
        Name = "LogOrderManager",
        Icon = PackIconKind.ViewList.ToString(),
        Descr = "LOG ORDER MANAGER",
        Value = false,
        DefaultValue = "False"
      };
      _paramList.Add(p);
      #endregion

      #region Master_MaxWorkLoad
      p = new ConfParameterInt()
      {
        Title = "MASTER MAX WORK LOAD",
        Name = "Master_MaxWorkLoad",
        Icon = PackIconKind.Truck.ToString(),

        Descr = "MASTER MAX WORK LOAD",
        Value = 5,
        DefaultValue = "5"
      };
      _paramList.Add(p);
      #endregion

      #region WEB CAM
      p = new ConfParameterBool()
      {
        Title = "LOG WEB CAM PLUGIN",
        Name = "WebCam_Log",
        Icon = PackIconKind.WebCamera.ToString(),
        Descr = "LOG WEB CAM PLUGIN",
        Value = false,
        DefaultValue = "True"
      };
      _paramList.Add(p);

      p = new ConfParameterInt()
      {
        Title = "WEB CAM PLUGIN INACTIVITY TIME",
        Name = "WebCam_InictivityTimeS",
        Icon = PackIconKind.WebCamera.ToString(),

        Descr = "WEB CAM PLUGIN INACTIVITY TIME",
        Value = 600,
        DefaultValue = "600"
      };
      _paramList.Add(p);

      #endregion

      foreach (var parameter in _paramList)
      {
        if (parameter.ParamType == typeof(string))
        {
          CtrlConfStringParam ctrl = new CtrlConfStringParam();
          ctrl.ConfParameter = parameter;
          ctrl.OnCancel += Ctrl_OnCancel;
          ctrl.OnConfirm += Ctrl_OnConfirm;
          wp.Children.Add(ctrl);
        }
        else if (parameter.ParamType == typeof(int))
        {
          CtrlConfIntParam ctrl = new CtrlConfIntParam();
          ctrl.ConfParameter = parameter;
          ctrl.OnCancel += Ctrl_OnCancel;
          ctrl.OnConfirm += Ctrl_OnConfirm;
          wp.Children.Add(ctrl);
        }
        else if (parameter.ParamType == typeof(bool))
        {
          CtrlConfCheckParameter ctrl = new CtrlConfCheckParameter();
          ctrl.ConfParameter = parameter;
          ctrl.OnCancel += Ctrl_OnCancel;
          ctrl.OnConfirm += Ctrl_OnConfirm;
          wp.Children.Add(ctrl);
        }
        else if (parameter.ParamType.IsEnum)
        {
          CtrlConfComboParameter ctrl = new CtrlConfComboParameter();
          ctrl.ConfParameter = parameter;
          ctrl.OnCancel += Ctrl_OnCancel;
          ctrl.OnConfirm += Ctrl_OnConfirm;
          wp.Children.Add(ctrl);
        }
      }

    }


    #region COMANDI E RISPOSTE
    private void Cmd_MM_RedestinatePallet(string palletCode, string positionCode)
    {
      CommandManagerC200153.MM_RedestinatePallet(this, palletCode, positionCode);
    }
    private void MM_RedestinatePallet(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      C200153_CustomResult cr = dwr.Result.ConvertTo<C200153_CustomResult>();
      MessageBox.Show(cr.ToString());


    }


    private void Cmd_CO_Configuration_GetAll()
    {
      CommandManagerC200153.CO_Configuration_GetAll(this);
    }
    private void CO_Configuration_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var listNew = dwr.Data as List<ConfParameterServer>;
      if (listNew != null)
      {
        foreach (var pNew in listNew)
        {
          var confParameter = _paramList.FirstOrDefault(p => p.Name == pNew.Name);
          if (confParameter != null)
          {
            confParameter.Update(pNew);
          }
        }
      }
    }

    private void Cmd_CO_Configuration_Get(string shippingLaneCode)
    {
      CommandManagerC200153.CO_Configuration_Get(this, shippingLaneCode);
    }
    private void CO_Configuration_Get(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var pNew = dwr.Data as ConfParameterServer;
      if (pNew != null)
      {
        var confParameter = _paramList.FirstOrDefault(p => p.Name == pNew.Name);
        if (confParameter != null)
        {
          confParameter.Update(pNew);
        }
      }
    }

    private void Cmd_CO_Configuration_Set(string parameterName, string parameterValue)
    {
      CommandManagerC200153.CO_Configuration_Set(this, parameterName, parameterValue);
    }

    private void CO_Configuration_Set(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      C200153_CustomResult result = dwr.Result.ConvertTo<C200153_CustomResult>();
      if (result == C200153_CustomResult.OK)
      {

      }
      else
      {

      }

      MessageBox.Show(result.ToString());

      Cmd_CO_Configuration_Get(commandMethodParameters[1]);
    }

    #endregion

    #region eventi
    private void Ctrl_OnConfirm(ConfParameterClient confPar)
    {
      Cmd_CO_Configuration_Set(confPar.Name, confPar.SelectedValue);
    }

    private void Ctrl_OnCancel(ConfParameterClient confPar)
    {
      Cmd_CO_Configuration_Get(confPar.Name);

    }

    private void CtrlBaseLoaded(object sender, RoutedEventArgs e)
    {
      Cmd_CO_Configuration_GetAll();
    }


    public override void Closing()
    {

    }
    #endregion

    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Cmd_CO_Configuration_GetAll();
      Translate();
    }


    #region REDESTINATE PALLET
    private async void butRededtinateConfirm_Click(object sender, RoutedEventArgs e)
    {
      string palletCode = txtRedestinatePalletCode.Text;
      if(string.IsNullOrEmpty(palletCode))
      {
        AlertDialogArguments dialogArgs = new AlertDialogArguments
        {
          Title = "Error on redestination request",
          Message = "Specify Pallet Code",
          OkButtonLabel = "GOT IT"
        };

        await AlertDialog.ShowDialogAsync("RootDialog", dialogArgs);

        return;
      }

      var si = cbRedestinateDestination.SelectedItem;
      if(si==null)
      {
        AlertDialogArguments dialogArgs = new AlertDialogArguments
        {
          Title = "Error on redestination request",
          Message = "Specify Pallet Destination",
          OkButtonLabel = "GOT IT"
        };

        await AlertDialog.ShowDialogAsync("RootDialog", dialogArgs);

        return;
      }

      var cbi = si as System.Windows.Controls.ComboBoxItem;
      string positionCode = cbi.Content.ToString();

      if(string.IsNullOrEmpty(positionCode))
      {
        AlertDialogArguments dialogArgs = new AlertDialogArguments
        {
          Title = "Error on redestination request",
          Message = "Specify Pallet Destination",
          OkButtonLabel = "GOT IT"
        };

        await AlertDialog.ShowDialogAsync("RootDialog", dialogArgs);

        return;
      }

      Cmd_MM_RedestinatePallet(palletCode, positionCode);

      


    }
    #endregion
  }
}
