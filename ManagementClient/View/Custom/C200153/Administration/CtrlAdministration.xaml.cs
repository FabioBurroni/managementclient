﻿using System.Windows.Controls;
using Model;

namespace View.Custom.C200153.Administration
{
    /// <summary>
    /// Interaction logic for CtrlAdministration.xaml
    /// </summary>
    public partial class CtrlAdministration : CtrlFunction
    {
        #region Constructor

        public CtrlAdministration()
        {
            InitializeComponent();
        }

        #endregion

        #region Override

        public override void Closing()
        {
            ctrlPluginManager.Closing();
        }
        #endregion
    }
}