﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.Administration;
using Utilities.Extensions;

namespace View.Custom.C200153.Administration
{
  /// <summary>
  /// Interaction logic for CtrlAdmin.xaml
  /// </summary>
  public partial class CtrlAdmin : CtrlBaseC200153
  {
    #region CONSTRUCTOR
    public CtrlAdmin()
    {
      InitializeComponent();
    }
    #endregion

    #region MASTERS
    #region PUBLIC PROPERTIES
    public ObservableCollectionFast<C200153_Master> MasterL { get; set; } = new ObservableCollectionFast<C200153_Master>(); 
    #endregion

    #region COMANDS
    private void Cmd_ADM_Master_GetAll()
    {
      CommandManagerC200153.ADM_Master_GetAll(this);
    }

    private void ADM_Master_GetAll(IList<string> commandMethodParameters, Model.IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      List<C200153_Master> masterL = dwr.Data as List<C200153_Master>;
      if (masterL == null)
        return;
      CollectionHelper.UpdateAll<C200153_Master>(MasterL, masterL, "Code");
    }

    private void Cmd_ADM_Master_GetByCode(string masterCode)
    {
      CommandManagerC200153.ADM_Master_GetByCode(this, masterCode);
    }

    private void ADM_Master_GetByCode(IList<string> commandMethodParameters, Model.IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C200153_Master master = dwr.Data as C200153_Master;
      if (master == null)
        return;
      MasterL.FirstOrDefault(m => m.Code.EqualsIgnoreCase(master.Code))?.Update(master);
    }


    private void Cmd_ADM_Master_Update(string masterCode, C200153_MasterCategory category)
    {
      CommandManagerC200153.ADM_Master_Update(this, masterCode, category);
    }

    private void ADM_Master_Update(IList<string> commandMethodParameters, Model.IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Result == null) return;
      C200153_CustomResult result = dwr.Result.ConvertTo<C200153_CustomResult>();
      if (result == C200153_CustomResult.OK)
      {
        string masterCode = commandMethodParameters[1];
        Cmd_ADM_Master_GetByCode(masterCode);
      }
      else
      {
        MessageBox.Show($"Error Occurred: {result}");
        Cmd_ADM_Master_GetAll();
      }
    }
    #endregion

    #region EVENTS
    private void butMasterRefresh_Click(object sender, RoutedEventArgs e)
    {
      Cmd_ADM_Master_GetAll();

    }

    private void butMasterUpdate_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      var master = b.Tag as C200153_Master;
      if (master != null)
        Cmd_ADM_Master_Update(master.Code, master.Category);
    }
    #endregion
    #endregion

    #region EXIT POSITIONS
    #region PUBLIC PROPERTIES
    public ObservableCollectionFast<C200153_ExitPosition> ExitPosL { get; set; } = new ObservableCollectionFast<C200153_ExitPosition>();
    #endregion

    #region COMANDS
    private void Cmd_ADM_ExitPosition_GetAll()
    {
      CommandManagerC200153.ADM_ExitPosition_GetAll(this);
    }

    private void ADM_ExitPosition_GetAll(IList<string> commandMethodParameters, Model.IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      List<C200153_ExitPosition> list = dwr.Data as List<C200153_ExitPosition>;
      if (list == null)
        return;
      CollectionHelper.UpdateAll<C200153_ExitPosition>(ExitPosL, list, "Code");
    }

    private void Cmd_ADM_ExitPosition_GetByCode(string code)
    {
      CommandManagerC200153.ADM_ExitPosition_GetByCode(this, code);
    }

    private void ADM_ExitPosition_GetByCode(IList<string> commandMethodParameters, Model.IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C200153_ExitPosition exit = dwr.Data as C200153_ExitPosition;
      if (exit == null)
        return;
      ExitPosL.FirstOrDefault(m => m.Code.EqualsIgnoreCase(exit.Code))?.Update(exit);
    }


    private void Cmd_ADM_ExitPosition_Update(string code, int maxPalletInTransit)
    {
      CommandManagerC200153.ADM_ExitPosition_Update(this, code, maxPalletInTransit);
    }

    private void ADM_ExitPosition_Update(IList<string> commandMethodParameters, Model.IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Result == null) return;
      C200153_CustomResult result = dwr.Result.ConvertTo<C200153_CustomResult>();
      if (result == C200153_CustomResult.OK)
      {
        string code = commandMethodParameters[1];
        Cmd_ADM_ExitPosition_GetByCode(code);
      }
      else
      {
        MessageBox.Show($"Error Occurred: {result}");
        Cmd_ADM_ExitPosition_GetAll();
      }
    }
    #endregion

    #region EVENTS
    private void butExitPosUpdate_Click(object sender, RoutedEventArgs e)
    {
      var b = sender as Button;
      var pos = b.Tag as C200153_ExitPosition;
      if(pos!=null)
      {
        Cmd_ADM_ExitPosition_Update(pos.Code, pos.MaxPalletInTransit);
      }
    }

    private void butExitPosRefresh_Click(object sender, RoutedEventArgs e)
    {
      Cmd_ADM_ExitPosition_GetAll();
    }
    #endregion
    #endregion

    #region REDESTINATION PALLET

    #region COMANDI E RISPOSTE
    private void Cmd_MM_RedestinatePallet()
    {
      if (string.IsNullOrEmpty(txtPalletCode.Text))
      {
        return;
      }
      if (string.IsNullOrEmpty(txtPositionCode.Text))
      {
        return;
      }
      CommandManagerC200153.MM_RedestinatePallet(this, txtPalletCode.Text, txtPositionCode.Text);
    }
    private void MM_RedestinatePallet(IList<string> commandMethodParameters, Model.IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null) return;

      MessageBox.Show(dwr.Result);
    }
    #endregion

    #region EVENTS
    private void butConfirm_Click(object sender, RoutedEventArgs e)
    {
      Cmd_MM_RedestinatePallet();
    }
    #endregion

    #endregion


    #region EVENTS

    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Cmd_ADM_Master_GetAll();
      Cmd_ADM_ExitPosition_GetAll();
    }

    #endregion

    
  }
}
