﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Custom.C200153;

namespace View.Custom.C200153.TrafficBoard
{
  /// <summary>
  /// Interaction logic for CtrlTaskFilter.xaml
  /// </summary>
  public partial class CtrlTrafficBoardTaskFilter : CtrlBaseC200153
  {
    public C200153_TrafficBoardTaskFilter Filter
    {
      get { return (C200153_TrafficBoardTaskFilter)GetValue(FilterProperty); }
      set { SetValue(FilterProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Filter.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty FilterProperty =
        DependencyProperty.Register("Filter", typeof(C200153_TrafficBoardTaskFilter), typeof(CtrlTrafficBoardTaskFilter), new PropertyMetadata(null));

    public CtrlTrafficBoardTaskFilter()
    {
      InitializeComponent();
    }

    protected override void Translate()
    {
      
    }

    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    
    private void butClear_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
    }

    private void butSend_Click(object sender, RoutedEventArgs e)
    {
      Filter.FireOnSearch();
    }
  }
}