﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.OrderManager;

namespace View.Custom.C200153.TrafficBoard
{
  /// <summary>
  /// Interaction logic for CtrlPalletTracking.xaml
  /// </summary>
  public partial class CtrlTrafficTask : CtrlBaseC200153
  {

    #region POPERTIES
    #region private

    private string _labelPalletCode = "PALLET CODE";
    private string _labelActualPosition = "POSITION";
    private string _labelDestination = "DESTINATION";

    #endregion private

    public C200153_TrafficBoardTaskFilter Filter { get; set; } = new C200153_TrafficBoardTaskFilter();

    public ObservableCollectionFast<C200153_PalletTraffic> PalletL_tot { get; set; } = new ObservableCollectionFast<C200153_PalletTraffic>();
    public ObservableCollectionFast<C200153_PalletTraffic> PalletL { get; set; } = new ObservableCollectionFast<C200153_PalletTraffic>();
    public ObservableCollectionFast<C200153_PalletTraffic> PalletL2 { get; set; } = new ObservableCollectionFast<C200153_PalletTraffic>();


    #region Label to localize

    public string LabelPalletCode
    {
      get { return _labelPalletCode; }
      set
      {
        if (value != _labelPalletCode)
        {
          _labelPalletCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    public string LabelActualPosition
    {
      get { return _labelActualPosition; }
      set
      {
        if (value != _labelActualPosition)
        {
          _labelActualPosition = value;
          NotifyPropertyChanged();
        }
      }
    }

    public string LabelDestination
    {
      get { return _labelDestination; }
      set
      {
        if (value != _labelDestination)
        {
          _labelDestination = value;
          NotifyPropertyChanged();
        }
      }
    }

    

    #endregion Label to localize


    #endregion POPERTIES

    #region COSTRUTTORE

    public CtrlTrafficTask()
    {
      Filter.OnSearch += Filter_OnSearch;
      Filter.MaxItems = 28;
      InitializeComponent();

      DispatcherTimer timer = new DispatcherTimer();
      timer.Interval = TimeSpan.FromSeconds(10);
      timer.Tick += Timer_Tick;
      timer.Start();
    }

    #endregion COSTRUTTORE

    protected override void Translate()
    {
      txtBlkTitle.Text = Localization.Localize.LocalizeDefaultString((String)txtBlkTitle.Tag);
      txtBlkDescription.Text = Localization.Localize.LocalizeDefaultString((String)txtBlkDescription.Tag);

      txtBlkNumberOfResult.Text = Localization.Localize.LocalizeDefaultString((String)txtBlkNumberOfResult.Tag);
      butBack.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");

      //Label column header
      string labelBox = "PALLET CODE";
      string labelPosition = "POSITION";
      string labelDestinazione = "DESTINATION";
      LabelPalletCode = Context.Instance.TranslateDefault((string)labelBox);
      LabelActualPosition = Context.Instance.TranslateDefault((string)labelPosition);
      LabelDestination = Context.Instance.TranslateDefault((string)labelDestinazione);


      CollectionViewSource.GetDefaultView(dataGrid.ItemsSource).Refresh();
      CollectionViewSource.GetDefaultView(dataGrid2.ItemsSource).Refresh();
    }

    public bool HasNext
    {
      get { return (bool)GetValue(HasNextProperty); }
      set { SetValue(HasNextProperty, value); }
    }

    // Using a DependencyProperty as the backing store for HasNext.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty HasNextProperty =
        DependencyProperty.Register("HasNext", typeof(bool), typeof(CtrlTrafficTask), new PropertyMetadata(false));

    public bool HasBack
    {
      get { return (bool)GetValue(HasBackProperty); }
      set { SetValue(HasBackProperty, value); }
    }

    // Using a DependencyProperty as the backing store for HasBack.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty HasBackProperty =
        DependencyProperty.Register("HasBack", typeof(bool), typeof(CtrlTrafficTask), new PropertyMetadata(false));

    #region COMANDI E RISPOSTE

    private void Cmd_Get_Pallet_Traffic()
    {
      PalletL_tot.Clear();
      PalletL.Clear();
      PalletL2.Clear();
      CommandManagerC200153.RM_Pallet_Traffic_GetAll(this, Filter);
    }

    private void RM_Pallet_Traffic_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var pL = dwr.Data as List<C200153_PalletTraffic>;
      var palletL = pL.Where(p => p.FinalDestination != "").ToList();
      if (palletL != null)
      {
        PalletL_tot.AddRange(palletL.Take(Filter.MaxItems));

        var n = palletL.Count();
        if (n > Filter.MaxItems / 2)
        {
          PalletL.AddRange(palletL.GetRange(0, Filter.MaxItems / 2));
          PalletL2.AddRange(palletL.GetRange(((Filter.MaxItems / 2) + 1), (n - ((Filter.MaxItems / 2) + 1))));
        }
        else
          PalletL.AddRange(palletL.Take(Filter.MaxItems));
      }

      if (PalletL.Count > Filter.MaxItems)
        HasNext = true;
      else
        HasNext = false;

      if (Filter.Index > 0)
        HasBack = true;
      else
        HasBack = false;
    }
     
    #endregion COMANDI E RISPOSTE

    #region EVENTI

    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
      Cmd_Get_Pallet_Traffic();
    }

    private void Filter_OnSearch()
    {
      Cmd_Get_Pallet_Traffic();
    }

    private void butBack_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > 0)
      {
        Filter.Index--;
        Cmd_Get_Pallet_Traffic();
      }
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_Get_Pallet_Traffic();
    }

    private void Timer_Tick(object sender, EventArgs e)
    {
      //...send the command only when the Control is selected
      if (!this.IsSelected)
        return;
      Cmd_Get_Pallet_Traffic();
    }

    #endregion EVENTI
  }
}