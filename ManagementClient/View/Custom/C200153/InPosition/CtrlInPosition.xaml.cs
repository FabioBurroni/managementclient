﻿using Configuration;
using Model;
using Model.Attributes;
using Model.Custom.C200153;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using XmlCommunicationManager.Common.Xml;

namespace View.Custom.C200153.InPosition
{
  /// <summary>
  /// Interaction logic for CtrlCheckIn.xaml
  /// </summary>
  public partial class CtrlInPosition : CtrlBaseC200153
  {
    #region PublicProperties

    private bool _TooManyPalletInPos;

    public bool TooManyPalletInPos
    {
      get { return _TooManyPalletInPos; }
      set
      { 
        _TooManyPalletInPos = value;
        NotifyPropertyChanged("TooManyPalletInPos");
      }
    }

    private C200153_PositionShapeControl_Pallet _PalletInPos;

    public C200153_PositionShapeControl_Pallet PalletInPos
    {
      get { return _PalletInPos; }
      set 
      { 
        _PalletInPos = value;
        NotifyPropertyChanged("PalletInPos");
      }
    }

    private string _PosCode;

    public string PosCode
    {
      get { return _PosCode; }
      set 
      { 
        _PosCode = value;
        NotifyPropertyChanged("PosCode");
      }
    }

    #endregion

   

    #region Costruttore

    public CtrlInPosition()
    {
      InitializeComponent();
            //...Timer will update the input display every 5 seconds
            int interval = 5;//seconds
            var timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, interval);
            timer.Start();
      Cmd_IP_Get_InputPositionCodes();
    }

    #endregion Costruttore

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      
      txtBlkBatchLength.Text = Context.Instance.TranslateDefault((string)txtBlkBatchLength.Tag);
      txtBlkDateBorn.Text = Context.Instance.TranslateDefault((string)txtBlkDateBorn.Tag);
      txtBlkProductionDate.Text = Context.Instance.TranslateDefault((string)txtBlkProductionDate.Tag);
      txtBlkExpiryDate.Text = Context.Instance.TranslateDefault((string)txtBlkExpiryDate.Tag);
      txtBlkBatchLength.Text = Context.Instance.TranslateDefault((string)txtBlkBatchLength.Tag);
      
      iconPalletCode.ToolTip = Context.Instance.TranslateDefault((string)iconPalletCode.Tag);
      iconPalletCodeService.ToolTip = Context.Instance.TranslateDefault((string)iconPalletCodeService.Tag);
      iconArticleCode.ToolTip = Context.Instance.TranslateDefault((string)iconArticleCode.Tag);
      iconDepositor.ToolTip = Context.Instance.TranslateDefault((string)iconDepositor.Tag);
      iconExpiryDate.ToolTip = Context.Instance.TranslateDefault((string)iconExpiryDate.Tag);
      iconBatch.ToolTip = Context.Instance.TranslateDefault((string)iconBatch.Tag);
      iconHeight.ToolTip = Context.Instance.TranslateDefault((string)iconHeight.Tag);
      iconWeight.ToolTip = Context.Instance.TranslateDefault((string)iconWeight.Tag);
      iconReject.ToolTip = Context.Instance.TranslateDefault((string)iconReject.Tag);
      
      icon_reject.ToolTip = Context.Instance.TranslateDefault((string)icon_reject.Tag);
      icon_Result.ToolTip = Context.Instance.TranslateDefault((string)icon_Result.Tag);

      txtBlkTooManyPalletInPosition.Text = Context.Instance.TranslateDefault((string)txtBlkTooManyPalletInPosition.Tag);
      txtBlkNoPalletInPosition.Text = Context.Instance.TranslateDefault((string)txtBlkNoPalletInPosition.Tag);
    }

    #endregion TRADUZIONI

    #region NOTIFICHE

    /// <summary>
    /// Cambio pallet in posizione
    /// </summary>
    /// <param name="xmlcmd"></param>
    [XmlNotification(BringToFront = false)]
    public void Notify_IP_Pallet_OnPosition_Changed(XmlCommand xmlcmd)
    {
      Console.WriteLine(xmlcmd);
      Cmd_IP_Get_PalletInPos();
    }

    #endregion NOTIFICHE

    #region COMANDI E RISPOSTE

    private void Cmd_IP_Get_InputPositionCodes()
    {
      CommandManagerC200153.IP_Get_InputPositionCodes(this, Context.Instance.Positions);
    }

    private void IP_Get_InputPositionCodes(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var positionEntryList = dwr.Data as List<string>;
      PosCode = positionEntryList.FirstOrDefault();
      Refresh();
    }

    private void Cmd_IP_Get_PalletInPos()
    {
      if (PosCode != null)
        CommandManagerC200153.IP_Get_PalletInPos(this, PosCode);
    }

    private void IP_Get_PalletInPos(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var list = dwr.Data as List<C200153_PositionShapeControl_Pallet>;
      TooManyPalletInPos = list.Count > 1;
      PalletInPos = list.FirstOrDefault();
    }

    #endregion COMANDI E RISPOSTE

    private void Refresh()
    {
      Cmd_IP_Get_PalletInPos();
    }

    private void timer_Tick(object sender, EventArgs e)
    {
        Refresh();
    }

    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();

      if (PosCode == null)
        Cmd_IP_Get_InputPositionCodes();
      else
        Refresh();
    }
  }
}