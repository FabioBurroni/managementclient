﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using ControlzEx.Standard;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.Router;
using Utilities.Extensions;
using View.Common.Languages;
using View.Custom.C200153.Converter;

namespace View.Custom.C200153.Router
{
  /// <summary>
  /// Interaction logic for CtrlRouter.xaml
  /// </summary>
  public partial class CtrlRouting : CtrlBaseC200153
  {
    Timer switchingTimer = new Timer();
    
    #region PUBLIC PROPERTIES
    public ObservableCollectionFast<C200153_RouterConfigurationInfo> RouterConfigurationInfoL { get; set; } = new ObservableCollectionFast<C200153_RouterConfigurationInfo>();
      
    private bool _isSwitching;
    public bool IsSwitching
    {
      get { return _isSwitching; }
      set 
      {
        _isSwitching = value;
        NotifyPropertyChanged();
      }
    }

    private C200153_RouterConfigurationInfo _RouterConfigurationSelected = new C200153_RouterConfigurationInfo();
    public C200153_RouterConfigurationInfo RouterConfigurationSelected
    {
      get { return _RouterConfigurationSelected; }
      set 
      { 
        _RouterConfigurationSelected = value;
        NotifyPropertyChanged("RouterConfigurationSelected");
      }
    }

    private C200153_RouterConfigurationEnum _ConfigToRequest;
    public C200153_RouterConfigurationEnum ConfigToRequest
    {
      get { return _ConfigToRequest; }
      set
      {
        _ConfigToRequest = value;
        NotifyPropertyChanged("ConfigToRequest");
      }
    }
    #endregion

   

    #region CONSTRUCTOR
    public CtrlRouting()
    {
      InitializeComponent();
      //Router = new C200153_RouterConfigurationInfo();
      ConfigToRequest = C200153_RouterConfigurationEnum.DEFAULT;

      switchingTimer.AutoReset = true;
      switchingTimer.Stop();
      switchingTimer.Elapsed += new ElapsedEventHandler(RouterConfigurationGet);
      switchingTimer.Interval = 10000;
    }

    #endregion

    #region TRANSLATE
    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      butRefresh.ToolTip = Localization.Localize.LocalizeDefaultString("REFRESH");
      txtBlkZone.Text = Context.Instance.TranslateDefault((string)txtBlkZone.Tag);
      txtBlkLegendGreenInput.Text = Context.Instance.TranslateDefault((string)txtBlkLegendGreenInput.Tag);
      txtBlkLegendRedOutput.Text = Context.Instance.TranslateDefault((string)txtBlkLegendRedOutput.Tag);
      txtBlkLegendOrangeReject.Text = Context.Instance.TranslateDefault((string)txtBlkLegendOrangeReject.Tag);

      //Step1
      stepHeaderCurrentConfiguration.FirstLevelTitle = Context.Instance.TranslateDefault("CURRENT CONFIGURATION");
      stepButtonBarChangeMode.Continue = Context.Instance.TranslateDefault((string)stepButtonBarChangeMode.Tag);

      //Step2
      txtBlkStepperHeader2.Text = Context.Instance.TranslateDefault((string)txtBlkStepperHeader2.Tag);
      HintAssist.SetHint(txtBoxActualConfiguration, Localization.Localize.LocalizeDefaultString((string)txtBoxActualConfiguration.Tag));
      txtBlkCurrentConfigurationLastUpdateSwitch.Text = Context.Instance.TranslateDefault((string)txtBlkCurrentConfigurationLastUpdateSwitch.Tag);
      HintAssist.SetHint(cmbBoxNewConfiguration, Localization.Localize.LocalizeDefaultString((string)cmbBoxNewConfiguration.Tag));
      txtBlkRequestedConfigurationLastUpdateSwitch.Text = Context.Instance.TranslateDefault((string)txtBlkRequestedConfigurationLastUpdateSwitch.Tag);

      btnCANCEL_step2_Switch.Content = Context.Instance.TranslateDefault((string)btnCANCEL_step2_Switch.Tag);
      btnCANCEL_step2_Switch.ToolTip = Context.Instance.TranslateDefault((string)btnCANCEL_step2_Switch.Tag);

      btnSET_step2_Switch.Content = Context.Instance.TranslateDefault((string)btnSET_step2_Switch.Tag);
      btnSET_step2_Switch.ToolTip = Context.Instance.TranslateDefault((string)btnSET_step2_Switch.Tag);

      btnFORCE_step2_Switch.Content = Context.Instance.TranslateDefault((string)btnFORCE_step2_Switch.Tag);
      btnFORCE_step2_Switch.ToolTip = Context.Instance.TranslateDefault((string)btnFORCE_step2_Switch.Tag);

      //Step3
      txtBlkStepperHeader3.Text = Context.Instance.TranslateDefault((string)txtBlkStepperHeader3.Tag);
      HintAssist.SetHint(txtBoxActualConfigurationSwitching, Localization.Localize.LocalizeDefaultString((string)txtBoxActualConfigurationSwitching.Tag));
      txtBlkCurrentConfigurationLastUpdateSwitching.Text = Context.Instance.TranslateDefault((string)txtBlkCurrentConfigurationLastUpdateSwitching.Tag);
      HintAssist.SetHint(txtBlkRequestedConfiguration, Localization.Localize.LocalizeDefaultString((string)txtBlkRequestedConfiguration.Tag));
      txtBlkRequestedConfigurationLastUpdateSwitching.Text = Context.Instance.TranslateDefault((string)txtBlkRequestedConfigurationLastUpdateSwitching.Tag);

      btnFORCE_step3_Switching.Content = Context.Instance.TranslateDefault((string)btnFORCE_step3_Switching.Tag);
      btnFORCE_step3_Switching.ToolTip = Context.Instance.TranslateDefault((string)btnFORCE_step3_Switching.Tag);
    }
    #endregion

    #region XML COMMANDS

    private void Cmd_RC_RouterConfigurationGetByZone()
    {
      CommandManagerC200153.RC_RouterConfigurationGetByZone(this, (RouterConfigurationSelected != null)? RouterConfigurationSelected.Zone : String.Empty );
    }

   
    private void RC_RouterConfigurationGetByZone(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var rc = dwr.Data as C200153_RouterConfigurationInfo;

      switchingTimer.Stop();

      RouterConfigurationInfoL.FirstOrDefault(r => r.Zone.ToLower() == rc.Zone.ToLower())?.Update(rc);

      IsSwitching = RouterConfigurationSelected.CurrentConfiguration != RouterConfigurationSelected.RequestedConfiguration;

      if (IsSwitching)
      {
        switchingTimer.Start();

        stepper.IsLinear = false;
        stepper.SelectedIndex = step3_Switching.TabIndex;
        stepper.IsLinear = true;
        LocalSnackbar.ShowMessageInfo("ROUTER IS SWITCHING".TD(), 3);
      }
      else if (stepper.SelectedIndex == step3_Switching.TabIndex)
      {
        cmbBoxNewConfiguration.SelectedValue = RouterConfigurationSelected.AllowedSwitch.FirstOrDefault();

        stepper.IsLinear = false;
        stepper.SelectedIndex = step1_Current.TabIndex;
        stepper.IsLinear = true;
        LocalSnackbar.ShowMessageInfo("ROUTER IS SWITCHED".TD(), 3);
      }
      else if (stepper.SelectedIndex == step1_Current.TabIndex)
      {
        cmbBoxNewConfiguration.SelectedValue = RouterConfigurationSelected.AllowedSwitch.FirstOrDefault();
      }


    }

    private void Cmd_RC_RouterConfigurationGetAllForPositions()
    {
      CommandManagerC200153.RC_RouterConfigurationGetAllForPositions(this, Model.Context.Instance.Positions.ToList());
    }
    private void RC_RouterConfigurationGetAllForPositions(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var rcL = dwr.Data as List<C200153_RouterConfigurationInfo>;
      if (rcL != null)
      {
        foreach (var rc in rcL)
        {
          var rcOld = RouterConfigurationInfoL.ToList().FirstOrDefault(rc_old => rc_old.Zone.ToLower() == rc.Zone.ToLower());
          if (rcOld != null)
          {
            rcOld.Update(rc);
          }
          else
          {
            RouterConfigurationInfoL.Add(rc);
          }
        }
      }

      if (RouterConfigurationInfoL.Count == 1)
      {
        RouterConfigurationSelected = RouterConfigurationInfoL.FirstOrDefault();

        IsSwitching = RouterConfigurationSelected.CurrentConfiguration != RouterConfigurationSelected.RequestedConfiguration;
      
        if (IsSwitching)
        {
          switchingTimer.Start();

          stepper.IsLinear = false;
          stepper.SelectedIndex = step3_Switching.TabIndex;
          stepper.IsLinear = true;
          LocalSnackbar.ShowMessageInfo("ROUTER IS SWITCHING".TD(), 3);
        }
        else if (stepper.SelectedIndex == step3_Switching.TabIndex)
        {
          stepper.IsLinear = false;
          stepper.SelectedIndex = step1_Current.TabIndex;
          stepper.IsLinear = true;
          LocalSnackbar.ShowMessageInfo("ROUTER IS SWITCHED".TD(), 3);
        }
      }
    }

    private void Cmd_RC_RouterConfigurationForceByZone()
    {
      CommandManagerC200153.RC_RouterConfigurationForceByZone(this, (RouterConfigurationSelected != null) ? RouterConfigurationSelected.Zone : String.Empty, ConfigToRequest);
    }
    private void RC_RouterConfigurationForceByZone(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;

      C200153_CustomResult Result = (C200153_CustomResult)dwr.Data;

      var zone = commandMethodParameters[1];

      if (Result == C200153_CustomResult.ROUTER_IS_SWITCHING)
      {
        LocalSnackbar.ShowMessageOk("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(Result), 2);
        stepper.IsLinear = false;
        stepper.SelectedIndex = step3_Switching.TabIndex;
        stepper.IsLinear = true;
      }
        else if (Result == C200153_CustomResult.OK)
      {
        LocalSnackbar.ShowMessageOk("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(Result), 2);
        stepper.IsLinear = false;
        stepper.SelectedIndex = step1_Current.TabIndex;
        stepper.IsLinear = true;
      }
      else if (Result == C200153_CustomResult.ROUTER_SWITCH_NOT_ALLOWED)
      {
        LocalSnackbar.ShowMessageFail("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(Result), 2);
      }

      Cmd_RC_RouterConfigurationGetByZone();

    }

    private void Cmd_RC_RouterConfigurationSetByZone()
    {
      CommandManagerC200153.RC_RouterConfigurationSetByZone(this, (RouterConfigurationSelected != null) ? RouterConfigurationSelected.Zone : String.Empty, ConfigToRequest);
    }
    private void RC_RouterConfigurationSetByZone(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      //if (dwr.Result == null) return;
      if (dwr.Data == null) return;
      C200153_CustomResult Result = (C200153_CustomResult)dwr.Data;
      string zone = commandMethodParameters[1];

      if (Result == C200153_CustomResult.ROUTER_IS_SWITCHING)
      {
        LocalSnackbar.ShowMessageOk("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(Result), 2);
        stepper.IsLinear = false;
        stepper.SelectedIndex = step3_Switching.TabIndex;
        stepper.IsLinear = true;
      }
      else if (Result == C200153_CustomResult.OK)
      {
        LocalSnackbar.ShowMessageOk("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(Result), 2);
        stepper.IsLinear = false;
        stepper.SelectedIndex = step1_Current.TabIndex;
        stepper.IsLinear = true;
      }
      else if (Result == C200153_CustomResult.ROUTER_SWITCH_NOT_ALLOWED)
      {
        LocalSnackbar.ShowMessageFail("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(Result), 2);
      }

      Cmd_RC_RouterConfigurationGetByZone();
      
    }

  
    #endregion

    #region View Event

    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();

      Cmd_RC_RouterConfigurationGetAllForPositions();
    }
    private void CtrlBaseC200153_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      if (!IsVisible)
        switchingTimer.Stop();
    }

  
    private void RouterConfigurationGet(Object myObject, ElapsedEventArgs myEventArgs)
    {
      if (RouterConfigurationSelected != null)
        Cmd_RC_RouterConfigurationGetByZone();
    }

    #endregion

    #region Command Event
    
    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {

      if (RouterConfigurationSelected != null)
        Cmd_RC_RouterConfigurationGetByZone();
      else
        Cmd_RC_RouterConfigurationGetAllForPositions();

    }

    
    private void cbZoneSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (RouterConfigurationSelected != null)
        Cmd_RC_RouterConfigurationGetByZone();

     }
    
    //private void cbNewConf_SelectionChanged(object sender, SelectionChangedEventArgs e)
    //{
    //  var cbi = cmbBoxNewConfiguration.SelectedItem as ComboBoxItem;
    //  if (cbi != null)
    //  {
    //    ConfigToRequest = cbi.Content.ToString().ConvertTo<C200153_RouterConfigurationEnum>();
    //  }

    //}
    
    private void btnSET_step2_Switch_Click(object sender, RoutedEventArgs e)
    {
      //check if current configuration 
      if (ConfigToRequest != RouterConfigurationSelected.CurrentConfiguration)
      {
        //already switching?
        if (!IsSwitching)
        {
          //OK, request new config....
          Cmd_RC_RouterConfigurationSetByZone();
        }
        else
        {
          //already switching....update values - snackbar error
          Cmd_RC_RouterConfigurationGetByZone();

        }
      }
      else
      {
        // selected configuration not valid - snackbar red
        Cmd_RC_RouterConfigurationGetByZone();

      }
    }

    private void btnCANCEL_step2_Switch_Click(object sender, RoutedEventArgs e)
    {
      stepper.IsLinear = false;
      stepper.SelectedIndex = step1_Current.TabIndex;
      stepper.IsLinear = true;
    }

    private void btnFORCE_step2_Switch_Click(object sender, RoutedEventArgs e)
    {
      //OK, force new config....
      Cmd_RC_RouterConfigurationForceByZone();
    }

    private void btnFORCE_step3_Switching_Click(object sender, RoutedEventArgs e)
    {
      //OK, force new config....
      Cmd_RC_RouterConfigurationForceByZone();
    }

    #endregion
  }

}
