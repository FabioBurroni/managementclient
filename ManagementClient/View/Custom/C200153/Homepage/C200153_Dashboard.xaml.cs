﻿using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Windows.Media;

namespace View.Custom.C200153.Homepage
{
  /// <summary>
  /// Interaction logic for C200153_Dashboard.xaml
  /// </summary>
  public partial class C200153_Dashboard : CtrlBaseC200153
  {
    public C200153_Dashboard()
    {
      InitializeComponent();

      prova.LabelFormatter = Formatter;
    }

    public Func<double, string> Formatter = value => (value + "%").ToString();
  }
}
