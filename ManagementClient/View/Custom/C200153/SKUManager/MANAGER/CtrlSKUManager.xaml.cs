﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Common;
using Model.Custom.C200153;
using Model.Custom.C200153.ArticleManager;
using Model.Custom.C200153.Report;
using View.Common.Converter;
using View.Common.Languages;
using View.Common.UserMessage;
using View.Custom.C200153.Converter;

namespace View.Custom.C200153.SKUManager
{
  /// <summary>
  /// Interaction logic for CtrlSKUManager.xaml
  /// </summary>
  public partial class CtrlSKUManager : CtrlBaseC200153
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public int StartIndex { get; set; } = _indexStartValue;
    public int MaxItems { get; set; } = _maxItemsStartValue;

    public C200153_ArticleFilter Filter { get; set; } = new C200153_ArticleFilter();

    public ObservableCollectionFast<C200153_Article> ArticleL { get; set; } = new ObservableCollectionFast<C200153_Article>();
    #endregion

    private C200153_Article ArticleEdit;

    private C200153_Article _ArticleSelected;

    public C200153_Article ArticleSelected
    {
      get { return _ArticleSelected; }
      set 
      { 
        _ArticleSelected = value;
        NotifyPropertyChanged("ArticleSelected");
      }
    }


    #region COSTRUTTORE
    public CtrlSKUManager()
    {

      ArticleEdit = new C200153_Article();
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlArticleFilter.Filter;

      
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkDescription.Text = Context.Instance.TranslateDefault((string)txtBlkDescription.Tag);

      colArtCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colTurnover.Header = Localization.Localize.LocalizeDefaultString("TURNOVER");
      colWrappingProgram.Header = Localization.Localize.LocalizeDefaultString("WRAPPING PROGRAM");

      //colStockWindow.Header = Localization.Localize.LocalizeDefaultString("STOCK WINDOW");
      //colFifoWindow.Header = Localization.Localize.LocalizeDefaultString("FIFO WINDOW");

      txtBlkAddNew.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkAddNew.Tag);
      btnAddNew.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnAddNew.Tag);

      txtBlkEdit.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkEdit.Tag);
      btnEdit.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnEdit.Tag);

      txtBlkDelete.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkDelete.Tag);
      btnDelete.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnDelete.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");

      CollectionViewSource.GetDefaultView(dgArticleL.ItemsSource).Refresh();

    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    private void Cmd_AM_Article_GetAll()
    {
      ArticleL.Clear();
      btnEdit.IsEnabled = false;
      btnDelete.IsEnabled = false;

      CommandManagerC200153.AM_Article_GetAll(this, Filter);
    }
    private void AM_Article_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var artL = dwr.Data as List<C200153_Article>;
      if (artL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (artL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        artL = artL.Take(Filter.MaxItems).ToList();
        ArticleL.AddRange(artL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

    private void Cmd_AM_Article_Update(C200153_Article art)
    {
      CommandManagerC200153.AM_Article_Update(this, art);
    }
    private void AM_Article_Update(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C200153_CustomResult Result = (C200153_CustomResult)dwr.Data;

      if (Result == C200153_CustomResult.OK)
      {
        LocalSnackbar.ShowMessageOk("ARTICLE UPDATED".TD(), 2);
      }
      else
      {
        LocalSnackbar.ShowMessageFail("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(Result), 2);
      }

      Cmd_AM_Article_GetAll();
    }


    private void Cmd_AM_Article_Delete(C200153_Article art)
    {
      CommandManagerC200153.AM_Article_Delete(this, art);
    }
    private void AM_Article_Delete(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C200153_ArticleDeleteResults Result = (C200153_ArticleDeleteResults)dwr.Data;

      if (Result.DeletableResult == Results_Enum.OK)
      {
        if (Result.DeleteResult == C200153_CustomResult.OK)
        {
          LocalSnackbar.ShowMessageOk("ARTICLE DELETED".TD(), 2);
        }
        else
        {
          LocalSnackbar.ShowMessageFail("RESULT".TD() + " : " + CustomResultToLocalizedStringConverter.Convert(Result.DeleteResult), 2);
        }
      }
      else
      {
        LocalSnackbar.ShowMessageFail("RESULT".TD() + " : " + ResultToLocalizedStringConverter.Convert(Result.DeletableResult), 2);
      }

      Cmd_AM_Article_GetAll();
    }

    #endregion


    #region Eventi Ricerca

    private void ctrlArticleFilter_OnSearch()
    {
      Cmd_AM_Article_GetAll();
    }

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_AM_Article_GetAll();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_AM_Article_GetAll();
    }

   

    #endregion

    #region Eventi controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();

      btnEdit.IsEnabled = false;
      btnDelete.IsEnabled = false;
    }

    private void CtrlBaseC200153_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_Article)
      {
        try
        {
          Clipboard.SetText((((C200153_Article)b.Tag)).Code);
        }
        catch
        {

        }
      }
    }

    #endregion

    private void dgArticleL_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (ArticleSelected != null)
      {
        btnEdit.IsEnabled = true;
        ArticleEdit = new C200153_Article()
            {
              Code = ArticleSelected.Code,
              Descr = ArticleSelected.Descr,
              Turnover = ArticleSelected.Turnover,
              WrappingProgram = ArticleSelected.WrappingProgram,
              IsDeleted = ArticleSelected.IsDeleted
            };

        btnDelete.IsEnabled = !ArticleEdit.IsDeleted;
      }
      else
      {
        btnEdit.IsEnabled = false;
        btnDelete.IsEnabled = false;
      }     

    }

    private async void btnAddNew_Click(object sender, RoutedEventArgs e)
    {
      // prepare the view
      var view = new CtrlSKUManagerInsert();

      //show the dialog
      await DialogHost.Show(view, "RootDialogWithoutExit");

      //OK, request update....
      if (view.Result == Model.Common.ShowDialogResults.OK)
        Cmd_AM_Article_Update(view.Article);

    }

    private async void btnEdit_Click(object sender, RoutedEventArgs e)
    {
      if(ArticleEdit != null)
      {
        if (!ArticleEdit.IsDeleted)
        {
          // prepare the view
          var view = new CtrlSKUManagerEdit(ArticleEdit);

          //show the dialog
          await DialogHost.Show(view, "RootDialogWithoutExit");

          //OK, request update....
          if (view.Result == Model.Common.ShowDialogResults.OK)
            Cmd_AM_Article_Update(view.Article);
        }
        else
        {
          LocalSnackbar.ShowMessageFail("ARTICLE IS MARKED AS DELETED".TD(), 3);
        }
      }
      else
      {
        LocalSnackbar.ShowMessageFail("PLEASE SELECT ARTICLE TO EDIT".TD(),3);
      }

      

    }

    private async void btnDelete_Click(object sender, RoutedEventArgs e)
    {
      if (ArticleEdit != null)
      {
        if (!ArticleEdit.IsDeleted)
        {
          // prepare the view
          var view = new CtrlSKUManagerDelete(ArticleEdit);

          //show the dialog
          await DialogHost.Show(view, "RootDialogWithoutExit");

          //OK, request update....
          if (view.Result == Model.Common.ShowDialogResults.OK)
            Cmd_AM_Article_Delete(view.Article);
        }
        else
        {
          LocalSnackbar.ShowMessageFail("ARTICLE IS MARKED AS DELETED".TD(), 3);
        }

      }
      else
      {
        LocalSnackbar.ShowMessageFail("PLEASE SELECT ARTICLE TO DELETE".TD(), 3);
      }



    }
  }
}
