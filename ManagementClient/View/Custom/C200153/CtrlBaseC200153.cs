﻿using Configuration;
using XmlCommunicationManager.XmlClient.Event;
using Utilities.Extensions;
using Model.Custom.C200153;
using Model;
using View.Common;

namespace View.Custom.C200153
{
	public class CtrlBaseC200153 : CtrlBase
	{
		
		#region Constructor

		public CtrlBaseC200153()
		{
			// Check for design mode. 
			if (DesignTimeHelper.IsInDesignMode)
				return;

			InitCommandManager();
		}

    #endregion
    

    #region Private Properties

    protected CommandManagerC200153 CommandManagerC200153 { get; private set; }

		#endregion

		#region Init CommandManager

		private void InitCommandManager()
		{
      CommandManagerC200153 = new CommandManagerC200153(Context.XmlClient, ConfigurationManager.Parameters.CommandPreambleSetting.GetPreamble("custom"));
		}

		#endregion

		#region Override

		protected override void XmlClientOnException(object sender, ExceptionEventArgs e)
		{
			if (ManageXmlClientException(CommandManagerC200153, e))
				return;

			base.XmlClientOnException(sender, e);
		}

		protected override void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs e)
		{
			if (ManageXmlClientReply(CommandManagerC200153, e))
				return;

			base.XmlClientOnRetrieveReplyFromServer(sender, e);
		}

		#endregion

  
  }
}