﻿using System.Collections.Generic;
using System.Windows;
using Localization;
using Model.Custom.C200153.OrderManager;
using Model;
using View.Common.Languages;

namespace View.Custom.C200153.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderInfo.xaml
  /// </summary>
  public partial class CtrlOrderInfo : CtrlBaseC200153
  {
    #region DELEGATI ED EVENTI
    public delegate void OnConfirmHandler(C200153_Order order, bool urgente, int priority, bool completamentoManuale);
    public event OnConfirmHandler OnConfirm; 
    #endregion

    #region PP-WrapperOrder
    public Wrapper_Order WrapperOrder { get; set; } = new Wrapper_Order(); 
    #endregion

    #region DP - OrderSelected


    public C200153_Order OrderSelected
    {
      get { return (C200153_Order)GetValue(OrderSelectedProperty); }
      set { SetValue(OrderSelectedProperty, value); }
    }

    // Using a DependencyProperty as the backing store for OrderSelected.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty OrderSelectedProperty =
        DependencyProperty.Register("OrderSelected", typeof(C200153_Order), typeof(CtrlOrderInfo)
          , new UIPropertyMetadata(null, new PropertyChangedCallback(OrderSelectedChanged)));


    private static void OrderSelectedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      CtrlOrderInfo ctrl = d as CtrlOrderInfo;
      if(ctrl!=null)
      {
        ctrl.WrapperOrder.Order = e.NewValue as C200153_Order;
      }
    }


    #endregion


    


    #region COSTRUTTORE
    public CtrlOrderInfo()
    {
      WordDicAdd("ORDER_INFORMATION", "ORDER INFORMATION");
      WordDicAdd("CUSTOMER", "CUSTOMER");
      WordDicAdd("TYPE", "TYPE");
      WordDicAdd("DATEBORN", "DATE BORN");
      WordDicAdd("DATEEXEC", "DATE EXECUTION");
      WordDicAdd("DATEEND", "DATE END");
      WordDicAdd("LAST_SCAN", "LAST SCAN");
      WordDicAdd("SCAN_RESULT", "SCAN RESULT");

      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkOrderInformation.Header = Context.Instance.TranslateDefault((string)txtBlkOrderInformation.Tag);
      txtBlkDateBorn.Text = Context.Instance.TranslateDefault((string)txtBlkDateBorn.Tag);
      txtBlkDateEnd.Text = Context.Instance.TranslateDefault((string)txtBlkDateEnd.Tag);
      txtBlkEditOrder.Header = Context.Instance.TranslateDefault((string)txtBlkEditOrder.Tag);
      txtBlkExecutionDate.Text = Context.Instance.TranslateDefault((string)txtBlkExecutionDate.Tag);
      txtBlkLastScan.Text = Context.Instance.TranslateDefault((string)txtBlkLastScan.Tag);
      txtBlkPriority.Text = Context.Instance.TranslateDefault((string)txtBlkPriority.Tag);
      txtBlkScanResult.Text = Context.Instance.TranslateDefault((string)txtBlkScanResult.Tag);
      txtBlkSelectedOrder.Text = Context.Instance.TranslateDefault((string)txtBlkSelectedOrder.Tag);
      txtBlkType.Text = Context.Instance.TranslateDefault((string)txtBlkType.Tag);

      butConfirm.ToolTip = Localization.Localize.LocalizeDefaultString((string)butConfirm.Tag);
      butCancel.ToolTip = Localization.Localize.LocalizeDefaultString((string)butCancel.Tag);
    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
    #endregion

    #region TRADUZIONI
    public Dictionary<string, Word> WordDic { get; set; } = new Dictionary<string, Word>();
    private void WordDicAdd(string key, string defaultVal)
    {
      if (WordDic.ContainsKey(key))
        return;
      WordDic.Add(key, new Localization.Word(key, defaultVal, defaultVal.TD()));
    }
    #endregion


    #region EVENTI
    private void butConfirm_Click(object sender, RoutedEventArgs e)
    {
      OnConfirm?.Invoke(OrderSelected, WrapperOrder.Urgente, WrapperOrder.Priority, WrapperOrder.CompletamentoManuale);
    }

    private void butCancel_Click(object sender, RoutedEventArgs e)
    {
      WrapperOrder.Reset();
    } 
    #endregion
  }


  #region Wrapper_Order
  public class Wrapper_Order : ModelBase
  {
    #region Order
    C200153_Order _Order;
    public C200153_Order Order
    {
      get { return _Order; }
      set
      {
        SetValues(value);
      }
    }
    #endregion

    #region Private SetValues
    private void SetValues(C200153_Order newOrder)
    {
      if (newOrder != _Order)
      {
        if (_Order != null)
          _Order.PropertyChanged -= _Order_PropertyChanged;
      }

      _Order = newOrder;

      if (_Order != null)
      {
        Priority = _Order.Priority;
        PriorityOriginalVal = _Order.Priority;
        IsValid = true;
        NotifyPropertyChanged("Priority_IsChanged");
        _Order.PropertyChanged += _Order_PropertyChanged;
      }
      else
      {
        IsValid = false;
      }
    }
    #endregion

    public void Reset()
    {
      Priority = PriorityOriginalVal;
      Urgente = UrgenteOriginalVal;
      CompletamentoManuale = CompletamentoManualeOriginalVal;
      NotifyPropertyChanged("Priority_IsChanged");
    }

    private void _Order_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      if (e.PropertyName == "Priority")
      {
        Priority = _Order.Priority;
        PriorityOriginalVal = _Order.Priority;
        NotifyPropertyChanged("Priority_IsChanged");
      }
    }


    #region IsValid
    private bool _IsValid;
    public bool IsValid
    {
      get { return _IsValid; }
      set
      {
        if (value != _IsValid)
        {
          _IsValid = value;
          NotifyPropertyChanged();
        }
      }
    }
    #endregion

    #region Urgente
    private bool _Urgente;
    public bool Urgente
    {
      get { return _Urgente; }
      set
      {
        if (value != _Urgente)
        {
          _Urgente = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("Urgente_IsChanged");
        }
      }
    }



    private bool _UrgenteOriginalVal;

    public bool UrgenteOriginalVal
    {
      get { return _UrgenteOriginalVal; }
      set { _UrgenteOriginalVal = value; }
    }

    public bool Urgente_IsChanged
    {
      get
      {
        return UrgenteOriginalVal != Urgente;
      }

    }
    #endregion

    #region Priorità

    private int _Priority;
    public int Priority
    {
      get { return _Priority; }
      set
      {
        if (value != _Priority)
        {
          _Priority = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("Priority_IsChanged");
        }
      }
    }

    private int _PriorityOriginalVal;
    public int PriorityOriginalVal
    {
      get { return _PriorityOriginalVal; }
      set
      {
        if (value != _PriorityOriginalVal)
        {
          _PriorityOriginalVal = value;
          //NotifyPropertyChanged();

        }
      }
    }



    public bool Priority_IsChanged
    {
      get
      {
        return Priority != PriorityOriginalVal;
      }
    }


    #endregion


    #region Priorità

    private bool _CompletamentoManuale;
    public bool CompletamentoManuale
    {
      get { return _CompletamentoManuale; }
      set
      {
        if (value != _CompletamentoManuale)
        {
          _CompletamentoManuale = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("CompletamentoManuale_IsChanged");
        }
      }
    }

    private bool _CompletamentoManualeOriginalVal;
    public bool CompletamentoManualeOriginalVal
    {
      get { return _CompletamentoManualeOriginalVal; }
      set
      {
        if (value != _CompletamentoManualeOriginalVal)
        {
          _CompletamentoManualeOriginalVal = value;
          //NotifyPropertyChanged();

        }
      }
    }



    public bool CompletamentoManuale_IsChanged
    {
      get
      {
        return CompletamentoManuale != CompletamentoManualeOriginalVal;
      }
    }


    #endregion




  } 
  #endregion
}
