﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Model;
using Model.Custom.C200153.OrderManager;

namespace View.Custom.C200153.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlCmps.xaml
  /// </summary>
  public partial class CtrlCmps : CtrlBaseC200153
  {
    /// <summary>
    /// Delegato per richiesta info pallet assegnati ad ordine
    /// </summary>
    /// <param name="cmp"></param>
    public delegate void OnInfoHandler(C200153_OrderCmp cmp);
    /// <summary>
    /// Evento per richiesta info pallet assegnati ad ordine
    /// </summary>
    /// <param name="cmp"></param>
    public event OnInfoHandler OnInfo;

    /// <summary>
    /// Delegato per Richiesta giacenza per riga di ordine
    /// </summary>
    /// <param name="cmp"></param>
    public delegate void OnStockRequestHandler(C200153_Order ord, C200153_OrderCmp cmp);
    /// <summary>
    /// Evento per Richiesta giacenza per riga di ordine
    /// </summary>
    /// <param name="cmp"></param>
    public event OnStockRequestHandler OnStockRequest;

    /// <summary>
    /// Delegate for Kil Component
    /// </summary>
    /// <param name="ord"></param>
    /// <param name="cmp"></param>
    public delegate void OnComponentKillRequest(C200153_Order ord, C200153_OrderCmp cmp);
    /// <summary>
    /// Event for Kill Component
    /// </summary>
    public event OnComponentKillRequest OnComponentKill;

    #region Contructor
    public CtrlCmps()
    {
      InitializeComponent();
      //var r = "pippo".TD();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      this.Resources["strCopyArticleCode"]= 
        Context.Instance.TranslateDefault("COPY ARTICLE CODE");
      this.Resources["strAssignedPallets"] = 
        Context.Instance.TranslateDefault("ASSIGNED PALLETS");
      this.Resources["strPressToCancel"] = 
        Context.Instance.TranslateDefault("PRESS TO CANCEL");
      this.Resources["strCheckAvailableStock"] = 
        Context.Instance.TranslateDefault("CHECK AVAILABLE STOCK");

      txtBlkItems.Text = Context.Instance.TranslateDefault((string)txtBlkItems.Tag);
      txtBlkAssigned.Text = Context.Instance.TranslateDefault((string)txtBlkAssigned.Tag);
      txtBlkDelivered.Text = Context.Instance.TranslateDefault((string)txtBlkDelivered.Tag);
      txtBlkLastScanDate.Text = Context.Instance.TranslateDefault((string)txtBlkLastScanDate.Tag);
      txtBlkRemaining.Text = Context.Instance.TranslateDefault((string)txtBlkRemaining.Tag);
      txtBlkRequested.Text = Context.Instance.TranslateDefault((string)txtBlkRequested.Tag);
      txtBlkExecutionResult.Text = Context.Instance.TranslateDefault((string)txtBlkExecutionResult.Tag);

      colArticle.Header = Localization.Localize.LocalizeDefaultString("ARTICLE");
      colAssigned.Header = Localization.Localize.LocalizeDefaultString("ASSIGNED");
      colDelivered.Header = Localization.Localize.LocalizeDefaultString("DELIVERED");
      colDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colBatch.Header = Localization.Localize.LocalizeDefaultString("BATCH");
      colInfo.Header = Localization.Localize.LocalizeDefaultString("INFO");
      colRemaining.Header = Localization.Localize.LocalizeDefaultString("REMAINING");
      colRequested.Header = Localization.Localize.LocalizeDefaultString("REQUESTED");
      colState.Header = Localization.Localize.LocalizeDefaultString("STATE");
      colType.Header = Localization.Localize.LocalizeDefaultString("TYPE");
      //colUnit.Header = Localization.Localize.LocalizeDefaultString("UNIT");
      colKill.Header = Localization.Localize.LocalizeDefaultString("CANCEL");
      //butEdit.ToolTip = Localization.Localize.LocalizeDefaultString("EDIT");

      if (dgOrderCmps.ItemsSource != null)
      CollectionViewSource.GetDefaultView(dgOrderCmps.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI


    #region DP - OrderSelected
    public C200153_Order OrderSelected
    {
      get { return (C200153_Order)GetValue(OrderSelectedProperty); }
      set { SetValue(OrderSelectedProperty, value); }
    }

    // Using a DependencyProperty as the backing store for OrderSelected.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty OrderSelectedProperty =
        DependencyProperty.Register("OrderSelected", typeof(C200153_Order), typeof(CtrlCmps), new PropertyMetadata(null));


    #endregion

    #region DP - CmpL
    public ObservableCollectionFast<C200153_OrderCmp> CmpL
    {
      get { return (ObservableCollectionFast<C200153_OrderCmp>)GetValue(CmpLProperty); }
      set { SetValue(CmpLProperty, value); }
    }

    // Using a DependencyProperty as the backing store for CmpL.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty CmpLProperty =
        DependencyProperty.Register("CmpL", typeof(ObservableCollectionFast<C200153_OrderCmp>), typeof(CtrlCmps), new PropertyMetadata(null));
    #endregion

    #region DP - Cmp




    public C200153_OrderCmp CmpSelected
    {
      get { return (C200153_OrderCmp)GetValue(CmpSelectedProperty); }
      set { SetValue(CmpSelectedProperty, value); }
    }

    // Using a DependencyProperty as the backing store for CmpSelected.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty CmpSelectedProperty =
        DependencyProperty.Register("CmpSelected", typeof(C200153_OrderCmp), typeof(CtrlCmps), new PropertyMetadata(null));






    #endregion

    #region Events
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
    
    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_OrderCmp)
      {
        try
        {
          Clipboard.SetText(((C200153_OrderCmp)b.Tag).ArticleCode);
        }
        catch
        {

        }
      }
    }

    private void butCmpInfo_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_OrderCmp)
      {
        C200153_OrderCmp cmp = b.Tag as C200153_OrderCmp;
        OnInfo?.Invoke(cmp);
      }
    }

    private void butStock_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_OrderCmp)
      {
        C200153_OrderCmp cmp = b.Tag as C200153_OrderCmp;
        OnStockRequest?.Invoke(OrderSelected, cmp);
      }
    }

    private void butKill_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_OrderCmp)
      {
        C200153_OrderCmp cmp = b.Tag as C200153_OrderCmp;
        OnComponentKill?.Invoke(OrderSelected, cmp);
      }
    }
    #endregion
  }
}
