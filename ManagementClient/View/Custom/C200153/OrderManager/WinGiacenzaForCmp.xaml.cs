﻿
using System.Windows;
using Model;
using Model.Custom.C200153.OrderManager;
namespace View.Custom.C200153.OrderManager
{
  /// <summary>
  /// Interaction logic for WinStockForCmp.xaml
  /// </summary>
  public partial class WinStockForCmp : Window
  {
    public string Area { get; set; }
    public string NumberOfPallets { get; set; }

    public C200153_ArticleStockByArea Stock { get; set; } = new C200153_ArticleStockByArea();
    public WinStockForCmp(C200153_ArticleStockByArea stock)
    {
      
      if(stock != null)
      {
        Stock = stock;
      }
      InitializeComponent();

      Translate();

    }

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void Translate()
    {
      Title = Localization.Localize.LocalizeDefaultString("STOCK FOR ORDER ITEM");
      //txtBlkStockFor.Text = Context.Instance.TranslateDefault((string)txtBlkStockFor.Tag);

      Area = Localization.Localize.LocalizeDefaultString("AREA");
      NumberOfPallets = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLETS");

      colAreaCode.Header = Area;
      colNumPallet.Header = NumberOfPallets;
    }
  }
}
