﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.OrderManager;

namespace View.Custom.C200153.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderManager.xaml
  /// </summary>
  public partial class CtrlOrderManager : CtrlBaseC200153
  {
    #region CONSTRUCTOR
    public CtrlOrderManager()
    {
      InitializeComponent();

    } 
    #endregion
        
    private void CtrlOrderCreate_OnOrderLoad(string orderCode)
    {
      ctrlOrderViewer.LoadOrderByCode(orderCode);
      tiOrdersViewer.IsSelected = true;
    }

    #region TRADUZIONI

    protected override void Translate()
    {
      txtTabHeaderOrderBuilder.Text = Context.Instance.TranslateDefault((string)txtTabHeaderOrderBuilder.Tag);
      txtTabHeaderOrderManager.Text = Context.Instance.TranslateDefault((string)txtTabHeaderOrderManager.Tag);
      
    }
    #endregion
    #region COMANDI E RISPOSTE

    #region OM_OrderDestination_GetAll
    private void Cmd_OM_OrderDestination_GetAllForPosition()
    {
      CommandManagerC200153.OM_OrderDestination_GetAllForPositions(this, Model.Context.Instance.Positions.ToList());
    }

    private void OM_OrderDestination_GetAllForPositions(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var list = dwr.Data as List<C200153_ListDestination>;


      if (C200153_ModelContext.Instance.ListDestinations.Count == 0)
      {
        if (list.Count == 0)
          Cmd_OM_OrderDestination_GetAll();
        else
        {
          C200153_ModelContext.Instance.IsListDestination = true;
          C200153_ModelContext.Instance.ListDestinations.AddRange(list);
        }
      }
    }

    private void Cmd_OM_OrderDestination_GetAll()
    {
      CommandManagerC200153.OM_OrderDestination_GetAll(this);
    }

    private void OM_OrderDestination_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var list = dwr.Data as List<C200153_ListDestination>;


      if (C200153_ModelContext.Instance.ListDestinations.Count == 0)
      {
        C200153_ModelContext.Instance.ListDestinations.AddRange(list);
      }
    }
    #endregion


    #endregion

    #region EVENTI COTROLLO
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      if (C200153_ModelContext.Instance.ListDestinations.Count == 0)
        Cmd_OM_OrderDestination_GetAllForPosition();

      Translate();
    }

    private void CtrlBaseC200153_Unloaded(object sender, RoutedEventArgs e)
    {
      //try
      //{
      //  //Authentication.AuthenticationManager.Instance.LoginSession -= Instance_LoginSession;
      //}
      //catch (Exception)
      //{

      //}
    }

    #endregion

  }
}
