﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Model;
using Model.Custom.C200153.OrderManager;
using View.Common.Languages;

namespace View.Custom.C200153.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderLTA.xaml
  /// </summary>
  public partial class CtrlOrderLTA : CtrlBaseC200153
  {
    /// <summary>
    /// Delegate for Complete Pallet
    /// </summary>
    /// <param name="PalletInTransit"></param>
    public delegate void OnPalletCompleteRequest(C200153_PalletInTransit Pallet);
    /// <summary>
    /// Event for Kill Component
    /// </summary>
    public event OnPalletCompleteRequest OnPalletComplete;

    #region DP - PalletInTransitL
    public ObservableCollectionFast<C200153_PalletInTransit> PalletInTransitL
    {
      get { return (ObservableCollectionFast<C200153_PalletInTransit>)GetValue(PalletInTransitLProperty); }
      set { SetValue(PalletInTransitLProperty, value);
          }
    }

    public static readonly DependencyProperty PalletInTransitLProperty =
        DependencyProperty.Register("PalletInTransitL", typeof(ObservableCollectionFast<C200153_PalletInTransit>)
          , typeof(CtrlOrderLTA)
          , new PropertyMetadata(null, PalletInTransitLPropertyChanged));

    #endregion

    private static void PalletInTransitLPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      //
    }


    #region CONSTRUCTOR
    public CtrlOrderLTA()
    {
      
      WordDicAdd("PALLET_IN_TRANSIT", "PALLET IN TRANSIT");
      WordDicAdd("PALLET_IN_TRANSIT_COUNT", "PALLET IN TRANSIT COUNT");
      WordDicAdd("COPY", "COPY");
      WordDicAdd("PALLET_CODE", "PALLET CODE");
      WordDicAdd("ARTICLE_CODE", "ARTICLE CODE");
      WordDicAdd("LOT_CODE", "LOT CODE");
      WordDicAdd("ARTICLE_DESCR", "ARTICLE DESCRIPTION");
      WordDicAdd("POSITION_CODE", "POSITION CODE");
      WordDicAdd("DESTINATION", "DESTINATION");

      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtbLKTitle.Text = Context.Instance.TranslateDefault((string)txtbLKTitle.Tag);
      txtBlkNumberOfPallets.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfPallets.Tag);

      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colCopy.Header = Localization.Localize.LocalizeDefaultString("COPY");
      colDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colBatchCode.Header = Localization.Localize.LocalizeDefaultString("BATCH");
      colDestination.Header = Localization.Localize.LocalizeDefaultString("DESTINATION");
      colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
      colPosition.Header = Localization.Localize.LocalizeDefaultString("POSITION");
      colComplete.Header = Localization.Localize.LocalizeDefaultString("COMPLETE");

      if (dgOrders.ItemsSource != null)
        CollectionViewSource.GetDefaultView(dgOrders.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();

      foreach (var kvp in WordDic)
      {
        kvp.Value.Localized = kvp.Value.DefaultVal.TD();
      }
    }
    #endregion

    #region TRADUZIONI
    public Dictionary<string, Localization.Word> WordDic { get; set; } = new Dictionary<string, Localization.Word>();

    private void WordDicAdd(string key, string defaultVal)
    {
      if (WordDic.ContainsKey(key))
        return;
      WordDic.Add(key, new Localization.Word(key, defaultVal, defaultVal.TD()));
    }
   
    #endregion

    #region EVENTI
    private void butCopyPalletCode_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        Button b = sender as Button;
        if (b == null)
          return;
        Clipboard.SetText(b.Tag as string);

      }
      catch
      {
      }
    }
    #endregion

    private void butComplete_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C200153_PalletInTransit)
      {
        C200153_PalletInTransit pal = b.Tag as C200153_PalletInTransit;

        OnPalletComplete?.Invoke(pal);
      }
    }

    private void CtrlBaseC200153_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
    {

    }
  }
}
