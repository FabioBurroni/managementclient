﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Utilities.Extensions;
using Model;
using Model.Custom.C200153;
using Model.Custom.C200153.OrderManager;
using Model.Custom.C200153.Filter;
using System.Windows.Controls;
using View.Common.Languages;

namespace View.Custom.C200153.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderCreate.xaml
  /// </summary>
  public partial class CtrlOrderCreate : CtrlBaseC200153
  {

    public delegate void OnOrderLoadHandler(string orderCode);
    public event OnOrderLoadHandler OnOrderLoad;


    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region PUBBLIC PROPERTIES
    public C200153_ArticleForOrderFilter Filter { get; set; } = new C200153_ArticleForOrderFilter();
    public ObservableCollectionFast<C200153_StockForOrder> ArticleList { get; set; } = new ObservableCollectionFast<C200153_StockForOrder>();
    public ObservableCollectionFast<C200153_ListDestination> DestinationL { get; set; } = new ObservableCollectionFast<C200153_ListDestination>();

    public C200153_OrderWrapper Order { get; set; } = new C200153_OrderWrapper();

    public C200153_StockForOrder ArticleSelected { get; set; }

    public C200153_OrderComponentWrapper ComponentSelected { get; set; }

    #endregion


    #region COSTRUTTORE
    public CtrlOrderCreate()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkBatchCode.Text = Context.Instance.TranslateDefault((string)txtBlkBatchCode.Tag);
      txtBlkArticleCode.Text = Context.Instance.TranslateDefault((string)txtBlkArticleCode.Tag);
      txtBlkArticlesInWarehouse.Text = Context.Instance.TranslateDefault((string)txtBlkArticlesInWarehouse.Tag);
      txtBlNumberOfItems.Text = Context.Instance.TranslateDefault((string)txtBlNumberOfItems.Tag);
      txtBlkOnlyAvailableArticles.Text = Context.Instance.TranslateDefault((string)txtBlkOnlyAvailableArticles.Tag);
      txtBlkOnlyForMyPosition.Text = Context.Instance.TranslateDefault((string)txtBlkOnlyForMyPosition.Tag);
      txtBlkOrderBuilder.Text = Context.Instance.TranslateDefault((string)txtBlkOrderBuilder.Tag);
      txtBlkOrderCode.Text = Context.Instance.TranslateDefault((string)txtBlkOrderCode.Tag);
      txtBlkOrderItems.Text = Context.Instance.TranslateDefault((string)txtBlkOrderItems.Tag);
      txtBlkSearchForArticle.Text = Context.Instance.TranslateDefault((string)txtBlkSearchForArticle.Tag);
      txtBlNumberOfItems.Text = Context.Instance.TranslateDefault((string)txtBlNumberOfItems.Tag);
      
      colArticle.Header = Localization.Localize.LocalizeDefaultString("ARTICLE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("CODE");
      colDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colBatchCode.Header = Localization.Localize.LocalizeDefaultString("BATCH");
      colDescriptionOrder.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colNumberOfPallets.Header = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLETS");
      colRemove.Header = Localization.Localize.LocalizeDefaultString("REMOVE"); 
      //colType.Header = Localization.Localize.LocalizeDefaultString("TYPE");
      colQuantity.Header = Localization.Localize.LocalizeDefaultString("REMAINING");
      //colUnit.Header = Localization.Localize.LocalizeDefaultString("REQUESTED");
      
      butCancelOrder.ToolTip = Localization.Localize.LocalizeDefaultString("CANCEL");
      butCreateOrder.ToolTip = Localization.Localize.LocalizeDefaultString("CREATE");
      butGenerateOrderCode.ToolTip = Localization.Localize.LocalizeDefaultString("GENERATE ORDER CODE");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butSearch.ToolTip = Localization.Localize.LocalizeDefaultString("SEARCH");

      CollectionViewSource.GetDefaultView(dgArticle.ItemsSource).Refresh();
      CollectionViewSource.GetDefaultView(dgOrder.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
    #endregion


    #region COMANDI E RISPOSTE
    private void Cmd_OM_Stock_BatchArticle()
    {
      string position = string.Empty;
      C200153_ListDestination destination = cbDestinations.SelectedItem as C200153_ListDestination;
      if (destination != null)
      {
        position = destination.PositionList.FirstOrDefault();
      }

      CommandManagerC200153.OM_Stock_BatchArticle(this, position, Filter);
    }
    private void OM_Stock_BatchArticle(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      var giacL = dwr.Data as List<C200153_StockForOrder>;
      if (giacL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (giacL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        giacL = giacL.Take(Filter.MaxItems).ToList();
        ArticleList.AddRange(giacL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

    private void Cmd_OM_Order_Create()
    {
      CommandManagerC200153.OM_Order_Create(this, Order);
    }
    private void OM_Order_Create(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      if (dwr.Data is C200153_CustomResult)
      {
        var result = (C200153_CustomResult)dwr.Data;
        if (result == C200153_CustomResult.OK)
        {
          string orderCode = Order.Code;
          MessageBox.Show("ORDINE CREATO CORRETTAMENTE");
          Order.Reset();
          OnOrderLoad?.Invoke(orderCode);
        }
        else
        {
          MessageBox.Show($"ERRORE CREAZIONE ORDINE: {result}");
        }
      }
    }
    #endregion


    #region EVENTI
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void dgAddArticleToOrder_Click(object sender, RoutedEventArgs e)
    {
      if (ArticleSelected != null)
      {
        var cmp = Order.CmpL.FirstOrDefault(cp => cp.BatchCode.EqualsIgnoreCase(ArticleSelected.BatchCode)
                                                  && cp.ArticleCode.EqualsIgnoreCase(ArticleSelected.Article.Code)
                                          );
        if (cmp != null)
          cmp.QtyRequested++;
        else
          Order.CmpL.Add(new C200153_OrderComponentWrapper()
          {
            BatchCode = ArticleSelected.BatchCode,
            ArticleCode = ArticleSelected.Article.Code,
            ArticleDescr = ArticleSelected.Article.Descr,
            QtyRequested = 1
          });
      }
    }

    private void butSearchCancel_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
      ArticleList.Clear();
    }
    private void butSearch_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index = _indexStartValue;
      Search();
    }


    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Search();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Search();
    }



    private void dgButCmpRemove_Click(object sender, RoutedEventArgs e)
    {
      if (ComponentSelected != null)
      {
        Order.CmpL.Remove(ComponentSelected);
      }
    }

    private void dgButCmpIncrease_Click(object sender, RoutedEventArgs e)
    {
      var rb = sender as System.Windows.Controls.Primitives.RepeatButton;
      if (rb.Tag != null && rb.Tag is C200153_OrderComponentWrapper)
      {
        ((C200153_OrderComponentWrapper)rb.Tag).QtyRequested++;
      }

    }

    private void dgButCmpDecease_Click(object sender, RoutedEventArgs e)
    {
      var rb = sender as System.Windows.Controls.Primitives.RepeatButton;
      if (rb.Tag != null && rb.Tag is C200153_OrderComponentWrapper)
      {
        var cmp = (C200153_OrderComponentWrapper)rb.Tag;
        cmp.QtyRequested--;
        if(cmp.QtyRequested<=0)
        {
          Order.CmpL.Remove(cmp);
        }
      }
    }
    #endregion

    private void butGenerateOrderCode_Click(object sender, RoutedEventArgs e)
    {
      Order.Code = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
    }

    private void butCancelOrder_Click(object sender, RoutedEventArgs e)
    {
      Order.Reset();
      ComponentSelected = null;
    }
    private void butCreateOrder_Click(object sender, RoutedEventArgs e)
    {
      Cmd_OM_Order_Create();
    }
    private void CtrlBaseC200153_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      ArticleList.Clear();
    }

    #region Metodi Privati

    private void Search()
    {
      ArticleList.Clear();
      Cmd_OM_Stock_BatchArticle();
    }

    #endregion

    private void dgArticle_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
    {

    }

    private void tbOnlyForMyPosition_Unchecked(object sender, RoutedEventArgs e)
    {
      Filter.OnlyForMyPositon = C200153_ModelContext.Instance.IsListDestination;
    }

    private void cbDestinations_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      DestinationL.Clear();
      DestinationL.AddRange(C200153_ModelContext.Instance.ListDestinations);
      foreach(var dest in DestinationL)
      {
        dest.Description = dest.Description.TD() ;
      }

      Filter.OnlyForMyPositon = C200153_ModelContext.Instance.IsListDestination;

      cbDestinations.SelectedIndex = 0;

    }
  }
}
