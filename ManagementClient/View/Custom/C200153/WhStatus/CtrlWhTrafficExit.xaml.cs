﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Model.Custom.C200153.WhStatus;

namespace View.Custom.C200153.WhStatus
{
  /// <summary>
  /// Interaction logic for CtrlWhStatus.xaml
  /// </summary>
  public partial class CtrlWhTrafficExit : UserControl
  {
    public string Title
    {
      get { return (string)GetValue(TitleProperty); }
      set { SetValue(TitleProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty TitleProperty =
        DependencyProperty.Register("Title", typeof(string), typeof(CtrlWhTrafficExit), new PropertyMetadata(""));

    public Func<double, string> Formatter = value => (value + "%").ToString();

    #region DP - WhTraffic
    public C200153_WhTrafficExit WhTraffic
    {
      get { return (C200153_WhTrafficExit)GetValue(WhTrafficProperty); }
      set { SetValue(WhTrafficProperty, value); }
    }

    // Using a DependencyProperty as the backing store for WhStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty WhTrafficProperty =
        DependencyProperty.Register("WhTraffic", typeof(C200153_WhTrafficExit), typeof(CtrlWhTrafficExit), new PropertyMetadata(null));

    #endregion

    #region COSTRUTTORE
    public CtrlWhTrafficExit()
    {
      InitializeComponent();

      gaugeWhTraffic.LabelFormatter = Formatter;
    } 
    #endregion

  }
}
