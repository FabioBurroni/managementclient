﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Utilities.Extensions;
using Model;
using Model.Custom.C200153.WhStatus;
using Model.Custom.C200153;
using System.Timers;
using Configuration;

namespace View.Custom.C200153.WhStatus
{
  /// <summary>
  /// Interaction logic for   .xaml
  /// </summary>
  public partial class CtrlWarehouseStatusAWH1 : CtrlBaseC200153
  {

    public Timer RefreshTimer = new Timer();

    private bool autorefresh;
    private double intervalTime = 60000;

    private double elapsedTime;
    public double ElapsedTime
    {
      get { return elapsedTime; }
      set
      {
        elapsedTime = value;
        NotifyPropertyChanged("ElapsedTime");
        NotifyPropertyChanged("RefreshTimeInSeconds");
      }
    }

    public double RefreshTimeInSeconds => ElapsedTime / 1000;

    List<C200153_WhStatus> _whStatusL = new List<C200153_WhStatus>();
    List<C200153_MasterStatus> _masterStatusL = new List<C200153_MasterStatus>();
    List<C200153_SlaveStatus> _slaveStatusL = new List<C200153_SlaveStatus>();
    List<C200153_WhTraffic> _whTrafficL = new List<C200153_WhTraffic>();
    List<C200153_WhTrafficExit> _TrafficExitL = new List<C200153_WhTrafficExit>();


    #region Costruttore
    public CtrlWarehouseStatusAWH1()
    {
      InitializeComponent();
      Init();
    }
    #endregion
 
    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkWarehouse1.Text = Context.Instance.TranslateDefault((string)txtBlkWarehouse1.Tag);
      txtBlkWarehouse2.Text = Context.Instance.TranslateDefault((string)txtBlkWarehouse2.Tag);
      txtBlkWarehouse3.Text = Context.Instance.TranslateDefault((string)txtBlkWarehouse3.Tag);

      ctrlA1Wh11T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh11T.Tag) + " 1";
      ctrlA1Wh12T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh12T.Tag) + " 2";
      ctrlA1Wh13T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh13T.Tag) + " 3";
      ctrlA1Wh14T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh14T.Tag) + " 4";
      ctrlA1Wh15T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh15T.Tag) + " 5";
      ctrlA1Wh16T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh16T.Tag) + " 6";
      ctrlA1Wh17T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh17T.Tag) + " 7";

    }

    #endregion TRADUZIONI

    #region Metodi Privati
    private void Refresh()
    {
      Cmd_RM_Wh_Status();
      Cmd_RM_Wh_MasterStatus();
      Cmd_RM_Wh_SatelliteStatus();

      Cmd_RM_Wh_Traffic();
      Cmd_RM_Exit_Traffic();
    }
    private void Init()
    {

      ctrlA1Wh11.Title = "1";
      ctrlA1Wh12.Title = "2";
      ctrlA1Wh13.Title = "3";
      ctrlA1Wh14.Title = "4";
      ctrlA1Wh15.Title = "5";
      ctrlA1Wh16.Title = "6";
      ctrlA1Wh17.Title = "7";


      var whStatus1_11 = new C200153_WhStatus() { Code = "awh1_1" };
      var whStatus1_12 = new C200153_WhStatus() { Code = "awh1_2" };
      var whStatus1_13 = new C200153_WhStatus() { Code = "awh1_3" };
      var whStatus1_14 = new C200153_WhStatus() { Code = "awh1_4" };
      var whStatus1_15 = new C200153_WhStatus() { Code = "awh1_5" };
      var whStatus1_16 = new C200153_WhStatus() { Code = "awh1_6" };
      var whStatus1_17 = new C200153_WhStatus() { Code = "awh1_7" };

      ctrlA1Wh11.WhStatus = whStatus1_11;
      ctrlA1Wh12.WhStatus = whStatus1_12;
      ctrlA1Wh13.WhStatus = whStatus1_13;
      ctrlA1Wh14.WhStatus = whStatus1_14;
      ctrlA1Wh15.WhStatus = whStatus1_15;
      ctrlA1Wh16.WhStatus = whStatus1_16;
      ctrlA1Wh17.WhStatus = whStatus1_17;



      _whStatusL.Add(whStatus1_11);
      _whStatusL.Add(whStatus1_12);
      _whStatusL.Add(whStatus1_13);
      _whStatusL.Add(whStatus1_14);
      _whStatusL.Add(whStatus1_15);
      _whStatusL.Add(whStatus1_16);
      _whStatusL.Add(whStatus1_17);


      C200153_MasterStatus a1m11 = new C200153_MasterStatus() { Code = "m11" };
      C200153_MasterStatus a1m12 = new C200153_MasterStatus() { Code = "m12" };
      C200153_MasterStatus a1m13 = new C200153_MasterStatus() { Code = "m13" };
      C200153_MasterStatus a1m14 = new C200153_MasterStatus() { Code = "m14" };
      C200153_MasterStatus a1m15 = new C200153_MasterStatus() { Code = "m15" };
      C200153_MasterStatus a1m16 = new C200153_MasterStatus() { Code = "m16" };
      C200153_MasterStatus a1m17 = new C200153_MasterStatus() { Code = "m17" };


      ctrlA1Wh11.MasterStatus = a1m11;
      ctrlA1Wh12.MasterStatus = a1m12;
      ctrlA1Wh13.MasterStatus = a1m13;
      ctrlA1Wh14.MasterStatus = a1m14;
      ctrlA1Wh15.MasterStatus = a1m15;
      ctrlA1Wh16.MasterStatus = a1m16;
      ctrlA1Wh17.MasterStatus = a1m17;

      _masterStatusL.Add(a1m11);
      _masterStatusL.Add(a1m12);
      _masterStatusL.Add(a1m13);
      _masterStatusL.Add(a1m14);
      _masterStatusL.Add(a1m15);
      _masterStatusL.Add(a1m16);
      _masterStatusL.Add(a1m17);

      var a1s11 = new C200153_SlaveStatus() { Code = "s11" };
      var a1s12 = new C200153_SlaveStatus() { Code = "s12" };
      var a1s13 = new C200153_SlaveStatus() { Code = "s13" };
      var a1s14 = new C200153_SlaveStatus() { Code = "s14" };
      var a1s15 = new C200153_SlaveStatus() { Code = "s15" };
      var a1s16 = new C200153_SlaveStatus() { Code = "s16" };
      var a1s17 = new C200153_SlaveStatus() { Code = "s17" };

      ctrlA1Wh11.SlaveStatus = a1s11;
      ctrlA1Wh12.SlaveStatus = a1s12;
      ctrlA1Wh13.SlaveStatus = a1s13;
      ctrlA1Wh14.SlaveStatus = a1s14;
      ctrlA1Wh15.SlaveStatus = a1s15;
      ctrlA1Wh16.SlaveStatus = a1s16;
      ctrlA1Wh17.SlaveStatus = a1s17;

      _slaveStatusL.Add(a1s11);
      _slaveStatusL.Add(a1s12);
      _slaveStatusL.Add(a1s13);
      _slaveStatusL.Add(a1s14);
      _slaveStatusL.Add(a1s15);
      _slaveStatusL.Add(a1s16);
      _slaveStatusL.Add(a1s17);

      // TRAFFIC 

      ctrlA1Wh11T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh11T.Tag) + " 1";
      ctrlA1Wh12T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh12T.Tag) + " 2";
      ctrlA1Wh13T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh13T.Tag) + " 3";
      ctrlA1Wh14T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh14T.Tag) + " 4";
      ctrlA1Wh15T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh15T.Tag) + " 5";
      ctrlA1Wh16T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh16T.Tag) + " 6";
      ctrlA1Wh17T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh17T.Tag) + " 7";

      ctrlP12.Title = "P12";
      ctrlP77.Title = "P77";
      ctrlP99.Title = "P99";

      var a1t11 = new C200153_WhTraffic() { Area = "awh1" , Floor = "1"};
      var a1t12 = new C200153_WhTraffic() { Area = "awh1" , Floor = "2" };
      var a1t13 = new C200153_WhTraffic() { Area = "awh1" , Floor = "3" };
      var a1t14 = new C200153_WhTraffic() { Area = "awh1" , Floor = "4" };
      var a1t15 = new C200153_WhTraffic() { Area = "awh1" , Floor = "5" };
      var a1t16 = new C200153_WhTraffic() { Area = "awh1" , Floor = "6" };
      var a1t17 = new C200153_WhTraffic() { Area = "awh1" , Floor = "7" };


      var P12 = new C200153_WhTrafficExit() { Code = "p12_out" };
      var P77 = new C200153_WhTrafficExit() { Code = "p77_out" };
      var P99 = new C200153_WhTrafficExit() { Code = "p99_out" };

      ctrlA1Wh11T.WhTraffic = a1t11;
      ctrlA1Wh12T.WhTraffic = a1t12;
      ctrlA1Wh13T.WhTraffic = a1t13;
      ctrlA1Wh14T.WhTraffic = a1t14;
      ctrlA1Wh15T.WhTraffic = a1t15;
      ctrlA1Wh16T.WhTraffic = a1t16;
      ctrlA1Wh17T.WhTraffic = a1t17;

      ctrlP12.WhTraffic = P12;
      ctrlP77.WhTraffic = P77;
      ctrlP99.WhTraffic = P99;

      _whTrafficL.Add(a1t11);
      _whTrafficL.Add(a1t12);
      _whTrafficL.Add(a1t13);
      _whTrafficL.Add(a1t14);
      _whTrafficL.Add(a1t15);
      _whTrafficL.Add(a1t16);
      _whTrafficL.Add(a1t17);

      _TrafficExitL.Add(P12);
      _TrafficExitL.Add(P77);
      _TrafficExitL.Add(P99);

    }

    #endregion


    #region Comandi

    private void Cmd_RM_Wh_Status()
    {
      CommandManagerC200153.RM_Wh_Status(this);
    }

    private void RM_Wh_Status(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var whStatusL = dwr.Data as List<C200153_WhStatus>;
      if (whStatusL != null)
      {

        foreach (var whs in whStatusL)
        {
          var currentWhS = _whStatusL.FirstOrDefault(w => w.Code.EqualsIgnoreCase(whs.Code));
          if (currentWhS != null) currentWhS.Update(whs);
        }
      }
    }

    private void Cmd_RM_Wh_MasterStatus()
    {
      CommandManagerC200153.RM_Wh_MasterStatus(this);
    }

    private void RM_Wh_MasterStatus(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var masterL = dwr.Data as List<C200153_MasterStatus>;
      if (masterL != null)
      {

        foreach (var master in masterL)
        {
          var currentMaster = _masterStatusL.FirstOrDefault(m => m.Code.EqualsIgnoreCase(master.Code));
          if (currentMaster != null) currentMaster.Update(master);
        }
      }
    }

    private void Cmd_RM_Wh_SatelliteStatus()
    {
      CommandManagerC200153.RM_Wh_SatelliteStatus(this);
    }

    private void RM_Wh_SatelliteStatus(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var slaveL = dwr.Data as List<C200153_SlaveStatus>;
      if (slaveL != null)
      {
        foreach (var slave in slaveL)
        {
          var currentSlave= _slaveStatusL.FirstOrDefault(s => s.Code.EqualsIgnoreCase(slave.Code));
          if (currentSlave != null) currentSlave.Update(slave);
        }
      }
    }

    private void Cmd_RM_Wh_Traffic()
    {
      CommandManagerC200153.RM_Wh_Traffic(this);
    }

    private void RM_Wh_Traffic(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var whTrafficL = dwr.Data as List<C200153_WhTraffic>;
      if (whTrafficL != null)
      {

        foreach (var wht in whTrafficL)
        {
          var currentWhT = _whTrafficL.FirstOrDefault(w => w.Area.EqualsIgnoreCase(wht.Area) && w.Floor.EqualsIgnoreCase(wht.Floor));

          if (currentWhT != null) currentWhT.Update(wht);
        }

        var sum = _whTrafficL.Select(x => x.PalletCount).Sum();

        foreach (var wht in _whTrafficL)
        {
          wht.PalletCountTotal = sum;
          wht.Update(wht);
        }
      }
    }

    private void Cmd_RM_Exit_Traffic()
    {
      CommandManagerC200153.RM_Exit_Traffic(this);
    }

    private void RM_Exit_Traffic(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var whTrafficL = dwr.Data as List<C200153_WhTrafficExit>;
      if (whTrafficL != null)
      {
        //...calculates total traffic
        int palletCountTotal = whTrafficL.Sum(p => p.PalletCount);
        whTrafficL.ForEach(p => p.PalletCountTotal = palletCountTotal);
        foreach (var wht in whTrafficL)
        {
          //var currentWhT = _TrafficExitL.FirstOrDefault(w => w.Code.EqualsIgnoreCase(wht.Code));
          //if (currentWhT != null) currentWhT.Update(wht);

          _TrafficExitL.FirstOrDefault(t => t.Code.EqualsIgnoreCase(wht.Code))?.Update(wht);
        }

        //var sum = _whTrafficL.Select(x => x.PalletCount).Sum();

        //foreach (var wht in _whTrafficL)
        //{
        //  wht.PalletCountTotal = sum;
        //  wht.Update(wht);
        //}
      }
    }

    #endregion



    #region Control Event

    private void RefreshTimer_Elapsed(object sender, ElapsedEventArgs e)
    {
      if (RefreshTimer != null)
      {
        ElapsedTime -= RefreshTimer.Interval;
        if (ElapsedTime <= 0)
        {
          ElapsedTime = intervalTime;
          Refresh();
        }
      }
    }

    private void CtrlBaseC200153_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
      Refresh();

      Translate();

      //get parameters...
      var statusPanelSettings = ConfigurationManager.Parameters.Layout.Where(x => x.Title == "Status Panel").FirstOrDefault();
      bool enableAutoRefresh = statusPanelSettings.Settings.Where(x => x.Name == "WarehouseStatusAutoRefresh").FirstOrDefault().Value.ConvertToBool();
      int durationAutoRefresh = statusPanelSettings.Settings.Where(x => x.Name == "WarehouseStatusAutoRefreshDuration").FirstOrDefault().Value.ConvertToInt();

      if (durationAutoRefresh >= 5)
        intervalTime = durationAutoRefresh * 1000;

      autorefresh = enableAutoRefresh;

      RefreshTimer = new Timer();
      RefreshTimer.Interval = 1000;
      RefreshTimer.AutoReset = true;
      RefreshTimer.Elapsed += RefreshTimer_Elapsed;
      ElapsedTime = intervalTime;

      if (autorefresh)
      {
        RefreshTimer.Start();

        iconRefresh.Visibility = System.Windows.Visibility.Hidden;
        busyOverlayViewBox.Visibility = System.Windows.Visibility.Visible;
      }
      else
      {
        iconRefresh.Visibility = System.Windows.Visibility.Visible;
        busyOverlayViewBox.Visibility = System.Windows.Visibility.Hidden;
      }

    }

    private void CtrlBaseC200153_IsVisibleChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
    {
      if (!IsVisible)
      {
        Closing();
      }
    }

    public override void Closing()
    {
      ElapsedTime = intervalTime;
      if (RefreshTimer != null)
      {
        RefreshTimer.Stop();
        RefreshTimer.Elapsed -= RefreshTimer_Elapsed;
        RefreshTimer = null;
      }
      base.Closing();
    }

    #endregion

    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      ElapsedTime = intervalTime;
      Refresh();
    }

    private void butRefresh_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
    {
      iconRefresh.Visibility = System.Windows.Visibility.Visible;
      busyOverlayViewBox.Visibility = System.Windows.Visibility.Hidden;
    }

    private void butRefresh_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
    {
      if (!autorefresh)
        return;

      iconRefresh.Visibility = System.Windows.Visibility.Hidden;
      busyOverlayViewBox.Visibility = System.Windows.Visibility.Visible;
    }
  }
}
