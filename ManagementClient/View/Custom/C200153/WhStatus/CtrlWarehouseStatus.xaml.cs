﻿using System.Windows;
using Model;

namespace View.Custom.C200153.WhStatus
{
  /// <summary>
  /// Interaction logic for CtrlWarehouseStatus.xaml
  /// </summary>
  public partial class CtrlWarehouseStatus : CtrlBaseC200153
  {

    #region Costruttore
    public CtrlWarehouseStatus()
    {
      InitializeComponent();
    }
    #endregion
 
    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkWarehouse.Text = Context.Instance.TranslateDefault((string)txtBlkWarehouse.Tag);

    }

    #endregion TRADUZIONI

 
    #region Control Event


    private void CtrlBaseC200153_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
      Translate();

    }


    #endregion

    private void btnMoveToSecond_Click(object sender, RoutedEventArgs e)
    {
      whStatus1.Visibility = Visibility.Hidden;
      whStatus2.Visibility = Visibility.Visible;

      btnMoveToFirst.Visibility = Visibility.Visible;
      btnMoveToSecond.Visibility = Visibility.Hidden;

      txtBlkWarehouseCode.Text = " 2 ";
    }

    private void btnMoveToFirst_Click(object sender, RoutedEventArgs e)
    {
      whStatus1.Visibility = Visibility.Visible;
      whStatus2.Visibility = Visibility.Hidden;

      btnMoveToFirst.Visibility = Visibility.Hidden;
      btnMoveToSecond.Visibility = Visibility.Visible;

      txtBlkWarehouseCode.Text = " 1 ";
    }
  }
}
