﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Utilities.Extensions;
using Model;
using Model.Custom.C200153.WhStatus;
using Model.Custom.C200153;
using System.Timers;
using Configuration;

namespace View.Custom.C200153.WhStatus
{
  /// <summary>
  /// Interaction logic for   .xaml
  /// </summary>
  public partial class CtrlWarehouseStatusAWH2 : CtrlBaseC200153
  {

    public Timer RefreshTimer = new Timer();

    private bool autorefresh;
    private double intervalTime = 60000;

    private double elapsedTime;
    public double ElapsedTime
    {
      get { return elapsedTime; }
      set
      {
        elapsedTime = value;
        NotifyPropertyChanged("ElapsedTime");
        NotifyPropertyChanged("RefreshTimeInSeconds");
      }
    }

    public double RefreshTimeInSeconds => ElapsedTime / 1000;


    List<C200153_WhStatus> _whStatusL = new List<C200153_WhStatus>();
    List<C200153_MasterStatus> _masterStatusL = new List<C200153_MasterStatus>();
    List<C200153_SlaveStatus> _slaveStatusL = new List<C200153_SlaveStatus>();
    List<C200153_WhTraffic> _whTrafficL = new List<C200153_WhTraffic>();
    List<C200153_WhTrafficExit> _TrafficExitL = new List<C200153_WhTrafficExit>();


    #region Costruttore
    public CtrlWarehouseStatusAWH2()
    {
      InitializeComponent();
      Init();
    }
    #endregion
 
    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkWarehouse1.Text = Context.Instance.TranslateDefault((string)txtBlkWarehouse1.Tag);
      txtBlkWarehouse2.Text = Context.Instance.TranslateDefault((string)txtBlkWarehouse2.Tag);
      txtBlkWarehouse3.Text = Context.Instance.TranslateDefault((string)txtBlkWarehouse3.Tag);

      ctrlA2Wh21T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh21T.Tag) + " 1";
      ctrlA2Wh22T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh22T.Tag) + " 2";
      ctrlA2Wh23T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh23T.Tag) + " 3";
      ctrlA2Wh24T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh24T.Tag) + " 4";
      ctrlA2Wh25T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh25T.Tag) + " 5";
      ctrlA2Wh26T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh26T.Tag) + " 6";
      ctrlA2Wh27T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh27T.Tag) + " 7";

    }

    #endregion TRADUZIONI

    #region Metodi Privati
    private void Refresh()
    {
      Cmd_RM_Wh_Status();
      Cmd_RM_Wh_MasterStatus();
      Cmd_RM_Wh_SatelliteStatus();

      Cmd_RM_Wh_Traffic();
      Cmd_RM_Exit_Traffic();
    }
    private void Init()
    {

      ctrlA2Wh21.Title = "21";
      ctrlA2Wh22.Title = "22";
      ctrlA2Wh23.Title = "23";
      ctrlA2Wh24.Title = "24";
      ctrlA2Wh25.Title = "25";
      ctrlA2Wh26.Title = "26";
      ctrlA2Wh27.Title = "27";


      var whStatus2_21 = new C200153_WhStatus() { Code = "awh2_1" };
      var whStatus2_22 = new C200153_WhStatus() { Code = "awh2_2" };
      var whStatus2_23 = new C200153_WhStatus() { Code = "awh2_3" };
      var whStatus2_24 = new C200153_WhStatus() { Code = "awh2_4" };
      var whStatus2_25 = new C200153_WhStatus() { Code = "awh2_5" };
      var whStatus2_26 = new C200153_WhStatus() { Code = "awh2_6" };
      var whStatus2_27 = new C200153_WhStatus() { Code = "awh2_7" };

      ctrlA2Wh21.WhStatus = whStatus2_21;
      ctrlA2Wh22.WhStatus = whStatus2_22;
      ctrlA2Wh23.WhStatus = whStatus2_23;
      ctrlA2Wh24.WhStatus = whStatus2_24;
      ctrlA2Wh25.WhStatus = whStatus2_25;
      ctrlA2Wh26.WhStatus = whStatus2_26;
      ctrlA2Wh27.WhStatus = whStatus2_27;



      _whStatusL.Add(whStatus2_21);
      _whStatusL.Add(whStatus2_22);
      _whStatusL.Add(whStatus2_23);
      _whStatusL.Add(whStatus2_24);
      _whStatusL.Add(whStatus2_25);
      _whStatusL.Add(whStatus2_26);
      _whStatusL.Add(whStatus2_27);


      C200153_MasterStatus a2m21 = new C200153_MasterStatus() { Code = "m21" };
      C200153_MasterStatus a2m22 = new C200153_MasterStatus() { Code = "m22" };
      C200153_MasterStatus a2m23 = new C200153_MasterStatus() { Code = "m23" };
      C200153_MasterStatus a2m24 = new C200153_MasterStatus() { Code = "m24" };
      C200153_MasterStatus a2m25 = new C200153_MasterStatus() { Code = "m25" };
      C200153_MasterStatus a2m26 = new C200153_MasterStatus() { Code = "m26" };
      C200153_MasterStatus a2m27 = new C200153_MasterStatus() { Code = "m27" };


      ctrlA2Wh21.MasterStatus = a2m21;
      ctrlA2Wh22.MasterStatus = a2m22;
      ctrlA2Wh23.MasterStatus = a2m23;
      ctrlA2Wh24.MasterStatus = a2m24;
      ctrlA2Wh25.MasterStatus = a2m25;
      ctrlA2Wh26.MasterStatus = a2m26;
      ctrlA2Wh27.MasterStatus = a2m27;

      _masterStatusL.Add(a2m21);
      _masterStatusL.Add(a2m22);
      _masterStatusL.Add(a2m23);
      _masterStatusL.Add(a2m24);
      _masterStatusL.Add(a2m25);
      _masterStatusL.Add(a2m26);
      _masterStatusL.Add(a2m27);

      var a2s21 = new C200153_SlaveStatus() { Code = "s21" };
      var a2s22 = new C200153_SlaveStatus() { Code = "s22" };
      var a2s23 = new C200153_SlaveStatus() { Code = "s23" };
      var a2s24 = new C200153_SlaveStatus() { Code = "s24" };
      var a2s25 = new C200153_SlaveStatus() { Code = "s25" };
      var a2s26 = new C200153_SlaveStatus() { Code = "s26" };
      var a2s27 = new C200153_SlaveStatus() { Code = "s27" };

      ctrlA2Wh21.SlaveStatus = a2s21;
      ctrlA2Wh22.SlaveStatus = a2s22;
      ctrlA2Wh23.SlaveStatus = a2s23;
      ctrlA2Wh24.SlaveStatus = a2s24;
      ctrlA2Wh25.SlaveStatus = a2s25;
      ctrlA2Wh26.SlaveStatus = a2s26;
      ctrlA2Wh27.SlaveStatus = a2s27;

      _slaveStatusL.Add(a2s21);
      _slaveStatusL.Add(a2s22);
      _slaveStatusL.Add(a2s23);
      _slaveStatusL.Add(a2s24);
      _slaveStatusL.Add(a2s25);
      _slaveStatusL.Add(a2s26);
      _slaveStatusL.Add(a2s27);

      // TRAFFIC 

      ctrlA2Wh21T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh21T.Tag) + " 21";
      ctrlA2Wh22T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh22T.Tag) + " 22";
      ctrlA2Wh23T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh23T.Tag) + " 23";
      ctrlA2Wh24T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh24T.Tag) + " 24";
      ctrlA2Wh25T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh25T.Tag) + " 25";
      ctrlA2Wh26T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh26T.Tag) + " 26";
      ctrlA2Wh27T.Title = Context.Instance.TranslateDefault((string)ctrlA2Wh27T.Tag) + " 27";

      ctrlP2012.Title = "P2012";
      ctrlP2077.Title = "P2077";
      ctrlP2099.Title = "P2099";

      var a2t21 = new C200153_WhTraffic() { Area = "awh2" , Floor = "1"};
      var a2t22 = new C200153_WhTraffic() { Area = "awh2" , Floor = "2" };
      var a2t23 = new C200153_WhTraffic() { Area = "awh2" , Floor = "3" };
      var a2t24 = new C200153_WhTraffic() { Area = "awh2" , Floor = "4" };
      var a2t25 = new C200153_WhTraffic() { Area = "awh2" , Floor = "5" };
      var a2t26 = new C200153_WhTraffic() { Area = "awh2" , Floor = "6" };
      var a2t27 = new C200153_WhTraffic() { Area = "awh2" , Floor = "7" };


      var P2012 = new C200153_WhTrafficExit() { Code = "p2012_out" };
      var P2077 = new C200153_WhTrafficExit() { Code = "p2077_out" };
      var P2099 = new C200153_WhTrafficExit() { Code = "p2099_out" };

      ctrlA2Wh21T.WhTraffic = a2t21;
      ctrlA2Wh22T.WhTraffic = a2t22;
      ctrlA2Wh23T.WhTraffic = a2t23;
      ctrlA2Wh24T.WhTraffic = a2t24;
      ctrlA2Wh25T.WhTraffic = a2t25;
      ctrlA2Wh26T.WhTraffic = a2t26;
      ctrlA2Wh27T.WhTraffic = a2t27;

      ctrlP2012.WhTraffic = P2012;
      ctrlP2077.WhTraffic = P2077;
      ctrlP2099.WhTraffic = P2099;

      _whTrafficL.Add(a2t21);
      _whTrafficL.Add(a2t22);
      _whTrafficL.Add(a2t23);
      _whTrafficL.Add(a2t24);
      _whTrafficL.Add(a2t25);
      _whTrafficL.Add(a2t26);
      _whTrafficL.Add(a2t27);

      _TrafficExitL.Add(P2012);
      _TrafficExitL.Add(P2077);
      _TrafficExitL.Add(P2099);

    }

    #endregion


    #region Comandi

    private void Cmd_RM_Wh_Status()
    {
      CommandManagerC200153.RM_Wh_Status(this);
    }

    private void RM_Wh_Status(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var whStatusL = dwr.Data as List<C200153_WhStatus>;
      if (whStatusL != null)
      {

        foreach (var whs in whStatusL)
        {
          var currentWhS = _whStatusL.FirstOrDefault(w => w.Code.EqualsIgnoreCase(whs.Code));
          if (currentWhS != null) currentWhS.Update(whs);
        }
      }
    }

    private void Cmd_RM_Wh_MasterStatus()
    {
      CommandManagerC200153.RM_Wh_MasterStatus(this);
    }

    private void RM_Wh_MasterStatus(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var masterL = dwr.Data as List<C200153_MasterStatus>;
      if (masterL != null)
      {

        foreach (var master in masterL)
        {
          var currentMaster = _masterStatusL.FirstOrDefault(m => m.Code.EqualsIgnoreCase(master.Code));
          if (currentMaster != null) currentMaster.Update(master);
        }
      }
    }

    private void Cmd_RM_Wh_SatelliteStatus()
    {
      CommandManagerC200153.RM_Wh_SatelliteStatus(this);
    }

    private void RM_Wh_SatelliteStatus(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var slaveL = dwr.Data as List<C200153_SlaveStatus>;
      if (slaveL != null)
      {
        foreach (var slave in slaveL)
        {
          var currentSlave= _slaveStatusL.FirstOrDefault(s => s.Code.EqualsIgnoreCase(slave.Code));
          if (currentSlave != null) currentSlave.Update(slave);
        }
      }
    }

    private void Cmd_RM_Wh_Traffic()
    {
      CommandManagerC200153.RM_Wh_Traffic(this);
    }

    private void RM_Wh_Traffic(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var whTrafficL = dwr.Data as List<C200153_WhTraffic>;
      if (whTrafficL != null)
      {

        foreach (var wht in whTrafficL)
        {
          var currentWhT = _whTrafficL.FirstOrDefault(w => w.Area.EqualsIgnoreCase(wht.Area) && w.Floor.EqualsIgnoreCase(wht.Floor));

          if (currentWhT != null) currentWhT.Update(wht);
        }

        var sum = _whTrafficL.Select(x => x.PalletCount).Sum();

        foreach (var wht in _whTrafficL)
        {
          wht.PalletCountTotal = sum;
          wht.Update(wht);
        }
      }
    }

    private void Cmd_RM_Exit_Traffic()
    {
      CommandManagerC200153.RM_Exit_Traffic(this);
    }

    private void RM_Exit_Traffic(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var whTrafficL = dwr.Data as List<C200153_WhTrafficExit>;
      if (whTrafficL != null)
      {
        //...calculates total traffic
        int palletCountTotal = whTrafficL.Sum(p => p.PalletCount);
        whTrafficL.ForEach(p => p.PalletCountTotal = palletCountTotal);
        foreach (var wht in whTrafficL)
        {
          //var currentWhT = _TrafficExitL.FirstOrDefault(w => w.Code.EqualsIgnoreCase(wht.Code));
          //if (currentWhT != null) currentWhT.Update(wht);

          _TrafficExitL.FirstOrDefault(t => t.Code.EqualsIgnoreCase(wht.Code))?.Update(wht);
        }

        //var sum = _whTrafficL.Select(x => x.PalletCount).Sum();

        //foreach (var wht in _whTrafficL)
        //{
        //  wht.PalletCountTotal = sum;
        //  wht.Update(wht);
        //}
      }
    }

    #endregion



    #region Control Event

    private void RefreshTimer_Elapsed(object sender, ElapsedEventArgs e)
    {
      ElapsedTime -= RefreshTimer.Interval;
      if (ElapsedTime <= 0)
      {
        ElapsedTime = intervalTime;
        Refresh();
      }
    }

    private void CtrlBaseC200153_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
      Refresh();

      Translate();

      //get parameters...
      var statusPanelSettings = ConfigurationManager.Parameters.Layout.Where(x => x.Title == "Status Panel").FirstOrDefault();
      bool enableAutoRefresh = statusPanelSettings.Settings.Where(x => x.Name == "WarehouseStatusAutoRefresh").FirstOrDefault().Value.ConvertToBool();
      int durationAutoRefresh = statusPanelSettings.Settings.Where(x => x.Name == "WarehouseStatusAutoRefreshDuration").FirstOrDefault().Value.ConvertToInt();

      if (durationAutoRefresh >= 5)
        intervalTime = durationAutoRefresh * 1000;

      autorefresh = enableAutoRefresh;

      RefreshTimer = new Timer();
      RefreshTimer.Interval = 1000;
      RefreshTimer.AutoReset = true;
      RefreshTimer.Elapsed += RefreshTimer_Elapsed;
      ElapsedTime = intervalTime;

      if (autorefresh)
      {
        RefreshTimer.Start();

        iconRefresh.Visibility = System.Windows.Visibility.Hidden;
        busyOverlayViewBox.Visibility = System.Windows.Visibility.Visible;
      }
      else
      {
        iconRefresh.Visibility = System.Windows.Visibility.Visible;
        busyOverlayViewBox.Visibility = System.Windows.Visibility.Hidden;
      }



    }

    private void CtrlBaseC200153_IsVisibleChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
    {
      if (!IsVisible)
      {
        Closing();
      }
    }

    public override void Closing()
    {
      ElapsedTime = intervalTime;
      if (RefreshTimer != null)
      {
        RefreshTimer.Stop();
        RefreshTimer.Elapsed -= RefreshTimer_Elapsed;
        RefreshTimer = null;
      }
      base.Closing();
    }

    #endregion

    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      ElapsedTime = intervalTime;
      Refresh();
    }

    private void butRefresh_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
    {
      iconRefresh.Visibility = System.Windows.Visibility.Visible;
      busyOverlayViewBox.Visibility = System.Windows.Visibility.Hidden;
    }

    private void butRefresh_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
    {
      if (!autorefresh)
        return;

      iconRefresh.Visibility = System.Windows.Visibility.Hidden;
      busyOverlayViewBox.Visibility = System.Windows.Visibility.Visible;
    }


  }
}
