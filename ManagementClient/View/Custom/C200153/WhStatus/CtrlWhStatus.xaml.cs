﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model;
using Model.Custom.C200153.WhStatus;

namespace View.Custom.C200153.WhStatus
{
  /// <summary>
  /// Interaction logic for CtrlWhStatus.xaml
  /// </summary>
  public partial class CtrlWhStatus : CtrlBaseC200153
  {
    public string Title
    {
      get { return (string)GetValue(TitleProperty); }
      set { SetValue(TitleProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty TitleProperty =
        DependencyProperty.Register("Title", typeof(string), typeof(CtrlWhStatus), new PropertyMetadata(""));

    public Func<double, string> Formatter = value => (value + "%").ToString();

    #region DP - WhStatus
    public C200153_WhStatus WhStatus
    {
      get { return (C200153_WhStatus)GetValue(WhStatusProperty); }
      set { SetValue(WhStatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for WhStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty WhStatusProperty =
        DependencyProperty.Register("WhStatus", typeof(C200153_WhStatus), typeof(CtrlWhStatus), new PropertyMetadata(null));

    #endregion


    #region DP - MasterStatus
    public C200153_MasterStatus MasterStatus
    {
      get { return (C200153_MasterStatus)GetValue(MasterStatusProperty); }
      set { SetValue(MasterStatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for MasterStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty MasterStatusProperty =
        DependencyProperty.Register("MasterStatus", typeof(C200153_MasterStatus), typeof(CtrlWhStatus), new PropertyMetadata(null));
    #endregion



    #region DP - SatelliteStatus
    public C200153_SlaveStatus SlaveStatus
    {
      get { return (C200153_SlaveStatus)GetValue(SlaveStatusProperty); }
      set { SetValue(SlaveStatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for SatelliteStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty SlaveStatusProperty =
        DependencyProperty.Register("SlaveStatus", typeof(C200153_SlaveStatus), typeof(CtrlWhStatus), new PropertyMetadata(null));
    #endregion

    #region COSTRUTTORE
    public CtrlWhStatus()
    {
      InitializeComponent();

      gaugeBlkBlocked.LabelFormatter = Formatter;
      gaugeBlkCompleted.LabelFormatter = Formatter;
      gaugeBlkEmpty.LabelFormatter = Formatter;
      gaugeBlkFilling.LabelFormatter = Formatter;
      gaugeBlkNotCompleted.LabelFormatter = Formatter;
    }
    #endregion
    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkBlocked.Text = Context.Instance.TranslateDefault((string)txtBlkBlocked.Tag);
      txtBlkCompleted.Text = Context.Instance.TranslateDefault((string)txtBlkCompleted.Tag);
      txtBlkEmpty.Text = Context.Instance.TranslateDefault((string)txtBlkEmpty.Tag);
      txtBlkMaster.Text = Context.Instance.TranslateDefault((string)txtBlkMaster.Tag);
      txtBlkNotCompleted.Text = Context.Instance.TranslateDefault((string)txtBlkNotCompleted.Tag);
      txtBlkSlave.Text = Context.Instance.TranslateDefault((string)txtBlkSlave.Tag);
      txtBlkFilling.Text = Context.Instance.TranslateDefault((string)txtBlkFilling.Tag);
    }
    #endregion

    private void CtrlBaseC200153_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
  }
}
