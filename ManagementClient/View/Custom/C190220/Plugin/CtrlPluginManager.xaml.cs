﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Model;
using Model.Custom.C190220;
using Model.Custom.C190220.Plugin;
using Utilities.Extensions;

namespace View.Custom.C190220.Plugin
{
  /// <summary>
  /// Interaction logic for CtrlPluginManager.xaml
  /// </summary>
  public partial class CtrlPluginManager : CtrlBaseC190220
  {
    #region DATI

    public ObservableCollection<PluginInfo> PluginCollection { get; } = new ObservableCollection<PluginInfo>();

    #endregion

    #region COSTRUTTORE
    public CtrlPluginManager()
    {
      InitializeComponent();

      // Check for design mode. 
      if (DesignTimeHelper.IsInDesignMode)
        return;
    }
    #endregion

    #region COMANDI

    private void Cmd_Plugin_GetAll(object receiverInstance)
    {
      CommandManagerC190220.Plugin_GetAll(receiverInstance);
    }

    private void Cmd_Plugin_Get(object receiverInstance, string pluginName)
    {
      CommandManagerC190220.Plugin_Get(receiverInstance, pluginName);
    }

    private void Cmd_Plugin_Check(object receiverInstance)
    {
      CommandManagerC190220.Plugin_Check(receiverInstance);
    }

    private void Cmd_Plugin_Add(object receiverInstance, string pluginName)
    {
      CommandManagerC190220.Plugin_Add(receiverInstance, pluginName);
    }

    private void Cmd_Plugin_Update(object receiverInstance, string pluginName)
    {
      CommandManagerC190220.Plugin_Update(receiverInstance, pluginName);
    }

    private void Cmd_Plugin_Start(object receiverInstance, string pluginName)
    {
      CommandManagerC190220.Plugin_Start(receiverInstance, pluginName);
    }

    private void Cmd_Plugin_Stop(object receiverInstance, string pluginName)
    {
      CommandManagerC190220.Plugin_Stop(receiverInstance, pluginName);
    }

    private void Cmd_Plugin_Enable(object receiverInstance, string pluginName)
    {
      CommandManagerC190220.Plugin_Enable(receiverInstance, pluginName);
    }

    private void Cmd_Plugin_Disable(object receiverInstance, string pluginName)
    {
      CommandManagerC190220.Plugin_Disable(receiverInstance, pluginName);
    }

    private void Cmd_Plugin_Unload(object receiverInstance, string pluginName)
    {
      CommandManagerC190220.Plugin_Unload(receiverInstance, pluginName);
    }

    private void Cmd_Plugin_XmlCommand_Get(object receiverInstance, string pluginName)
    {
      CommandManagerC190220.Plugin_XmlCommand_Get(receiverInstance, pluginName);
    }

    #endregion

    #region RISPOSTE

    private void Plugin_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null)
        return;

      var plugins = dwr.GetData<List<PluginInfo>>();
      foreach (var plugin in plugins)
      {
        var plg = PluginCollection.FirstOrDefault(pl => pl.Name.EqualsIgnoreCase(plugin.Name));
        if (plg != null)
          plg.Update(plugin);
        else
          PluginCollection.Add(plugin);
      }
    }

    private void Plugin_Get(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null)
        return;

      var plugin = dwr.GetData<PluginInfo>();
      var plg = PluginCollection.FirstOrDefault(pl => pl.Name.EqualsIgnoreCase(plugin.Name));
      if (plg != null)
        plg.Update(plugin);
      else
        PluginCollection.Add(plugin);
    }

    private void Plugin_Check(IList<string> commandMethodParameters, IModel model)
    {
    }

    private void Plugin_Add(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null)
        return;

      var result = dwr.Result.ConvertTo<bool>();
      if (!result)
      {
        MessageBox.Show("Error Adding Plugin");
      }
      else
      {
        MessageBox.Show("Plugin Added ok");
      }

      var plugin = dwr.GetData<PluginInfo>();
      var plg = PluginCollection.FirstOrDefault(pl => pl.Name.EqualsIgnoreCase(plugin.Name));
      if (plg != null)
        plg.Update(plugin);
      else
        PluginCollection.Add(plugin);
    }

    private void Plugin_Update(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null)
        return;

      bool result = dwr.Result.ConvertTo<bool>();
      if (!result)
      {
        MessageBox.Show("Error Updating Plugin");
      }
      else
      {
        MessageBox.Show("Plugin Updated ok");
      }

      var plugin = dwr.GetData<PluginInfo>();
      var plg = PluginCollection.FirstOrDefault(pl => pl.Name.EqualsIgnoreCase(plugin.Name));
      if (plg != null)
        plg.Update(plugin);
      else
        PluginCollection.Add(plugin);
    }

    private void Plugin_Start(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null)
        return;

      bool result = dwr.Result.ConvertTo<bool>();
      if (!result)
      {
        MessageBox.Show("Error Starting Plugin");
      }
      else
      {
        MessageBox.Show("Plugin Started ok");
      }

      var plugin = dwr.GetData<PluginInfo>();
      var plg = PluginCollection.FirstOrDefault(pl => pl.Name.EqualsIgnoreCase(plugin.Name));
      if (plg != null)
        plg.Update(plugin);
      else
        PluginCollection.Add(plugin);
    }

    private void Plugin_Stop(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null)
        return;

      bool result = dwr.Result.ConvertTo<bool>();
      if (!result)
      {
        MessageBox.Show("Error Stopping Plugin");
      }
      else
      {
        MessageBox.Show("Plugin Stopped ok");
      }

      var plugin = dwr.GetData<PluginInfo>();
      var plg = PluginCollection.FirstOrDefault(pl => pl.Name.EqualsIgnoreCase(plugin.Name));
      if (plg != null)
        plg.Update(plugin);
      else
        PluginCollection.Add(plugin);
    }

    private void Plugin_Unload(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null)
        return;

      bool result = dwr.Result.ConvertTo<bool>();
      if (!result)
      {
        MessageBox.Show("Error Unloading Plugin");
      }
      else
      {
        MessageBox.Show("Plugin Unloaded ok");
      }
      if (dwr.Data != null)
      {
        var plugin = dwr.GetData<PluginInfo>();
        var plg = PluginCollection.FirstOrDefault(pl => pl.Name.EqualsIgnoreCase(plugin.Name));
        if (plg != null)
          plg.Update(plugin);
        else
          PluginCollection.Add(plugin);
      }

    }

    private void Plugin_Enable(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null)
        return;

      bool result = dwr.Result.ConvertTo<bool>();
      if (!result)
      {
        MessageBox.Show("Error Enabling Plugin");
      }
      else
      {
        MessageBox.Show("Plugin Enabled ok");
      }

      var plugin = dwr.GetData<PluginInfo>();
      var plg = PluginCollection.FirstOrDefault(pl => pl.Name.EqualsIgnoreCase(plugin.Name));
      if (plg != null)
        plg.Update(plugin);
      else
        PluginCollection.Add(plugin);
    }

    private void Plugin_Disable(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null)
        return;

      bool result = dwr.Result.ConvertTo<bool>();
      if (!result)
      {
        MessageBox.Show("Error Disabling Plugin");
      }
      else
      {
        MessageBox.Show("Plugin Disabled ok");
      }

      var plugin = dwr.GetData<PluginInfo>();
      var plg = PluginCollection.FirstOrDefault(pl => pl.Name.EqualsIgnoreCase(plugin.Name));
      if (plg != null)
        plg.Update(plugin);
      else
        PluginCollection.Add(plugin);
    }

    private void Plugin_XmlCommand_Get(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null)
        return;

      string pluginName = commandMethodParameters[0];
      var pg = PluginCollection.FirstOrDefault(plg => plg.Name.EqualsIgnoreCase(pluginName));
      if (pg != null)
      {
        pg.XmlCommands.Clear();
        List<string> xmlCmds = dwr.GetData<List<string>>();
        xmlCmds.ForEach(x => pg.XmlCommands.Add(x));
      }
    }

    #endregion

    #region EVENTI INTERFACCIA

    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      Cmd_Plugin_GetAll(this);
    }

    private void butCheckPlugin_Click(object sender, RoutedEventArgs e)
    {
      Cmd_Plugin_Check(this);
    }

    private void butAddPlugin_Click(object sender, RoutedEventArgs e)
    {
      string pluginName = txtPluginName.Text;
      if (!string.IsNullOrEmpty(pluginName))
        Cmd_Plugin_Add(this, pluginName);
      else
        MessageBox.Show("Specify a valid plugin name");
    }

    private void CtrlPluginInfo_OnPluginAction(PluginInfo pluginInfo, PluginAction action)
    {
      switch (action)
      {
        case PluginAction.ADD:
          //string pluginName = txtPluginName.Text;
          //if (!string.IsNullOrEmpty(pluginName))
          //Cmd_Plugin_Add(this, pluginName);
          //else
          //MessageBox.Show("Specify a valid plugin name");
          break;

        case PluginAction.CHECK:
          //Cmd_Plugin_Check(this);
          break;
        case PluginAction.ENABLE:
          Cmd_Plugin_Enable(this, pluginInfo.Name);
          break;
        case PluginAction.DISABLE:
          Cmd_Plugin_Disable(this, pluginInfo.Name);
          break;
        case PluginAction.START:
          Cmd_Plugin_Start(this, pluginInfo.Name);
          break;
        case PluginAction.STOP:
          Cmd_Plugin_Stop(this, pluginInfo.Name);
          break;
        case PluginAction.UNLOAD:
          Cmd_Plugin_Unload(this, pluginInfo.Name);
          break;
        case PluginAction.UPDATE:
          Cmd_Plugin_Update(this, pluginInfo.Name);
          break;
        case PluginAction.XMLCOMMAND:
          Cmd_Plugin_XmlCommand_Get(this, pluginInfo.Name);
          break;
      }
    }

    //private void CtrlPluginInfo_OnPluginAction(PluginInfo pluginInfo, PluginAction action)
    //{

    //}

    #endregion

    
  }
}