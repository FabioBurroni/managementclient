﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using LINQtoCSV;
using System.Windows;
using System.Windows.Data;
using Model;
using Model.Custom.C190220;
using Model.Custom.C190220.ArticleManager;
using System.Windows.Forms;
using MaterialDesignExtensions.Controls;
using View.Common.Languages;

namespace View.Custom.C190220.SKUManager
{
  /// <summary>
  /// Interaction logic for CtrlSKUManager.xaml
  /// </summary>
  public partial class CtrlSKUImport : CtrlBaseC190220
  {
    #region PRIVATE FIELDS
    private int i = 0;

    
    #endregion

    #region Public Properties
    //public ObservableCollectionFast<C190220_ArticleImport> ArticleL { get; set; } = new ObservableCollectionFast<C190220_ArticleImport>();
    public ObservableCollectionFast<C190220_ArticleImport> ArticleL
    {
      get { return (ObservableCollectionFast<C190220_ArticleImport>)GetValue(ArticleLProperty); }
      set { SetValue(ArticleLProperty, value); }
    }

    // Using a DependencyProperty as the backing store for string.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ArticleLProperty =
        DependencyProperty.Register("ArticleL", typeof(ObservableCollectionFast<C190220_ArticleImport>), typeof(CtrlSKUImport)
          , new UIPropertyMetadata(null));


    #endregion

    #region FilePath

    public string FilePath
    {
      get { return (string)GetValue(FilePathProperty); }
      set { SetValue(FilePathProperty, value); }
    }

    // Using a DependencyProperty as the backing store for string.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty FilePathProperty =
        DependencyProperty.Register("FilePath", typeof(string), typeof(CtrlSKUImport)
          , new UIPropertyMetadata(null));

    #endregion

    #region COSTRUTTORE
    public CtrlSKUImport()
    {           
      InitializeComponent();
      FilePath = Localization.Localize.LocalizeDefaultString("OPEN CSV FILE");

      ArticleL = new ObservableCollectionFast<C190220_ArticleImport>();

    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colArtCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colUnit.Header = Localization.Localize.LocalizeDefaultString("UNIT");
      colResult.Header = Localization.Localize.LocalizeDefaultString("RESULT");
   
      txtBlkConfirm.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkConfirm.Tag);
      txtBlkClearSelection.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkClearSelection.Tag);
      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      txtBlkSelectAll.Text = Context.Instance.TranslateDefault((string)txtBlkSelectAll.Tag);
      txtBoxSelectFile.Text = Context.Instance.TranslateDefault((string)txtBoxSelectFile.Tag);

      btnConfirm.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnConfirm.Tag);
      btnCancel.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnCancel.Tag);
      btnClearSelection.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnClearSelection.Tag);
      btnOpenFile.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnOpenFile.Tag);
      btnSelectAll.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnSelectAll.Tag);
     
      CollectionViewSource.GetDefaultView(dgArticleL.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    
    private void Cmd_AM_Article_Import(C190220_ArticleImport art)
    {
      while (!ArticleL.ElementAt(i).Selected && (i < ArticleL.Count()-1))
           i++;

      if (ArticleL.ElementAt(i).Selected)
      {      
     
        C190220_Article a = new C190220_Article();
        a.Code = art.Code;
        
        a.Descr = art.Descr?? "ND";
        a.Unit = art.Unit;

        CommandManagerC190220.AM_Article_Import(this, a);
       }
      
    }
    private void AM_Article_Import(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C190220_CustomResult Result = (C190220_CustomResult)dwr.Data;

      ArticleL.ElementAt(i).Result = Result;
      ArticleL.ElementAt(i).Valid = (ArticleL.ElementAt(i).Result == C190220_CustomResult.OK);
      CollectionViewSource.GetDefaultView(dgArticleL.ItemsSource).Refresh();

      i++;
      if (i < ArticleL.Count())
      {
        Cmd_AM_Article_Import(ArticleL.ElementAt(i));
      }

    }

    #endregion
     

    #region Eventi controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();
    }
      
    #endregion

    #region Private methods 

    private async void ReadArticleFromCsv()
    {
      ArticleL.Clear();

      try
      {
        CsvFileDescription inputFileDescription = new CsvFileDescription
        {
          SeparatorChar = ';',
          FirstLineHasColumnNames = true,
          EnforceCsvColumnAttribute = true,
          IgnoreUnknownColumns = true,
          MaximumNbrExceptions = 50
        };
        CsvContext cc = new CsvContext();
        IEnumerable<C190220_ArticleImport> articles = cc.Read<C190220_ArticleImport>(FilePath, inputFileDescription);

        if (articles is null) return;

        foreach (C190220_ArticleImport art in articles)
        {

          if (art != null)
          {
            C190220_ArticleImport artImp = art;
          
            // valid - controlla che i campi siano validi
            artImp.Valid = true;
              
            ArticleL.Add(artImp);
          }
          

        }

      }
      catch (AggregatedException ae)
      {
        // Process all exceptions generated while processing the file
        List<Exception> innerExceptionsList =
            (List<Exception>)ae.Data["InnerExceptionsList"];
        foreach (Exception e in innerExceptionsList)
        {
          AlertDialogArguments alertDialogArgs = new AlertDialogArguments
          {
            Title = "ERROR READING FROM CSV".TD(),
            Message = e.Message.TD(),
            OkButtonLabel = "OK".TD()
          };

          await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);
          
        }
      }
      catch (DuplicateFieldIndexException dfie)
      {
        // name of the class used with the Read method - in this case "Product"
        string typeName = Convert.ToString(dfie.Data["TypeName"]);
        // Names of the two fields or properties that have the same FieldIndex
        string fieldName = Convert.ToString(dfie.Data["FieldName"]);
        string fieldName2 = Convert.ToString(dfie.Data["FieldName2"]);
        // Actual FieldIndex that the two fields have in common
        int commonFieldIndex = Convert.ToInt32(dfie.Data["Index"]);
        // Do some processing with this information
        // .........
        // Inform user of error situation
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "ERROR READING FROM CSV".TD(),
          Message = dfie.Message.TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

      }
      catch (Exception e)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "ERROR READING FROM CSV".TD(),
          Message = e.Message.TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);
      }

    }

    #endregion

    private async void btnConfirm_Click(object sender, RoutedEventArgs e)
    {
      if(ArticleL.Where(x=> x.Selected==true).Count()==0)
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "INSERT ARTICLE NOT POSSIBLE".TD(),
          Message = "PLEASE SELECT AT LEAST ONE ARTICLE FROM LIST".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);
                
          return;
      }

    
      ConfirmationDialogArguments dialogArgs = new ConfirmationDialogArguments
      {
        Title = "INSERT ARTICLE CONFIRM".TD(),
        Message = "Are you sure?".TD(),
        OkButtonLabel = "YES".TD(),
        CancelButtonLabel = "CANCEL".TD(),
        StackedButtons = false
      };

      bool result = await ConfirmationDialog.ShowDialogAsync("RootDialog", dialogArgs);

      if (!result)
        return;

      i = 0;

      if (ArticleL.Count>0)
        Cmd_AM_Article_Import(ArticleL.ElementAt(i));
       
            
    }

    private async void btnOpenFile_Click(object sender, RoutedEventArgs e)
    {
      using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
      {
       // openFileDialog.InitialDirectory = "c:\\";
        openFileDialog.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
        openFileDialog.FilterIndex = 2;
        openFileDialog.RestoreDirectory = true;

        if (openFileDialog.ShowDialog() == DialogResult.OK)
        {
          //Get the path of specified file
          FilePath = openFileDialog.FileName;
        }
      }

      if (File.Exists(FilePath))
      {

        ReadArticleFromCsv();
      }
      else
      {
        AlertDialogArguments alertDialogArgs = new AlertDialogArguments
        {
          Title = "FILE NOT SELECTED".TD(),
          Message = "PLEASE SELECT A VALID CSV FILE".TD(),
          OkButtonLabel = "OK".TD()
        };

        await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);
        
      }

    }

    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      FilePath = Localization.Localize.LocalizeDefaultString("OPEN CSV FILE");
      ArticleL.Clear();
    }

    private void btnSelectAll_Click(object sender, RoutedEventArgs e)
    {
      if (ArticleL != null)
      {
        foreach (var art in ArticleL)
        {
          art.Selected = true;
        }
        CollectionViewSource.GetDefaultView(dgArticleL.ItemsSource).Refresh();

      }
    }    

    private void btnClearSelection_Click(object sender, RoutedEventArgs e)
    {
      if (ArticleL != null)
      {
        foreach (var art in ArticleL)
        {
          art.Selected = false;
        }
        CollectionViewSource.GetDefaultView(dgArticleL.ItemsSource).Refresh();

      }
    }

  
  }
}
