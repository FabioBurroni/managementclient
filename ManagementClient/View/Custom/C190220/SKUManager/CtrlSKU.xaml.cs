﻿using System;
using System.Windows;
using Model.Custom.C190220;


namespace View.Custom.C190220.SKUManager
{
  /// <summary>
  /// Interaction logic for CtrlSKU.xaml
  /// </summary>
  public partial class CtrlSKU : CtrlBaseC190220
  {
    public C190220_UserLogged UserLogged { get; set; } = new C190220_UserLogged();

    #region Costruttore
    public CtrlSKU()
    {
      InitializeComponent();
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
      Authentication.AuthenticationManager.Instance.LoginSession += Instance_LoginSession;

    }
    #endregion

    #region TRADUZIONI
    protected override void Translate()
    {
      tiManage.Text = Localization.Localize.LocalizeDefaultString((string)tiManage.Tag);
      tiImport.Text = Localization.Localize.LocalizeDefaultString((string)tiImport.Tag);
    }
    #endregion TRADUZIONI

    #region Eventi controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    #endregion

    private void Instance_LoginSession(object sender, Authentication.LoginSessionEventArgs sessionArgs)
    {
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
    }

    private void CtrlBaseC190220_Unloaded(object sender, RoutedEventArgs e)
    {
      try
      {
        Authentication.AuthenticationManager.Instance.LoginSession -= Instance_LoginSession;
      }
      catch (Exception)
      {

      }
    }
  }
}
