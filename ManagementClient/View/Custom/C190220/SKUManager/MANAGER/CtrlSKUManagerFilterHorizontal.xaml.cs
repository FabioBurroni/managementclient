﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MaterialDesignThemes.Wpf;
using Model.Custom.C190220.Report;

namespace View.Custom.C190220.SKUManager
{
  /// <summary>
  /// Interaction logic for CtrlSKUManagerFilterHorizontal.xaml
  /// </summary>
  public partial class CtrlSKUManagerFilterHorizontal : CtrlBaseC190220
  {
    #region EVENTI
    public delegate void OnSearchHandler();
    public event OnSearchHandler OnSearch;
    #endregion

    #region Fields
    private const int _indexStartValue = 0;
    #endregion

    #region PUBLIC PROPERTIES
    public C190220_ArticleFilter  Filter { get; set; } = new C190220_ArticleFilter();
    #endregion
    
    #region Constructor
    public CtrlSKUManagerFilterHorizontal()
    {
      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      HintAssist.SetHint(txtBoxArticleCode, Localization.Localize.LocalizeDefaultString((string)txtBoxArticleCode.Tag));
      HintAssist.SetHint(txtBoxArticleDescription, Localization.Localize.LocalizeDefaultString((string)txtBoxArticleDescription.Tag));
      HintAssist.SetHint(cbMaxItems, Localization.Localize.LocalizeDefaultString((string)cbMaxItems.Tag));

      btnCancel.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnCancel.Tag);
      btnSearch.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnSearch.Tag);
    }

    #endregion TRADUZIONI

    #region Eventi controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    #endregion

    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
    }

    private void btnSearch_Click(object sender, RoutedEventArgs e)
    {
      if (cbMaxItems.SelectedItem != null)
      {
        Filter.MaxItems = (int)cbMaxItems.SelectedItem;
      }
      Filter.Index = _indexStartValue;
      Search();
    }

    #region private Methods
    private void Search()
    {
      OnSearch?.Invoke();
    }
    #endregion

    private void TextBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Return)
        Search();

    }
  }
}
