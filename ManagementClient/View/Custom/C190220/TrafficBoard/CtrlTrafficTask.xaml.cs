﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;
using Model;
using Model.Custom.C190220;
using View.Common.Languages;

namespace View.Custom.C190220.TrafficBoard
{
  /// <summary>
  /// Interaction logic for CtrlPalletTracking.xaml
  /// </summary>
  public partial class CtrlTrafficTask : CtrlBaseC190220
  {

    #region POPERTIES
    #region private

    private string _labelActualPosition = "POSITION";
    private string _labelDestination = "DESTINATION";

    #endregion private

    public C190220_TrafficBoardTaskFilter Filter { get; set; } = new C190220_TrafficBoardTaskFilter();

     public ObservableCollectionFast<C190220_PalletTraffic> PalletL { get; set; } = new ObservableCollectionFast<C190220_PalletTraffic>();
  
    #region Label to localize


    public string LabelActualPosition
    {
      get { return _labelActualPosition; }
      set
      {
        if (value != _labelActualPosition)
        {
          _labelActualPosition = value;
          NotifyPropertyChanged();
        }
      }
    }

    public string LabelDestination
    {
      get { return _labelDestination; }
      set
      {
        if (value != _labelDestination)
        {
          _labelDestination = value;
          NotifyPropertyChanged();
        }
      }
    }

    

    #endregion Label to localize


    #endregion POPERTIES

    #region COSTRUTTORE

    public CtrlTrafficTask()
    {
      Filter.OnSearch += Filter_OnSearch;
      Filter.MaxItems = 12;
      InitializeComponent();

      DispatcherTimer timer = new DispatcherTimer();
      timer.Interval = TimeSpan.FromSeconds(10);
      timer.Tick += Timer_Tick;
      timer.Start();
    }

    #endregion COSTRUTTORE

    protected override void Translate()
    {
      txtBlkTitle.Text = Localization.Localize.LocalizeDefaultString((String)txtBlkTitle.Tag);
      txtBlkDescription.Text = Localization.Localize.LocalizeDefaultString((String)txtBlkDescription.Tag);

      

      txtBlkNumberOfResult.Text = Localization.Localize.LocalizeDefaultString((String)txtBlkNumberOfResult.Tag);
      butBack.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");

      //Label column header
       string labelPosition = "POSITION";
      string labelDestinazione = "DESTINATION";
      LabelActualPosition = Context.Instance.TranslateDefault((string)labelPosition);
      LabelDestination = Context.Instance.TranslateDefault((string)labelDestinazione);


      CollectionViewSource.GetDefaultView(dataGrid.ItemsSource).Refresh();
    }

    public bool HasNext
    {
      get { return (bool)GetValue(HasNextProperty); }
      set { SetValue(HasNextProperty, value); }
    }

    // Using a DependencyProperty as the backing store for HasNext.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty HasNextProperty =
        DependencyProperty.Register("HasNext", typeof(bool), typeof(CtrlTrafficTask), new PropertyMetadata(false));

    public bool HasBack
    {
      get { return (bool)GetValue(HasBackProperty); }
      set { SetValue(HasBackProperty, value); }
    }

    // Using a DependencyProperty as the backing store for HasBack.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty HasBackProperty =
        DependencyProperty.Register("HasBack", typeof(bool), typeof(CtrlTrafficTask), new PropertyMetadata(false));

    #region COMANDI E RISPOSTE

    private void Cmd_Get_Pallet_Traffic()
    {      
      PalletL.Clear();
      CommandManagerC190220.RM_Pallet_Traffic_GetAll(this, Filter);
    }

    private void RM_Pallet_Traffic_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var pL = dwr.Data as List<C190220_PalletTraffic>;
      var palletL = pL.Where(p => p.FinalDestination != "").ToList();
      if (palletL != null)
      {
        PalletL.AddRange(palletL.Take(Filter.MaxItems));
      }

      if (PalletL.Count > Filter.MaxItems)
        HasNext = true;
      else
        HasNext = false;

      if (Filter.Index > 0)
        HasBack = true;
      else
        HasBack = false;
    }
     
    #endregion COMANDI E RISPOSTE

    #region EVENTI

    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
      Cmd_Get_Pallet_Traffic();
    }

    private void Filter_OnSearch()
    {
      Cmd_Get_Pallet_Traffic();
    }

    private void butBack_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > 0)
      {
        Filter.Index--;
        Cmd_Get_Pallet_Traffic();
      }
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_Get_Pallet_Traffic();
    }

    private void Timer_Tick(object sender, EventArgs e)
    {
      //...send the command only when the Control is selected
      if (!this.IsSelected)
        return;
      Cmd_Get_Pallet_Traffic();
    }
    private void butCopyPalletCode_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_PalletTraffic)
      {
        try
        {
          Clipboard.SetText((((C190220_PalletTraffic)b.Tag)).Code);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyArticleCode_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_PalletTraffic)
      {
        try
        {
          Clipboard.SetText((((C190220_PalletTraffic)b.Tag)).ArticleCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyLotCode_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_PalletTraffic)
      {
        try
        {
          Clipboard.SetText((((C190220_PalletTraffic)b.Tag)).LotCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyOrderCode_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_PalletTraffic)
      {
        try
        {
          Clipboard.SetText((((C190220_PalletTraffic)b.Tag)).OrderCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }
    #endregion EVENTI


  }
}