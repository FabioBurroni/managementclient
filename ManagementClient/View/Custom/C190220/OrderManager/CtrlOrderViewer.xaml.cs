﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Utilities.Extensions;
using Model;
using Model.Common;
using Model.Custom.C190220;
using Model.Custom.C190220.OrderManager;
using View.Common.Languages;
using MaterialDesignExtensions.Controls;

namespace View.Custom.C190220.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderViewer.xaml
  /// </summary>
  public partial class CtrlOrderViewer : CtrlBaseC190220
  {
    #region Public Properties
    //20200504
    public UserLogged UserLogged { get; set; } = new UserLogged();
    #endregion


    #region DP - Is Emergencial

    //private bool isEmergencial;

    //public bool IsEmergencial
    //{
    //  get { return isEmergencial; }
    //  set
    //  {
    //    isEmergencial = value;
    //    NotifyPropertyChanged("IsEmergencial");
    //  }
    //}
    //public bool IsEmergencial
    //{
    //  get { return (bool)GetValue(IsEmergencialProperty); }
    //  set { SetValue(IsEmergencialProperty, value); }
    //}

    //// Using a DependencyProperty as the backing store for IsEmergencial.  This enables animation, styling, binding, etc...
    //public static readonly DependencyProperty IsEmergencialProperty =
    //    DependencyProperty.Register("IsEmergencial", typeof(bool), typeof(CtrlOrderViewer), new PropertyMetadata(false));

    #endregion


    #region Fields
    private int _indexStartValue = 0; 
    #endregion

    #region Contructor
    public CtrlOrderViewer()
    {
      InitializeComponent();
      
      if (DesignTimeHelper.IsInDesignMode)
        return;


      Filter = ctrlFilter.Filter;
      ctrlOrders.OrderL = OrderL;
      //20200504
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
      Authentication.AuthenticationManager.Instance.LoginSession += Instance_LoginSession;

      //OrderDestinationL.AddRange(C190220_ModelContext.Instance.ListDestinasions);

    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkCurrentDestination.Text = Context.Instance.TranslateDefault((string)txtBlkCurrentDestination.Tag);
      txtBlkDestination.Text = Context.Instance.TranslateDefault((string)txtBlkDestination.Tag);
      txtBlkDestinationChange.Header = Context.Instance.TranslateDefault((string)txtBlkDestinationChange.Tag);
      txtBlkInfo.Header = Context.Instance.TranslateDefault((string)txtBlkInfo.Tag);
      txtBlkTransit.Header = Context.Instance.TranslateDefault((string)txtBlkTransit.Tag);
      //TextBlockMenu.Text = Context.Instance.TranslateDefault((string)TextBlockMenu.Tag);
      butOrderChangeDestination.Content = Localization.Localize.LocalizeDefaultString("CHANGE DESTINATION");

      btnFilter.ToolTip = Context.Instance.TranslateDefault((string)btnFilter.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
    }

    #endregion TRADUZIONI
    
    #region Evento Login
    //20200504
    private void Instance_LoginSession(object sender, Authentication.LoginSessionEventArgs sessionArgs)
    {
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
    }
    #endregion

    #region PUBLIC PROPERTIES
    public ObservableCollectionFast<C190220_ListDestination> OrderDestinationL { get; set; } = new ObservableCollectionFast<C190220_ListDestination>();
    public C190220_OrderFilter Filter { get; set; } = new C190220_OrderFilter();
    public ObservableCollectionFast<C190220_Order> OrderL { get; set; } = new ObservableCollectionFast<C190220_Order>();
    #endregion

    #region COMANDI E RISPOSTE

    #region OM_OrderComponent_Kill
    private void Cmd_OM_OrderComponent_Kill(int order_id, int cmp_id)
    {
      CommandManagerC190220.OM_OrderComponent_Kill(this, order_id,cmp_id);
    }

    private async void OM_OrderComponent_Kill(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data is C190220_CustomResult)
      {
        var result = (C190220_CustomResult)dwr.Data;
        if (result == C190220_CustomResult.OK)
        {
          int orderId = commandMethodParameters[1].ConvertTo<int>();
          if (orderId != 0)
          {
            Cmd_OM_Order_Get("", orderId);
          }
        }
        else
        {
          AlertDialogArguments alertDialogArgs = new AlertDialogArguments
          {
            Title = "ORDER COMPONENT".TD(),
            Message = "ERROR CANCELLING".TD(),
            OkButtonLabel = "OK".TD()
          };

          await AlertDialog.ShowDialogAsync("RootDialog", alertDialogArgs);

        }
      }
    }
    #endregion

    #region OM_OrderDestination_GetAll
    private void Cmd_OM_OrderDestination_GetAll()
    {
      CommandManagerC190220.OM_OrderDestination_GetAll(this);
    }

    private void OM_OrderDestination_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var list = dwr.Data as List<C190220_ListDestination>;

      if(OrderDestinationL.Count==0)
        OrderDestinationL.AddRange(list);
    }
    #endregion

    #region OM_Order_Get
    private void Cmd_OM_Order_Get(string orderCode, int orderId)
    {
      CommandManagerC190220.OM_Order_Get(this, orderCode, orderId);
    }

    private void OM_Order_Get(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var orderNew = dwr.Data as C190220_Order;
      if (orderNew != null)
      {
        var orderOld = OrderL.FirstOrDefault(ord => ord.Code == orderNew.Code);
        if (orderOld != null)
        {
          orderOld.Update(orderNew);
        }
        else
        {
          OrderL.Add(orderNew);
        }
      }
    }
    #endregion

    #region OM_Order_UpdateState
    private void Cmd_OM_Order_UpdateState(C190220_Order order, C190220_State newState)
    {
      //ShowWait(true, "PALLET", "ATTENDERE RECUPERO INFORMAZIONI");
      CommandManagerC190220.OM_Order_UpdateState(this, order.Id, newState);
    }
    private void OM_Order_UpdateState(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      if (dwr.Data is C190220_CustomResult)
      {
        var result = (C190220_CustomResult)dwr.Data;
        if (result != C190220_CustomResult.OK)
        {
          MessageBox.Show("SI E' VERIFICATO UN ERRORE! " + result.ToString());
        }
        //MessageBox.Show(result.ToString());
      }

      Cmd_OM_Order_Get("", commandMethodParameters[1].ConvertToInt());

    }
    #endregion

    #region OM_Order_Update
    private void Cmd_OM_Order_Update(C190220_Order order, bool urgente, int priority, bool completamentoManuale)
    {
      //ShowWait(true, "PALLET", "ATTENDERE RECUPERO INFORMAZIONI");
      if (order == null)
        return;

      CommandManagerC190220.OM_Order_Update(this, order.Id, priority, urgente, completamentoManuale);
    }
    private void OM_Order_Update(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      if (dwr.Data is C190220_CustomResult)
      {
        var result = (C190220_CustomResult)dwr.Data;
        MessageBox.Show(result.ToString());
      }

      Cmd_OM_Order_Get("", commandMethodParameters[1].ConvertToInt());


    }
    #endregion

    #region OM_Order_GetAll_Paged
    private void Cmd_OM_Order_GetAll_Paged()
    {
      //ShowWait(true, "PALLET", "ATTENDERE RECUPERO INFORMAZIONI");
      OrderL.Clear();
      CommandManagerC190220.OM_Order_GetAll_Paged(this, Filter);
    }
    private void OM_Order_GetAll_Paged(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var orderL = dwr.Data as List<C190220_Order>;
      if (orderL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (orderL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        orderL = orderL.Take(Filter.MaxItems).ToList();
        OrderL.AddRange(orderL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }
    #endregion

    #region OM_OrderComponent_PalletInTransit
    private void Cmd_OM_OrderComponent_PalletInTransit(int lcpId)
    {
      CommandManagerC190220.OM_OrderComponent_PalletInTransit(this, lcpId);
    }

    WinPalletInTransitForCmp winPalletInTransitForCmp = null;

    private void OM_OrderComponent_PalletInTransit(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      if (dwr.Data is List<C190220_PalletInTransit>)
      {
        var palL = dwr.Data as List<C190220_PalletInTransit>;
        if (winPalletInTransitForCmp != null)
        {
          winPalletInTransitForCmp.Close();
        }

        winPalletInTransitForCmp = new WinPalletInTransitForCmp(palL);
        winPalletInTransitForCmp.OnPalletComplete += WinPalletInTransitForCmp_OnPalletComplete;
        winPalletInTransitForCmp.Closing += (sender, e) =>
          {
            winPalletInTransitForCmp = null;
            
          };

        winPalletInTransitForCmp.ShowDialog();
      }
    }

    private void WinPalletInTransitForCmp_OnPalletComplete(C190220_PalletInTransit Pallet)
    {
      CommandManagerC190220.OM_Emergencial_CheckOut(this, Pallet.Code);
    }

    private void CtrlOrderLTA_OnPalletComplete(C190220_PalletInTransit Pallet)
    {
      CommandManagerC190220.OM_Emergencial_CheckOut(this, Pallet.Code);
    }

    private void OM_Emergencial_CheckOut(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      if (dwr.Data is C190220_CustomResult)
      {
        var result = (C190220_CustomResult)dwr.Data;

        if (result == C190220_CustomResult.OK)
          LocalSnackbar.ShowMessageOk("RESULT".TD() + ": " + result.ToString(),5);
        else
          LocalSnackbar.ShowMessageFail("RESULT".TD() + ": " + result.ToString(), 5);

        if (winPalletInTransitForCmp != null)
        {
          winPalletInTransitForCmp.Close();
        }

        if (ctrlOrders.OrderSelected != null)
          Cmd_OM_Order_Get("", ctrlOrders.OrderSelected.Id);
      }

    }

    #endregion

    #region OM_Order_DestinationChange
    private void Cmd_OM_Order_DestinationChange(string orderCode, string destinationCode)
    {
      CommandManagerC190220.OM_Order_DestinationChange(this, orderCode, destinationCode);
    }

    private void OM_Order_DestinationChange(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      C190220_CustomResult result = (C190220_CustomResult)dwr.Data;
      MessageBox.Show(result.ToString());
      string orderCode = commandMethodParameters[1].Base64Decode();
      Cmd_OM_Order_Get(orderCode, 0);
    }
    #endregion

    #region OM_Stock_Article_ByOrderDestination
    private void Cmd_OM_Stock_Article_ByOrderDestination(C190220_ListDestination ld, bool available,string articleCode, string lotCode)
    {
      int index = 0;
      int maxItems = int.MaxValue-10;
      CommandManagerC190220.OM_Stock_LotArticle_ByOrderDestination(this, index, maxItems, ld, available, articleCode, lotCode);
    }
    private void OM_Stock_LotArticle_ByOrderDestination(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      List<C190220_ArticleLotStockByCell> stock = dwr.Data as List<C190220_ArticleLotStockByCell>;

      if (stock.Count == 0)
      {
        MessageBox.Show("ARTICLE NOT AVAILABLE IN WAREHOUSE".TD());
        return;
      }

      if (winStock != null)
      {
        winStock.Close();
      }

      winStock = new WinStockForCmp(stock.FirstOrDefault());
      winStock.Closing += (sender, e) =>
      {
        winStock = null;
      };

      winStock.ShowDialog();



    }
    #endregion

    #endregion

    #region PUBLIC METHODS
    public void LoadOrderByCode(string orderCode)
    {
      OrderL.Clear();
      Cmd_OM_Order_Get(orderCode,0);
    }
    public void LoadOrderById(int orderId)
    {
      OrderL.Clear();

      Cmd_OM_Order_Get("",orderId);
    }
    #endregion

    #region EVENTI

    #region Eventi Ricerca
    private void btnFilter_Click(object sender, RoutedEventArgs e)
    {
      ExpanderMenu.IsExpanded = !ExpanderMenu.IsExpanded;
    }

    private void CtrlOrderFilter_OnSearch()
    {
      Cmd_OM_Order_GetAll_Paged();
    }
    #endregion

    #region Eventi Paginazione
    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_OM_Order_GetAll_Paged();
    }
    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_OM_Order_GetAll_Paged();
    }
    #endregion

    #region Evento Cambio Stato Ordine
    private void ctrlOrders_OnOrderChangeState(C190220_Order order, C190220_State newState)
    {
      Cmd_OM_Order_UpdateState(order, newState);
    }
    #endregion

    #region EVENTO CAMBIO VALORI
    private void CtrlOrderInfo_OnConfirm(C190220_Order order, bool urgente, int priority)
    {

    }
    private void CtrlOrderInfo_OnConfirm(C190220_Order order, bool urgente, int priority, bool completamentoManuale)
    {
      Cmd_OM_Order_Update(order,urgente,priority, completamentoManuale);
    }
    #endregion

    #region EVENTO REFRESH
    private void ctrlOrders_OnOrderRefresh(C190220_Order order)
    {
      if (order != null)
        Cmd_OM_Order_Get("", order.Id);
      else
        Cmd_OM_Order_GetAll_Paged();
    }

    private void ctrlOrders_OnOrderRefreshByState(C190220_State state)
    {
      Filter.Reset();
      Filter.StateSelected_Set(state);
      Cmd_OM_Order_GetAll_Paged();
    }
    #endregion

    #region Evento richiesta Info Componente
    private void CtrlCmps_OnInfo(C190220_OrderCmp cmp)
    {
      if (cmp != null)
      {
        Cmd_OM_OrderComponent_PalletInTransit(cmp.Id);
      }
    } 
    #endregion

    #region Evento Loaded Unloaded
    private void CtrlBaseC190220_Unloaded(object sender, RoutedEventArgs e)
    {
      try
      {
        Authentication.AuthenticationManager.Instance.LoginSession -= Instance_LoginSession;
      }
      catch (Exception)
      {

      }
    }

   

    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();

      if (Utilities.Extensions.DesignTimeHelper.IsInDesignMode)
        return;

      //if (C190220_ModelContext.Instance.ListDestinasions.Count == 0)
        Cmd_OM_OrderDestination_GetAll();

    }
    #endregion

    #region GESTIONE VISUALIZZAZIONE GIACENZA PER RIGA DI ORDINE ARTICOLO
    WinStockForCmp winStock = null;

    /// <summary>
    /// Evento Richiesta Stock poer componente dell'ordine
    /// </summary>
    /// <param name="ord"></param>
    /// <param name="cmp"></param>
    private void CtrlCmps_OnStockRequest(C190220_Order ord, C190220_OrderCmp cmp)
    {
      if (cmp == null)
        return;
      Cmd_OM_Stock_Article_ByOrderDestination (ord.Destination, true, cmp.ArticleCode, cmp.LotCode);
    }

    #endregion

    #region Evento Cambio Destinazione
    private void butOrderChangeDestination_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      var order = b.Tag as C190220_Order;
      var destination = cbChangeOrderDestination.SelectedItem as C190220_ListDestination;
      if (order != null && destination != null)
      {
        Cmd_OM_Order_DestinationChange(order.Code, destination.Code);
      }
      else if (destination == null)
        Cmd_OM_OrderDestination_GetAll();
    }
    #endregion

    #region EVENTO KILL COMPONMENTE
    private async void CtrlCmps_OnComponentKill(C190220_Order ord, C190220_OrderCmp cmp)
    {
      ConfirmationDialogArguments dialogArgs = new ConfirmationDialogArguments
      {
        Title = "CANCEL ORDER COMPONENT".TD(),
        Message = "Are you sure?".TD(),
        OkButtonLabel = "YES".TD(),
        CancelButtonLabel = "CANCEL".TD(),
        StackedButtons = false
      };

      bool result = await ConfirmationDialog.ShowDialogAsync("RootDialog", dialogArgs);

      if (result)
      {
        Cmd_OM_OrderComponent_Kill(ord.Id, cmp.Id);
      }
    }
    #endregion

    #endregion

   
  }

 
}
