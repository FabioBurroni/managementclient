﻿
using System.Windows;
using Model;
using Model.Custom.C190220.OrderManager;
namespace View.Custom.C190220.OrderManager
{
  /// <summary>
  /// Interaction logic for WinStockForCmp.xaml
  /// </summary>
  public partial class WinStockForCmp : Window
  {
    public string Cell { get; set; }
    public string NumberOfPallets { get; set; }

    public C190220_ArticleLotStockByCell Stock { get; set; } = new C190220_ArticleLotStockByCell();
    public WinStockForCmp(C190220_ArticleLotStockByCell stock)
    {
      
      if(stock != null)
      {
        Stock = stock;
      }
      InitializeComponent();

      Translate();

    }

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void Translate()
    {
      Title = Localization.Localize.LocalizeDefaultString("STOCK FOR ORDER ITEM");
      //txtBlkStockFor.Text = Context.Instance.TranslateDefault((string)txtBlkStockFor.Tag);

      Cell = Localization.Localize.LocalizeDefaultString("CELL");
      NumberOfPallets = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLETS");

      colCellCode.Header = Cell;
      colNumPallet.Header = NumberOfPallets;
    }
  }
}
