﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Model;
using Model.Custom.C190220.OrderManager;
using View.Common.Languages;

namespace View.Custom.C190220.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderLTA.xaml
  /// </summary>
  public partial class CtrlOrderLTA : CtrlBaseC190220
  {
    /// <summary>
    /// Delegate for Complete Pallet
    /// </summary>
    /// <param name="PalletInTransit"></param>
    public delegate void OnPalletCompleteRequest(C190220_PalletInTransit Pallet);
    /// <summary>
    /// Event for Kill Component
    /// </summary>
    public event OnPalletCompleteRequest OnPalletComplete;

    #region DP - PalletInTransitL
    public ObservableCollectionFast<C190220_PalletInTransit> PalletInTransitL
    {
      get { return (ObservableCollectionFast<C190220_PalletInTransit>)GetValue(PalletInTransitLProperty); }
      set { SetValue(PalletInTransitLProperty, value);
          }
    }

    public static readonly DependencyProperty PalletInTransitLProperty =
        DependencyProperty.Register("PalletInTransitL", typeof(ObservableCollectionFast<C190220_PalletInTransit>)
          , typeof(CtrlOrderLTA)
          , new PropertyMetadata(null, PalletInTransitLPropertyChanged));

    #endregion

    private static void PalletInTransitLPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var ctrl = (CtrlOrderLTA)d;

      if (ctrl.PalletInTransitL != null)
      {
        if (ctrl.PalletInTransitL.Count > 0)
        {
          if (ctrl.PalletInTransitL.FirstOrDefault() != null)
          {
            //if (ctrl.PalletInTransitL.FirstOrDefault().WorkModality == C190220_WorkModality.EMERGENCIAL)
            //{
            //  ctrl.ButtonCompleteVisibility = Visibility.Visible;
            //}
            //else
              ctrl.ButtonCompleteVisibility = Visibility.Collapsed;
          }
          else
            ctrl.ButtonCompleteVisibility = Visibility.Collapsed;
        }
        else
          ctrl.ButtonCompleteVisibility = Visibility.Collapsed;
      }
      else
        ctrl.ButtonCompleteVisibility = Visibility.Collapsed;

      ctrl.colComplete.Visibility = ctrl.ButtonCompleteVisibility;
    }

    private Visibility buttonCompleteVisibility;

    public Visibility ButtonCompleteVisibility
    {
      get { return buttonCompleteVisibility; }
      set { buttonCompleteVisibility = value;
            NotifyPropertyChanged("ButtonCompleteVisibility");
            if (dgOrders.ItemsSource != null)
              CollectionViewSource.GetDefaultView(dgOrders.ItemsSource).Refresh();
      }
    }



    #region CONSTRUCTOR
    public CtrlOrderLTA()
    {
      
      WordDicAdd("PALLET_IN_TRANSIT", "PALLET IN TRANSIT");
      WordDicAdd("PALLET_IN_TRANSIT_COUNT", "PALLET IN TRANSIT COUNT");
      WordDicAdd("COPY", "COPY");
      WordDicAdd("PALLET_CODE", "PALLET CODE");
      WordDicAdd("ARTICLE_CODE", "ARTICLE CODE");
      WordDicAdd("LOT_CODE", "LOT CODE");
      WordDicAdd("ARTICLE_DESCR", "ARTICLE DESCRIPTION");
      WordDicAdd("POSITION_CODE", "POSITION CODE");
      WordDicAdd("DESTINATION", "DESTINATION");

      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtbLKTitle.Text = Context.Instance.TranslateDefault((string)txtbLKTitle.Tag);
      txtBlkNumberOfPallets.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfPallets.Tag);

      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colCopy.Header = Localization.Localize.LocalizeDefaultString("COPY");
      colDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colLotCode.Header = Localization.Localize.LocalizeDefaultString("LOT CODE");
      colDestination.Header = Localization.Localize.LocalizeDefaultString("DESTINATION");
      colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
      colPosition.Header = Localization.Localize.LocalizeDefaultString("POSITION");
      colComplete.Header = Localization.Localize.LocalizeDefaultString("COMPLETE");

      if (dgOrders.ItemsSource != null)
        CollectionViewSource.GetDefaultView(dgOrders.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();

      foreach (var kvp in WordDic)
      {
        kvp.Value.Localized = kvp.Value.DefaultVal.TD();
      }
    }
    #endregion

    #region TRADUZIONI
    public Dictionary<string, Localization.Word> WordDic { get; set; } = new Dictionary<string, Localization.Word>();

    private void WordDicAdd(string key, string defaultVal)
    {
      if (WordDic.ContainsKey(key))
        return;
      WordDic.Add(key, new Localization.Word(key, defaultVal, defaultVal.TD()));
    }
   
    #endregion

    #region EVENTI
    private void butCopyPalletCode_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        Button b = sender as Button;
        if (b == null)
          return;
        Clipboard.SetText(b.Tag as string);

      }
      catch
      {
      }
    }
    #endregion

    private void butComplete_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_PalletInTransit)
      {
        C190220_PalletInTransit pal = b.Tag as C190220_PalletInTransit;

        OnPalletComplete?.Invoke(pal);
      }
    }

    private void CtrlBaseC190220_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
    {

    }
  }
}
