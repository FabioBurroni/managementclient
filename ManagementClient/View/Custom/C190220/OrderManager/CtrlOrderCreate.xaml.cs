﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Utilities.Extensions;
using Model;
using Model.Custom.C190220;
using Model.Custom.C190220.OrderManager;
using Model.Custom.C190220.Filter;

namespace View.Custom.C190220.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderCreate.xaml
  /// </summary>
  public partial class CtrlOrderCreate : CtrlBaseC190220
  {

    public delegate void OnOrderLoadHandler(string orderCode);
    public event OnOrderLoadHandler OnOrderLoad;


    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region PUBBLIC PROPERTIES
    public C190220_ArticleForOrderFilter Filter { get; set; } = new C190220_ArticleForOrderFilter();
    public ObservableCollectionFast<C190220_StockForOrder> ArticleList { get; set; } = new ObservableCollectionFast<C190220_StockForOrder>();

    public C190220_OrderWrapper Order { get; set; } = new C190220_OrderWrapper();

    public C190220_StockForOrder ArticleSelected { get; set; }

    public C190220_OrderComponentWrapper ComponentSelected { get; set; }

    #endregion


    #region COSTRUTTORE
    public CtrlOrderCreate()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkArticleCode.Text = Context.Instance.TranslateDefault((string)txtBlkArticleCode.Tag);
      txtBlkArticlesInWarehouse.Text = Context.Instance.TranslateDefault((string)txtBlkArticlesInWarehouse.Tag);
      txtBlNumberOfItems.Text = Context.Instance.TranslateDefault((string)txtBlNumberOfItems.Tag);
      txtBlkOnlyAvailableArticles.Text = Context.Instance.TranslateDefault((string)txtBlkOnlyAvailableArticles.Tag);
      txtBlkOrderBuilder.Text = Context.Instance.TranslateDefault((string)txtBlkOrderBuilder.Tag);
      txtBlkOrderCode.Text = Context.Instance.TranslateDefault((string)txtBlkOrderCode.Tag);
      txtBlkLotCode.Text = Context.Instance.TranslateDefault((string)txtBlkLotCode.Tag);
      txtBlkOrderItems.Text = Context.Instance.TranslateDefault((string)txtBlkOrderItems.Tag);
      txtBlkSearchForArticle.Text = Context.Instance.TranslateDefault((string)txtBlkSearchForArticle.Tag);
      txtBlNumberOfItems.Text = Context.Instance.TranslateDefault((string)txtBlNumberOfItems.Tag);
      txtBlkOnlyForMyPosition.Text = Context.Instance.TranslateDefault((string)txtBlkOnlyForMyPosition.Tag);
      txtBlkDestinationMachine.Text = Context.Instance.TranslateDefault((string)txtBlkDestinationMachine.Tag);
      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);

      colArticle.Header = Localization.Localize.LocalizeDefaultString("ARTICLE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("CODE");
      colDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colLotCode.Header = Localization.Localize.LocalizeDefaultString("LOT");
      colLot.Header = Localization.Localize.LocalizeDefaultString("LOT");
      colDescriptionOrder.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colNumberOfPallets.Header = Localization.Localize.LocalizeDefaultString("NUMBER OF PALLETS");
      colRemove.Header = Localization.Localize.LocalizeDefaultString("REMOVE"); 
      //colType.Header = Localization.Localize.LocalizeDefaultString("TYPE");
      colQuantity.Header = Localization.Localize.LocalizeDefaultString("REQUESTED");
      //colUnit.Header = Localization.Localize.LocalizeDefaultString("REQUESTED");
      
      butCancelOrder.ToolTip = Localization.Localize.LocalizeDefaultString("CANCEL");
      butCreateOrder.ToolTip = Localization.Localize.LocalizeDefaultString("CREATE");
      butGenerateOrderCode.ToolTip = Localization.Localize.LocalizeDefaultString("GENERATE ORDER CODE");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butSearch.ToolTip = Localization.Localize.LocalizeDefaultString("SEARCH");

      CollectionViewSource.GetDefaultView(dgArticle.ItemsSource).Refresh();
      CollectionViewSource.GetDefaultView(dgOrder.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
    #endregion


    #region COMANDI E RISPOSTE
    private void Cmd_OM_Stock_Article()
    {
      CommandManagerC190220.OM_Stock_ArticleLotCode(this, Context.Position, Filter);
    }
    private void OM_Stock_ArticleLotCode(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      var giacL = dwr.Data as List<C190220_StockForOrder>;
      if (giacL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (giacL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        giacL = giacL.Take(Filter.MaxItems).ToList();
        ArticleList.AddRange(giacL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

    private void Cmd_OM_Order_Create()
    {
      CommandManagerC190220.OM_Order_Create(this, Order);
    }
    private void OM_Order_Create(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      if (dwr.Data is C190220_CustomResult)
      {
        var result = (C190220_CustomResult)dwr.Data;
        if (result == C190220_CustomResult.OK)
        {
          string orderCode = Order.Code;
          MessageBox.Show(Context.Instance.TranslateDefault("ORDER CREATED SUCCESSFULLY"));
          Order.Reset();
          OnOrderLoad?.Invoke(orderCode);
        }
        else
        {
          MessageBox.Show(Context.Instance.TranslateDefault("ERROR CREATING ORDER: ") + result);
        }
      }
    }
    #endregion


    #region EVENTI
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void dgAddArticleToOrder_Click(object sender, RoutedEventArgs e)
    {
      if (ArticleSelected != null)
      {
        var cmp = Order.CmpL.FirstOrDefault(cp => cp.ArticleCode.EqualsIgnoreCase(ArticleSelected.Article.Code)&&
                                                  cp.LotCode.EqualsIgnoreCase(ArticleSelected.LotCode)
                                          );
        if (cmp != null)
          cmp.QtyRequested++;
        else
          Order.CmpL.Add(new C190220_OrderComponentWrapper()
          {
            ArticleCode = ArticleSelected.Article.Code,
            ArticleDescr = ArticleSelected.Article.Descr,
            LotCode = ArticleSelected.LotCode,
            QtyRequested = 1
          });
      }
    }

    private void butSearchCancel_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
      ArticleList.Clear();
    }
    private void butSearch_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index = _indexStartValue;
      Search();
    }


    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Search();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Search();
    }



    private void dgButCmpRemove_Click(object sender, RoutedEventArgs e)
    {
      if (ComponentSelected != null)
      {
        Order.CmpL.Remove(ComponentSelected);
      }
    }

    private void dgButCmpIncrease_Click(object sender, RoutedEventArgs e)
    {
      var rb = sender as System.Windows.Controls.Primitives.RepeatButton;
      if (rb.Tag != null && rb.Tag is C190220_OrderComponentWrapper)
      {
        ((C190220_OrderComponentWrapper)rb.Tag).QtyRequested++;
      }

      //if (ComponentSelected != null)
      //{
      //  ComponentSelected.QtyRequested++;
      //}
    }

    private void dgButCmpDecease_Click(object sender, RoutedEventArgs e)
    {
      var rb = sender as System.Windows.Controls.Primitives.RepeatButton;
      if (rb.Tag != null && rb.Tag is C190220_OrderComponentWrapper)
      {
        var cmp = (C190220_OrderComponentWrapper)rb.Tag;
        cmp.QtyRequested--;
        if(cmp.QtyRequested<=0)
        {
          Order.CmpL.Remove(cmp);
        }
      }
      //if (ComponentSelected != null)
      //{
      //  ComponentSelected.QtyRequested--;
      //  if (ComponentSelected.QtyRequested <= 0)
      //  {
      //    Order.CmpL.Remove(ComponentSelected);
      //  }
      //}
    }
    #endregion

    private void butGenerateOrderCode_Click(object sender, RoutedEventArgs e)
    {
      Order.Code = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
    }

    private void butGenerateDestinationMachine_Click(object sender, RoutedEventArgs e)
    {
      Order.Destination = new Random().Next(1,999999999).ToString();
    }

    private void butCancelOrder_Click(object sender, RoutedEventArgs e)
    {
      Order.Reset();
      ComponentSelected = null;
    }
    private void butCreateOrder_Click(object sender, RoutedEventArgs e)
    {
      Cmd_OM_Order_Create();
    }
    private void CtrlBaseC190220_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      ArticleList.Clear();
    }

    #region Metodi Privati

    private void Search()
    {
      ArticleList.Clear();
      Cmd_OM_Stock_Article();
    }

    #endregion

  }
}
