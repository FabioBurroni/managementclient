﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Model;
using Model.Custom.C190220;
using Model.Custom.C190220.OrderManager;
using Utilities.Extensions;
using View.Common.Languages;

namespace View.Custom.C190220.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrders.xaml
  /// </summary>
  public partial class CtrlOrdersInExecution : CtrlBaseC190220
  { 
    private int _indexStartValue = 0;
    public C190220_OrderFilter Filter { get; set; } = new C190220_OrderFilter();

    public ObservableCollectionFast<C190220_Order> OrderL { get; set; } = new ObservableCollectionFast<C190220_Order>();

    private C190220_Order orderSelected;

    public C190220_Order OrderSelected
    {
      get { return orderSelected; }
      set 
      { 
        orderSelected = value;
        NotifyPropertyChanged("OrderSelected");
        NotifyPropertyChanged("PalletInTransitL");
      }
    }

    public ObservableCollectionFast<C190220_PalletInTransit> PalletInTransitL
    {
      get { return (OrderSelected != null) ? OrderSelected.PalletInTransitL : new ObservableCollectionFast<C190220_PalletInTransit>(); }      
    }


    #region Constructor
    public CtrlOrdersInExecution()
    {
      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      this.Resources["strPressToPause"] = Context.Instance.TranslateDefault("PRESS TO PAUSE");
      this.Resources["strPressToExecute"] = Context.Instance.TranslateDefault("PRESS TO EXECUTE");
      this.Resources["strPressToCancel"] = Context.Instance.TranslateDefault("PRESS TO CANCEL");
      
      txtBlkOrderInExecution.Text = Context.Instance.TranslateDefault((string)txtBlkOrderInExecution.Tag);

      colCode.Header = Localization.Localize.LocalizeDefaultString("CODE");
      colDestination.Header = Localization.Localize.LocalizeDefaultString("DESTINATION");
      colKill.Header = Localization.Localize.LocalizeDefaultString("KILL");
      colPause.Header = Localization.Localize.LocalizeDefaultString("PAUSE");
      colRefresh.Header = Localization.Localize.LocalizeDefaultString("REFRESH");
      colState.Header = Localization.Localize.LocalizeDefaultString("STATE");

      butRefresh.ToolTip = Localization.Localize.LocalizeDefaultString((string)butRefresh.Tag.ToString());

      if (dgOrders.ItemsSource != null)
      CollectionViewSource.GetDefaultView(dgOrders.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
    private void CtrlBaseC190220_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      Filter.Reset();
      Cmd_OM_Order_GetAll_Paged();
    }
    #endregion

    #region xml commands

    #region CheckOut in Emergencial
    private void CtrlOrderLTA_OnPalletComplete(C190220_PalletInTransit Pallet)
    {
      CommandManagerC190220.OM_Emergencial_CheckOut(this, Pallet.Code);
    }

    private void OM_Emergencial_CheckOut(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      if (dwr.Data is C190220_CustomResult)
      {
        var result = (C190220_CustomResult)dwr.Data;

        if (result == C190220_CustomResult.OK)
          LocalSnackbar.ShowMessageOk("RESULT".TD() + ": " + result.ToString(), 5);
        else
          LocalSnackbar.ShowMessageFail("RESULT".TD() + ": " + result.ToString(), 5);

        if (OrderSelected != null)
          Cmd_OM_Order_Get("", OrderSelected.Id);
      }

    }
    #endregion

    #region OM_Order_Get
    private void Cmd_OM_Order_Get(string orderCode, int orderId)
    {
      CommandManagerC190220.OM_Order_Get(this, orderCode, orderId);
    }

    private void OM_Order_Get(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var orderNew = dwr.Data as C190220_Order;
      if (orderNew != null)
      {
        var orderOld = OrderL.FirstOrDefault(ord => ord.Code == orderNew.Code);
        if (orderOld != null)
        {
          orderOld.Update(orderNew);
        }
        else
        {
          OrderL.Add(orderNew);
        }
      }
    }
    #endregion

    #region OM_Order_GetAll_Paged
    private void Cmd_OM_Order_GetAll_Paged()
    {      
      Filter.StateSelected_Set(C190220_State.LEXEC);

      if (OrderL != null)
        OrderL.Clear();

      CommandManagerC190220.OM_Order_GetAll_Paged(this, Filter);
    }
    private void OM_Order_GetAll_Paged(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var orderL = dwr.Data as List<C190220_Order>;
      if (orderL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (orderL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        orderL = orderL.Take(Filter.MaxItems).ToList();
        OrderL.AddRange(orderL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }
    #endregion

    #region OM_Order_UpdateState
    private void Cmd_OM_Order_UpdateState(C190220_Order order, C190220_State newState)
    {
      //ShowWait(true, "PALLET", "ATTENDERE RECUPERO INFORMAZIONI");
      CommandManagerC190220.OM_Order_UpdateState(this, order.Id, newState);
    }
    private void OM_Order_UpdateState(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      if (dwr.Data is C190220_CustomResult)
      {
        var result = (C190220_CustomResult)dwr.Data;
        if (result != C190220_CustomResult.OK)
        {
          MessageBox.Show("SI E' VERIFICATO UN ERRORE! " + result.ToString());
        }
        //MessageBox.Show(result.ToString());
      }

      Cmd_OM_Order_Get("", commandMethodParameters[1].ConvertToInt());

    }
    #endregion
    #endregion

    #region Eventi

    #region Eventi Paginazione
    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_OM_Order_GetAll_Paged();
    }
    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_OM_Order_GetAll_Paged();
    }
    #endregion

    private void butOrderKill_Click(object sender, RoutedEventArgs e)
    {
      var dr = MessageBox.Show("SEI SICURO DI ANNULLARE L'ORDINE?", "ANNULLARE ORDINE", MessageBoxButton.YesNo);
      if (dr == MessageBoxResult.No)
      {
        e.Handled = true;
        return;
      }

      Button b = sender as Button;
      var order = b.Tag as C190220_Order;
      if(order!=null)
      {
        Cmd_OM_Order_UpdateState(order, C190220_State.LKILL);
      }
    }
    

    private void butOrderPause_Click(object sender, RoutedEventArgs e)
    {
      var dr = MessageBox.Show("SEI SICURO DI SOSPENDERE L'ORDINE?", "SOSPENDERE ORDINE", MessageBoxButton.YesNo);
      if (dr == MessageBoxResult.No)
      {
        e.Handled = true;
        return;
      }

      Button b = sender as Button;
      var order = b.Tag as C190220_Order;
      if (order != null)
      {
        Cmd_OM_Order_UpdateState(order, C190220_State.LPAUSE);
      }
    }
    
    private void butOrderRefresh_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      var order = b.Tag as C190220_Order;
      if (order != null)
      {
        if (order != null)
          Cmd_OM_Order_Get("", order.Id);
        else
          Cmd_OM_Order_GetAll_Paged();

      }
    }

    private void butOrderCopy_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        var b = sender as Button;
        if (b == null)
          return;
        var ord = b.Tag as C190220_Order;
        if (ord != null)
          Clipboard.SetText(ord.Code);
      }
      catch (Exception ex)
      {
        var exc = ex.ToString();
      }

    }

    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
      Cmd_OM_Order_GetAll_Paged();
    }

    #endregion

  }
}
