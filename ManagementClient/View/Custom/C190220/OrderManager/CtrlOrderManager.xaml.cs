﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Model;
using Model.Custom.C190220;
using Model.Custom.C190220.OrderManager;
using View.Common.Languages;

namespace View.Custom.C190220.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrderManager.xaml
  /// </summary>
  public partial class CtrlOrderManager : CtrlBaseC190220
  {   

    #region CONSTRUCTOR
    public CtrlOrderManager()
    {
      InitializeComponent();

    } 
    #endregion
        
    private void CtrlOrderCreate_OnOrderLoad(string orderCode)
    {
      ctrlOrderViewer.LoadOrderByCode(orderCode);
      tiOrdersViewer.IsSelected = true;
    }

    #region TRADUZIONI

    protected override void Translate()
    {
      txtTabHeaderOrderBuilder.Text = Context.Instance.TranslateDefault((string)txtTabHeaderOrderBuilder.Tag);
      txtTabHeaderOrderManager.Text = Context.Instance.TranslateDefault((string)txtTabHeaderOrderManager.Tag);
    }
    #endregion
    #region COMANDI E RISPOSTE

    #region OM_OrderDestination_GetAll
    private void Cmd_OM_OrderDestination_GetAll()
    {
      CommandManagerC190220.OM_OrderDestination_GetAll(this);
    }

    private void OM_OrderDestination_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var list = dwr.Data as List<C190220_ListDestination>;
      if (C190220_ModelContext.Instance.ListDestinasions.Count == 0)
      {
        C190220_ModelContext.Instance.ListDestinasions.AddRange(list);
      }
    }
    #endregion    

   
    #endregion

    #region EVENTI COTROLLO
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      if (C190220_ModelContext.Instance.ListDestinasions.Count == 0)
        Cmd_OM_OrderDestination_GetAll();

      Translate();
    }

    private void CtrlBaseC190220_Unloaded(object sender, RoutedEventArgs e)
    {
      //try
      //{
      //  //Authentication.AuthenticationManager.Instance.LoginSession -= Instance_LoginSession;
      //}
      //catch (Exception)
      //{

      //}
    }

    #endregion

  }
}
