﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Model;
using Model.Custom.C190220.OrderManager;
using View.Common.Languages;

namespace View.Custom.C190220.OrderManager
{
  /// <summary>
  /// Interaction logic for CtrlOrders.xaml
  /// </summary>
  public partial class CtrlOrders : CtrlBaseC190220
  {
    public delegate void OnOrderChangeStateHandler(C190220_Order order, C190220_State newState);
    public event OnOrderChangeStateHandler OnOrderChangeState;

    public delegate void OnOrderRefreshHandler(C190220_Order order);
    public event OnOrderRefreshHandler OnOrderRefresh;

    public delegate void OnOrderRefreshByStateHandler(C190220_State state);
    public event OnOrderRefreshByStateHandler OnOrderRefreshByState;

    #region Constructor
    public CtrlOrders()
    {
      InitializeComponent();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      this.Resources["strPressToPause"] = Context.Instance.TranslateDefault("PRESS TO PAUSE");
      this.Resources["strPressToExecute"] = Context.Instance.TranslateDefault("PRESS TO EXECUTE");
      this.Resources["strPressToCancel"] = Context.Instance.TranslateDefault("PRESS TO CANCEL");
      
      txtBlkOrderManager.Text = Context.Instance.TranslateDefault((string)txtBlkOrderManager.Tag);

      colCode.Header = Localization.Localize.LocalizeDefaultString("CODE");
      colDestination.Header = Localization.Localize.LocalizeDefaultString("DESTINATION");
      colExecution.Header = Localization.Localize.LocalizeDefaultString("EXECUTION");
      colKill.Header = Localization.Localize.LocalizeDefaultString("KILL");
      colPause.Header = Localization.Localize.LocalizeDefaultString("PAUSE");
      colPriority.Header = Localization.Localize.LocalizeDefaultString("PRIORITY");
      colRefresh.Header = Localization.Localize.LocalizeDefaultString("REFRESH");
      colState.Header = Localization.Localize.LocalizeDefaultString("STATE");

      butLoadOrderPlay.ToolTip = Localization.Localize.LocalizeDefaultString((string)butLoadOrderPlay.Tag.ToString());
      butRefresh.ToolTip = Localization.Localize.LocalizeDefaultString((string)butRefresh.Tag.ToString());

      if (dgOrders.ItemsSource != null)
      CollectionViewSource.GetDefaultView(dgOrders.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region Eventi Controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
    #endregion

    #region DP PROPERTIES
    //public ObservableCollectionFast<C190220_Order> OrderL { get; set;}

    #region DP - OrderL
    public ObservableCollectionFast<C190220_Order> OrderL
    {
      get { return (ObservableCollectionFast<C190220_Order>)GetValue(OrderLProperty); }
      set { SetValue(OrderLProperty, value); }
    }

    // Using a DependencyProperty as the backing store for OrderL.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty OrderLProperty =
        DependencyProperty.Register("OrderL", typeof(ObservableCollectionFast<C190220_Order>), typeof(CtrlOrders), new PropertyMetadata(null));


    #endregion

    #region DP - OrderSelected
    public C190220_Order OrderSelected
    {
      get { return (C190220_Order)GetValue(OrderSelectedProperty); }
      set { SetValue(OrderSelectedProperty, value); }
    }

    // Using a DependencyProperty as the backing store for OrderSelected.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty OrderSelectedProperty =
        DependencyProperty.Register("OrderSelected", typeof(C190220_Order), typeof(CtrlOrders), new PropertyMetadata(null));
    #endregion

    #endregion

    #region Eventi
    private void butOrderCodeCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is string)
      {
        try
        {
          Clipboard.SetText(((string)b.Tag));
        }
        catch
        {

        }
      }
    }
    private void butOrderKill_Click(object sender, RoutedEventArgs e)
    {
      var dr = MessageBox.Show(Context.Instance.TranslateDefault("ARE YOU SURE TO KILL THE ORDER") 
      + "?", Context.Instance.TranslateDefault("KILL ORDER"), MessageBoxButton.YesNo);
      if (dr == MessageBoxResult.No)
      {
        e.Handled = true;
        return;
      }

      Button b = sender as Button;
      var order = b.Tag as C190220_Order;
      if (order != null)
      {
        OnOrderChangeState?.Invoke(order, C190220_State.LKILL);
      }
    }
    private void butOrderPause_Click(object sender, RoutedEventArgs e)
    {
      var dr = MessageBox.Show(Context.Instance.TranslateDefault("ARE YOU SURE TO SUSPEND THE ORDER") 
      + "?", Context.Instance.TranslateDefault("SUSPEND ORDER"), MessageBoxButton.YesNo);
      if (dr == MessageBoxResult.No)
      {
        e.Handled = true;
        return;
      }

      Button b = sender as Button;
      var order = b.Tag as C190220_Order;
      if (order != null)
      {
        OnOrderChangeState?.Invoke(order, C190220_State.LPAUSE);
      }
    }
    private void butOrderExecute_Click(object sender, RoutedEventArgs e)
    {
      var dr = MessageBox.Show(Context.Instance.TranslateDefault("ARE YOU SURE TO EXECUTE THE ORDER")
      + "?", Context.Instance.TranslateDefault("ORDER EXECUTION"), MessageBoxButton.YesNo);
      if (dr == MessageBoxResult.No)
      {
        e.Handled = true;
        return;
      }

      Button b = sender as Button;
      var order = b.Tag as C190220_Order;
      if (order != null)
      {
        OnOrderChangeState?.Invoke(order, C190220_State.LEXEC);
      }
    }
    private void butOrderRefresh_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      var order = b.Tag as C190220_Order;
      if (order != null)
      {
        OnOrderRefresh?.Invoke(order);
      }
    }
    private void butOrderCopy_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        var b = sender as Button;
        if (b == null)
          return;
        var ord = b.Tag as C190220_Order;
        if (ord != null)
          Clipboard.SetText(ord.Code);
      }
      catch (Exception ex)
      {
        var exc = ex.ToString();
      }

    }
    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      OnOrderRefresh?.Invoke(null);
    }

    private void butLoadOrderPlay_Click(object sender, RoutedEventArgs e)
    {
      OnOrderRefreshByState?.Invoke(C190220_State.LEXEC);
    }

    private void butLoadOrderWait_Click(object sender, RoutedEventArgs e)
    {
      OnOrderRefreshByState?.Invoke(C190220_State.LWAIT);
    }

    private void butLoadOrderPause_Click(object sender, RoutedEventArgs e)
    {
      OnOrderRefreshByState?.Invoke(C190220_State.LPAUSE);
    }

    private void butLoadOrderDone_Click(object sender, RoutedEventArgs e)
    {
      OnOrderRefreshByState?.Invoke(C190220_State.LDONE);
    }

    private void butLoadOrderKill_Click(object sender, RoutedEventArgs e)
    {
      OnOrderRefreshByState?.Invoke(C190220_State.LKILL);
    }
    #endregion

    private void CtrlBaseC190220_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      //OrderL.Clear();
    }
  }
}
