﻿
using System.Collections.Generic;
using System.Windows;
using Model.Custom.C190220.OrderManager;
using Model;
using System.Windows.Controls;
using System.Linq;
using MaterialDesignThemes.Wpf;
using System;
using System.Windows.Documents;

namespace View.Custom.C190220.OrderManager
{
  /// <summary>
  /// Interaction logic for WinPalletInTransitForCell.xaml
  /// </summary>
  public partial class CtrlPalletInTransitForCell : CtrlBaseC190220
  {
    /// <summary>
    /// Delegate for Complete Pallet
    /// </summary>
    /// <param name="PalletInTransit"></param>
    public delegate void OnPalletCompleteRequest(C190220_PalletInTransit Pallet);

    /// <summary>
    /// Event for Kill Component
    /// </summary>
    public event OnPalletCompleteRequest OnPalletComplete;

    private ObservableCollectionFast<C190220_PalletInTransit> _PalletInTransit;
    public ObservableCollectionFast<C190220_PalletInTransit> PalletInTransit
    {
      get { return _PalletInTransit; }
      set
      {
        _PalletInTransit = value;
        NotifyPropertyChanged("PalletInTransit");
      }
    }

    public CtrlPalletInTransitForCell(List<C190220_PalletInTransit> palletInTransit)
    {
      InitializeComponent();

      PalletInTransit = new ObservableCollectionFast<C190220_PalletInTransit>();

      PalletInTransit.AddRange(palletInTransit);

      if (palletInTransit.Count > 0)
      {
        //if (palletInTransit.FirstOrDefault().WorkModality == C190220_WorkModality.EMERGENCIAL)
        //  colComplete.Visibility = Visibility.Visible;
        //else
          colComplete.Visibility = Visibility.Collapsed;
      }

      Translate();
    }

    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    protected override void Translate()
    {
      txtBlkNoPallet.Text = Context.Instance.TranslateDefault((string)txtBlkNoPallet.Tag);
      butClose.Content = Context.Instance.TranslateDefault((string)butClose.Tag);
      butClose.ToolTip = Context.Instance.TranslateDefault((string)butClose.Tag);

      colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
      
      if(PalletInTransit != null && PalletInTransit.Count() > 0)
      {
        var value = GetLCR(PalletInTransit.FirstOrDefault().PositionCode);
        Title.Text = PalletInTransit.FirstOrDefault().PositionCode;

        SubTitle.Inlines.Add(Localization.Localize.LocalizeDefaultString("COLUMN") + ": ");
        SubTitle.Inlines.Add(new Bold(new Run(value.Item1)));

        SubTitle.Inlines.Add(", " + Localization.Localize.LocalizeDefaultString("FLOOR") + ": ");
        SubTitle.Inlines.Add(new Bold(new Run(value.Item2)));

        SubTitle.Inlines.Add(", " + Localization.Localize.LocalizeDefaultString("SIDE") + ": ");
        SubTitle.Inlines.Add(new Bold(new Run(value.Item3)));
      }
    }

    
    private void butComplete_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_PalletInTransit)
      {
        C190220_PalletInTransit pal = b.Tag as C190220_PalletInTransit;

        OnPalletComplete?.Invoke(pal);

        DialogHost.CloseDialogCommand.Execute(true, null);
      }
    }

    Tuple<string, string, string> GetLCR(string value)
    {
      var column = "";
      var floor = "";
      var side = "";
      if (value != null)
      {
        string val = (string)value;

        var v = val.Split('-');

        if (v.Count() == 4)
        {
          int col;
          Int32.TryParse(v[1], out col);

          column = col.ToString();

          Int32.TryParse(v[2], out col);

          floor = col.ToString();

          Int32.TryParse(v[3], out col);

          side = col.ToString();
        }
      }

      return Tuple.Create(column, floor, side);
    }
  }
}
