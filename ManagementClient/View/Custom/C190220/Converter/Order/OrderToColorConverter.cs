﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Model.Custom.C190220;

namespace View.Custom.C190220.Converter
{
  public class OrderToColorConverter : IValueConverter
  {

    private bool _IsBackground;
    public bool IsBackground
    {
      get { return _IsBackground; }
      set
      {
        if (value != _IsBackground)
        {
          _IsBackground = value;
        }
      }
    }

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is C190220_ListColor)
      {
        switch ((C190220_ListColor)value)
        {
          case C190220_ListColor.AZURE:
            return IsBackground ? Brushes.Black : Brushes.Aquamarine;
          case C190220_ListColor.BLUE:
            return IsBackground ? Brushes.White : Brushes.Blue;
          case C190220_ListColor.BROWN:
            return IsBackground ? Brushes.White : Brushes.Brown;
          case C190220_ListColor.GRAY:
            return IsBackground ? Brushes.White : Brushes.Gray;
          case C190220_ListColor.GREEN:
            return IsBackground ? Brushes.White : Brushes.Green;
          case C190220_ListColor.ORANGE:
            return IsBackground ? Brushes.White : Brushes.Orange;
          case C190220_ListColor.PINK:
            return IsBackground ? Brushes.Black : Brushes.Pink;
          case C190220_ListColor.RED:
            return IsBackground ? Brushes.White : Brushes.Red;
          case C190220_ListColor.YELLOW:
            return IsBackground ? Brushes.Black : Brushes.Yellow;
          case C190220_ListColor.WHITE:
            return IsBackground ? Brushes.Black : Brushes.White;
          default:
            return IsBackground ? Brushes.Gray : Brushes.Azure;
        }
      }

      return Brushes.White;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
