﻿using System;
using System.Windows.Data;
using System.Globalization;

using Model.Custom.C190220.OrderManager;

namespace View.Custom.C190220.Converter
{
  public class OrderStateToStringConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is C190220_State)
      {
        switch((C190220_State)value)
        {
          case C190220_State.CDONE:
            return "COMPLETED";
          case C190220_State.CEXEC:
            return "EXECUTING";
          case C190220_State.CKILL:
            return "KILLED";
          case C190220_State.CPAUSE:
            return "PAUSED";
          case C190220_State.CWAIT:
            return "WAITING";
          case C190220_State.LDONE:
            return "COMPLETED";
          case C190220_State.LEDIT:
            return "EDITING";
          case C190220_State.LEXEC:
            return "EXECUTING";
          case C190220_State.LKILL:
            return "KILLED";
          case C190220_State.LPAUSE:
            return "PAUSED";
          case C190220_State.LUNCOMPLETE:
            return "NOT COMPLETED";
          case C190220_State.LWAIT:
            return "WAITING";
          case C190220_State.SERVICE:
            return "SERVICE";
        }
      }
      return "";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
