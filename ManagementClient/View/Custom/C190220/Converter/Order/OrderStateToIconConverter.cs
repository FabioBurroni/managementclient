﻿using System;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model.Custom.C190220;
using Model.Custom.C190220.OrderManager;

namespace View.Custom.C190220.Converter
{
  public class OrderStateToIconConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      try
      {
        if (value != null)
        {
          C190220_State val = 0;
          if (!(value is C190220_State))
          {
           if(!(Enum.TryParse(value.ToString(),out val)))
           {
                return PackIconKind.About;
            }


          }
          else
          {
            val = (C190220_State)value;
          }

          switch (val)
          {
            case C190220_State.CDONE:
              return PackIconKind.HandOkay;//new Uri("/images/ok.png", UriKind.Relative);
            case C190220_State.CEXEC:
              return PackIconKind.Play;//new Uri("/images/play.png", UriKind.Relative);
            case C190220_State.CKILL:
              return PackIconKind.CancelBold;//new Uri("/images/nok.png", UriKind.Relative);
            case C190220_State.CPAUSE:
              return PackIconKind.Pause;//new Uri("/images/Pausa.gif", UriKind.Relative);
            case C190220_State.CWAIT:
              return PackIconKind.Hourglass;//new Uri("/images/wait.gif", UriKind.Relative);
            case C190220_State.LDONE:
              return PackIconKind.HandOkay;//new Uri("/images/ok.png", UriKind.Relative);
            case C190220_State.LEDIT:
              return PackIconKind.ModeEdit;//new Uri("/images/order.png", UriKind.Relative);
            case C190220_State.LEXEC:
              return PackIconKind.Play;//new Uri("/images/play.png", UriKind.Relative);
            case C190220_State.LKILL:
              return PackIconKind.CancelBold;//new Uri("/images/nok.png", UriKind.Relative);
            case C190220_State.LPAUSE:
              return PackIconKind.Pause;//new Uri("/images/Pausa.gif", UriKind.Relative);
            case C190220_State.LUNCOMPLETE:
              return PackIconKind.CallMissed;//new Uri("/images/tick.gif", UriKind.Relative);
            case C190220_State.LWAIT:
              return PackIconKind.Hourglass;//new Uri("/images/wait.gif", UriKind.Relative);
            case C190220_State.SERVICE:
              return PackIconKind.Warning;//new Uri("/images/warning.gif", UriKind.Relative);
          }
          return PackIconKind.AboutVariant;///???
        }
        return PackIconKind.About;///???
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        return PackIconKind.About;///???
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotSupportedException();
    }
  }
}
