﻿using System;
using System.Windows.Data;
using Model.Custom.C190220.OrderManager;

namespace View.Custom.C190220.Converter
{
  public class OrderStateToImageConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      if (value is C190220_State)
      {
        switch ((C190220_State)value)
        {
          case C190220_State.CDONE:
            return new Uri("/images/ok.png", UriKind.Relative);
          case C190220_State.CEXEC:
            return new Uri("/images/play.png", UriKind.Relative);
          case C190220_State.CKILL:
            return new Uri("/images/nok.png", UriKind.Relative);
          case C190220_State.CPAUSE:
            return new Uri("/images/Pausa.gif", UriKind.Relative);
            case C190220_State.CWAIT:
            return new Uri("/images/wait.gif", UriKind.Relative);
          case C190220_State.LDONE:
            return new Uri("/images/ok.png", UriKind.Relative);
          case C190220_State.LEDIT:
            return new Uri("/images/order.png", UriKind.Relative);
          case C190220_State.LEXEC:
            return new Uri("/images/play.png", UriKind.Relative);
          case C190220_State.LKILL:
            return new Uri("/images/nok.png", UriKind.Relative);
          case C190220_State.LPAUSE:
            return new Uri("/images/Pausa.gif", UriKind.Relative);
          case C190220_State.LUNCOMPLETE:
            return new Uri("/images/tick.gif", UriKind.Relative);
          case C190220_State.LWAIT:
            return new Uri("/images/wait.gif", UriKind.Relative);
          case C190220_State.SERVICE:
            return new Uri("/images/warning.gif", UriKind.Relative);
        }
      }
      return null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotSupportedException();
    }
  }
}
