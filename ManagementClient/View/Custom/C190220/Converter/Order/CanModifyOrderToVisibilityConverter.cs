﻿using System;
using System.Windows;
using System.Globalization;
using System.Windows.Data;
using Model.Custom.C190220.OrderManager;

namespace View.Custom.C190220.Converter
{
  public class CanModifyOrderToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is C190220_State)
      {
        if (((C190220_State)value) == C190220_State.LWAIT)
          return Visibility.Visible;
      }
      return Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
