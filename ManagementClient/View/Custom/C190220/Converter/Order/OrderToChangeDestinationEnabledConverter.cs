﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Model.Custom.C190220.OrderManager;

namespace View.Custom.C190220.Converter
{
  public class OrderToChangeDestinationEnabledConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value!=null && value is C190220_Order)
      {
        return ((C190220_Order)value).State == C190220_State.LWAIT;
      }

      return false;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
