﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Model.Custom.C190220;
using Model.Custom.C190220.OrderManager;


namespace View.Custom.C190220.Converter
{
  public class CmpToColorConverter : IMultiValueConverter
  {

    public Brush StateWait { get; set; } = Brushes.Orange;
    public Brush StateDone{ get; set; } = Brushes.Green;
    public Brush StateExecNearToEnd{ get; set; } = Brushes.Purple;
    public Brush StateExec{ get; set; } = Brushes.Blue;
    public Brush StateKill{ get; set; } = Brushes.Red;
    public Brush StateDefault{ get; set; } = Brushes.Black;


    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var cmp = value as C190220_OrderCmp;
      if(cmp!=null)
      {
        switch (cmp.State)
        {
          case C190220_State.CWAIT:
            return StateWait;
          case C190220_State.CEXEC:
            if(cmp.QtyRemaining<=0)
              return StateExecNearToEnd;
            else
              return StateExec;
          case C190220_State.CKILL:
            return StateKill;
          case C190220_State.CDONE:
            return StateDone;
          default:
            return StateDefault;
        }
      }
      return StateDefault;

    }

    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      if (values != null && values.Length == 2)
      {
        var cmp = values[0] as C190220_OrderCmp;

        if (cmp != null)
        {
          switch (cmp.State)
          {
            case C190220_State.CWAIT:
              return StateWait;
            case C190220_State.CEXEC:
              if (cmp.QtyRemaining <= 0)
                return StateExecNearToEnd;
              else
                return StateExec;
            case C190220_State.CKILL:
              return StateKill;
            case C190220_State.CDONE:
              return StateDone;
            default:
              return StateDefault;
          }
        }
      }
      return StateDefault;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
