﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
using Model.Custom.C190220.OrderManager;
namespace View.Custom.C190220.Converter
{
  public class CmpStateToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      Visibility ret = Visibility.Collapsed;
      string par = parameter as string;
      if (value is C190220_State)
      {
      
        switch((C190220_State)value)
        {
          case C190220_State.LDONE:
          case C190220_State.LEDIT:
          case C190220_State.LKILL:
          case C190220_State.LUNCOMPLETE:
            break;
          case C190220_State.LEXEC:
            if (par == "pause" || par=="kill" || par=="refresh")
              ret = Visibility.Visible;
            break;
          case C190220_State.LWAIT:
            if (par == "kill" || par=="exec")
              ret = Visibility.Visible;
            break;
          case C190220_State.LPAUSE:
            if (par == "exec" || par == "kill" || par == "refresh")
              ret = Visibility.Visible;
            break;

          case C190220_State.CDONE:
          case C190220_State.CKILL:
          case C190220_State.CPAUSE:
            break;
          case C190220_State.CEXEC:
          case C190220_State.CWAIT:
            if (par == "kill")
              ret = Visibility.Visible;
            break;
        }
      }
      return ret;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
