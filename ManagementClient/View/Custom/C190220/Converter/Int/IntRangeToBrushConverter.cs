﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows;

namespace View.Custom.C190220.Converter
{
  public class IntRangeToBrushConverter : IValueConverter
  {


    private int _Level1;
    public int Level1
    {
      get { return _Level1; }
      set
      {
        if (value != _Level1)
        {
          _Level1 = value;
        }
      }
    }

    private int _Level2;
    public int Level2
    {
      get { return _Level2; }
      set
      {
        if (value != _Level2)
        {
          _Level2 = value;
        }
      }
    }

    private Brush _DefaultColor = Brushes.Black;
    public Brush DefaultColor
    {
      get { return _DefaultColor; }
      set
      {
        if (value != _DefaultColor)
        {
          _DefaultColor = value;
        }
      }
    }

    private Brush _Color1 = Brushes.Red;
    public Brush Color1
    {
      get { return _Color1; }
      set
      {
        if (value != _Color1)
        {
          _Color1 = value;
          
        }
      }
    }



    private Brush _Color2 = Brushes.Orange;
    public Brush Color2
    {
      get { return _Color2; }
      set
      {
        if (value != _Color2)
        {
          _Color2 = value;
        }
      }
    }


    private Brush _Color3 = Brushes.DarkGreen;
    public Brush Color3
    {
      get { return _Color3; }
      set
      {
        if (value != _Color3)
        {
          _Color3 = value;
        }
      }
    }


    

   

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      


       if (value is int)
      {
        if (((int)value) <= _Level1)
          return _Color1;
        else if (((int)value) > _Level1 && ((int)value) <=_Level2)
          return _Color2;
        else if (((int)value) > _Level2)
          return _Color3;
      }

      return _DefaultColor;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
