﻿using Model.Custom.C190220.Report;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace View.Custom.C190220.Converter
{
  public class IntRangeGiacToBrushConverter : IValueConverter
  {

    private Brush _DefaultColor = Brushes.Black;
    public Brush DefaultColor
    {
      get { return _DefaultColor; }
      set
      {
        if (value != _DefaultColor)
        {
          _DefaultColor = value;
        }
      }
    }

    private Brush _Color1 = Brushes.Red;
    public Brush Color1
    {
      get { return _Color1; }
      set
      {
        if (value != _Color1)
        {
          _Color1 = value;
          
        }
      }
    }



    private Brush _Color2 = Brushes.Orange;
    public Brush Color2
    {
      get { return _Color2; }
      set
      {
        if (value != _Color2)
        {
          _Color2 = value;
        }
      }
    }


    private Brush _Color3 = Brushes.DarkGreen;
    public Brush Color3
    {
      get { return _Color3; }
      set
      {
        if (value != _Color3)
        {
          _Color3 = value;
        }
      }
    }


    

   

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
       if (value is C200138_StockMinMax)
      {
        C200138_StockMinMax giac = (C200138_StockMinMax)value; 

        if (giac.Stock <= giac.Min)
          return _Color1;
        else if (giac.Stock > giac.Min && giac.Stock <= giac.Max)
          return _Color2;
        else if (giac.Stock > giac.Min)
          return _Color3;
      }

      return _DefaultColor;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
