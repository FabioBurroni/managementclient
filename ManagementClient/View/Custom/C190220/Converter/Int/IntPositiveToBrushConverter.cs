﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace View.Custom.C190220.Converter
{
  public class IntPositiveToBrushConverter : IValueConverter
  {
    private Brush _PositiveBrush = Brushes.Green;

    public Brush PositiveBrush
    {
      get { return _PositiveBrush; }
      set { _PositiveBrush = value; }
    }

    private Brush _DefaultBrush = Brushes.Red;

    public Brush DefaultBrush
    {
      get { return _DefaultBrush; }
      set { _DefaultBrush = value; }
    }

    private int _trigger = 0;

    public int Trigger
    {
      get { return _trigger; }
      set { _trigger = value; }
    }





    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is int)
        if ((int)value > _trigger)
          return _PositiveBrush;
      return _DefaultBrush;

    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
