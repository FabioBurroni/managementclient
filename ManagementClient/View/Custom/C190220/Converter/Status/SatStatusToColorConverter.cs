﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using Model.Custom.C190220.WhStatus;

namespace View.Custom.C190220.Converter
{
  public class SatStatusToColorConverter : IValueConverter
  {

    public bool IsForeground { get; set; } = true;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is C190220_SatelliteStatusEnum)
      {
        var status = (C190220_SatelliteStatusEnum)value;

        switch (status)
        {
          case C190220_SatelliteStatusEnum.PLAY:
            return IsForeground ? Brushes.White : Brushes.Green;
          case C190220_SatelliteStatusEnum.STOP:
            return IsForeground ? Brushes.Yellow: Brushes.Red;
          case C190220_SatelliteStatusEnum.ERROR:
            return IsForeground ? Brushes.Yellow : Brushes.Red;
          case C190220_SatelliteStatusEnum.RESTORE:
            return IsForeground ? Brushes.Yellow : Brushes.Red;
          case C190220_SatelliteStatusEnum.RESET:
            return IsForeground ? Brushes.Yellow : Brushes.Red;
          case C190220_SatelliteStatusEnum.CHARGING:
            return IsForeground ? Brushes.Yellow : Brushes.Orange;
          case C190220_SatelliteStatusEnum.PARKING:
            return IsForeground ? Brushes.White: Brushes.Blue;
          case C190220_SatelliteStatusEnum.MANUAL:
            return IsForeground ? Brushes.White : Brushes.Gray;
          default:
            return IsForeground ? Brushes.White : Brushes.Gray;
        }
      }
        return IsForeground ? Brushes.White : Brushes.Gray;

    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
