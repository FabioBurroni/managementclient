﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows;

namespace View.Custom.C190220.Converter
{
  public class ConvBoolToVis2 : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is bool)
      {
        if(parameter is string && ((string)parameter)=="inverted")
          return ((bool)value) ? Visibility.Collapsed: Visibility.Visible;
        return ((bool)value) ? Visibility.Visible : Visibility.Collapsed;


      }
      return Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
