﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Utilities.Extensions;
using Model;
using Model.Common.Configuration;
using Model.Custom.C190220;
using View.Common.Configuration;
using MaterialDesignThemes.Wpf;
using MaterialDesignExtensions.Controls;

namespace View.Custom.C190220.Configuration
{
  /// <summary>
  /// Interaction logic for CtrlConfigurations.xaml
  /// </summary>
  public partial class CtrlConfigurations : CtrlBaseC190220
  {

    #region Costruttore
    public CtrlConfigurations()
    {
      InitializeComponent();


      Init();
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitleConfPar.Text = Context.Instance.TranslateDefault((string)txtBlkTitleConfPar.Tag);
      txtBlkTitlePallRed.Text = Context.Instance.TranslateDefault((string)txtBlkTitlePallRed.Tag);

      txtBlkHeaderMaintenance.Text = Context.Instance.TranslateDefault((string)txtBlkHeaderMaintenance.Tag);
      txtBlkPalletCode.Text = Context.Instance.TranslateDefault((string)txtBlkPalletCode.Tag);
      txtBlkDestination.Text = Context.Instance.TranslateDefault((string)txtBlkDestination.Tag);
      butRededtinateConfirm.Content = Context.Instance.TranslateDefault((string)butRededtinateConfirm.Tag);

      //txtBlkHeaderConfig.Text = Context.Instance.TranslateDefault((string)txtBlkHeaderConfig.Tag);


      //CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region FIELDS
    List<ConfParameterClient> _paramList = new List<ConfParameterClient>(); 
    #endregion

    private void Init()
    {

      #region SHAPE CONTROL
      #region P3_ShapeCtrlGoOnIfNoAreaAvailable
      ConfParameterClient p = new ConfParameterBool()
      {
        Title = "SHAPE CONTROL",
        Name = "P13_ShapeCtrlGoOnIfNoAreaAvailable",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "P13 " + "SHAPECONTROL GO ON IF NO AREA IS AVAILABLE",
        Value = false,
        DefaultValue = "True"
      };
      _paramList.Add(p);
      #endregion 
      #endregion

      #region SELECT WH CELL

      #region SelectWhCell_LogDb
      p = new ConfParameterBool()
      {
        Title = "SELECT WAREHOUSE CELL LOG DB",
        Name = "SelectWhCell_LogDb",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "LOG IN DATABASE DATA ABOUT SELECT WH CELL",
        Value = true,
        DefaultValue = "true"
      };
      _paramList.Add(p);
      #endregion

      #region SelectWhCell_LogFile
      p = new ConfParameterBool()
      {
        Title = "SELECT WAREHOUSE CELL LOG FILE",
        Name = "SelectWhCell_LogFile",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "LOG IN FILE DETAILS ABOUT SELECT WH CELL",
        Value = true,
        DefaultValue = "true"
      };
      _paramList.Add(p);
      #endregion

      #region SelectWhCell_MaxPalletInTransitForMainArea
      p = new ConfParameterInt()
      {
        Title = "MAX PALLET IN TRANSIT FOR MAIN AREA",
        Name = "SelectWhCell_MaxPalletInTransitForMainArea",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "MAXIMUM LEVEL OF TRAFFIC TO BE CONSIDERED LOW",
        Value = 10,
        DefaultValue = "10"
      };
      _paramList.Add(p);
      #endregion

      #region SelectWhCell_ExtraPalletInTransitForMainArea
      p = new ConfParameterInt()
      {
        Title = "EXTRA PALLET IN TRANSIT FOR MAIN AREA",
        Name = "SelectWhCell_ExtraPalletInTransitForMainArea",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "EXTRA LEVEL OF TRAFFIC TO BE CONSIDERED MEDIUM",
        Value = 3,
        DefaultValue = "3"
      };
      _paramList.Add(p);
      #endregion

      #region SelectWhCell_MaxPalletInTransitForLocalArea
      p = new ConfParameterInt()
      {
        Title = "MAX PALLET IN TRANSIT FOR LOCAL AREA",
        Name = "SelectWhCell_MaxPalletInTransitForLocalArea",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "MAXIMUM LEVEL OF TRAFFIC TO BE CONSIDERED LOW",
        Value = 3,
        DefaultValue = "3"
      };
      _paramList.Add(p);
      #endregion

      #region SelectWhCell_ExtraPalletInTransitForLocalArea
      p = new ConfParameterInt()
      {
        Title = "EXTRA PALLET IN TRANSIT FOR LOCAL AREA",
        Name = "SelectWhCell_ExtraPalletInTransitForLocalArea",
        Icon = PackIconKind.DistributeVerticalTop.ToString(),

        Descr = "EXTRA LEVEL OF TRAFFIC TO BE CONSIDERED MEDIUM",
        Value = 3,
        DefaultValue = "3"
      };
      _paramList.Add(p);
      #endregion
      #endregion

      #region ORDER MANAGER

      #region LogOrderManager
      p = new ConfParameterBool()
      {
        Title = "LOG ORDER MANAGER",
        Name = "LogOrderManager",
        Icon = PackIconKind.ViewList.ToString(),
        Descr = "LOG ORDER MANAGER",
        Value = false,
        DefaultValue = "False"
      };
      _paramList.Add(p);
      #endregion

      #region OrderManager_MaxPalletsForJob
      p = new ConfParameterInt()
      {
        Title = "ORDER MANAGER MAX PALLETS FOR JOB",
        Name = "OrderManager_MaxPalletsForJob",
        Icon = PackIconKind.Package.ToString(),

        Descr = "MAX PALLETS FOR JOB",
        Value = 1,
        DefaultValue = "2"
      };
      _paramList.Add(p);
      #endregion

      #region OrderManager_DestinationAssignement
      p = new ConfParameterBool()
      {
        Title = "ORDER AUTOMATIC DESTINATION ASSIGNEMENT",
        Name = "OrderManager_DestinationAssignement",
        Icon = PackIconKind.Truck.ToString(),

        Descr = "AUTOMATIC DESTINATION ASSIGNEMENT FOR WAITING ORDERS.",
        Value = true,
        DefaultValue = true
      };
      _paramList.Add(p);
      #endregion

      #region OrderManager_SortByWorkLoad
      p = new ConfParameterBool()
      {
        Title = "ORDER MANAGER SORTO BY WORK LOAD",
        Name = "OrderManager_SortByWorkLoad",
        Icon = PackIconKind.ViewList.ToString(),
        Descr = "SORT PALLET FOR ORDER TO OPTIMIZE MASTER WORKLOAD",
        Value = false,
        DefaultValue = "False"
      };
      _paramList.Add(p);
      #endregion

      #region OrderManager_GlobalFifoWindow
      p = new ConfParameterInt()
      {
        Title = "ORDER MANAGER GLOBAL FIFO WINDOW",
        Name = "OrderManager_GlobalFifoWindow",
        Icon = PackIconKind.Package.ToString(),

        Descr = "FifoWindow for all articles when executing order to maximize speed".ToUpper(),
        Value = 1,
        DefaultValue = "1"
      };
      _paramList.Add(p);
      #endregion

      #region Master_MaxWorkLoad
      p = new ConfParameterInt()
      {
        Title = "MASTER MAX WORK LOAD",
        Name = "Master_MaxWorkLoad",
        Icon = PackIconKind.Truck.ToString(),

        Descr = "MASTER MAX WORK LOAD",
        Value = 5,
        DefaultValue = "5"
      };
      _paramList.Add(p);
      #endregion
      #endregion

      #region PLUGIN MASTER MANAGER

      #region MASTERMANAGER LONGCHARGE REFRESH SECONDS
      p = new ConfParameterInt()
      {
        Title = "MASTERMANAGER LONGCHARGE REFRESH SECONDS",
        Name = "MasterManager_LongChargeRefreshSeconds",
        Icon = PackIconKind.Battery.ToString(),

        Descr = "REFRESH SECONDS",
        Value = 20,
        DefaultValue = 20
      };
      _paramList.Add(p);
      #endregion

      #region MASTER MANAGER REQUEST OPENING GATE REFRESH SECONDS
      p = new ConfParameterInt()
      {
        Title = "MASTER MANAGER REQUEST OPENING GATE REFRESH SECONDS",
        Name = "MasterManager_RequestOpeningGateRefreshSeconds",
        Icon = PackIconKind.DoorOpen.ToString(),

        Descr = "REFRESH SECONDS",
        Value = 20,
        DefaultValue = 20
      };
      _paramList.Add(p);
      #endregion

      #endregion
     
      #region TestMode
      p = new ConfParameterBool()
      {
        Title = "TEST MODALITY",
        Name = "TestMode",
        Icon = PackIconKind.TestTube.ToString(),

        Descr = "WORK MODALITY",
        Value = false,
        DefaultValue = "false"
      };
      _paramList.Add(p);
      #endregion

      //... WEBCAM
      #region WEB CAM
      p = new ConfParameterBool()
      {
        Title = "LOG WEB CAM PLUGIN",
        Name = "WebCam_Log",
        Icon = PackIconKind.WebCamera.ToString(),
        Descr = "LOG WEB CAM PLUGIN",
        Value = false,
        DefaultValue = "True"
      };
      _paramList.Add(p);

      p = new ConfParameterInt()
      {
        Title = "WEB CAM PLUGIN INACTIVITY TIME",
        Name = "WebCam_InictivityTimeS",
        Icon = PackIconKind.WebCamera.ToString(),

        Descr = "WEB CAM PLUGIN INACTIVITY TIME",
        Value = 20,
        DefaultValue = "20"
      };
      _paramList.Add(p);

      #endregion


      foreach (var parameter in _paramList)
      {
        if (parameter.ParamType == typeof(string))
        {
          CtrlConfStringParam ctrl = new CtrlConfStringParam();
          ctrl.ConfParameter = parameter;
          ctrl.OnCancel += Ctrl_OnCancel;
          ctrl.OnConfirm += Ctrl_OnConfirm;
          wp.Children.Add(ctrl);
        }
        else if (parameter.ParamType == typeof(int))
        {
          CtrlConfIntParam ctrl = new CtrlConfIntParam();
          ctrl.ConfParameter = parameter;
          ctrl.OnCancel += Ctrl_OnCancel;
          ctrl.OnConfirm += Ctrl_OnConfirm;
          wp.Children.Add(ctrl);
        }
        else if (parameter.ParamType == typeof(bool))
        {
          CtrlConfCheckParameter ctrl = new CtrlConfCheckParameter();
          ctrl.ConfParameter = parameter;
          ctrl.OnCancel += Ctrl_OnCancel;
          ctrl.OnConfirm += Ctrl_OnConfirm;
          wp.Children.Add(ctrl);
        }
        else if (parameter.ParamType.IsEnum)
        {
          CtrlConfComboParameter ctrl = new CtrlConfComboParameter();
          ctrl.ConfParameter = parameter;
          ctrl.OnCancel += Ctrl_OnCancel;
          ctrl.OnConfirm += Ctrl_OnConfirm;
          wp.Children.Add(ctrl);
        }
      }

    }


    #region COMANDI E RISPOSTE
    private void Cmd_MM_RedestinatePallet(string palletCode, string positionCode)
    {
      CommandManagerC190220.MM_RedestinatePallet(this, palletCode, positionCode);
    }
    private void MM_RedestinatePallet(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      C190220_CustomResult cr = dwr.Result.ConvertTo<C190220_CustomResult>();
      MessageBox.Show(cr.ToString());


    }


    private void Cmd_CO_Configuration_GetAll()
    {
      CommandManagerC190220.CO_Configuration_GetAll(this);
    }
    private void CO_Configuration_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var listNew = dwr.Data as List<ConfParameterServer>;
      if (listNew != null)
      {
        foreach (var pNew in listNew)
        {
          var confParameter = _paramList.FirstOrDefault(p => p.Name == pNew.Name);
          if (confParameter != null)
          {
            confParameter.Update(pNew);
          }
        }
      }
    }

    private void Cmd_CO_Configuration_Get(string shippingLaneCode)
    {
      CommandManagerC190220.CO_Configuration_Get(this, shippingLaneCode);
    }
    private void CO_Configuration_Get(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var pNew = dwr.Data as ConfParameterServer;
      if (pNew != null)
      {
        var confParameter = _paramList.FirstOrDefault(p => p.Name == pNew.Name);
        if (confParameter != null)
        {
          confParameter.Update(pNew);
        }
      }
    }

    private void Cmd_CO_Configuration_Set(string parameterName, string parameterValue)
    {
      CommandManagerC190220.CO_Configuration_Set(this, parameterName, parameterValue);
    }

    private void CO_Configuration_Set(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      C190220_CustomResult result = dwr.Result.ConvertTo<C190220_CustomResult>();
      if (result == C190220_CustomResult.OK)
      {

      }
      else
      {

      }

      MessageBox.Show(result.ToString());

      Cmd_CO_Configuration_Get(commandMethodParameters[1]);
    }

    #endregion

    #region eventi
    private void Ctrl_OnConfirm(ConfParameterClient confPar)
    {
      Cmd_CO_Configuration_Set(confPar.Name, confPar.SelectedValue);
    }

    private void Ctrl_OnCancel(ConfParameterClient confPar)
    {
      Cmd_CO_Configuration_Get(confPar.Name);

    }

    private void CtrlBaseLoaded(object sender, RoutedEventArgs e)
    {
      Cmd_CO_Configuration_GetAll();
    }


    public override void Closing()
    {

    }
    #endregion

    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Cmd_CO_Configuration_GetAll();
      Translate();
    }


    #region REDESTINATE PALLET
    private async void butRededtinateConfirm_Click(object sender, RoutedEventArgs e)
    {
      string palletCode = txtRedestinatePalletCode.Text;
      if(string.IsNullOrEmpty(palletCode))
      {
        AlertDialogArguments dialogArgs = new AlertDialogArguments
        {
          Title = "Error on redestination request",
          Message = "Specify Pallet Code",
          OkButtonLabel = "GOT IT"
        };

        await AlertDialog.ShowDialogAsync("RootDialog", dialogArgs);

        return;
      }

      var si = cbRedestinateDestination.SelectedItem;
      if(si==null)
      {
        AlertDialogArguments dialogArgs = new AlertDialogArguments
        {
          Title = "Error on redestination request",
          Message = "Specify Pallet Destination",
          OkButtonLabel = "GOT IT"
        };

        await AlertDialog.ShowDialogAsync("RootDialog", dialogArgs);

        return;
      }

      var cbi = si as System.Windows.Controls.ComboBoxItem;
      string positionCode = cbi.Content.ToString();

      if(string.IsNullOrEmpty(positionCode))
      {
        AlertDialogArguments dialogArgs = new AlertDialogArguments
        {
          Title = "Error on redestination request",
          Message = "Specify Pallet Destination",
          OkButtonLabel = "GOT IT"
        };

        await AlertDialog.ShowDialogAsync("RootDialog", dialogArgs);

        return;
      }

      Cmd_MM_RedestinatePallet(palletCode, positionCode);

      


    }
    #endregion

    private void tiConfParammeters_Loaded(object sender, RoutedEventArgs e)
    {
      Cmd_CO_Configuration_GetAll();
    }
  }
}
