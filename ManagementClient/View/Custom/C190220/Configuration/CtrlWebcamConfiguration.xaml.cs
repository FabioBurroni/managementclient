﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Model;
using Model.Custom.C190220;
using Model.Custom.C190220.WebCam;
using Utilities.Extensions;
using View.Common.Languages;

namespace View.Custom.C190220.Configuration
{
  /// <summary>
  /// Interaction logic for CtrlPalletTracking.xaml
  /// </summary>
  public partial class CtrlWebCamConfiguration : CtrlBaseC190220
  {
    public ObservableCollectionFast<C190220_WebCam> WebCamL { get; set; } = new ObservableCollectionFast<C190220_WebCam>();

    #region COSTRUTTORE

    public CtrlWebCamConfiguration()
    {      
      InitializeComponent();

    }

    #endregion COSTRUTTORE

    #region LOCALIZATION

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkDescriptionHeader.Text = Context.Instance.TranslateDefault((string)txtBlkDescriptionHeader.Tag);
      //...refresh  button tooltip
     
      ////...new printer button tooltip
      //txtBlkPrinterNewButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkPrinterNewButtonToolTip.Tag);
      //txtBlkClear.Text = Context.Instance.TranslateDefault((string)txtBlkClear.Tag);
      ////...clear button tooltip
      //txtBlkPrinterClearButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkPrinterClearButtonToolTip.Tag);
      //txtBlkConfirm.Text = Context.Instance.TranslateDefault((string)txtBlkConfirm.Tag);
      ////...confirm button tooltip
      //txtBlkPrinterConfirmButtonToolTip.Text = Context.Instance.TranslateDefault((string)txtBlkPrinterConfirmButtonToolTip.Tag);
    }

    #endregion 

    #region COMANDI E RISPOSTE
    private void Cmd_WCM_WebCam_Move(int webCamId, int preset)
    {
      CommandManagerC190220.WCM_WebCam_Move(this, webCamId, preset);
    }
    private void WCM_WebCam_Move(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C190220_CustomResult r = dwr.Data.ConvertTo<C190220_CustomResult>();
      MessageBox.Show(r.ToString());
    }
    private void Cmd_WCM_WebCam_GetAll()
    {
      WebCamL.Clear();
      CommandManagerC190220.WCM_WebCam_GetAll(this);
    }
    private void WCM_WebCam_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var list = dwr.Data as List<C190220_WebCam>;
      if (list != null)
      {
        list.ForEach(wc => wc.ResetChanged());
        WebCamL.AddRange(list);
      }
    }
   
    private void Cmd_WCM_WebCam_Get(int id)
    {
      CommandManagerC190220.WCM_WebCam_Get(this, id);
    }
    private void WCM_WebCam_Get(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      var wc = dwr.Data as C190220_WebCam;
      var webcam = WebCamL.FirstOrDefault(w => w.Id == wc.Id);

      webcam?.Update(wc);
      webcam.ResetChanged();
    }
    
    private void Cmd_WCM_WebCam_Update(C190220_WebCam wc)
    {
      CommandManagerC190220.WCM_WebCam_Update(this, wc);
    }
    private void WCM_WebCam_Update(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      if (dwr.Data == null) return;
      C190220_CustomResult r = dwr.Data.ConvertTo<C190220_CustomResult>();
     
      LocalSnackbar.ShowMessageInfo(r.ToString(), 3);

      int id = 0;
      if (int.TryParse(commandMethodParameters[1], out id))
      {
        Cmd_WCM_WebCam_Get(id);
      }
    }
    #endregion

    #region EVENTI
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
      Cmd_WCM_WebCam_GetAll();      
    }
    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      Cmd_WCM_WebCam_GetAll();
    }

    private void butWebCamCancel_Click(object sender, RoutedEventArgs e)
    {
      var b = sender as Button;
      if (b == null)
      {
        LocalSnackbar.ShowMessageInfo("PLEASE SELECT WEBCAM".TD(), 3);
        return;
      }

      var wc = b.Tag as C190220_WebCam;

      if (wc == null)
      {
        LocalSnackbar.ShowMessageInfo("PLEASE SELECT WEBCAM".TD(), 3);
        return;
      }

      Cmd_WCM_WebCam_Get(wc.Id);
    }

    private void butWebCamUpdate_Click(object sender, RoutedEventArgs e)
    {
      var b = sender as Button;
      if (b == null)
      {
        LocalSnackbar.ShowMessageInfo("PLEASE SELECT WEBCAM".TD(), 3);
        return;
      }

      var wc = b.Tag as C190220_WebCam;

      if (wc == null)
      {
        LocalSnackbar.ShowMessageInfo("PLEASE SELECT WEBCAM".TD(), 3);
        return;
      }

      Cmd_WCM_WebCam_Update(wc);

    }
    private void butWebCamMoveLeft_Click(object sender, RoutedEventArgs e)
    {
      var wc = (sender as Button).Tag as C190220_WebCam;
      if (wc != null) Cmd_WCM_WebCam_Move(wc.Id, wc.PresetLeft);
    }

    private void butWebCamMoveRight_Click(object sender, RoutedEventArgs e)
    {
      var wc = (sender as Button).Tag as C190220_WebCam;
      if (wc != null) Cmd_WCM_WebCam_Move(wc.Id, wc.PresetRight);
    }

    private void butWebCamMoveHome_Click(object sender, RoutedEventArgs e)
    {
      var wc = (sender as Button).Tag as C190220_WebCam;
      if (wc != null) Cmd_WCM_WebCam_Move(wc.Id, wc.PresetHome);
    }
    #endregion
  }
}