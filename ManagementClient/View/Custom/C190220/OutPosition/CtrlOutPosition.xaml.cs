﻿using System;
using System.Linq;
using Model.Attributes;
using Model;
using XmlCommunicationManager.Common.Xml;
using System.Collections.Generic;
using System.Windows;
using Model.Custom.C190220;
using Configuration;
using Utilities.Extensions;

namespace View.Custom.C190220.OutPosition
{
  /// <summary>
  /// Interaction logic for CtrlExitPosition.xaml
  /// </summary>
  public partial class CtrlOutPosition : CtrlBaseC190220
  {
    private bool refreshAvailable;

    public bool RefreshAvailable
    {
      get { return refreshAvailable; }
      set { refreshAvailable = value;
        NotifyPropertyChanged("RefreshAvailable");
      }
    }


    #region DP - TooManyPalletInPos

    public bool TooManyPalletInPos
    {
      get { return (bool)GetValue(TooManyPalletInPosProperty); }
      set { SetValue(TooManyPalletInPosProperty, value); }
    }

    // Using a DependencyProperty as the backing store for ToManyPalletInPos.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty TooManyPalletInPosProperty =
        DependencyProperty.Register("TooManyPalletInPos", typeof(bool), typeof(CtrlOutPosition), new PropertyMetadata(false));

    #endregion DP - TooManyPalletInPos

    #region DP - PalletInPos

    public C190220_PositionOut_Pallet PalletInPos
    {
      get { return (C190220_PositionOut_Pallet)GetValue(PalletInPosProperty); }
      set { SetValue(PalletInPosProperty, value); }
    }

    // Using a DependencyProperty as the backing store for PalletInPos.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty PalletInPosProperty =
        DependencyProperty.Register("PalletInPos", typeof(C190220_PositionOut_Pallet), typeof(CtrlOutPosition), new PropertyMetadata(null));

    #endregion DP - PalletInPos

    #region DP - PosCode

    public string PosCode
    {
      get { return (string)GetValue(PosCodeProperty); }
      set { SetValue(PosCodeProperty, value); }
    }

    // Using a DependencyProperty as the backing store for PosCode.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty PosCodeProperty = DependencyProperty.Register("PosCode", typeof(string), typeof(CtrlOutPosition), new PropertyMetadata(null));

    #endregion DP - PosCode

    #region Costruttore

    public CtrlOutPosition()
    {
      InitializeComponent();
      Cmd_EP_Get_ExitPositionCodes();
      Translate();
    }

    #endregion Costruttore

    #region TRADUZIONI

    protected override void Translate()
    {
      labTitle.Text = Context.Instance.TranslateDefault((string)labTitle.Tag);
      //txtBlkBoxCode.Text = Context.Instance.TranslateDefault((string)txtBlkBoxCode.Tag);
      icon_code_1.ToolTip = Context.Instance.TranslateDefault((string)icon_code_1.Tag);
      icon_weight_1.ToolTip = Context.Instance.TranslateDefault((string)icon_weight_1.Tag);
      icon_article_1.ToolTip = Context.Instance.TranslateDefault((string)icon_article_1.Tag);
      icon_reject_r.ToolTip = Context.Instance.TranslateDefault((string)icon_reject_r.Tag);
      icon_Result.ToolTip = Context.Instance.TranslateDefault((string)icon_Result.Tag);

      txtBlkLotCode.Text = Context.Instance.TranslateDefault((string)txtBlkLotCode.Tag);
      txtBlkLotLength.Text = Context.Instance.TranslateDefault((string)txtBlkLotLength.Tag);
      txtBlkOrderCode.Text = Context.Instance.TranslateDefault((string)txtBlkOrderCode.Tag);
      //txtBlkDestination.Text = Context.Instance.TranslateDefault((string)txtBlkDestination.Tag);
      txtBlkArticleCode.Text = Context.Instance.TranslateDefault((string)txtBlkArticleCode.Tag);
      txtBlkQuantityRequested.Text = Context.Instance.TranslateDefault((string)txtBlkQuantityRequested.Tag);
      txtBlkQuantityDelivered.Text = Context.Instance.TranslateDefault((string)txtBlkQuantityDelivered.Tag);
      txtBlkOrderLotCode.Text = Context.Instance.TranslateDefault((string)txtBlkOrderLotCode.Tag);
      txtBlkArticleDescription.Text = Context.Instance.TranslateDefault((string)txtBlkArticleDescription.Tag);
      txtBlkTooManyPalletInPosition.Text = Context.Instance.TranslateDefault((string)txtBlkTooManyPalletInPosition.Tag);
      txtBlkNoPalletInPosition.Text = Context.Instance.TranslateDefault((string)txtBlkNoPalletInPosition.Tag);
      txtBlkNoAssociatedOrder.Text = Context.Instance.TranslateDefault((string)txtBlkNoAssociatedOrder.Tag);
    }

    #endregion TRADUZIONI

    #region NOTIFICHE

    /// <summary>
    /// Cambio pallet in posizione
    /// </summary>
    /// <param name="xmlcmd"></param>
    [XmlNotification(BringToFront = false)]
    public void Notify_Pallet_OnPosition_Changed(XmlCommand xmlcmd)
    {
      Console.WriteLine(xmlcmd);

      //get parameters...
      var exitPositionPanelSettings = ConfigurationManager.Parameters.Layout.Where(x => x.Title == "Exit Position Panel").FirstOrDefault();
      bool enableAutoRefresh = exitPositionPanelSettings.Settings.Where(x => x.Name == "ExitPositionAutoRefresh").FirstOrDefault().Value.ConvertToBool();

      if (enableAutoRefresh || PalletInPos is null)
        Cmd_EP_Get_PalletInPos();
      else
        RefreshAvailable = true;      
    }

    #endregion NOTIFICHE

    #region COMANDI E RISPOSTE

    private void Cmd_EP_Get_ExitPositionCodes()
    {
      CommandManagerC190220.EP_Get_ExitPositionCodes(this, Context.Instance.Positions);
    }

    private void EP_Get_ExitPositionCodes(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var positionExitList = dwr.Data as List<string>;
      PosCode = positionExitList.FirstOrDefault();
      Refresh();
    }

    private void Cmd_EP_Get_PalletInPos()
    {
      if (PosCode != null)
      {
        CommandManagerC190220.EP_Get_PalletInPos(this, PosCode);
      }
    }

    private void EP_Get_PalletInPos(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var list = dwr.Data as List<C190220_PositionOut_Pallet>;
      TooManyPalletInPos = list.Count > 1;
           
      PalletInPos = list.FirstOrDefault();

      RefreshAvailable = false;
    }

    #endregion COMANDI E RISPOSTE

    private void Refresh()
    {
      Cmd_EP_Get_PalletInPos();
    }

    
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();

      if (PosCode == null)
        Cmd_EP_Get_ExitPositionCodes();
      else
        Refresh();
    }

    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      Refresh();
    }
  }
}