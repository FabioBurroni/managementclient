﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C190220.Report;
using Model.Export;
using View.Common.Languages;
using View.Common.Report;

namespace View.Custom.C190220.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlStockByCell.xaml
  /// </summary>
  public partial class CtrlStockByCell : CtrlBaseC190220
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public C190220_StockByCellFilter Filter { get; set; } = new C190220_StockByCellFilter();

    public ObservableCollectionFast<C190220_StockByCell> GiacL { get; set; } = new ObservableCollectionFast<C190220_StockByCell>();
    #endregion

    #region COSTRUTTORE
    public CtrlStockByCell()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlStockByCellFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colLotCode.Header = Localization.Localize.LocalizeCustomString("LOT CODE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colArticleDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colCellCode.Header = Localization.Localize.LocalizeDefaultString("CELL CODE");
      colMaxPalletAllowed.Header = Localization.Localize.LocalizeDefaultString("MAX PALLET ALLOWED");
      colNumberOfDays.Header = Localization.Localize.LocalizeDefaultString("NUMBER OF DAYS");
      colStockWindow.Header = Localization.Localize.LocalizeDefaultString("STOCK WINDOW");
      colPalletCount.Header = Localization.Localize.LocalizeDefaultString("PALLET COUNT");
      colPalletFirstProductionDate.Header = Localization.Localize.LocalizeDefaultString("FIRST PRODUCTION DATE");
      colPalletLastProductionDate.Header = Localization.Localize.LocalizeDefaultString("LAST PRODUCTION DATE");
      colSeniority.Header = Localization.Localize.LocalizeDefaultString("SENIORITY");

      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");

      CollectionViewSource.GetDefaultView(dgCells.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    private void Cmd_RM_StockByCell_GetAll()
    {
      busyOverlay.IsBusy = true;
      GiacL.Clear();
      CommandManagerC190220.RM_StockByCell_GetAll(this, Filter);
    }
    private void RM_StockByCell_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var gL = dwr.Data as List<C190220_StockByCell>;
      if (gL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (gL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        gL = gL.Take(Filter.MaxItems).ToList();
        //GiacL.AddRange(gL);
        GiacL.AddRange(gL.OrderBy(g => g.LotCode).ThenBy(g => g.ArticleCode).ThenBy(g => g.CellCode.Substring(0, 3)).ThenBy(g => g.Completa).ToList());
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }


    #endregion


    #region Eventi Ricerca

    private void ctrlStockByCellFilter_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;

      Cmd_RM_StockByCell_GetAll();
    }

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_RM_StockByCell_GetAll();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_RM_StockByCell_GetAll();
    }
    #endregion

    #region Eventi controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    #endregion


    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_StockByCell)
      {
        try
        {
          Clipboard.SetText((((C190220_StockByCell)b.Tag)).ArticleCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyCellCode_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_StockByCell)
      {
        try
        {
          Clipboard.SetText((((C190220_StockByCell)b.Tag)).CellCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }


    private void butCopyLot_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_StockByCell)
      {
        try
        {
          Clipboard.SetText((((C190220_StockByCell)b.Tag)).LotCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    string _lastArticleCode = string.Empty;
    Brush _lastBack = Brushes.White;
    BrushConverter _brushConverter = new BrushConverter();


    private void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
    {
      var g = e.Row.Item as C190220_StockByCell;
      if (g != null)
      {
        if (g.ArticleCode != _lastArticleCode)
        {
          Brush back = _lastBack == Brushes.White ? (Brush)_brushConverter.ConvertFrom("#FFE0E0E0") : Brushes.White;
          e.Row.Background = back;

          _lastBack = back;
          _lastArticleCode = g.ArticleCode;
        }
        else
        {
          e.Row.Background = _lastBack;
        }
      }
    }

    private async void butExport_Click(object sender, RoutedEventArgs e)
    {
      var view = new CtrlReportExport("CellStock", GiacL.Select(g => new Exportable_CellStock(g)).Cast<IExportable>().ToArray(), typeof(Exportable_CellStock));

      //show the dialog
      var result = await DialogHost.Show(view, "RootDialog");

      if (view.Result == ExportResult.OK)
        LocalSnackbar.ShowMessageOkAndOpenFile("EXPORT COMPLETED".TD() + " " + view.FileCreatedName, 5, "OPEN", view.FileCreatedName);
    }

  }

  public class Exportable_CellStock : IExportable
  {

    public Exportable_CellStock(C190220_StockByCell artIn)
    {
      Code = artIn.ArticleCode;
      Description = artIn.ArticleDescr;
      CellCode = artIn.CellCode.ToString();
      MaxPalletAllowed = artIn.MaxPalletAllowed.ToString();
      NumberOfDays = artIn.NumDays.ToString();
      StockWindow = artIn.FinestraStoccaggio.ToString();
      PalletCount = artIn.PalletCount.ToString();
      FirstProductionDate = artIn.PalletFirstProductionDate.ToString();
      LastProductionDate = artIn.PalletLastProductionDate.ToString();
      Seniority = artIn.NumDaysFromToday.ToString();
    }

    [Exportation(ColumnIndex = 0, ColumnName = "ARTICLE CODE")]
    public string Code { get; set; }

    [Exportation(ColumnIndex = 1, ColumnName = "DESCRIPTION")]
    public string Description { get; set; }

    [Exportation(ColumnIndex = 2, ColumnName = "CELL CODE")]
    public string CellCode { get; set; }

    [Exportation(ColumnIndex = 3, ColumnName = "MAX PALLET ALLOWED")]
    public string MaxPalletAllowed { get; set; }

    [Exportation(ColumnIndex = 4, ColumnName = "NUMBER OF DAYS")]
    public string NumberOfDays { get; set; }

    [Exportation(ColumnIndex = 5, ColumnName = "STOCK WINDOW")]
    public string StockWindow { get; set; }

    [Exportation(ColumnIndex = 6, ColumnName = "PALLET COUNT")]
    public string PalletCount { get; set; }

    [Exportation(ColumnIndex = 7, ColumnName = "FIRST PRODUCTION DATE")]
    public string FirstProductionDate { get; set; }

    [Exportation(ColumnIndex = 8, ColumnName = "LAST PRODUCTION DATE")]
    public string LastProductionDate { get; set; }

    [Exportation(ColumnIndex = 9, ColumnName = "SENIORITY")]
    public string Seniority { get; set; }
        
  }
}
