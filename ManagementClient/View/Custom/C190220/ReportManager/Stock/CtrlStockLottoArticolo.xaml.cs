﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C190220.Report;
using Model.Export;
using View.Common.Languages;
using View.Common.Report;

namespace View.Custom.C190220.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlStockLottoArticolo.xaml
  /// </summary>
  public partial class CtrlStockLottoArticolo : CtrlBaseC190220
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public C190220_StockLotArticleFilter Filter { get; set; } = new C190220_StockLotArticleFilter();

     public ObservableCollectionFast<C190220_StockLotArticle> GiacL { get; set; } = new ObservableCollectionFast<C190220_StockLotArticle>();

    #endregion

    #region COSTRUTTORE
    public CtrlStockLottoArticolo()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlStockFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag); 
      
      colLotCode.Header = Localization.Localize.LocalizeDefaultString("LOT CODE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colArticleDescription.Header = Localization.Localize.LocalizeDefaultString("ARTICLE DESCRIPTION");
      colStockTotale.Header = Localization.Localize.LocalizeDefaultString("TOTAL STOCK");

      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");

      CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    private void Cmd_RM_Stock_LotArticle()
    {
      busyOverlay.IsBusy = true;
      GiacL.Clear();
      CommandManagerC190220.RM_Stock_LotArticle(this, Filter);
    }
    private void RM_Stock_LotArticle(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      var giacArtL = dwr.Data as List<C190220_StockLotArticle>;
      if (giacArtL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (giacArtL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        giacArtL = giacArtL.Take(Filter.MaxItems).ToList();
        GiacL.AddRange(giacArtL);

      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

    #endregion

    #region Eventi controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    private void CtrlBaseC190220_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    #endregion


    #region Eventi Ricerca
    private void ctrlStockFilter_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;
      ctrlStockFilter.SearchHighlight = false;

      Cmd_RM_Stock_LotArticle();
    }
    #endregion

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_RM_Stock_LotArticle();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_RM_Stock_LotArticle();
    }

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_StockLotArticle)
      {
        try
        {
          Clipboard.SetText((((C190220_StockLotArticle)b.Tag)).ArticleCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyLot_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_StockLotArticle)
      {
        try
        {
          Clipboard.SetText((((C190220_StockLotArticle)b.Tag)).LotCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private async void butExport_Click(object sender, RoutedEventArgs e)
    {
      var view = new CtrlReportExport("ArticleStock", GiacL.Select(g => new Exportable_ArticleStock(g)).Cast<IExportable>().ToArray(), typeof(Exportable_ArticleStock));

      //show the dialog
      var result = await DialogHost.Show(view, "RootDialog");

      if (view.Result == ExportResult.OK)
        LocalSnackbar.ShowMessageOkAndOpenFile("EXPORT COMPLETED".TD() + " " + view.FileCreatedName, 5, "OPEN", view.FileCreatedName);
    }
  }

  public class Exportable_ArticleStock : IExportable
  {

    public Exportable_ArticleStock(C190220_StockLotArticle artLotIn)
    {
      LotCode = artLotIn.LotCode;
      ArticleCode = artLotIn.ArticleCode;
      ArticleDescription = artLotIn.ArticleDescr;
      StockTotale = artLotIn.StockTotale.ToString();
    }

    [Exportation(ColumnIndex = 0, ColumnName = "LOT CODE")]
    public string LotCode { get; set; }

    [Exportation(ColumnIndex = 1, ColumnName = "ARTICLE CODE")]
    public string ArticleCode { get; set; }

    [Exportation(ColumnIndex = 2, ColumnName = "DESCRIPTION")]
    public string ArticleDescription { get; set; }

    [Exportation(ColumnIndex = 3, ColumnName = "TOTAL STOCK")]
    public string StockTotale { get; set; }

   

  }
}