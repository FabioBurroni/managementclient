﻿using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C190220.Report;
using Model.Custom.C190220.Report.Stock;
using Model.Export;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Common.Languages;
using View.Common.Report;

namespace View.Custom.C190220.ReportManager.Stock
{
    /// <summary>
    /// Interaction logic for CtrlExpireSoon.xaml
    /// </summary>
    public partial class CtrlExpireSoon : CtrlBaseC190220
    {
        #region PRIVATE FIELDS
        private const int _indexStartValue = 0;
        private const int _maxItemsStartValue = 100;
        #endregion

        #region Public Properties
        public C190220_StockLotArticleFilter Filter { get; set; } = new C190220_StockLotArticleFilter();

        public ObservableCollectionFast<C190220_SoonExpire> GiacL { get; set; } = new ObservableCollectionFast<C190220_SoonExpire>();

        #endregion

        #region CONSTRUCTOR
        public CtrlExpireSoon()
        {
            Filter.Index = _indexStartValue;
            Filter.MaxItems = _maxItemsStartValue;
            InitializeComponent();
            Filter = ctrlSoonExpireFilter.Filter;
        }
        #endregion

        #region TRADUZIONI

        protected override void Translate()
        {
            txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);

            colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
            colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
            colCellCode.Header = Localization.Localize.LocalizeDefaultString("CELL CODE");
            colProductionDate.Header = Localization.Localize.LocalizeDefaultString("PRODUCTION DATE");
            colExpiryDate.Header = Localization.Localize.LocalizeDefaultString("EXPIRY DATE");
            colDaysLeft.Header = Localization.Localize.LocalizeCustomString("DAYS LEFT");
            colQuantity.Header = Localization.Localize.LocalizeDefaultString("QUANTITY");

            txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

            txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
            butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
            butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
            butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");

            CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
        }

        #endregion TRADUZIONI


        #region COMANDI E RISPOSTE
        private void Cmd_RM_Soon_Expire()
        {
            busyOverlay.IsBusy = true;
            GiacL.Clear();
            CommandManagerC190220.RM_Soon_Expire(this, Filter);
        }
        private void RM_Soon_Expire(IList<string> commandMethodParameters, IModel model)
        {
            busyOverlay.IsBusy = false;
            var dwr = model as DataWrapperResult;
            if (dwr == null) return;

            var palletL = dwr.Data as List<C190220_SoonExpire>;
            if (palletL != null)
            {
                if (Filter.Index == _indexStartValue)
                    butPrev.IsEnabled = false;
                else
                    butPrev.IsEnabled = true;

                if (palletL.Count > Filter.MaxItems)
                    butNext.IsEnabled = true;
                else
                    butNext.IsEnabled = false;

                palletL = palletL.Take(Filter.MaxItems).ToList();
                GiacL.AddRange(palletL);

            }
            else
            {
                butPrev.IsEnabled = false;
                butNext.IsEnabled = false;
            }
        }

        #endregion

        #region Eventi controllo
        private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }
            Translate();
        }

        private void CtrlBaseC190220_Unloaded(object sender, RoutedEventArgs e)
        {

        }
        #endregion


        #region Eventi Ricerca
        private void ctrlSoonExpireFilter_OnSearch()
        {
            gridInfoCentral.Visibility = Visibility.Collapsed;
            ctrlSoonExpireFilter.SearchHighlight = false;

            Cmd_RM_Soon_Expire();
        }
        #endregion

        private void butPrev_Click(object sender, RoutedEventArgs e)
        {
            if (Filter.Index > _indexStartValue)
                Filter.Index--;
            Cmd_RM_Soon_Expire();
        }

        private void butNext_Click(object sender, RoutedEventArgs e)
        {
            Filter.Index++;
            Cmd_RM_Soon_Expire();
        }

        private void butCopy_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            if (b.Tag is C190220_StockLotArticle)
            {
                try
                {
                    Clipboard.SetText((((C190220_StockLotArticle)b.Tag)).ArticleCode);

                    LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + Localization.Localize.LocalizeCustomString("COPIED"), 1);
                }
                catch
                {

                }
            }
        }

        private void butCopyLot_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            if (b.Tag is C190220_StockLotArticle)
            {
                try
                {
                    Clipboard.SetText((((C190220_StockLotArticle)b.Tag)).LotCode);

                    LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + Localization.Localize.LocalizeCustomString("COPIED"), 1);
                }
                catch
                {

                }
            }
        }

        private async void butExport_Click(object sender, RoutedEventArgs e)
        {
            var view = new CtrlReportExport("Soon Expire", GiacL.Select(g => new Exportable_ArticleStock(g)).Cast<IExportable>().ToArray(), typeof(Exportable_ArticleStock));

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog");

            if (view.Result == ExportResult.OK)
                LocalSnackbar.ShowMessageOkAndOpenFile(Localization.Localize.LocalizeCustomString("EXPORT COMPLETED") 
                + " " + view.FileCreatedName, 5, Localization.Localize.LocalizeCustomString("OPEN"), view.FileCreatedName);
        }
    }

    public class Exportable_ArticleStock : IExportable
    {

        public Exportable_ArticleStock(C190220_SoonExpire pallet)
        {
            PalletCode = pallet.Code;
            ArticleCode = pallet.ArticleCode;
            CellCode = pallet.CellCode;
            ProductionDate = pallet.DateBorn.ToString();
            ExpiryDate = pallet.DateExpiry.ToString();
            DaysLeft = pallet.DaysLeft.ToString();
            Quantity = pallet.Qty.ToString();
        }

        [Exportation(ColumnIndex = 0, ColumnName = "PALLET CODE")]
        public string PalletCode { get; set; }

        [Exportation(ColumnIndex = 1, ColumnName = "ARTICLE CODE")]
        public string ArticleCode { get; set; }

        [Exportation(ColumnIndex = 2, ColumnName = "CELL CODE")]
        public string CellCode { get; set; }

        [Exportation(ColumnIndex = 3, ColumnName = "PRODUCTION DATE")]
        public string ProductionDate { get; set; }

        [Exportation(ColumnIndex = 4, ColumnName = "EXPIRY DATE")]
        public string ExpiryDate { get; set; }

        [Exportation(ColumnIndex = 5, ColumnName = "DAYS LEFT")]
        public string DaysLeft { get; set; }

        [Exportation(ColumnIndex = 6, ColumnName = "QUANTITY")]
        public string Quantity { get; set; }


    }
}
