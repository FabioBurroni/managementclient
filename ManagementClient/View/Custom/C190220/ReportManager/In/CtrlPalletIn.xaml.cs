﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C190220.Report;
using Model.Export;
using View.Common.Languages;
using View.Common.Report;

namespace View.Custom.C190220.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletIn.xaml
  /// </summary>
  public partial class CtrlPalletIn : CtrlBaseC190220
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public C190220_PalletInFilter Filter { get; set; } = new C190220_PalletInFilter();

    public ObservableCollectionFast<C190220_PalletIn> GiacL { get; set; } = new ObservableCollectionFast<C190220_PalletIn>();
    #endregion

    #region COSTRUTTORE
    public CtrlPalletIn()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlPalletInFilterHorizontal.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colLotCode.Header = Localization.Localize.LocalizeDefaultString("LOT CODE");
      colArticleCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colData.Header = Localization.Localize.LocalizeDefaultString("DATE");
      colPalletCode.Header = Localization.Localize.LocalizeDefaultString("PALLET CODE");
      colPosition.Header = Localization.Localize.LocalizeDefaultString("POSITION");
      colCellCode.Header = Localization.Localize.LocalizeDefaultString("CELL CODE");
      colQuantity.Header = Localization.Localize.LocalizeCustomString("QUANTITY");
      colLotLength.Header = Localization.Localize.LocalizeCustomString("LOT LENGTH");
      colExpiryDate.Header = Localization.Localize.LocalizeCustomString("EXPIRY DATE");

      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");

      CollectionViewSource.GetDefaultView(dgPallet.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI

    #region COMANDI E RISPOSTE
    private void Cmd_RM_Pallet()
    {
      busyOverlay.IsBusy = true;
      GiacL.Clear();
      CommandManagerC190220.RM_PalletIn(this,Filter);
    }
    private void RM_PalletIn(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;

      var giacL = dwr.Data as List<C190220_PalletIn>;
      if(giacL!=null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (giacL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        giacL = giacL.Take(Filter.MaxItems).ToList();
        GiacL.AddRange(giacL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }
       
    #endregion

    #region Eventi controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }
      Translate();
    }

    private void CtrlBaseC190220_Unloaded(object sender, RoutedEventArgs e)
    {

    }
    #endregion


    #region Eventi Ricerca
    private void ctrlPalletInFilterHorizontal_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;

      Cmd_RM_Pallet();

    }
    #endregion

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_RM_Pallet();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_RM_Pallet();
    }

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_PalletIn)
      {
        try
        {
          Clipboard.SetText((((C190220_PalletIn)b.Tag)).PalletCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyArticle_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_PalletIn)
      {
        try
        {
          Clipboard.SetText((((C190220_PalletIn)b.Tag)).ArticleCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private void butCopyLot_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_PalletIn)
      {
        try
        {
          Clipboard.SetText((((C190220_PalletIn)b.Tag)).LotCode);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }

    private async void butExport_Click(object sender, RoutedEventArgs e)
    {
      var view = new CtrlReportExport("PalletIn", GiacL.Select(g => new Exportable_PalletIn(g)).Cast<IExportable>().ToArray(), typeof(Exportable_PalletIn));

      //show the dialog
      var result = await DialogHost.Show(view, "RootDialog");

      if (view.Result == ExportResult.OK)
        LocalSnackbar.ShowMessageOkAndOpenFile("EXPORT COMPLETED".TD() + " " + view.FileCreatedName, 5, "OPEN", view.FileCreatedName);

    }


  }


  public class Exportable_PalletIn : IExportable
  {

    public Exportable_PalletIn(C190220_PalletIn palletIn)
    {
      PalletCode = palletIn.PalletCode;
      LotCode = palletIn.LotCode;
      ArticleCode = palletIn.ArticleCode;
      InputPosition = palletIn.Position;
      BornDate = palletIn.DateBorn.ToString();
      CellCode = palletIn.CellCode;
      Quantity = palletIn.Qty.ToString();
      ExpiryDate = palletIn.ExpiryDate.ToString();

    }

    [Exportation(ColumnIndex = 0, ColumnName = "PALLET CODE")]
    public string PalletCode { get; set; }

    [Exportation(ColumnIndex = 1, ColumnName = "LOT CODE")]
    public string LotCode { get; set; }

    [Exportation(ColumnIndex = 2, ColumnName = "ARTICLE CODE")]
    public string ArticleCode { get; set; }

    [Exportation(ColumnIndex = 3, ColumnName = "INPUT POSITION")]
    public string InputPosition { get; set; }

    [Exportation(ColumnIndex = 4, ColumnName = "BORN DATE")]
    public string BornDate { get; set; }

    [Exportation(ColumnIndex = 5, ColumnName = "CELL CODE")]
    public string CellCode { get; set; }

    [Exportation(ColumnIndex = 6, ColumnName = "BOX QUANTITY")]
    public string Quantity { get; set; }

    [Exportation(ColumnIndex = 7, ColumnName = "EXPIRY DATE")]
    public string ExpiryDate { get; set; }
  }


}
