﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C190220;
using Model.Custom.C190220.Report;
using Utilities.Extensions.MoreLinq;
using View.Common.Languages;

namespace View.Custom.C190220.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletOutFilterHorizontal.xaml
  /// </summary>
  public partial class CtrlPalletOutFilterHorizontal : CtrlBaseC190220
  {
    #region EVENTI
    public delegate void OnSearchHandler();
    public event OnSearchHandler OnSearch;
    #endregion

    #region Fields
    private const int _indexStartValue = 0;
    #endregion

    #region PUBLIC PROPERTIES
    public C190220_PalletOutFilter Filter { get; set; } = new C190220_PalletOutFilter();

    public ObservableCollectionFast<C190220_Position> PositionL = new ObservableCollectionFast<C190220_Position>();

    #region Press to search highlight

    private bool searchHighlight = true;

    public bool SearchHighlight
    {
      get { return searchHighlight; }
      set
      {
        searchHighlight = value;
        NotifyPropertyChanged("SearchHighlight");
      }
    }

    #endregion
    #endregion

    #region Constructor
    public CtrlPalletOutFilterHorizontal()
    {
      InitializeComponent();

      Cmd_RM_ListDestinationPositions_GetAll();
    }
    #endregion

    #region TRADUZIONI
    protected override void Translate()
    {
      HintAssist.SetHint(cbMaxItems, Localization.Localize.LocalizeDefaultString((string)cbMaxItems.Tag));
      HintAssist.SetHint(txtBoxLotCode, Localization.Localize.LocalizeDefaultString((string)txtBoxLotCode.Tag));
      HintAssist.SetHint(txtBoxArticleCode, Localization.Localize.LocalizeDefaultString((string)txtBoxArticleCode.Tag));
      HintAssist.SetHint(txtBoxPosition, Localization.Localize.LocalizeDefaultString((string)txtBoxPosition.Tag));
      HintAssist.SetHint(txtBoxOrderCode, Localization.Localize.LocalizeDefaultString((string)txtBoxOrderCode.Tag));
      HintAssist.SetHint(txtBoxPalletCode, Localization.Localize.LocalizeDefaultString((string)txtBoxPalletCode.Tag));

      btnCancel.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnCancel.Tag);
      btnSearch.ToolTip = Localization.Localize.LocalizeDefaultString((string)btnSearch.Tag);
      
      ctrlDateStart.Title= Localization.Localize.LocalizeDefaultString((string)ctrlDateStart.Tag);
      ctrlDateEnd.Title = Localization.Localize.LocalizeDefaultString((string)ctrlDateEnd.Tag);

      UpdatePositionList();
    }

    #endregion TRADUZIONI

    #region Comandi e Risposte
    private void Cmd_RM_ListDestinationPositions_GetAll()
    {
      PositionL.Clear();

      CommandManagerC190220.RM_ListDestinationPositions_GetAll(this);
    }
    private void RM_ListDestinationPositions_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var posL = dwr.Data as List<C190220_Position>;
      if (posL != null)
      {
        PositionL.AddRange(posL.GroupBy(pos => pos.Code).Select(grp => grp.First()).ToList());
      }

      UpdatePositionList();

    }

    #endregion

    #region Eventi controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    #endregion

    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      Filter.Reset();
    }

    private void btnSearch_Click(object sender, RoutedEventArgs e)
    {
      if (cbMaxItems.SelectedItem != null)
      {
        Filter.MaxItems = (int)cbMaxItems.SelectedItem;
      }
      Filter.Index = _indexStartValue;
      Search();
    }

    #region private Methods
    private void Search()
    {
      OnSearch?.Invoke();
    }
    #endregion

    private void TextBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Return)
        Search();

    }

    private void UpdatePositionList()
    {
      //Clear buttons stack panel
      spPositions.Children.Clear();

      foreach (var pos in PositionL)
      {
        Button btn = new Button();
        btn.Name = "btn" + pos.Code;
        btn.Tag = pos;
        btn.ToolTip = "PRESS TO SELECT".TD();
        btn.Width = 100;
        btn.Height = 100;
        btn.Padding = new Thickness(10);
        btn.Margin = new Thickness(10, 0, 10, 0);
        btn.Click += Btn_Click;
        btn.Style = this.FindResource("Custom_MaterialDesignFloatingActionButton") as Style;

        //costruisco il content
        TextBlock txtBlkPositionCode = new TextBlock();
        txtBlkPositionCode.Foreground = Brushes.White;
        txtBlkPositionCode.HorizontalAlignment = HorizontalAlignment.Center;
        txtBlkPositionCode.Text = pos.Code;


        TextBlock txtBlkPositionDescr = new TextBlock();
        txtBlkPositionDescr.Foreground = Brushes.White;
        txtBlkPositionDescr.HorizontalAlignment = HorizontalAlignment.Center;
        txtBlkPositionDescr.Text = pos.Description.TD();

        //add descr to viewbox
        Viewbox vbDescr = new Viewbox();
        vbDescr.Child = txtBlkPositionDescr;

        // add text code and descr to stack panel
        StackPanel spBtnContent = new StackPanel();
        spBtnContent.HorizontalAlignment = HorizontalAlignment.Center;
        spBtnContent.VerticalAlignment = VerticalAlignment.Center;
        spBtnContent.Children.Add(txtBlkPositionCode);
        spBtnContent.Children.Add(vbDescr);

        //add stack panel to viewbox
        Viewbox vb = new Viewbox();
        vb.Child = spBtnContent;

        // Add viewbox content to button
        btn.Content = vb;

        //aggiungo button allo stack panel già dichiarato nello Xaml
        spPositions.Children.Add(btn);

      }

    }

    private void Btn_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_Position)
      {
        try
        {
          Filter.Position.Code = ((C190220_Position)b.Tag).Code;
          Filter.Position.Description = ((C190220_Position)b.Tag).Description;
        }
        catch
        {

        }
      }

    }

    private void txtBoxPosition_TextChanged(object sender, TextChangedEventArgs e)
    {
      TextBox b = sender as TextBox;

      if (b.Text.IsNullOrEmpty())
        Filter.Position = new C190220_Position();
    }
  }
}
