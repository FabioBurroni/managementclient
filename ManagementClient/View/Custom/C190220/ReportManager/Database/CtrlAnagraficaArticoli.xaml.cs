﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C190220;
using Model.Custom.C190220.ArticleManager;
using Model.Custom.C190220.Report;
using Model.Export;
using View.Common.Languages;
using View.Common.Report;
using View.Custom.C190220.Converter;

namespace View.Custom.C190220.ReportManager
{
  /// <summary>
  /// Interaction logic for CtrlAnagraficaArticoli.xaml
  /// </summary>
  public partial class CtrlAnagraficaArticoli : CtrlBaseC190220
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public int StartIndex { get; set; } = _indexStartValue;
    public int MaxItems { get; set; } = _maxItemsStartValue;

    public C190220_ArticleFilter Filter { get; set; } = new C190220_ArticleFilter();

    public ObservableCollectionFast<C190220_Article> ArticleL { get; set; } = new ObservableCollectionFast<C190220_Article>();
    #endregion

    #region Article selected dp


    public C190220_Article ArticleSelected
    {
      get { return (C190220_Article)GetValue(ArticleSelectedProperty); }
      set { SetValue(ArticleSelectedProperty, value); }
    }

    // Using a DependencyProperty as the backing store for OrderSelected.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ArticleSelectedProperty =
        DependencyProperty.Register("ArticleSelected", typeof(C190220_Article), typeof(CtrlAnagraficaArticoli)
          , new UIPropertyMetadata(null));

    #endregion

    #region Article edit dp


    public C190220_Article ArticleEdit
    {
      get { return (C190220_Article)GetValue(ArticleEditProperty); }
      set { SetValue(ArticleEditProperty, value); }
    }

    // Using a DependencyProperty as the backing store for OrderSelected.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ArticleEditProperty =
        DependencyProperty.Register("ArticleEdit", typeof(C190220_Article), typeof(CtrlAnagraficaArticoli)
          , new UIPropertyMetadata(null));

    #endregion

   
    #region COSTRUTTORE
    public CtrlAnagraficaArticoli()
    {
      

      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlArticleFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colArtCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colUnitOfMeasure.Header = Localization.Localize.LocalizeDefaultString("UNIT OF MEASURE");
      colMinQuantity.Header = Localization.Localize.LocalizeDefaultString("MIN QTY");
      colMaxQuantity.Header = Localization.Localize.LocalizeDefaultString("MAX QTY");
      colStockWindow.Header = Localization.Localize.LocalizeDefaultString("STOCK WINDOW");
      colTurnover.Header = Localization.Localize.LocalizeDefaultString("TURNOVER");
      
      txtBlkCentralText.Text = Context.Instance.TranslateDefault((string)txtBlkCentralText.Tag);

      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butExport.ToolTip = Localization.Localize.LocalizeDefaultString("EXPORT");

      CollectionViewSource.GetDefaultView(dgArticleL.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    private void Cmd_RM_Article_GetAll()
    {
      busyOverlay.IsBusy = true;
      ArticleL.Clear();
      CommandManagerC190220.RM_Article_GetAll(this, Filter);
    }
    private void RM_Article_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;

      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var artL = dwr.Data as List<C190220_Article>;
      if (artL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (artL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        artL = artL.Take(Filter.MaxItems).ToList();
        ArticleL.AddRange(artL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

   
    #endregion


    #region Eventi Ricerca

    private void ctrlArticleFilter_OnSearch()
    {
      gridInfoCentral.Visibility = Visibility.Collapsed;
      ctrlArticleFilter.SearchHighlight = false;

      Cmd_RM_Article_GetAll();
    }

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_RM_Article_GetAll();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_RM_Article_GetAll();
    }

   

    #endregion

    #region Eventi controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();
    }

    private void CtrlBaseC190220_Unloaded(object sender, RoutedEventArgs e)
    {

    }    

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_Article)
      {
        try
        {
          Clipboard.SetText((((C190220_Article)b.Tag)).Code);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }


    #endregion



    private async void butExport_Click(object sender, RoutedEventArgs e)
    {
      var view = new CtrlReportExport("Article", ArticleL.Select(g => new Exportable_Article(g)).Cast<IExportable>().ToArray(),typeof(Exportable_Article));

      //show the dialog
      var result = await DialogHost.Show(view, "RootDialog");

      if (view.Result == ExportResult.OK)
        LocalSnackbar.ShowMessageOkAndOpenFile("EXPORT COMPLETED".TD() + " " + view.FileCreatedName, 5, "OPEN", view.FileCreatedName);

    }

    
  }


  public class Exportable_Article : IExportable
  {

    public Exportable_Article(C190220_Article artIn)
    {
      Code = artIn.Code;
      Description = artIn.Descr;
      Unity = artIn.Unit.ToString();

      AW_MinQuantity = artIn.AW_MinQuantity.ToString();
      AW_MaxQuantity = artIn.AW_MaxQuantity.ToString();
      StockWindow = artIn.StockWindow.ToString();
      Turnover = artIn.Turnover.ToString();
    }

    [Exportation(ColumnIndex = 0, ColumnName = "CODE")]
    public string Code { get; set; }

    [Exportation(ColumnIndex = 1, ColumnName = "DESCRIPTION")]
    public string Description { get; set; }

    [Exportation(ColumnIndex = 2, ColumnName = "UNITY")]
    public string Unity { get; set; }

    [Exportation(ColumnIndex = 3, ColumnName = "MIN QUANTITY")]
    public string AW_MinQuantity { get; set; }

    [Exportation(ColumnIndex = 4, ColumnName = "MAX QUANTITY")]
    public string AW_MaxQuantity { get; set; }

    [Exportation(ColumnIndex = 5, ColumnName = "STOCK WINDOW")]
    public string StockWindow { get; set; }

    [Exportation(ColumnIndex = 6, ColumnName = "TURNOVER")]
    public string Turnover { get; set; }



   
  }

}
