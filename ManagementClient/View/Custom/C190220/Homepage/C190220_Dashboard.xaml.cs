﻿
using System;

namespace View.Custom.C190220.Homepage
{
  /// <summary>
  /// Interaction logic for C190220_Dashboard.xaml
  /// </summary>
  public partial class C190220_Dashboard : CtrlBaseC190220
  {
    public C190220_Dashboard()
    {
      InitializeComponent();

      prova.LabelFormatter = Formatter;
    }

    public Func<double, string> Formatter = value => (value + "%").ToString();
  }
}
