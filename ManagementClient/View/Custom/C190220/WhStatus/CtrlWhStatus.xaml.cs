﻿using System;
using System.Windows;
using Model;
using Model.Custom.C190220.WhStatus;

namespace View.Custom.C190220.WhStatus
{
  /// <summary>
  /// Interaction logic for CtrlWhStatus.xaml
  /// </summary>
  public partial class CtrlWhStatus : CtrlBaseC190220
  {
    public string Title
    {
      get { return (string)GetValue(TitleProperty); }
      set { SetValue(TitleProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty TitleProperty =
        DependencyProperty.Register("Title", typeof(string), typeof(CtrlWhStatus), new PropertyMetadata(""));

    public Func<double, string> Formatter = value => (value + "%").ToString();

    #region DP - WhStatus
    public C190220_WhStatus WhStatus
    {
      get { return (C190220_WhStatus)GetValue(WhStatusProperty); }
      set { SetValue(WhStatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for WhStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty WhStatusProperty =
        DependencyProperty.Register("WhStatus", typeof(C190220_WhStatus), typeof(CtrlWhStatus), new PropertyMetadata(null));

    #endregion


    #region DP - MasterStatus
    public C190220_MasterStatus MasterStatus
    {
      get { return (C190220_MasterStatus)GetValue(MasterStatusProperty); }
      set { SetValue(MasterStatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for MasterStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty MasterStatusProperty =
        DependencyProperty.Register("MasterStatus", typeof(C190220_MasterStatus), typeof(CtrlWhStatus), new PropertyMetadata(null));
    #endregion



    #region DP - SatelliteStatus
    public C190220_SlaveStatus SlaveStatus
    {
      get { return (C190220_SlaveStatus)GetValue(SlaveStatusProperty); }
      set { SetValue(SlaveStatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for SatelliteStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty SlaveStatusProperty =
        DependencyProperty.Register("SlaveStatus", typeof(C190220_SlaveStatus), typeof(CtrlWhStatus), new PropertyMetadata(null));
    #endregion

    #region COSTRUTTORE
    public CtrlWhStatus()
    {
      InitializeComponent();

      gaugeBlkBlocked.LabelFormatter = Formatter;
      gaugeBlkCompleted.LabelFormatter = Formatter;
      gaugeBlkEmpty.LabelFormatter = Formatter;
      gaugeBlkFilling.LabelFormatter = Formatter;
      gaugeBlkNotCompleted.LabelFormatter = Formatter;
    }
    #endregion
    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkBlocked.Text = Context.Instance.TranslateDefault((string)txtBlkBlocked.Tag);
      txtBlkCompleted.Text = Context.Instance.TranslateDefault((string)txtBlkCompleted.Tag);
      txtBlkEmpty.Text = Context.Instance.TranslateDefault((string)txtBlkEmpty.Tag);
      txtBlkMaster.Text = Context.Instance.TranslateDefault((string)txtBlkMaster.Tag);
      txtBlkNotCompleted.Text = Context.Instance.TranslateDefault((string)txtBlkNotCompleted.Tag);
      txtBlkSlave.Text = Context.Instance.TranslateDefault((string)txtBlkSlave.Tag);
      txtBlkFilling.Text = Context.Instance.TranslateDefault((string)txtBlkFilling.Tag);
    }
    #endregion

    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }
  }
}
