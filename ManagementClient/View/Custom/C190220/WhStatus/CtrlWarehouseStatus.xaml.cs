﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Utilities.Extensions;
using Model;
using Model.Custom.C190220.WhStatus;
using Model.Custom.C190220;
using System.Timers;
using Configuration;

namespace View.Custom.C190220.WhStatus
{
  /// <summary>
  /// Interaction logic for CtrlWarehouseStatus.xaml
  /// </summary>
  public partial class CtrlWarehouseStatus : CtrlBaseC190220
  {
    public Timer RefreshTimer = new Timer();

    private bool autorefresh;
    private double intervalTime = 60000;

    private double elapsedTime;
    public double ElapsedTime
    {
      get { return elapsedTime; }
      set
      {
        elapsedTime = value;
        NotifyPropertyChanged("ElapsedTime");
        NotifyPropertyChanged("RefreshTimeInSeconds");
      }
    }

    public double RefreshTimeInSeconds => ElapsedTime / 1000;


    List<C190220_WhStatus> _whStatusL = new List<C190220_WhStatus>();
    List<C190220_MasterStatus> _masterStatusL = new List<C190220_MasterStatus>();
    List<C190220_SlaveStatus> _slaveStatusL = new List<C190220_SlaveStatus>();
    List<C190220_WhTraffic> _whTrafficL = new List<C190220_WhTraffic>();
    List<C190220_WhTrafficExit> _TrafficExitL = new List<C190220_WhTrafficExit>();

    public ObservableCollectionFast<C190220_ShippingLaneConf> ShL { get; set; } = new ObservableCollectionFast<C190220_ShippingLaneConf>();

    #region Costruttore
    public CtrlWarehouseStatus()
    {
      InitializeComponent();
      Init();
    }
    #endregion
 
    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      txtBlkDescription.Text = Context.Instance.TranslateDefault((string)txtBlkDescription.Tag);

      //txtBlkTraffic1.Text = Context.Instance.TranslateDefault((string)txtBlkTraffic1.Tag);
      //txtBlkTraffic2.Text = Context.Instance.TranslateDefault((string)txtBlkTraffic2.Tag);
      //txtBlkTrafficExit.Text = Context.Instance.TranslateDefault((string)txtBlkTrafficExit.Tag);
      //txtBlkTrafficExitWh.Text = Context.Instance.TranslateDefault((string)txtBlkTrafficExitWh.Tag);
      //txtBlkTrafficWH1.Text = Context.Instance.TranslateDefault((string)txtBlkTrafficWH1.Tag);
      //txtBlkTrafficWH2.Text = Context.Instance.TranslateDefault((string)txtBlkTrafficWH2.Tag);
      txtBlkWarehouse1.Text = Context.Instance.TranslateDefault((string)txtBlkWarehouse1.Tag);
      txtBlkWarehouse2.Text = Context.Instance.TranslateDefault((string)txtBlkWarehouse2.Tag);
      txtBlkWarehouse3.Text = Context.Instance.TranslateDefault((string)txtBlkWarehouse3.Tag);

      ctrlA1Wh1T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh1T.Tag) + " 1";
      ctrlA1Wh2T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh2T.Tag) + " 2";
      ctrlA1Wh3T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh3T.Tag) + " 3";
      ctrlA1Wh4T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh4T.Tag) + " 4";
      ctrlA1Wh5T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh5T.Tag) + " 5";
      ctrlA1Wh6T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh6T.Tag) + " 6";


    }

    #endregion TRADUZIONI

    #region Metodi Privati
    private void Init()
    {

      ctrlA1Wh1.Title = "1";
      ctrlA1Wh2.Title = "2";
      ctrlA1Wh3.Title = "3";
      ctrlA1Wh4.Title = "4";
      ctrlA1Wh5.Title = "5";
      ctrlA1Wh6.Title = "6";


      var whStatus1_1 = new C190220_WhStatus() { Code = "awh1_1" };
      var whStatus1_2 = new C190220_WhStatus() { Code = "awh1_2" };
      var whStatus1_3 = new C190220_WhStatus() { Code = "awh1_3" };
      var whStatus1_4 = new C190220_WhStatus() { Code = "awh1_4" };
      var whStatus1_5 = new C190220_WhStatus() { Code = "awh1_5" };
      var whStatus1_6 = new C190220_WhStatus() { Code = "awh1_6" };

      ctrlA1Wh1.WhStatus = whStatus1_1;
      ctrlA1Wh2.WhStatus = whStatus1_2;
      ctrlA1Wh3.WhStatus = whStatus1_3;
      ctrlA1Wh4.WhStatus = whStatus1_4;
      ctrlA1Wh5.WhStatus = whStatus1_5;
      ctrlA1Wh6.WhStatus = whStatus1_6;



      _whStatusL.Add(whStatus1_1);
      _whStatusL.Add(whStatus1_2);
      _whStatusL.Add(whStatus1_3);
      _whStatusL.Add(whStatus1_4);
      _whStatusL.Add(whStatus1_5);
      _whStatusL.Add(whStatus1_6);


      C190220_MasterStatus a1m1 = new C190220_MasterStatus() { Code = "m1" };
      C190220_MasterStatus a1m2 = new C190220_MasterStatus() { Code = "m2" };
      C190220_MasterStatus a1m3 = new C190220_MasterStatus() { Code = "m3" };
      C190220_MasterStatus a1m4 = new C190220_MasterStatus() { Code = "m4" };
      C190220_MasterStatus a1m5 = new C190220_MasterStatus() { Code = "m5" };
      C190220_MasterStatus a1m6 = new C190220_MasterStatus() { Code = "m6" };
      C190220_MasterStatus a1m7 = new C190220_MasterStatus() { Code = "m7" };


      ctrlA1Wh1.MasterStatus = a1m1;
      ctrlA1Wh2.MasterStatus = a1m2;
      ctrlA1Wh3.MasterStatus = a1m3;
      ctrlA1Wh4.MasterStatus = a1m4;
      ctrlA1Wh5.MasterStatus = a1m5;
      ctrlA1Wh6.MasterStatus = a1m6;

      _masterStatusL.Add(a1m1);
      _masterStatusL.Add(a1m2);
      _masterStatusL.Add(a1m3);
      _masterStatusL.Add(a1m4);
      _masterStatusL.Add(a1m5);
      _masterStatusL.Add(a1m6);
      _masterStatusL.Add(a1m7);

      var a1s1 = new C190220_SlaveStatus() { Code = "s1" };
      var a1s2 = new C190220_SlaveStatus() { Code = "s2" };
      var a1s3 = new C190220_SlaveStatus() { Code = "s3" };
      var a1s4 = new C190220_SlaveStatus() { Code = "s4" };
      var a1s5 = new C190220_SlaveStatus() { Code = "s5" };
      var a1s6 = new C190220_SlaveStatus() { Code = "s6" };

      ctrlA1Wh1.SlaveStatus = a1s1;
      ctrlA1Wh2.SlaveStatus = a1s2;
      ctrlA1Wh3.SlaveStatus = a1s3;
      ctrlA1Wh4.SlaveStatus = a1s4;
      ctrlA1Wh5.SlaveStatus = a1s5;
      ctrlA1Wh6.SlaveStatus = a1s6;

      _slaveStatusL.Add(a1s1);
      _slaveStatusL.Add(a1s2);
      _slaveStatusL.Add(a1s3);
      _slaveStatusL.Add(a1s4);
      _slaveStatusL.Add(a1s5);
      _slaveStatusL.Add(a1s6);

      // TRAFFIC 

      ctrlA1Wh1T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh1T.Tag) + " 1";
      ctrlA1Wh2T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh2T.Tag) + " 2";
      ctrlA1Wh3T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh3T.Tag) + " 3";
      ctrlA1Wh4T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh4T.Tag) + " 4";
      ctrlA1Wh5T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh5T.Tag) + " 5";
      ctrlA1Wh6T.Title = Context.Instance.TranslateDefault((string)ctrlA1Wh6T.Tag) + " 6";

      ctrlP17.Title = "P17";
      ctrlP167.Title = "P167";
      ctrlP175.Title = "P175";

      var a1t1 = new C190220_WhTraffic() { Area = "awh1" , Floor = "1"};
      var a1t2 = new C190220_WhTraffic() { Area = "awh1" , Floor = "2" };
      var a1t3 = new C190220_WhTraffic() { Area = "awh1" , Floor = "3" };
      var a1t4 = new C190220_WhTraffic() { Area = "awh1" , Floor = "4" };
      var a1t5 = new C190220_WhTraffic() { Area = "awh1" , Floor = "5" };
      var a1t6 = new C190220_WhTraffic() { Area = "awh1" , Floor = "6" };


      var P17 = new C190220_WhTrafficExit() { Code = "p17_out" };
      var P167 = new C190220_WhTrafficExit() { Code = "p167_out" };
      var P175 = new C190220_WhTrafficExit() { Code = "p175_out" };

      ctrlA1Wh1T.WhTraffic = a1t1;
      ctrlA1Wh2T.WhTraffic = a1t2;
      ctrlA1Wh3T.WhTraffic = a1t3;
      ctrlA1Wh4T.WhTraffic = a1t4;
      ctrlA1Wh5T.WhTraffic = a1t5;
      ctrlA1Wh6T.WhTraffic = a1t6;

      ctrlP17.WhTraffic = P17;
      ctrlP167.WhTraffic = P167;
      ctrlP175.WhTraffic = P175;

      _whTrafficL.Add(a1t1);
      _whTrafficL.Add(a1t2);
      _whTrafficL.Add(a1t3);
      _whTrafficL.Add(a1t4);
      _whTrafficL.Add(a1t5);
      _whTrafficL.Add(a1t6);

      _TrafficExitL.Add(P17);
      _TrafficExitL.Add(P167);
      _TrafficExitL.Add(P175);

    }

    private void Refresh()
    {
      Cmd_RM_Wh_Status();
      Cmd_RM_Wh_MasterStatus();
      Cmd_RM_Wh_SatelliteStatus();
      
      Cmd_RM_Wh_Traffic();
      Cmd_RM_Exit_Traffic();
    }
    #endregion


    #region Comandi

    private void Cmd_RM_Wh_Status()
    {
      CommandManagerC190220.RM_Wh_Status(this);
    }

    private void RM_Wh_Status(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var whStatusL = dwr.Data as List<C190220_WhStatus>;
      if (whStatusL != null)
      {

        foreach (var whs in whStatusL)
        {
          var currentWhS = _whStatusL.FirstOrDefault(w => w.Code.EqualsIgnoreCase(whs.Code));
          if (currentWhS != null) currentWhS.Update(whs);
        }
      }
    }

    private void Cmd_RM_Wh_MasterStatus()
    {
      CommandManagerC190220.RM_Wh_MasterStatus(this);
    }

    private void RM_Wh_MasterStatus(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var masterL = dwr.Data as List<C190220_MasterStatus>;
      if (masterL != null)
      {

        foreach (var master in masterL)
        {
          var currentMaster = _masterStatusL.FirstOrDefault(m => m.Code.EqualsIgnoreCase(master.Code));
          if (currentMaster != null) currentMaster.Update(master);
        }
      }
    }

    private void Cmd_RM_Wh_SatelliteStatus()
    {
      CommandManagerC190220.RM_Wh_SatelliteStatus(this);
    }

    private void RM_Wh_SatelliteStatus(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var slaveL = dwr.Data as List<C190220_SlaveStatus>;
      if (slaveL != null)
      {
        foreach (var slave in slaveL)
        {
          var currentSlave= _slaveStatusL.FirstOrDefault(s => s.Code.EqualsIgnoreCase(slave.Code));
          if (currentSlave != null) currentSlave.Update(slave);
        }
      }
    }

    private void Cmd_RM_Wh_Traffic()
    {
      CommandManagerC190220.RM_Wh_Traffic(this);
    }

    private void RM_Wh_Traffic(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var whTrafficL = dwr.Data as List<C190220_WhTraffic>;
      if (whTrafficL != null)
      {

        foreach (var wht in whTrafficL)
        {
          var currentWhT = _whTrafficL.FirstOrDefault(w => w.Area.EqualsIgnoreCase(wht.Area) && w.Floor.EqualsIgnoreCase(wht.Floor));

          if (currentWhT != null) currentWhT.Update(wht);
        }

        var sum = _whTrafficL.Select(x => x.PalletCount).Sum();

        foreach (var wht in _whTrafficL)
        {
          wht.PalletCountTotal = sum;
          wht.Update(wht);
        }
      }
    }

    private void Cmd_RM_Exit_Traffic()
    {
      CommandManagerC190220.RM_Exit_Traffic(this);
    }

    private void RM_Exit_Traffic(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as Model.DataWrapperResult;
      if (dwr == null)
      {
        return;
      }
      var whTrafficL = dwr.Data as List<C190220_WhTrafficExit>;
      if (whTrafficL != null)
      {
        //...calculates total traffic
        int palletCountTotal = whTrafficL.Sum(p => p.PalletCount);
        whTrafficL.ForEach(p => p.PalletCountTotal = palletCountTotal);
        foreach (var wht in whTrafficL)
        {
          //var currentWhT = _TrafficExitL.FirstOrDefault(w => w.Code.EqualsIgnoreCase(wht.Code));
          //if (currentWhT != null) currentWhT.Update(wht);

          _TrafficExitL.FirstOrDefault(t => t.Code.EqualsIgnoreCase(wht.Code))?.Update(wht);
        }

        //var sum = _whTrafficL.Select(x => x.PalletCount).Sum();

        //foreach (var wht in _whTrafficL)
        //{
        //  wht.PalletCountTotal = sum;
        //  wht.Update(wht);
        //}
      }
    }

    #endregion


    #region Control Event

    private void RefreshTimer_Elapsed(object sender, ElapsedEventArgs e)
    {
      ElapsedTime -= RefreshTimer.Interval;
      if (ElapsedTime <= 0)
      {
        ElapsedTime = intervalTime;
        Refresh();
      }
    }

    private void CtrlBaseC190220_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
      Refresh();

      Translate();
          
      //get parameters...
      var statusPanelSettings = ConfigurationManager.Parameters.Layout.Where(x => x.Title == "Status Panel").FirstOrDefault();
      bool enableAutoRefresh = statusPanelSettings.Settings.Where(x => x.Name == "WarehouseStatusAutoRefresh").FirstOrDefault().Value.ConvertToBool();
      int durationAutoRefresh = statusPanelSettings.Settings.Where(x => x.Name == "WarehouseStatusAutoRefreshDuration").FirstOrDefault().Value.ConvertToInt();

      if (durationAutoRefresh >= 5)
        intervalTime = durationAutoRefresh * 1000;

      autorefresh = enableAutoRefresh;

      RefreshTimer = new Timer();
      RefreshTimer.Interval = 1000;
      RefreshTimer.AutoReset = true;
      RefreshTimer.Elapsed += RefreshTimer_Elapsed;
      ElapsedTime = intervalTime;

      if (autorefresh)
      {
        RefreshTimer.Start();

        iconRefresh.Visibility = System.Windows.Visibility.Hidden;
        busyOverlayViewBox.Visibility = System.Windows.Visibility.Visible;
      }
      else
      {
        iconRefresh.Visibility = System.Windows.Visibility.Visible;
        busyOverlayViewBox.Visibility = System.Windows.Visibility.Hidden;
      }



    }

    private void CtrlBaseC190220_IsVisibleChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
    {
      if (!IsVisible)
      {
        Closing();
      }
    }

    public override void Closing()
    {
      ElapsedTime = intervalTime;
      if (RefreshTimer != null)
      {
        RefreshTimer.Stop();
        RefreshTimer.Elapsed -= RefreshTimer_Elapsed;
        RefreshTimer = null;
      }
      base.Closing();
    }

    #endregion

    private void butRefresh_Click(object sender, RoutedEventArgs e)
    {
      ElapsedTime = intervalTime;
      Refresh();
    }

    private void butRefresh_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
    {
      iconRefresh.Visibility = System.Windows.Visibility.Visible;
      busyOverlayViewBox.Visibility = System.Windows.Visibility.Hidden;
    }

    private void butRefresh_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
    {
      if (!autorefresh)
        return;

      iconRefresh.Visibility = System.Windows.Visibility.Hidden;
      busyOverlayViewBox.Visibility = System.Windows.Visibility.Visible;
    }
  }
}
