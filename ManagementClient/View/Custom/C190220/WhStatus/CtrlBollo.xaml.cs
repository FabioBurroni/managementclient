﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace View.Custom.C190220.WhStatus
{
  /// <summary>
  /// Interaction logic for CtrlBollo.xaml
  /// </summary>
  public partial class CtrlBollo : UserControl
  {
    public CtrlBollo()
    {
      InitializeComponent();
    }

    public Brush ForeColor
    {
      get { return (Brush)GetValue(ForeColorProperty); }
      set { SetValue(ForeColorProperty, value); }
    }

    // Using a DependencyProperty as the backing store for ForeColor.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ForeColorProperty =
       DependencyProperty.Register("ForeColor", typeof(Brush), typeof(CtrlBollo), new PropertyMetadata(Brushes.Blue));

  }
}
