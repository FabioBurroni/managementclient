﻿using System;
using System.Windows;
using System.Windows.Controls;

using Model.Custom.C190220.WhStatus;

namespace View.Custom.C190220.WhStatus
{
  /// <summary>
  /// Interaction logic for CtrlWhStatus.xaml
  /// </summary>
  public partial class CtrlWhTrafficExit : UserControl
  {
    public string Title
    {
      get { return (string)GetValue(TitleProperty); }
      set { SetValue(TitleProperty, value); }
    }

    // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty TitleProperty =
        DependencyProperty.Register("Title", typeof(string), typeof(CtrlWhTrafficExit), new PropertyMetadata(""));

    public Func<double, string> Formatter = value => (value + "%").ToString();

    #region DP - WhTraffic
    public C190220_WhTrafficExit WhTraffic
    {
      get { return (C190220_WhTrafficExit)GetValue(WhTrafficProperty); }
      set { SetValue(WhTrafficProperty, value); }
    }

    // Using a DependencyProperty as the backing store for WhStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty WhTrafficProperty =
        DependencyProperty.Register("WhTraffic", typeof(C190220_WhTrafficExit), typeof(CtrlWhTrafficExit), new PropertyMetadata(null));

    #endregion

    #region COSTRUTTORE
    public CtrlWhTrafficExit()
    {
      InitializeComponent();

      gaugeWhTraffic.LabelFormatter = Formatter;
    } 
    #endregion

  }
}
