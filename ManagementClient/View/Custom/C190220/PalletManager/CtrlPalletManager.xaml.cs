﻿using System;
using System.Windows;
using Model.Custom.C190220;


namespace View.Custom.C190220.PalletManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletManager.xaml
  /// </summary>
  public partial class CtrlPalletManager : CtrlBaseC190220
  {
    public C190220_UserLogged UserLogged { get; set; } = new C190220_UserLogged();

    #region Costruttore
    public CtrlPalletManager()
    {
      InitializeComponent();
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
      Authentication.AuthenticationManager.Instance.LoginSession += Instance_LoginSession;

    }
    #endregion

    #region TRADUZIONI
    protected override void Translate()
    {
      tiCreateNew.Text = Localization.Localize.LocalizeDefaultString((string)tiCreateNew.Tag);
      tiEdit.Text = Localization.Localize.LocalizeDefaultString((string)tiEdit.Tag);
    }
    #endregion TRADUZIONI

    #region Eventi controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
    }

    #endregion

    private void Instance_LoginSession(object sender, Authentication.LoginSessionEventArgs sessionArgs)
    {
      UserLogged.UserName = Authentication.AuthenticationManager.Instance.Session.User.Username;
    }

    private void CtrlBaseC190220_Unloaded(object sender, RoutedEventArgs e)
    {
      try
      {
        Authentication.AuthenticationManager.Instance.LoginSession -= Instance_LoginSession;
      }
      catch (Exception)
      {

      }
    }
  }
}
