﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MaterialDesignThemes.Wpf;
using Model;
using Model.Custom.C190220;
using Model.Custom.C190220.ArticleManager;
using Model.Custom.C190220.PalletManager;
using View.Common.Languages;

namespace View.Custom.C190220.PalletManager
{
  /// <summary>
  /// Interaction logic for CtrlPalletArticles.xaml
  /// </summary>
  public partial class CtrlPalletArticles : CtrlBaseC190220
  {
    #region PRIVATE FIELDS
    private const int _indexStartValue = 0;
    private const int _maxItemsStartValue = 100;
    #endregion

    #region Public Properties
    public int StartIndex { get; set; } = _indexStartValue;
    public int MaxItems { get; set; } = _maxItemsStartValue;

    public C190220_PalletArticleFilter Filter { get; set; } = new C190220_PalletArticleFilter();

    public ObservableCollectionFast<C190220_Article> ArticleL { get; set; } = new ObservableCollectionFast<C190220_Article>();
    #endregion

    #region Article selected dp


    public C190220_Article ArticleSelected
    {
      get { return (C190220_Article)GetValue(ArticleSelectedProperty); }
      set { SetValue(ArticleSelectedProperty, value); }
    }

    // Using a DependencyProperty as the backing store for OrderSelected.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ArticleSelectedProperty =
        DependencyProperty.Register("ArticleSelected", typeof(C190220_Article), typeof(CtrlPalletArticles)
          , new UIPropertyMetadata(null));

    #endregion

    #region COSTRUTTORE
    public CtrlPalletArticles()
    {
      Filter.Index = _indexStartValue;
      Filter.MaxItems = _maxItemsStartValue;
      InitializeComponent();
      Filter = ctrlArticleFilter.Filter;
    }
    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkTitle.Text = Context.Instance.TranslateDefault((string)txtBlkTitle.Tag);
      colArtCode.Header = Localization.Localize.LocalizeDefaultString("ARTICLE CODE");
      colDescription.Header = Localization.Localize.LocalizeDefaultString("DESCRIPTION");
      colUnitOfMeasure.Header = Localization.Localize.LocalizeDefaultString("UNIT OF MEASURE");
      colTypeCode.Header = Localization.Localize.LocalizeDefaultString("TYPE");
      colMinQuantity.Header = Localization.Localize.LocalizeDefaultString("MIN QTY");
      colMaxQuantity.Header = Localization.Localize.LocalizeDefaultString("MAX QTY");
      colStockWindow.Header = Localization.Localize.LocalizeDefaultString("STOCK WINDOW");
      colTurnover.Header = Localization.Localize.LocalizeDefaultString("TURNOVER");
      
      txtBlkNumberOfResult.Text = Context.Instance.TranslateDefault((string)txtBlkNumberOfResult.Tag);
      butPrev.ToolTip = Localization.Localize.LocalizeDefaultString("BACK");
      butNext.ToolTip = Localization.Localize.LocalizeDefaultString("NEXT");
      butClose.ToolTip = Localization.Localize.LocalizeDefaultString("CLOSE");

      CollectionViewSource.GetDefaultView(dgArticleL.ItemsSource).Refresh();
    }

    #endregion TRADUZIONI


    #region COMANDI E RISPOSTE
    private void Cmd_PM_Article_GetAll()
    {
      busyOverlay.IsBusy = true;
      ArticleL.Clear();
      CommandManagerC190220.PM_Article_GetAll(this, Filter);
    }
    private void PM_Article_GetAll(IList<string> commandMethodParameters, IModel model)
    {
      busyOverlay.IsBusy = false;

      //ShowWait(false, "", "");
      var dwr = model as DataWrapperResult;
      if (dwr == null) return;
      var artL = dwr.Data as List<C190220_Article>;
      if (artL != null)
      {
        if (Filter.Index == _indexStartValue)
          butPrev.IsEnabled = false;
        else
          butPrev.IsEnabled = true;

        if (artL.Count > Filter.MaxItems)
          butNext.IsEnabled = true;
        else
          butNext.IsEnabled = false;

        artL = artL.Take(Filter.MaxItems).ToList();
        ArticleL.AddRange(artL);
      }
      else
      {
        butPrev.IsEnabled = false;
        butNext.IsEnabled = false;
      }
    }

   
    #endregion


    #region Eventi Ricerca

    private void ctrlArticleFilter_OnSearch()
    {
      Cmd_PM_Article_GetAll();
    }

    private void butPrev_Click(object sender, RoutedEventArgs e)
    {
      
      if (Filter.Index > _indexStartValue)
        Filter.Index--;
      Cmd_PM_Article_GetAll();
    }

    private void butNext_Click(object sender, RoutedEventArgs e)
    {
      Filter.Index++;
      Cmd_PM_Article_GetAll();
    }

   

    #endregion

    #region Eventi controllo
    private void CtrlBaseC190220_Loaded(object sender, RoutedEventArgs e)
    {
      if (DesignerProperties.GetIsInDesignMode(this))
      {
        return;
      }

      Translate();

      Cmd_PM_Article_GetAll();
    }

  

    private void butCopy_Click(object sender, RoutedEventArgs e)
    {
      Button b = sender as Button;
      if (b.Tag is C190220_Article)
      {
        try
        {
          Clipboard.SetText((((C190220_Article)b.Tag)).Code);

          LocalSnackbar.ShowMessageInfo(Clipboard.GetText() + " " + "COPIED".TD(), 1);
        }
        catch
        {

        }
      }
    }


    private void butClose_Click(object sender, RoutedEventArgs e)
    {
      ArticleSelected = null;
      DialogHost.CloseDialogCommand.Execute(true, null);
    }

    private void dgArticleL_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      DialogHost.CloseDialogCommand.Execute(true, null);
    }
    #endregion

  }



}
