﻿using Configuration;
using XmlCommunicationManager.XmlClient.Event;
using Utilities.Extensions;
using Model.Custom.C190220;
using Model;
using View.Common;

namespace View.Custom.C190220
{
	public class CtrlBaseC190220 : CtrlBase
	{
		
		#region Constructor

		public CtrlBaseC190220()
		{
			// Check for design mode. 
			if (DesignTimeHelper.IsInDesignMode)
				return;

			InitCommandManager();
		}

    #endregion
    

    #region Private Properties

    protected CommandManagerC190220 CommandManagerC190220 { get; private set; }

		#endregion

		#region Init CommandManager

		private void InitCommandManager()
		{
      CommandManagerC190220 = new CommandManagerC190220(Context.XmlClient, ConfigurationManager.Parameters.CommandPreambleSetting.GetPreamble("custom"));
		}

		#endregion

		#region Override

		protected override void XmlClientOnException(object sender, ExceptionEventArgs e)
		{
			if (ManageXmlClientException(CommandManagerC190220, e))
				return;

			base.XmlClientOnException(sender, e);
		}

		protected override void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs e)
		{
			if (ManageXmlClientReply(CommandManagerC190220, e))
				return;

			base.XmlClientOnRetrieveReplyFromServer(sender, e);
		}

		#endregion

  
  }
}