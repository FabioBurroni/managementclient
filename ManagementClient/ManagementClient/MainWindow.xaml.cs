﻿using Authentication;
using Authentication.Default;
using Configuration;
using Configuration.Settings.Global;
using ExtendedUtilities.Keyboard.Controls;
using ExtendedUtilities.SnackbarTools;
using ExtendedUtilities.Wpf;
using Localization;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using Model;
using Model.Attributes;
using Model.Common;
using Model.Common.Bcr;
using NAppUpdate.UI.WPF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Utilities;
using Utilities.Compatibility;
using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;
using View.Common.Homepage;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlServer;
using XmlCommunicationManager.XmlServer.Event;

namespace ManagementClient
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    #region Fields

    private readonly Context _context = Context.Instance;
    private readonly AuthenticationManager _authenticationManager = AuthenticationManager.Instance;
    private readonly CultureManager _cultureManager = CultureManager.Instance;
    private ConfigurationParameters _confParameters;

    private CustomKeyboard CustomKeyboard;
    private CustomKeyboard CustomFullscreenKeyboard;
    private bool isKeyboardInizialized = false;
    private Point _mouseDownPosition;
    private Point _mouseDownOffset;

    private List<Language> _languages;
    private RelayCommand<CultureInfo> _languageCommand;

    /// <summary>
    /// Form di autenticazione
    /// </summary>
    private IAuthenticationForm _authenticationForm;

    #endregion Fields

    #region COSTRUTTORE

    public MainWindow()
    {
      InitializeComponent();


      MainSnackbar.control = mainSnackbar;

      CtrlHeader.AppVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

      try
      {
        InitAndStart();
      }
      catch (Exception ex)
      {
        MessageBox.Show($"Error Loading {nameof(ManagementClient)}: {ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        Application.Current.Shutdown();
      }

      StateChanged += MainWindowStateChangeRaised;

      EventManager.RegisterClassHandler(typeof(TextBox), TextBox.MouseDoubleClickEvent, new RoutedEventHandler(OnMouseDoubleClickEvent));
      EventManager.RegisterClassHandler(typeof(TextBox), TextBox.TouchUpEvent, new RoutedEventHandler(OnMouseDoubleClickEvent));
    }

    #endregion COSTRUTTORE

    #region PROPERTIES

    public ICommand LanguageCommand
    {
      get
      {
        return _languageCommand ?? (_languageCommand = new RelayCommand<CultureInfo>(SetLanguage));
      }
    }

    #endregion PROPERTIES

    #region INITIALIZATION

    private void InitAndStart()
    {
      #region Init

      Init_Configuration();
      Init_Position();
      Init_Language();
      Init_Window();
      Init_Setup();

      Init_XmlClient();
      Init_XmlServer();

      Init_Header();
      Init_LeftMenu();
      Init_Footer();

      Init_PopUp();

      SubscribeAuthenticationEvents();

      #endregion Init

      #region Start

      Start_XmlClient();
      Start_XmlServer();

      Start_SerialBarcodeReader();

      Start_CheckForUpdates();

      #endregion Start
    }

    private void Init_PopUp()
    {
      var keyLan = InputLanguageManager.Current.AvailableInputLanguages.Cast<CultureInfo>();
      _languages = new List<Language>();
      var configLan = ConfigurationManager.Parameters.Localization.Languages.ToList();

      foreach (var lan in configLan)
      {
        if (keyLan.Any(s => s.Name == lan.Name))
          _languages.Add(lan);
      }

      var defaultKey = InputLanguageManager.Current.CurrentInputLanguage;
      if (_languages.Any(s => s.Name == defaultKey.Name))
        SelectedLanguage.Source = new BitmapImage(_languages.FirstOrDefault(s => s.Name == defaultKey.Name).ImageUri);

      Languages.ItemsSource = new ObservableCollection<Language>(_languages);
    }

    private void Init_Position()
    {
      //20171205 si setta la posizione di lavoro presa dal file di configurazione
      //20180113 se sono stati specicati dei parametri da linea di comando, prendo quelli altrimenti recupero quelli da file di configurazione

      var positions = ArgumentsConfiguration.QuickLoginInPositions?.Enabled == true
        ? ArgumentsConfiguration.QuickLoginInPositions.Positions
        : ConfigurationManager.Parameters.AuthenticationSetting.LoginSetting.Positions;

      //rimuovo eventuali caratteri speciali e aggiungo tutto al conteto
      positions.Select(p => p.Replace("@", "").Trim()).Distinct(StringComparer.OrdinalIgnoreCase).ForEach(p => _context.Positions.Add(p));
    }

    /// <summary>
    /// Inizializza la configurazione
    /// </summary>
    private void Init_Configuration()
    {
      #region CONFIGURATION

      Logging.Message("InitConfiguration(): Avvio lettura file di configurazione");
      try
      {
        ConfigurationManager.LoadConfiguration();
        _confParameters = ConfigurationManager.Parameters;
      }
      catch (Exception ex)
      {
        Logging.Message($"InitConfiguration(): errore lettura file di configurazione => Exception={ex.Message}");
        Environment.Exit(0);
      }

      Logging.Message("InitConfiguration(): Fine lettura di file di configurazione");

      #endregion CONFIGURATION
    }

    #region WINDOW

    private void Init_Window()
    {
      var assembly = Assembly.GetExecutingAssembly().GetName();
      //Title = $"{assembly.Name} v{assembly.Version}";
      Title = $"Management Client v{assembly.Version}";

      Height = Math.Max(600, _confParameters.Window.Height);
      Width = Math.Max(800, _confParameters.Window.Width);

      if (_confParameters.Window.CanMinimize && !_confParameters.Window.Resizable)
        ResizeMode = ResizeMode.CanMinimize;
      else if (_confParameters.Window.CanMinimize && _confParameters.Window.Resizable)
        ResizeMode = ResizeMode.CanResize;
      else if (!_confParameters.Window.CanMinimize && !_confParameters.Window.Resizable)
        ResizeMode = ResizeMode.NoResize;
      else if (!_confParameters.Window.CanMinimize && _confParameters.Window.Resizable)
        SourceInitialized += delegate { HideMinimizeAndMaximizeButtons(this); };

      WindowState = _confParameters.Window.Maximized ? WindowState.Maximized : WindowState.Normal;
    }

    // from winuser.h
    private const int GWL_STYLE = -16,
      WS_MAXIMIZEBOX = 0x10000,
      WS_MINIMIZEBOX = 0x20000;

    [DllImport("user32.dll")]
    private static extern int GetWindowLong(IntPtr hwnd, int index);

    [DllImport("user32.dll")]
    private static extern int SetWindowLong(IntPtr hwnd, int index, int value);

    internal static void HideMinimizeAndMaximizeButtons(Window window)
    {
      IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(window).Handle;
      var currentStyle = GetWindowLong(hwnd, GWL_STYLE);

      SetWindowLong(hwnd, GWL_STYLE, (currentStyle & ~WS_MAXIMIZEBOX & ~WS_MINIMIZEBOX));
    }

    #endregion WINDOW

    #region SETUP

    private void Init_Setup()
    {
      //registro o deregistro l'applicazione per partire lo start up di Windows
      SetupLaunchAtWindowsStartup(_confParameters.RunApplicationAtWindowsStartup);
    }

    /// <summary>
    /// true:registra l'applicazione allo startup; false deregistra l'applicazione
    /// </summary>
    private static void SetupLaunchAtWindowsStartup(bool isChecked)
    {
      //eventualmente utilizzare LocalMachine se si vuole questo settaggio per tutti gli utenti
      using (var registryKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true))
      {
        var assembly = Assembly.GetExecutingAssembly();
        string appName = assembly.GetName().Name;
        string location = System.IO.Path.GetDirectoryName(new Uri(assembly.GetName().CodeBase).LocalPath);
        if (registryKey != null)
        {
          if (isChecked)
          {
            registryKey.SetValue(appName, location);
          }
          else if (registryKey.GetValue(appName) != null)
          {
            registryKey.DeleteValue(appName);
          }
        }
      }
    }

    #endregion SETUP

    #region HEADER

    private void Init_Header()
    {
      CtrlHeader.Init();

      CtrlHeader.LogoutRequest += delegate
      {
        //la sessione non è ancora terminata, manda il logout e ferma il manager
        StopAndCleanAuthentication();
      };
    }

    #endregion HEADER

    #region FOOTER

    private void Init_Footer()
    {
      CtrlFooter.Init();
    }



    #endregion FOOTER

    #endregion INITIALIZATION

    #region UPDATE

    private CheckForUpdatesWindow _checkForUpdatesWindow;

    private void Start_CheckForUpdates()
    {

      CtrlFooter.AutomaticUpdateEnabled = _confParameters.UpdateSetting != null;
      if (_confParameters.UpdateSetting == null)
        return;

      var updateSetting = _confParameters.UpdateSetting;

      var feedUrl = updateSetting?.FeedUrl;
      var checkingInterval = updateSetting?.CheckingInterval ?? TimeSpan.Zero;
      var remindMeLaterInterval = updateSetting?.RemindMeLaterInterval ?? TimeSpan.Zero;
      var installWithoutUserConfirm = updateSetting?.InstallWithoutUserConfirm ?? false;

      _checkForUpdatesWindow = new CheckForUpdatesWindow
      {
        InstallWithoutUserConfirm = installWithoutUserConfirm,
        RemindMeLaterInterval = remindMeLaterInterval
      };

      try
      {
        _checkForUpdatesWindow.UpdatesAvailableChanged += _checkForUpdatesWindow_UpdatesAvailableChanged;

        _checkForUpdatesWindow.Start(feedUrl, checkingInterval);
      }
      catch (Exception e)
      {
        MessageBox.Show(e.Message);
      }
    }

    private void _checkForUpdatesWindow_UpdatesAvailableChanged(object sender, UpdatesAvailableEventArgs e)
    {
      Dispatcher.InvokeAsync(() => { CtrlFooter.AutomaticUpdateAvailable = e.UpdatesAvailable; }, DispatcherPriority.Input);
    }

    private void CtrlFooter_CheckForUpdate()
    {
      CheckForUpdatesNow();
    }

    private void CheckForUpdatesNow()
    {
      _checkForUpdatesWindow?.CheckForUpdatesNow();
    }

    private void Stop_CheckForUpdates()
    {
      _checkForUpdatesWindow?.Stop();
      _checkForUpdatesWindow?.Close();
    }

    #endregion

    #region SERIAL BARCODE READER

    private void Start_SerialBarcodeReader()
    {
      _context.BcrManager.SerialBcrChanged += BcrManagerOnSerialBcrChanged;
      _context.BcrManager.BcrDataReceived += BcrManagerOnBcrDataReceived;

      _context.BcrManager.Start();
    }

    private void BcrManagerOnSerialBcrChanged(object sender, SerialBcrChangedEventArgs e)
    {
    }

    private void BcrManagerOnBcrDataReceived(object sender, BcrDataReceivedEventArgs e)
    {
      Application.Current.Dispatcher.Invoke(() =>
      {
        //se ho la schermata di autenticazione aperta, devo passare a lei i dati
        if (_authenticationForm != null)
        {
          _authenticationForm.SetBcrData(e.Data);
          return;
        }

        //la schermata di autenticazione è chiusa, passo il bcr letto al controllo
        _currentItem?.Control?.BcrScanned(e.Data);
      });
    }

    private void Stop_SerialBarcodeReader()
    {
      try
      {
        if (_context.BcrManager != null)
        {
          _context.BcrManager.SerialBcrChanged -= BcrManagerOnSerialBcrChanged;
          _context.BcrManager.BcrDataReceived -= BcrManagerOnBcrDataReceived;
          _context.BcrManager.Dispose();
        }
      }
      catch { }
    }

    #endregion SERIAL BARCODE READER

    #region CLOSE

    private void CloseWindow()
    {
      Stop_CheckForUpdates();
      UnsubscribeAuthenticationEvents(); //mi deregistro agli eventi di autenticazione
      StopAndCleanAuthentication(); //se la sessione non è ancora terminata, manda il logout e ferma il manager

      Close_LeftMenu();
      Stop_SerialBarcodeReader();
      Stop_XmlClient();
      Stop_XmlServer();
    }

    #endregion CLOSE

    #region LANGUAGE

    /// <summary>
    /// Inizializza la lingua
    /// </summary>
    private void Init_Language()
    {
      _cultureManager.Culture = _confParameters.Localization.DefaultLanguage.Name;
      _cultureManager.CultureChanged += delegate { Translate(); };

      Translate();
    }

    #endregion LANGUAGE

    #region XML CLIENT

    private void Init_XmlClient()
    {
      _context.XmlClient = XmlClientCreator.CreateXmlClient(_confParameters.XmlClientConnectionSetting.Version.ConvertTo<XmlClientVersion>(),
        _confParameters.XmlClientConnectionSetting.IpAddress,
        _confParameters.XmlClientConnectionSetting.PortNumber,
        _confParameters.XmlClientConnectionSetting.TimeOut);
    }

    private void Start_XmlClient()
    {
      _context.XmlClient.OnException += XmlClient_OnException;
      _context.XmlClient.OnRetrieveReplyFromServer += XmlClient_OnRetrieveReplyFromServer;

      _context.XmlClient.Start();
    }

    private void Stop_XmlClient()
    {
      _context.XmlClient.OnException -= XmlClient_OnException;
      _context.XmlClient.OnRetrieveReplyFromServer -= XmlClient_OnRetrieveReplyFromServer;

      _context.XmlClient?.Dispose();
    }

    #endregion XML CLIENT

    #region XML SERVER

    private void Init_XmlServer()
    {
      _context.XmlServer = XmlServerCreator.CreateXmlServer(_confParameters.XmlServerConnectionSetting.Version.ConvertTo<XmlServerVersion>(),
        _confParameters.XmlServerConnectionSetting.IpAddress,
        _confParameters.XmlServerConnectionSetting.PortNumber);
    }

    private void Start_XmlServer()
    {
      _context.XmlServer.OnRequest += XmlServer_OnRequest;

      _context.XmlServer.Start();
    }

    private void Stop_XmlServer()
    {
      _context.XmlServer.OnRequest -= XmlServer_OnRequest;

      _context.XmlServer?.Dispose();
    }

    private void XmlServer_OnRequest(object sender, XmlOnRequestEventArgs xmlOnRequestEventArgs)
    {
      //if (!_loginMgr.IsLoggedInAnyPosition)
      //{
      //  return;
      //}

      //recupero il comando della notifica
      var xmlCmd = xmlOnRequestEventArgs.request.request as XmlCommand;
      if (xmlCmd == null)
        return;

      Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
      {
        InvokeNotificationMethod(xmlCmd);
      }));

      XmlResponse xmlResp = new XmlResponse(xmlOnRequestEventArgs.request);
      xmlResp.setResponse(new XmlCommandResponse(xmlCmd));

      _context.XmlServer.SendResponse(xmlResp);
    }

    private void InvokeNotificationMethod(XmlCommand xmlCmd)
    {
      //recupero il nome del comando di riferimento
      IList<string> controlsName;
      if (_dicNotifyCommands.TryGetValue(xmlCmd.commandMethodName, out controlsName))
      {
        //il meccanismo di notifica permette di creare un controllo (se ancora non esiste) e portarlo in primo piano ma solo se il suo attributo
        //ha la proprietà BringToFront = true, invoca il metodo SOLO se il controllo è già stato creato.

        #region Parte 1: creazione controllo (solo se attributo BringToFront è true)

        foreach (var c in controlsName)
        {
          var controlType = TypeExtension.GetTypeInAppDomain(c);

          //...metodo del controllo
          var controlMethod = controlType?.GetMethod(xmlCmd.commandMethodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);

          //recupero il suo attributo notificante
          var attribute = controlMethod?.GetCustomAttributes(typeof(XmlNotificationAttribute), false);

          if (attribute != null && attribute.Cast<XmlNotificationAttribute>().Any(a => a.BringToFront))
          {
            //l'attributo esiste ed è impostato per portare in primo piano il controllo, recupero il corrispettivo item

            try
            {
              var control = ctrlLeftMenu.GetMenuInfoItemByControlName(c);
              //istanzio il controllo
              ControlFunction_Select(control);
            }
            catch (Exception ex)
            {
              var exc = ex.ToString();
            }
          }
        }

        #endregion Parte 1: creazione controllo (solo se attributo BringToFront è true)

        #region Parte 2: Notifica al controllo

        //tento di recuperare i controlli che hanno il metodo indicato, se ne esiste almeno uno lo invoco, altrimenti lascio perdere. Questo significa che se la schermata è aperta (non importa che sia in primo piano), aggiorno i dati
        //se ancora non è stata aperta, i dati si aggiorneranno all'apertura (da gestire opportunamente) e la notifica verrà scartata
        var controls = _menuItemList.Where(m => controlsName.Contains(m.ControlName, StringComparer.OrdinalIgnoreCase))
          .Select(c => c.Control)
          .Where(c => c != null)
          .ToArray();

        foreach (var c in controls)
        {
          var method = c.GetType().GetMethod(xmlCmd.commandMethodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
          if (method == null)
            return;

          //Invoca il nome del comando che è presente anche come metodo. Quidi se il comando si chiama User_LoggedOut, allora verrà invocato il metodo User_LoggedOut
          method.Invoke(c, new object[] { xmlCmd });
        }

        #endregion Parte 2: Notifica al controllo
      }
    }

    #endregion XML SERVER

    #region LEFT MENU

    private readonly List<MenuInfoItem> _menuItemList = new List<MenuInfoItem>();
    private MenuInfoItem _currentItem;

    private void Init_LeftMenu()
    {
      ctrlLeftMenu.OnMenuSelected += CtrlLeftMenu_OnMenuSelected;
    }

    private void btnHome_Click(object sender, RoutedEventArgs e)
    {
      var menuItem = new MenuInfoItem("HOMEPAGE", "HOMEPAGE", null, "View.Common.Homepage.CtrlHomepage", null, "Black");
      //ControlFunction_Select(menuItem);

      if (menuItem.Control == null)
        menuItem.Control = (CtrlFunction)Activator.CreateInstance(menuItem.ControlName.GetTypeInAppDomain());

      _menuItemList.ForEach(m => { if (m != menuItem) m.Control.IsSelected = false; });

      if (menuItem.Control != null)
      {
        brdMain.Child = menuItem.Control;
        menuItem.Control.IsSelected = true;
      }

      ctrlLeftMenu.ClearSelection();
    }

    private void Load_LeftMenu()
    {
      ctrlLeftMenu.Init();

      //...creazione controlli
      //foreach (var mi in _menuItemList)
      //{
      //  mi.Control = (UserControl)Activator.CreateInstance(Type.GetType(mi.ControlName));
      //  ctrlLeftMenu.MenuItems.Add(mi);
      //}

      ExpanderMenu.IsExpanded = true;

      //metto il controllo homepage //NOTE la chiusura del controllo non è gestita, quindi per adesso evitare di metterci thread o altre risorse
      brdMain.Child = new CtrlHomepage();
    }

    private void Close_LeftMenu()
    {
      _menuItemList.Select(m => m.Control).ForEach(c => c?.Closing());
      _menuItemList.Clear();
      //ctrlLeftMenu.MenuItems.Clear();
      ctrlLeftMenu.Close();
      brdMain.Child = null;

      ExpanderMenu.IsExpanded = false;
    }

    private void MenuToggleButton_Click(object sender, RoutedEventArgs e)
    {
      ExpanderMenu.IsExpanded = !ExpanderMenu.IsExpanded;
    }

    #endregion LEFT MENU

    #region GESTIONE CONTROLLI FUNZIONE

    private void CtrlLeftMenu_OnMenuSelected(MenuInfoItem menuItem)
    {
      ControlFunction_Select(menuItem);
    }

    private void ControlFunction_Select(MenuInfoItem mi)
    {
      //tento di recuperare il controllo dalla lista, se non c'è lo creo e lo faccio partire
      if (mi.Control == null)
        mi.Control = (CtrlFunction)Activator.CreateInstance(mi.ControlName.GetTypeInAppDomain());

      //mi.Control.MainSnackbar = MainSnackbar;

      //se il controllo non è in lista, lo aggiungo
      if (!_menuItemList.Contains(mi))
        _menuItemList.Add(mi);

      _currentItem = mi;

      _menuItemList.ForEach(m => { if (m != mi) m.Control.IsSelected = false; });

      if (mi.Control != null)
      {
        brdMain.Child = mi.Control;
        mi.Control.IsSelected = true;
      }
    }

    #endregion GESTIONE CONTROLLI FUNZIONE

    #region EVENTI

    #region Custom Touch Screen Keyboard
    private async void OnMouseDoubleClickEvent(object sender, RoutedEventArgs e)
    {
      // Check Layout Settings for keyboards
      var toolSettings = ConfigurationManager.Parameters.Layout.Where(x => x.Title == "Tools").FirstOrDefault();
      bool keyboardAutoOpen = toolSettings.Settings.Where(x => x.Name == "KeyboardAutoOpen").FirstOrDefault().Value.ConvertToBool();
      bool keyboardTypeFullScreen = toolSettings.Settings.Where(x => x.Name == "KeyboardTypeFullScreen").FirstOrDefault().Value.ConvertToBool();

      if (!keyboardAutoOpen)
        return;

      if (keyboardTypeFullScreen)
      {
        await OpenFullScreenKeyboard(sender);
      }  
      else
      {
        OpenPopupKeyboard();
      }
    }

    private void CtrlFooter_OnKeyboardClick()
    {

      OpenPopupKeyboard();
    }

    private void OpenPopupKeyboard()
    {
      if (!isKeyboardInizialized)
      {
        isKeyboardInizialized = true;

        CustomKeyboard = new CustomKeyboard();
        CustomKeyboard.Width = 1600;
        CustomKeyboard.Height = 540;
        CustomKeyboard.FontSizeText = 25;
        CustomKeyboard.Background = Brushes.Transparent;
        CustomKeyboard.KeyType = ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.Floating;

        ChageTypeIcon.Kind = PackIconKind.Numeric;
        ExtendedType.IsChecked = false;

        CustomKeyboard.Init();
        Init_PopUp();

        KeyPopup.AllowsTransparency = true;
        KeyPopup.PlacementTarget = brdMain;
        KeyPopup.Placement = PlacementMode.Center;
        KeyPopup.Width = 1600;
        KeyPopup.Height = 600;
        KeyPopup.IsOpen = true;

        LayoutGrid.Children.Add(CustomKeyboard);
        CustomKeyboard.SetValue(Grid.RowProperty, 1);
        CustomKeyboard.SetValue(Grid.ColumnSpanProperty, 6);
      }
      else
      {
        // keyboard.IsOpen = !keyboard.IsOpen;
        Init_PopUp();
        KeyPopup.IsOpen = !KeyPopup.IsOpen;
      }
    }

    private async Task OpenFullScreenKeyboard(object sender)
    {
      if (CustomFullscreenKeyboard is null)
      {
        if (sender != null && sender is TextBox)
        {
          TextBox focusedTextBox = (TextBox)sender;

          CustomFullscreenKeyboard = new CustomKeyboard();
          CustomFullscreenKeyboard.FontSizeText = 25;
          CustomFullscreenKeyboard.Background = Brushes.DarkGray;
          CustomFullscreenKeyboard.KeyType = ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.FullScreen;
          CustomFullscreenKeyboard.Width = 1600;
          CustomFullscreenKeyboard.Height = 800;
          CustomFullscreenKeyboard.MyText = focusedTextBox.Text;

          CustomFullscreenKeyboard.Init();

          //show the dialog
          var result = await DialogHost.Show(CustomFullscreenKeyboard, "RootDialog");

          if (!string.IsNullOrEmpty((string)result))
          {
            focusedTextBox.Text = result.ToString();
          }

          CustomFullscreenKeyboard = null;
        }
      }
    }

    private void ExtendedType_Click(object sender, RoutedEventArgs e)
    {
      if ((sender as ToggleButton).IsChecked == true)
      {
        if (CustomKeyboard.KeyType == ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.Floating)
          CustomKeyboard.KeyType = ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.FloatingAdditional;
        else if (CustomKeyboard.KeyType == ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.NumFloating)
        {
          CustomKeyboard.KeyType = ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.NumFloatingAdditional;

          CustomKeyboard.Width = 1000;
          KeyPopup.Width = 1000;
        }
      }
      else
      {
        if (CustomKeyboard.KeyType == ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.FloatingAdditional)
          CustomKeyboard.KeyType = ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.Floating;
        else if (CustomKeyboard.KeyType == ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.NumFloatingAdditional)
        {
          CustomKeyboard.KeyType = ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.NumFloating;

          CustomKeyboard.Width = 700;
          KeyPopup.Width = 700;
        }
      }
    }

    private void ChageType_Click(object sender, RoutedEventArgs e)
    {
      switch (CustomKeyboard.KeyType)
      {
        case ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.Floating:
          ChageTypeIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Keyboard;
          CustomKeyboard.KeyType = ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.NumFloating;
          CustomKeyboard.Width = 700;
          CustomKeyboard.Height = 400;
          KeyPopup.Width = 700;
          KeyPopup.Height = 460;

          break;

        case ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.FloatingAdditional:
          ChageTypeIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Keyboard;
          CustomKeyboard.KeyType = ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.NumFloatingAdditional;
          CustomKeyboard.Width = 1000;
          CustomKeyboard.Height = 400;
          KeyPopup.Width = 1000;
          KeyPopup.Height = 460;

          break;

        case ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.NumFloating:
          CustomKeyboard.KeyType = ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.Floating;
          CustomKeyboard.Width = 1600;
          CustomKeyboard.Height = 540;
          KeyPopup.Width = 1600;
          KeyPopup.Height = 600;

          break;

        case ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.NumFloatingAdditional:
          ChageTypeIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Numeric;
          CustomKeyboard.KeyType = ExtendedUtilities.Keyboard.LogicalKeys.KeyboardType.FloatingAdditional;
          CustomKeyboard.Width = 1600;
          CustomKeyboard.Height = 540;
          KeyPopup.Width = 1600;
          KeyPopup.Height = 600;

          break;
      }
    }

    private void DragHandle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
    {
      _mouseDownPosition = e.GetPosition(KeyPopup.PlacementTarget);
      _mouseDownOffset = new Point(KeyPopup.HorizontalOffset, KeyPopup.VerticalOffset);
    }

    private void DragHandle_PreviewMouseMove(object sender, MouseEventArgs e)
    {
      if (e.LeftButton == MouseButtonState.Pressed)
      {
        var delta = e.GetPosition(KeyPopup.PlacementTarget) - _mouseDownPosition;
        if (!CustomKeyboard.IsDragging && delta.Length > CustomKeyboard.DeadZone)
        {
          CustomKeyboard.IsDragging = true;

          DragHandle.CaptureMouse();
        }

        if (CustomKeyboard.IsDragging)
        {
          KeyPopup.HorizontalOffset = _mouseDownOffset.X + delta.X;
          KeyPopup.VerticalOffset = _mouseDownOffset.Y + delta.Y;
        }
      }
    }

    private void DragHandle_PreviewMouseUp(object sender, MouseButtonEventArgs e)
    {
      DragHandle.ReleaseMouseCapture();
      CustomKeyboard.IsDragging = false;
    }

    private void KeyClose_Click(object sender, RoutedEventArgs e)
    {
      KeyPopup.IsOpen = !KeyPopup.IsOpen;
    }

    private void SetLanguage(CultureInfo culture)
    {
      var image = _languages.FirstOrDefault(s => s.Name == culture.Name);
      SelectedLanguage.Source = new BitmapImage(image.ImageUri);

      InputLanguageManager.Current.CurrentInputLanguage = image.Culture;

      CustomKeyboard.KeyLanguage = image.Name;
    }
    #endregion
  
    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      //setto il supporto ai tablet
      TabletSupport.ShowOnScreenKeyboard(this);

      //verifico se l'autenticazione è abilitata o meno
      if (_confParameters.AuthenticationSetting.Enabled)
      {
        //NOTE Thomas: questa istruzione può risultare strana, per quale motivo dovrei reinvocare tramite dispatcher il metodo di esecuzione? Non sono già nel thread principale?
        //NOTE Teoricamente non sarebbe necessario MA, se faccio partire il client dal file exe, non vengono mostrati header e footer fino a quando non faccio login (penso che lo ShowDialog freezi il rendering grafico)
        //NOTE la risolvo invocando il tutto in modo asincrono quando la schermata è completamente caricata
        Dispatcher.InvokeAsync(() => { ExecuteAuthentication(); }, DispatcherPriority.Loaded);
      }
      else
      {
        //avvio senza autenticazione

        //TODO eseguire qui la parte di login fittizio (che è esattamente il contenuto del gestore dell'evento AuthenticationManagerOnLoginSession)
      }
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      CloseWindow();
    }

    private void Window_Unloaded(object sender, RoutedEventArgs e)
    {
    }

    #region Custom Window Events

    private void Window_MouseMove(object sender, MouseEventArgs e)
    {
      Point pointToWindow = Mouse.GetPosition(this);

      if (pointToWindow.Y <= 30)
      {
        var headerSettings = ConfigurationManager.Parameters.Layout.Where(x => x.Title == "Header").FirstOrDefault();
        string headerVisibility = headerSettings.Settings.Where(x => x.Name == "CommandWindowVisible").FirstOrDefault().Value;
        int headerPosition = headerSettings.Settings.Where(x => x.Name == "CommandWindowPosition").FirstOrDefault().Value.ConvertToInt();

        brdTitleBar.Margin = new Thickness(0, 0, headerPosition, 0);

        brdTitleBar.Visibility = GetVisibilityFromString(headerVisibility);
      }
      else
      {
        brdTitleBar.Visibility = Visibility.Hidden;
      }
    }

    private Visibility GetVisibilityFromString(string value)
    {
      switch (value.ToLowerInvariant())
      {
        case "visible":
          return Visibility.Visible;

        case "true":
          return Visibility.Visible;

        case "hidden":
          return Visibility.Hidden;

        case "collapsed":
          return Visibility.Collapsed;

        case "false":
          return Visibility.Collapsed;
      }
      return Visibility.Collapsed;
    }

    //// Can execute
    //private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
    //{
    //  e.CanExecute = true;
    //}

    // Minimize
    private void Minimize(object sender, RoutedEventArgs e)
    {
      SystemCommands.MinimizeWindow(this);
    }

    // Maximize
    private void Maximize(object sender, RoutedEventArgs e)
    {
      SystemCommands.MaximizeWindow(this);
    }

    // Restore
    private void Restore(object sender, RoutedEventArgs e)
    {
      SystemCommands.RestoreWindow(this);
    }

    // Close
    private void Close(object sender, RoutedEventArgs e)
    {
      SystemCommands.CloseWindow(this);
    }

    // State change
    private void MainWindowStateChangeRaised(object sender, EventArgs e)
    {
      if (WindowState == WindowState.Maximized)
      {
        btnRestore.Visibility = Visibility.Visible;
        btnMaximize.Visibility = Visibility.Collapsed;
      }
      else
      {
        btnRestore.Visibility = Visibility.Collapsed;
        btnMaximize.Visibility = Visibility.Visible;
      }
    }

    private void brdTitleBar_MouseDown(object sender, MouseButtonEventArgs e)
    {
      if (e.ChangedButton == MouseButton.Left)
      {
        this.DragMove();
      }
    }

    private void brdTitleBar_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      if (WindowState == WindowState.Maximized)
      {
        SystemCommands.RestoreWindow(this);
      }
      else
      {
        SystemCommands.MaximizeWindow(this);
      }
    }

    #endregion Custom Window Events

    #endregion EVENTI

    #region GESTIONE NOTIFICHE

    //Key: nome della notifica. Value: lista dei nomi di controlli che sono sottoscritti alla notifica
    private readonly Dictionary<string, IList<string>> _dicNotifyCommands = new Dictionary<string, IList<string>>();

    /// <summary>
    /// Registra per ogni controllo
    /// </summary>
    private void Notification_Register()
    {
      //recupero tutti i controlli disponibili nel menu
      foreach (var mi in ctrlLeftMenu.AvailableMenuItems())
      {
        //ancora non ho il controllo, per recuperare il tipo mi serve il nome
        var t = TypeExtension.GetTypeInAppDomain(mi.ControlName);
        //...methodi del tipo
        var ms = t?.GetMethods();
        //...metodi che hanno l'attributo XmlCommandMethod
        var mas = ms?.Where(m => m.GetCustomAttributes(typeof(XmlNotificationAttribute), false).Length > 0).ToList();

        //scorro i metodi e li aggiungo al mio dizionario
        foreach (var m in mas.EmptyIfNull())
        {
          Console.WriteLine($"Notification_Register() for : {mi.Code}, method found: {m.Name}");

          IList<string> controlsName;
          if (!_dicNotifyCommands.TryGetValue(m.Name, out controlsName))
            _dicNotifyCommands.Add(m.Name, controlsName = new List<string>());

          controlsName.Add(mi.ControlName);
        }
      }
    }

    /// <summary>
    /// Deregistra i controlli
    /// </summary>
    private void Notification_Unregister()
    {
      _dicNotifyCommands.Clear();
    }

    #endregion GESTIONE NOTIFICHE

    #region EVENTI AUTENTICAZIONE

    private void AuthenticationManagerOnCloseRequest(object sender, EventArgs eventArgs)
    {
      //l'utente ha richiesto la chiusura, chiudo il form principale. Ci penserà il Dispose a pulire i dati
      Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
      {
        Close();
      }));

      //InvokeControlAction(this, delegate
      //{
      //	Close();
      //});
    }

    private void AuthenticationManagerOnLoginSession(object sender, LoginSessionEventArgs e)
    {
      //è stato fatto un login, pulisco
      Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
      {
        _authenticationForm = null;
        Load_LeftMenu();
        Init_AutoRefresh();
        Notification_Register();
      }));

      //InvokeControlAction(this, delegate
      //{
      //	_authenticationForm = null;
      //});
    }

    private void AuthenticationManagerOnLogoutSession(object sender, LogoutSessionEventArgs e)
    {
      Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
      {
        if (e.IsSessionTerminated)
        {
          //è configurato per rimanere aperto al logout, pulisco tutte le schermate, le stoppo e rimando a quella di login

          Close_AutoRefresh();

          //CloseFunctionalityControl(false, false);
          Close_LeftMenu();

          Notification_Unregister();

          ////aggiorno il controllo
          //_ctrlMain.UpdateCtrl();

          ExecuteAuthentication();
        }
      }));

      //InvokeControlAction(this, delegate
      //{
      //	if (e.IsSessionTerminated)
      //	{
      //		//è configurato per rimanere aperto al logout, pulisco tutte le schermate, le stoppo e rimando a quella di login
      //		CloseFunctionalityControl(false, false);

      //		//aggiorno il controllo
      //		_ctrlMain.UpdateCtrl();

      //		ExecuteAuthentication();
      //	}
      //});
    }

    #endregion EVENTI AUTENTICAZIONE

    #region AUTENTICAZIONE

    /// <summary>
    /// Questo booleano mi indica se è il primo tentativo di autenticazione che faccio.
    /// Questo perchè, in caso di logout dell'applicazione e poi login, devo mostrare l'interfaccia di autenticazione
    /// senza usare quick login o eredità
    /// </summary>
    private bool _isFirstAuthentication;

    private void ExecuteAuthentication()
    {
      if (_authenticationForm == null)
        _authenticationForm = (IAuthenticationForm)Activator.CreateInstance(_confParameters.AuthenticationSetting.Form.GetTypeInAppDomain());

      //se sono state specificate delle posizioni da linea di comando, prendo quelle, altrimenti recupero quelle da configurazione
      var loginPositions = ArgumentsConfiguration.QuickLoginInPositions?.Enabled == true
        ? ArgumentsConfiguration.QuickLoginInPositions.Positions
        : _confParameters.AuthenticationSetting.LoginSetting.Positions;

      var loginParameters = new LoginParameters(_confParameters.AuthenticationSetting.LoginSetting.ClientCode, _confParameters.AuthenticationSetting.LoginSetting.SourceType.ConvertTo<SourceType>(),
        _confParameters.XmlServerConnectionSetting.PortNumber, null, loginPositions);

      var authResponse = new AuthenticationResponse();
      var remoteErrorCodeMapper = (IErrorCodeMapper)Activator.CreateInstance(_confParameters.AuthenticationSetting.RemoteErrorCodeMapper.GetTypeInAppDomain());

      StopAndCleanAuthentication();
      //_authenticationManager.XmlCommandPreamble = _confParameters.CommandPreambleSetting.GetPreamble("authentication");
      _authenticationManager.InactivityTimeoutMilliseconds = TimeSpan.FromMinutes(_confParameters.AuthenticationSetting.UserInactivity.TimeoutMinutes).TotalMilliseconds;
      _authenticationManager.Configure(Context.Instance.XmlClient, Context.Instance.XmlServer, loginParameters, authResponse, remoteErrorCodeMapper);

      var tryToInheritSession = !_isFirstAuthentication;
      var quickLoginAsCassioli = !_isFirstAuthentication && ArgumentsConfiguration.QuickLoginAsCassioli;

      //_authenticationForm.ShowDialog(this, tryToInheritSession, quickLoginAsCassioli);

      var win32Window = new System.Windows.Forms.NativeWindow();
      win32Window.AssignHandle(new System.Windows.Interop.WindowInteropHelper(this).Handle);
      _authenticationForm.ShowDialog(win32Window, tryToInheritSession, quickLoginAsCassioli);

      if (!_isFirstAuthentication)
        _isFirstAuthentication = true;
    }

    private void StopAndCleanAuthentication()
    {
      _authenticationManager?.StopAndClean();
    }

    private void SubscribeAuthenticationEvents()
    {
      _authenticationManager.CloseRequest += AuthenticationManagerOnCloseRequest;
      _authenticationManager.LoginSession += AuthenticationManagerOnLoginSession;
      _authenticationManager.LogoutSession += AuthenticationManagerOnLogoutSession;
    }

    private void UnsubscribeAuthenticationEvents()
    {
      if (_authenticationManager == null)
        return;

      _authenticationManager.CloseRequest -= AuthenticationManagerOnCloseRequest;
      _authenticationManager.LoginSession -= AuthenticationManagerOnLoginSession;
      _authenticationManager.LogoutSession -= AuthenticationManagerOnLogoutSession;
    }

    #endregion AUTENTICAZIONE

    #region REFRESH

    private DispatcherTimer _dispatcherTimer;

    private void Init_AutoRefresh()
    {
      if (_dispatcherTimer == null)
      {
        _dispatcherTimer = new DispatcherTimer();

        _dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
        //_dispatcherTimer.Interval = new TimeSpan(0, 0, 30);
        _dispatcherTimer.Start();
      }
    }

    private void Close_AutoRefresh()
    {
      if (_dispatcherTimer != null)
      {
        try
        {
          _dispatcherTimer.Tick -= new EventHandler(dispatcherTimer_Tick);
          _dispatcherTimer.Stop();
          _dispatcherTimer = null;
        }
        catch
        {
        }
      }
    }

    private void dispatcherTimer_Tick(object sender, EventArgs e)
    {
      Refresh();
      _dispatcherTimer.Interval = new TimeSpan(0, 0, 30);
    }

    private void Refresh()
    {
      //Cmd_SystemStatus_Get();
      //Cmd_KanBan_GetAllKanBanMode();
    }

    #endregion REFRESH

    #region COMUNICAZIONE

    #region EVENTI XML CLIENT

    private void XmlClient_OnRetrieveReplyFromServer(object sender, XmlCommunicationManager.XmlClient.Event.MsgEventArgs e)
    {
      //string commandMethodName = null;
      //string[] commandMethodParameters = null;

      //Model.Context.Instance.CommandManagerHms.PreDecode(e, out commandMethodName, out commandMethodParameters);
      //var m = this.GetType().GetMethod(commandMethodName, BindingFlags.NonPublic | BindingFlags.Instance);
      //if (m == null)
      //	return;

      //var data = Context.Instance.CommandManagerHms.Decode(e, out commandMethodName, out commandMethodParameters);
      //Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
      //{
      //	Dispatch(commandMethodName, data, commandMethodParameters);
      //}));
    }

    private void ButtonHome_MouseEnter(object sender, MouseEventArgs e)
    {
      imgClientLogo.Visibility = Visibility.Hidden;
      iconHome.Visibility = Visibility.Visible;
    }

    private void ButtonHome_MouseLeave(object sender, MouseEventArgs e)
    {
      imgClientLogo.Visibility = Visibility.Visible;
      iconHome.Visibility = Visibility.Hidden;
    }


    private void XmlClient_OnException(object sender, XmlCommunicationManager.XmlClient.Event.ExceptionEventArgs e)
    {
      //Console.WriteLine(e.exception);
      //string commandMethodName = null;
      //string[] commandMethodParameters = null;
      //Context.Instance.CommandManagerHms.DecodeException(e, out commandMethodName, out commandMethodParameters);
      //var m = this.GetType().GetMethod(commandMethodName, BindingFlags.NonPublic | BindingFlags.Instance);
      //if (m == null)
      //	return;
      //Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
      //	{
      //		Dispatch(commandMethodName, null, commandMethodParameters);
      //	}));
    }

    //private void Dispatch(string commandMethodName, IDataStructure data, string[] commandMethodParameters)
    //{
    //	object[] parameters = new object[] { data, commandMethodParameters };
    //	var m = GetType().GetMethod(commandMethodName, BindingFlags.NonPublic | BindingFlags.Instance);

    //	try
    //	{
    //		if (m != null)
    //			m.Invoke(this, parameters);
    //	}
    //	catch (Exception ex)
    //	{
    //		Console.WriteLine(ex);
    //	}

    //	//      _type.InvokeMember(commandMethodName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.InvokeMethod, null, this, parameters);
    //	//var m = _type.GetMethod(commandMethodName);

    //	//if(m!=null)
    //	//{
    //	//	m.Invoke(this, parameters);
    //	//}

    //}

    #endregion EVENTI XML CLIENT

    #region COMANDI

    //TODO

    #endregion COMANDI

    #region RISPOSTE

    //TODO

    #endregion RISPOSTE

    #endregion COMUNICAZIONE

    #region TRADUZIONE

    private void Translate()
    {
      //TextBlockMenu.Text = Localize.LocalizeDefaultString("Menu").ToUpperInvariant();
      Drag.Text = Localize.LocalizeDefaultString("DRAG TO MOVE");
      textExt.Text = Localize.LocalizeDefaultString("EXT");
    }

    #endregion TRADUZIONE
  }
}