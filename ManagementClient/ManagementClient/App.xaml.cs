﻿using System;
using System.IO;
using System.Windows;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Linq;
using Model;

namespace ManagementClient
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private void Application_Startup(object sender, StartupEventArgs e)
		{
			AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
      /*
						//20210324 
						//...When automatic update is perfomed, new updated files take as owner the current user
						//...then is the operaive system user changes, the application can have some problems and doesn't start.

						//Set Permission for EveryOne to FullControl to all dll files in the root folder
						var baseDir = AppDomain.CurrentDomain.BaseDirectory;
						var filesDll = Directory.EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory,"*.*", SearchOption.TopDirectoryOnly)
							.Where(s => s.EndsWith(".dll") ); //|| s.EndsWith(".jpg")
						foreach (var file in filesDll)
						{
							GrantAccess(file);

						}
						//Set Permission for EveryOne to FullControl to all xml files in the root and sub folders
						var filesXml = Directory.EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory, "*.*", SearchOption.AllDirectories)
							.Where(s => s.EndsWith(".xml")); //|| s.EndsWith(".jpg")
						foreach (var file in filesXml)
						{
							GrantAccess(file);
						}
			*/
      //20210325 Grant Access Full Control to the base Folder

      try
      {
        var baseDir = AppDomain.CurrentDomain.BaseDirectory;
        GrantAccess(baseDir);
      }
      catch (Exception ex)
      {
				string exc = ex.ToString();
      }


			//carico gli argomenti passati all'app
			ArgumentsConfiguration.ReadArguments(e.Args);
			MainWindow wnd = new MainWindow();
			wnd.Show();
		}

		private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			Exception ex = e.ExceptionObject as Exception;
			if (ex != null)
			{
				Utilities.Logging.WriteToLog($"UnhandledException => Message: {ex.Message}{Environment.NewLine}StackTrace: {ex.StackTrace}");
			}
		}
		//20210324 Added method to grant access FullControl to Everyone user
		private void GrantAccess(string fullPath)
		{
			DirectoryInfo dInfo = new DirectoryInfo(fullPath);
			DirectorySecurity dSecurity = dInfo.GetAccessControl();
			dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null),
					FileSystemRights.FullControl,
					InheritanceFlags.ObjectInherit |
						 InheritanceFlags.ContainerInherit,
					PropagationFlags.NoPropagateInherit,
					AccessControlType.Allow));

			dInfo.SetAccessControl(dSecurity);
		}
	}
}