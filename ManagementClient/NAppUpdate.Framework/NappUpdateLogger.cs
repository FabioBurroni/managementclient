﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAppUpdate.Framework
{
  public static class NappUpdateLogger
  {
    static string localPath = @"log\NapUpdateLog.txt";
    private static bool ToLog = false;
    public static void Log(string msg)
    {
      if (!ToLog)
        return;

      var appDirectory = System.IO.Path.GetDirectoryName(UpdateManager.Instance.ApplicationPath);
      string fullPath = System.IO.Path.Combine(appDirectory, localPath);
      // This text is added only once to the file.
      if (!System.IO.File.Exists(fullPath))
      {
        // Create a file to write to.
        using (StreamWriter sw = File.CreateText(fullPath))
        {
          sw.WriteLine(msg);
        }
      }
      else
      {
        using (StreamWriter sw = File.AppendText(fullPath))
        {
          sw.WriteLine(msg);
        }
      }
    }
  }
}
