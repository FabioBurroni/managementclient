﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318
{
  public class C200318_ShippingLaneOrder : ModelBase
  {
    private string _OrderCode;
    public string OrderCode
    {
      get { return _OrderCode; }
      set
      {
        if (value != _OrderCode)
        {
          _OrderCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200318_ListColor _ListColor;
    public C200318_ListColor ListColor
    {
      get { return _ListColor; }
      set
      {
        if (value != _ListColor)
        {
          _ListColor = value;
          NotifyPropertyChanged();
        }
      }
    }



   

  }
}
