﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318
{
  public class C200318_Performance:ModelBase
  {
 
    private DateTime _DateStart;
    public DateTime DateStart
    {
      get { return _DateStart; }
      set
      {
        if (value != _DateStart)
        {
          _DateStart = value;
          DateStartS = _DateStart.ToString("HH:mm:ss");
        }
      }
    }

    public string DateStartS = string.Empty;


    private DateTime _DateEnd;
    public DateTime DateEnd
    {
      get { return _DateEnd; }
      set
      {
        if (value != _DateEnd)
        {
          _DateEnd = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Input;
    public int Input
    {
      get { return _Input; }
      set
      {
        if (value != _Input)
        {
          _Input = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Reject;
    public int Reject
    {
      get { return _Reject; }
      set
      {
        if (value != _Reject)
        {
          _Reject = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _Warehouse;
    public int Warehouse
    {
      get { return _Warehouse; }
      set
      {
        if (value != _Warehouse)
        {
          _Warehouse = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _Output;
    public int Output
    {
      get { return _Output; }
      set
      {
        if (value != _Output)
        {
          _Output = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Exit;
    public int Exit
    {
      get { return _Exit; }
      set
      {
        if (value != _Exit)
        {
          _Exit = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _Wh1In;
    public int Wh1In
    {
      get { return _Wh1In; }
      set
      {
        if (value != _Wh1In)
        {
          _Wh1In = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Wh2In;
    public int Wh2In
    {
      get { return _Wh2In; }
      set
      {
        if (value != _Wh2In)
        {
          _Wh2In = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Wh3In;
    public int Wh3In
    {
      get { return _Wh3In; }
      set
      {
        if (value != _Wh3In)
        {
          _Wh3In = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Wh1Out;
    public int Wh1Out
    {
      get { return _Wh1Out; }
      set
      {
        if (value != _Wh1Out)
        {
          _Wh1Out = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Wh2Out;
    public int Wh2Out
    {
      get { return _Wh2Out; }
      set
      {
        if (value != _Wh2Out)
        {
          _Wh2Out = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Wh3Out;
    public int Wh3Out
    {
      get { return _Wh3Out; }
      set
      {
        if (value != _Wh3Out)
        {
          _Wh3Out = value;
          NotifyPropertyChanged();
        }
      }
    }
  }
}
