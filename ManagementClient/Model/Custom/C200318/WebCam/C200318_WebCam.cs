﻿
namespace Model.Custom.C200318.WebCam
{
  public class C200318_WebCam : ModelBase
  {


    private int _Id;
    public int Id
    {
      get { return _Id; }
      set
      {
        if (value != _Id)
        {
          _Id = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _MasterCode = string.Empty;
    public string MasterCode
    {
      get { return _MasterCode; }
      set
      {
        if (value != _MasterCode)
        {
          _MasterCode = value;
          NotifyPropertyChanged();
          MasterCodeChanged = true;
          return;
        }
        MasterCodeChanged = false;
      }
    }

    private bool _MasterCodeChanged;
    public bool MasterCodeChanged
    {
      get { return _MasterCodeChanged; }
      set
      {
        if (value != _MasterCodeChanged)
        {
          _MasterCodeChanged = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _Side;
    public int Side
    {
      get { return _Side; }
      set
      {
        if (value != _Side)
        {
          _Side = value;
          NotifyPropertyChanged();
          SideChanged = true;
          return;
        }
        SideChanged = false;
      }
    }


    private bool _SideChanged;
    public bool SideChanged
    {
      get { return _SideChanged; }
      set
      {
        if (value != _SideChanged)
        {
          _SideChanged = value;
          NotifyPropertyChanged();
        }
      }
    }

  


    private string _IpAddress = string.Empty;
    public string IpAddress
    {
      get { return _IpAddress; }
      set
      {
        if (value != _IpAddress)
        {
          _IpAddress = value;
          NotifyPropertyChanged();
          IpAddressChanged = true;
          return;
        }
        IpAddressChanged = false;
      }
    }


    private bool _IpAddressChanged;
    public bool IpAddressChanged
    {
      get { return _IpAddressChanged; }
      set
      {
        if (value != _IpAddressChanged)
        {
          _IpAddressChanged = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _PortNumber;
    public int PortNumber
    {
      get { return _PortNumber; }
      set
      {
        if (value != _PortNumber)
        {
          _PortNumber = value;
          NotifyPropertyChanged();
          PortNumberChanged = true;
          return;
        }
        PortNumberChanged = false;

      }
    }
    private bool _PortNumberChanged;
    public bool PortNumberChanged
    {
      get { return _PortNumberChanged; }
      set
      {
        if (value != _PortNumberChanged)
        {
          _PortNumberChanged = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _UserName;
    public string UserName
    {
      get { return _UserName; }
      set
      {
        if (value != _UserName)
        {
          _UserName = value;
          NotifyPropertyChanged();
          UserNameChanged = true;
          return;
        }
        UserNameChanged = false;
      }
    }


    private bool _UserNameChanged;
    public bool UserNameChanged
    {
      get { return _UserNameChanged; }
      set
      {
        if (value != _UserNameChanged)
        {
          _UserNameChanged = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _Password;
    public string Password
    {
      get { return _Password; }
      set
      {
        if (value != _Password)
        {
          _Password = value;
          NotifyPropertyChanged();
          PasswordChanged = true;
          return;
        }
        PasswordChanged = false;
      }
    }



    private bool _PasswordChanged;
    public bool PasswordChanged
    {
      get { return _PasswordChanged; }
      set
      {
        if (value != _PasswordChanged)
        {
          _PasswordChanged = value;
          NotifyPropertyChanged();
        }
      }
    }




    private int _PresetLeft;
    public int PresetLeft
    {
      get { return _PresetLeft; }
      set
      {
        if (value != _PresetLeft)
        {
          _PresetLeft = value;
          NotifyPropertyChanged();
          PresetLeftChanged= true;
          return;
        }
        PresetLeftChanged = false;
      }
    }

    private bool _PresetLeftChanged;
    public bool PresetLeftChanged
    {
      get { return _PresetLeftChanged; }
      set
      {
        if (value != _PresetLeftChanged)
        {
          _PresetLeftChanged = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _PresetRight;
    public int PresetRight
    {
      get { return _PresetRight; }
      set
      {
        if (value != _PresetRight)
        {
          _PresetRight = value;
          NotifyPropertyChanged();
          PresetRightChanged = true;
          return;
        }
        PresetRightChanged = false;
      }
    }


    private bool _PresetRightChanged;
    public bool PresetRightChanged
    {
      get { return _PresetRightChanged; }
      set
      {
        if (value != _PresetRightChanged)
        {
          _PresetRightChanged = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _PresetHome;
    public int PresetHome
    {
      get { return _PresetHome; }
      set
      {
        if (value != _PresetHome)
        {
          _PresetHome = value;
          NotifyPropertyChanged();
          PresetHomeChanged = true;
          return;
        }
        PresetHomeChanged = false;
      }
    }


    private bool _PresetHomeChanged;
    public bool PresetHomeChanged
    {
      get { return _PresetHomeChanged; }
      set
      {
        if (value != _PresetHomeChanged)
        {
          _PresetHomeChanged = value;
          NotifyPropertyChanged();
        }
      }
    }



    private bool _Enabled;
    public bool Enabled
    {
      get { return _Enabled; }
      set
      {
        if (value != _Enabled)
        {
          _Enabled = value;
          NotifyPropertyChanged();
        }
      }
    }



    private bool _EnabledChanged;
    public bool EnabledChanged
    {
      get { return _EnabledChanged; }
      set
      {
        if (value != _EnabledChanged)
        {
          _EnabledChanged = value;
          NotifyPropertyChanged();
        }
      }
    }

    public void ResetChanged()
    {
      MasterCodeChanged = false;
      SideChanged = false;
      IpAddressChanged = false;
      PortNumberChanged = false;
      UserNameChanged = false;
      PasswordChanged = false;
      PresetLeftChanged = false;
      PresetRightChanged = false;
      PresetHomeChanged = false;
      EnabledChanged = false;
    }


    public void Update(C200318_WebCam wc)
    {
      MasterCode = wc.MasterCode;
      IpAddress = wc.IpAddress;
      PortNumber = wc.PortNumber;
      UserName = wc.UserName;
      Password = wc.Password;
      PresetLeft = wc.PresetLeft;
      PresetRight = wc.PresetRight;
      PresetHome = wc.PresetHome;
      Enabled = wc.Enabled;
    }




  }
}
