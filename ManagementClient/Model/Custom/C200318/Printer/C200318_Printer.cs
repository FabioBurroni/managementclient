﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.Printer
{
  public class C200318_Printer : ModelBase
  {
    #region CONSTRUCTOR
    public C200318_Printer()
    {

    }
    public C200318_Printer(string positionCode, string ipAddress, int portNumber, string description, int timeout)
    {
      _PositionCode = positionCode;
      _IpAddress = ipAddress;
      _PortNumber = portNumber;
      _Description = description;
      _Timeout = timeout;
    }
    #endregion

    #region PUBLIC PROPERTIES

    private string _PositionCode = string.Empty;
    public string PositionCode
    {
      get { return _PositionCode; }
      set
      {
        if (value != _PositionCode)
        {
          _PositionCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _IpAddress = "127.0.0.1";
    public string IpAddress
    {
      get { return _IpAddress; }
      set
      {
        if (value != _IpAddress)
        {
          _IpAddress = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _PortNumber = 9100;
    public int PortNumber
    {
      get { return _PortNumber; }
      set
      {
        if (value != _PortNumber)
        {
          _PortNumber = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Description = string.Empty;
    public string Description
    {
      get { return _Description; }
      set
      {
        if (value != _Description)
        {
          _Description = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _Timeout = 15;
    public int Timeout
    {
      get { return _Timeout; }
      set
      {
        if (value != _Timeout)
        {
          _Timeout = value;
          NotifyPropertyChanged();
        }
      }
    }
    #endregion

    #region PUBLIC METHODS
    public void Clear()
    {
      PositionCode = string.Empty;
      IpAddress = string.Empty;
      PortNumber = 0;
      Description = string.Empty;
      Timeout = 0;
    }

    public void Update(C200318_Printer newPrinter)
    {
      PositionCode = newPrinter.PositionCode;
      IpAddress = newPrinter.IpAddress;
      PortNumber = newPrinter.PortNumber;
      Description = newPrinter.Description;
      Timeout = newPrinter.Timeout;
    }
    #endregion
  }
}
