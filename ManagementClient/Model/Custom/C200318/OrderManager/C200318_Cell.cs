﻿using System;
using System.Linq;
using Utilities.Extensions;

namespace Model.Custom.C200318.OrderManager
{
  public class C200318_Cell : ModelBase
  {

    private string _Code = string.Empty;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _x;
    public int X
    {
      get { return _x; }
      set
      {
        if (value != _x)
        {
          _x = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _y;
    public int Y
    {
      get { return _y; }
      set
      {
        if (value != _y)
        {
          _y = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _z;
    public int Z
    {
      get { return _z; }
      set
      {
        if (value != _z)
        {
          _z = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsAvailableForManualSatFetch;
    public bool IsAvailableForManualSatFetch
    {
      get { return _IsAvailableForManualSatFetch; }
      set
      {
        if (value != _IsAvailableForManualSatFetch)
        {
          _IsAvailableForManualSatFetch = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsDisabled;
    public bool IsDisabled
    {
      get { return _IsDisabled; }
      set
      {
        if (value != _IsDisabled)
        {
          _IsDisabled = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsLocked;
    public bool IsLocked
    {
      get { return _IsLocked; }
      set
      {
        if (value != _IsLocked)
        {
          _IsLocked = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _PalletCount;
    public int PalletCount
    {
      get { return _PalletCount; }
      set
      {
        if (value != _PalletCount)
        {
          _PalletCount = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _LotCode;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    public void Update(C200318_Cell newCell)
    {
      this.Code = newCell.Code;
      if (this.Code != null)
      {
        string[] s = this.Code.Split('-');
        if (s != null)
        {
          this.X = s[1].ConvertToInt();
          this.Y = s[2].ConvertToInt();
          this.Z = s[3].ConvertToInt();
        }
      }
    
      this.IsAvailableForManualSatFetch = newCell.IsAvailableForManualSatFetch;
      this.IsDisabled = newCell.IsDisabled;
      this.IsLocked = newCell.IsLocked;
      this.PalletCount = newCell.PalletCount;
      this.ArticleCode = newCell.ArticleCode;
      this.ArticleDescr = newCell.ArticleDescr;
      this.LotCode = newCell.LotCode;
    }

    public void Reset()
    {
      this.Code = string.Empty; 
      this.X = 0;
      this.Y = 0;
      this.Z = 0;
      this.IsAvailableForManualSatFetch = false;
      this.IsDisabled = false;
      this.IsLocked = false;
      this.PalletCount = 0;
      this.ArticleCode = string.Empty;
      this.ArticleDescr = string.Empty;
      this.LotCode = string.Empty;
    }
  }
}
