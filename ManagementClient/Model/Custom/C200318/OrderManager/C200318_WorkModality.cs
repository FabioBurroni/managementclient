﻿
namespace Model.Custom.C200318.OrderManager
{
  public enum C200318_WorkModality
  {
    NORMAL,         //...Esecuzione automatica degli ordini
    EMERGENCIAL     //...Esecuzione degli ordini in modalità emergenziale: prelievi in manuale

  }
}
