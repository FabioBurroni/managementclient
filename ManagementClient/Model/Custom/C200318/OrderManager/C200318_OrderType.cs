﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.OrderManager
{
  public enum C200318_OrderType
  {
    AUTO, //...ordine di prelievo da magazzino centrale
    MNL,  //...ordine di prelievo manuale
    SRV,  //...non usata in SACI		
  }
}
