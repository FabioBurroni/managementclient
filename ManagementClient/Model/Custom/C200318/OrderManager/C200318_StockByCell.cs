﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.OrderManager
{
  public class C200318_StockByCell
  {
    public int NumPallet { get; set; } = 0;
    public string CellCode { get; set; }



    public override string ToString()
    {
      return $"CellCode:{CellCode}, NumPallet:{NumPallet}";
    }
  }
}
