﻿

namespace Model.Custom.C200318.OrderManager
{
  public enum C200318_WorkModalitySwitchRequestOption
  {

    UNDEFINED = 0,
    OK = 1,
    FAIL = 2,    
    CANCELED = 4,
   
  }
}
