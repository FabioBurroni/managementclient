﻿using System;

namespace Model.Custom.C200318.OrderManager
{
  public class C200318_WorkModalityConfiguration : ModelBase
  {
    private C200318_WorkModality _Current;
    public C200318_WorkModality Current
    {
      get { return _Current; }
      set
      {
        if (value != _Current)
        {
          _Current = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsSwitching");
        }
      }
    }

    private DateTime _CurrentLastUpdate = DateTime.Now;
    public DateTime CurrentLastUpdate
    {
      get { return _CurrentLastUpdate; }
      set
      {
        if (value != _CurrentLastUpdate)
        {
          _CurrentLastUpdate = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200318_WorkModality _Requested;
    public C200318_WorkModality Requested
    {
      get { return _Requested; }
      set
      {
        if (value != _Requested)
        {
          _Requested = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsSwitching");
        }
      }
    }

    private DateTime _RequestedLastUpdate = DateTime.Now;
    public DateTime RequestedLastUpdate
    {
      get { return _RequestedLastUpdate; }
      set
      {
        if (value != _RequestedLastUpdate)
        {
          _RequestedLastUpdate = value;
          NotifyPropertyChanged();
        }
      }
    }

    public bool IsSwitching
    {
      get { return Current != Requested; }
      
    }

    public void Update(C200318_WorkModalityConfiguration newRouting)
    {
      this.Current = newRouting.Current;
      this.CurrentLastUpdate = newRouting.CurrentLastUpdate;
      this.Requested = newRouting.Requested;
      this.RequestedLastUpdate = newRouting.RequestedLastUpdate;

    }

    public void Reset()
    {
      this.Current = C200318_WorkModality.NORMAL;
      this.CurrentLastUpdate = DateTime.Now;
      this.Requested = C200318_WorkModality.NORMAL;
      this.RequestedLastUpdate = DateTime.Now;


    }
  }
}
