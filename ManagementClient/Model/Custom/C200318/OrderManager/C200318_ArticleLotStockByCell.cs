﻿
namespace Model.Custom.C200318.OrderManager
{
  public class C200318_ArticleLotStockByCell
  {
    public string ArticleCode { get; set; }
    public string ArticleDescr { get; set; }
    public string LotCode { get; set; }
    public ObservableCollectionFast<C200318_StockByCell> StockByCellL { get; set; } = new ObservableCollectionFast<C200318_StockByCell>();
  }
}
