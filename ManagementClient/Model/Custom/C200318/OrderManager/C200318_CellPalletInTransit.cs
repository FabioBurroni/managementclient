﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Model.Custom.C200318.OrderManager
{
  public class C200318_CellPalletInTransit : ModelBase
  {

   
    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }
  


    private C200318_Order _Order;
    public C200318_Order Order
    {
      get { return _Order; }
      set
      {
        if (value != _Order)
        {
          _Order = value;
          NotifyPropertyChanged();
        }
      }
    }

   
   public ObservableCollectionFast<C200318_PalletInTransit> PalletInTransitL { get; set; } = new ObservableCollectionFast<C200318_PalletInTransit>();





  }
}
