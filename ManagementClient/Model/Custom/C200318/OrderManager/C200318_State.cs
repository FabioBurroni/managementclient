﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.OrderManager
{
  public enum C200318_State
  {
    // LIST
    LWAIT = 1,
    LEDIT = 2,
    LEXEC = 3,
    LKILL = 4,
    LDONE = 5,
    LPAUSE = 6,
    LUNCOMPLETE = 7,

    //SERVICE
    SERVICE = 10,

    // COMPONENTLIST
    CWAIT = 11,
    CEXEC = 13,
    CKILL = 14,
    CDONE = 15,
    CPAUSE = 16,
  }
}
