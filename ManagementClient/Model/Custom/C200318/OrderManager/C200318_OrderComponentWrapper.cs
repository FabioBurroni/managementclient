﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.OrderManager
{
  public class C200318_OrderComponentWrapper:ModelBase
  {
    public string ArticleCode { get; set; } = string.Empty;
    public string ArticleDescr { get; set; } = string.Empty;

    public string LotCode { get; set; } = string.Empty;

    private int _QtyRequested;
    public int QtyRequested
    {
      get { return _QtyRequested; }
      set
      {
        if (value != _QtyRequested)
        {
          _QtyRequested = value;
          NotifyPropertyChanged();
        }
      }
    }

    //...19/04/2022
    private int _Karina_Qty = 0;

    public int Karina_Qty
        {
        get { return _Karina_Qty; }
        set 
        { 
          if(value != _Karina_Qty)
          {
            _Karina_Qty = value;
            NotifyPropertyChanged();
          }
        }
    }



        private C200318_ListCmpTypeEnum _CmpType = C200318_ListCmpTypeEnum.FIFO;
    public C200318_ListCmpTypeEnum CmpType
    {
      get { return _CmpType; }
      set
      {
        if (value != _CmpType)
        {
          _CmpType = value;
          NotifyPropertyChanged();
        }
      }
    }


    //private C200318_ListCmpUnitOfMeasureEnum _Unit = C200318_ListCmpUnitOfMeasureEnum.P;
    //public C200318_ListCmpUnitOfMeasureEnum Unit
    //{
    //  get { return _Unit; }
    //  set
    //  {
    //    if (value != _Unit)
    //    {
    //      _Unit = value;
    //      NotifyPropertyChanged();
    //    }
    //  }
    //}

    public override string ToString()
    {
      return $"ArticleCode:{ArticleCode}, ArticleDescr:{ArticleDescr}, LotCode:{LotCode}, QtyRequested:{QtyRequested}, Karina_Qty:{Karina_Qty}";
    }
  }
}
