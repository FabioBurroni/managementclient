﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.OrderManager
{
  public enum C200318_ListCmpTypeEnum
  {
    FIFO = 1,
    FEFO = 2,
  }
}
