﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.OrderManager
{
  public class C200318_OrderWrapper:ModelBase
  {
    public C200318_OrderType OrderType = C200318_OrderType.MNL;
    public ObservableCollectionFast<C200318_OrderComponentWrapper> CmpL { get; set; } = new ObservableCollectionFast<C200318_OrderComponentWrapper>();


    public C200318_OrderWrapper()
    {
      CmpL.CollectionChanged += CmpL_CollectionChanged;
    }

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsValid");
        }
      }
    }


    private string _Destination = string.Empty;
    public string Destination
    {
      get { return _Destination; }
      set
      {
        if (value != _Destination)
        {
          _Destination = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Priority;
    public int Priority
    {
      get { return _Priority; }
      set
      {
        if (value != _Priority)
        {
          _Priority = value;
          NotifyPropertyChanged();
        }
      }
    }



    private void CmpL_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
      NotifyPropertyChanged("IsValid");
    }

    public bool IsValid
    {
      get
      {
        return CmpL.Count > 0 && !string.IsNullOrEmpty(Code);
      }
    }

    
    

  
    public void Reset()
    {
      CmpL.Clear();
      Code= string.Empty;
      Destination = string.Empty;
      Priority= 0;

    }

  }
}
