﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.OrderManager
{
  public class C200318_PalletInTransit:C200318_Pallet,IUpdatable
  {

    public C200318_PalletInTransit(C200318_Pallet pallet)
    {
      this.Code = pallet.Code;
      this.DateBorn = pallet.DateBorn;
      this.IsService = pallet.IsService;
      this.CustomResult = pallet.CustomResult;
      this.IsCustomReject = pallet.IsCustomReject;
      this.IsCustomReject = pallet.IsCustomReject;
      this.ProductionDate = pallet.ProductionDate;
      this.ExpiryDate= pallet.ExpiryDate;
      this.IsReject = pallet.IsReject;
      this.RejectResult = pallet.RejectResult;
      this.ArticleCode = pallet.ArticleCode;
      this.ArticleDescr = pallet.ArticleDescr;
      this.LotCode = pallet.LotCode;
      this.State = pallet.State;
      this.Days_Spent_FromProduction = pallet.Days_Spent_FromProduction;
      this.Days_to_Expiry= pallet.Days_to_Expiry;
      this.FinalDestination = pallet.FinalDestination;
    }

    private string _PositionCode;
    public string PositionCode
    {
      get { return _PositionCode; }
      set
      {
        if (value != _PositionCode)
        {
          _PositionCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200318_WorkModality _workModality;
    public C200318_WorkModality WorkModality
    {
      get { return _workModality; }
      set
      {
        if (value != _workModality)
        {
          _workModality = value;
          NotifyPropertyChanged();
        }
      }
    }


    public void Update(object newElement)
    {
      this.PositionCode = ((C200318_PalletInTransit)newElement).PositionCode;
    }
  }
}
