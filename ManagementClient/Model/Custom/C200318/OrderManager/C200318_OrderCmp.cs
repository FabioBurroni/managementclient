﻿using System;

namespace Model.Custom.C200318.OrderManager
{
  public class C200318_OrderCmp : ModelBase
  {

    private int _Id;
    public int Id
    {
      get { return _Id; }
      set
      {
        if (value != _Id)
        {
          _Id = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C200318_State _State;
    public C200318_State State
    {
      get { return _State; }
      set
      {
        if (value != _State)
        {
          _State = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }



    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _LotCode;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _QtyRequested;
    public int QtyRequested
    {
      get { return _QtyRequested; }
      set
      {
        if (value != _QtyRequested)
        {
          _QtyRequested = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _QtyDelivered;
    public int QtyDelivered
    {
      get { return _QtyDelivered; }
      set
      {
        if (value != _QtyDelivered)
        {
          _QtyDelivered = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _NumPalletInTransit;
    public int NumPalletInTransit
    {
      get { return _NumPalletInTransit; }
      set
      {
        if (value != _NumPalletInTransit)
        {
          _NumPalletInTransit = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _QtyRemaining;
    public int QtyRemaining
    {
      get { return _QtyRemaining; }
      set
      {
        if (value != _QtyRemaining)
        {
          _QtyRemaining = value;
          NotifyPropertyChanged();
        }
      }
    }



    private C200318_ExecutionResult _ExecutionResult;
    public C200318_ExecutionResult ExecutionResult
    {
      get { return _ExecutionResult; }
      set
      {
        if (value != _ExecutionResult)
        {
          _ExecutionResult = value;
          NotifyPropertyChanged();
        }
      }
    }


    private DateTime _LastScanningDateTime;
    public DateTime LastScanningDateTime
    {
      get { return _LastScanningDateTime; }
      set
      {
        if (value != _LastScanningDateTime)
        {
          _LastScanningDateTime = value;
          NotifyPropertyChanged();
        }
      }
    }


    //20200903
    private C200318_ListCmpTypeEnum _CmpType = C200318_ListCmpTypeEnum.FIFO;
    public C200318_ListCmpTypeEnum CmpType
    {
      get { return _CmpType; }
      set
      {
        if (value != _CmpType)
        {
          _CmpType = value;
          NotifyPropertyChanged();

        }
      }
    }

    //20200903

    //private C200318_ListCmpUnitOfMeasureEnum _UnitOfMeasure = C200318_ListCmpUnitOfMeasureEnum.P;
    //public C200318_ListCmpUnitOfMeasureEnum UnitOfMeasure
    //{
    //  get { return _UnitOfMeasure; }
    //  set
    //  {
    //    if (value != _UnitOfMeasure)
    //    {
    //      _UnitOfMeasure = value;
    //      NotifyPropertyChanged();

    //    }
    //  }
    //}



    public void Update(C200318_OrderCmp cmpNew)
    {
      this.State = cmpNew.State;
      this.QtyDelivered = cmpNew.QtyDelivered;
      this.QtyRemaining= cmpNew.QtyRemaining;
      this.NumPalletInTransit= cmpNew.NumPalletInTransit;
      this.LastScanningDateTime = cmpNew.LastScanningDateTime;
      this.ExecutionResult = cmpNew.ExecutionResult;
      this.CmpType = cmpNew.CmpType;
      //this.UnitOfMeasure= cmpNew.UnitOfMeasure;

    }






  }
}
