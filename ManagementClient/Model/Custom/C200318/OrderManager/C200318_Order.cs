﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Model.Custom.C200318.OrderManager
{
  public class C200318_Order : ModelBase
  {

    private int _Id;
    public int Id
    {
      get { return _Id; }
      set
      {
        if (value != _Id)
        {
          _Id = value;
          NotifyPropertyChanged();
        }
      }
    }
    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }
    private string _DestinationMachine;
    public string DestinationMachine
    {
      get { return _DestinationMachine; }
      set
      {
        if (value != _DestinationMachine)
        {
          _DestinationMachine = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C200318_State _State;
    public C200318_State State
    {
      get { return _State; }
      set
      {
        if (value != _State)
        {
          _State = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200318_ListDestination _Destination = new C200318_ListDestination();
    public C200318_ListDestination Destination
    {
      get { return _Destination; }
      set
      {
        if (value != _Destination)
        {
          _Destination = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200318_ExecutionModality _ExecutionModality;
    public C200318_ExecutionModality ExecutionModality
    {
      get { return _ExecutionModality; }
      set
      {
        if (value != _ExecutionModality)
        {
          _ExecutionModality = value;
          NotifyPropertyChanged();
        }
      }
    }



    private C200318_OrderType _OrderType;
    public C200318_OrderType OrderType
    {
      get { return _OrderType; }
      set
      {
        if (value != _OrderType)
        {
          _OrderType = value;
          NotifyPropertyChanged();
        }
      }
    }


    private DateTime _DateBorn;
    public DateTime DateBorn
    {
      get { return _DateBorn; }
      set
      {
        if (value != _DateBorn)
        {
          _DateBorn = value;
          NotifyPropertyChanged();
        }
      }
    }


    private DateTime _DateSend;
    public DateTime DateSend
    {
      get { return _DateSend; }
      set
      {
        if (value != _DateSend)
        {
          _DateSend = value;
          NotifyPropertyChanged();
        }
      }
    }


    private DateTime _DateExec;
    public DateTime DateExec
    {
      get { return _DateExec; }
      set
      {
        if (value != _DateExec)
        {
          _DateExec = value;
          NotifyPropertyChanged();
        }
      }
    }


    private DateTime _DateEnd;
    public DateTime DateEnd
    {
      get { return _DateEnd; }
      set
      {
        if (value != _DateEnd)
        {
          _DateEnd = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _priority;
    public int Priority
    {
      get { return _priority; }
      set
      {
        if (value != _priority)
        {
          _priority = value;
          NotifyPropertyChanged();
        }
      }
    }


    
    private C200318_ExecutionResult _ExecutionResult;
    public C200318_ExecutionResult ExecutionResult
    {
      get { return _ExecutionResult; }
      set
      {
        if (value != _ExecutionResult)
        {
          _ExecutionResult= value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _LastScanningDateTime;
    public DateTime LastScanningDateTime
    {
      get { return _LastScanningDateTime; }
      set
      {
        if (value != _LastScanningDateTime)
        {
          _LastScanningDateTime = value;
          NotifyPropertyChanged();
        }
      }
    }
    #region PROPRIETA' CALCOLATE
    public int NumPalletInTransit
    {
      get
      {
        return CmpL.Sum(cmp => cmp.NumPalletInTransit);
      }
    }

    public int TotalQtyDelivered
    {
      get
      {
        return CmpL.Sum(cmp => cmp.QtyDelivered);
      }
    }

    public int TotalQuantityRequested
    {
      get
      {
        return CmpL.Sum(cmp => cmp.QtyRequested);
      }
    }

    public int TotalQuantityRemaining
    {
      get
      {
        return CmpL.Sum(cmp => cmp.QtyRemaining);
      }
    } 

    public bool HasDestinationValid
    {
      get
      {
        return !string.IsNullOrEmpty(Destination.Code);
      }
    }

    #endregion




    public void Update(C200318_Order orderNew)
    {
      this.DateSend = orderNew.DateSend;
      this.DateExec= orderNew.DateExec;
      this.DateEnd= orderNew.DateEnd;
      this.State = orderNew.State;
      this.Priority = orderNew.Priority;
      this.LastScanningDateTime = orderNew.LastScanningDateTime;
      this.ExecutionResult= orderNew.ExecutionResult;
      Destination_Update(orderNew.Destination);
      
      List<C200318_OrderCmp> cmpToAdd = new List<C200318_OrderCmp>();
      foreach (var cmpNew in orderNew.CmpL)
      {
        var c = CmpL.FirstOrDefault(cmp => cmp.Id == cmpNew.Id);
        if (c != null)
          c.Update(cmpNew);
        else
          cmpToAdd.Add(cmpNew);
      }
      cmpToAdd.ForEach(cmp => CmpL.Add(cmp));


      CollectionHelper.UpdateAll<C200318_PalletInTransit>(PalletInTransitL, orderNew.PalletInTransitL,"Code");

      NotifyPropertyChanged("NumPalletInTransit");
      NotifyPropertyChanged("TotalQuantityRequested");
      NotifyPropertyChanged("TotalQtyDelivered");
      NotifyPropertyChanged("TotalQuantityRemaining");
      NotifyPropertyChanged("HasDestinationValid");
      
    }

    public void Destination_Update(C200318_ListDestination newDestination)
    {
      if(Destination.Code!=newDestination.Code)
      {
        Destination = newDestination;
      }
    }


    public ObservableCollectionFast<C200318_OrderCmp> CmpL { get; set; } = new ObservableCollectionFast<C200318_OrderCmp>();
    public ObservableCollectionFast<C200318_PalletInTransit> PalletInTransitL { get; set; } = new ObservableCollectionFast<C200318_PalletInTransit>();





  }
}
