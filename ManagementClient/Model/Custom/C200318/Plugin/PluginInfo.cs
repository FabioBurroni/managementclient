﻿using System.Collections.ObjectModel;

namespace Model.Custom.C200318.Plugin
{
	public class PluginInfo : ModelBase
	{
		public PluginInfo(string name)
		{
			Name = name;
		}

		/// <summary>
		/// Nome della classe del plugin
		/// </summary>
		public string Name { get; }

		private string _fileName;
		/// <summary>
		/// Nome del file della dll
		/// </summary>
		public string FileName
		{
			get
			{
				return _fileName;
			}
			set
			{
				if (_fileName != value)
				{
					_fileName = value;
					NotifyPropertyChanged();
				}
			}
		}

		private string _version;
		/// <summary>
		/// Nome del file della dll
		/// </summary>
		public string Version
		{
			get
			{
				return _version;
			}
			set
			{
				if (_version != value)
				{
					_version = value;

					NotifyPropertyChanged();
				}
			}
		}

		private Plugin_State _status;
		public Plugin_State Status
		{
			get
			{
				return _status;
			}
			set
			{
				if (_status != value)
				{
					_status = value;
					NotifyPropertyChanged();
				}
			}
		}

		public ObservableCollection<string> XmlCommands { get; set; } = new ObservableCollection<string>();

		public void Update(PluginInfo plg)
		{
			Status = plg.Status;
			FileName = plg.FileName;
		}
	}

	public enum Plugin_State
	{
		INITIALIZING,     //...plugin is initializing 
		STARTED,          //...plugin is started
		STOPPING,         //...plugin is stopping, wait for thread exit
		STOPPED,          //...plugin is stopped
		DISABLED          //...plugin is disabled, thread is running but no operation is performed, xmlcommand are bypassed
	}
}