﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Model.Common;


namespace Model.Custom.C200318
{
  public class C200318_PositionOut_Pallet:ModelBase
  {
    public C200318_PositionOut_Pallet()
    {

    }

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _LotCode;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _LotLength;
    public int LotLength
    {
      get { return _LotLength; }
      set
      {
        if (value != _LotLength)
        {
          _LotLength = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _Height;
    public int Height
    {
      get { return _Height; }
      set
      {
        if (value != _Height)
        {
          _Height = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _Weight;
    public int Weight
    {
      get { return _Weight; }
      set
      {
        if (value != _Weight)
        {
          _Weight = value;
          NotifyPropertyChanged();
        }
      }
    }

    //TODO: usare un converter
    //private string _WeightS = string.Empty;
    //public string WeightS
    //{
    //  get { return _WeightS; }
    //  set
    //  {
    //    if (value != _WeightS)
    //    {
    //      try
    //      {
    //        _WeightS = value.Substring(0, 3);
    //      }
    //      catch (Exception ex)
    //      {
    //        var exc = ex.ToString();
    //        _WeightS = value;
    //      }
    //      NotifyPropertyChanged();
    //    }
    //  }
    //}



    private bool _IsService;
    public bool IsService
    {
      get { return _IsService; }
      set
      {
        if (value != _IsService)
        {
          _IsService = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200318_CustomResult _CustomResult;
    public C200318_CustomResult CustomResult
    {
      get { return _CustomResult; }
      set
      {
        if (value != _CustomResult)
        {
          _CustomResult = value;
          NotifyPropertyChanged();
          IsCustomReject = _CustomResult != C200318_CustomResult.OK;
        }
      }
    }
    
    private bool _IsCustomReject;
    public bool IsCustomReject
    {
      get { return _IsCustomReject; }
      set
      {
        if (value != _IsCustomReject)
        {
          _IsCustomReject = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsReject;
    public bool IsReject
    {
      get { return _IsReject; }
      set
      {
        if (value != _IsReject)
        {
          _IsReject = value;
          NotifyPropertyChanged();
          IsCustomReject = IsCustomReject && !IsReject;
        }
      }
    }

    private Results_Enum _RejectResult;
    public Results_Enum RejectResult
    {
      get { return _RejectResult; }
      set
      {
        if (value != _RejectResult)
        {
          _RejectResult = value;
          NotifyPropertyChanged();
        }
      }
    }

    #region DATI ORDINE

    private bool _HasOrder;
    public bool HasOrder
    {
      get { return _HasOrder; }
      set
      {
        if (value != _HasOrder)
        {
          _HasOrder = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _OrderCode = string.Empty;
    public string OrderCode
    {
      get { return _OrderCode; }
      set
      {
        if (value != _OrderCode)
        {
          _OrderCode = value;
          NotifyPropertyChanged();
        }
      }
    }
    
    private string _OrderDestinationMachine = string.Empty;
    public string OrderDestinationMachine
    {
      get { return _OrderDestinationMachine; }
      set
      {
        if (value != _OrderDestinationMachine)
        {
          _OrderDestinationMachine = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _OrderCmpArticleCode = string.Empty;
    public string OrderCmpArticleCode
    {
      get { return _OrderCmpArticleCode; }
      set
      {
        if (value != _OrderCmpArticleCode)
        {
          _OrderCmpArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _OrderCmpArticleDescr = string.Empty;
    public string OrderCmpArticleDescr
    {
      get { return _OrderCmpArticleDescr; }
      set
      {
        if (value != _OrderCmpArticleDescr)
        {
          _OrderCmpArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _OrderCmpLotCode = string.Empty;
    public string OrderCmpLotCode
    {
      get { return _OrderCmpLotCode; }
      set
      {
        if (value != _OrderCmpLotCode)
        {
          _OrderCmpLotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _OrderCmpQtyRequested = 0;
    public int OrderCmpQtyRequested
    {
      get { return _OrderCmpQtyRequested; }
      set
      {
        if (value != _OrderCmpQtyRequested)
        {
          _OrderCmpQtyRequested = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _OrderCmpQuantityDelivered = 0;
    public int OrderCmpQuantityDelivered
    {
      get { return _OrderCmpQuantityDelivered; }
      set
      {
        if (value != _OrderCmpQuantityDelivered)
        {
          _OrderCmpQuantityDelivered = value;
          NotifyPropertyChanged();
        }
      }
    }

    
    #endregion


    #region Metodi Pubblici
    public void Update(C200318_PositionOut_Pallet newPallet)
    {
      this.Code = newPallet.Code;
      this.ArticleCode = newPallet.ArticleCode;
      this.ArticleDescr = newPallet.ArticleDescr;
      this.LotCode = newPallet.LotCode;
      this.LotLength = newPallet.LotLength;
      this.Height = newPallet.Height;
      this.Weight = newPallet.Weight;
      this.IsService = newPallet.IsService;
      this.CustomResult = newPallet.CustomResult;
      this.IsCustomReject = newPallet.IsCustomReject;
      this.IsReject = newPallet.IsReject;
      this.RejectResult = newPallet.RejectResult;
      this.HasOrder = newPallet.HasOrder;
      this.OrderCode = newPallet.OrderCode;
      this.OrderDestinationMachine = newPallet.OrderDestinationMachine;
      this.OrderCmpArticleCode = newPallet.OrderCmpArticleCode;
      this.OrderCmpArticleDescr = newPallet.OrderCmpArticleDescr;
      this.OrderCmpLotCode = newPallet.OrderCmpLotCode;
      this.OrderCmpQtyRequested = newPallet.OrderCmpQtyRequested;
      this.OrderCmpQuantityDelivered = newPallet.OrderCmpQuantityDelivered;

    }



    #endregion




    public void Reset()
    {
      this.Code = string.Empty;
      this.ArticleCode = string.Empty;
      this.ArticleDescr = string.Empty;
      this.LotCode = string.Empty;
      this.LotLength = 0;
      this.Height = 0; 
      this.Weight = 0;
      this.IsService = false; 
      this.CustomResult = C200318_CustomResult.UNDEFINED;
      this.IsCustomReject = false;
      this.IsReject = false;
      this.RejectResult = Results_Enum.UNDEFINED;
      this.HasOrder = false;
      this.OrderCode = string.Empty;
      this.OrderDestinationMachine = string.Empty;
      this.OrderCmpArticleCode = string.Empty;
      this.OrderCmpArticleDescr = string.Empty;
      this.OrderCmpLotCode = string.Empty;
      this.OrderCmpQtyRequested = 0;
      this.OrderCmpQuantityDelivered = 0;
    }
  }
}
