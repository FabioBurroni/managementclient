﻿using System;

namespace Model.Custom.C200318.Report
{
  public class C200318_PalletOut
  {
    public DateTime DateBorn { get; set; }
    public string PalletCode { get; set; }
    public string LotCode { get; set; }
    public int Qty { get; set; }
    public string ArticleCode { get; set; }
    public string Position { get; set; }
    public string OrderCode { get; set; }
    public string CellCode { get; set; }
    //20210901_1148 Added Property KarinaId
    public int KarinaId { get; set; }
    //...21/04/2022 added property 
    public int Karina_Qty_For_Pallet { get; set; }
  }
}
