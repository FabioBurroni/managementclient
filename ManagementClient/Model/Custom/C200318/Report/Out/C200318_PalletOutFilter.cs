﻿using System;
using System.Collections.Generic;
using Utilities.Extensions;


namespace Model.Custom.C200318.Report
{
  public class C200318_PalletOutFilter : ModelBase
  {

    public C200318_PalletOutFilter()
    {
      Reset();
    }

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; }
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000, 5000, 10000, 30000 };
    #endregion

    private string _PalletCode = string.Empty;
    public string PalletCode
    {
      get { return _PalletCode; }
      set
      {
        if (value != _PalletCode)
        {
          _PalletCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    //20210901_1148 Added Public Property KarinaId

    private string _KarinaId = string.Empty;
    public string KarinaId
    {
      get { return _KarinaId; }
      set
      {
        if (value != _KarinaId)
        {
          _KarinaId = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _LotCode = string.Empty;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode = string.Empty;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200318_Position _Position = new C200318_Position();
    public C200318_Position Position
    {
      get { return _Position; }
      set
      {
        if (value != _Position)
        {
          _Position = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _OrderCode = string.Empty;
    public string OrderCode
    {
      get { return _OrderCode; }
      set
      {
        if (value != _OrderCode)
        {
          _OrderCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _DateRangeSelected;
    public bool DateRangeSelected
    {
      get { return _DateRangeSelected; }
      set
      {
        if (value != _DateRangeSelected)
        {
          _DateRangeSelected = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateBornMin;
    public DateTime DateBornMin
    {
      get { return _DateBornMin; }
      set
      {
        if (value != _DateBornMin)
        {
          _DateBornMin = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateBornMax;
    public DateTime DateBornMax
    {
      get { return _DateBornMax; }
      set
      {
        if (value != _DateBornMax)
        {
          _DateBornMax = value;
          NotifyPropertyChanged();
        }
      }
    }
    
    #region public methods
    public void Reset()
    {
      PalletCode = string.Empty;
      ArticleCode = string.Empty;
      DateBornMin = DateTime.Now;
      DateBornMax = DateTime.Now;
      Position = new C200318_Position();
      OrderCode = string.Empty;
    }
    #endregion

    private DateTime _defaultDateTime = DateTime.MinValue;

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        PalletCode==null?"":PalletCode.Base64Encode(),
        LotCode==null?"":LotCode.Base64Encode(),
        ArticleCode==null?"":ArticleCode.Base64Encode(),
        DateRangeSelected ? DateBornMin.ConvertToDateTimeFormatString():_defaultDateTime.ConvertToDateTimeFormatString(),
        DateRangeSelected ? DateBornMax.ConvertToDateTimeFormatString():_defaultDateTime.ConvertToDateTimeFormatString(),
        Position==null?"":Position.Code.Base64Encode(),
        OrderCode==null?"":OrderCode.Base64Encode(),
        string.IsNullOrEmpty(KarinaId) ? "0":KarinaId.ToString()
      };
    }

  }
}
