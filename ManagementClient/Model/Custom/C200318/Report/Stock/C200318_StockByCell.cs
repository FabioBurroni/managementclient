﻿using System;
namespace Model.Custom.C200318.Report
{
  public class C200318_StockByCell:ModelBase
  {

    private string _CellCode;
    public string CellCode
    {
      get { return _CellCode; }
      set
      {
        if (value != _CellCode)
        {
          _CellCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _LotCode;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _FinestraStoccaggio;
    public int FinestraStoccaggio
    {
      get { return _FinestraStoccaggio; }
      set
      {
        if (value != _FinestraStoccaggio)
        {
          _FinestraStoccaggio = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxPalletAllowed;
    public int MaxPalletAllowed
    {
      get { return _MaxPalletAllowed; }
      set
      {
        if (value != _MaxPalletAllowed)
        {
          _MaxPalletAllowed = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _PalletCount;
    public int PalletCount
    {
      get { return _PalletCount; }
      set
      {
        if (value != _PalletCount)
        {
          _PalletCount = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _PalletFirstProductionDate;
    public DateTime PalletFirstProductionDate
    {
      get { return _PalletFirstProductionDate; }
      set
      {
        if (value != _PalletFirstProductionDate)
        {
          _PalletFirstProductionDate = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Filling;
    /// <summary>
    /// Percentuiale di riempimento
    /// </summary>
    public int Filling
    {
      get 
      {
        var perc = ((double)PalletCount / (double)MaxPalletAllowed) * 100;
        _Filling = (int)Math.Floor((double)perc);
        return _Filling;

      }
    }


    public bool Completa
    {
      get 
      { 
        return PalletCount==MaxPalletAllowed; 
      }
    }

    private DateTime _PalletLastProductionDate;
    public DateTime PalletLastProductionDate
    {
      get { return _PalletLastProductionDate; }
      set
      {
        if (value != _PalletLastProductionDate)
        {
          _PalletLastProductionDate = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _NumDays;
    public int NumDays
    {
      get { return _NumDays; }
      set
      {
        if (value != _NumDays)
        {
          _NumDays = value;
          NotifyPropertyChanged();
        }
      }
    }



    private bool _IsSelected = false;
    public bool IsSelected
    {
      get { return _IsSelected; }
      set
      {
        if (value != _IsSelected)
        {
          _IsSelected = value;
          NotifyPropertyChanged();
        }
      }
    }

    public int SelectionSorting
    {
      get
      {
        return _IsSelected ? 1 : 0;
      }
    }

    public double NumDaysFromToday
    {
      get 
      { 
        return Math.Floor((DateTime.Now - _PalletLastProductionDate).TotalDays); 
      }
    }


  }
}
