﻿

namespace Model.Custom.C200318.Report
{
  public class C200318_PalletFlow
  {
    public ObservableCollectionFast<C200318_PalletIn> listIn = new ObservableCollectionFast<C200318_PalletIn>();
    public ObservableCollectionFast<C200318_PalletOut> listOut = new ObservableCollectionFast<C200318_PalletOut>();
    public ObservableCollectionFast<C200318_PalletReject> listReject = new ObservableCollectionFast<C200318_PalletReject>();

  }
}
