﻿
namespace Model.Custom.C200318.Report
{
  public class C200318_DistributionLotArticle : C200318_StockLotArticle
  {
    private bool _isSelected;
    public bool IsSelected
    {
      get { return _isSelected; }
      set
      {
        if (value != _isSelected)
        {
          _isSelected = value;
          NotifyPropertyChanged();
        }
      }
    }

   

  }
  
}
