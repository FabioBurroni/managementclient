﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Model.Custom.C200318.Report.Log
{
  public class C200318_LogFilter:ModelBase
  {

    public C200318_LogFilter()
    {
      Reset();
    }

    private bool _DateRangeSelected;
    public bool DateRangeSelected
    {
      get { return _DateRangeSelected; }
      set
      {
        if (value != _DateRangeSelected)
        {
          _DateRangeSelected = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateStart;
    public DateTime DateStart
    {
      get { return _DateStart; }
      set
      {
        if (value != _DateStart)
        {
          _DateStart = value;
          NotifyPropertyChanged();
        }
      }
    }



    private DateTime _DateEnd;
    public DateTime DateEnd
    {
      get { return _DateEnd; }
      set
      {
        if (value != _DateEnd)
        {
          _DateEnd = value;
          NotifyPropertyChanged();
        }
      }
    }

    #region Paginazione

    private bool _PrevEnabled;
    public bool PrevEnabled
    {
      get { return _PrevEnabled; }
      set
      {
        if (value != _PrevEnabled)
        {
          _PrevEnabled = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _NextEnabled;
    public bool NextEnabled
    {
      get { return _NextEnabled; }
      set
      {
        if (value != _NextEnabled)
        {
          _NextEnabled = value;
          NotifyPropertyChanged();
        }
      }
    }



    public int TotalItems
    {
      set
      {
        if (value > MaxItems)
          NextEnabled = true;
        else
          NextEnabled = false;

        if (Index > 0)
          PrevEnabled = true;
        else
          PrevEnabled = false;


      }
    }

    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; } = new ObservableCollectionFast<int>() { 10, 100, 500, 1000, 5000, 10000, 30000 };
    #endregion


    private DateTime _defaultDateTime = DateTime.MinValue;

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        DateRangeSelected ? DateStart.ConvertToDateTimeFormatString() : _defaultDateTime.ConvertToDateTimeFormatString(),
        DateRangeSelected ? DateEnd.ConvertToDateTimeFormatString() : _defaultDateTime.ConvertToDateTimeFormatString(),
      };
    }


    public void Reset()
    {
      DateRangeSelected = false;
      DateStart = DateTime.Now;
      DateEnd = DateTime.Now;
    }

  }
}
