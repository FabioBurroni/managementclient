﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.Report.Log
{
  public class C200318_LogXmlCommand:ModelBase
  {


    private DateTime _DateBorn;
    public DateTime DateBorn
    {
      get { return _DateBorn; }
      set
      {
        if (value != _DateBorn)
        {
          _DateBorn = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _Command;
    public string Command
    {
      get { return _Command; }
      set
      {
        if (value != _Command)
        {
          _Command = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _User;
    public string User
    {
      get { return _User; }
      set
      {
        if (value != _User)
        {
          _User = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _Description;
    public string Description
    {
      get { return _Description; }
      set
      {
        if (value != _Description)
        {
          _Description = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _Parameters = string.Empty;
    public string Parameters
    {
      get { return _Parameters; }
      set
      {
        if (value != _Parameters)
        {
          _Parameters = value;
          NotifyPropertyChanged();
        }
      }
    }


  }
}
