﻿
using System.Collections.Generic;
using Utilities.Extensions;


namespace Model.Custom.C200318.Report
{
  public class C200318_PalletAllFilter : ModelBase
  {

    public C200318_PalletAllFilter()
    {
     
    }

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; } 
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000,5000,10000,30000};
    #endregion

    private string _PalletCode = string.Empty;
    public string PalletCode
    {
      get { return _PalletCode; }
      set
      {
        if (value != _PalletCode)
        {
          _PalletCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _LotCode = string.Empty;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleCode = string.Empty;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }



    private string _ArticleDescr = string.Empty;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }



    #region public methods
    public void Reset()
    {
      PalletCode = string.Empty;
      LotCode = string.Empty;
      ArticleCode = string.Empty;
      ArticleDescr= string.Empty;
    }
    #endregion

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        PalletCode==null?"":PalletCode.Base64Encode(),
        LotCode==null?"":LotCode.Base64Encode(),
        ArticleCode==null?"":ArticleCode.Base64Encode(),
        ArticleDescr==null?"":ArticleDescr.Base64Encode(),
      };
    }

  }
}
