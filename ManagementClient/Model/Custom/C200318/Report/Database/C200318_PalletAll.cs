﻿using System;

namespace Model.Custom.C200318.Report
{
  public class C200318_PalletAll : ModelBase
  {

    private string _PalletCode = string.Empty;
    public string PalletCode
    {
      get { return _PalletCode; }
      set
      {
        if (value != _PalletCode)
        {
          _PalletCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private DateTime _DateBorn = DateTime.MinValue;
    public DateTime DateBorn
    {
      get { return _DateBorn; }
      set
      {
        if (value != _DateBorn)
        {
          _DateBorn = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _LotCode;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }



    private DateTime _ProductionDate = DateTime.MinValue;
    public DateTime ProductionDate
    {
      get { return _ProductionDate; }
      set
      {
        if (value != _ProductionDate)
        {
          _ProductionDate = value;
          NotifyPropertyChanged();
        }
      }
    }



    private string _PositionCode;
    public string PositionCode
    {
      get { return _PositionCode; }
      set
      {
        if (value != _PositionCode)
        {
          _PositionCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _ExpiryDate = DateTime.MinValue;
    public DateTime ExpiryDate
    {
      get { return _ExpiryDate; }
      set
      {
        if (value != _ExpiryDate)
        {
          _ExpiryDate = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _Quantity;
    public int Quantity
    {
      get { return _Quantity; }
      set
      {
        if (value != _Quantity)
        {
          _Quantity = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _LotLength;
    public int LotLength
    {
      get { return _LotLength; }
      set
      {
        if (value != _LotLength)
        {
          _LotLength = value;
          NotifyPropertyChanged();
        }
      }
    }
  }
}
