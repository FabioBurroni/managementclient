﻿using System;

namespace Model.Custom.C200318.Report
{
  public class C200318_PalletReject
  {
    public DateTime DateBorn { get; set; }
    public string LotCode { get; set; }
    public string PalletCode { get; set; }
    public string ArticleCode { get; set; }
    public int Qty { get; set; }
    public string Position { get; set; }
    public string Reason { get; set; }
  }
}
