﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318
{
  public class C200318_PositionShapeControl_Pallet : ModelBase
  {

    private string _PositionCode;
    public string PositionCode
    {
      get { return _PositionCode; }
      set
      {
        if (value != _PositionCode)
        {
          _PositionCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _PalletCode=string.Empty;
    public string PalletCode
    {
      get { return _PalletCode; }
      set
      {
        if (value != _PalletCode)
        {
          _PalletCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsService;
    public bool IsService
    {
      get { return _IsService; }
      set
      {
        if (value != _IsService)
        {
          _IsService = value;
          NotifyPropertyChanged();
      }
    }
    }

    private C200318_CustomResult _CustomResult;
    public C200318_CustomResult CustomResult
    {
      get { return _CustomResult; }
      set
      {
        if (value != _CustomResult)
        {
          _CustomResult = value;
          NotifyPropertyChanged();
          if(_CustomResult!= C200318_CustomResult.OK)
          {
            IsCustomReject = true;
          }
          else
          {
            IsCustomReject = false;
          }
        }
      }
    }



    private bool _IsCustomReject;
    public bool IsCustomReject
    {
      get { return _IsCustomReject; }
      set
      {
        if (value != _IsCustomReject)
        {
          _IsCustomReject = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsReject;
    public bool IsReject
    {
      get { return _IsReject; }
      set
      {
        if (value != _IsReject)
        {
          _IsReject = value;
          NotifyPropertyChanged();
        }
      }
    }


    private Results_Enum _RejectResult;
    public Results_Enum RejectResult
    {
      get { return _RejectResult; }
      set
      {
        if (value != _RejectResult)
        {
          _RejectResult = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _LotCode;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _Quantity;
    public int Quantity
    {
      get { return _Quantity; }
      set
      {
        if (value != _Quantity)
        {
          _Quantity = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _LotLength;
    public int LotLength
    {
      get { return _LotLength; }
      set
      {
        if (value != _LotLength)
        {
          _LotLength = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _ExpiryDate;
    public DateTime ExpiryDate
    {
      get { return _ExpiryDate; }
      set
      {
        if (value != _ExpiryDate)
        {
          _ExpiryDate = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _Weight;
    public int Weight
    {
      get { return _Weight; }
      set
      {
        if (value != _Weight)
        {
          _Weight = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _Height;
    public int Height
    {
      get { return _Height; }
      set
      {
        if (value != _Height)
        {
          _Height = value;
          NotifyPropertyChanged();
        }
      }
    }

    
    public void Update(C200318_PositionShapeControl_Pallet newPallet)
    {
      this.PalletCode = newPallet.PalletCode;
      this.IsService = newPallet.IsService;
      this.CustomResult = newPallet.CustomResult;
      this.IsReject = newPallet.IsReject;
      this.RejectResult = newPallet.RejectResult;
      this.ArticleCode = newPallet.ArticleCode;
      this.ArticleDescr = newPallet.ArticleDescr;
      this.LotCode = newPallet.LotCode;
      this.Quantity = newPallet.Quantity;
      this.LotLength = newPallet.LotLength;
      this.ExpiryDate = newPallet.ExpiryDate;
      this.Weight = newPallet.Weight;
      this.Height = newPallet.Height;
    }

    public void Reset()
    {
      this.PalletCode = string.Empty;
      this.IsService = false;
      this.CustomResult = C200318_CustomResult.UNDEFINED;
      this.IsReject = false;
      this.RejectResult = Results_Enum.UNDEFINED;
      this.ArticleCode = string.Empty;
      this.ArticleDescr = string.Empty;
      this.LotCode = string.Empty;
      this.Quantity = 0;
      this.LotLength = 0;
      this.ExpiryDate = DateTime.MinValue;
      this.Weight = 0;
      this.Height = 0;
    }
  }
}
