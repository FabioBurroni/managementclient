﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Model.Custom.C200318.Administration
{
  public class C200318_Master:ModelBase, IUpdatable
  {
    public C200318_Master(string code, C200318_MasterCategory category)
    {
      _Code = code;
      _Category = category;
      OriginalCategory = category;
    }

    private string _Code;
    public string Code
    {
      get { return _Code; }
    }


    private C200318_MasterCategory _Category=C200318_MasterCategory.NORMAL;
    public C200318_MasterCategory Category
    {
      get { return _Category; }
      set
      {
        if (value != _Category)
        {
          _Category = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsChanged");
        }
      }
    }


    public C200318_MasterCategory OriginalCategory { get; set; }

    public bool IsChanged
    {
      get { return Category!=OriginalCategory; }
    }


    public void Update(object newElement)
    {
      Category = ((C200318_Master)newElement).Category;
      OriginalCategory = Category;
      NotifyPropertyChanged("IsChanged");
    }
  }


}
