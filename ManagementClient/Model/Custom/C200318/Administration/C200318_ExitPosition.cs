﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Model.Custom.C200318.Administration
{
  public class C200318_ExitPosition:ModelBase, IUpdatable
  {
    public C200318_ExitPosition(string code, int maxPalletInTransit)
    {
      _Code = code;
      MaxPalletInTransitOriginal = maxPalletInTransit;
      MaxPalletInTransit = maxPalletInTransit;
    }

    private string _Code;
    public string Code
    {
      get { return _Code; }
    }


    private int _MaxPalletInTransit;
    public int MaxPalletInTransit
    {
      get { return _MaxPalletInTransit; }
      set
      {
        if (value != _MaxPalletInTransit)
        {
          _MaxPalletInTransit = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsChanged");
        }
      }
    }

    private int MaxPalletInTransitOriginal { get; set; }

    public bool IsChanged =>  MaxPalletInTransit!=MaxPalletInTransitOriginal; 
    


    public void Update(object newElement)
    {
      MaxPalletInTransit = ((C200318_ExitPosition)newElement).MaxPalletInTransit;
      MaxPalletInTransitOriginal = MaxPalletInTransit;
      NotifyPropertyChanged("IsChanged");
    }
  }


}
