﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318
{
  public class C200318_WhArea:ModelBase
  {

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("Descr");
        }
      }
    }


    private C200318_WhAreaType _WhAreaType;
    public C200318_WhAreaType WhAreaType
    {
      get { return _WhAreaType; }
      set
      {
        if (value != _WhAreaType)
        {
          _WhAreaType = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("Descr");
        }
      }
    }


    private string _Descr;
    public string Descr
    {
      get { return $"{Code.ToUpper()} - {WhAreaType}"; }
    }
  }

  public enum C200318_WhAreaType
  {
    AUTOMATICO,
    MANUALE,
    ESTERNO,

  }
}
