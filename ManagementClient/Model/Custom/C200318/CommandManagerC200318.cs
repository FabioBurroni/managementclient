﻿using System;
using System.Linq;
using System.Collections.Generic;

using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;

using XmlCommunicationManager.Authorization;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.Encoding;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlClient.Event;

using Model.Common;
using Model.Common.Configuration;
using Model.Custom.C200318.Plugin;
using Model.Custom.C200318.OrderManager;
using Model.Custom.C200318.ArticleManager;
using Model.Custom.C200318.Filter;
using Model.Custom.C200318.Defrag;

using System.Collections.ObjectModel;
using System.Globalization;
using Model.Custom.C200318.Printer;
using Model.Custom.C200318.Label;
using Model.Custom.C200318.PalletManager;
using Model.Custom.C200318.Report;
using Model.Custom.C200318.WebCam;
using Model.Custom.C200318.Report.Log;

namespace Model.Custom.C200318
{
  public class CommandManagerC200318 : CommandManager
  {
    #region COSTRUTTORE

    public CommandManagerC200318(IXmlClient xmlClient, string commandHeader) : base(xmlClient, commandHeader)
    {
    }
    #endregion

    #region PLUGIN MANAGER

    public void Plugin_GetAll(object receiverInstance)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_GetAll"));
    }
    private IModel Plugin_GetAll(XmlCommandResponse resp)
    {
      var plgL = new List<PluginInfo>();
      foreach (var item in resp.Items)
      {
        plgL.Add(Plugin_FromXml(item));
      }
      return new DataWrapperResult(plgL);
    }

    public void Plugin_Get(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Get", pluginName));
    }
    private IModel Plugin_Get(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        dwr.Data = Plugin_FromXml(item);
      }
      return dwr;
    }

    public void Plugin_Check(object receiverInstance)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Check"));
    }

    private IModel Plugin_Check(XmlCommandResponse resp)
    {
      return new DataWrapperResult();
    }

    public void Plugin_Add(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Add", pluginName));
    }
    private IModel Plugin_Add(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }

    public void Plugin_Update(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Update", pluginName));
    }
    private IModel Plugin_Update(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }
    public void Plugin_Start(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Start", pluginName));
    }

    private IModel Plugin_Start(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }
    public void Plugin_Stop(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Stop", pluginName));
    }
    private IModel Plugin_Stop(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }

    public void Plugin_Enable(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Enable", pluginName));
    }
    private IModel Plugin_Enable(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }

    public void Plugin_Disable(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Disable", pluginName));
    }
    private IModel Plugin_Disable(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }

    public void Plugin_Unload(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Unload", pluginName));
    }
    private IModel Plugin_Unload(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }

    public void Plugin_XmlCommand_Get(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_XmlCommand_Get", pluginName));
    }
    private IModel Plugin_XmlCommand_Get(XmlCommandResponse resp)
    {
      List<string> xmlCommands = new List<string>();
      if (resp.itemCount == 1)
      {
        foreach (var field in resp.Items[0])
        {
          xmlCommands.Add(field);
        }
      }
      return new DataWrapperResult(xmlCommands);
    }

    public void Plugin_Command(object receiverInstance, params string[] parameters)
    {
      Plugin_Command(receiverInstance, parameters.EmptyIfNull());
    }
    public void Plugin_Command(object receiverInstance, IEnumerable<string> parameters, ICredentials advancedUserCredentials = null)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Command", parameters), advancedUserCredentials);
    }

    #endregion

    #region CONFIGURATION
    public void CO_Configuration_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Configuration_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Configuration_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<ConfParameterServer> cpL = new List<ConfParameterServer>();
      dwr.Data = cpL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          ConfParameterServer p = new ConfParameterServer();
          p.Name = item.getFieldVal<string>(startIndex++);
          p.OriginalValue = item.getFieldVal<string>(startIndex++);
          cpL.Add(p);
        }
      }
      return dwr;
    }

    public void CO_Configuration_Get(object receiverInstance, string parameterName)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Configuration_Get");
      parameters.Add(parameterName);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Configuration_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        ConfParameterServer p = new ConfParameterServer();
        p.Name = item.getFieldVal<string>(startIndex++);
        p.OriginalValue = item.getFieldVal<string>(startIndex++);

        dwr.Data = p;
      }
      return dwr;
    }


    public void CO_Configuration_Set(object receiverInstance, string parameterName, string parameterValue)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Configuration_Set");
      parameters.Add(parameterName);
      parameters.Add(parameterValue);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Configuration_Set(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        var result = FromXmlTo_Result(item, ref startIndex);
        dwr.Result = result.ToString();
      }
      return dwr;
    }




    #endregion

    #region PRINTER

    //CHK_Print
    public void CO_PrintRaw(object receiverInstance, string positionCode, string labelCode)
    {
      var parameters = new List<string>();
      parameters.Add("CO_PrintRaw");
      parameters.AddRange(new string[] { positionCode, labelCode });
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_PrintRaw(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var itemResult = resp.Items[0];
        int startIndex = 1;
        dwr.Result = itemResult.getFieldVal(startIndex++).ToString();
      }
      return dwr;

    }
    //CHK_Print
    public void CO_Print(object receiverInstance, string positionCode, string palletCode)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Print");
      parameters.AddRange(new string[] { positionCode, palletCode });
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Print(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var itemResult = resp.Items[0];
        int startIndex = 1;
        dwr.Result = itemResult.getFieldVal(startIndex++).ToString();
      }
      return dwr;
    }

    //  CHK_Printer_GetAll
    public void CO_Printer_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Printer_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Printer_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Printer> list = new List<C200318_Printer>();
      dwr.Data = list;
      foreach (var item in resp.Items)
      {
        int startIndex = 1;
        var element = FromXmlTo_Printer(item, ref startIndex);
        list.Add(element);
      }
      return dwr;
    }

    //  CHK_Printer_Get
    public void CO_Printer_Get(object receiverInstance, string positionCode)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Printer_Get");
      parameters.Add(positionCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Printer_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      if (resp.itemCount >= 2)
      {
        int startIndex = 1;
        dwr.Data = FromXmlTo_Printer(resp.Items[1], ref startIndex);
      }
      return dwr;
    }

    //  CHK_Printer_Delete
    public void CO_Printer_Delete(object receiverInstance, string positionCode)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Printer_Delete");
      parameters.Add(positionCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Printer_Delete(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      return dwr;
    }


    //  CHK_Printer_InsertOrUpdate
    public void CO_Printer_InsertOrUpdate(object receiverInstance, string positionCode, string ipAddress, int portNumber, string description, int timeout)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Printer_InsertOrUpdate");
      parameters.AddRange(new string[] { positionCode, ipAddress, portNumber.ToString(), description.Base64Encode(), timeout.ToString() });
      Plugin_Command(receiverInstance, parameters);
    }

    private IModel CO_Printer_InsertOrUpdate(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      return dwr;
    }

    private C200318_Printer FromXmlTo_Printer(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_Printer printer = new C200318_Printer();
      printer.PositionCode = item.getFieldVal(startIndex++);
      printer.Description = item.getFieldVal(startIndex++).Base64Decode();
      printer.IpAddress = item.getFieldVal(startIndex++);
      printer.PortNumber = item.getFieldVal<int>(startIndex++);
      printer.Timeout = item.getFieldVal<int>(startIndex++);
      return printer;
    }

    #endregion

    #region LABEL

    public void CO_Label_Delete(object receiverInstance, string code)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Label_Delete");
      parameters.Add(code);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Label_Delete(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      return dwr;
    }
    //  CHK_Label_GetAll
    public void CO_Label_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Label_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Label_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Label> list = new List<C200318_Label>();
      dwr.Data = list;
      foreach (var item in resp.Items)
      {
        int startIndex = 1;
        var element = FromXmlTo_Label(item, ref startIndex);
        list.Add(element);
      }
      return dwr;
    }

    //  CHK_Label_Get
    public void CO_Label_Get(object receiverInstance, string code)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Label_Get");
      parameters.Add(code);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Label_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      if (resp.itemCount >= 1)
      {
        int startIndex = 1;
        dwr.Data = FromXmlTo_Label(resp.Items[1], ref startIndex);
      }
      return dwr;
    }
    //  CHK_Label_InsertOrUpdate
    public void CO_Label_InsertOrUpdate(object receiverInstance, string code, string label)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Label_InsertOrUpdate");
      parameters.AddRange(new string[] { code, label.Base64Encode() });
      Plugin_Command(receiverInstance, parameters);
    }

    private IModel CO_Label_InsertOrUpdate(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      return dwr;
    }

    private C200318_Label FromXmlTo_Label(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_Label label = new C200318_Label();
      label.Code = item.getFieldVal(startIndex++);
      label.Label = item.getFieldVal(startIndex++).Base64Decode();
      return label;
    }

    #endregion

    #region LABEL FIELD

    //  CO_LabelField_GetAll
    public void CO_LabelField_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("CO_LabelField_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_LabelField_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_LabelFieldTranslator> list = new List<C200318_LabelFieldTranslator>();
      dwr.Data = list;
      foreach (var item in resp.Items)
      {
        int startIndex = 1;
        var element = FromXmlTo_LabelField(item, ref startIndex);
        list.Add(element);
      }
      return dwr;
    }

    //  CO_LabelField_Get
    public void CO_LabelField_Get(object receiverInstance, string code)
    {
      var parameters = new List<string>();
      parameters.Add("CO_LabelField_Get");
      parameters.Add(code);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_LabelField_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      if (resp.itemCount >= 1)
      {
        int startIndex = 1;
        dwr.Data = FromXmlTo_LabelField(resp.Items[1], ref startIndex);
      }
      return dwr;
    }



    //  CO_LabelField_InsertOrUpdate
    public void CO_LabelField_InsertOrUpdate(object receiverInstance, string field, string fieldTranslated
      , string fieldEnglish, string description)
    {
      var parameters = new List<string>();
      parameters.Add("CO_LabelField_InsertOrUpdate");
      parameters.AddRange(new string[] { field, fieldTranslated.Base64Encode(), fieldEnglish.Base64Encode(), description.Base64Encode() });
      Plugin_Command(receiverInstance, parameters);
    }

    private IModel CO_LabelField_InsertOrUpdate(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      return dwr;
    }



    public void CO_LabelField_Delete(object receiverInstance, string field)
    {
      var parameters = new List<string>();
      parameters.Add("CO_LabelField_Delete");
      parameters.Add(field);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_LabelField_Delete(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      return dwr;
    }



    private C200318_LabelFieldTranslator FromXmlTo_LabelField(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_LabelFieldTranslator label = new C200318_LabelFieldTranslator();
      label.Field = item.getFieldVal(startIndex++);
      label.FieldTranslated = item.getFieldVal(startIndex++).Base64Decode();
      label.FieldEnglish = item.getFieldVal(startIndex++).Base64Decode();
      label.Description = item.getFieldVal(startIndex++).Base64Decode();
      return label;
    }
    #endregion

    #region Position Input

    public void IP_Get_InputPositionCodes(object receiverInstance, IList<string> posCodesFromClient)
    {
      var parameters = new List<string>();
      parameters.Add("IP_Get_InputPositionCodes");
      parameters.AddRange(posCodesFromClient);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel IP_Get_InputPositionCodes(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<string> list = new List<string>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          list.Add(item.getFieldVal<string>(startIndex));
        }
      }
      return dwr;
    }

    public void IP_Get_PalletInPos(object receiverInstance, string posCode)
    {
      var parameters = new List<string>();
      parameters.Add("IP_Get_PalletInPos");
      parameters.Add(posCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel IP_Get_PalletInPos(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_PositionShapeControl_Pallet> list = new List<C200318_PositionShapeControl_Pallet>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200318_PositionShapeControl_Pallet pallet = IP_FromXmlTo_PosPallet(item, ref startIndex);
          list.Add(pallet);
        }
      }
      return dwr;
    }


    #region from xml to
    private C200318_PositionShapeControl_Pallet IP_FromXmlTo_PosPallet(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_PositionShapeControl_Pallet ret = new C200318_PositionShapeControl_Pallet();

      if (ret != null)
      {
        ret.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.IsService = item.getFieldVal<bool>(startIndex++);
        ret.CustomResult = item.getFieldVal<C200318_CustomResult>(startIndex++);
        ret.IsReject = item.getFieldVal<bool>(startIndex++);
        ret.RejectResult = item.getFieldVal<Results_Enum>(startIndex++);
        ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.Quantity = item.getFieldVal<int>(startIndex++);
        ret.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);
        ret.LotLength = item.getFieldVal<int>(startIndex++);
        ret.Weight = item.getFieldVal<int>(startIndex++);
        ret.Height = item.getFieldVal<int>(startIndex++);
      }
      return ret;
    }

    #endregion

    #endregion

    #region PALLET MANAGER

    public void PM_Article_GetAll(object receiverInstance, C200318_PalletArticleFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Article_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Article_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Article> list = new List<C200318_Article>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var listItem = PM_FromXmlTo_Article(item, ref startIndex);
          list.Add(listItem);
        }
      }
      return dwr;
    }

    public void PM_Pallet_GetAll(object receiverInstance, C200318_PalletManagerAllFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Pallet_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Pallet_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_PalletManagerAll> pallets = new List<C200318_PalletManagerAll>();
      dwr.Data = pallets;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var pallet = PM_FromXmlTo_PalletAll(item, ref startIndex);

          pallets.Add(pallet);
        }
      }
      return dwr;
    }

    public void PM_Get_CheckInCodes(object receiverInstance, IList<string> posCodesFromClient)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Get_CheckInCodes");
      parameters.AddRange(posCodesFromClient);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Get_CheckInCodes(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<string> list = new List<string>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          list.Add(item.getFieldVal<string>(startIndex));
        }
      }
      return dwr;
    }

    public void PM_Pallet_Register(object receiverInstance, C200318_PalletInsert pal)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Pallet_Register");

      parameters.Add(pal.ArticleCode.ToString().Base64Encode());
      parameters.Add(pal.LotCode.ToString().Base64Encode());
      parameters.Add(pal.Quantity.ToString());
      parameters.Add(pal.ExpiryDate.ConvertToDateTimeFormatString() ?? "");
      parameters.Add(pal.LotLength.ToString());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Pallet_Register(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200318_PalletInsertResult InsertResult = new C200318_PalletInsertResult();

        InsertResult.Result = FromXmlTo_Result(resp.Items[0], ref startIndex);


        if (InsertResult.Result == C200318_CustomResult.OK)
        {
          InsertResult.Code = resp.Items[0].getFieldVal<string>(startIndex++);
        }

        dwr.Data = InsertResult;
      }
      return dwr;
    }

    public void PM_Pallet_Update(object receiverInstance, C200318_PalletInsert pal)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Pallet_Update");

      parameters.Add(pal.Code.ToString().Base64Encode());
      parameters.Add(pal.Quantity.ToString());
      parameters.Add(pal.ExpiryDate.ConvertToDateTimeFormatString() ?? "");
      parameters.Add(pal.LotLength.ToString());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Pallet_Update(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200318_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);
        dwr.Result = result.ToString();

        if (resp.itemCount > 1 && (result == C200318_CustomResult.OK))
        {
          startIndex = 1;

          dwr.Data = resp.Items[1].getFieldVal<string>(startIndex++).Base64Decode();
        }
      }
      return dwr;
    }

    public void PM_Check_Pallet(object receiverInstance, string palCode)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Check_Pallet");
      parameters.Add(palCode.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Check_Pallet(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200318_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);
        dwr.Result = result.ToString();

        if (resp.itemCount > 1)
        {
          startIndex = 1;
          var itemPallet = resp.Items[1];
          C200318_PalletInsert pallet = new C200318_PalletInsert();
          dwr.Data = pallet;
          pallet.Code = itemPallet.getFieldVal<string>(startIndex++).Base64Decode();
          pallet.ArticleCode = itemPallet.getFieldVal(startIndex++).Base64Decode();
          pallet.LotCode = itemPallet.getFieldVal(startIndex++).Base64Decode();
          pallet.Quantity = itemPallet.getFieldVal<int>(startIndex++);
          pallet.ExpiryDate = itemPallet.getFieldVal<DateTime>(startIndex++);
          pallet.LotLength = itemPallet.getFieldVal<int>(startIndex++);
        }
      }
      return dwr;
    }

    public void PM_Article_CheckPresence(object receiverInstance, string palArtCode)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Article_CheckPresence");

      parameters.Add(palArtCode.ToString().Base64Encode());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Article_CheckPresence(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200318_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);

        if (result == C200318_CustomResult.OK && (resp.itemCount > 1))
        {
          startIndex = 1;
          C200318_Article art = new C200318_Article();
          art = PM_FromXmlTo_Article(resp.Items[1], ref startIndex);
          dwr.Data = art;
        }

      }
      return dwr;
    }

    public void PM_Pallet_Print_GetAll(object receiverInstance, C200318_PalletPrintFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Pallet_Print_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Pallet_Print_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_PalletManagerAll> pallets = new List<C200318_PalletManagerAll>();
      dwr.Data = pallets;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var pallet = PM_FromXmlTo_PalletAll(item, ref startIndex);

          pallets.Add(pallet);
        }
      }
      return dwr;
    }

    public void PM_Print(object receiverInstance, string positionCode, string palletCode)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Print");
      parameters.AddRange(new string[] { positionCode, palletCode });
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Print(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var itemResult = resp.Items[0];
        int startIndex = 1;
        dwr.Result = itemResult.getFieldVal(startIndex++).ToString();
      }
      return dwr;
    }

    #region From Xml To
    private C200318_PalletManagerAll PM_FromXmlTo_PalletAll(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_PalletManagerAll ret = new C200318_PalletManagerAll();
      ret.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.DateBorn = item.getFieldVal<DateTime>(startIndex++);
      ret.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ProductionDate = item.getFieldVal<DateTime>(startIndex++);
      ret.PositionCode = item.getFieldVal<string>(startIndex++);
      return ret;

    }

    private C200318_Article PM_FromXmlTo_Article(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_Article art = new C200318_Article();
      art.Code = item.getFieldVal<string>(startIndex++)?.Base64Decode();
      art.Descr = item.getFieldVal<string>(startIndex++).Base64Decode();
      art.Unit = item.getFieldVal<string>(startIndex++);
      art.Unit = art.Unit?.Base64Decode();
      art.Type = item.getFieldVal<string>(startIndex++)?.Base64Decode();


      art.AW_MinQuantity = item.getFieldVal<int>(startIndex++);
      art.AW_MaxQuantity = item.getFieldVal<int>(startIndex++);

      art.StockWindow = item.getFieldVal<int>(startIndex++);
      art.FifoWindow = item.getFieldVal<int>(startIndex++);
      art.Turnover = item.getFieldVal<C200318_TurnoverEnum>(startIndex++);

      return art;

    }



    #endregion

    #endregion

    #region ORDER MANAGER
    public void OM_OrderComponent_Kill(object receiverInstance, int order_id, int cmp_id)
    {
      var parameters = new List<string>();
      parameters.Add("OM_OrderComponent_Kill");
      parameters.AddRange(new string[] { order_id.ToString(), cmp_id.ToString() });
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_OrderComponent_Kill(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(resp.Items[0], ref startIndex);
      }
      return dwr;
    }

    public void OM_OrderDestination_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("OM_OrderDestination_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_OrderDestination_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_ListDestination> list = new List<C200318_ListDestination>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          list.Add(OM_FromXmlTo_ListDestination(item, ref startIndex));
        }
      }
      return dwr;
    }

    public void OM_Stock_LotArticle_ByOrderDestination(object receiverInstance, int startIndex, int maxItems, C200318_ListDestination ld, bool available, string articleCode, string lotCode)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Stock_LotArticle_ByOrderDestination");
      //...listDestination
      parameters.Add(startIndex.ToString());
      parameters.Add(maxItems.ToString());
      parameters.Add((ld.Code != null) ? ld.Code : "");
      parameters.Add(available.ToString());
      parameters.Add(articleCode.Base64Encode());
      parameters.Add((lotCode != null) ? lotCode.Base64Encode() : string.Empty.Base64Encode());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Stock_LotArticle_ByOrderDestination(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_ArticleLotStockByCell> giacL = new List<C200318_ArticleLotStockByCell>();
      dwr.Data = giacL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          C200318_ArticleLotStockByCell asa = new C200318_ArticleLotStockByCell()
          {
            ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode(),
            ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode(),
            LotCode = item.getFieldVal<string>(startIndex++).Base64Decode()
          };
          giacL.Add(asa);
          asa.StockByCellL.AddRange(OM_FromXmlTo_StockByCellList(item, ref startIndex));
        }
      }
      return dwr;
    }

    public void OM_Stock_ArticleLotCode(object receiverInstance, string posDestCode, C200318_ArticleForOrderFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Stock_ArticleLotCode");
      if (posDestCode == null)
        posDestCode = string.Empty;
      parameters.Add(posDestCode);
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Stock_ArticleLotCode(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_StockForOrder> giacL = new List<C200318_StockForOrder>();
      dwr.Data = giacL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          C200318_StockForOrder giac = new C200318_StockForOrder()
          {
            Article = OM_FromXmlTo_Article(item, ref startIndex),
            LotCode = item.getFieldVal<string>(startIndex++).Base64Decode(),
            NumPallet = item.getFieldVal<int>(startIndex++),
          };

          giacL.Add(giac);
        }
      }
      return dwr;
    }

    public void OM_Order_Create(object receiverInstance, C200318_OrderWrapper order)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_Create");
      parameters.Add(order.Code.Base64Encode());
      parameters.Add(order.Destination.Base64Encode());

      foreach (var cmp in order.CmpL)
      {
        parameters.Add(cmp.ArticleCode.Base64Encode());
        parameters.Add(cmp.LotCode.Base64Encode());
        parameters.Add(cmp.QtyRequested.ToString());
        parameters.Add(cmp.Karina_Qty.ToString());
        parameters.Add(cmp.CmpType.ToString());
        //parameters.Add(cmp.Unit.ToString());
      }
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_Create(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }

    public void OM_Order_GetAll_Paged(object receiverInstance, C200318_OrderFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_GetAll_Paged");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_GetAll_Paged(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Order> orderL = new List<C200318_Order>();
      dwr.Data = orderL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var order = OM_FromXmlTo_Order(item, ref startIndex);
          orderL.Add(order);
        }
      }
      return dwr;
    }

    public void OM_Order_Get(object receiverInstance, string orderCode, int orderId)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_Get");
      parameters.Add(orderCode.Base64Encode());
      parameters.Add(orderId.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = OM_FromXmlTo_Order(item, ref startIndex);
      }
      return dwr;
    }

    public void OM_Order_UpdateState(object receiverInstance, int order_id, C200318_State newState)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_UpdateState");
      parameters.Add(order_id.ToString());
      parameters.Add(newState.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_UpdateState(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }

    public void OM_Order_Update(object receiverInstance, int order_id, int priotity, bool urgente, bool completamentoManuale)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_Update");
      parameters.Add(order_id.ToString());
      parameters.Add(priotity.ToString());
      parameters.Add(urgente ? "1" : "0");
      parameters.Add(completamentoManuale ? "1" : "0");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_Update(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }

    public void OM_OrderComponent_PalletInTransit(object receiverInstance, int lcpId)
    {
      var parameters = new List<string>();
      parameters.Add("OM_OrderComponent_PalletInTransit");
      parameters.Add(lcpId.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_OrderComponent_PalletInTransit(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_PalletInTransit> list = new List<C200318_PalletInTransit>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.getAllItem())
        {
          int startIndex = 1;
          var listItem = OM_FromXmlTo_PalletInTransit(item, ref startIndex);
          //20200831
          //listItem.PositionCode = item.getFieldVal<string>(startIndex++);
          list.Add(listItem);
        }
      }
      return dwr;
    }

    public void OM_Order_DestinationChange(object receiverInstance, string orderCode, string destinationCode)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_DestinationChange");
      parameters.Add(orderCode.Base64Encode());
      parameters.Add(destinationCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_DestinationChange(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_PalletInTransit> list = new List<C200318_PalletInTransit>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }

    public void OM_Emergencial_CheckOut(object receiverInstance, string palletCode)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Emergencial_CheckOut");
      parameters.Add(palletCode.Base64Encode());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Emergencial_CheckOut(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }

    public void OM_OrderCell_PalletInTransit(object receiverInstance, string orderCode, string cellCode)
    {
      var parameters = new List<string>();
      parameters.Add("OM_OrderCell_PalletInTransit");
      parameters.Add(orderCode.Base64Encode());
      parameters.Add(cellCode.Base64Encode());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_OrderCell_PalletInTransit(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_PalletInTransit> list = new List<C200318_PalletInTransit>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.getAllItem())
        {
          int startIndex = 1;
          var listItem = OM_FromXmlTo_PalletInTransit(item, ref startIndex);
          //20200831
          //listItem.PositionCode = item.getFieldVal<string>(startIndex++);
          list.Add(listItem);
        }
      }
      return dwr;
    }



    #region WORK MODALITY CONFIGURATION
    public void OM_WorkModalityGet(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("OM_WorkModalityGet");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_WorkModalityGet(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      C200318_WorkModalityConfiguration workModality = new C200318_WorkModalityConfiguration();
      dwr.Data = workModality;

      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        workModality.Current = item.getFieldVal<C200318_WorkModality>(startIndex++);
        workModality.CurrentLastUpdate = item.getFieldVal<DateTime>(startIndex++);
        workModality.Requested = item.getFieldVal<C200318_WorkModality>(startIndex++);
        workModality.RequestedLastUpdate = item.getFieldVal<DateTime>(startIndex++);
      }
      return dwr;
    }

    public void OM_WorkModalitySet(object receiverInstance, C200318_WorkModality requested)
    {
      var parameters = new List<string>();
      parameters.Add("OM_WorkModalitySet");
      parameters.Add(requested.ToString());
      Plugin_Command(receiverInstance, parameters);
    }

    private IModel OM_WorkModalitySet(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        var item = resp.Items[0];
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }

    public void OM_WorkModalityForce(object receiverInstance, C200318_WorkModality requested)
    {
      var parameters = new List<string>();
      parameters.Add("OM_WorkModalityForce");
      parameters.Add(requested.ToString());
      Plugin_Command(receiverInstance, parameters);
    }

    private IModel OM_WorkModalityForce(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        var item = resp.Items[0];
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }

    public void OM_WorkModalitySwitchingInfo(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("OM_WorkModalitySwitchingInfo");
      Plugin_Command(receiverInstance, parameters);
    }

    private IModel OM_WorkModalitySwitchingInfo(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      //if (resp.itemCount > 0)
      //{
      //  int startIndex = 1;
      //  var item = resp.Items[0];
      //  dwr.Data = FromXmlTo_Result(item, ref startIndex);
      //}
      return dwr;
    }
    #endregion

    public void OM_GetWorkModality(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("OM_GetWorkModality");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_GetWorkModality(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_WorkModality> lResult = new List<C200318_WorkModality>();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        lResult.Add(item.getFieldVal<C200318_WorkModality>(startIndex++));
        lResult.Add(item.getFieldVal<C200318_WorkModality>(startIndex++));
        dwr.Data = lResult;

      }
      return dwr;

    }

    public void OM_GetAllCellForManualSatFetch(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("OM_GetAllCellForManualSatFetch");

      Plugin_Command(receiverInstance, parameters);
    }

    private IModel OM_GetAllCellForManualSatFetch(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Cell> cellL = new List<C200318_Cell>();
      dwr.Data = cellL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          C200318_Cell cell = new C200318_Cell()
          {
            Code = item.getFieldVal<string>(startIndex++)?.Base64Decode(),
            IsAvailableForManualSatFetch = item.getFieldVal<bool>(startIndex++),
            IsDisabled = item.getFieldVal<bool>(startIndex++),
            IsLocked = item.getFieldVal<bool>(startIndex++),
            PalletCount = item.getFieldVal<int>(startIndex++),
            ArticleCode = item.getFieldVal<string>(startIndex++)?.Base64Decode(),
            ArticleDescr = item.getFieldVal<string>(startIndex++)?.Base64Decode(),
            LotCode = item.getFieldVal<string>(startIndex++)?.Base64Decode(),
          };

          // retrieve x,y,z
          if (cell.Code != null)
          {
            string[] s = cell.Code.Split('-');
            if (s != null)
            {
              cell.X = s[1].ConvertToInt();
              cell.Y = s[2].ConvertToInt();
              cell.Z = s[3].ConvertToInt();
            }
          }

          cellL.Add(cell);
        }
      }
      return dwr;
    }

    public void OM_AddCellForManualSatFetch(object receiverInstance, string cellCode)
    {
      var parameters = new List<string>();
      parameters.Add("OM_AddCellForManualSatFetch");
      parameters.Add(cellCode.Base64Encode());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_AddCellForManualSatFetch(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }

    public void OM_RemoveCellForManualSatFetch(object receiverInstance, string cellCode)
    {
      var parameters = new List<string>();
      parameters.Add("OM_RemoveCellForManualSatFetch");
      parameters.Add(cellCode.Base64Encode());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_RemoveCellForManualSatFetch(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }

    #region COLLAUDO
    //OM_Order_CreateCollaudo
    public void OM_Order_CreateCollaudo(object receiverInstance, C200318_OrderWrapper order)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_CreateCollaudo");
      parameters.Add(order.Code.Base64Encode());

      foreach (var cmp in order.CmpL)
      {
        parameters.Add(cmp.ArticleCode.Base64Encode());
        parameters.Add(cmp.QtyRequested.ToString());
      }
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_CreateCollaudo(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }

    public void OM_OrdersForCollaudo(object receiverInstance, int numOrders, int numCmps, int maxQty)
    {
      var parameters = new List<string>();
      parameters.Add("OM_OrdersForCollaudo");
      parameters.Add(numOrders.ToString());
      parameters.Add(numCmps.ToString());
      parameters.Add(maxQty.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_OrdersForCollaudo(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Order> orderL = new List<C200318_Order>();
      dwr.Data = orderL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var order = OM_FromXmlTo_Order(item, ref startIndex);
          orderL.Add(order);
        }
      }
      return dwr;
    }

    public void OM_OrdersForCollaudoReset(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("OM_OrdersForCollaudoReset");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_OrdersForCollaudoReset(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      return dwr;
    }

    #endregion


    #region FromXmlTo

    private List<OrderManager.C200318_StockByCell> OM_FromXmlTo_StockByCellList(XmlCommandResponse.Item item, ref int startIndex)
    {
      List<OrderManager.C200318_StockByCell> ret = new List<OrderManager.C200318_StockByCell>();
      while (startIndex <= item.Fields.Count)
      {
        ret.Add(OM_FromXmlTo_StockByCell(item, ref startIndex));
      }
      return ret;
    }

    private OrderManager.C200318_StockByCell OM_FromXmlTo_StockByCell(XmlCommandResponse.Item item, ref int startIndex)
    {
      OrderManager.C200318_StockByCell ret = new OrderManager.C200318_StockByCell()
      {
        CellCode = item.getFieldVal<string>(startIndex++),
        NumPallet = item.getFieldVal<int>(startIndex++),
      };

      return ret;
    }

    private List<C200318_PalletInTransit> OM_FromXmlTo_PalletsInTransit(XmlCommandResponse.Item item, ref int startIndex)
    {
      List<C200318_PalletInTransit> ret = new List<C200318_PalletInTransit>();
      int count = item.getFieldVal<int>(startIndex++);
      for (int i = 0; i < count; i++)
      {
        var pt = OM_FromXmlTo_PalletInTransit(item, ref startIndex);
        ret.Add(pt);
      }
      return ret;
    }

    private C200318_PalletInTransit OM_FromXmlTo_PalletInTransit(XmlCommandResponse.Item item, ref int startIndex)
    {
      var pallet = OM_FromXmlTo_Pallet(item, ref startIndex);
      OrderManager.C200318_PalletInTransit ret = new OrderManager.C200318_PalletInTransit(pallet);
      ret.PositionCode = item.getFieldVal<string>(startIndex++);
      ret.WorkModality = item.getFieldVal<C200318_WorkModality>(startIndex++);
      return ret;
    }

    private C200318_Pallet OM_FromXmlTo_Pallet(XmlCommandResponse.Item item, ref int startIndex)
    {
      OrderManager.C200318_Pallet ret = new OrderManager.C200318_Pallet();
      ret.Code = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.DateBorn = item.getFieldVal<DateTime>(startIndex++);
      ret.IsService = item.getFieldVal<bool>(startIndex++);
      ret.CustomResult = item.getFieldVal<C200318_CustomResult>(startIndex++);
      ret.IsReject = item.getFieldVal<bool>(startIndex++);
      ret.RejectResult = item.getFieldVal<Results_Enum>(startIndex++);
      ret.ProductionDate = item.getFieldVal<DateTime>(startIndex++);
      ret.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);
      ret.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.State = item.getFieldVal<C200318_Pallet_State>(startIndex++);
      ret.Days_Spent_FromProduction = item.getFieldVal<int>(startIndex++);
      ret.Days_to_Expiry = item.getFieldVal<int>(startIndex++);
      ret.FinalDestination = item.getFieldVal<string>(startIndex++);

      return ret;

    }

    private C200318_Article OM_FromXmlTo_Article(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_Article art = new C200318_Article();
      art.Code = item.getFieldVal<string>(startIndex++).Base64Decode();
      art.Descr = item.getFieldVal<string>(startIndex++).Base64Decode();
      //art.PalletTypeId = (C200318_PalletType)item.getFieldVal<int>(startIndex++);
      art.AW_MinQuantity = item.getFieldVal<int>(startIndex++);
      art.AW_MaxQuantity = item.getFieldVal<int>(startIndex++);
      art.StockWindow = item.getFieldVal<int>(startIndex++);
      art.FifoWindow = item.getFieldVal<int>(startIndex++);
      //art.BatchCount = item.getFieldVal<int>(startIndex++);
      return art;
    }

    private C200318_Order OM_FromXmlTo_Order(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_Order order = new C200318_Order();
      OM_FromXmlSet_Order(order, item, ref startIndex);
      OM_FromXmlSet_OrderCmps(order, item, ref startIndex);
      var palletInTransitL = OM_FromXmlTo_PalletsInTransit(item, ref startIndex);
      order.PalletInTransitL.AddRange(palletInTransitL);
      return order;
    }

    private void OM_FromXmlSet_Order(C200318_Order ord, XmlCommandResponse.Item item, ref int startIndex)
    {
      ord.Id = item.getFieldVal<int>(startIndex++);
      ord.Code = item.getFieldVal<string>(startIndex++).Base64Decode();
      ord.DestinationMachine = item.getFieldVal<string>(startIndex++);
      ord.State = item.getFieldVal<C200318_State>(startIndex++);
      ord.ExecutionModality = item.getFieldVal<C200318_ExecutionModality>(startIndex++);
      ord.OrderType = item.getFieldVal<C200318_OrderType>(startIndex++);
      ord.DateBorn = item.getFieldVal<DateTime>(startIndex++);
      ord.DateSend = item.getFieldVal<DateTime>(startIndex++);
      ord.DateExec = item.getFieldVal<DateTime>(startIndex++);
      ord.DateEnd = item.getFieldVal<DateTime>(startIndex++);
      ord.Priority = item.getFieldVal<int>(startIndex++);
      ord.LastScanningDateTime = item.getFieldVal<DateTime>(startIndex++);
      ord.ExecutionResult = item.getFieldVal<C200318_ExecutionResult>(startIndex++);
      bool hasDestination = item.getFieldVal<bool>(startIndex++);
      if (hasDestination)
      {
        var destination = OM_FromXmlTo_ListDestination(item, ref startIndex);
        ord.Destination = destination;
      }
    }

    private void OM_FromXmlSet_OrderCmps(C200318_Order order, XmlCommandResponse.Item item, ref int startIndex)
    {
      int count = item.getFieldVal<int>(startIndex++);
      for (int i = 0; i < count; i++)
      {
        var cmp = OM_FromXmlTo_OrderCmp(item, ref startIndex);
        order.CmpL.Add(cmp);
      }
    }

    private C200318_OrderCmp OM_FromXmlTo_OrderCmp(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_OrderCmp ret = new C200318_OrderCmp();
      ret.Id = item.getFieldVal<int>(startIndex++);
      ret.State = item.getFieldVal<C200318_State>(startIndex++);
      ret.ArticleCode = item.getFieldVal<string>(startIndex++)?.Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++)?.Base64Decode();
      //20200903
      //ret.CmpType= item.getFieldVal<C200318_ListCmpTypeEnum>(startIndex++);
      //20200903
      //ret.UnitOfMeasure= item.getFieldVal<C200318_ListCmpUnitOfMeasureEnum>(startIndex++);

      ret.LotCode = item.getFieldVal<string>(startIndex++)?.Base64Decode();

      ret.QtyRequested = item.getFieldVal<int>(startIndex++);
      ret.QtyDelivered = item.getFieldVal<int>(startIndex++);
      ret.NumPalletInTransit = item.getFieldVal<int>(startIndex++);
      ret.QtyRemaining = item.getFieldVal<int>(startIndex++);
      ret.ExecutionResult = item.getFieldVal<C200318_ExecutionResult>(startIndex++);
      ret.LastScanningDateTime = item.getFieldVal<DateTime>(startIndex++);
      return ret;
    }

    //20200820 aggiunto OM_FromXmlTo_ListDestination
    private C200318_ListDestination OM_FromXmlTo_ListDestination(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_ListDestination destination = new C200318_ListDestination();
      destination.Code = item.getFieldVal(startIndex++);
      destination.Description = item.getFieldVal(startIndex++);
      destination.IsDefault = item.getFieldVal<bool>(startIndex++);
      int posDestCount = item.getFieldVal<int>(startIndex++);
      for (int i = 0; i < posDestCount; i++)
      {
        destination.PositionList.Add(item.getFieldVal(startIndex++));
      }
      return destination;
    }

    #endregion

    #endregion

    #region EXIT POSITION

    public void EP_Get_ExitPositionCodes(object receiverInstance, IList<string> posCodesFromClient)
    {
      var parameters = new List<string>();
      parameters.Add("EP_Get_ExitPositionCodes");
      parameters.AddRange(posCodesFromClient);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel EP_Get_ExitPositionCodes(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<string> list = new List<string>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          list.Add(item.getFieldVal<string>(startIndex));
        }
      }
      return dwr;
    }

    public void EP_Get_PalletInPos(object receiverInstance, string positionCode)
    {
      var parameters = new List<string>();
      parameters.Add("EP_Get_PalletInPos");
      parameters.Add(positionCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel EP_Get_PalletInPos(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_PositionOut_Pallet> orderL = new List<C200318_PositionOut_Pallet>();
      dwr.Data = orderL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var pallet = EP_FromXmlTo_Pallet(item, ref startIndex);
          orderL.Add(pallet);
        }
      }
      return dwr;
    }

    #region FromXmlTo
    private C200318_PositionOut_Pallet EP_FromXmlTo_Pallet(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_PositionOut_Pallet ret = new C200318_PositionOut_Pallet();
      if (ret != null)
      {
        ret.Code = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.LotLength = item.getFieldVal<int>(startIndex++);
        ret.Height = item.getFieldVal<int>(startIndex++);
        ret.Weight = item.getFieldVal<int>(startIndex++);
        ret.IsService = item.getFieldVal<bool>(startIndex++);
        ret.CustomResult = item.getFieldVal<C200318_CustomResult>(startIndex++);
        ret.IsReject = item.getFieldVal<bool>(startIndex++);
        ret.RejectResult = item.getFieldVal<Results_Enum>(startIndex++);
        ret.HasOrder = item.getFieldVal<bool>(startIndex++);
        if (ret.HasOrder)
        {
          ret.OrderCode = item.getFieldVal<string>(startIndex++)?.Base64Decode();
          ret.OrderDestinationMachine = item.getFieldVal<string>(startIndex++)?.Base64Decode();

          ret.OrderCmpArticleCode = item.getFieldVal<string>(startIndex++)?.Base64Decode();
          ret.OrderCmpArticleDescr = item.getFieldVal<string>(startIndex++)?.Base64Decode();
          ret.OrderCmpLotCode = item.getFieldVal<string>(startIndex++)?.Base64Decode();
          ret.OrderCmpQtyRequested = item.getFieldVal<int>(startIndex++);
          ret.OrderCmpQuantityDelivered = item.getFieldVal<int>(startIndex++);
        }
      }
      return ret;
    }
    #endregion

    #endregion

    #region REPORT MANAGER

    #region DATABASE
    public void RM_Article_GetAll(object receiverInstance, C200318_ArticleFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Article_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Article_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Article> list = new List<C200318_Article>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var listItem = RM_FromXmlTo_Article(item, ref startIndex);
          list.Add(listItem);
        }
      }
      return dwr;
    }

    public void RM_Lot_GetAll(object receiverInstance, C200318_LotFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Lot_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Lot_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Lot> list = new List<C200318_Lot>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var listItem = RM_FromXmlTo_Lot(item, ref startIndex);
          list.Add(listItem);
        }
      }
      return dwr;
    }

    public void RM_Pallet_GetAll(object receiverInstance, C200318_PalletAllFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Pallet_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Pallet_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_PalletAll> pallets = new List<C200318_PalletAll>();
      dwr.Data = pallets;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var pallet = RM_FromXmlTo_PalletAll(item, ref startIndex);

          pallets.Add(pallet);
        }
      }
      return dwr;
    }

    #endregion

    #region PERFORMANCE
    public void RM_Performance(object receiverInstance, DateTime dateStart)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Performance");
      parameters.Add(dateStart.ConvertToDateTimeFormatString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Performance(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Performance> list = new List<C200318_Performance>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200318_Performance pr = new C200318_Performance();
          pr.DateStart = item.getFieldVal<DateTime>(startIndex++);
          pr.DateEnd = item.getFieldVal<DateTime>(startIndex++);
          pr.Input = item.getFieldVal<int>(startIndex++);
          pr.Reject = item.getFieldVal<int>(startIndex++);
          pr.Warehouse = item.getFieldVal<int>(startIndex++);
          pr.Output = item.getFieldVal<int>(startIndex++);
          pr.Exit = item.getFieldVal<int>(startIndex++);
          pr.Wh1In = item.getFieldVal<int>(startIndex++);
          pr.Wh2In = item.getFieldVal<int>(startIndex++);
          pr.Wh3In = item.getFieldVal<int>(startIndex++);
          pr.Wh1Out = item.getFieldVal<int>(startIndex++);
          pr.Wh2Out = item.getFieldVal<int>(startIndex++);
          pr.Wh3Out = item.getFieldVal<int>(startIndex++);
          list.Add(pr);
        }
      }
      return dwr;
    }

    public void RM_PerformanceByInterval(object receiverInstance, DateTime dateStart, DateTime dateEnd)
    {
      var parameters = new List<string>();
      parameters.Add("RM_PerformanceByInterval");
      parameters.AddRange(new string[] { dateStart.ConvertToDateTimeFormatString(), dateEnd.ConvertToDateTimeFormatString() });
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_PerformanceByInterval(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Performance> list = new List<C200318_Performance>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200318_Performance pr = new C200318_Performance();
          pr.DateStart = item.getFieldVal<DateTime>(startIndex++);
          pr.DateEnd = item.getFieldVal<DateTime>(startIndex++);
          pr.Input = item.getFieldVal<int>(startIndex++);
          pr.Reject = item.getFieldVal<int>(startIndex++);
          pr.Warehouse = item.getFieldVal<int>(startIndex++);
          pr.Output = item.getFieldVal<int>(startIndex++);
          pr.Exit = item.getFieldVal<int>(startIndex++);
          pr.Wh1In = item.getFieldVal<int>(startIndex++);
          pr.Wh2In = item.getFieldVal<int>(startIndex++);
          pr.Wh3In = item.getFieldVal<int>(startIndex++);
          pr.Wh1Out = item.getFieldVal<int>(startIndex++);
          pr.Wh2Out = item.getFieldVal<int>(startIndex++);
          pr.Wh3Out = item.getFieldVal<int>(startIndex++);
          list.Add(pr);
        }
      }
      return dwr;
    }

    #endregion

    #region ACTIVITIES
    public void RM_Jobs_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Jobs_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Jobs_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      Dictionary<string, int> dic = resp.getAllItem().ToDictionary(item => item.getFieldVal<string>(1), item => item.getFieldVal<int>(2));
      dwr.Data = dic;
      return dwr;
    }

    #endregion

    #region STOCK
    public void RM_StockByCell_GetAll(object receiverInstance, C200318_StockByCellFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_StockByCell_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_StockByCell_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<Report.C200318_StockByCell> giacL = new List<Report.C200318_StockByCell>();
      dwr.Data = giacL;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          Report.C200318_StockByCell g = new Report.C200318_StockByCell();
          g.CellCode = item.getFieldVal<string>(startIndex++);
          g.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          g.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          g.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
          g.FinestraStoccaggio = item.getFieldVal<int>(startIndex++);
          g.MaxPalletAllowed = item.getFieldVal<int>(startIndex++);
          g.PalletCount = item.getFieldVal<int>(startIndex++);
          g.PalletFirstProductionDate = item.getFieldVal<DateTime>(startIndex++);
          g.PalletLastProductionDate = item.getFieldVal<DateTime>(startIndex++);
          g.NumDays = item.getFieldVal<int>(startIndex++);
          giacL.Add(g);
        }
      }
      return dwr;
    }

    public void RM_Stock_Pallet_ByWorkModality(object receiverInstance, C200318_StockPalletFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Stock_Pallet_ByWorkModality");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Stock_Pallet_ByWorkModality(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_StockPallet> pallets = new List<C200318_StockPallet>();
      dwr.Data = pallets;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var pallet = RM_FromXmlTo_StockPallet(item, ref startIndex);
          pallets.Add(pallet);
        }
      }
      return dwr;
    }

    public void RM_Stock_LotArticle(object receiverInstance, C200318_StockLotArticleFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Stock_LotArticle");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Stock_LotArticle(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_StockLotArticle> giacL = new List<C200318_StockLotArticle>();
      dwr.Data = giacL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var giac = RM_FromXmlTo_StockArticle(item, ref startIndex);
          giacL.Add(giac);
        }
      }
      return dwr;
    }

    #endregion

    #region PALLET IN
    public void RM_PalletIn(object receiverInstance, C200318_PalletInFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_PalletIn");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_PalletIn(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_PalletIn> list = new List<C200318_PalletIn>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200318_PalletIn pr = new C200318_PalletIn();
          pr.DateBorn = item.getFieldVal<DateTime>(startIndex++);
          pr.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.Qty = item.getFieldVal<int>(startIndex++);
          pr.LotLength = item.getFieldVal<int>(startIndex++);
          pr.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);
          pr.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.Position = item.getFieldVal<string>(startIndex++);
          pr.CellCode = item.getFieldVal<string>(startIndex++);
          list.Add(pr);
        }
      }
      return dwr;
    }

    public void RM_InputPositions_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_InputPositions_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_InputPositions_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Position> list = new List<C200318_Position>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          C200318_Position position = new C200318_Position();

          int startIndex = 1;
          position.Code = item.getFieldVal<string>(startIndex++);
          position.Description = item.getFieldVal<string>(startIndex++);

          list.Add(position);
        }
      }
      return dwr;
    }

    #endregion

    #region LOG XML COMMAND
    public void RM_LogXmlCommand(object receiverInstance, C200318_LogFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_LogXmlCommand");
      parameters.Add(filter.Index.ToString());
      parameters.Add(filter.MaxItems.ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_LogXmlCommand(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_LogXmlCommand> list = new List<C200318_LogXmlCommand>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200318_LogXmlCommand lg = new C200318_LogXmlCommand();
          lg.DateBorn = item.getFieldVal<DateTime>(startIndex++);
          lg.Command = item.getFieldVal(startIndex++).Base64Decode();
          lg.User = item.getFieldVal(startIndex++).Base64Decode();
          list.Add(lg);
        }
      }
      return dwr;
    }


    public void RM_PalletOut(object receiverInstance, C200318_PalletOutFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_PalletOut");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_PalletOut(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_PalletOut> list = new List<C200318_PalletOut>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200318_PalletOut pr = new C200318_PalletOut();
          pr.DateBorn = item.getFieldVal<DateTime>(startIndex++);
          pr.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.Qty = item.getFieldVal<int>(startIndex++);
          pr.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.Position = item.getFieldVal<string>(startIndex++);
          pr.CellCode = item.getFieldVal<string>(startIndex++);
          pr.OrderCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.KarinaId = item.getFieldVal<int>(startIndex++);
          pr.Karina_Qty_For_Pallet = item.getFieldVal<int>(startIndex++);
          list.Add(pr);
        }
      }
      return dwr;
    }

    public void RM_ListDestinationPositions_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_ListDestinationPositions_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_ListDestinationPositions_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Position> list = new List<C200318_Position>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          C200318_Position position = new C200318_Position();

          int startIndex = 1;
          position.Code = item.getFieldVal<string>(startIndex++);
          position.Description = item.getFieldVal<string>(startIndex++);

          list.Add(position);
        }
      }
      return dwr;
    }


    #endregion

    #region PALLET REJECT
    public void RM_PalletReject(object receiverInstance, C200318_PalletRejectFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_PalletReject");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_PalletReject(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_PalletReject> list = new List<C200318_PalletReject>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200318_PalletReject pr = new C200318_PalletReject();
          pr.DateBorn = item.getFieldVal<DateTime>(startIndex++);
          pr.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.Qty = item.getFieldVal<int>(startIndex++);
          pr.Position = item.getFieldVal<string>(startIndex++);
          pr.Reason = item.getFieldVal<string>(startIndex++);
          list.Add(pr);
        }
      }
      return dwr;
    }

    public void RM_ExitPositions_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_ExitPositions_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_ExitPositions_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Position> list = new List<C200318_Position>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          C200318_Position position = new C200318_Position();

          int startIndex = 1;
          position.Code = item.getFieldVal<string>(startIndex++);
          position.Description = item.getFieldVal<string>(startIndex++);

          list.Add(position);
        }
      }
      return dwr;
    }

    #endregion

    #region DISTRIBUTION

    public void RM_Pallet_Flow(object receiverInstance, C200318_PalletFlowFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Pallet_Flow");
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Pallet_Flow(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      C200318_PalletFlow palletFlow = new C200318_PalletFlow();
      dwr.Data = palletFlow;

      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          switch (item.getFieldVal<string>(startIndex++))
          {
            case "Input":
              C200318_PalletIn pi = new C200318_PalletIn();
              pi.DateBorn = item.getFieldVal<DateTime>(startIndex++);
              pi.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pi.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pi.Qty = item.getFieldVal<int>(startIndex++);
              pi.LotLength = item.getFieldVal<int>(startIndex++);
              pi.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);
              pi.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pi.Position = item.getFieldVal<string>(startIndex++);
              pi.CellCode = item.getFieldVal<string>(startIndex++);

              palletFlow.listIn.Add(pi);

              break;

            case "Output":
              C200318_PalletOut po = new C200318_PalletOut();
              po.DateBorn = item.getFieldVal<DateTime>(startIndex++);
              po.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              po.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              po.Qty = item.getFieldVal<int>(startIndex++);
              po.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              po.Position = item.getFieldVal<string>(startIndex++);
              po.CellCode = item.getFieldVal<string>(startIndex++);
              po.OrderCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              po.KarinaId = item.getFieldVal<int>(startIndex++);

              palletFlow.listOut.Add(po);

              break;

            case "Reject":
              C200318_PalletReject pr = new C200318_PalletReject();
              pr.DateBorn = item.getFieldVal<DateTime>(startIndex++);
              pr.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pr.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pr.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pr.Qty = item.getFieldVal<int>(startIndex++);
              pr.Position = item.getFieldVal<string>(startIndex++);
              pr.Reason = item.getFieldVal<string>(startIndex++);

              palletFlow.listReject.Add(pr);

              break;
          }


        }
      }
      return dwr;
    }

    public void RM_GetAllAreas(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_GetAllAreas");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_GetAllAreas(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_WhArea> areaL = new List<C200318_WhArea>();
      dwr.Data = areaL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          int startIndex = 1;
          var item = resp.Items[i];
          C200318_WhArea area = new C200318_WhArea();
          area.Code = item.getFieldVal<string>(startIndex++);
          area.WhAreaType = item.getFieldVal<C200318_WhAreaType>(startIndex++);
          areaL.Add(area);
        }
      }
      return dwr;
    }

    public void RM_Distribution_Pallet_GetByCell(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Distribution_Pallet_GetByCell");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Distribution_Pallet_GetByCell(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_DistributionPallet> list = new List<C200318_DistributionPallet>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          var d = RM_FromXmlTo_DistributionPallet(item, ref startIndex);
          list.Add(d);
        }
      }
      return dwr;
    }


    public void RM_Distribution_LotArticles_ByCell(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Distribution_LotArticles_ByCell");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Distribution_LotArticles_ByCell(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_DistributionArticleByCell> list = new List<C200318_DistributionArticleByCell>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200318_DistributionArticleByCell d = new C200318_DistributionArticleByCell();
          d.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          d.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          d.NumPallet = item.getFieldVal<int>(startIndex++);
          d.Cell = item.getFieldVal<string>(startIndex++);

          list.Add(d);
        }
      }
      return dwr;
    }
    #endregion

    #region TRAFFIC PANEL
    public void RM_Pallet_Traffic_GetAll(object receiverInstance, C200318_TrafficBoardTaskFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Pallet_Traffic_GetAll");
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Pallet_Traffic_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_PalletTraffic> list = new List<C200318_PalletTraffic>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          var pal = RM_FromXmlTo_PalletTraffic(item, ref startIndex);
          list.Add(pal);
        }
      }
      return dwr;
    }

    #endregion

    //public void RM_Distribution_Articles_Batch_ByMainArea(object receiverInstance)
    //{
    //  var parameters = new List<string>();
    //  parameters.Add("RM_Distribution_Articles_Batch_ByMainArea");
    //  Plugin_Command(receiverInstance, parameters);
    //}
    //private IModel RM_Distribution_Articles_Batch_ByMainArea(XmlCommandResponse resp)
    //{
    //  DataWrapperResult dwr = new DataWrapperResult();
    //  List<C200318_DistributionArticleByMainArea> list = new List<C200318_DistributionArticleByMainArea>();
    //  dwr.Data = list;
    //  if (resp.itemCount > 0)
    //  {
    //    foreach (var item in resp.Items)
    //    {
    //      int startIndex = 1;
    //      C200318_DistributionArticleByMainArea d = new C200318_DistributionArticleByMainArea();
    //      d.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
    //      d.AreaCode = item.getFieldVal<string>(startIndex++);
    //      d.NumPallet = item.getFieldVal<int>(startIndex++);

    //      list.Add(d);
    //    }
    //  }
    //  return dwr;
    //}

    //public void RM_Distribution_StoricFlowWhIn(object receiverInstance, C200318_Distribution_Storic_Filter filter)
    //{
    //  var parameters = new List<string>();
    //  parameters.Add("RM_Distribution_StoricFlowWhIn");
    //  parameters.AddRange(filter.GetXmlCommandParameters());
    //  /*
    //  parameters.Add(filter.Index.ToString());
    //  parameters.Add((filter.MaxItems).ToString());

    //  parameters.Add((filter.AreaCode ?? "").ToString());
    //  parameters.Add((filter.Floor).ToString());       
    //  parameters.Add(filter.DateRangeSelected ? filter.DateBornMin.ConvertToDateTimeFormatString() : DateTime.MinValue.ConvertToDateTimeFormatString());
    //  parameters.Add(filter.DateRangeSelected ? filter.DateBornMax.ConvertToDateTimeFormatString() : DateTime.MinValue.ConvertToDateTimeFormatString()); 
    //  parameters.Add((filter.ArticleCode ?? "").ToString().Base64Encode());
    //  */
    //  Plugin_Command(receiverInstance, parameters);
    //}
    //private IModel RM_Distribution_StoricFlowWhIn(XmlCommandResponse resp)
    //{
    //  DataWrapperResult dwr = new DataWrapperResult();
    //  List<C200318_Distribution_StoricFlowWhIn> list = new List<C200318_Distribution_StoricFlowWhIn>();
    //  dwr.Data = list;
    //  if (resp.itemCount > 0)
    //  {
    //    foreach (var item in resp.Items)
    //    {
    //      int startIndex = 1;
    //      C200318_Distribution_StoricFlowWhIn d = new C200318_Distribution_StoricFlowWhIn();

    //      d.AreaCode = item.getFieldVal<string>(startIndex++);
    //      d.NumPallet = item.getFieldVal<int>(startIndex++);
    //      d.AreaCodeFull = item.getFieldVal<string>(startIndex++);
    //      d.WhFloor = item.getFieldVal<int>(startIndex++);

    //      list.Add(d);
    //    }
    //  }
    //  return dwr;
    //}

    //public void RM_Distribution_StoricFlowArticleWhIn(object receiverInstance, C200318_Distribution_Storic_Filter filter)
    //{
    //  var parameters = new List<string>();
    //  parameters.Add("RM_Distribution_StoricFlowArticleWhIn");
    //  parameters.AddRange(filter.GetXmlCommandParameters());

    //  /*
    //  parameters.Add(filter.Index.ToString());
    //  parameters.Add((filter.MaxItems).ToString());
    //  parameters.Add((filter.AreaCode ?? "").ToString());
    //  parameters.Add((filter.Floor).ToString());
    //  parameters.Add(filter.DateRangeSelected ? filter.DateBornMin.ConvertToDateTimeFormatString() : DateTime.MinValue.ConvertToDateTimeFormatString());
    //  parameters.Add(filter.DateRangeSelected ? filter.DateBornMax.ConvertToDateTimeFormatString() : DateTime.MinValue.ConvertToDateTimeFormatString());
    //  parameters.Add((filter.ArticleCode ?? "").ToString().Base64Encode());
    //  */


    //  Plugin_Command(receiverInstance, parameters);
    //}
    //private IModel RM_Distribution_StoricFlowArticleWhIn(XmlCommandResponse resp)
    //{
    //  DataWrapperResult dwr = new DataWrapperResult();
    //  List<C200318_Distribution_StoricFlowArticleWhIn> list = new List<C200318_Distribution_StoricFlowArticleWhIn>();
    //  dwr.Data = list;
    //  if (resp.itemCount > 0)
    //  {
    //    foreach (var item in resp.Items)
    //    {
    //      int startIndex = 1;
    //      C200318_Distribution_StoricFlowArticleWhIn d = new C200318_Distribution_StoricFlowArticleWhIn();

    //      d.AreaCode = item.getFieldVal<string>(startIndex++);
    //      d.NumPallet = item.getFieldVal<int>(startIndex++);
    //      d.AreaCodeFull = item.getFieldVal<string>(startIndex++);
    //      d.WhFloor = item.getFieldVal<int>(startIndex++);
    //      d.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();

    //      list.Add(d);
    //    }
    //  }
    //  return dwr;
    //}

    #region FromXmlTo
    private C200318_DistributionPallet RM_FromXmlTo_DistributionPallet(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_DistributionPallet ret = new C200318_DistributionPallet();
      ret.Cell = item.getFieldVal<string>(startIndex++);
      ret.NumPallet = item.getFieldVal<int>(startIndex++);
      return ret;

    }

    private C200318_PalletAll RM_FromXmlTo_PalletAll(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_PalletAll ret = new C200318_PalletAll();
      ret.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.DateBorn = item.getFieldVal<DateTime>(startIndex++);
      ret.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ProductionDate = item.getFieldVal<DateTime>(startIndex++);
      ret.PositionCode = item.getFieldVal<string>(startIndex++);
      ret.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);
      ret.Quantity = item.getFieldVal<int>(startIndex++);
      ret.LotLength = item.getFieldVal<int>(startIndex++);
      return ret;

    }

    private C200318_StockPallet RM_FromXmlTo_StockPallet(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_StockPallet ret = new C200318_StockPallet();
      ret.Id = item.getFieldVal<int>(startIndex++);
      ret.Code = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.DateBorn = item.getFieldVal<DateTime>(startIndex++);
      ret.MapperEntityCode = item.getFieldVal<string>(startIndex++).ToUpper();
      ret.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.Qty = item.getFieldVal<int>(startIndex++);
      return ret;
    }

    private C200318_StockLotArticle RM_FromXmlTo_StockArticle(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_StockLotArticle ret = new C200318_StockLotArticle();
      ret.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.MaxQty = item.getFieldVal<int>(startIndex++);
      ret.MinQty = item.getFieldVal<int>(startIndex++);
      ret.StockTotale = item.getFieldVal<int>(startIndex++);
      ret.StockInEmergenziale = item.getFieldVal<int>(startIndex++);
      return ret;
    }

    private C200318_Article RM_FromXmlTo_Article(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_Article art = new C200318_Article();
      art.Code = item.getFieldVal<string>(startIndex++)?.Base64Decode();
      art.Descr = item.getFieldVal<string>(startIndex++).Base64Decode();
      art.Unit = item.getFieldVal<string>(startIndex++);
      art.Unit = art.Unit?.Base64Decode();
      art.Type = item.getFieldVal<string>(startIndex++)?.Base64Decode();


      art.AW_MinQuantity = item.getFieldVal<int>(startIndex++);
      art.AW_MaxQuantity = item.getFieldVal<int>(startIndex++);

      art.StockWindow = item.getFieldVal<int>(startIndex++);
      art.FifoWindow = item.getFieldVal<int>(startIndex++);
      art.Turnover = item.getFieldVal<C200318_TurnoverEnum>(startIndex++);

      return art;

    }
    private C200318_Lot RM_FromXmlTo_Lot(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_Lot lot = new C200318_Lot();
      lot.Code = item.getFieldVal<string>(startIndex++)?.Base64Decode();
      lot.Descr = item.getFieldVal<string>(startIndex++).Base64Decode();
      lot.DateBorn = item.getFieldVal<DateTime>(startIndex++);

      return lot;
    }
    private C200318_PalletTraffic RM_FromXmlTo_PalletTraffic(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_PalletTraffic ret = new C200318_PalletTraffic();
      ret.Code = item.getFieldVal<string>(startIndex++);
      ret.Position = item.getFieldVal<string>(startIndex++);
      ret.FinalDestination = item.getFieldVal<string>(startIndex++);
      ret.LotCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.OrderCode = item.getFieldVal<string>(startIndex++)?.Base64Decode();

      return ret;

    }
    #endregion

    #endregion

    #region WHSTATUS
    public void RM_Wh_Status(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Wh_Status");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Wh_Status(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<WhStatus.C200318_WhStatus> list = new List<WhStatus.C200318_WhStatus>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          WhStatus.C200318_WhStatus whs = new WhStatus.C200318_WhStatus();
          whs.Code = item.getFieldVal<string>(startIndex++);
          whs.CellsCount = item.getFieldVal<int>(startIndex++);
          whs.NumTotFullCells = item.getFieldVal<int>(startIndex++);
          whs.NumTotEmptyCells = item.getFieldVal<int>(startIndex++);
          whs.NumTotHalfFullCells = item.getFieldVal<int>(startIndex++);
          whs.NumTotBlockedCells = item.getFieldVal<int>(startIndex++);
          whs.Capacity = item.getFieldVal<int>(startIndex++);
          whs.NumTotPallet = item.getFieldVal<int>(startIndex++);
          list.Add(whs);
        }
      }
      return dwr;
    }

    public void RM_Wh_MasterStatus(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Wh_MasterStatus");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Wh_MasterStatus(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<WhStatus.C200318_MasterStatus> list = new List<WhStatus.C200318_MasterStatus>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          WhStatus.C200318_MasterStatus master = new WhStatus.C200318_MasterStatus();
          master.Code = item.getFieldVal<string>(startIndex++);
          master.Status = item.getFieldVal<WhStatus.C200318_SatelliteStatusEnum>(startIndex++);
          master.IsOk = item.getFieldVal<bool>(startIndex++);
          master.IsMaintenaceRequest = item.getFieldVal<bool>(startIndex++);
          list.Add(master);
        }
      }
      return dwr;
    }

    public void RM_Wh_SatelliteStatus(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Wh_SatelliteStatus");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Wh_SatelliteStatus(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<WhStatus.C200318_SlaveStatus> list = new List<WhStatus.C200318_SlaveStatus>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          WhStatus.C200318_SlaveStatus satellite = new WhStatus.C200318_SlaveStatus();
          satellite.Code = item.getFieldVal<string>(startIndex++);
          satellite.Status = item.getFieldVal<WhStatus.C200318_SatelliteStatusEnum>(startIndex++);
          satellite.IsOk = item.getFieldVal<bool>(startIndex++);
          satellite.BatteryLevel = item.getFieldVal<int>(startIndex++);
          satellite.Wifi = item.getFieldVal<bool>(startIndex++);
          list.Add(satellite);
        }
      }
      return dwr;
    }

    public void RM_Wh_Traffic(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Wh_Traffic");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Wh_Traffic(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<WhStatus.C200318_WhTraffic> list = new List<WhStatus.C200318_WhTraffic>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          if (item.Count() > 0)
          {
            int startIndex = 1;
            WhStatus.C200318_WhTraffic whs = new WhStatus.C200318_WhTraffic();

            whs.Area = item.getFieldVal<string>(startIndex++);
            whs.Floor = item.getFieldVal<string>(startIndex++);
            whs.PalletCount = item.getFieldVal<int>(startIndex++);

            list.Add(whs);
          }
        }
      }
      return dwr;
    }

    public void RM_Exit_Traffic(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Exit_Traffic");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Exit_Traffic(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<WhStatus.C200318_WhTrafficExit> list = new List<WhStatus.C200318_WhTrafficExit>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          if (item.Count() > 0)
          {
            int startIndex = 1;
            WhStatus.C200318_WhTrafficExit whs = new WhStatus.C200318_WhTrafficExit();

            whs.Code = item.getFieldVal<string>(startIndex++);
            whs.PalletCount = item.getFieldVal<int>(startIndex++);

            list.Add(whs);
          }
        }
      }
      return dwr;
    }

    #endregion

    #region Article Manager

    public void AM_Article_GetAll(object receiverInstance, C200318_ArticleFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("AM_Article_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel AM_Article_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_Article> list = new List<C200318_Article>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var listItem = RM_FromXmlTo_Article(item, ref startIndex);
          listItem.IsDeleted = item.getFieldVal<bool>(startIndex++);
          list.Add(listItem);
        }
      }
      return dwr;
    }

    public void AM_Article_Update(object receiverInstance, C200318_Article art)
    {
      var parameters = new List<string>();
      parameters.Add("AM_Article_Update");

      parameters.Add(art.Code?.Base64Encode());
      parameters.Add(art.Descr?.Base64Encode());
      parameters.Add(art.Unit?.Base64Encode());
      parameters.Add(art.Type?.Base64Encode());
      parameters.Add(art.IsDeleted.ToString());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel AM_Article_Update(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200318_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);
        dwr.Data = result;
      }
      return dwr;
    }

    public void AM_Article_Delete(object receiverInstance, C200318_Article art)
    {
      var parameters = new List<string>();
      parameters.Add("AM_Article_Delete");

      parameters.Add(art.Code?.Base64Encode());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel AM_Article_Delete(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;

        C200318_ArticleDeleteResults result = new C200318_ArticleDeleteResults();

        result.DeletableResult = resp.Items[0].getFieldVal<Results_Enum>(startIndex++);
        result.DeleteResult = resp.Items[0].getFieldVal<C200318_CustomResult>(startIndex++);

        dwr.Data = result;
      }
      return dwr;
    }

    public void AM_Article_Import(object receiverInstance, C200318_Article art)
    {
      var parameters = new List<string>();
      parameters.Add("AM_Article_Import");

      parameters.Add(art.Code?.Base64Encode());
      parameters.Add(art.Descr?.Base64Encode());
      parameters.Add(art.Unit?.Base64Encode());
      parameters.Add(art.Type?.Base64Encode());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel AM_Article_Import(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200318_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);
        dwr.Data = result;
      }
      return dwr;
    }

    #endregion

    #region MAPMANAGER
    public void MM_RedestinatePallet(object receiverInstance, string palletCode, string positionCode)
    {
      var parameters = new List<string>();
      parameters.Add("MM_RedestinatePallet");
      parameters.Add(palletCode);
      parameters.Add(positionCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel MM_RedestinatePallet(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        int startIndex = 1;
        dwr.Result = FromXmlTo_Result(items[0], ref startIndex).ToString();
      }
      return dwr;
    }

    #endregion

    #region ADMINISTRATION
    public void ADM_Master_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("ADM_Master_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel ADM_Master_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<Administration.C200318_Master> masterL = new List<Administration.C200318_Master>();
      dwr.Data = masterL;
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        foreach (var item in items)
        {
          int startIndex = 1;
          Administration.C200318_Master master = new Administration.C200318_Master(item.getFieldVal<string>(startIndex++)
            , item.getFieldVal<Administration.C200318_MasterCategory>(startIndex++));
          masterL.Add(master);
        }
      }
      return dwr;
    }

    public void ADM_Master_GetByCode(object receiverInstance, string masterCode)
    {
      var parameters = new List<string>();
      parameters.Add("ADM_Master_GetByCode");
      parameters.Add(masterCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel ADM_Master_GetByCode(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        int startIndex = 1;
        var item = items[0];
        Administration.C200318_Master master = new Administration.C200318_Master(item.getFieldVal<string>(startIndex++), item.getFieldVal<Administration.C200318_MasterCategory>(startIndex++));
        dwr.Data = master;
      }
      return dwr;
    }

    public void ADM_Master_Update(object receiverInstance, string masterCode, Administration.C200318_MasterCategory category)
    {
      var parameters = new List<string>();
      parameters.Add("ADM_Master_Update");
      parameters.Add(masterCode);
      parameters.Add(category.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel ADM_Master_Update(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        int startIndex = 1;
        dwr.Result = FromXmlTo_Result(items[0], ref startIndex).ToString();
      }
      return dwr;
    }

    public void ADM_ExitPosition_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("ADM_ExitPosition_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel ADM_ExitPosition_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<Administration.C200318_ExitPosition> list = new List<Administration.C200318_ExitPosition>();
      dwr.Data = list;
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        foreach (var item in items)
        {
          int startIndex = 1;
          Administration.C200318_ExitPosition element = new Administration.C200318_ExitPosition(item.getFieldVal<string>(startIndex++), item.getFieldVal<int>(startIndex++));
          list.Add(element);
        }
      }
      return dwr;
    }

    public void ADM_ExitPosition_Update(object receiverInstance, string posCode, int maxPalletInTransit)
    {
      var parameters = new List<string>();
      parameters.Add("ADM_ExitPosition_Update");
      parameters.Add(posCode);
      parameters.Add(maxPalletInTransit.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel ADM_ExitPosition_Update(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        int startIndex = 1;
        dwr.Result = FromXmlTo_Result(items[0], ref startIndex).ToString();
      }
      return dwr;
    }

    public void ADM_ExitPosition_GetByCode(object receiverInstance, string posCode)
    {
      var parameters = new List<string>();
      parameters.Add("ADM_ExitPosition_GetByCode");
      parameters.Add(posCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel ADM_ExitPosition_GetByCode(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        int startIndex = 1;
        var item = items[0];
        Administration.C200318_ExitPosition pos = new Administration.C200318_ExitPosition(item.getFieldVal<string>(startIndex++), item.getFieldVal<int>(startIndex++));
        dwr.Data = pos;
      }
      return dwr;
    }


    #endregion

    #region WEBCAMS
    //WCM_WebCam_GetAll
    //WCM_WebCam_Get
    //WCM_WebCam_Update
    public void WCM_WebCam_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("WCM_WebCam_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel WCM_WebCam_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_WebCam> list = new List<C200318_WebCam>();
      dwr.Data = list;
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        foreach (var item in items)
        {
          int startIndex = 1;
          var wc = FromXmlTo_WebCam(item, ref startIndex);
          list.Add(wc);
        }
      }
      return dwr;
    }
    public void WCM_WebCam_Get(object receiverInstance, int id)
    {
      var parameters = new List<string>();
      parameters.Add("WCM_WebCam_Get");
      parameters.Add(id.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel WCM_WebCam_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        int startIndex = 1;
        var wc = FromXmlTo_WebCam(items[0], ref startIndex);
        dwr.Data = wc;

      }
      return dwr;
    }

    public void WCM_WebCam_Update(object receiverInstance, C200318_WebCam wc)
    {
      var parameters = new List<string>();
      parameters.Add("WCM_WebCam_Update");
      parameters.AddRange(new string[] { wc.Id.ToString(),
        wc.MasterCode,
        wc.Side.ToString(),
        wc.IpAddress,
        wc.PortNumber.ToString(),
        wc.UserName.Base64Encode(),
        wc.Password.Base64Encode(),
        wc.PresetHome.ToString(),
        wc.PresetLeft.ToString(),
        wc.PresetRight.ToString(),
        wc.Enabled.ToString()});
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel WCM_WebCam_Update(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(items[0], ref startIndex);
      }
      return dwr;
    }

    //WCM_WebCam_Move

    public void WCM_WebCam_Move(object receiverInstance, int webCamId, int preset)
    {
      var parameters = new List<string>();
      parameters.Add("WCM_WebCam_Move");
      parameters.AddRange(new string[] { webCamId.ToString(), preset.ToString() });
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel WCM_WebCam_Move(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(items[0], ref startIndex);
      }
      return dwr;
    }

    private C200318_WebCam FromXmlTo_WebCam(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200318_WebCam webCam = new C200318_WebCam();
      webCam.Id = item.getFieldVal<int>(startIndex++);
      webCam.Enabled = item.getFieldVal<bool>(startIndex++);
      webCam.MasterCode = item.getFieldVal<string>(startIndex++);
      webCam.Side = item.getFieldVal<int>(startIndex++);
      webCam.IpAddress = item.getFieldVal<string>(startIndex++);
      webCam.PortNumber = item.getFieldVal<int>(startIndex++);
      webCam.UserName = item.getFieldVal<string>(startIndex++);
      webCam.Password = item.getFieldVal<string>(startIndex++);
      webCam.PresetHome = item.getFieldVal<int>(startIndex++);
      webCam.PresetLeft = item.getFieldVal<int>(startIndex++);
      webCam.PresetRight = item.getFieldVal<int>(startIndex++);
      return webCam;
    }
    #endregion

    #region FROM XML TO
    private C200318_CustomResult FromXmlTo_Result(XmlCommandResponse.Item item, ref int startIndex)
    {
      return item.getFieldVal<C200318_CustomResult>(startIndex++);
    }

    #endregion

    #region  METODI UTILITA
    private static PluginInfo Plugin_FromXml(XmlCommandResponse.Item item)
    {
      int i = 1;

      var name = item.getFieldVal(i++);
      if (name == "null")
        return null;
      var fileName = item.getFieldVal(i++);
      var version = item.getFieldVal(i++);
      var status = item.getFieldVal<Plugin_State>(i++);

      return new PluginInfo(name) { FileName = fileName, Version = version, Status = status };
    }


    #endregion



    #region DECODIFICA

    public override ICommandResponse PreDecode(MsgEventArgs e)
    {
      XmlCommandResponse resp = e.iXmlStreamer as XmlCommandResponse;
      if (resp == null)
        return null;

      //restituisco la risposta al comando
      return new CommandResponseC200318(resp.xmlCommand);
    }

    public override ICommandResponse Decode(MsgEventArgs e)
    {
      XmlCommandResponse resp = e.iXmlStreamer as XmlCommandResponse;
      if (resp == null)
        return null;

      //recupero la risposta al comando
      var commandResponse = new CommandResponseC200318(resp.xmlCommand);

      try
      {
        //invoco il metodo
        var respModel = (IModel)GetType().InvokeMethod(this, commandResponse.CommandName, new object[] { resp });

        //setto il dato
        commandResponse.ResponseModel = respModel;
      }
      catch (Exception ex)
      {
        Console.WriteLine($"{commandResponse.CommandName} => Exception={ex.Message}");
      }

      return commandResponse;
    }

    public override ICommandResponse Decode(ExceptionEventArgs e)
    {
      var xmlCmd = e.xmlCommand as XmlCommand;
      if (xmlCmd == null)
        return null;

      //restituisco la risposta al comando
      return new CommandResponseC200318(xmlCmd);
    }

    #endregion


    #region DEFRAG
    public void DFG_DefragStartingCondition_Get(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("DFG_DefragStartingCondition_Get");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel DFG_DefragStartingCondition_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        var item = items[0];
        int startIndex = 1;
        C200318_DefragStartingCondition dfsc = new C200318_DefragStartingCondition();
        dfsc.StartType = item.getFieldVal<C200318_DefragStartTypeEnum>(startIndex++);
        dfsc.Enabled = item.getFieldVal<bool>(startIndex++);

        dfsc.DateTimeFrom = item.getFieldVal<DateTime>(startIndex++);
        dfsc.DateTimeTo = item.getFieldVal<DateTime>(startIndex++);

        dfsc.Monday = item.getFieldVal<bool>(startIndex++);
        dfsc.Tuesday = item.getFieldVal<bool>(startIndex++);
        dfsc.Wednesday = item.getFieldVal<bool>(startIndex++);
        dfsc.Thursday = item.getFieldVal<bool>(startIndex++);
        dfsc.Friday = item.getFieldVal<bool>(startIndex++);
        dfsc.Saturday = item.getFieldVal<bool>(startIndex++);
        dfsc.Sunday = item.getFieldVal<bool>(startIndex++);

        dwr.Data = dfsc;
      }
      return dwr;
    }


    public void DFG_DefragStartingCondition_Set(object receiverInstance, C200318_DefragStartingCondition dfgsc)
    {
      var parameters = new List<string>();
      parameters.Add("DFG_DefragStartingCondition_Set");
      parameters.Add(dfgsc.StartType.ToString());
      parameters.Add(dfgsc.Enabled.ToString());
      parameters.Add(dfgsc.DateTimeFrom.ConvertToDateTimeFormatString());
      parameters.Add(dfgsc.DateTimeTo.ConvertToDateTimeFormatString());
      parameters.Add(dfgsc.Monday.ConvertToString());
      parameters.Add(dfgsc.Tuesday.ConvertToString());
      parameters.Add(dfgsc.Wednesday.ConvertToString());
      parameters.Add(dfgsc.Thursday.ConvertToString());
      parameters.Add(dfgsc.Friday.ConvertToString());
      parameters.Add(dfgsc.Saturday.ConvertToString());
      parameters.Add(dfgsc.Sunday.ConvertToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel DFG_DefragStartingCondition_Set(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      dwr.Result = (items != null && items.Count() > 0) ? items[0].getFieldVal(1) : "";
      return dwr;
    }


    public void DFG_DefragStatus_Get(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("DFG_DefragStatus_Get");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel DFG_DefragStatus_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        var item = items[0];
        int startIndex = 1;
        C200318_DefragStatus dfs = new C200318_DefragStatus();
        dfs.Id = item.getFieldVal<int>(startIndex++);
        dfs.StartDate = item.getFieldVal<DateTime>(startIndex++);
        dfs.EndDate = item.getFieldVal<DateTime>(startIndex++);
        dfs.Status = item.getFieldVal<C200318_DefragStatusEnum>(startIndex++, true);
        dfs.Result = FromXmlTo_DefragResult(item, ref startIndex);
        dwr.Data = dfs;
      }
      return dwr;
    }

    public void DFG_DefragStatus_Set(object receiverInstance, C200318_DefragStatus dfgs)
    {
      var parameters = new List<string>();
      parameters.Add("DFG_DefragStatus_Get");
      parameters.Add(dfgs.Status.ConvertToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel DFG_DefragStatus_Set(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      dwr.Result = (items != null && items.Count() > 0) ? items[0].getFieldVal(1) : "";
      return dwr;
    }

    public void DFG_DefragStatus_Start(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("DFG_DefragStatus_Start");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel DFG_DefragStatus_Start(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      dwr.Result = (items != null && items.Count() > 0) ? items[0].getFieldVal(1) : "";
      return dwr;
    }
    public void DFG_DefragStatus_Stop(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("DFG_DefragStatus_Stop");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel DFG_DefragStatus_Stop(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      dwr.Result = (items != null && items.Count() > 0) ? items[0].getFieldVal(1) : "";
      return dwr;
    }

    public void DFG_DefragStepList_Get(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("DFG_DefragStepList_Get");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel DFG_DefragStepList_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null)
      {
        List<C200318_DefragStep> dfgstepL = new List<C200318_DefragStep>();
        dwr.Data = dfgstepL;
        foreach (var item in items)
        {
          int startIndex = 1;
          C200318_DefragStep dfgstep = new C200318_DefragStep();
          dfgstep.Id = item.getFieldVal<int>(startIndex++);
          dfgstep.DateBorn = item.getFieldVal<DateTime>(startIndex++);
          dfgstep.DateEnd = item.getFieldVal<DateTime>(startIndex++);
          dfgstep.FromLane = item.getFieldVal<string>(startIndex++);
          dfgstep.ToLane = item.getFieldVal<string>(startIndex++);
          dfgstep.Status = item.getFieldVal<C200318_DefragStatusEnum>(startIndex++);
          dfgstep.PalletAmount = item.getFieldVal<int>(startIndex++);
          dfgstep.NumPalletTransferred = item.getFieldVal<int>(startIndex++);
          dfgstep.StartingCondition = item.getFieldVal<C200318_DefragStartTypeEnum>(startIndex++);
          dfgstep.DefragStatusId = item.getFieldVal<int>(startIndex++);

          dfgstepL.Add(dfgstep);
        }
      }

      return dwr;
    }


    public void DFG_DefragParameters_Get(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("DFG_DefragParameters_Get");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel DFG_DefragParameters_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();

      List<C200318_DefragParameter> dfgpL = new List<C200318_DefragParameter>();
      dwr.Data = dfgpL;
      foreach (var item in items)
      {
        int startIndex = 1;
        C200318_DefragParameter dfgp = new C200318_DefragParameter();
        dfgp.ParameterName = item.getFieldVal(startIndex++);
        dfgp.ParameterDescription= item.getFieldVal(startIndex++).Base64Decode();
        dfgp.ParameterValue= item.getFieldVal(startIndex++);
        dfgpL.Add(dfgp);
      }
      return dwr;
    }

    public void DFG_DefragParameter_Set(object receiverInstance, C200318_DefragParameter dfgp)
    {
      var parameters = new List<string>();
      parameters.Add("DFG_DefragParameter_Set");
      parameters.Add(dfgp.ParameterName);
      parameters.Add(dfgp.ParameterDescription.Base64Encode());
      parameters.Add(dfgp.ParameterValue);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel DFG_DefragParameter_Set(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      dwr.Result = (items != null && items.Count() > 0) ? items[0].getFieldVal(1) : "";
      return dwr;
    }

    private C200318_DefragResultEnum FromXmlTo_DefragResult(XmlCommandResponse.Item item, ref int startIndex)
    {
      return item.getFieldVal<C200318_DefragResultEnum>(startIndex++);
    }
    #endregion
  }
}

