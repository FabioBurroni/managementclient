﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Model.Custom.C200318
{
  public class C200318_TrafficBoardTaskFilter : ModelBase
  {
    public delegate void OnSearchHandler();

    public event OnSearchHandler OnSearch;

    #region Constructor

    public C200318_TrafficBoardTaskFilter()
    {
    }

    #endregion Constructor

    #region Paginazione

    public ObservableCollectionFast<int> MaxItemsL { get; set; } = new ObservableCollectionFast<int>() { 10, 50, 100, 500, 1000 };

    private int _startIndex = 0;

    private int _Index = 0;

    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _MaxItems = 100;

    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    #endregion Paginazione

    #region Public Properties

    private string _BoxCode;

    public string BoxCode
    {
      get { return _BoxCode; }
      set
      {
        if (value != _BoxCode)
        {
          _BoxCode = value;
          NotifyPropertyChanged();
        }
      }
    }

  

    private long _Id = 0;

    public long Id
    {
      get { return _Id; }
      set
      {
        if (value != _Id)
        {
          _Id = value;
          NotifyPropertyChanged();
        }
      }
    }

  

    private DateTime _DateFrom = DateTime.Now;

    public DateTime DateFrom
    {
      get { return _DateFrom; }
      set
      {
        if (value != _DateFrom)
        {
          _DateFrom = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateTo = DateTime.Now;

    public DateTime DateTo
    {
      get { return _DateTo; }
      set
      {
        if (value != _DateTo)
        {
          _DateTo = value;
          NotifyPropertyChanged();
        }
      }
    }

    #endregion Public Properties

    #region Public Properties Parameters Enabled

    private bool _ByDateFrom;

    public bool ByDateFrom
    {
      get { return _ByDateFrom; }
      set
      {
        if (value != _ByDateFrom)
        {
          _ByDateFrom = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _ByDateTo;

    public bool ByDateTo
    {
      get { return _ByDateTo; }
      set
      {
        if (value != _ByDateTo)
        {
          _ByDateTo = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _ByBoxCode;

    public bool ByBoxCode
    {
      get { return _ByBoxCode; }
      set
      {
        if (value != _ByBoxCode)
        {
          _ByBoxCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _BySource;

    public bool BySource
    {
      get { return _BySource; }
      set
      {
        if (value != _BySource)
        {
          _BySource = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _ByDestination;

    public bool ByDestination
    {
      get { return _ByDestination; }
      set
      {
        if (value != _ByDestination)
        {
          _ByDestination = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _ById;

    public bool ById
    {
      get { return _ById; }
      set
      {
        if (value != _ById)
        {
          _ById = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _ByState;

    public bool ByState
    {
      get { return _ByState; }
      set
      {
        if (value != _ByState)
        {
          _ByState = value;
          NotifyPropertyChanged();
        }
      }
    }

    #endregion Public Properties Parameters Enabled

    #region Public Methods

    public void FireOnSearch()
    {
      Index = _startIndex;
      OnSearch?.Invoke();
    }

    public void Reset()
    {
      Index = _startIndex;
      BoxCode = string.Empty;
   
      Id = 0;
      DateFrom = DateTime.Now;
      DateTo = DateTime.Now;

      ByBoxCode = false;
      BySource = false;
      ByDestination = false;
      ById = false;
      ByState = false;
      ByDateFrom = false;
      ByDateTo = false;
    }

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        Index.ToString(),
        MaxItems.ToString(),
        ByBoxCode ? BoxCode:string.Empty,
        
        ById ? Id.ToString() : "0",
        ByDateFrom ? DateFrom.ConvertToDateTimeFormatString():string.Empty,
        ByDateTo ? DateTo.ConvertToDateTimeFormatString():string.Empty,
      };
    }

    #endregion Public Methods
  }
}