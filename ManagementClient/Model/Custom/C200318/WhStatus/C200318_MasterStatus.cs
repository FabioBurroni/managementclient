﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.WhStatus
{
  public class C200318_MasterStatus:ModelBase
  {

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C200318_SatelliteStatusEnum _Status = C200318_SatelliteStatusEnum.PLAY;
    public C200318_SatelliteStatusEnum Status
    {
      get { return _Status; }
      set
      {
        if (value != _Status)
        {
          _Status = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsOk;
    public bool IsOk
    {
      get { return _IsOk; }
      set
      {
        if (value != _IsOk)
        {
          _IsOk = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsMaintenaceRequest;
    public bool IsMaintenaceRequest
    {
      get { return _IsMaintenaceRequest; }
      set
      {
        if (value != _IsMaintenaceRequest)
        {
          _IsMaintenaceRequest = value;
          NotifyPropertyChanged();
        }
      }
    }


    public void Update(C200318_MasterStatus newMaster)
    {
      this.Status = newMaster.Status;
      this.IsOk= newMaster.IsOk;
      this.IsMaintenaceRequest = newMaster.IsMaintenaceRequest;
    }
  }
}
