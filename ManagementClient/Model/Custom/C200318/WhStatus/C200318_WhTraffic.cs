﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.WhStatus
{
  public class C200318_WhTraffic : ModelBase
  {
    private string _Area;
    public string Area
    {
      get { return _Area; }
      set
      {
        if (value != _Area)
        {
          _Area = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Floor;
    public string Floor
    {
      get { return _Floor; }
      set
      {
        if (value != _Floor)
        {
          _Floor = value;
          NotifyPropertyChanged();
        }
      }
    }

   
    private int _PalletCount;
    public int PalletCount
    {
      get { return _PalletCount; }
      set
      {
        if (value != _PalletCount)
        {
          _PalletCount = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _PalletCountTotal;
    public int PalletCountTotal
    {
      get { return _PalletCountTotal; }
      set
      {
        if (value != _PalletCountTotal)
        {
          _PalletCountTotal = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _Percentage;
    public int Percentage
    {
      get { return _Percentage; }
      set
      {
        if (value != _Percentage)
        {
          _Percentage = value;
          NotifyPropertyChanged();
        }
      }
    }

    public void Update(C200318_WhTraffic newWhs)
    {
    
      this.Area = newWhs.Area;
      this.Floor = newWhs.Floor;
      this.PalletCount = newWhs.PalletCount;

      this.PalletCountTotal = newWhs.PalletCountTotal;
    
      if (PalletCountTotal > 0)
      {
        Percentage = (int)Math.Floor(((decimal)PalletCount / (decimal)PalletCountTotal) *100);
      }
      else
      {
        Percentage = 0;
      }
    }
  }
}
