﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.WhStatus
{
  public enum C200318_SatelliteStatusEnum
  {
    PLAY = 1,
    STOP = 2,
    ERROR = 3,
    RESTORE = 4,
    RESET = 5,
    CHARGING = 6,
    PARKING = 7,
    MANUAL = 8,
  }
}
