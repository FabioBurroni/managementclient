﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.WhStatus
{
  public class C200318_SlaveStatus:ModelBase
  {

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C200318_SatelliteStatusEnum _Status=C200318_SatelliteStatusEnum.PLAY;
    public C200318_SatelliteStatusEnum Status
    {
      get { return _Status; }
      set
      {
        if (value != _Status)
        {
          _Status = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsOk;
    public bool IsOk
    {
      get { return _IsOk; }
      set
      {
        if (value != _IsOk)
        {
          _IsOk = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _BatteryLevel;
    public int BatteryLevel
    {
      get { return _BatteryLevel; }
      set
      {
        if (value != _BatteryLevel)
        {
          _BatteryLevel = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _Wifi;
    public bool Wifi
    {
      get { return _Wifi; }
      set
      {
        if (value != _Wifi)
        {
          _Wifi = value;
          NotifyPropertyChanged();
        }
      }
    }


    public void Update(C200318_SlaveStatus newSlave)
    {
      this.BatteryLevel = newSlave.BatteryLevel;
      this.IsOk= newSlave.IsOk;
      this.Status= newSlave.Status;
      this.Wifi= newSlave.Wifi;
    }
  }
}
