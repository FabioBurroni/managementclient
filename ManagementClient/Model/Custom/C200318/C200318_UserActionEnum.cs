﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318
{
  public enum C200318_UserActionEnum
  {
    BcrConfiguration,
    ShippingChannelEnabled,
    ShippingChannelUrgent,
    ShippingChannelConfiguration,
    OrderKill,
    OrderPause,
    OrderExec,
    OrderPriority,
    OrderUrgent,
    ManagerAdministrator,
    ManagerConfiguration,
    OrderCompletamentoManuale,
    ManualCheckOut,
  }
}
