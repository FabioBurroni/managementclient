﻿using System;
namespace Model.Custom.C200318.PalletManager
{
  public class C200318_PalletCheckResult : ModelBase
  {
    private C200318_CustomResult _Result;

    public C200318_CustomResult Result
    {
      get { return _Result; }
      set 
      {
        if (value != _Result)
        {
          _Result = value;
          NotifyPropertyChanged();
        }
      }
    }



    private C200318_PalletInsert _Pallet ;
    public C200318_PalletInsert Pallet
    {
      get { return _Pallet; }
      set
      {
        if (value != _Pallet)
        {
          _Pallet = value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
