﻿using System;
namespace Model.Custom.C200318.PalletManager
{
  public class C200318_PalletInsert : ModelBase
  {

    private string _Code = string.Empty;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleCode = string.Empty;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _LotCode = string.Empty;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _ExpiryDate = DateTime.Now;
    public DateTime ExpiryDate
    {
      get { return _ExpiryDate; }
      set
      {
        if (value != _ExpiryDate)
        {
          _ExpiryDate = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _Quantity;
    public int Quantity
    {
      get { return _Quantity; }
      set
      {
        if (value != _Quantity)
        {
          _Quantity = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _LotLength;
    public int LotLength
    {
      get { return _LotLength; }
      set
      {
        if (value != _LotLength)
        {
          _LotLength = value;
          NotifyPropertyChanged();
        }
      }
    }


    public void Update(C200318_PalletInsert newPallet)
    {
      this.Code = newPallet.Code;
      this.ArticleCode = newPallet.ArticleCode;
      this.LotCode = newPallet.LotCode;
      this.ExpiryDate = newPallet.ExpiryDate;
      this.Quantity = newPallet.Quantity;
      this.LotLength = newPallet.LotLength;
    }

    public void Reset()
    {
      this.Code = string.Empty;
      this.ArticleCode = string.Empty;
      this.LotCode = string.Empty;
      this.ExpiryDate = DateTime.Now;
      this.Quantity = 0;
      this.LotLength = 0;
    }
  }
}
