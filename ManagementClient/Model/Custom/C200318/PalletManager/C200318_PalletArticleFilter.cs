﻿using System.Collections.Generic;
using Utilities.Extensions;

namespace Model.Custom.C200318.PalletManager
{
  public class C200318_PalletArticleFilter : ModelBase
  {

    public C200318_PalletArticleFilter()
    {
     
    }

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; } 
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000 };
    #endregion

    private string _Code = string.Empty;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _Descr = string.Empty;
    public string Descr
    {
      get { return _Descr; }
      set
      {
        if (value != _Descr)
        {
          _Descr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Type = string.Empty;
    public string Type
    {
      get { return _Type; }
      set
      {
        if (value != _Type)
        {
          _Type = value;
          NotifyPropertyChanged();
        }
      }
    }


    #region public methods
    public void Reset()
    {
      Code = string.Empty;      
      Descr= string.Empty;
      Type = string.Empty;
    }
    #endregion

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        Code==null?"":Code.Base64Encode(),
        Descr==null?"":Descr.Base64Encode(),
        Type==null?"":Descr.Base64Encode()
      };
    }

  }
}
