﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318
{
  public class C200318_ShippingLaneConf : ModelBase
  {
    public C200318_ShippingLaneConf()
    {

    }
    public C200318_ShippingLaneConf(C200318_ShippingLaneConf shNew)
    {
      _Code = shNew.Code;
      _Descr = shNew.Descr;
      _BufferCode = shNew.BufferCode;
    }

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Descr;
    public string Descr
    {
      get { return _Descr; }
      set
      {
        if (value != _Descr)
        {
          _Descr = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _BufferCode;
    public string BufferCode
    {
      get { return _BufferCode; }
      set
      {
        if (value != _BufferCode)
        {
          _BufferCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    #region Proprietà Modificabili

    private bool _Enabled;
    public bool Enabled
    {
      get { return _Enabled; }
      set
      {
        if (value != _Enabled)
        {
          _Enabled = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsChanged");
        }
      }
    }

    private int _MaxPalletInPos;
    public int MaxPalletInPos
    {
      get { return _MaxPalletInPos; }
      set
      {
        if (value != _MaxPalletInPos)
        {
          _MaxPalletInPos = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsChanged");
        }
      }
    }


    private int _MaxPalletInTransit;
    public int MaxPalletInTransit
    {
      get { return _MaxPalletInTransit; }
      set
      {
        if (value != _MaxPalletInTransit)
        {
          _MaxPalletInTransit = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsChanged");
        }
      }
    }



    private bool _Urgent;
    public bool Urgent
    {
      get { return _Urgent; }
      set
      {
        if (value != _Urgent)
        {
          _Urgent = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsChanged");
        }
      }
    }

    #endregion


    private int _PalletInPositionCount;
    public int PalletInPositionCount
    {
      get { return _PalletInPositionCount; }
      set
      {
        if (value != _PalletInPositionCount)
        {
          _PalletInPositionCount = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _PalletInTransitFormAutoWarehouse;
    public int PalletInTransitFormAutoWarehouse
    {
      get { return _PalletInTransitFormAutoWarehouse; }
      set
      {
        if (value != _PalletInTransitFormAutoWarehouse)
        {
          _PalletInTransitFormAutoWarehouse = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _PalletInTransitFromManWarehouse;
    public int PalletInTransitFromManWarehouse
    {
      get { return _PalletInTransitFromManWarehouse; }
      set
      {
        if (value != _PalletInTransitFromManWarehouse)
        {
          _PalletInTransitFromManWarehouse = value;
          NotifyPropertyChanged();
        }
      }
    }


    public ObservableCollectionFast<C200318_ShippingLaneOrder> OrderL { get; set; } = new ObservableCollectionFast<C200318_ShippingLaneOrder>();

    C200318_ShippingLaneConf _original;

    public void Update(C200318_ShippingLaneConf shNew)
    {
      _original = shNew;
      this.MaxPalletInPos = shNew.MaxPalletInPos;
      this.MaxPalletInTransit = shNew.MaxPalletInTransit;
      
      this.Urgent = shNew.Urgent;
      this.Enabled = shNew.Enabled;

      this.PalletInPositionCount= shNew.PalletInPositionCount;
      this.PalletInTransitFromManWarehouse= shNew.PalletInTransitFromManWarehouse;
      this.PalletInTransitFormAutoWarehouse = shNew.PalletInTransitFormAutoWarehouse;

      UpdateOrders(shNew);
      NotifyPropertyChanged("IsChanged");

    }

    private void UpdateOrders(C200318_ShippingLaneConf shNew)
    {
      OrderL.Clear();
      OrderL.AddRange(shNew.OrderL.ToList());
    }
  public bool IsChanged
    {
      get
      {
        if(_original!=null)
        {

          return this.MaxPalletInPos != _original.MaxPalletInPos ||
            this.MaxPalletInTransit != _original.MaxPalletInTransit ||
            this.Urgent != _original.Urgent ||
            this.Enabled != _original.Enabled;


        }
        return false;
      }
    }
      





  }
}
