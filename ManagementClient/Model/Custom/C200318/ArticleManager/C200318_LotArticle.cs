﻿using LINQtoCSV;
namespace Model.Custom.C200318.ArticleManager
{
  public class C200318_LotArticle : ModelBase
  {

    private C200318_Lot _Lot;
    public C200318_Lot Lot
    {
      get { return _Lot; }
      set
      {
        if (value != _Lot)
        {
          _Lot = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200318_Article _Article;
    public C200318_Article Article
    {
      get { return _Article; }
      set
      {
        if (value != _Article)
        {
          _Article = value;
          NotifyPropertyChanged();
        }
      }
    }





  }
}
