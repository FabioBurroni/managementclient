﻿
namespace Model.Custom.C200318.ArticleManager
{
  public class C200318_ArticleImport : C200318_Article
  { 
    private bool _Selected;
    public bool Selected
    {
      get { return _Selected; }
      set
      {
        if (value != _Selected)
        {
          _Selected = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _Valid;
    public bool Valid
    {
      get { return _Valid; }
      set
      {
        if (value != _Valid)
        {
          _Valid = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200318_CustomResult _Result;
    public C200318_CustomResult Result
    {
      get { return _Result; }
      set
      {
        if (value != _Result)
        {
          _Result = value;
          NotifyPropertyChanged();
        }
      }
    }

    

  }
}
