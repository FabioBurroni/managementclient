﻿
using LINQtoCSV;

namespace Model.Custom.C200318.ArticleManager
{
  public class C200318_Article : ModelBase
  {

    private string _Code;
    [CsvColumn(Name = "Code", FieldIndex = 1)]
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Descr;

    [CsvColumn(Name = "Description", FieldIndex = 2)]
    public string Descr
    {
      get { return _Descr; }
      set
      {
        if (value != _Descr)
        {
          _Descr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200318_PalletType _PalletTypeId = C200318_PalletType.PBR;
    public C200318_PalletType PalletTypeId
    {
      get { return _PalletTypeId; }
      set
      {
        if (value != _PalletTypeId)
        {
          _PalletTypeId = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _AW_MinQuantity;
    public int AW_MinQuantity
    {
      get { return _AW_MinQuantity; }
      set
      {
        if (value != _AW_MinQuantity)
        {
          _AW_MinQuantity = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _AW_MaxQuantity;
    public int AW_MaxQuantity
    {
      get { return _AW_MaxQuantity; }
      set
      {
        if (value != _AW_MaxQuantity)
        {
          _AW_MaxQuantity = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _StockWindow;
    public int StockWindow
    {
      get { return _StockWindow; }
      set
      {
        if (value != _StockWindow)
        {
          _StockWindow = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _FifoWindow;
    public int FifoWindow
    {
      get { return _FifoWindow; }
      set
      {
        if (value != _FifoWindow)
        {
          _FifoWindow = value;
          NotifyPropertyChanged();
        }
      }
    }
       
    private C200318_TurnoverEnum _Turnover;
    public C200318_TurnoverEnum Turnover
    {
      get { return _Turnover; }
      set
      {
        if (value != _Turnover)
        {
          _Turnover = value;
          NotifyPropertyChanged();
        }
      }
    }
      
    private string _Unit;

    [CsvColumn(Name = "Unit", FieldIndex = 3)]
    public string Unit
    {
      get { return _Unit; }
      set
      {
        if (value != _Unit)
        {
          _Unit = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _Type;

    [CsvColumn(Name = "Type", FieldIndex = 4)]
    public string Type
    {
      get { return _Type; }
      set
      {
        if (value != _Type)
        {
          _Type = value;
          NotifyPropertyChanged();
        }
      }
    }

   
    private bool _IsDeleted;
    public bool IsDeleted
    {
      get { return _IsDeleted; }
      set
      {
        if (value != _IsDeleted)
        {
          _IsDeleted = value;
          NotifyPropertyChanged();
        }
      }
    }
  }
}
