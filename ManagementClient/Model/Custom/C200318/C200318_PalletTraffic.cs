﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Common;

namespace Model.Custom.C200318
{
  public class C200318_PalletTraffic: ModelBase
  {

    private string _Code = string.Empty;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Position = string.Empty;
    public string Position
    {
      get { return _Position; }
      set
      {
        if (value != _Position)
        {
          _Position = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _FinalDestination = string.Empty;
    public string FinalDestination
    {
      get { return _FinalDestination; }
      set
      {
        if (value != _FinalDestination)
        {
          _FinalDestination = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _LotCode;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _OrderCode = string.Empty;
    public string OrderCode
    {
      get { return _OrderCode; }
      set
      {
        if (value != _OrderCode)
        {
          _OrderCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    public void Update(C200318_PalletTraffic newPallet)
    {
      this.Code = newPallet.Code;
      this.Position = newPallet.Position;
      this.FinalDestination = newPallet.FinalDestination;
      this.ArticleCode = newPallet.ArticleCode;
      this.ArticleDescr = newPallet.ArticleDescr;
      this.OrderCode = newPallet.OrderCode;
    }

    public void Reset()
    {
      this.Code = string.Empty;
      this.Position = string.Empty;
      this.FinalDestination = string.Empty;
      this.ArticleCode = string.Empty;
      this.ArticleDescr = string.Empty;
      this.OrderCode = string.Empty;
    }
  }
}
