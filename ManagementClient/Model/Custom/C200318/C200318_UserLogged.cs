﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Model.Custom.C200318
{
  public class C200318_UserLogged:ModelBase
  {

    private bool _IsCassioliLogged;
    public bool IsCassioliLogged
    {
      get { return _IsCassioliLogged; }
      set
      {
        if (value != _IsCassioliLogged)
        {
          _IsCassioliLogged = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _UserName;
    public string UserName
    {
      get { return _UserName; }
      set
      {
        if (value != _UserName)
        {
          _UserName = value;
          IsCassioliLogged = _UserName.EqualsIgnoreCase("cassioli");
          NotifyPropertyChanged();
        }
      }
    }
  }
}
