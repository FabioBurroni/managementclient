﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.Defrag
{
  public enum C200318_DefragStartTypeEnum
  {
    /// <summary>
    /// Operator can start and stop Defrag Procedure at any time.
    /// </summary>
    MANUAL,
    /// <summary>
    /// System start automatically defrag procedure when the system is on rest, at any time.
    /// </summary>
    AUTOMATIC,
    /// <summary>
    /// System start defrag procedure at specific time and day.
    /// </summary>
    BYTIME,
  }
}
