﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.Defrag
{
  public enum C200318_DefragStatusEnum
  {
    /// <summary>
    /// Defrag procedure is stopped
    /// </summary>
    STOPPED,
    /// <summary>
    /// 
    /// Defrag procedure is running
    /// </summary>
    RUNNING,
    /// <summary>
    /// Defrag procedure is completed
    /// </summary>
    COMPLETED,
    /// <summary>
    /// Defrag procedure is deleted
    /// </summary>
    DELETED
  }
}
