﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.Defrag
{
  public class C200318_DefragStartingCondition : ModelBase
  {

    #region IsChanged
    private bool _IsChanged = false;
    /// <summary>
    /// Store if obeject is changed
    /// </summary>
    public bool IsChanged
    {
      get { return _IsChanged; }
      set
      {
        if (value != _IsChanged)
        {
          _IsChanged = value;
          NotifyPropertyChanged();
        }
      }
    } 
    #endregion

    #region NOTIFY PROPERTY
    /// <summary>
    /// Type of starting
    /// </summary>
    private C200318_DefragStartTypeEnum _StartType = C200318_DefragStartTypeEnum.MANUAL;
    public C200318_DefragStartTypeEnum StartType
    {
      get { return _StartType; }
      set
      {
        if (value != _StartType)
        {
          _StartType = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }

    /// <summary>
    /// If true Defrag procedure is enabled.
    /// </summary>

    private bool _Enabled;

    public bool Enabled
    {
      get { return _Enabled; }
      set
      {
        if (value != _Enabled)
        {
          _Enabled = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }

    /// <summary>
    /// Starting Time of the day
    /// </summary>
    /*
    private TimeSpan _From=TimeSpan.Parse("00:00");
    public TimeSpan From 
    {
      get { return _From; }
      set
      {
        if (value != _From)
        {
          _From = value;
          _DateTimeFrom = 
            _DateTimeFrom.Date
            .AddHours(_From.Hours)
            .AddMinutes(_From.Minutes)
            .AddSeconds(0);

          IsChanged = true;
          NotifyPropertyChanged();
          NotifyPropertyChanged("DateTimeFrom");
        }
      }
    }
    */

    public DateTime _DateTimeFrom;
    public DateTime DateTimeFrom
    {
      get 
      { 
        return _DateTimeFrom; 
      }
      set
      {
        if (value != _DateTimeFrom)
        {
          _DateTimeFrom = value;
          IsChanged = true;
          //_From = _DateTimeFrom.TimeOfDay;
          NotifyPropertyChanged();
          //NotifyPropertyChanged("From");
        }
      }
    }

    private DateTime _DateTimeTo;
    public DateTime DateTimeTo
    {
      get { return _DateTimeTo; }
      set
      {
        if (value != _DateTimeTo)
        {
          _DateTimeTo = value;
          IsChanged = true;
          //_To = _DateTimeTo.TimeOfDay;
          NotifyPropertyChanged();
          //NotifyPropertyChanged("To");
        }
      }
    }
    /// <summary>
    /// Monday
    /// </summary>
    private bool _Monday;
    public bool Monday
    {
      get { return _Monday; }
      set
      {
        if (value != _Monday)
        {
          _Monday = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }

    
    private bool _Tuesday;
    public bool Tuesday
    {
      get { return _Tuesday; }
      set
      {
        if (value != _Tuesday)
        {
          _Tuesday= value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }
    private bool _Wednesday;
    public bool Wednesday
    {
      get { return _Wednesday; }
      set
      {
        if (value != _Wednesday)
        {
          _Wednesday = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }
    private bool _Thursday;
    public bool Thursday
    {
      get { return _Thursday; }
      set
      {
        if (value != _Thursday)
        {
          _Thursday = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }
    private bool _Friday;
    public bool Friday
    {
      get { return _Friday; }
      set
      {
        if (value != _Friday)
        {
          _Friday = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _Saturday;
    public bool Saturday
    {
      get { return _Saturday; }
      set
      {
        if (value != _Saturday)
        {
          _Saturday = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _Sunday;
    public bool Sunday
    {
      get { return _Sunday; }
      set
      {
        if (value != _Sunday)
        {
          _Sunday = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }
    #endregion

    #region CALCULATED PROPERTY
    private bool _CanStartAndStopManaully;
    public bool CanStartAndStopManaully
    {
      get { return _CanStartAndStopManaully; }
      set
      {
        if (value != _CanStartAndStopManaully)
        {
          _CanStartAndStopManaully = value;
          NotifyPropertyChanged();
        }
      }
    }


    #endregion

    #region PUBLIC METHODS
    public void Update(C200318_DefragStartingCondition dfgsc)
    {
      Update(dfgsc.StartType, dfgsc.Enabled, dfgsc.DateTimeFrom, dfgsc.DateTimeTo, dfgsc.Monday, dfgsc.Tuesday, dfgsc.Wednesday, dfgsc.Thursday, dfgsc.Friday, dfgsc.Saturday, dfgsc.Sunday);
    }

    public void Update(C200318_DefragStartTypeEnum startType, bool enabled, DateTime dateTimeFrom
      , DateTime dateTimeTo, bool monday, bool tuesday, bool wednesday, bool thursday, bool friday, bool saturday, bool sunday)
    {
      StartType = startType;
      Enabled = enabled;
      DateTimeFrom = dateTimeFrom;
      DateTimeTo = dateTimeTo;
      Monday = monday;
      Tuesday = tuesday;
      Wednesday = wednesday;
      Thursday = thursday;
      Friday = friday;
      Saturday = saturday;
      Sunday = sunday;
      IsChanged = false;

      CanStartAndStopManaully = StartType == C200318_DefragStartTypeEnum.MANUAL && Enabled;
    }
    /// <summary>
    /// Returns a copy of the object
    /// </summary>
    /// <returns></returns>
    public C200318_DefragStartingCondition Copy()
    {
      C200318_DefragStartingCondition copy = (C200318_DefragStartingCondition)MemberwiseClone();
      return copy;
    }

    #endregion

  }
}
