﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.Defrag
{
  public static class C200318_DefragExtensions
  {
    public static TimeSpan StringToTimeSpan(this string str)
    {
      TimeSpan ret = TimeSpan.Zero;

      if(TimeSpan.TryParse(str,out ret))
      {
        return ret; 
      }

      return ret;

    }
  }
}
