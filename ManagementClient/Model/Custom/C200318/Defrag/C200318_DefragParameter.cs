﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.Defrag
{
  public class C200318_DefragParameter:ModelBase
  {

    #region CTOR
    public C200318_DefragParameter()
    {
    }
    #endregion

    #region IsChanged
    private bool _IsChanged;
    public bool IsChanged
    {
      get { return _IsChanged; }
      set
      {
        if (value != _IsChanged)
        {
          _IsChanged = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    } 
    #endregion

    #region NOTIFY PROPERTY
    private string _ParameterName;
    public string ParameterName
    {
      get { return _ParameterName; }
      set
      {
        if (value != _ParameterName)
        {
          _ParameterName = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ParameterValue;
    public string ParameterValue
    {
      get { return _ParameterValue; }
      set
      {
        if (value != _ParameterValue)
        {
          _ParameterValue = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ParameterDescription;

    

    public string ParameterDescription
    {
      get { return _ParameterDescription; }
      set
      {
        if (value != _ParameterDescription)
        {
          _ParameterDescription = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }

    #region PUBLIC METHODS
    public void Update(string parameterName, string parameterValue, string parameterDescription)
    {
      ParameterName = parameterName;
      ParameterValue = parameterValue;
      ParameterDescription = parameterDescription;
    }

    public C200318_DefragParameter Copy()
    {
      return (C200318_DefragParameter)MemberwiseClone();
    }

    #endregion

    #endregion

  }
}
