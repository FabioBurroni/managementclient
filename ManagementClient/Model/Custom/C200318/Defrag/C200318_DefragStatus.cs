﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.Defrag
{
  public class C200318_DefragStatus : ModelBase
  {

    #region CTOR
    public C200318_DefragStatus()
    {

    }
    #endregion

    #region IsChanged
    private bool _IsChanged;
    public bool IsChanged
    {
      get { return _IsChanged; }
      set
      {
        if (value != _IsChanged)
        {
          _IsChanged = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    } 
    #endregion

    #region NOTIFY PROPERTY

    private int _Id;
    public int Id
    {
      get { return _Id; }
      set
      {
        if (value != _Id)
        {
          _Id = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }
    /// <summary>
    /// Date Start
    /// </summary>


    private DateTime _StartDate;
    public DateTime StartDate
    {
      get { return _StartDate; }
      set
      {
        if (value != _StartDate)
        {
          _StartDate = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }
    /// <summary>
    /// Date End
    /// </summary>
    private DateTime _EndDate;
    public DateTime EndDate
    {
      get { return _EndDate; }
      set
      {
        if (value != _EndDate)
        {
          _EndDate = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }

    /// <summary>
    /// Status of the defrag procedure
    /// </summary>
    private C200318_DefragStatusEnum _Status;
    public C200318_DefragStatusEnum Status
    {
      get { return _Status; }
      set
      {
        if (value != _Status)
        {
          _Status = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }
    /// <summary>
    /// Result of the defrag procedure
    /// </summary>

    private C200318_DefragResultEnum _Result=C200318_DefragResultEnum.OK;
    public C200318_DefragResultEnum Result
    {
      get { return _Result; }
      set
      {
        if (value != _Result)
        {
          _Result = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }



    private string _ResultTranslated;
    public string ResultTranslated
    {
      get { return _ResultTranslated; }
      set
      {
        if (value != _ResultTranslated)
        {
          _ResultTranslated = value;
          NotifyPropertyChanged();
        }
      }
    }

    #endregion

    #region PUBLIC METHOD
    public void Update(C200318_DefragStatus defragStatus)
    {
      Update(defragStatus.Id, defragStatus.StartDate
        , defragStatus.EndDate, defragStatus.Status
        , defragStatus.Result);
    }
    public void Update(int id, DateTime startDate, DateTime endDate, C200318_DefragStatusEnum status, C200318_DefragResultEnum result)
    {
      
      Id = id;
      StartDate = startDate;
      EndDate = endDate;
      Status = status;
      Result = result;
    }

    public C200318_DefragStatus Copy()
    {
      C200318_DefragStatus copy = (C200318_DefragStatus)MemberwiseClone();
      return copy;
    }
    #endregion

  }
}
