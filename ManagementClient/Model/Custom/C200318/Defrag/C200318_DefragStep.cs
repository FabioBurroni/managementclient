﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200318.Defrag
{
  public class C200318_DefragStep : ModelBase
  {

    #region CTOR
    public C200318_DefragStep()
    {

    }
    #endregion

    #region IsChanged
    private bool _IsChanged;
    public bool IsChanged
    {
      get { return _IsChanged; }
      set
      {
        if (value != _IsChanged)
        {
          _IsChanged = value;
          NotifyPropertyChanged();
        }
      }
    } 
    #endregion

    #region NOTIFY PROPERTY

    /// <summary>
    /// ID
    /// </summary>
    private int _Id;
    public int Id
    {
      get { return _Id; }
      set
      {
        if (value != _Id)
        {
          _Id = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }
    
    /// <summary>
    /// DateBorn
    /// </summary>
    private DateTime _DateBorn;
    public DateTime DateBorn
    {
      get { return _DateBorn; }
      set
      {
        if (value != _DateBorn)
        {
          _DateBorn = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }
    
    /// <summary>
    /// DateEnd
    /// </summary>
    private DateTime _DateEnd;
    public DateTime DateEnd
    {
      get { return _DateEnd; }
      set
      {
        if (value != _DateEnd)
        {
          _DateEnd = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }

    /// <summary>
    /// Source Lane
    /// </summary>
    private string _FromLane = string.Empty;
    public string FromLane
    {
      get { return _FromLane; }
      set
      {
        if (value != _FromLane)
        {
          _FromLane = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }


    /// <summary>
    /// Destination Lane
    /// </summary>
    private string _ToLane = string.Empty;
    public string ToLane
    {
      get { return _ToLane; }
      set
      {
        if (value != _ToLane)
        {
          _ToLane = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }
    /// <summary>
    /// Status
    /// </summary>
    private C200318_DefragStatusEnum _Status = C200318_DefragStatusEnum.STOPPED;
    public C200318_DefragStatusEnum Status
    {
      get { return _Status; }
      set
      {
        if (value != _Status)
        {
          _Status = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }

    /// <summary>
    /// Number of pallet to defrag
    /// </summary>
    private int _PalletAmount;
    public int PalletAmount
    {
      get { return _PalletAmount; }
      set
      {
        if (value != _PalletAmount)
        {
          _PalletAmount = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }
    /// <summary>
    /// Number of pallet transferrred
    /// </summary>
    private int _NumPalletTransferred;
    public int  NumPalletTransferred
    {
      get { return _NumPalletTransferred; }
      set
      {
        if (value != _NumPalletTransferred)
        {
          _NumPalletTransferred = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }

    /// <summary>
    /// The type of starting condition
    /// </summary>
    private C200318_DefragStartTypeEnum _StartingCondition = C200318_DefragStartTypeEnum.MANUAL;
    public C200318_DefragStartTypeEnum StartingCondition
    {
      get { return _StartingCondition; }
      set
      {
        if (value != _StartingCondition)
        {
          _StartingCondition = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }
    /// <summary>
    /// The Id of the defrag status
    /// </summary>
    private int _DefragStatusId;

    

    public int DefragStatusId
    {
      get { return _DefragStatusId; }
      set
      {
        if (value != _DefragStatusId)
        {
          _DefragStatusId = value;
          IsChanged = true;
          NotifyPropertyChanged();
        }
      }
    }
    #endregion

    #region PUBLIC METHODS
    public void Update(int id, DateTime dateBorn, DateTime dateEnd, string fromLane, string toLane
      , C200318_DefragStatusEnum status, int palletAmount, int numPalletTransferred
      , C200318_DefragStartTypeEnum startingCondition, int defragStatusId)
    {
      Id = id;
      DateBorn = dateBorn;
      DateEnd = dateEnd;
      FromLane = fromLane;
      ToLane = toLane;
      Status = status;
      PalletAmount = palletAmount;
      NumPalletTransferred = numPalletTransferred;
      StartingCondition = startingCondition;
      DefragStatusId = defragStatusId;
    }

    public C200318_DefragStep Copy()
    {
      return (C200318_DefragStep)MemberwiseClone();
    }
    #endregion

  }
}
