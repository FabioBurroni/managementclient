﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Custom.C200318.OrderManager;

namespace Model.Custom.C200318
{
  public class C200318_ModelContext
  {
    #region Istanza

    private static readonly Lazy<C200318_ModelContext> Lazy = new Lazy<C200318_ModelContext>(() => new C200318_ModelContext());

    public static C200318_ModelContext Instance => Lazy.Value;

    private C200318_ModelContext()
    {
    }
    #endregion
    
    #region ListDestinasions 
    public ObservableCollectionFast<C200318_ListDestination> ListDestinasions { get; set; } = new ObservableCollectionFast<C200318_ListDestination>(); 
    #endregion
  }
}
