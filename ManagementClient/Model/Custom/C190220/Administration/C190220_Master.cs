﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Model.Custom.C190220.Administration
{
  public class C190220_Master:ModelBase, IUpdatable
  {
    public C190220_Master(string code, C190220_MasterCategory category)
    {
      _Code = code;
      _Category = category;
      OriginalCategory = category;
    }

    private string _Code;
    public string Code
    {
      get { return _Code; }
    }


    private C190220_MasterCategory _Category=C190220_MasterCategory.NORMAL;
    public C190220_MasterCategory Category
    {
      get { return _Category; }
      set
      {
        if (value != _Category)
        {
          _Category = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsChanged");
        }
      }
    }


    public C190220_MasterCategory OriginalCategory { get; set; }

    public bool IsChanged
    {
      get { return Category!=OriginalCategory; }
    }


    public void Update(object newElement)
    {
      Category = ((C190220_Master)newElement).Category;
      OriginalCategory = Category;
      NotifyPropertyChanged("IsChanged");
    }
  }


}
