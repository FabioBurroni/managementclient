﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C190220.OrderManager
{
  public enum C190220_ListCmpUnitOfMeasureEnum
  {
    P=1,  //...pallet
    W=2,  //...peso
    C=3,  //...scatole
  }
}
