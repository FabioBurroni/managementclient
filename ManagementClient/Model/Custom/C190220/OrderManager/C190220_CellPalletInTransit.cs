﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Model.Custom.C190220.OrderManager
{
  public class C190220_CellPalletInTransit : ModelBase
  {

   
    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }
  


    private C190220_Order _Order;
    public C190220_Order Order
    {
      get { return _Order; }
      set
      {
        if (value != _Order)
        {
          _Order = value;
          NotifyPropertyChanged();
        }
      }
    }

   
   public ObservableCollectionFast<C190220_PalletInTransit> PalletInTransitL { get; set; } = new ObservableCollectionFast<C190220_PalletInTransit>();





  }
}
