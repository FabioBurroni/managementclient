﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C190220.OrderManager
{
  public class C190220_StockByCell
  {
    public int NumPallet { get; set; } = 0;
    public string CellCode { get; set; }



    public override string ToString()
    {
      return $"CellCode:{CellCode}, NumPallet:{NumPallet}";
    }
  }
}
