﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C190220.OrderManager
{
  public enum C190220_OrderType
  {
    AUTO, //...ordine di prelievo da magazzino centrale
    MNL,  //...ordine di prelievo manuale
    SRV,  //...non usata in SACI		
  }
}
