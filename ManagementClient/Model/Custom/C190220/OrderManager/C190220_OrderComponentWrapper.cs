﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C190220.OrderManager
{
  public class C190220_OrderComponentWrapper:ModelBase
  {
    public string ArticleCode { get; set; } = string.Empty;
    public string ArticleDescr { get; set; } = string.Empty;

    public string LotCode { get; set; } = string.Empty;

    private int _QtyRequested;
    public int QtyRequested
    {
      get { return _QtyRequested; }
      set
      {
        if (value != _QtyRequested)
        {
          _QtyRequested = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C190220_ListCmpTypeEnum _CmpType = C190220_ListCmpTypeEnum.FIFO;
    public C190220_ListCmpTypeEnum CmpType
    {
      get { return _CmpType; }
      set
      {
        if (value != _CmpType)
        {
          _CmpType = value;
          NotifyPropertyChanged();
        }
      }
    }


    //private C190220_ListCmpUnitOfMeasureEnum _Unit = C190220_ListCmpUnitOfMeasureEnum.P;
    //public C190220_ListCmpUnitOfMeasureEnum Unit
    //{
    //  get { return _Unit; }
    //  set
    //  {
    //    if (value != _Unit)
    //    {
    //      _Unit = value;
    //      NotifyPropertyChanged();
    //    }
    //  }
    //}

    public override string ToString()
    {
      return $"ArticleCode:{ArticleCode}, ArticleDescr:{ArticleDescr}, LotCode:{LotCode}, QtyRequested:{QtyRequested}";
    }
  }
}
