﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Custom.C190220.ArticleManager;
namespace Model.Custom.C190220.OrderManager
{
  public class C190220_StockForOrder : ModelBase
  {

    private C190220_Article _Article;
    public C190220_Article Article
    {
      get { return _Article; }
      set
      {
        if (value != _Article)
        {
          _Article = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _LotCode;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }




    private int _NumPallet = 0;
    /// <summary>
    /// Numero di Pallet nei magazzini automatici
    /// </summary>
    public int NumPallet
    {
      get { return _NumPallet; }
      set
      {
        if (value != _NumPallet)
        {
          _NumPallet= value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
