﻿
namespace Model.Custom.C190220.OrderManager
{
  public class C190220_ArticleLotStockByCell
  {
    public string ArticleCode { get; set; }
    public string ArticleDescr { get; set; }
    public string LotCode { get; set; }
    public ObservableCollectionFast<C190220_StockByCell> StockByCellL { get; set; } = new ObservableCollectionFast<C190220_StockByCell>();
  }
}
