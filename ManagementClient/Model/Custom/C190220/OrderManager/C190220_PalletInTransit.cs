﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C190220.OrderManager
{
  public class C190220_PalletInTransit:C190220_Pallet,IUpdatable
  {

    public C190220_PalletInTransit(C190220_Pallet pallet)
    {
      this.Code = pallet.Code;
      this.DateBorn = pallet.DateBorn;
      this.IsService = pallet.IsService;
      this.CustomResult = pallet.CustomResult;
      this.IsCustomReject = pallet.IsCustomReject;
      this.IsCustomReject = pallet.IsCustomReject;
      this.ProductionDate = pallet.ProductionDate;
      this.ExpiryDate= pallet.ExpiryDate;
      this.IsReject = pallet.IsReject;
      this.RejectResult = pallet.RejectResult;
      this.ArticleCode = pallet.ArticleCode;
      this.ArticleDescr = pallet.ArticleDescr;
      this.LotCode = pallet.LotCode;
      this.State = pallet.State;
      this.Days_Spent_FromProduction = pallet.Days_Spent_FromProduction;
      this.Days_to_Expiry= pallet.Days_to_Expiry;
      this.FinalDestination = pallet.FinalDestination;
    }

    private string _PositionCode;
    public string PositionCode
    {
      get { return _PositionCode; }
      set
      {
        if (value != _PositionCode)
        {
          _PositionCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    public void Update(object newElement)
    {
      this.PositionCode = ((C190220_PalletInTransit)newElement).PositionCode;
    }
  }
}
