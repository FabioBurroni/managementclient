﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Extensions;


namespace Model.Custom.C190220.Filter
{
  public class C190220_ArticleForOrderFilter : ModelBase
  {

    public C190220_ArticleForOrderFilter()
    {
   
    }

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; } 
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000,5000,10000,30000};
    #endregion

    private string _ArticleCode = string.Empty;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _LotCode = string.Empty;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsAvailable;
    public bool IsAvailable
    {
      get { return _IsAvailable; }
      set
      {
        if (value != _IsAvailable)
        {
          _IsAvailable = value;
          NotifyPropertyChanged();
        }
      }
    }


    //20201001 Aggiunto Parametro Only per vedere giacenza solo magazzino/i che può raggiungere la detinazione
    private bool _OnlyForMyPositon;
    public bool OnlyForMyPositon
    {
      get { return _OnlyForMyPositon; }
      set
      {
        if (value != _OnlyForMyPositon)
        {
          _OnlyForMyPositon = value;
          NotifyPropertyChanged();
        }
      }
    }


    #region public methods
    public void Reset()
    {
      ArticleCode = string.Empty;
      LotCode = string.Empty;
      IsAvailable = false;
    }
    #endregion

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        IsAvailable.ToString(),
        ArticleCode==null?"":ArticleCode.Base64Encode(),
        LotCode==null?"":LotCode.Base64Encode(),
        OnlyForMyPositon.ToString()
      };
    }

  }
}
