﻿using System;
using System.Collections.Generic;
using Utilities.Extensions;


namespace Model.Custom.C190220.Report
{
  public class C190220_PalletRejectFilter : ModelBase
  {

    public C190220_PalletRejectFilter()
    {
      Reset();
    }

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; }
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000, 5000, 10000, 30000 };
    #endregion

    private string _LotCode = string.Empty;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode = string.Empty;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _PalletCode = string.Empty;
    public string PalletCode
    {
        get{ return _PalletCode; }
        set
        {
            if(value != _PalletCode)
            {
                    _PalletCode = value;
                    NotifyPropertyChanged();
            }
        }
    }

    private C190220_Position _Position = new C190220_Position();
    public C190220_Position Position
    {
      get { return _Position; }
      set
      {
        if (value != _Position)
        {
          _Position = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _DateRangeSelected;
    public bool DateRangeSelected
    {
      get { return _DateRangeSelected; }
      set
      {
        if (value != _DateRangeSelected)
        {
          _DateRangeSelected = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateBornMin;
    public DateTime DateBornMin
    {
      get { return _DateBornMin; }
      set
      {
        if (value != _DateBornMin)
        {
          _DateBornMin = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateBornMax;
    public DateTime DateBornMax
    {
      get { return _DateBornMax; }
      set
      {
        if (value != _DateBornMax)
        {
          _DateBornMax = value;
          NotifyPropertyChanged();
        }
      }
    }
    
    #region public methods
    public void Reset()
    {
      ArticleCode = string.Empty;
      LotCode = string.Empty;
      PalletCode = string.Empty;
      DateBornMin = DateTime.Now;
      DateBornMax = DateTime.Now;
      Position = new C190220_Position();
    }
    #endregion

    private DateTime _defaultDateTime = DateTime.MinValue;

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        LotCode==null?"":LotCode.Base64Encode(),
        ArticleCode==null?"":ArticleCode.Base64Encode(),
        PalletCode==null?"":PalletCode.Base64Encode(),
        DateRangeSelected ? DateBornMin.ConvertToDateTimeFormatString():_defaultDateTime.ConvertToDateTimeFormatString(),
        DateRangeSelected ? DateBornMax.ConvertToDateTimeFormatString():_defaultDateTime.ConvertToDateTimeFormatString(),
        Position==null?"":Position.Code.Base64Encode()
      };
    }

  }
}
