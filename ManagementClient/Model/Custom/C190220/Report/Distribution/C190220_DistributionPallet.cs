﻿
namespace Model.Custom.C190220.Report
{
  public class C190220_DistributionPallet : ModelBase
  {
    private string _Cell = string.Empty;
    public string Cell
    {
      get { return _Cell; }
      set
      {
        if (value != _Cell)
        {
          _Cell = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _NumPallet;
    public int NumPallet
    {
      get { return _NumPallet; }
      set
      {
        if (value != _NumPallet)
        {
          _NumPallet = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _TotalPallet;
    public int TotalPallet
    {
      get { return _TotalPallet; }
      set
      {
        if (value != _TotalPallet)
        {
          _TotalPallet = value;
          NotifyPropertyChanged();
        }
      }
    }


    public double Percent
    {
      get
      {

        var perc = TotalPallet != 0 ? ((double)NumPallet / (double)TotalPallet) * 100 : 0;
        return perc;
      }

    }


  }
}
