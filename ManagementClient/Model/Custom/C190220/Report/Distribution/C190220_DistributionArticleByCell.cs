﻿
namespace Model.Custom.C190220.Report
{
  public class C190220_DistributionArticleByCell : ModelBase
  {
    private string _Cell;
    public string Cell
    {
      get { return _Cell; }
      set
      {
        if (value != _Cell)
        {
          _Cell = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _LotCode;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _NumPallet;
    public int NumPallet
    {
      get { return _NumPallet; }
      set
      {
        if (value != _NumPallet)
        {
          _NumPallet = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _TotalPallet;
    public int TotalPallet
    {
      get { return _TotalPallet; }
      set
      {
        if (value != _TotalPallet)
        {
          _TotalPallet = value;
          NotifyPropertyChanged();
        }
      }
    }


    public double Percent
    {
      get
      {

        var perc = TotalPallet != 0 ? ((double)NumPallet / (double)TotalPallet) * 100 : 0;
        return perc;
      }

    }




  }
}
