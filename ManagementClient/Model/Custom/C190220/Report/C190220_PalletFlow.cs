﻿

namespace Model.Custom.C190220.Report
{
    public class C190220_PalletFlow
    {
        public ObservableCollectionFast<C190220_PalletIn> listIn = new ObservableCollectionFast<C190220_PalletIn>();
        public ObservableCollectionFast<C190220_PalletOut> listOut = new ObservableCollectionFast<C190220_PalletOut>();
        public ObservableCollectionFast<C190220_PalletReject> listReject = new ObservableCollectionFast<C190220_PalletReject>();
    }
}
