﻿using System;

namespace Model.Custom.C190220.Report
{
  public class C190220_PalletIn
  {
    public DateTime DateBorn { get; set; }
    public string PalletCode { get; set; }
    public string LotCode { get; set; }
    public int Qty { get; set; }
    public int LotLength { get; set; }
    public DateTime ExpiryDate { get; set; }
    public string ArticleCode { get; set; }
    public string Position { get; set; }
    public string CellCode { get; set; }
  }
}
