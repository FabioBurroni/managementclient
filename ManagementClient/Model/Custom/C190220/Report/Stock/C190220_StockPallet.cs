﻿using System;

namespace Model.Custom.C190220.Report
{
  public class C190220_StockPallet:ModelBase
  {

    private int _Id;
    public int Id
    {
      get { return _Id; }
      set
      {
        if (value != _Id)
        {
          _Id = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private DateTime _DateBorn;
    public DateTime DateBorn
    {
      get { return _DateBorn; }
      set
      {
        if (value != _DateBorn)
        {
          _DateBorn = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateExpiry;
    public DateTime DateExpiry
    {
      get { return _DateExpiry; }
      set
      {
        if (value != _DateExpiry)
        {
          _DateExpiry = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _MapperEntityCode;
    public string MapperEntityCode
    {
      get { return _MapperEntityCode; }
      set
      {
        if (value != _MapperEntityCode)
        {
          _MapperEntityCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _LotCode;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Qty;
    public int Qty
    {
      get { return _Qty; }
      set
      {
        if (value != _Qty)
        {
          _Qty = value;
          NotifyPropertyChanged();
        }
      }
    }
  }
}
