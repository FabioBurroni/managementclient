﻿using System;

namespace Model.Custom.C190220.Report.Stock
{
    public class C190220_SoonExpire : ModelBase
    {
        private string _Code;
        public string Code
        {
            get { return _Code; }
            set
            {
                if (value != _Code)
                {
                    _Code = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private DateTime _DateBorn;
        public DateTime DateBorn
        {
            get { return _DateBorn; }
            set
            {
                if (value != _DateBorn)
                {
                    _DateBorn = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private DateTime _DateExpiry;
        public DateTime DateExpiry
        {
            get { return _DateExpiry; }
            set
            {
                if (value != _DateExpiry)
                {
                    _DateExpiry = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _ArticleCode;
        public string ArticleCode
        {
            get { return _ArticleCode; }
            set
            {
                if (value != _ArticleCode)
                {
                    _ArticleCode = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _CellCode;
        public string CellCode
        {
            get { return _CellCode; }
            set
            {
                if (value != _CellCode)
                {
                    _CellCode = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private int _DaysLeft;
        public int DaysLeft
        {
            get { return _DaysLeft; }
            set
            {
                if (value != _DaysLeft)
                {
                    _DaysLeft = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private int _Qty;
        public int Qty
        {
            get { return _Qty; }
            set
            {
                if (value != _Qty)
                {
                    _Qty = value;
                    NotifyPropertyChanged();
                }
            }
        }
    }
}
