﻿using Model.Custom.C190220.OrderManager;
using System;
using System.Collections.Generic;
using Utilities.Extensions;

namespace Model.Custom.C190220.Report
{
  public class C190220_StockLotArticleFilter : ModelBase
  {


    public C190220_StockLotArticleFilter()
    {

    }

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; }
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000, 5000, 10000, 30000 };
    #endregion

    private string _LotCode;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

  

    private DateTime _DateStart = DateTime.MinValue;
    public DateTime DateStart
    {
      get { return _DateStart; }
      set
      {
        if (value != _DateStart)
        {
          _DateStart = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateEnd;
    public DateTime DateEnd
    {
      get { return _DateEnd; }
      set
      {
        if (value != _DateEnd)
        {
          _DateEnd = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _Available;
    public bool Available
    {
      get { return _Available; }
      set
      {
        if (value != _Available)
        {
          _Available = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _Reserved = false;
    public bool Reserved
    {
      get { return _Reserved; }
      set
      {
        if (value != _Reserved)
        {
          _Reserved = value;
          NotifyPropertyChanged();
        }
      }
    }

    #region public methods
    public void Reset()
    {
      LotCode = string.Empty;
      ArticleCode = string.Empty;
      ArticleDescr = string.Empty;
      Available = false;
      DateEnd = DateTime.MinValue;
      DateStart = DateTime.MinValue;
      Reserved = false;
    }


    #endregion

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        LotCode==null?"":LotCode.Base64Encode(),
        ArticleCode==null?"":ArticleCode.Base64Encode(),
        ArticleDescr==null?"":ArticleDescr.Base64Encode(),
        Available ?"true":"false",
        DateStart.ConvertToDateTimeFormatString()??"",
        DateEnd.ConvertToDateTimeFormatString()??"",
        Reserved ? "true" : "false",
      };
    }

  }
}
