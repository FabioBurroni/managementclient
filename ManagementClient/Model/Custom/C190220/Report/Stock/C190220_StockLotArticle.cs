﻿using System;
using System.Collections.Generic;

namespace Model.Custom.C190220.Report
{
  public class C190220_StockLotArticle : ModelBase
  {
    private string _LotCode;
    public string LotCode
    {
      get { return _LotCode; }
      set
      {
        if (value != _LotCode)
        {
          _LotCode = value;
          NotifyPropertyChanged();
        }
      }
    }
        
    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _MaxQty;
    public int MaxQty
    {
      get { return _MaxQty; }
      set
      {
        if (value != _MaxQty)
        {
          _MaxQty = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _MinQty;
    public int MinQty
    {
      get { return _MinQty; }
      set
      {
        if (value != _MinQty)
        {
          _MinQty = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _StockTotale;
    public int StockTotale
    {
      get { return _StockTotale; }
      set
      {
        if (value != _StockTotale)
        {
          _StockTotale = value;
          NotifyPropertyChanged();

          StockTotaleMinMax = new C200138_StockMinMax(StockTotale, MinQty, MaxQty);
        }
      }
    }

    private int _StockInEmergenziale;
    public int StockInEmergenziale
    {
      get { return _StockInEmergenziale; }
      set
      {
        if (value != _StockInEmergenziale)
        {
          _StockInEmergenziale = value;

          NotifyPropertyChanged();

          StockInEmergenzialeMinMax = new C200138_StockMinMax(StockInEmergenziale, MinQty, MaxQty);
        }
      }
    }

    private C200138_StockMinMax _StockInEmergenzialeMinMax;
    public C200138_StockMinMax StockInEmergenzialeMinMax
    {
      get { return _StockInEmergenzialeMinMax; }
      set
      {
        if (value != _StockInEmergenzialeMinMax)
        {
          _StockInEmergenzialeMinMax = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200138_StockMinMax _StockTotaleMinMax;
    public C200138_StockMinMax StockTotaleMinMax
    {
      get { return _StockTotaleMinMax; }
      set
      {
        if (value != _StockTotaleMinMax)
        {
          _StockTotaleMinMax = value;
          NotifyPropertyChanged();
        }
      }
    }





  }
  public class C200138_StockMinMax
    {

      public int Stock { get; set; }

      public int Min { get; set; }

      public int Max { get; set; }

      public C200138_StockMinMax(int giac, int min, int max)
      {
        Stock = giac;
        Min = min;
        Max = max;
      }
    }
}
