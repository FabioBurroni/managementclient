﻿
namespace Model.Custom.C190220.ArticleManager
{
  public class C190220_ArticleImport : C190220_Article
  { 
    private bool _Selected;
    public bool Selected
    {
      get { return _Selected; }
      set
      {
        if (value != _Selected)
        {
          _Selected = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _Valid;
    public bool Valid
    {
      get { return _Valid; }
      set
      {
        if (value != _Valid)
        {
          _Valid = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C190220_CustomResult _Result;
    public C190220_CustomResult Result
    {
      get { return _Result; }
      set
      {
        if (value != _Result)
        {
          _Result = value;
          NotifyPropertyChanged();
        }
      }
    }

    

  }
}
