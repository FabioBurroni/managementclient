﻿using LINQtoCSV;
namespace Model.Custom.C190220.ArticleManager
{
  public class C190220_LotArticle : ModelBase
  {

    private C190220_Lot _Lot;
    public C190220_Lot Lot
    {
      get { return _Lot; }
      set
      {
        if (value != _Lot)
        {
          _Lot = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C190220_Article _Article;
    public C190220_Article Article
    {
      get { return _Article; }
      set
      {
        if (value != _Article)
        {
          _Article = value;
          NotifyPropertyChanged();
        }
      }
    }





  }
}
