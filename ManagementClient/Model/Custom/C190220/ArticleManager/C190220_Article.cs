﻿
using LINQtoCSV;

namespace Model.Custom.C190220.ArticleManager
{
  public class C190220_Article : ModelBase
  {

    private string _Code;
    [CsvColumn(Name = "Code", FieldIndex = 1)]
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Descr;

    [CsvColumn(Name = "Description", FieldIndex = 2)]
    public string Descr
    {
      get { return _Descr; }
      set
      {
        if (value != _Descr)
        {
          _Descr = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _AW_MinQuantity;
    public int AW_MinQuantity
    {
      get { return _AW_MinQuantity; }
      set
      {
        if (value != _AW_MinQuantity)
        {
          _AW_MinQuantity = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _AW_MaxQuantity;
    public int AW_MaxQuantity
    {
      get { return _AW_MaxQuantity; }
      set
      {
        if (value != _AW_MaxQuantity)
        {
          _AW_MaxQuantity = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _StockWindow;
    public int StockWindow
    {
      get { return _StockWindow; }
      set
      {
        if (value != _StockWindow)
        {
          _StockWindow = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _FifoWindow;
    public int FifoWindow
    {
      get { return _FifoWindow; }
      set
      {
        if (value != _FifoWindow)
        {
          _FifoWindow = value;
          NotifyPropertyChanged();
        }
      }
    }
       
    private C190220_TurnoverEnum _Turnover = C190220_TurnoverEnum.A;
    public C190220_TurnoverEnum Turnover
    {
      get { return _Turnover; }
      set
      {
        if (value != _Turnover)
        {
          _Turnover = value;
          NotifyPropertyChanged();
        }
      }
    }
      
    private string _Unit;

    [CsvColumn(Name = "Unit", FieldIndex = 3)]
    public string Unit
    {
      get { return _Unit; }
      set
      {
        if (value != _Unit)
        {
          _Unit = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsDeleted;
    public bool IsDeleted
    {
      get { return _IsDeleted; }
      set
      {
        if (value != _IsDeleted)
        {
          _IsDeleted = value;
          NotifyPropertyChanged();
        }
      }
    }
  }
}
