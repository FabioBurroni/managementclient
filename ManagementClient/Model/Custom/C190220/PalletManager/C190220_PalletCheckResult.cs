﻿using System;
namespace Model.Custom.C190220.PalletManager
{
  public class C190220_PalletCheckResult : ModelBase
  {
    private C190220_CustomResult _Result;

    public C190220_CustomResult Result
    {
      get { return _Result; }
      set 
      {
        if (value != _Result)
        {
          _Result = value;
          NotifyPropertyChanged();
        }
      }
    }



    private C190220_PalletInsert _Pallet ;
    public C190220_PalletInsert Pallet
    {
      get { return _Pallet; }
      set
      {
        if (value != _Pallet)
        {
          _Pallet = value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
