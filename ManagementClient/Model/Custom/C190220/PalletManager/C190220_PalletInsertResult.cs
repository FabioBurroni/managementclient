﻿using System;
namespace Model.Custom.C190220.PalletManager
{
  public class C190220_PalletInsertResult : ModelBase
  {
    private C190220_CustomResult _Result;

    public C190220_CustomResult Result
    {
      get { return _Result; }
      set 
      {
        if (value != _Result)
        {
          _Result = value;
          NotifyPropertyChanged();
        }
      }
    }



    private string _Code = string.Empty;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
