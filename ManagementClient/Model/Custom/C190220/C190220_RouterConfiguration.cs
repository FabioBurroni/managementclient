﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C190220
{
  public enum C190220_RouterConfiguration
  {
    DEFAULT,
    BACKUP1,
    BACKUP2,
    BACKUP3,
    BACKUP4,
    BACKUP5,
    BACKUP6,
    BACKUP7,
    BACKUP8,
    BACKUP9,
    BACKUP10,
  }
}
