﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C190220
{
  public enum C190220_ListColor
  {
    AZURE = 0,
    BLUE = 1,
    BROWN = 2,
    GRAY = 3,
    GREEN = 4,
    ORANGE = 5,
    PINK = 6,
    RED = 7,
    YELLOW = 8,
    WHITE = 9,
  }
}
