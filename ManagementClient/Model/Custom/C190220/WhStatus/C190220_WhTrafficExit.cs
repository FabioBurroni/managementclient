﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C190220.WhStatus
{
  public class C190220_WhTrafficExit : ModelBase
  {
  

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _PalletCount;
    public int PalletCount
    {
      get { return _PalletCount; }
      set
      {
        if (value != _PalletCount)
        {
          _PalletCount = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _PalletCountTotal;
    public int PalletCountTotal
    {
      get { return _PalletCountTotal; }
      set
      {
        if (value != _PalletCountTotal)
        {
          _PalletCountTotal = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _Percentage;
    public int Percentage
    {
      get { return _Percentage; }
      set
      {
        if (value != _Percentage)
        {
          _Percentage = value;
          NotifyPropertyChanged();
        }
      }
    }

    public void Update(C190220_WhTrafficExit newWhs)
    {
    
      this.Code = newWhs.Code;
      this.PalletCount = newWhs.PalletCount;

      this.PalletCountTotal = newWhs.PalletCountTotal;
    
      if (PalletCountTotal > 0)
      {
        Percentage = (int)Math.Floor(((decimal)PalletCount / (decimal)PalletCountTotal) *100);
      }
      else
      {
        Percentage = 0;
      }
    }
  }
}
