﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Custom.C190220.OrderManager;

namespace Model.Custom.C190220
{
  public class C190220_ModelContext
  {
    #region Istanza

    private static readonly Lazy<C190220_ModelContext> Lazy = new Lazy<C190220_ModelContext>(() => new C190220_ModelContext());

    public static C190220_ModelContext Instance => Lazy.Value;

    private C190220_ModelContext()
    {
    }
    #endregion
    
    #region ListDestinasions 
    public ObservableCollectionFast<C190220_ListDestination> ListDestinasions { get; set; } = new ObservableCollectionFast<C190220_ListDestination>(); 
    #endregion
  }
}
