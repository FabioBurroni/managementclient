﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C190220
{
  public class C190220_ShippingLaneOrder : ModelBase
  {
    private string _OrderCode;
    public string OrderCode
    {
      get { return _OrderCode; }
      set
      {
        if (value != _OrderCode)
        {
          _OrderCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C190220_ListColor _ListColor;
    public C190220_ListColor ListColor
    {
      get { return _ListColor; }
      set
      {
        if (value != _ListColor)
        {
          _ListColor = value;
          NotifyPropertyChanged();
        }
      }
    }



   

  }
}
