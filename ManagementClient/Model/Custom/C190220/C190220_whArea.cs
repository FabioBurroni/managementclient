﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C190220
{
  public class C190220_WhArea:ModelBase
  {

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("Descr");
        }
      }
    }


    private C190220_WhAreaType _WhAreaType;
    public C190220_WhAreaType WhAreaType
    {
      get { return _WhAreaType; }
      set
      {
        if (value != _WhAreaType)
        {
          _WhAreaType = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("Descr");
        }
      }
    }


    private string _Descr;
    public string Descr
    {
      get { return $"{Code.ToUpper()} - {WhAreaType}"; }
    }
  }

  public enum C190220_WhAreaType
  {
    AUTOMATICO,
    MANUALE,
    ESTERNO,

  }
}
