﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C190220
{
  public enum C190220_CustomResult
  {
    //Generic 0
    UNDEFINED = 0,
    OK = 1,
    DATABASE_NOT_CONNECTED = 2,
    STORED_PROCEDURE_EXCEPTION = 3,
    QUERY_EXCEPTION = 4,
    ERROR_READING_DATA_FROM_PLC = 5,
    ERROR_DATA_FROM_PLC_NOT_VALID = 6,
    UNTRANSLATED_ERROR = 7,           //...errore interno ResultEnum non tradotto in CustomResult
    CONFIGURATION_ERROR = 8,          //...errore di configurazione
    PARAMETERS_ERROR = 9,

    ////BarCode 10  
    //BAR_CODE_NOT_VALID = 11,  //codice a barre non valido
    //BAR_CODE_NOT_READ = 12,  //codice a barre non letto
    //BAR_CODE_READER_DISABLED = 13,    //lettore di codici a barre disabilitato

    //QRCODE 11  
    QRCODE_NOT_READ               = 11,  
    QRCODE_WRONG_FORMAT           = 12,  
    QRCODE_PALLETCODE_NOT_VALID   = 13,
    QRCODE_ARTICLECODE_NOT_VALID  = 14,
    QRCODE_LOTCODE_NOT_VALID      = 15,
    QRCODE_QTY_NOT_VALID          = 16,
    QRCODE_EXPIRYDATE_NOT_VALID   = 17,
    QRCODE_LOTLENGTH_NOT_VALID    = 18,  

    //Lotto 20
    LOT_NOT_VALID = 20,

    //PalletType30
    PALLETTYPE_NOT_VALID = 30,

    //Article 40
    ARTICLE_NOT_VALID = 40,
    ARTICLE_NOT_PRESENT = 41,
    ARTICLETYPE_NOT_PRESENT = 42,
    ARTICLE_ERROR_INSERT = 43,
    ARTICLE_ERROR_UPDATE = 44,
    ARTICLE_TYPECUSTOM_NOT_PRESENT = 45,
    ARTICLE_UNIT_NOT_PRESENT = 46,
    ARTICLE_DESCRIPTION_NOT_VALID = 49,     //20211014

    //Pallet 50
    PALLET_NOT_VALID = 50, //codice pallet non valido
    PALLET_NOT_FOUND = 51, //codice pallet non trovato
    PALLET_INSERT_ERROR = 52,//errore inserimento pallet nel db
    PALLET_ALREADY_PRESENT_IN_WH = 53, //il pallet è presente nel magazzino automatico
    PALLET_ALREADY_PRESENT_IN_HANDLING = 54, //il pallet è presente nel magazzino manuale
    PALLET_DESTINED_TO_REJECT = 59, //il pallet è destinato al reject
    PALLET_ALREADY_MAPPED_IN_POSITION = 60, //c'è un pallet già mappato nella posizione
    PALLET_SERVICE_ERROR_CREATING = 61, //errore creazione pallet di servizio
    PALLET_WITHOUT_DESTINATION = 62, //pallet senza destinazione
    PALLET_DESTINED_TO_EXIT = 63, //il pallet è destinato all'uscita
    PALLET_TRACKING_ERROR = 65,
    PALLET_ALREADY_PRESENT_IN_DB = 66, //il pallet è presente nel database
    PALLET_ALREADY_RESERVED = 67, //...il pallet ha già una finaldest
    PALLET_UPDATE_ERROR = 68, //...errore aggiornamento pallet


    //CustomPallet 70
    PALLETCUSTOM_INSERT_ERROR = 70,//errore inserimento pallet custom nel db
    PALLETCUSTOM_NOT_FOUND = 71,//pallet custom non trovato
    PALLETCUSTOM_UPDATE_ERROR = 72,// Error Updating Custom Pallet Info

    //Product
    PRODUCT_INSERT_ERROR = 80,
    PRODUCT_UPDATE_ERROR = 81,


    //Mapping 100
    MAPPING_INSERT_ERROR = 100,
    POSITION_NOT_FOUND = 101,
    MAPPING_PALLET_ERROR = 102,


    //ShapeControl
    SHAPECONTROL_OVERFLOW_WIDTH_RIGHT = 130,
    SHAPECONTROL_OVERFLOW_WIDTH_LEFT = 131,
    SHAPECONTROL_OVERFLOW_WIDTH_RIGHTLEFT = 132,
    SHAPECONTROL_OVERFLOW_DEPTH_REAR = 133,
    SHAPECONTROL_OVERFLOW_DEPTH_FRONT = 134,
    SHAPECONTROL_OVERFLOW_DEPTH_REARFRONT = 135,
    SHAPECONTROL_OVERFLOW_HEIGHT = 136,
    SHAPECONTROL_OVERFLOW_WEIGHT = 137,
    SHAPECONTROL_NO_AVAILABLE_AREA = 138,
    SHAPECONTROL_NO_REACHABLE_AREA = 139,
    SHAPECONTROL_NO_AVAILABLE_CELL = 140,
    SHAPECONTROL_PALLET_NOT_VALID = 141,
    SHAPECONTROL_ARTICLE_NOT_VALID = 142,   //...articolo non valido
    SHAPECONTROL_ARTICLE_NOT_ACCEPTED = 143, //...articolo non accettato in magazzzino automatico
    SHAPECONTROL_LABEL_NOT_VALID = 144, //...articolo non accettato in magazzzino automatico
    SHAPECONTROL_LABEL_NOT_READ = 145, //...articolo non accettato in magazzzino automatico
    SHAPECONTROL_PALLETTYPE_NOT_VALID = 146,
    SHAPECONTROL_PALLET_IN_WH = 147,
    SHAPECONTROL_GENERIC_ERROR = 148,
    SHAPECONTROL_ERROR_READING_DATA_FROM_PLC = 149,
    SHAPECONTROL_BAR_CODE_NOT_READ = 150,
    SHAPECONTROL_BAR_CODE_NOT_VALID = 151,
    //20210416
    SHAPECONTROL_ROUTING_SWITCHING = 152, //...routing is switching
    //20210416
    SHAPECONTROL_ROUTE_NOT_AVAILABLE = 153, //...route not available due to backup mopdality

    SHAPECONTROL_SLATS_NOK = 154,           //...pallet slats are nok


    RESERVED_CELL_ERROR = 170,

    FINALDEST_ERROR = 180,

    JOB_ERROR_CREATE = 190,
    JOB_CANNOT_CREATE = 191,

    ORDER_NUM_ROWS_NOT_VALID = 200,
    ORDER_ERROR_UPDATE = 201,
    ORDER_NOT_VALID = 202,
    ORDERTYPE_NOT_VALID = 203,
    ORDERSTATE_NOT_VALID = 204,
    ORDER_CODE_NOT_VALID = 205,
    ORDER_ALREADY_PRESENT = 206,
    ORDER_ERROR_INSERT = 207,
    ORDER_QTY_NOT_VALID = 208,
    ORDERCMP_ERROR_INSERT = 209,
    ORDER_NOT_EDITABLE = 210,
    ORDER_DESTINATION_NOT_VALID = 211,
    ORDER_ERROR_DESTINATION_UPDATE = 212,
    //20201204
    ORDER_COMPONENT_ERROR_UPDATE = 213,

    //...printer
    PRINTER_ERROR_UPDATE = 400,
    PRINTER_ERROR_INSERT = 401,
    PRINTER_ERROR_DELETE = 402,
    PRINTER_NOT_VALID = 403,
    PRINTER_ERROR = 404,

    //...label
    LABEL_ERROR_UPDATE = 420,
    LABEL_ERROR_INSERT = 421,
    LABEL_ERROR_DELETE = 422,
    LABEL_NOT_VALID = 423,

    //...label field
    LABEL_FIELD_ERROR_UPDATE = 430,
    LABEL_FIELD_ERROR_INSERT = 431,
    LABEL_FIELD_ERROR_DELETE = 432,
    LABEL_FIELD_NOT_VALID = 433,

    //...communications
    COMMUNICATION_TIMEOUT = 500,
    COMMUNICATION_RESET_RUNNING = 501,



    ROUTER_IS_SWITCHING = 600,
    ROUTER_SWITCH_NOT_ALLOWED = 601,

    CHECKIN_PALLET_NOT_PRESENT = 700,

    //BCR CHECK    
    BCRCHECK_PALLET_IN_WH = 801,
    BCRCHECK_GENERIC_ERROR = 802,
    BCRCHECK_ERROR_READING_DATA_FROM_PLC = 803,
    BCRCHECK_BAR_CODE_NOT_READ = 804,
    BCRCHECK_BAR_CODE_NOT_VALID = 805,
    BCRCHECK_BAR_CODE_NOT_MATCH_PALLET_ID = 806,

    //...Cell Available for manual sat fetch field
    CELL_MANUAL_SAT_ERROR_INSERT = 900,
    CELL_MANUAL_SAT_ERROR_INSERT_ALREADY_PRESENT = 901, // already present in table
    CELL_MANUAL_SAT_ERROR_INSERT_NOT_FOUND = 902,       // not found in position list

    CELL_MANUAL_SAT_ERROR_DELETE = 905,
    CELL_MANUAL_SAT_ERROR_DELETE_NOT_PRESENT = 906,     // not present in table
    CELL_MANUAL_SAT_ERROR_DELETE_NOT_FOUND = 907,       // not found in position list

    CELL_MANUAL_SAT_NOT_VALID = 909,

    WORKMODALITY_SWITCH_MASTER_NOT_MANUAL = 950,              //master not in automatic mode
    WORKMODALITY_SWITCH_MODE_EQUAL_CURRENT = 951,         //Requested mode is the actual
    WORKMODALITY_SWITCH_MODE_ALREADY_REQUESTED = 952,         //Requested mode is already in charge
    WORKMODALITY_SWITCH_MODE_SWITCHING = 953,         //Requested mode is ACCEPTED
    WORKMODALITY_SWITCH_MODE_DONE = 954,         //Requested mode is COMPLETED

  }
}
