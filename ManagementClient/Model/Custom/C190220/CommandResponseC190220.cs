﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;
using XmlCommunicationManager.Common.Xml;

namespace Model.Custom.C190220
{
	public class CommandResponseC190220 : CommandResponse
	{
		#region Constructor

		public CommandResponseC190220(XmlCommand xmlCmd)
			: this(xmlCmd.commandMethodName, xmlCmd.commandMethodParameters)
		{

		}

		public CommandResponseC190220(string commandName, IEnumerable<string> commandParameters)
		{
			if (commandName == null)
				throw new ArgumentNullException(nameof(commandName));

			//tolgo subito l'eventuale preambolo
			commandName = commandName.TrimPreamble();

			var parameters = commandParameters.EmptyIfNull().ToArray();

			#region Plugin

			//se il nome è 'Plugin_Command' allora il nome del metodo è il primo parametro
			if (commandName == "Plugin_Command")
				commandName = parameters.First();

			#endregion

			CommandName = commandName;
			CommandParameters = parameters.ToArray();
		}

		#endregion
	}
}