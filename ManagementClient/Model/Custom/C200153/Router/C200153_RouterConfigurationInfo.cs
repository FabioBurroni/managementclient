﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153.Router
{
  public class C200153_RouterConfigurationInfo : ModelBase
  {


    private string _Zone;
    public string Zone
    {
      get { return _Zone; }
      set
      {
        if (value != _Zone)
        {
          _Zone = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_RouterConfigurationEnum _CurrentConfiguration;
    public C200153_RouterConfigurationEnum CurrentConfiguration
    {
      get { return _CurrentConfiguration; }
      set
      {
        if (value != _CurrentConfiguration)
        {
          _CurrentConfiguration = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _CurrentConfigurationLastUpdate = DateTime.Now;
    public DateTime CurrentConfigurationLastUpdate
    {
      get { return _CurrentConfigurationLastUpdate; }
      set
      {
        if (value != _CurrentConfigurationLastUpdate)
        {
          _CurrentConfigurationLastUpdate = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_RouterConfigurationEnum _RequestedConfiguration;
    public C200153_RouterConfigurationEnum RequestedConfiguration
    {
      get { return _RequestedConfiguration; }
      set
      {
        if (value != _RequestedConfiguration)
        {
          _RequestedConfiguration = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _RequestedConfigurationLastUpdate = DateTime.Now;
    public DateTime RequestedConfigurationLastUpdate
    {
      get { return _RequestedConfigurationLastUpdate; }
      set
      {
        if (value != _RequestedConfigurationLastUpdate)
        {
          _RequestedConfigurationLastUpdate = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsSwitching;
    public bool IsSwitching
    {
      get { return _IsSwitching; }
      set
      {
        if (value != _IsSwitching)
        {
          _IsSwitching = value;
          NotifyPropertyChanged();
        }
      }
    }

    private ObservableCollectionFast<string> _AllowedSwitch= new ObservableCollectionFast<string>();

    public ObservableCollectionFast<string> AllowedSwitch
    {
      get { return _AllowedSwitch; }
      set {
        if (value != _AllowedSwitch)
        {
          _AllowedSwitch = value;
          NotifyPropertyChanged();
        }
      }
    }



    public void Update(C200153_RouterConfigurationInfo newRouting)
    {
      this.CurrentConfiguration = newRouting.CurrentConfiguration;
      this.CurrentConfigurationLastUpdate = newRouting.CurrentConfigurationLastUpdate;
      this.RequestedConfiguration = newRouting.RequestedConfiguration;
      this.RequestedConfigurationLastUpdate = newRouting.RequestedConfigurationLastUpdate;
      this.IsSwitching = newRouting.IsSwitching;
      this.AllowedSwitch = newRouting.AllowedSwitch;
    }

    public void Reset()
    {
      this.CurrentConfiguration = C200153_RouterConfigurationEnum.DEFAULT;
      this.CurrentConfigurationLastUpdate = DateTime.Now;
      this.RequestedConfiguration = C200153_RouterConfigurationEnum.DEFAULT;
      this.RequestedConfigurationLastUpdate = DateTime.Now;
      this.IsSwitching = false;
      this.AllowedSwitch = new ObservableCollectionFast<string>();


    }
  }
}
