﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Common;

namespace Model.Custom.C200153
{
  public class C200153_PalletTraffic: ModelBase
  {

    private string _Code = string.Empty;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Position = string.Empty;
    public string Position
    {
      get { return _Position; }
      set
      {
        if (value != _Position)
        {
          _Position = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _FinalDestination = string.Empty;
    public string FinalDestination
    {
      get { return _FinalDestination; }
      set
      {
        if (value != _FinalDestination)
        {
          _FinalDestination = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

   

    public void Update(C200153_PalletTraffic newPallet)
    {
      this.Code = newPallet.Code;
      this.Position = newPallet.Position;
      this.FinalDestination = newPallet.FinalDestination;
      this.ArticleCode = newPallet.ArticleCode;
      this.ArticleDescr = newPallet.ArticleDescr;
    }

    public void Reset()
    {
      this.Code = string.Empty;
      this.Position = string.Empty;
      this.FinalDestination = string.Empty;
      this.ArticleCode = string.Empty;
      this.ArticleDescr = string.Empty;
    }
  }
}
