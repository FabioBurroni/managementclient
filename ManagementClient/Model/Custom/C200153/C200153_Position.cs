﻿
namespace Model.Custom.C200153
{
  public class C200153_Position : ModelBase
  {
    private string _Code = string.Empty;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Description=string.Empty;
    public string Description
    {
      get { return _Description; }
      set
      {
        if (value != _Description)
        {
          _Description = value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
