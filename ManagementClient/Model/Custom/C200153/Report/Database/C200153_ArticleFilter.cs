﻿using System;
using System.Collections.Generic;
using Utilities.Extensions;


namespace Model.Custom.C200153.Report
{
  public class C200153_ArticleFilter : ModelBase
  {

    public C200153_ArticleFilter()
    {
     
    }

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; } 
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000 };
    #endregion


    private string _ArticleCode = string.Empty;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleDescr = string.Empty;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }


    #region public methods
    public void Reset()
    {
      ArticleCode = string.Empty;
      ArticleDescr= string.Empty;
    }
    #endregion

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        ArticleCode==null?"":ArticleCode.Base64Encode(),
        ArticleDescr==null?"":ArticleDescr.Base64Encode(),
      };
    }

  }
}
