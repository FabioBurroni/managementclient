﻿using System;

namespace Model.Custom.C200153.Report
{
  public class C200153_PalletAll : ModelBase
  {

    private string _PalletCode = string.Empty;
    public string PalletCode
    {
      get { return _PalletCode; }
      set
      {
        if (value != _PalletCode)
        {
          _PalletCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_PalletType _PalletTypeId;
    public C200153_PalletType PalletTypeId
    {
      get { return _PalletTypeId; }
      set
      {
        if (value != _PalletTypeId)
        {
          _PalletTypeId = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateBorn = DateTime.MinValue;
    public DateTime DateBorn
    {
      get { return _DateBorn; }
      set
      {
        if (value != _DateBorn)
        {
          _DateBorn = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _BatchCode;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _BatchLength;
    public int BatchLength
    {
      get { return _BatchLength; }
      set
      {
        if (value != _BatchLength)
        {
          _BatchLength = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _ProductionDate = DateTime.MinValue;
    public DateTime ProductionDate
    {
      get { return _ProductionDate; }
      set
      {
        if (value != _ProductionDate)
        {
          _ProductionDate = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _PositionCode;
    public string PositionCode
    {
      get { return _PositionCode; }
      set
      {
        if (value != _PositionCode)
        {
          _PositionCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _ExpiryDate = DateTime.MinValue;
    public DateTime ExpiryDate
    {
      get { return _ExpiryDate; }
      set
      {
        if (value != _ExpiryDate)
        {
          _ExpiryDate = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsLabeled;
    public bool IsLabeled
    {
      get { return _IsLabeled; }
      set
      {
        if (value != _IsLabeled)
        {
          _IsLabeled = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsWrapped;
    public bool IsWrapped
    {
      get { return _IsWrapped; }
      set
      {
        if (value != _IsWrapped)
        {
          _IsWrapped = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _DepositorCode;
    public string DepositorCode
    {
      get { return _DepositorCode; }
      set
      {
        if (value != _DepositorCode)
        {
          _DepositorCode = value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
