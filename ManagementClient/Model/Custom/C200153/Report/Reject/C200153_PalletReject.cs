﻿using System;

namespace Model.Custom.C200153.Report
{
  public class C200153_PalletReject
  {
    public DateTime DateBorn { get; set; }
    public string PalletCode { get; set; }
    public string ArticleCode { get; set; }
    public string BatchCode { get; set; }
    public int BatchLength { get; set; }
    public string DepositorCode { get; set; }
    public string Position { get; set; }
    public string Reason { get; set; }
    public string CellCode { get; set; }
    public bool IsLabeled { get; set; }
    public bool IsWrapped { get; set; }
    public DateTime ProductionDate { get; set; }
    public DateTime ExpiryDate { get; set; }
  }
}
