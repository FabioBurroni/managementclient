﻿using System;
using System.Collections.Generic;
using Utilities.Extensions;


namespace Model.Custom.C200153.Report
{
  public class C200153_PalletRejectFilter : ModelBase
  {

    public C200153_PalletRejectFilter()
    {
      Reset();
    }

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; }
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000, 5000, 10000, 30000 };
    #endregion

    private string _ArticleCode = string.Empty;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _BatchCode = string.Empty;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _DepositorCode = string.Empty;
    public string DepositorCode
    {
      get { return _DepositorCode; }
      set
      {
        if (value != _DepositorCode)
        {
          _DepositorCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _Position = string.Empty;
    public string Position
    {
      get { return _Position; }
      set
      {
        if (value != _Position)
        {
          _Position = value;
          NotifyPropertyChanged();
        }
      }
    }



    private bool _DateRangeSelected;
    public bool DateRangeSelected
    {
      get { return _DateRangeSelected; }
      set
      {
        if (value != _DateRangeSelected)
        {
          _DateRangeSelected = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateBornMin;
    public DateTime DateBornMin
    {
      get { return _DateBornMin; }
      set
      {
        if (value != _DateBornMin)
        {
          _DateBornMin = value;
          NotifyPropertyChanged();
        }
      }
    }



    private DateTime _DateBornMax;
    public DateTime DateBornMax
    {
      get { return _DateBornMax; }
      set
      {
        if (value != _DateBornMax)
        {
          _DateBornMax = value;
          NotifyPropertyChanged();
        }
      }
    }
    
    #region public methods
    public void Reset()
    {
      ArticleCode = string.Empty;
      BatchCode = string.Empty;
      DepositorCode = string.Empty;
      DateBornMin = DateTime.Now;
      DateBornMax = DateTime.Now;
      Position = string.Empty;
    }
    #endregion

    private DateTime _defaultDateTime = DateTime.MinValue;

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        BatchCode==null?"":BatchCode.Base64Encode(),
        ArticleCode==null?"":ArticleCode.Base64Encode(),
        DepositorCode==null?"":DepositorCode.Base64Encode(),
        DateRangeSelected ? DateBornMin.ConvertToDateTimeFormatString():_defaultDateTime.ConvertToDateTimeFormatString(),
        DateRangeSelected ? DateBornMax.ConvertToDateTimeFormatString():_defaultDateTime.ConvertToDateTimeFormatString(),
        Position==null?"":Position.Base64Encode()
      };
    }

  }
}
