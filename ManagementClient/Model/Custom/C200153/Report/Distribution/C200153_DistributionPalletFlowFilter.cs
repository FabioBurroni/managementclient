﻿using System;
using System.Collections.Generic;
using Utilities.Extensions;


namespace Model.Custom.C200153.Report
{
  public class C200153_DistributionPalletFlowFilter : ModelBase
  {

    public C200153_DistributionPalletFlowFilter()
    {
      Reset();
    }


    private string _BatchCode = string.Empty;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode = string.Empty;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_Position _InputPosition = new C200153_Position();
    public C200153_Position InputPosition
    {
      get { return _InputPosition; }
      set
      {
        if (value != _InputPosition)
        {
          _InputPosition = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_Position _OutputPosition = new C200153_Position();
    public C200153_Position OutputPosition
    {
      get { return _OutputPosition; }
      set
      {
        if (value != _OutputPosition)
        {
          _OutputPosition = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_Position _RejectPosition = new C200153_Position();
    public C200153_Position RejectPosition
    {
      get { return _RejectPosition; }
      set
      {
        if (value != _RejectPosition)
        {
          _RejectPosition = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _DateRangeSelected;
    public bool DateRangeSelected
    {
      get { return _DateRangeSelected; }
      set
      {
        if (value != _DateRangeSelected)
        {
          _DateRangeSelected = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateBornMin;
    public DateTime DateBornMin
    {
      get { return _DateBornMin; }
      set
      {
        if (value != _DateBornMin)
        {
          _DateBornMin = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateBornMax;
    public DateTime DateBornMax
    {
      get { return _DateBornMax; }
      set
      {
        if (value != _DateBornMax)
        {
          _DateBornMax = value;
          NotifyPropertyChanged();
        }
      }
    }
    
    #region public methods
    public void Reset()
    {
      BatchCode = string.Empty;
      ArticleCode = string.Empty;
      DateBornMin = DateTime.Now;
      DateBornMax = DateTime.Now;
      InputPosition = new C200153_Position();
      OutputPosition = new C200153_Position();
      RejectPosition = new C200153_Position();
    }
    #endregion

    private DateTime _defaultDateTime = DateTime.MinValue;

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        BatchCode==null?"":BatchCode.Base64Encode(),
        ArticleCode==null?"":ArticleCode.Base64Encode(),
        DateRangeSelected ? DateBornMin.ConvertToDateTimeFormatString():_defaultDateTime.ConvertToDateTimeFormatString(),
        DateRangeSelected ? DateBornMax.ConvertToDateTimeFormatString():_defaultDateTime.ConvertToDateTimeFormatString(),
        InputPosition==null?"":InputPosition.Code,
        OutputPosition==null?"":OutputPosition.Code,
        RejectPosition==null?"":RejectPosition.Code,
      };
    }

  }
}
