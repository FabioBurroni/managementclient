﻿using System;
using System.Collections.Generic;
using Utilities.Extensions;
namespace Model.Custom.C200153.Report
{
 
  public class C200153_Distribution_Storic_Filter : ModelBase
  {
  
    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; }
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000, 5000, 10000, 30000 };
    #endregion



    private string _PalletCode;
    public string PalletCode
    {
      get { return _PalletCode; }
      set
      {
        if (value != _PalletCode)
        {
          _PalletCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    public List<string> AreaCodeList { get; set; } = new List<string>() { "AWH1", "AWH2" };


    private string _AreaCode;
    public string AreaCode
    {
      get { return _AreaCode; }
      set
      {
        if (value != _AreaCode)
        {
          _AreaCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _AreaCodeSelected;
    public bool AreaCodeSelected
    {
      get { return _AreaCodeSelected; }
      set
      {
        if (value != _AreaCodeSelected)
        {
          _AreaCodeSelected = value;
          NotifyPropertyChanged();
        }
      }
    }
   



    private int _Floor;
    public int Floor
    {
      get { return _Floor; }
      set
      {
        if (value != _Floor)
        {
          _Floor = value;
          NotifyPropertyChanged();
        }
      }
    }



    private bool _FloorSelected;
    public bool FloorSelected
    {
      get { return _FloorSelected; }
      set
      {
        if (value != _FloorSelected)
        {
          _FloorSelected = value;
          NotifyPropertyChanged();
        }
      }
    }

    public List<int> FloorList { get; set; } = new List<int> { 1, 2, 3, 4, 5, 6 };

    private DateTime _StartDate = DateTime.Now;
    public DateTime StartDate
    {
      get { return _StartDate; }
      set
      {
        if (value != _StartDate)
        {
          _StartDate = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateBornMin=DateTime.Now;
    public DateTime DateBornMin
    {
      get { return _DateBornMin; }
      set
      {
        if (value != _DateBornMin)
        {
          _DateBornMin = value;
          NotifyPropertyChanged();
        }
      }
    }



    private DateTime _DateBornMax = DateTime.Now;
    public DateTime DateBornMax
    {
      get { return _DateBornMax; }
      set
      {
        if (value != _DateBornMax)
        {
          _DateBornMax = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _DateRangeSelected;
    public bool DateRangeSelected
    {
      get { return _DateRangeSelected; }
      set
      {
        if (value != _DateRangeSelected)
        {
          _DateRangeSelected = value;
          NotifyPropertyChanged();
        }
      }
    }
    #region public methods
    public void Reset()
    {
      DateBornMin = DateTime.Now;
      DateBornMax = DateTime.Now;
      StartDate = DateTime.MinValue;
      DateRangeSelected = false;
      Floor = 0;
      FloorSelected = false;
      ArticleCode = string.Empty;
      AreaCode = string.Empty;
      AreaCodeSelected = false;
      PalletCode = string.Empty;
    }


    public List<string> GetXmlCommandParameters()
    {
      return new List<string>()
      {
        Index.ToString(),
        MaxItems.ToString(),
        AreaCodeSelected?AreaCode:"",
        FloorSelected?Floor.ToString():"0".ToString(),
        DateRangeSelected?DateBornMin.ConvertToDateTimeFormatString(): DateTime.MinValue.ConvertToDateTimeFormatString(),
        DateRangeSelected?DateBornMax.ConvertToDateTimeFormatString(): DateTime.MinValue.ConvertToDateTimeFormatString(),
        (string.IsNullOrEmpty(ArticleCode)?"":ArticleCode).Base64Encode(),
      };
    }

    #endregion


  }
}
