﻿

namespace Model.Custom.C200153.Report
{
  public class C200153_DistributionPalletFlow
  {
    public ObservableCollectionFast<C200153_PalletIn> listIn = new ObservableCollectionFast<C200153_PalletIn>();
    public ObservableCollectionFast<C200153_PalletOut> listOut = new ObservableCollectionFast<C200153_PalletOut>();
    public ObservableCollectionFast<C200153_PalletReject> listReject = new ObservableCollectionFast<C200153_PalletReject>();

  }
}
