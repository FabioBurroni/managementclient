﻿
namespace Model.Custom.C200153.Report
{
  
 public class C200153_Distribution_StoricFlowWhIn : ModelBase
  {


    private string _AreaCode;
    public string AreaCode
    {
      get { return _AreaCode; }
      set
      {
        if (value != _AreaCode)
        {
          _AreaCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _WhFloor;
    public int WhFloor
    {
      get { return _WhFloor; }
      set
      {
        if (value != _WhFloor)
        {
          _WhFloor = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _AreaCodeFull;
    public string AreaCodeFull
    {
      get { return _AreaCodeFull; }
      set
      {
        if (value != _AreaCodeFull)
        {
          _AreaCodeFull = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _NumPallet;
    public int NumPallet
    {
      get { return _NumPallet; }
      set
      {
        if (value != _NumPallet)
        {
          _NumPallet = value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
