﻿using Model.Custom.C200153.ArticleManager;
using System;

namespace Model.Custom.C200153.Report
{
  public class C200153_DistributionPallet : ModelBase
  {
    private string _AreaCode;
    public string AreaCode
    {
      get { return _AreaCode; }
      set
      {
        if (value != _AreaCode)
        {
          _AreaCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _FloorNumber;
    public int FloorNumber
    {
      get { return _FloorNumber; }
      set
      {
        if (value != _FloorNumber)
        {
          _FloorNumber = value;
          NotifyPropertyChanged();
        }
      }
    }



    public string AreaCodeFull
    {
      get
      {
        return AreaCode + FloorNumber.ToString();

      }
    }


    private int _NumPallet;
    public int NumPallet
    {
      get { return _NumPallet; }
      set
      {
        if (value != _NumPallet)
        {
          _NumPallet = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _TotalPallet;
    public int TotalPallet
    {
      get { return _TotalPallet; }
      set
      {
        if (value != _TotalPallet)
        {
          _TotalPallet = value;
          NotifyPropertyChanged();
        }
      }
    }


    public double Percent
    {
      get
      {

        var perc = TotalPallet != 0 ? ((double)NumPallet / (double)TotalPallet) * 100 : 0;
        return perc;
      }

    }


  }
}
