﻿namespace Model.Custom.C200153.Report
{
  public class C200153_DistributionArticleBatchByLocalArea : ModelBase
  {

    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _BatchCode;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _AreaCode;
    public string AreaCode
    {
      get { return _AreaCode; }
      set
      {
        if (value != _AreaCode)
        {
          _AreaCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Floor;
    public int Floor
    {
      get { return _Floor; }
      set
      {
        if (value != _Floor)
        {
          _Floor = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _NumPallet;
    public int NumPallet
    {
      get { return _NumPallet; }
      set
      {
        if (value != _NumPallet)
        {
          _NumPallet = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _TotalPallet;
    public int TotalPallet
    {
      get { return _TotalPallet; }
      set
      {
        if (value != _TotalPallet)
        {
          _TotalPallet = value;
          NotifyPropertyChanged();
        }
      }
    }


    public double Percent
    {
      get
      {

        var perc = TotalPallet != 0 ? ((double)NumPallet / (double)TotalPallet) * 100 : 0;
        return perc;
      }

    }




  }
}
