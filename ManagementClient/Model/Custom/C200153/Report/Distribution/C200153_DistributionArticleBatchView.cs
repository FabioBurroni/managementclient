﻿
namespace Model.Custom.C200153.Report
{
  public class C200153_DistributionArticleBatchView : ModelBase
  {


    private string _Area;
    public string Area
    {
      get { return _Area; }
      set
      {
        if (value != _Area)
        {
          _Area = value;
          NotifyPropertyChanged();
        }
      }
    }


    private double _Percent;
    public double Percent
    {
      get { return _Percent; }
      set
      {
        if (value != _Percent)
        {
          _Percent = value;
          NotifyPropertyChanged();
        }
      }
    }





  }
}
