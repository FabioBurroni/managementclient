﻿
namespace Model.Custom.C200153.Report
{
 
  public class C200153_Distribution_StoricFlowArticleWhIn : C200153_Distribution_StoricFlowWhIn
  {
    private bool _Selected;
    public bool Selected
    {
      get { return _Selected; }
      set
      {
        if (value != _Selected)
        {
          _Selected = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }
  }
}
