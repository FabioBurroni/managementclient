﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Extensions;
namespace Model.Custom.C200153.Report
{
  public class C200153_StockArticleBatchFilter : ModelBase
  {


    public C200153_StockArticleBatchFilter()
    {

    }

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; }
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000, 5000, 10000, 30000 };
    #endregion

    public ObservableCollectionFast<C200153_WhArea> WhAreaL { get; set; } = new ObservableCollectionFast<C200153_WhArea>();


    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleDescription;
    public string ArticleDescription
    {
      get { return _ArticleDescription; }
      set
      {
        if (value != _ArticleDescription)
        {
          _ArticleDescription = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _BatchCode = string.Empty;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _DepositorCode = string.Empty;
    public string DepositorCode
    {
      get { return _DepositorCode; }
      set
      {
        if (value != _DepositorCode)
        {
          _DepositorCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C200153_WhArea _AreaSelected = new C200153_WhArea();
    public C200153_WhArea AreaSelected
    {
      get { return _AreaSelected; }
      set
      {
        if (value != _AreaSelected)
        {
          _AreaSelected = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _WarehousePreference;
    public bool WarehousePreference
    {
      get { return _WarehousePreference; }
      set
      {
        if (value != _WarehousePreference)
        {
          _WarehousePreference = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateStart = DateTime.MinValue;
    public DateTime DateStart
    {
      get { return _DateStart; }
      set
      {
        if (value != _DateStart)
        {
          _DateStart = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateEnd;
    public DateTime DateEnd
    {
      get { return _DateEnd; }
      set
      {
        if (value != _DateEnd)
        {
          _DateEnd = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _Available;
    public bool Available
    {
      get { return _Available; }
      set
      {
        if (value != _Available)
        {
          _Available = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _Reserved = false;
    public bool Reserved
    {
      get { return _Reserved; }
      set
      {
        if (value != _Reserved)
        {
          _Reserved = value;
          NotifyPropertyChanged();
        }
      }
    }


    #region public methods
    public void Reset()
    {
      WarehousePreference = false;
      AreaSelected = WhAreaL.FirstOrDefault();
      ArticleCode = string.Empty;
      ArticleDescription = string.Empty;
      BatchCode = string.Empty;
      DepositorCode = string.Empty;
      Available = false;
      DateEnd = DateTime.MinValue;
      DateStart = DateTime.MinValue;
      Reserved = false;
    }


    #endregion

    public List<string> GetXmlCommandParameters()
    {
      string empty = "";
      return new List<string>
      {
        WarehousePreference ? AreaSelected.Code:"",
        Available?"true":"false",
        ArticleCode==null?empty.Base64Encode():ArticleCode.Base64Encode(),
        ArticleDescription==null?empty.Base64Encode():ArticleDescription.Base64Encode(),
        BatchCode==null?empty.Base64Encode():BatchCode.Base64Encode(),
        DepositorCode==null?empty.Base64Encode():DepositorCode.Base64Encode(),
        DateStart.ConvertToDateTimeFormatString()??"",
        DateEnd.ConvertToDateTimeFormatString()??"",
        Reserved ? "true" : "false",
      };
    }

  }
}
