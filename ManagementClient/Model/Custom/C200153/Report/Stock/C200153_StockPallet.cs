﻿using System;

namespace Model.Custom.C200153.Report
{
  public class C200153_StockPallet:ModelBase
  {

    private int _Id;
    public int Id
    {
      get { return _Id; }
      set
      {
        if (value != _Id)
        {
          _Id = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private DateTime _DateBorn;
    public DateTime DateBorn
    {
      get { return _DateBorn; }
      set
      {
        if (value != _DateBorn)
        {
          _DateBorn = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _MapperEntityCode;
    public string MapperEntityCode
    {
      get { return _MapperEntityCode; }
      set
      {
        if (value != _MapperEntityCode)
        {
          _MapperEntityCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _BatchCode;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _ProductionDate;
    public DateTime ProductionDate
    {
      get { return _ProductionDate; }
      set
      {
        if (value != _ProductionDate)
        {
          _ProductionDate = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _ExpiryDate;
    public DateTime ExpiryDate
    {
      get { return _ExpiryDate; }
      set
      {
        if (value != _ExpiryDate)
        {
          _ExpiryDate = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Depositor;
    public string Depositor
    {
      get { return _Depositor; }
      set
      {
        if (value != _Depositor)
        {
          _Depositor = value;
          NotifyPropertyChanged();
        }
      }
    }
  }
}
