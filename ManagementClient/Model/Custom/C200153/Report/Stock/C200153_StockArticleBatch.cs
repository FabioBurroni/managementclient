﻿using System.Collections.Generic;

namespace Model.Custom.C200153.Report
{
  public class C200153_StockArticleBatch : ModelBase
  {
    public string BatchCode { get; set; }
    public string ArticleCode { get; set; }
    public string ArticleDescr{ get; set; }
    public string DepositorCode { get; set; }
    public List<C200153_StockByArea> StockByArea { get; set; } = new List<C200153_StockByArea>();

    public int MaxQty { get; set; }
    public int MinQty { get; set; }
  }

  public class C200153_StockByArea
  {
    public string AreaCode { get; set; }
    public int NumPallet { get; set; }
    public int MaxQty { get; set; }
    public int MinQty { get; set; }
  }

  public class C200153_StockMinMax
  {

    public int Stock { get; set; }

    public int Min { get; set; }

    public int Max { get; set; }

    public C200153_StockMinMax(int giac, int min, int max)
    {
      Stock = giac;
      Min = min;
      Max = max;
    }
  }

}
