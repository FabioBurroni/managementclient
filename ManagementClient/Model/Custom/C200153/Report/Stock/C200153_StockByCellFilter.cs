﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Extensions;


namespace Model.Custom.C200153.Report
{
  public class C200153_StockByCellFilter : ModelBase
  {

    public C200153_StockByCellFilter()
    {
     
    }

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; } 
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000,5000,10000,30000};
    #endregion

    public ObservableCollectionFast<C200153_WhArea> WhAreaL { get; set; } = new ObservableCollectionFast<C200153_WhArea>();


    private string _BatchCode = string.Empty;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleCode = string.Empty;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }



    private string _ArticleDescr = string.Empty;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C200153_WhArea _AreaSelected = new C200153_WhArea();
    public C200153_WhArea AreaSelected
    {
      get { return _AreaSelected; }
      set
      {
        if (value != _AreaSelected)
        {
          _AreaSelected = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _WarehousePreference;
    public bool WarehousePreference
    {
      get { return _WarehousePreference; }
      set
      {
        if (value != _WarehousePreference)
        {
          _WarehousePreference = value;
          NotifyPropertyChanged();
        }
      }
    }

    #region public methods
    public void Reset()
    {
      WarehousePreference = false;
      AreaSelected = WhAreaL.FirstOrDefault();
      BatchCode = string.Empty;
      ArticleCode = string.Empty;
      ArticleDescr= string.Empty;
    }
    #endregion

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        BatchCode==null?"":BatchCode.Base64Encode(),
        ArticleCode==null?"":ArticleCode.Base64Encode(),
        ArticleDescr==null?"":ArticleDescr.Base64Encode(),
        WarehousePreference ? AreaSelected.Code:"",
      };
    }

  }
}
