﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153
{
  public class C200153_PositionIn_Pallet : ModelBase
  {

    private string _PositionCode;
    public string PositionCode
    {
      get { return _PositionCode; }
      set
      {
        if (value != _PositionCode)
        {
          _PositionCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _PalletCode=string.Empty;
    public string PalletCode
    {
      get { return _PalletCode; }
      set
      {
        if (value != _PalletCode)
        {
          _PalletCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsService;
    public bool IsService
    {
      get { return _IsService; }
      set
      {
        if (value != _IsService)
        {
          _IsService = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_CustomResult _CustomResult;
    public C200153_CustomResult CustomResult
    {
      get { return _CustomResult; }
      set
      {
        if (value != _CustomResult)
        {
          _CustomResult = value;
          NotifyPropertyChanged();
          if(_CustomResult!= C200153_CustomResult.OK)
          {
            IsCustomReject = true;
          }
          else
          {
            IsCustomReject = false;
          }
        }
      }
    }



    private bool _IsCustomReject;
    public bool IsCustomReject
    {
      get { return _IsCustomReject; }
      set
      {
        if (value != _IsCustomReject)
        {
          _IsCustomReject = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsReject;
    public bool IsReject
    {
      get { return _IsReject; }
      set
      {
        if (value != _IsReject)
        {
          _IsReject = value;
          NotifyPropertyChanged();
        }
      }
    }


    private Results_Enum _RejectResult;
    public Results_Enum RejectResult
    {
      get { return _RejectResult; }
      set
      {
        if (value != _RejectResult)
        {
          _RejectResult = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _BarCode1;
    public string BarCode1
    {
      get { return _BarCode1; }
      set
      {
        if (value != _BarCode1)
        {
          _BarCode1 = value;
          NotifyPropertyChanged();
        }
      }
    }



    private string _BarCode2;
    public string BarCode2
    {
      get { return _BarCode2; }
      set
      {
        if (value != _BarCode2)
        {
          _BarCode2 = value;
          NotifyPropertyChanged();
        }
      }
    }




    private bool _PalletSchiavoNeeded;
    public bool PalletSchiavoNeeded
    {
      get { return _PalletSchiavoNeeded; }
      set
      {
        if (value != _PalletSchiavoNeeded)
        {
          _PalletSchiavoNeeded = value;
          NotifyPropertyChanged();
        }
      }
    }



    private bool _PalletSchiavo;
    public bool PalletSchiavo
    {
      get { return _PalletSchiavo; }
      set
      {
        if (value != _PalletSchiavo)
        {
          _PalletSchiavo = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsLabeled;
    public bool IsLabeled
    {
      get { return _IsLabeled; }
      set
      {
        if (value != _IsLabeled)
        {
          _IsLabeled = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsWrapped;
    public bool IsWrapped
    {
      get { return _IsWrapped; }
      set
      {
        if (value != _IsWrapped)
        {
          _IsWrapped = value;
          NotifyPropertyChanged();
        }
      }
    }


    public void Update(C200153_PositionIn_Pallet newPallet)
    {
      this.PalletCode = newPallet.PalletCode;
      this.IsService = newPallet.IsService;
      this.CustomResult = newPallet.CustomResult;
      this.IsReject = newPallet.IsReject;
      this.RejectResult = newPallet.RejectResult;
      this.ArticleCode = newPallet.ArticleCode;
      this.ArticleDescr = newPallet.ArticleDescr;
      this.BarCode1 = newPallet.BarCode1;
      this.BarCode2 = newPallet.BarCode2;
      this.PalletSchiavoNeeded = newPallet.PalletSchiavoNeeded;
      this.PalletSchiavo = newPallet.PalletSchiavo;
      this.IsLabeled = newPallet.IsLabeled;
      this.IsWrapped = newPallet.IsWrapped;
    }

    public void Reset()
    {
      this.PalletCode = string.Empty;
      this.IsService = false;
      this.CustomResult = C200153_CustomResult.UNDEFINED;
      this.IsReject = false;
      this.RejectResult = Results_Enum.UNDEFINED;
      this.ArticleCode = string.Empty;
      this.ArticleDescr = string.Empty;
      this.BarCode1 = string.Empty;
      this.BarCode2 = string.Empty;
      this.PalletSchiavoNeeded = false;
      this.PalletSchiavo = false;
      this.IsLabeled = false;
      this.IsWrapped = false;
    }
  }
}
