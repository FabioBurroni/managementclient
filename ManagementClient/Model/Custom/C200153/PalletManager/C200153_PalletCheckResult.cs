﻿
namespace Model.Custom.C200153.PalletManager
{
  public class C200153_PalletCheckResult : ModelBase
  {
    private C200153_CustomResult _Result;

    public C200153_CustomResult Result
    {
      get { return _Result; }
      set 
      {
        if (value != _Result)
        {
          _Result = value;
          NotifyPropertyChanged();
        }
      }
    }



    private C200153_PalletInsert _Pallet ;
    public C200153_PalletInsert Pallet
    {
      get { return _Pallet; }
      set
      {
        if (value != _Pallet)
        {
          _Pallet = value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
