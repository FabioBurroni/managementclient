﻿using System;
namespace Model.Custom.C200153.PalletManager
{
  public class C200153_PalletInsert : ModelBase
  {
    private string _Code = string.Empty;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_PalletType _PalletTypeId = C200153_PalletType.EUROPALLET;
    public C200153_PalletType PalletTypeId
    {
      get { return _PalletTypeId; }
      set
      {
        if (value != _PalletTypeId)
        {
          _PalletTypeId = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode = string.Empty;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _ProductionDate = DateTime.Now;
    public DateTime ProductionDate
    {
      get { return _ProductionDate; }
      set
      {
        if (value != _ProductionDate)
        {
          _ProductionDate = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _ExpiryDate = DateTime.Now;
    public DateTime ExpiryDate
    {
      get { return _ExpiryDate; }
      set
      {
        if (value != _ExpiryDate)
        {
          _ExpiryDate = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _BatchCode = string.Empty;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _BatchLength;
    public int BatchLength
    {
      get { return _BatchLength; }
      set
      {
        if (value != _BatchLength)
        {
          _BatchLength = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsLabeled;
    public bool IsLabeled
    {
      get { return _IsLabeled; }
      set
      {
        if (value != _IsLabeled)
        {
          _IsLabeled = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsWrapped;
    public bool IsWrapped
    {
      get { return _IsWrapped; }
      set
      {
        if (value != _IsWrapped)
        {
          _IsWrapped = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _DepositorCode = string.Empty;
    public string DepositorCode
    {
      get { return _DepositorCode; }
      set
      {
        if (value != _DepositorCode)
        {
          _DepositorCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    public void Update(C200153_PalletInsert newPallet)
    {
      this.Code = newPallet.Code;
      this.PalletTypeId = newPallet.PalletTypeId;
      this.ArticleCode = newPallet.ArticleCode;
      this.ProductionDate = newPallet.ProductionDate;
      this.ExpiryDate = newPallet.ExpiryDate;
      this.BatchCode = newPallet.BatchCode;
      this.BatchLength = newPallet.BatchLength;
      this.IsLabeled = newPallet.IsLabeled;
      this.IsWrapped = newPallet.IsWrapped;
      this.DepositorCode = newPallet.DepositorCode;
    }

    public void Reset()
    {
      this.Code = string.Empty;
      this.PalletTypeId = C200153_PalletType.UNDEFINED;
      this.ArticleCode = string.Empty;
      this.ProductionDate = DateTime.Now;
      this.ExpiryDate = DateTime.Now;
      this.BatchCode = string.Empty;
      this.BatchLength = 0;
      this.IsLabeled = false;
      this.IsWrapped = false;
      this.DepositorCode = string.Empty;
    }
  }
}
