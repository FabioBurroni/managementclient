﻿
using System.Collections.Generic;
using Utilities.Extensions;


namespace Model.Custom.C200153.PalletManager
{
  public class C200153_PalletManagerAllFilter : ModelBase
  {

    public C200153_PalletManagerAllFilter()
    {
     
    }

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; } 
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000,5000,10000,30000};
    #endregion

    private string _PalletCode = string.Empty;
    public string PalletCode
    {
      get { return _PalletCode; }
      set
      {
        if (value != _PalletCode)
        {
          _PalletCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _BatchCode = string.Empty;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleCode = string.Empty;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }



    private string _ArticleDescr = string.Empty;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _DepositorCode = string.Empty;
    public string DepositorCode
    {
      get { return _DepositorCode; }
      set
      {
        if (value != _DepositorCode)
        {
          _DepositorCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    #region public methods
    public void Reset()
    {
      PalletCode = string.Empty;
      BatchCode = string.Empty;
      ArticleCode = string.Empty;
      ArticleDescr= string.Empty;
      DepositorCode = string.Empty;
    }
    #endregion

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        PalletCode==null?"":PalletCode.Base64Encode(),
        BatchCode==null?"":BatchCode.Base64Encode(),
        ArticleCode==null?"":ArticleCode.Base64Encode(),
        ArticleDescr==null?"":ArticleDescr.Base64Encode(),
        DepositorCode==null?"":DepositorCode.Base64Encode(),
      };
    }

  }
}
