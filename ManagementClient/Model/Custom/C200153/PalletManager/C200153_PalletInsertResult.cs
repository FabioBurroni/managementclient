﻿using System;
namespace Model.Custom.C200153.PalletManager
{
  public class C200153_PalletInsertResult : ModelBase
  {
    private C200153_CustomResult _Result;

    public C200153_CustomResult Result
    {
      get { return _Result; }
      set 
      {
        if (value != _Result)
        {
          _Result = value;
          NotifyPropertyChanged();
        }
      }
    }



    private string _Code = string.Empty;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
