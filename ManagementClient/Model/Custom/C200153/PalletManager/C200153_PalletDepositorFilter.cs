﻿using System.Collections.Generic;
using Utilities.Extensions;


namespace Model.Custom.C200153.PalletManager
{
  public class C200153_PalletDepositorFilter : ModelBase
  {

    public C200153_PalletDepositorFilter()
    {
      
    }

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; } 
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000 };
    #endregion





    private string _DepositorCode = string.Empty;
    public string DepositorCode
    {
      get { return _DepositorCode; }
      set
      {
        if (value != _DepositorCode)
        {
          _DepositorCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _DepositorAddress = string.Empty;
    public string DepositorAddress
    {
      get { return _DepositorAddress; }
      set
      {
        if (value != _DepositorAddress)
        {
          _DepositorAddress = value;
          NotifyPropertyChanged();
        }
      }
    }

    #region public methods
    public void Reset()
    {
      DepositorCode = string.Empty;
      DepositorAddress = string.Empty;
    }
    #endregion

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        DepositorCode==null?"":DepositorCode.Base64Encode(),
        DepositorAddress==null?"":DepositorAddress.Base64Encode(),
      };
    }

  }
}
