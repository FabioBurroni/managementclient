﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Model.Custom.C200153.Administration
{
  public class C200153_Master:ModelBase, IUpdatable
  {
    public C200153_Master(string code, C200153_MasterCategory category)
    {
      _Code = code;
      _Category = category;
      OriginalCategory = category;
    }

    private string _Code;
    public string Code
    {
      get { return _Code; }
    }


    private C200153_MasterCategory _Category=C200153_MasterCategory.NORMAL;
    public C200153_MasterCategory Category
    {
      get { return _Category; }
      set
      {
        if (value != _Category)
        {
          _Category = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsChanged");
        }
      }
    }


    public C200153_MasterCategory OriginalCategory { get; set; }

    public bool IsChanged
    {
      get { return Category!=OriginalCategory; }
    }


    public void Update(object newElement)
    {
      Category = ((C200153_Master)newElement).Category;
      OriginalCategory = Category;
      NotifyPropertyChanged("IsChanged");
    }
  }


}
