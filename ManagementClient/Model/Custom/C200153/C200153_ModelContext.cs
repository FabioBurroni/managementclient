﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Custom.C200153.OrderManager;

namespace Model.Custom.C200153
{
  public class C200153_ModelContext
  {
    #region Istanza

    private static readonly Lazy<C200153_ModelContext> Lazy = new Lazy<C200153_ModelContext>(() => new C200153_ModelContext());

    public static C200153_ModelContext Instance => Lazy.Value;

    private C200153_ModelContext()
    {
    }
    #endregion

    #region ListDestinasions 

    public bool IsListDestination { get; set; }
    public ObservableCollectionFast<C200153_ListDestination> ListDestinations { get; set; } = new ObservableCollectionFast<C200153_ListDestination>(); 
    #endregion
  }
}
