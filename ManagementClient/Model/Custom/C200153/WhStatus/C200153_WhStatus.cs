﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153.WhStatus
{
  public class C200153_WhStatus:ModelBase
  {

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _CellsCount;
    public int CellsCount
    {
      get { return _CellsCount; }
      set
      {
        if (value != _CellsCount)
        {
          _CellsCount = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _Capacity;
    public int Capacity
    {
      get { return _Capacity; }
      set
      {
        if (value != _Capacity)
        {
          _Capacity = value;
          NotifyPropertyChanged();
        }
      }
    }




    private int _FullnessPercentage;
    public int FullnessPercentage
    {
      get { return _FullnessPercentage; }
      set
      {
        if (value != _FullnessPercentage)
        {
          _FullnessPercentage = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _NumTotFullCells;
    public int NumTotFullCells
    {
      get { return _NumTotFullCells; }
      set
      {
        if (value != _NumTotFullCells)
        {
          _NumTotFullCells = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _NumTotFullCellsPercentage;
    public int NumTotFullCellsPercentage
    {
      get { return _NumTotFullCellsPercentage; }
      set
      {
        if (value != _NumTotFullCellsPercentage)
        {
          _NumTotFullCellsPercentage = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _NumTotEmptyCells;
    public int NumTotEmptyCells
    {
      get { return _NumTotEmptyCells; }
      set
      {
        if (value != _NumTotEmptyCells)
        {
          _NumTotEmptyCells = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _NumTotEmptyCellsPercentage;
    public int NumTotEmptyCellsPercentage
    {
      get { return _NumTotEmptyCellsPercentage; }
      set
      {
        if (value != _NumTotEmptyCellsPercentage)
        {
          _NumTotEmptyCellsPercentage = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _NumTotHalfFullCells;
    public int NumTotHalfFullCells
    {
      get { return _NumTotHalfFullCells; }
      set
      {
        if (value != _NumTotHalfFullCells)
        {
          _NumTotHalfFullCells = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _NumTotHalfFullCellsPercentage;
    public int NumTotHalfFullCellsPercentage
    {
      get { return _NumTotHalfFullCellsPercentage; }
      set
      {
        if (value != _NumTotHalfFullCellsPercentage)
        {
          _NumTotHalfFullCellsPercentage = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _NumTotBlockedCells;
    public int NumTotBlockedCells
    {
      get { return _NumTotBlockedCells; }
      set
      {
        if (value != _NumTotBlockedCells)
        {
          _NumTotBlockedCells = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _NumTotBlockedCellsPercentage;
    public int NumTotBlockedCellsPercentage
    {
      get { return _NumTotBlockedCellsPercentage; }
      set
      {
        if (value != _NumTotBlockedCellsPercentage)
        {
          _NumTotBlockedCellsPercentage = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _NumTotPallet;
    public int NumTotPallet
    {
      get { return _NumTotPallet; }
      set
      {
        if (value != _NumTotPallet)
        {
          _NumTotPallet = value;
          NotifyPropertyChanged();
        }
      }
    }


    public void Update(C200153_WhStatus newWhs)
    {
      /*
      this.Capacity = 100;
      this.CellsCount = 50;

      this.CellsBlockedCount = 30;
      this.NumTotHalfFullCells = 10;
      this.NumTotPallet = 10;
      */

      this.Capacity = newWhs.Capacity;
      this.CellsCount = newWhs.CellsCount;

      this.NumTotFullCells = newWhs.NumTotFullCells;
      this.NumTotEmptyCells = newWhs.NumTotEmptyCells;
      this.NumTotHalfFullCells = newWhs.NumTotHalfFullCells;
      this.NumTotBlockedCells= newWhs.NumTotBlockedCells;
      this.NumTotPallet = newWhs.NumTotPallet;

      if (Capacity > 0)
      {
        decimal res = (decimal)NumTotPallet / (decimal)Capacity;
        var res1 = (int)Math.Floor(res * 100);
        FullnessPercentage = res1;
      }
      else
      {
        FullnessPercentage = 0;
      }


      if (CellsCount > 0)
      {
        NumTotFullCellsPercentage = (int)Math.Floor(((decimal)NumTotFullCells/ (decimal)CellsCount)*100);
        NumTotEmptyCellsPercentage = (int)Math.Floor(((decimal)NumTotEmptyCells/ (decimal)CellsCount) * 100);
        NumTotHalfFullCellsPercentage = (int)Math.Floor(((decimal)NumTotHalfFullCells / (decimal)CellsCount)*100);
        NumTotBlockedCellsPercentage= (int)Math.Floor(((decimal)NumTotBlockedCells/ (decimal)CellsCount)*100);
      }
      else
      {
        NumTotFullCellsPercentage = 0;
        NumTotEmptyCellsPercentage = 0;
        NumTotHalfFullCellsPercentage = 0;
        NumTotBlockedCellsPercentage = 0;
      }
    }
  }
}
