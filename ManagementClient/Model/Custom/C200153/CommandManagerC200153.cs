﻿using System;
using System.Linq;
using System.Collections.Generic;

using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;

using XmlCommunicationManager.Authorization;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.Encoding;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlClient.Event;

using Model.Common;
using Model.Common.Configuration;
using Model.Custom.C200153.Plugin;
using Model.Custom.C200153.OrderManager;
using Model.Custom.C200153.ArticleManager;
using Model.Custom.C200153.Filter;

using System.Collections.ObjectModel;
using System.Globalization;
using Model.Custom.C200153.Label;
using Model.Custom.C200153.Printer;
using Model.Custom.C200153.Report;
using Model.Custom.C200153.PalletManager;
using Model.Custom.C200153.Router;
using Model.Custom.C200153.Depositor;

namespace Model.Custom.C200153
{
  public class CommandManagerC200153 : CommandManager
  {
    #region COSTRUTTORE

    public CommandManagerC200153(IXmlClient xmlClient, string commandHeader) : base(xmlClient, commandHeader)
    {
    }
    #endregion

    #region PLUGIN MANAGER

    public void Plugin_GetAll(object receiverInstance)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_GetAll"));
    }
    private IModel Plugin_GetAll(XmlCommandResponse resp)
    {
      var plgL = new List<PluginInfo>();
      foreach (var item in resp.Items)
      {
        plgL.Add(Plugin_FromXml(item));
      }
      return new DataWrapperResult(plgL);
    }

    public void Plugin_Get(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Get", pluginName));
    }
    private IModel Plugin_Get(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        dwr.Data = Plugin_FromXml(item);
      }
      return dwr;
    }

    public void Plugin_Check(object receiverInstance)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Check"));
    }

    private IModel Plugin_Check(XmlCommandResponse resp)
    {
      return new DataWrapperResult();
    }

    public void Plugin_Add(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Add", pluginName));
    }
    private IModel Plugin_Add(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }

    public void Plugin_Update(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Update", pluginName));
    }
    private IModel Plugin_Update(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }
    public void Plugin_Start(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Start", pluginName));
    }

    private IModel Plugin_Start(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }
    public void Plugin_Stop(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Stop", pluginName));
    }
    private IModel Plugin_Stop(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }

    public void Plugin_Enable(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Enable", pluginName));
    }
    private IModel Plugin_Enable(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }

    public void Plugin_Disable(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Disable", pluginName));
    }
    private IModel Plugin_Disable(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }

    public void Plugin_Unload(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Unload", pluginName));
    }
    private IModel Plugin_Unload(XmlCommandResponse resp)
    {
      var dwr = new DataWrapperResult();
      if (resp.itemCount == 2)
      {
        dwr.Result = resp.Items[0].getFieldVal(1);
        dwr.Data = Plugin_FromXml(resp.Items[1]);
      }
      return dwr;
    }

    public void Plugin_XmlCommand_Get(object receiverInstance, string pluginName)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_XmlCommand_Get", pluginName));
    }
    private IModel Plugin_XmlCommand_Get(XmlCommandResponse resp)
    {
      List<string> xmlCommands = new List<string>();
      if (resp.itemCount == 1)
      {
        foreach (var field in resp.Items[0])
        {
          xmlCommands.Add(field);
        }
      }
      return new DataWrapperResult(xmlCommands);
    }

    public void Plugin_Command(object receiverInstance, params string[] parameters)
    {
      Plugin_Command(receiverInstance, parameters.EmptyIfNull());
    }
    public void Plugin_Command(object receiverInstance, IEnumerable<string> parameters, ICredentials advancedUserCredentials = null)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Command", parameters), advancedUserCredentials);
    }

    #endregion

    #region CONFIGURATION
    public void CO_Configuration_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Configuration_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Configuration_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<ConfParameterServer> cpL = new List<ConfParameterServer>();
      dwr.Data = cpL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          ConfParameterServer p = new ConfParameterServer();
          p.Name = item.getFieldVal<string>(startIndex++);
          p.OriginalValue = item.getFieldVal<string>(startIndex++);
          cpL.Add(p);
        }
      }
      return dwr;
    }

    public void CO_Configuration_Get(object receiverInstance, string parameterName)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Configuration_Get");
      parameters.Add(parameterName);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Configuration_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        ConfParameterServer p = new ConfParameterServer();
        p.Name = item.getFieldVal<string>(startIndex++);
        p.OriginalValue = item.getFieldVal<string>(startIndex++);

        dwr.Data = p;
      }
      return dwr;
    }


    public void CO_Configuration_Set(object receiverInstance, string parameterName, string parameterValue)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Configuration_Set");
      parameters.Add(parameterName);
      parameters.Add(parameterValue);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Configuration_Set(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        var result = FromXmlTo_Result(item, ref startIndex);
        dwr.Result = result.ToString();
      }
      return dwr;
    }




    #endregion

    #region PRINTER

    //CHK_Print
    public void CO_PrintRaw(object receiverInstance, string positionCode, string labelCode)
    {
      var parameters = new List<string>();
      parameters.Add("CO_PrintRaw");
      parameters.AddRange(new string[] { positionCode, labelCode });
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_PrintRaw(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var itemResult = resp.Items[0];
        int startIndex = 1;
        dwr.Result = itemResult.getFieldVal(startIndex++).ToString();
      }
      return dwr;

    }
    //CHK_Print
    public void CO_Print(object receiverInstance, string positionCode, string palletCode)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Print");
      parameters.AddRange(new string[] { positionCode, palletCode });
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Print(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var itemResult = resp.Items[0];
        int startIndex = 1;
        dwr.Result = itemResult.getFieldVal(startIndex++).ToString();
      }
      return dwr;
    }

    //  CHK_Printer_GetAll
    public void CO_Printer_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Printer_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Printer_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Printer> list = new List<C200153_Printer>();
      dwr.Data = list;
      foreach (var item in resp.Items)
      {
        int startIndex = 1;
        var element = FromXmlTo_Printer(item, ref startIndex);
        list.Add(element);
      }
      return dwr;
    }

    //  CHK_Printer_Get
    public void CO_Printer_Get(object receiverInstance, string positionCode)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Printer_Get");
      parameters.Add(positionCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Printer_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      if (resp.itemCount >= 2)
      {
        int startIndex = 1;
        dwr.Data = FromXmlTo_Printer(resp.Items[1], ref startIndex);
      }
      return dwr;
    }

    //  CHK_Printer_Delete
    public void CO_Printer_Delete(object receiverInstance, string positionCode)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Printer_Delete");
      parameters.Add(positionCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Printer_Delete(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      return dwr;
    }


    //  CHK_Printer_InsertOrUpdate
    public void CO_Printer_InsertOrUpdate(object receiverInstance, string positionCode, string ipAddress, int portNumber, string description, int timeout)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Printer_InsertOrUpdate");
      parameters.AddRange(new string[] { positionCode, ipAddress, portNumber.ToString(), description.Base64Encode(), timeout.ToString() });
      Plugin_Command(receiverInstance, parameters);
    }

    private IModel CO_Printer_InsertOrUpdate(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      return dwr;
    }

    private C200153_Printer FromXmlTo_Printer(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_Printer printer = new C200153_Printer();
      printer.PositionCode = item.getFieldVal(startIndex++);
      printer.Description = item.getFieldVal(startIndex++).Base64Decode();
      printer.IpAddress = item.getFieldVal(startIndex++);
      printer.PortNumber = item.getFieldVal<int>(startIndex++);
      printer.Timeout = item.getFieldVal<int>(startIndex++);
      return printer;
    }

    #endregion

    #region LABEL

    public void CO_Label_Delete(object receiverInstance, string code)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Label_Delete");
      parameters.Add(code);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Label_Delete(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      return dwr;
    }
    //  CHK_Label_GetAll
    public void CO_Label_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Label_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Label_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Label> list = new List<C200153_Label>();
      dwr.Data = list;
      foreach (var item in resp.Items)
      {
        int startIndex = 1;
        var element = FromXmlTo_Label(item, ref startIndex);
        list.Add(element);
      }
      return dwr;
    }

    //  CHK_Label_Get
    public void CO_Label_Get(object receiverInstance, string code)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Label_Get");
      parameters.Add(code);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_Label_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      if (resp.itemCount >= 1)
      {
        int startIndex = 1;
        dwr.Data = FromXmlTo_Label(resp.Items[1], ref startIndex);
      }
      return dwr;
    }
    //  CHK_Label_InsertOrUpdate
    public void CO_Label_InsertOrUpdate(object receiverInstance, string code, string label)
    {
      var parameters = new List<string>();
      parameters.Add("CO_Label_InsertOrUpdate");
      parameters.AddRange(new string[] { code, label.Base64Encode() });
      Plugin_Command(receiverInstance, parameters);
    }

    private IModel CO_Label_InsertOrUpdate(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      return dwr;
    }

    private C200153_Label FromXmlTo_Label(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_Label label = new C200153_Label();
      label.Code = item.getFieldVal(startIndex++);
      label.Label = item.getFieldVal(startIndex++).Base64Decode();
      return label;
    }

    #endregion

    #region LABEL FIELD

    //  CO_LabelField_GetAll
    public void CO_LabelField_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("CO_LabelField_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_LabelField_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_LabelFieldTranslator> list = new List<C200153_LabelFieldTranslator>();
      dwr.Data = list;
      foreach (var item in resp.Items)
      {
        int startIndex = 1;
        var element = FromXmlTo_LabelField(item, ref startIndex);
        list.Add(element);
      }
      return dwr;
    }

    //  CO_LabelField_Get
    public void CO_LabelField_Get(object receiverInstance, string code)
    {
      var parameters = new List<string>();
      parameters.Add("CO_LabelField_Get");
      parameters.Add(code);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_LabelField_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      if (resp.itemCount >= 1)
      {
        int startIndex = 1;
        dwr.Data = FromXmlTo_LabelField(resp.Items[1], ref startIndex);
      }
      return dwr;
    }



    //  CO_LabelField_InsertOrUpdate
    public void CO_LabelField_InsertOrUpdate(object receiverInstance, string field, string fieldTranslated
      , string fieldEnglish, string description)
    {
      var parameters = new List<string>();
      parameters.Add("CO_LabelField_InsertOrUpdate");
      parameters.AddRange(new string[] { field, fieldTranslated.Base64Encode(), fieldEnglish.Base64Encode(), description.Base64Encode() });
      Plugin_Command(receiverInstance, parameters);
    }

    private IModel CO_LabelField_InsertOrUpdate(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      return dwr;
    }



    public void CO_LabelField_Delete(object receiverInstance, string field)
    {
      var parameters = new List<string>();
      parameters.Add("CO_LabelField_Delete");
      parameters.Add(field);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel CO_LabelField_Delete(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Result = resp.Items[0].getFieldVal(startIndex++);
      }
      return dwr;
    }



    private C200153_LabelFieldTranslator FromXmlTo_LabelField(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_LabelFieldTranslator label = new C200153_LabelFieldTranslator();
      label.Field = item.getFieldVal(startIndex++);
      label.FieldTranslated = item.getFieldVal(startIndex++).Base64Decode();
      label.FieldEnglish = item.getFieldVal(startIndex++).Base64Decode();
      label.Description = item.getFieldVal(startIndex++).Base64Decode();
      return label;
    }
    #endregion

    #region ROUTER
    public void RC_RouterConfigurationGetByZone(object receiverInstance, string zone)
    {
      var parameters = new List<string>();
      parameters.Add("RC_RouterConfigurationGetByZone");
      parameters.Add(zone);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RC_RouterConfigurationGetByZone(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        C200153_RouterConfigurationInfo rc = new C200153_RouterConfigurationInfo();
        dwr.Data = rc;
        int startIndex = 1;
        FromXmlTo(rc, item, ref startIndex);

        var Item2 = resp.Items[1];
 
        for (int i = 1; i <= Item2.Fields.Count; i++)
        {
          rc.AllowedSwitch.Add(Item2.getFieldVal<string>(i));
        }
      }
      return dwr;
    }

    public void RC_RouterConfigurationGetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RC_RouterConfigurationGetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RC_RouterConfigurationGetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_RouterConfigurationInfo> rcL = new List<C200153_RouterConfigurationInfo>();
      dwr.Data = rcL;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          C200153_RouterConfigurationInfo rc = new C200153_RouterConfigurationInfo();
          rcL.Add(rc);
          int startIndex = 1;
          FromXmlTo(rc, item, ref startIndex);
        }
      }
      return dwr;
    }

    public void RC_RouterConfigurationGetAllForPositions(object receiverInstance, List<string> positions)
    {
      var parameters = new List<string>();
      parameters.Add("RC_RouterConfigurationGetAllForPositions");
      if (positions != null)
        parameters.AddRange(positions);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RC_RouterConfigurationGetAllForPositions(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_RouterConfigurationInfo> rcL = new List<C200153_RouterConfigurationInfo>();
      dwr.Data = rcL;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          C200153_RouterConfigurationInfo rc = new C200153_RouterConfigurationInfo();
          rcL.Add(rc);
          int startIndex = 1;
          FromXmlTo(rc, item, ref startIndex);
        }
      }
      return dwr;
    }

    

    public void RC_RouterConfigurationSetByZone(object receiverInstance, string zone, C200153_RouterConfigurationEnum requestedConfiguration)
    {
      var parameters = new List<string>();
      parameters.Add("RC_RouterConfigurationSetByZone");
      parameters.Add(zone);
      parameters.Add(requestedConfiguration.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RC_RouterConfigurationSetByZone(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
       int startIndex = 1;
       var item = resp.Items[0];
       dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }

    public void RC_RouterConfigurationForceByZone(object receiverInstance, string zone, C200153_RouterConfigurationEnum requestedConfiguration)
    {
        var parameters = new List<string>();
        parameters.Add("RC_RouterConfigurationForceByZone");
        parameters.Add(zone);
        parameters.Add(requestedConfiguration.ToString());
        Plugin_Command(receiverInstance, parameters);
    }
    private IModel RC_RouterConfigurationForceByZone(XmlCommandResponse resp)
    {
        DataWrapperResult dwr = new DataWrapperResult();
        if (resp.itemCount > 0)
        {
            int startIndex = 1;
            var item = resp.Items[0];
            dwr.Data = FromXmlTo_Result(item, ref startIndex);
        }
        return dwr;
    }

    public void RC_RouterConfigurationGetAllowedSwitch(object receiverInstance, string zone)
    {
      var parameters = new List<string>();
      parameters.Add("RC_RouterConfigurationGetAllowedSwitch");
      parameters.Add(zone);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RC_RouterConfigurationGetAllowedSwitch(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<string> RouterList = new List<string>();
      dwr.Data = RouterList;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          RouterList.Add(item.getFieldVal<string>(startIndex++));
         }
      }
      return dwr;
    }

    

    private void FromXmlTo(C200153_RouterConfigurationInfo rc, XmlCommandResponse.Item item, ref int startIndex)
        {
            rc.Zone = item.getFieldVal(startIndex++);
            rc.CurrentConfiguration = item.getFieldVal<C200153_RouterConfigurationEnum>(startIndex++);
            rc.CurrentConfigurationLastUpdate = item.getFieldVal<DateTime>(startIndex++);
            rc.RequestedConfiguration = item.getFieldVal<C200153_RouterConfigurationEnum>(startIndex++);
            rc.RequestedConfigurationLastUpdate = item.getFieldVal<DateTime>(startIndex++);
            rc.IsSwitching = item.getFieldVal<bool>(startIndex++);
        }
             
    #endregion

    #region ORDER MANAGER
    public void OM_OrderComponent_Kill(object receiverInstance, int order_id, int cmp_id)
    {
      var parameters = new List<string>();
      parameters.Add("OM_OrderComponent_Kill");
      parameters.AddRange(new string[] { order_id.ToString(), cmp_id.ToString() });
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_OrderComponent_Kill(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(resp.Items[0], ref startIndex);
      }
      return dwr;
    }

    public void OM_OrderDestination_GetAllForPositions(object receiverInstance, List<string> positions)
    {
      var parameters = new List<string>();
      parameters.Add("OM_OrderDestination_GetAllForPositions");
      if (positions != null)
       parameters.AddRange(positions);

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_OrderDestination_GetAllForPositions(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_ListDestination> list = new List<C200153_ListDestination>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          list.Add(OM_FromXmlTo_ListDestination(item, ref startIndex));
        }
      }
      return dwr;
    }


    public void OM_OrderDestination_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("OM_OrderDestination_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }    
    private IModel OM_OrderDestination_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_ListDestination> list = new List<C200153_ListDestination>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          list.Add(OM_FromXmlTo_ListDestination(item, ref startIndex));
        }
      }
      return dwr;
    }

    public void OM_Stock_BatchArticle_ByOrderDestination(object receiverInstance, int startIndex, int maxItems, C200153_ListDestination ld, bool available, string articleCode, string batchCode)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Stock_BatchArticle_ByOrderDestination");
      //...listDestination
      parameters.Add(startIndex.ToString());
      parameters.Add(maxItems.ToString());
      parameters.Add((ld.Code != null) ? ld.Code : "");
      parameters.Add(available.ToString());
      parameters.Add(articleCode.Base64Encode());
      parameters.Add((batchCode != null) ? batchCode.Base64Encode() : string.Empty.Base64Encode());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Stock_BatchArticle_ByOrderDestination(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_ArticleStockByArea> giacL = new List<C200153_ArticleStockByArea>();
      dwr.Data = giacL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          C200153_ArticleStockByArea asa = new C200153_ArticleStockByArea()
          {
            BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode(),
            ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode(),
            ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode(),
          };
          giacL.Add(asa);
          asa.StockByAreaL.AddRange(OM_FromXmlTo_StockByAreaList(item, ref startIndex));
        }
      }
      return dwr;
    }

    public void OM_Stock_BatchArticle(object receiverInstance, string posDestCode, C200153_ArticleForOrderFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Stock_BatchArticle"); 
      if (posDestCode == null)
        posDestCode = string.Empty;
      parameters.Add(posDestCode);
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Stock_BatchArticle(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_StockForOrder> giacL = new List<C200153_StockForOrder>();
      dwr.Data = giacL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          C200153_StockForOrder giac = new C200153_StockForOrder()
          {
            Article = OM_FromXmlTo_Article(item, ref startIndex),
            BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode(),
            NumPallet = item.getFieldVal<int>(startIndex++),
          };

          giacL.Add(giac);
        }
      }
      return dwr;
    }
  
    public void OM_Order_Create(object receiverInstance, C200153_OrderWrapper order)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_Create");
      parameters.Add(order.Code.Base64Encode());

      foreach (var cmp in order.CmpL)
      {
        parameters.Add(cmp.BatchCode.Base64Encode());
        parameters.Add(cmp.ArticleCode.Base64Encode());
        parameters.Add(cmp.QtyRequested.ToString());
        parameters.Add(cmp.CmpType.ToString());
      }
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_Create(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }
  
    public void OM_Order_GetAll_Paged(object receiverInstance, C200153_OrderFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_GetAll_Paged");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_GetAll_Paged(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Order> orderL = new List<C200153_Order>();
      dwr.Data = orderL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var order = OM_FromXmlTo_Order(item, ref startIndex);
          orderL.Add(order);
        }
      }
      return dwr;
    }
  
    public void OM_Order_Get(object receiverInstance, string orderCode, int orderId)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_Get");
      parameters.Add(orderCode.Base64Encode());
      parameters.Add(orderId.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_Get(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = OM_FromXmlTo_Order(item, ref startIndex);
      }
      return dwr;
    }
  
    public void OM_Order_UpdateState(object receiverInstance, int order_id, C200153_State newState)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_UpdateState");
      parameters.Add(order_id.ToString());
      parameters.Add(newState.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_UpdateState(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }
  
    public void OM_Order_Update(object receiverInstance, int order_id, int priotity, bool urgente, bool completamentoManuale)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_Update");
      parameters.Add(order_id.ToString());
      parameters.Add(priotity.ToString());
      parameters.Add(urgente ? "1" : "0");
      parameters.Add(completamentoManuale ? "1" : "0");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_Update(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }
  
    public void OM_OrderComponent_PalletInTransit(object receiverInstance, int lcpId)
    {
      var parameters = new List<string>();
      parameters.Add("OM_OrderComponent_PalletInTransit");
      parameters.Add(lcpId.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_OrderComponent_PalletInTransit(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_PalletInTransit> list = new List<C200153_PalletInTransit>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.getAllItem())
        {
          int startIndex = 1;
          var listItem = OM_FromXmlTo_PalletInTransit(item, ref startIndex);
          //20200831
          //listItem.PositionCode = item.getFieldVal<string>(startIndex++);
          list.Add(listItem);
        }
      }
      return dwr;
    }
  
    public void OM_Order_DestinationChange(object receiverInstance, string orderCode, string destinationCode)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_DestinationChange");
      parameters.Add(orderCode.Base64Encode());
      parameters.Add(destinationCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_DestinationChange(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_PalletInTransit> list = new List<C200153_PalletInTransit>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }

    #region COLLAUDO
    //OM_Order_CreateCollaudo
    public void OM_Order_CreateCollaudo(object receiverInstance, C200153_OrderWrapper order)
    {
      var parameters = new List<string>();
      parameters.Add("OM_Order_CreateCollaudo");
      parameters.Add(order.Code.Base64Encode());

      foreach (var cmp in order.CmpL)
      {
        parameters.Add(cmp.ArticleCode.Base64Encode());
        parameters.Add(cmp.QtyRequested.ToString());
      }
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_Order_CreateCollaudo(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var item = resp.Items[0];
        int startIndex = 1;
        dwr.Data = FromXmlTo_Result(item, ref startIndex);
      }
      return dwr;
    }
   
    public void OM_OrdersForCollaudo(object receiverInstance, int numOrders, int numCmps, int maxQty)
    {
      var parameters = new List<string>();
      parameters.Add("OM_OrdersForCollaudo");
      parameters.Add(numOrders.ToString());
      parameters.Add(numCmps.ToString());
      parameters.Add(maxQty.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_OrdersForCollaudo(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Order> orderL = new List<C200153_Order>();
      dwr.Data = orderL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var order = OM_FromXmlTo_Order(item, ref startIndex);
          orderL.Add(order);
        }
      }
      return dwr;
    }
  
    public void OM_OrdersForCollaudoReset(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("OM_OrdersForCollaudoReset");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel OM_OrdersForCollaudoReset(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      return dwr;
    }

    #endregion


    #region FromXmlTo

    private List<OrderManager.C200153_StockByArea> OM_FromXmlTo_StockByAreaList(XmlCommandResponse.Item item,ref int startIndex)
    {
      List<OrderManager.C200153_StockByArea> ret = new List<OrderManager.C200153_StockByArea>();
      while(startIndex<=item.Fields.Count)
      {
        ret.Add(OM_FromXmlTo_StockByArea(item, ref startIndex));
      }
      return ret;
    }
   
    private OrderManager.C200153_StockByArea OM_FromXmlTo_StockByArea(XmlCommandResponse.Item item,ref int startIndex)
    {
      OrderManager.C200153_StockByArea ret = new OrderManager.C200153_StockByArea()
      {
        AreaCode = item.getFieldVal<string>(startIndex++),
        Floor = item.getFieldVal<int>(startIndex++),
        NumPallet = item.getFieldVal<int>(startIndex++),
      };

      return ret;
    }

    private List<C200153_PalletInTransit> OM_FromXmlTo_PalletsInTransit(XmlCommandResponse.Item item, ref int startIndex)
    {
      List<C200153_PalletInTransit> ret = new List<C200153_PalletInTransit>();
      int count = item.getFieldVal<int>(startIndex++);
      for (int i = 0; i < count; i++)
      {
        var pt = OM_FromXmlTo_PalletInTransit(item, ref startIndex);
        ret.Add(pt);
      }
      return ret;
    }
  
    private C200153_PalletInTransit OM_FromXmlTo_PalletInTransit(XmlCommandResponse.Item item, ref int startIndex)
    {
      var pallet = OM_FromXmlTo_Pallet(item, ref startIndex);
      OrderManager.C200153_PalletInTransit ret = new OrderManager.C200153_PalletInTransit(pallet);
      ret.PositionCode = item.getFieldVal<string>(startIndex++);
      return ret;
    }

    private C200153_Pallet OM_FromXmlTo_Pallet(XmlCommandResponse.Item item, ref int startIndex)
    {
      OrderManager.C200153_Pallet ret = new OrderManager.C200153_Pallet();
      ret.Code = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.DateBorn = item.getFieldVal<DateTime>(startIndex++);
      ret.IsService = item.getFieldVal<bool>(startIndex++);
      ret.CustomResult = item.getFieldVal<C200153_CustomResult>(startIndex++);
      ret.IsReject = item.getFieldVal<bool>(startIndex++);
      ret.RejectResult = item.getFieldVal<Results_Enum>(startIndex++);
      ret.ProductionDate = item.getFieldVal<DateTime>(startIndex++);
      ret.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);
      ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.State = item.getFieldVal<C200153_Pallet_State>(startIndex++);
      ret.Days_Spent_FromProduction= item.getFieldVal<int>(startIndex++);
      ret.Days_to_Expiry= item.getFieldVal<int>(startIndex++);
      ret.FinalDestination= item.getFieldVal<string>(startIndex++);
      ret.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.DepositorCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      return ret;

    }
 
    private C200153_Article OM_FromXmlTo_Article(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_Article art = new C200153_Article();
      art.Code = item.getFieldVal<string>(startIndex++).Base64Decode();
      art.Descr = item.getFieldVal<string>(startIndex++).Base64Decode();
      art.StockWindow = item.getFieldVal<int>(startIndex++);
      art.FifoWindow = item.getFieldVal<int>(startIndex++);
      art.Turnover = item.getFieldVal<C200153_TurnoverEnum>(startIndex++);
      art.WrappingProgram = item.getFieldVal<int>(startIndex++);
      return art;
    }
 
    private C200153_Order OM_FromXmlTo_Order(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_Order order = new C200153_Order();
      OM_FromXmlSet_Order(order, item, ref startIndex);
      OM_FromXmlSet_OrderCmps(order, item, ref startIndex);
      var palletInTransitL = OM_FromXmlTo_PalletsInTransit(item, ref startIndex);
      order.PalletInTransitL.AddRange(palletInTransitL);
      return order;
    }

    private void OM_FromXmlSet_Order(C200153_Order ord, XmlCommandResponse.Item item, ref int startIndex)
    {
      ord.Id = item.getFieldVal<int>(startIndex++);
      ord.Code = item.getFieldVal<string>(startIndex++).Base64Decode();
      ord.Customer = item.getFieldVal<string>(startIndex++);
      ord.State = item.getFieldVal<C200153_State>(startIndex++);
      ord.ExecutionModality = item.getFieldVal<C200153_ExecutionModality>(startIndex++);
      ord.OrderType = item.getFieldVal<C200153_OrderType>(startIndex++);
      ord.DateBorn = item.getFieldVal<DateTime>(startIndex++);
      ord.DateSend = item.getFieldVal<DateTime>(startIndex++);
      ord.DateExec = item.getFieldVal<DateTime>(startIndex++);
      ord.DateEnd = item.getFieldVal<DateTime>(startIndex++);
      ord.Priority = item.getFieldVal<int>(startIndex++);
      ord.LastScanningDateTime = item.getFieldVal<DateTime>(startIndex++);
      ord.ExecutionResult= item.getFieldVal<C200153_ExecutionResult>(startIndex++);
      bool hasDestination = item.getFieldVal<bool>(startIndex++);
      if(hasDestination)
      {
        var destination = OM_FromXmlTo_ListDestination(item, ref startIndex);
        ord.Destination = destination;
      }
    }

    private void OM_FromXmlSet_OrderCmps(C200153_Order order, XmlCommandResponse.Item item, ref int startIndex)
    {
      int count = item.getFieldVal<int>(startIndex++);
      for (int i = 0; i < count; i++)
      {
        var cmp = OM_FromXmlTo_OrderCmp(item, ref startIndex);
        order.CmpL.Add(cmp);
      }
    }
 
    private C200153_OrderCmp OM_FromXmlTo_OrderCmp(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_OrderCmp ret = new C200153_OrderCmp();
      ret.Id = item.getFieldVal<int>(startIndex++);
      ret.State = item.getFieldVal<C200153_State>(startIndex++); 
      ret.BatchCode = item.getFieldVal<string>(startIndex++)?.Base64Decode();
      ret.ArticleCode = item.getFieldVal<string>(startIndex++)?.Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++)?.Base64Decode();
      //20200903
      ret.CmpType= item.getFieldVal<C200153_ListCmpTypeEnum>(startIndex++);
      //20200903
     
      ret.QtyRequested = item.getFieldVal<int>(startIndex++);
      ret.QtyDelivered = item.getFieldVal<int>(startIndex++);
      ret.NumPalletInTransit = item.getFieldVal<int>(startIndex++);
      ret.QtyRemaining = item.getFieldVal<int>(startIndex++);
      ret.ExecutionResult= item.getFieldVal<C200153_ExecutionResult>(startIndex++);
      ret.LastScanningDateTime = item.getFieldVal<DateTime>(startIndex++);
      return ret;
    }

    //20200820 aggiunto OM_FromXmlTo_ListDestination
    private C200153_ListDestination OM_FromXmlTo_ListDestination(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_ListDestination destination = new C200153_ListDestination();
      destination.Code = item.getFieldVal(startIndex++);
      destination.Description = item.getFieldVal(startIndex++);
      destination.IsDefault= item.getFieldVal<bool>(startIndex++);
      int posDestCount = item.getFieldVal<int>(startIndex++);
      for (int i = 0; i < posDestCount; i++)
      {
        destination.PositionList.Add(item.getFieldVal(startIndex++));
      }
      return destination;
    }

    #endregion

    #endregion

    #region ArticleManager

    public void AM_Article_GetAll(object receiverInstance, C200153_ArticleFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("AM_Article_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel AM_Article_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Article> list = new List<C200153_Article>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var listItem = RM_FromXmlTo_Article(item, ref startIndex);
          list.Add(listItem);
        }
      }
      return dwr;
    }

    public void AM_Article_Update(object receiverInstance, C200153_Article art)
    {
      var parameters = new List<string>();
      parameters.Add("AM_Article_Update");

      parameters.Add(art.Code?.Base64Encode());
      parameters.Add(art.Descr?.Base64Encode());
      parameters.Add(art.Turnover.ToString());
      parameters.Add(art.WrappingProgram.ToString());
      parameters.Add(art.IsDeleted.ToString());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel AM_Article_Update(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200153_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);
        dwr.Data = result;
      }
      return dwr;
    }

    public void AM_Article_Delete(object receiverInstance, C200153_Article art)
    {
      var parameters = new List<string>();
      parameters.Add("AM_Article_Delete");

      parameters.Add(art.Code?.Base64Encode());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel AM_Article_Delete(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200153_ArticleDeleteResults result = new C200153_ArticleDeleteResults();

        result.DeletableResult = resp.Items[0].getFieldVal<Results_Enum>(startIndex++);
        result.DeleteResult = resp.Items[0].getFieldVal<C200153_CustomResult>(startIndex++);

        dwr.Data = result;
      }
      return dwr;
    }


    public void AM_Article_Import(object receiverInstance, C200153_Article art)
    {
      var parameters = new List<string>();
      parameters.Add("AM_Article_Import");

      parameters.Add(art.Code?.Base64Encode());
      parameters.Add(art.Descr?.Base64Encode());
      parameters.Add(art.Turnover.ToString());
      parameters.Add(art.WrappingProgram.ToString());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel AM_Article_Import(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200153_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);
        dwr.Data = result;
      }
      return dwr;
    }

    #endregion

    #region DepositorManager

    public void DM_Depositor_GetAll(object receiverInstance, C200153_DepositorFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("DM_Depositor_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel DM_Depositor_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Depositor> list = new List<C200153_Depositor>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var listItem = DM_FromXmlTo_Depositor(item, ref startIndex);
          list.Add(listItem);
        }
      }
      return dwr;
    }

    public void DM_Depositor_Update(object receiverInstance, C200153_Depositor dep)
    {
      var parameters = new List<string>();
      parameters.Add("DM_Depositor_Update");

      parameters.Add(dep.Code?.Base64Encode());
      parameters.Add(dep.Address?.Base64Encode());
      parameters.Add(dep.IsDeleted.ToString());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel DM_Depositor_Update(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200153_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);
        dwr.Data = result;
      }
      return dwr;
    }

    public void DM_Depositor_Delete(object receiverInstance, C200153_Depositor dep)
    {
      var parameters = new List<string>();
      parameters.Add("DM_Depositor_Delete");

      parameters.Add(dep.Code?.Base64Encode());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel DM_Depositor_Delete(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200153_DepositorDeleteResults result = new C200153_DepositorDeleteResults();

        result.DeletableResult = resp.Items[0].getFieldVal<Results_Enum>(startIndex++);
        result.DeleteResult = resp.Items[0].getFieldVal<C200153_CustomResult>(startIndex++);
        dwr.Data = result;
      }
      return dwr;
    }


    public void DM_Depositor_Import(object receiverInstance, C200153_Depositor dep)
    {
      var parameters = new List<string>();
      parameters.Add("DM_Depositor_Import");

      parameters.Add(dep.Code?.Base64Encode());
      parameters.Add(dep.Address?.Base64Encode());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel DM_Depositor_Import(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200153_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);
        dwr.Data = result;
      }
      return dwr;
    }

    #region Utility
    private C200153_Depositor DM_FromXmlTo_Depositor(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_Depositor ret = new C200153_Depositor();
      ret.Code = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.Address = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.IsDeleted = item.getFieldVal<bool>(startIndex++);
      return ret;

    }
    #endregion

    #endregion

    #region PALLET MANAGER
    public void PM_Article_GetAll(object receiverInstance, C200153_PalletArticleFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Article_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Article_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Article> list = new List<C200153_Article>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var listItem = PM_FromXmlTo_Article(item, ref startIndex);
          list.Add(listItem);
        }
      }
      return dwr;
    }

    public void PM_Depositor_GetAll(object receiverInstance, C200153_PalletDepositorFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Depositor_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Depositor_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Depositor> depositors = new List<C200153_Depositor>();
      dwr.Data = depositors;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var depositor = PM_FromXmlTo_Depositor(item, ref startIndex);

          depositors.Add(depositor);
        }
      }
      return dwr;
    }

    public void PM_Pallet_GetAll(object receiverInstance, C200153_PalletManagerAllFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Pallet_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Pallet_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_PalletManagerAll> pallets = new List<C200153_PalletManagerAll>();
      dwr.Data = pallets;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var pallet = PM_FromXmlTo_PalletAll(item, ref startIndex);

          pallets.Add(pallet);
        }
      }
      return dwr;
    }

    public void PM_Get_CheckInCodes(object receiverInstance, IList<string> posCodesFromClient)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Get_CheckInCodes");
      parameters.AddRange(posCodesFromClient);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Get_CheckInCodes(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<string> list = new List<string>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          list.Add(item.getFieldVal<string>(startIndex));
        }
      }
      return dwr;
    }

    public void PM_Pallet_Register(object receiverInstance, C200153_PalletInsert pal)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Pallet_Register");

      parameters.Add(((int)pal.PalletTypeId).ToString());
      parameters.Add(pal.ArticleCode.ToString().Base64Encode());
      parameters.Add(pal.BatchCode.ToString().Base64Encode());
      parameters.Add(pal.BatchLength.ToString());
      parameters.Add(pal.DepositorCode.ToString().Base64Encode());
      parameters.Add(pal.ProductionDate.ConvertToDateTimeFormatString() ?? "");
      parameters.Add(pal.ExpiryDate.ConvertToDateTimeFormatString() ?? "");
      parameters.Add(pal.IsWrapped.ToString());
      parameters.Add(pal.IsLabeled.ToString());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Pallet_Register(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200153_PalletInsertResult InsertResult = new C200153_PalletInsertResult();

        InsertResult.Result = FromXmlTo_Result(resp.Items[0], ref startIndex);


        if (InsertResult.Result == C200153_CustomResult.OK)
        {
          InsertResult.Code = resp.Items[0].getFieldVal<string>(startIndex++);
        }

        dwr.Data = InsertResult;
      }
      return dwr;
    }

    public void PM_Article_CheckPresence(object receiverInstance, string palArtCode)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Article_CheckPresence");

      parameters.Add(palArtCode.ToString().Base64Encode());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Article_CheckPresence(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200153_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);

        if (result == C200153_CustomResult.OK && (resp.itemCount > 1))
        {
          startIndex = 1;
          C200153_Article art = new C200153_Article();
          art = PM_FromXmlTo_Article(resp.Items[1], ref startIndex);
          dwr.Data = art;
        }

      }
      return dwr;
    }

    public void PM_Depositor_CheckPresence(object receiverInstance, string palDepCode)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Depositor_CheckPresence");

      parameters.Add(palDepCode.ToString().Base64Encode());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Depositor_CheckPresence(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200153_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);

        if (result == C200153_CustomResult.OK && (resp.itemCount > 1))
        {
          startIndex = 1;
          C200153_Depositor dep = new C200153_Depositor();
          dep = PM_FromXmlTo_Depositor(resp.Items[1], ref startIndex);
          dwr.Data = dep;
        }

      }
      return dwr;
    }

    public void PM_Pallet_Update(object receiverInstance, C200153_PalletInsert pal)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Pallet_Update");

      parameters.Add(pal.Code.ToString().Base64Encode());
      parameters.Add(((int)pal.PalletTypeId).ToString());
      parameters.Add(pal.ArticleCode.ToString().Base64Encode());
      parameters.Add(pal.BatchCode.ToString().Base64Encode());
      parameters.Add(pal.BatchLength.ToString());
      parameters.Add(pal.DepositorCode.ToString().Base64Encode());
      parameters.Add(pal.ProductionDate.ConvertToDateTimeFormatString() ?? "");
      parameters.Add(pal.ExpiryDate.ConvertToDateTimeFormatString() ?? "");
      parameters.Add(pal.IsWrapped.ToString());
      parameters.Add(pal.IsLabeled.ToString());

      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Pallet_Update(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200153_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);
        dwr.Result = result.ToString();

        if (resp.itemCount > 1 && (result == C200153_CustomResult.OK))
        {
          startIndex = 1;

          dwr.Data = resp.Items[1].getFieldVal<string>(startIndex++).Base64Decode();
        }
      }
      return dwr;
    }

    public void PM_Check_Pallet(object receiverInstance, string palCode)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Check_Pallet");
      parameters.Add(palCode.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Check_Pallet(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200153_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);
        dwr.Result = result.ToString();

        if (resp.itemCount > 1)
        {
          startIndex = 1;
          var itemPallet = resp.Items[1];
          C200153_PalletInsert pallet = new C200153_PalletInsert();
          dwr.Data = pallet;
          pallet.Code = itemPallet.getFieldVal<string>(startIndex++).Base64Decode();
          pallet.PalletTypeId = itemPallet.getFieldVal<C200153_PalletType>(startIndex++);
          pallet.ArticleCode = itemPallet.getFieldVal(startIndex++).Base64Decode();
          pallet.ProductionDate = itemPallet.getFieldVal<DateTime>(startIndex++);
          pallet.ExpiryDate = itemPallet.getFieldVal<DateTime>(startIndex++);
          pallet.IsLabeled = itemPallet.getFieldVal<bool>(startIndex++);
          pallet.IsWrapped = itemPallet.getFieldVal<bool>(startIndex++);
          pallet.BatchCode = itemPallet.getFieldVal<string>(startIndex++).Base64Decode();
          pallet.DepositorCode = itemPallet.getFieldVal<string>(startIndex++).Base64Decode();
        }
      }
      return dwr;
    }

    public void PM_Print(object receiverInstance, string positionCode, string palletCode)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Print");
      parameters.AddRange(new string[] { positionCode, palletCode });
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Print(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        var itemResult = resp.Items[0];
        int startIndex = 1;
        dwr.Result = itemResult.getFieldVal(startIndex++).ToString();
      }
      return dwr;
    }

    public void PM_Pallet_Print_GetAll(object receiverInstance, C200153_PalletPrintFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Pallet_Print_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel PM_Pallet_Print_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_PalletManagerAll> pallets = new List<C200153_PalletManagerAll>();
      dwr.Data = pallets;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var pallet = PM_FromXmlTo_PalletAll(item, ref startIndex);

          pallets.Add(pallet);
        }
      }
      return dwr;
    }

    public void PM_Pallet_CheckIn(object receiverInstance, string posCode, string palletCode)
    {
      var parameters = new List<string>();
      parameters.Add("PM_Pallet_CheckIn");
      parameters.Add(posCode);
      parameters.Add(palletCode.ToString().Base64Encode());
      Plugin_Command(receiverInstance, parameters);

    }
    private IModel PM_Pallet_CheckIn(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      if (resp.itemCount > 0)
      {
        int startIndex = 1;
        C200153_CustomResult result = FromXmlTo_Result(resp.Items[0], ref startIndex);
        dwr.Data = result;
      }
      return dwr;
    }

    #region From xml to
    private C200153_Article PM_FromXmlTo_Article(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_Article art = new C200153_Article();
      art.Code = item.getFieldVal<string>(startIndex++)?.Base64Decode();
      art.Descr = item.getFieldVal<string>(startIndex++).Base64Decode();
      art.StockWindow = item.getFieldVal<int>(startIndex++);
      art.FifoWindow = item.getFieldVal<int>(startIndex++);
      art.Turnover = item.getFieldVal<C200153_TurnoverEnum>(startIndex++);
      art.WrappingProgram = item.getFieldVal<int>(startIndex++);

      return art;

    }

    private C200153_Depositor PM_FromXmlTo_Depositor(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_Depositor ret = new C200153_Depositor();
      ret.Code = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.Address = item.getFieldVal<string>(startIndex++).Base64Decode();
      return ret;

    }

    private C200153_PalletManagerAll PM_FromXmlTo_PalletAll(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_PalletManagerAll ret = new C200153_PalletManagerAll();
      ret.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.PalletTypeId = (C200153_PalletType)item.getFieldVal<int>(startIndex++);
      ret.DateBorn = item.getFieldVal<DateTime>(startIndex++);
      ret.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.BatchLength = item.getFieldVal<int>(startIndex++);
      ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ProductionDate = item.getFieldVal<DateTime>(startIndex++);
      ret.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);
      ret.PositionCode = item.getFieldVal<string>(startIndex++);
      ret.DepositorCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.IsWrapped = item.getFieldVal<bool>(startIndex++);
      ret.IsLabeled = item.getFieldVal<bool>(startIndex++);
      return ret;

    }


    #endregion

    #endregion

    #region WHSTATUS
    public void RM_Wh_Status(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Wh_Status");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Wh_Status(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<WhStatus.C200153_WhStatus> list = new List<WhStatus.C200153_WhStatus>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          WhStatus.C200153_WhStatus whs = new WhStatus.C200153_WhStatus();
          whs.Code = item.getFieldVal<string>(startIndex++);
          whs.CellsCount = item.getFieldVal<int>(startIndex++);
          whs.NumTotFullCells = item.getFieldVal<int>(startIndex++);
          whs.NumTotEmptyCells = item.getFieldVal<int>(startIndex++);
          whs.NumTotHalfFullCells = item.getFieldVal<int>(startIndex++);
          whs.NumTotBlockedCells = item.getFieldVal<int>(startIndex++);
          whs.Capacity = item.getFieldVal<int>(startIndex++);
          whs.NumTotPallet = item.getFieldVal<int>(startIndex++);
          list.Add(whs);
        }
      }
      return dwr;
    }

    public void RM_Wh_MasterStatus(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Wh_MasterStatus");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Wh_MasterStatus(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<WhStatus.C200153_MasterStatus> list = new List<WhStatus.C200153_MasterStatus>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          WhStatus.C200153_MasterStatus master = new WhStatus.C200153_MasterStatus();
          master.Code = item.getFieldVal<string>(startIndex++);
          master.Status = item.getFieldVal<WhStatus.C200153_SatelliteStatusEnum>(startIndex++);
          master.IsOk = item.getFieldVal<bool>(startIndex++);
          master.IsMaintenaceRequest = item.getFieldVal<bool>(startIndex++);
          list.Add(master);
        }
      }
      return dwr;
    }

    public void RM_Wh_SatelliteStatus(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Wh_SatelliteStatus");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Wh_SatelliteStatus(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<WhStatus.C200153_SlaveStatus> list = new List<WhStatus.C200153_SlaveStatus>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          WhStatus.C200153_SlaveStatus satellite = new WhStatus.C200153_SlaveStatus();
          satellite.Code = item.getFieldVal<string>(startIndex++);
          satellite.Status = item.getFieldVal<WhStatus.C200153_SatelliteStatusEnum>(startIndex++);
          satellite.IsOk = item.getFieldVal<bool>(startIndex++);
          satellite.BatteryLevel = item.getFieldVal<int>(startIndex++);
          satellite.Wifi = item.getFieldVal<bool>(startIndex++);
          list.Add(satellite);
        }
      }
      return dwr;
    }

    public void RM_Wh_Traffic(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Wh_Traffic");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Wh_Traffic(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<WhStatus.C200153_WhTraffic> list = new List<WhStatus.C200153_WhTraffic>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          if (item.Count() > 0)
          {
            int startIndex = 1;
            WhStatus.C200153_WhTraffic whs = new WhStatus.C200153_WhTraffic();

            whs.Area = item.getFieldVal<string>(startIndex++);
            whs.Floor = item.getFieldVal<string>(startIndex++);
            whs.PalletCount = item.getFieldVal<int>(startIndex++);

            list.Add(whs);
          }
        }
      }
      return dwr;
    }

    public void RM_Exit_Traffic(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Exit_Traffic");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Exit_Traffic(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<WhStatus.C200153_WhTrafficExit> list = new List<WhStatus.C200153_WhTrafficExit>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          if (item.Count() > 0)
          {
            int startIndex = 1;
            WhStatus.C200153_WhTrafficExit whs = new WhStatus.C200153_WhTrafficExit();

            whs.Code = item.getFieldVal<string>(startIndex++);
            whs.PalletCount = item.getFieldVal<int>(startIndex++);

            list.Add(whs);
          }
        }
      }
      return dwr;
    }

    #endregion

    #region EXIT POSITION

    public void EP_Get_ExitPositionCodes(object receiverInstance, IList<string> posCodesFromClient)
    {
      var parameters = new List<string>();
      parameters.Add("EP_Get_ExitPositionCodes");
      parameters.AddRange(posCodesFromClient);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel EP_Get_ExitPositionCodes(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<string> list = new List<string>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          list.Add(item.getFieldVal<string>(startIndex));
        }
      }
      return dwr;
    }

    public void EP_Get_PalletInPos(object receiverInstance, string positionCode)
    {
      var parameters = new List<string>();
      parameters.Add("EP_Get_PalletInPos");
      parameters.Add(positionCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel EP_Get_PalletInPos(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_PositionOut_Pallet> orderL = new List<C200153_PositionOut_Pallet>();
      dwr.Data = orderL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var pallet = EP_FromXmlTo_Pallet(item, ref startIndex);
          orderL.Add(pallet);
        }
      }
      return dwr;
    }

    #region FromXmlTo
    private C200153_PositionOut_Pallet EP_FromXmlTo_Pallet(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_PositionOut_Pallet ret = new C200153_PositionOut_Pallet();
      if (ret != null)
      {
        ret.Code = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.ArticleCode= item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.ArticleDescr= item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.Height = item.getFieldVal<int>(startIndex++);
        ret.Weight = item.getFieldVal<int>(startIndex++);
        ret.IsService = item.getFieldVal<bool>(startIndex++);
        ret.CustomResult= item.getFieldVal<C200153_CustomResult>(startIndex++);
        ret.IsReject  = item.getFieldVal<bool>(startIndex++);
        ret.RejectResult = item.getFieldVal<Results_Enum>(startIndex++);
        ret.HasOrder = item.getFieldVal<bool>(startIndex++);
        if(ret.HasOrder)
        {
          ret.OrderCode = item.getFieldVal<string>(startIndex++)?.Base64Decode();
          ret.OrderCustomer= item.getFieldVal<string>(startIndex++)?.Base64Decode();

          ret.OrderCmpArticleCode= item.getFieldVal<string>(startIndex++)?.Base64Decode();
          ret.OrderCmpArticleDescr= item.getFieldVal<string>(startIndex++)?.Base64Decode();
          ret.OrderCmpQtyRequested= item.getFieldVal<int>(startIndex++);
          ret.OrderCmpQuantityDelivered = item.getFieldVal<int>(startIndex++);
        }
        ret.IsLabeled = item.getFieldVal<bool>(startIndex++);
        ret.IsWrapped = item.getFieldVal<bool>(startIndex++);

        ret.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.BatchLength = item.getFieldVal<int>(startIndex++);
        ret.DepositorCode = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.DepositorAddress = item.getFieldVal<string>(startIndex++).Base64Decode();

        ret.ProductionDate = item.getFieldVal<DateTime>(startIndex++);
        ret.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);
      }
      return ret;
    }
    #endregion

    #endregion

    #region Position CheckIn
   
    public void IP_Get_InputPositionCodes(object receiverInstance, IList<string> posCodesFromClient)
    {
      var parameters = new List<string>();
      parameters.Add("IP_Get_InputPositionCodes");
      parameters.AddRange(posCodesFromClient);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel IP_Get_InputPositionCodes(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<string> list = new List<string>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          list.Add(item.getFieldVal<string>(startIndex));
        }
      }
      return dwr;
    }

    public void IP_Get_PalletInPos(object receiverInstance, string posCode)
    {
      var parameters = new List<string>();
      parameters.Add("IP_Get_PalletInPos");
      parameters.Add(posCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel IP_Get_PalletInPos(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_PositionShapeControl_Pallet> list = new List<C200153_PositionShapeControl_Pallet>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200153_PositionShapeControl_Pallet pallet = IP_FromXmlTo_PosPallet(item, ref startIndex);
          list.Add(pallet);
        }
      }
      return dwr;
    }
  
    
   
    #region from xml to
    private C200153_PositionShapeControl_Pallet IP_FromXmlTo_PosPallet(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_PositionShapeControl_Pallet ret = new C200153_PositionShapeControl_Pallet();

      if (ret != null)
      {
        ret.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.IsService = item.getFieldVal<bool>(startIndex++);
        ret.CustomResult = item.getFieldVal<C200153_CustomResult>(startIndex++);
        ret.IsReject = item.getFieldVal<bool>(startIndex++);
        ret.RejectResult = item.getFieldVal<Results_Enum>(startIndex++);
        ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.Weight = item.getFieldVal<int>(startIndex++);
        ret.IsLabeled = item.getFieldVal<bool>(startIndex++);
        ret.IsWrapped = item.getFieldVal<bool>(startIndex++);
        ret.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.BatchLength = item.getFieldVal<int>(startIndex++);
        ret.DepositorCode = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.DepositorAddress = item.getFieldVal<string>(startIndex++).Base64Decode();
        ret.ProductionDate = item.getFieldVal<DateTime>(startIndex++);
        ret.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);
      }
      return ret;
    }
 

    #endregion

    #endregion

    #region REPORT MANAGER

    #region Activities
    public void RM_Jobs_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Jobs_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Jobs_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      Dictionary<string, int> dic = resp.getAllItem().ToDictionary(item => item.getFieldVal<string>(1), item => item.getFieldVal<int>(2));
      dwr.Data = dic;
      return dwr;
    }
    #endregion

    #region Performance
    public void RM_Performance(object receiverInstance, DateTime dateStart)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Performance");
      parameters.Add(dateStart.ConvertToDateTimeFormatString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Performance(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Performance> list = new List<C200153_Performance>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200153_Performance pr = new C200153_Performance();
          pr.DateStart = item.getFieldVal<DateTime>(startIndex++);
          pr.DateEnd = item.getFieldVal<DateTime>(startIndex++);
          pr.Input = item.getFieldVal<int>(startIndex++);
          pr.Reject = item.getFieldVal<int>(startIndex++);
          pr.Warehouse = item.getFieldVal<int>(startIndex++);
          pr.Output = item.getFieldVal<int>(startIndex++);
          pr.Exit = item.getFieldVal<int>(startIndex++);
          pr.Wh1In = item.getFieldVal<int>(startIndex++);
          pr.Wh2In = item.getFieldVal<int>(startIndex++);
          pr.Wh3In = item.getFieldVal<int>(startIndex++);
          pr.Wh1Out = item.getFieldVal<int>(startIndex++);
          pr.Wh2Out = item.getFieldVal<int>(startIndex++);
          pr.Wh3Out = item.getFieldVal<int>(startIndex++);
          list.Add(pr);
        }
      }
      return dwr;
    }
    public void RM_PerformanceByInterval(object receiverInstance, DateTime dateStart, DateTime dateEnd)
    {
      var parameters = new List<string>();
      parameters.Add("RM_PerformanceByInterval");
      parameters.AddRange(new string[] { dateStart.ConvertToDateTimeFormatString(), dateEnd.ConvertToDateTimeFormatString() });
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_PerformanceByInterval(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Performance> list = new List<C200153_Performance>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200153_Performance pr = new C200153_Performance();
          pr.DateStart = item.getFieldVal<DateTime>(startIndex++);
          pr.DateEnd = item.getFieldVal<DateTime>(startIndex++);
          pr.Input = item.getFieldVal<int>(startIndex++);
          pr.Reject = item.getFieldVal<int>(startIndex++);
          pr.Warehouse = item.getFieldVal<int>(startIndex++);
          pr.Output = item.getFieldVal<int>(startIndex++);
          pr.Exit = item.getFieldVal<int>(startIndex++);
          pr.Wh1In = item.getFieldVal<int>(startIndex++);
          pr.Wh2In = item.getFieldVal<int>(startIndex++);
          pr.Wh3In = item.getFieldVal<int>(startIndex++);
          pr.Wh1Out = item.getFieldVal<int>(startIndex++);
          pr.Wh2Out = item.getFieldVal<int>(startIndex++);
          pr.Wh3Out = item.getFieldVal<int>(startIndex++);
          list.Add(pr);
        }
      }
      return dwr;
    }
    #endregion
    
    #region Stock

    public void RM_StockByCell_GetAll(object receiverInstance, C200153_StockByCellFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_StockByCell_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_StockByCell_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_StockByCell> giacL = new List<C200153_StockByCell>();
      dwr.Data = giacL;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200153_StockByCell g = new C200153_StockByCell();
          g.CellCode = item.getFieldVal<string>(startIndex++);
          g.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          g.DepositorCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          g.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          g.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
          g.FinestraStoccaggio = item.getFieldVal<int>(startIndex++);
          g.MaxPalletAllowed = item.getFieldVal<int>(startIndex++);
          g.PalletCount = item.getFieldVal<int>(startIndex++);
          g.PalletFirstProductionDate = item.getFieldVal<DateTime>(startIndex++);
          g.PalletLastProductionDate = item.getFieldVal<DateTime>(startIndex++);
          g.NumDays = item.getFieldVal<int>(startIndex++);
          giacL.Add(g);
        }
      }
      return dwr;
    }

    public void RM_Stock_Pallet_ByArea(object receiverInstance, C200153_StockPalletFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Stock_Pallet_ByArea");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Stock_Pallet_ByArea(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_StockPallet> pallets = new List<C200153_StockPallet>();
      dwr.Data = pallets;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var pallet =RM_FromXmlTo_StockPallet(item, ref startIndex);
          pallets.Add(pallet);
        }
      }
      return dwr;
    }

    public void RM_Stock_BatchArticle(object receiverInstance, C200153_StockArticleBatchFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Stock_Article_ByArea");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Stock_BatchArticle(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_StockArticleBatch> giacL = new List<C200153_StockArticleBatch>();
      dwr.Data = giacL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var pallet = RM_FromXmlTo_StockBatchArticle(item, ref startIndex);
          giacL.Add(pallet);
        }
      }
      return dwr;
    }

    public void RM_Stock_BatchArticleDepositor(object receiverInstance, C200153_StockArticleBatchFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Stock_Article_ByArea");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Stock_BatchArticleDepositor(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_StockArticleBatch> giacL = new List<C200153_StockArticleBatch>();
      dwr.Data = giacL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var pallet = RM_FromXmlTo_StockBatchArticle(item, ref startIndex);
          giacL.Add(pallet);
        }
      }
      return dwr;
    }

    public void RM_GetAllAreas(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_GetAllAreas");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_GetAllAreas(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_WhArea> areaL = new List<C200153_WhArea>();
      dwr.Data = areaL;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          int startIndex = 1;
          var item = resp.Items[i];
          C200153_WhArea area = new C200153_WhArea();
          area.Code = item.getFieldVal<string>(startIndex++);
          area.WhAreaType = item.getFieldVal<C200153_WhAreaType>(startIndex++);
          areaL.Add(area);
        }
      }
      return dwr;
    }

    #endregion

    #region Database
    public void RM_Article_GetAll(object receiverInstance, C200153_ArticleFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Article_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Article_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Article> list = new List<C200153_Article>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var listItem = RM_FromXmlTo_Article(item, ref startIndex);
          list.Add(listItem);
        }
      }
      return dwr;
    }

    public void RM_Pallet_GetAll(object receiverInstance, C200153_PalletAllFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Pallet_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Pallet_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_PalletAll> pallets = new List<C200153_PalletAll>();
      dwr.Data = pallets;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var pallet = RM_FromXmlTo_PalletAll(item, ref startIndex);

          pallets.Add(pallet);
        }
      }
      return dwr;
    }

    public void RM_Depositor_GetAll(object receiverInstance, C200153_DepositorFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Depositor_GetAll");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Depositor_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Depositor> depositors = new List<C200153_Depositor>();
      dwr.Data = depositors;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          var depositor = RM_FromXmlTo_Depositor(item, ref startIndex);

          depositors.Add(depositor);
        }
      }
      return dwr;
    }


    #endregion

    #region Pallet In
    public void RM_PalletIn(object receiverInstance, C200153_PalletInFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_PalletIn");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_PalletIn(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_PalletIn> list = new List<C200153_PalletIn>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200153_PalletIn pr = new C200153_PalletIn();
          pr.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.Position = item.getFieldVal<string>(startIndex++);
          pr.DateBorn = item.getFieldVal<DateTime>(startIndex++);
          pr.CellCode = item.getFieldVal<string>(startIndex++);
          pr.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.IsLabeled = item.getFieldVal<bool>(startIndex++);
          pr.IsWrapped = item.getFieldVal<bool>(startIndex++);
          pr.DepositorCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.ProductionDate = item.getFieldVal<DateTime>(startIndex++);
          pr.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);

          list.Add(pr);
        }
      }
      return dwr;
    }

    public void RM_InputPositions_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_InputPositions_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_InputPositions_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Position> list = new List<C200153_Position>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          C200153_Position position = new C200153_Position();

          int startIndex = 1;
          position.Code = item.getFieldVal<string>(startIndex++);
          position.Description = item.getFieldVal<string>(startIndex++);

          list.Add(position);
        }
      }
      return dwr;
    }

    #endregion

    #region Pallet Out
    public void RM_PalletOut(object receiverInstance, C200153_PalletOutFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_PalletOut");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_PalletOut(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_PalletOut> list = new List<C200153_PalletOut>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200153_PalletOut pr = new C200153_PalletOut();
          pr.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.OrderCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.Position = item.getFieldVal<string>(startIndex++);
          pr.DateBorn = item.getFieldVal<DateTime>(startIndex++);
          pr.CellCode = item.getFieldVal<string>(startIndex++);
          pr.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.IsLabeled = item.getFieldVal<bool>(startIndex++);
          pr.IsWrapped = item.getFieldVal<bool>(startIndex++);
          pr.DepositorCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.ProductionDate = item.getFieldVal<DateTime>(startIndex++);
          pr.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);

          list.Add(pr);
        }
      }
      return dwr;
    }

    public void RM_ListDestinationPositions_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_ListDestinationPositions_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_ListDestinationPositions_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Position> list = new List<C200153_Position>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          C200153_Position position = new C200153_Position();

          int startIndex = 1;
          position.Code = item.getFieldVal<string>(startIndex++);
          position.Description = item.getFieldVal<string>(startIndex++);

          list.Add(position);
        }
      }
      return dwr;
    }
    #endregion

    #region Pallet Reject
    public void RM_PalletReject(object receiverInstance, C200153_PalletRejectFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_PalletReject");
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_PalletReject(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_PalletReject> list = new List<C200153_PalletReject>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200153_PalletReject pr = new C200153_PalletReject();
          pr.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.Position = item.getFieldVal<string>(startIndex++);
          pr.Reason = item.getFieldVal<string>(startIndex++);
          pr.DateBorn = item.getFieldVal<DateTime>(startIndex++);
          pr.CellCode = item.getFieldVal<string>(startIndex++);
          pr.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.IsLabeled = item.getFieldVal<bool>(startIndex++);
          pr.IsWrapped = item.getFieldVal<bool>(startIndex++);
          pr.DepositorCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          pr.ProductionDate = item.getFieldVal<DateTime>(startIndex++);
          pr.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);
          list.Add(pr);
        }
      }
      return dwr;
    }


    public void RM_ExitPositions_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_ExitPositions_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_ExitPositions_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Position> list = new List<C200153_Position>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          C200153_Position position = new C200153_Position();

          int startIndex = 1;
          position.Code = item.getFieldVal<string>(startIndex++);
          position.Description = item.getFieldVal<string>(startIndex++);

          list.Add(position);
        }
      }
      return dwr;
    }
    #endregion

    #region Distribution

    public void RM_Pallet_Flow(object receiverInstance, C200153_DistributionPalletFlowFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Pallet_Flow");
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Pallet_Flow(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      C200153_DistributionPalletFlow palletFlow = new C200153_DistributionPalletFlow();
      dwr.Data = palletFlow;

      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          switch (item.getFieldVal<string>(startIndex++))
          {
            case "Input":
              C200153_PalletIn pi = new C200153_PalletIn();
              pi.DateBorn = item.getFieldVal<DateTime>(startIndex++);
              pi.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pi.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pi.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pi.DepositorCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pi.Position = item.getFieldVal<string>(startIndex++);
              
              palletFlow.listIn.Add(pi);

              break;

            case "Output":
              C200153_PalletOut po = new C200153_PalletOut();
              po.DateBorn = item.getFieldVal<DateTime>(startIndex++);
              po.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              po.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              po.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              po.DepositorCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              po.Position = item.getFieldVal<string>(startIndex++);

              palletFlow.listOut.Add(po);

              break;

            case "Reject":
              C200153_PalletReject pr = new C200153_PalletReject();
              pr.DateBorn = item.getFieldVal<DateTime>(startIndex++);
              pr.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pr.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pr.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pr.DepositorCode = item.getFieldVal<string>(startIndex++).Base64Decode();
              pr.Position = item.getFieldVal<string>(startIndex++);

              palletFlow.listReject.Add(pr);

              break;
          }


        }
      }
      return dwr;
    }

    public void RM_Distribution_Pallet_GetLocal(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Distribution_Pallet_GetLocal");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Distribution_Pallet_GetLocal(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_DistributionPallet> list = new List<C200153_DistributionPallet>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          var d = RM_FromXmlTo_DistributionPallet(item, ref startIndex);
          list.Add(d);
        }
      }
      return dwr;
    }

    public void RM_Distribution_ArticleBatch_ByLocalArea(object receiverInstance, C200153_DistributionArticleBatchFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Distribution_ArticleBatch_ByLocalArea");
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Distribution_ArticleBatch_ByLocalArea(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_DistributionArticleBatchByLocalArea> list = new List<C200153_DistributionArticleBatchByLocalArea>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200153_DistributionArticleBatchByLocalArea d = new C200153_DistributionArticleBatchByLocalArea();
          d.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          d.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          d.AreaCode = item.getFieldVal<string>(startIndex++);
          d.Floor = item.getFieldVal<int>(startIndex++);
          d.NumPallet = item.getFieldVal<int>(startIndex++);

          list.Add(d);
        }
      }
      return dwr;
    }

    public void RM_Distribution_ArticleBatch_ByMainArea(object receiverInstance, C200153_DistributionArticleBatchFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Distribution_ArticleBatch_ByMainArea");
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Distribution_ArticleBatch_ByMainArea(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_DistributionArticleBatchByMainArea> list = new List<C200153_DistributionArticleBatchByMainArea>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200153_DistributionArticleBatchByMainArea d = new C200153_DistributionArticleBatchByMainArea();
          d.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          d.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
          d.AreaCode = item.getFieldVal<string>(startIndex++);
          d.NumPallet = item.getFieldVal<int>(startIndex++);

          list.Add(d);
        }
      }
      return dwr;
    }

    public void RM_Distribution_StoricFlowWhIn(object receiverInstance, C200153_Distribution_Storic_Filter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Distribution_StoricFlowWhIn");
      parameters.AddRange(filter.GetXmlCommandParameters());
      /*
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());

      parameters.Add((filter.AreaCode ?? "").ToString());
      parameters.Add((filter.Floor).ToString());       
      parameters.Add(filter.DateRangeSelected ? filter.DateBornMin.ConvertToDateTimeFormatString() : DateTime.MinValue.ConvertToDateTimeFormatString());
      parameters.Add(filter.DateRangeSelected ? filter.DateBornMax.ConvertToDateTimeFormatString() : DateTime.MinValue.ConvertToDateTimeFormatString()); 
      parameters.Add((filter.ArticleCode ?? "").ToString().Base64Encode());
      */
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Distribution_StoricFlowWhIn(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Distribution_StoricFlowWhIn> list = new List<C200153_Distribution_StoricFlowWhIn>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200153_Distribution_StoricFlowWhIn d = new C200153_Distribution_StoricFlowWhIn();

          d.AreaCode = item.getFieldVal<string>(startIndex++);
          d.NumPallet = item.getFieldVal<int>(startIndex++);
          d.AreaCodeFull = item.getFieldVal<string>(startIndex++);
          d.WhFloor = item.getFieldVal<int>(startIndex++);

          list.Add(d);
        }
      }
      return dwr;
    }

    public void RM_Distribution_StoricFlowArticleWhIn(object receiverInstance, C200153_Distribution_Storic_Filter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Distribution_StoricFlowArticleWhIn");
      parameters.AddRange(filter.GetXmlCommandParameters());

      /*
      parameters.Add(filter.Index.ToString());
      parameters.Add((filter.MaxItems).ToString());
      parameters.Add((filter.AreaCode ?? "").ToString());
      parameters.Add((filter.Floor).ToString());
      parameters.Add(filter.DateRangeSelected ? filter.DateBornMin.ConvertToDateTimeFormatString() : DateTime.MinValue.ConvertToDateTimeFormatString());
      parameters.Add(filter.DateRangeSelected ? filter.DateBornMax.ConvertToDateTimeFormatString() : DateTime.MinValue.ConvertToDateTimeFormatString());
      parameters.Add((filter.ArticleCode ?? "").ToString().Base64Encode());
      */


      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Distribution_StoricFlowArticleWhIn(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_Distribution_StoricFlowArticleWhIn> list = new List<C200153_Distribution_StoricFlowArticleWhIn>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          C200153_Distribution_StoricFlowArticleWhIn d = new C200153_Distribution_StoricFlowArticleWhIn();

          d.AreaCode = item.getFieldVal<string>(startIndex++);
          d.NumPallet = item.getFieldVal<int>(startIndex++);
          d.AreaCodeFull = item.getFieldVal<string>(startIndex++);
          d.WhFloor = item.getFieldVal<int>(startIndex++);
          d.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();

          list.Add(d);
        }
      }
      return dwr;
    }
    #endregion

    #region Traffic Panel

    public void RM_Pallet_Traffic_GetAll(object receiverInstance, C200153_TrafficBoardTaskFilter filter)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Pallet_Traffic_GetAll");
      parameters.AddRange(filter.GetXmlCommandParameters());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel RM_Pallet_Traffic_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200153_PalletTraffic> list = new List<C200153_PalletTraffic>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        foreach (var item in resp.Items)
        {
          int startIndex = 1;
          var pal = RM_FromXmlTo_PalletTraffic(item, ref startIndex);
          list.Add(pal);
        }
      }
      return dwr;
    }
  

    #endregion



    #region FromXmlTo
    private C200153_DistributionPallet RM_FromXmlTo_DistributionPallet(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_DistributionPallet ret = new C200153_DistributionPallet();
      ret.AreaCode = item.getFieldVal<string>(startIndex++);
      ret.FloorNumber = item.getFieldVal<int>(startIndex++);
      ret.NumPallet = item.getFieldVal<int>(startIndex++);
      return ret;

    }

    private C200153_PalletAll RM_FromXmlTo_PalletAll(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_PalletAll ret = new C200153_PalletAll();
      ret.PalletCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.PalletTypeId = item.getFieldVal<C200153_PalletType>(startIndex++);
      ret.DateBorn = item.getFieldVal<DateTime>(startIndex++);
      ret.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ProductionDate = item.getFieldVal<DateTime>(startIndex++);
      ret.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);
      ret.PositionCode = item.getFieldVal<string>(startIndex++);
      ret.DepositorCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      return ret;

    }

    private C200153_Depositor RM_FromXmlTo_Depositor(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_Depositor ret = new C200153_Depositor();
      ret.Code = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.Address = item.getFieldVal<string>(startIndex++).Base64Decode();
      return ret;

    }

    private C200153_StockPallet RM_FromXmlTo_StockPallet(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_StockPallet ret = new C200153_StockPallet();
      ret.Id = item.getFieldVal<int>(startIndex++);
      ret.Code = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.DateBorn = item.getFieldVal<DateTime>(startIndex++);
      ret.MapperEntityCode = item.getFieldVal<string>(startIndex++).ToUpper();
      ret.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.Depositor = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ProductionDate = item.getFieldVal<DateTime>(startIndex++);
      ret.ExpiryDate = item.getFieldVal<DateTime>(startIndex++);

      return ret;

    }

    private C200153_StockArticleBatch RM_FromXmlTo_StockBatchArticle(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_StockArticleBatch ret = new C200153_StockArticleBatch();
      ret.BatchCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.DepositorCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.MaxQty = item.getFieldVal<int>(startIndex++);
      ret.MinQty = item.getFieldVal<int>(startIndex++);
      while (startIndex < item.Fields.Count)
      {
        Report.C200153_StockByArea gba = new Report.C200153_StockByArea();
        gba.AreaCode = item.getFieldVal<string>(startIndex++);
        gba.NumPallet = item.getFieldVal<int>(startIndex++);
        gba.MaxQty = ret.MaxQty;
        gba.MinQty = ret.MinQty;
        ret.StockByArea.Add(gba);
      }
      return ret;
    }

    private C200153_Article RM_FromXmlTo_Article(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_Article art = new C200153_Article();
      art.Code = item.getFieldVal<string>(startIndex++)?.Base64Decode();
      art.Descr = item.getFieldVal<string>(startIndex++).Base64Decode();
      art.StockWindow = item.getFieldVal<int>(startIndex++);
      art.FifoWindow = item.getFieldVal<int>(startIndex++);
      art.Turnover = item.getFieldVal<C200153_TurnoverEnum>(startIndex++);
      art.WrappingProgram = item.getFieldVal<int>(startIndex++);
      art.IsDeleted = item.getFieldVal<bool>(startIndex++);
      return art;

    }


    private C200153_PalletTraffic RM_FromXmlTo_PalletTraffic(XmlCommandResponse.Item item, ref int startIndex)
    {
      C200153_PalletTraffic ret = new C200153_PalletTraffic();
      ret.Code = item.getFieldVal<string>(startIndex++);
      ret.Position = item.getFieldVal<string>(startIndex++);
      ret.FinalDestination = item.getFieldVal<string>(startIndex++);
      ret.ArticleCode = item.getFieldVal<string>(startIndex++).Base64Decode();
      ret.ArticleDescr = item.getFieldVal<string>(startIndex++).Base64Decode();

      return ret;

    }
    #endregion

    #endregion

    #region MAPMANAGER
    public void MM_RedestinatePallet(object receiverInstance, string palletCode, string positionCode)
    {
      var parameters = new List<string>();
      parameters.Add("MM_RedestinatePallet");
      parameters.Add(palletCode);
      parameters.Add(positionCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel MM_RedestinatePallet(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if(items!=null && items.Count() > 0)
      {
        int startIndex = 1;
        dwr.Result = FromXmlTo_Result(items[0], ref startIndex).ToString();
      }
      return dwr;
    }

    #endregion

    #region ADMINISTRATION
    public void ADM_Master_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("ADM_Master_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel ADM_Master_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<Administration.C200153_Master> masterL = new List<Administration.C200153_Master>();
      dwr.Data = masterL;
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        foreach (var item in items)
        {
          int startIndex = 1;
          Administration.C200153_Master master = new Administration.C200153_Master(item.getFieldVal<string>(startIndex++)
            , item.getFieldVal<Administration.C200153_MasterCategory>(startIndex++));
          masterL.Add(master);
        }
      }
      return dwr;
    }

    public void ADM_Master_GetByCode(object receiverInstance, string masterCode)
    {
      var parameters = new List<string>();
      parameters.Add("ADM_Master_GetByCode");
      parameters.Add(masterCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel ADM_Master_GetByCode(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        int startIndex = 1;
        var item = items[0];
        Administration.C200153_Master master = new Administration.C200153_Master(item.getFieldVal<string>(startIndex++), item.getFieldVal<Administration.C200153_MasterCategory>(startIndex++));
        dwr.Data = master;
      }
      return dwr;
    }

    public void ADM_Master_Update(object receiverInstance, string masterCode, Administration.C200153_MasterCategory category)
    {
      var parameters = new List<string>();
      parameters.Add("ADM_Master_Update");
      parameters.Add(masterCode);
      parameters.Add(category.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel ADM_Master_Update(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        int startIndex = 1;
        dwr.Result = FromXmlTo_Result(items[0], ref startIndex).ToString();
      }
      return dwr;
    }

    public void ADM_ExitPosition_GetAll(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("ADM_ExitPosition_GetAll");
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel ADM_ExitPosition_GetAll(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<Administration.C200153_ExitPosition> list = new List<Administration.C200153_ExitPosition>();
      dwr.Data = list;
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        foreach (var item in items)
        {
          int startIndex = 1;
          Administration.C200153_ExitPosition element = new Administration.C200153_ExitPosition(item.getFieldVal<string>(startIndex++), item.getFieldVal<int>(startIndex++));
          list.Add(element);
        }
      }
      return dwr;
    }

    public void ADM_ExitPosition_Update(object receiverInstance, string posCode, int maxPalletInTransit)
    {
      var parameters = new List<string>();
      parameters.Add("ADM_ExitPosition_Update");
      parameters.Add(posCode);
      parameters.Add(maxPalletInTransit.ToString());
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel ADM_ExitPosition_Update(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        int startIndex = 1;
        dwr.Result = FromXmlTo_Result(items[0], ref startIndex).ToString();
      }
      return dwr;
    }

    public void ADM_ExitPosition_GetByCode(object receiverInstance, string posCode)
    {
      var parameters = new List<string>();
      parameters.Add("ADM_ExitPosition_GetByCode");
      parameters.Add(posCode);
      Plugin_Command(receiverInstance, parameters);
    }
    private IModel ADM_ExitPosition_GetByCode(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      var items = resp.getAllItem();
      if (items != null && items.Count() > 0)
      {
        int startIndex = 1;
        var item = items[0];
        Administration.C200153_ExitPosition pos = new Administration.C200153_ExitPosition(item.getFieldVal<string>(startIndex++), item.getFieldVal<int>(startIndex++));
        dwr.Data = pos;
      }
      return dwr;
    }


    #endregion


    #region FROM XML TO
    private C200153_CustomResult FromXmlTo_Result(XmlCommandResponse.Item item, ref int startIndex)
    {
      return item.getFieldVal<C200153_CustomResult>(startIndex++);
    }

    #endregion

    #region  METODI UTILITA
    private static PluginInfo Plugin_FromXml(XmlCommandResponse.Item item)
    {
      int i = 1;

      var name = item.getFieldVal(i++);
      if (name == "null")
        return null;
      var fileName = item.getFieldVal(i++);
      var version = item.getFieldVal(i++);
      var status = item.getFieldVal<Plugin_State>(i++);

      return new PluginInfo(name) { FileName = fileName, Version = version, Status = status };
    }
    #endregion



    #region DECODIFICA

    public override ICommandResponse PreDecode(MsgEventArgs e)
    {
      XmlCommandResponse resp = e.iXmlStreamer as XmlCommandResponse;
      if (resp == null)
        return null;

      //restituisco la risposta al comando
      return new CommandResponseC200153(resp.xmlCommand);
    }

    public override ICommandResponse Decode(MsgEventArgs e)
    {
      XmlCommandResponse resp = e.iXmlStreamer as XmlCommandResponse;
      if (resp == null)
        return null;

      //recupero la risposta al comando
      var commandResponse = new CommandResponseC200153(resp.xmlCommand);

      try
      {
        //invoco il metodo
        var respModel = (IModel)GetType().InvokeMethod(this, commandResponse.CommandName, new object[] { resp });

        //setto il dato
        commandResponse.ResponseModel = respModel;
      }
      catch (Exception ex)
      {
        Console.WriteLine($"{commandResponse.CommandName} => Exception={ex.Message}");
      }

      return commandResponse;
    }

    public override ICommandResponse Decode(ExceptionEventArgs e)
    {
      var xmlCmd = e.xmlCommand as XmlCommand;
      if (xmlCmd == null)
        return null;

      //restituisco la risposta al comando
      return new CommandResponseC200153(xmlCmd);
    }

    #endregion


  }
}