﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Model.Common;


namespace Model.Custom.C200153
{
  public class C200153_PositionOut_Pallet:ModelBase
  {
    public C200153_PositionOut_Pallet()
    {

    }

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }




    private int _Height;
    public int Height
    {
      get { return _Height; }
      set
      {
        if (value != _Height)
        {
          _Height = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _Weight;
    public int Weight
    {
      get { return _Weight; }
      set
      {
        if (value != _Weight)
        {
          _Weight = value;
          NotifyPropertyChanged();
        }
      }
    }



    private bool _IsService;
    public bool IsService
    {
      get { return _IsService; }
      set
      {
        if (value != _IsService)
        {
          _IsService = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_CustomResult _CustomResult;
    public C200153_CustomResult CustomResult
    {
      get { return _CustomResult; }
      set
      {
        if (value != _CustomResult)
        {
          _CustomResult = value;
          NotifyPropertyChanged();
          IsCustomReject = _CustomResult != C200153_CustomResult.OK;
        }
      }
    }
    
    private bool _IsCustomReject;
    public bool IsCustomReject
    {
      get { return _IsCustomReject; }
      set
      {
        if (value != _IsCustomReject)
        {
          _IsCustomReject = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsReject;
    public bool IsReject
    {
      get { return _IsReject; }
      set
      {
        if (value != _IsReject)
        {
          _IsReject = value;
          NotifyPropertyChanged();
        }
      }
    }

    private Results_Enum _RejectResult;
    public Results_Enum RejectResult
    {
      get { return _RejectResult; }
      set
      {
        if (value != _RejectResult)
        {
          _RejectResult = value;
          NotifyPropertyChanged();
        }
      }
    }

    #region DATI ORDINE

    private bool _HasOrder;
    public bool HasOrder
    {
      get { return _HasOrder; }
      set
      {
        if (value != _HasOrder)
        {
          _HasOrder = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _OrderCode = string.Empty;
    public string OrderCode
    {
      get { return _OrderCode; }
      set
      {
        if (value != _OrderCode)
        {
          _OrderCode = value;
          NotifyPropertyChanged();
        }
      }
    }
    
    private string _OrderCustomer = string.Empty;
    public string OrderCustomer
    {
      get { return _OrderCustomer; }
      set
      {
        if (value != _OrderCustomer)
        {
          _OrderCustomer = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _OrderCmpArticleCode = string.Empty;
    public string OrderCmpArticleCode
    {
      get { return _OrderCmpArticleCode; }
      set
      {
        if (value != _OrderCmpArticleCode)
        {
          _OrderCmpArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _OrderCmpArticleDescr = string.Empty;
    public string OrderCmpArticleDescr
    {
      get { return _OrderCmpArticleDescr; }
      set
      {
        if (value != _OrderCmpArticleDescr)
        {
          _OrderCmpArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _OrderCmpQtyRequested = 0;
    public int OrderCmpQtyRequested
    {
      get { return _OrderCmpQtyRequested; }
      set
      {
        if (value != _OrderCmpQtyRequested)
        {
          _OrderCmpQtyRequested = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _OrderCmpQuantityDelivered = 0;
    public int OrderCmpQuantityDelivered
    {
      get { return _OrderCmpQuantityDelivered; }
      set
      {
        if (value != _OrderCmpQuantityDelivered)
        {
          _OrderCmpQuantityDelivered = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsLabeled;
    public bool IsLabeled
    {
      get { return _IsLabeled; }
      set
      {
        if (value != _IsLabeled)
        {
          _IsLabeled = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsWrapped;
    public bool IsWrapped
    {
      get { return _IsWrapped; }
      set
      {
        if (value != _IsWrapped)
        {
          _IsWrapped = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _ProductionDate = DateTime.MinValue;
    public DateTime ProductionDate
    {
      get { return _ProductionDate; }
      set
      {
        if (value != _ProductionDate)
        {
          _ProductionDate = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _ExpiryDate = DateTime.MinValue;
    public DateTime ExpiryDate
    {
      get { return _ExpiryDate; }
      set
      {
        if (value != _ExpiryDate)
        {
          _ExpiryDate = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _BatchCode = String.Empty;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _BatchLength;
    public int BatchLength
    {
      get { return _BatchLength; }
      set
      {
        if (value != _BatchLength)
        {
          _BatchLength = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _DepositorCode = String.Empty;
    public string DepositorCode
    {
      get { return _DepositorCode; }
      set
      {
        if (value != _DepositorCode)
        {
          _DepositorCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _DepositorAddress = String.Empty;
    public string DepositorAddress
    {
      get { return _DepositorAddress.Replace('\n', new char()).Replace('\r', new char()); }
      set
      {
        if (value != _DepositorAddress)
        {
          _DepositorAddress = value;
          NotifyPropertyChanged();
        }
      }
    }

    #endregion


  }
}
