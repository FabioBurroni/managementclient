﻿
namespace Model.Custom.C200153.Depositor
{
  public class C200153_DepositorImport : ModelBase
  {
    
    
    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Address;
    public string Address
    {
      get { return _Address; }
      set
      {
        if (value != _Address)
        {
          _Address = value;
          NotifyPropertyChanged();
        }
      }
    }



    private bool _Selected;
    public bool Selected
    {
      get { return _Selected; }
      set
      {
        if (value != _Selected)
        {
          _Selected = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _Valid;
    public bool Valid
    {
      get { return _Valid; }
      set
      {
        if (value != _Valid)
        {
          _Valid = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_CustomResult _Result;
    public C200153_CustomResult Result
    {
      get { return _Result; }
      set
      {
        if (value != _Result)
        {
          _Result = value;
          NotifyPropertyChanged();
        }
      }
    }

    

  }
}
