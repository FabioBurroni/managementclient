﻿using LINQtoCSV;

namespace Model.Custom.C200153.Depositor
{
  public class C200153_Depositor : ModelBase
  {

    private string _Code;
    [CsvColumn(Name = "Code", FieldIndex = 1)]
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Address;
    [CsvColumn(Name = "Address", FieldIndex = 2)]
    public string Address
    {
      get { return _Address; }
      set
      {
        if (value != _Address)
        {
          _Address = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsDeleted;
    [CsvColumn(Name = "IsDeleted", FieldIndex = 3)]
    public bool IsDeleted
    {
      get { return _IsDeleted; }
      set
      {
        if (value != _IsDeleted)
        {
          _IsDeleted = value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
