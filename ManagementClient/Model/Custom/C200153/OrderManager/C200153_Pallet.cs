﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Common;

namespace Model.Custom.C200153.OrderManager
{
  public class C200153_Pallet: ModelBase
  {

    private string _Code = string.Empty;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private DateTime _DateBorn = DateTime.MinValue;
    public DateTime DateBorn
    {
      get { return _DateBorn; }
      set
      {
        if (value != _DateBorn)
        {
          _DateBorn = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsService;
    public bool IsService
    {
      get { return _IsService; }
      set
      {
        if (value != _IsService)
        {
          _IsService = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_CustomResult _CustomResult;
    public C200153_CustomResult CustomResult
    {
      get { return _CustomResult; }
      set
      {
        if (value != _CustomResult)
        {
          _CustomResult = value;
          NotifyPropertyChanged();
          if (_CustomResult != C200153_CustomResult.OK)
          {
            IsCustomReject = true;
          }
          else
          {
            IsCustomReject = false;
          }
        }
      }
    }



    private bool _IsCustomReject;
    public bool IsCustomReject
    {
      get { return _IsCustomReject; }
      set
      {
        if (value != _IsCustomReject)
        {
          _IsCustomReject = value;
          NotifyPropertyChanged();
        }
      }
    }



    private DateTime _ProductionDate = DateTime.MinValue;
    public DateTime ProductionDate
    {
      get { return _ProductionDate; }
      set
      {
        if (value != _ProductionDate)
        {
          _ProductionDate = value;
          NotifyPropertyChanged();
        }
      }
    }

    //20200902

    private DateTime _ExpiryDate = DateTime.MinValue;
    public DateTime ExpiryDate
    {
      get { return _ExpiryDate; }
      set
      {
        if (value != _ExpiryDate)
        {
          _ExpiryDate = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Days_to_Expiry;
    public int Days_to_Expiry
    {
      get { return _Days_to_Expiry; }
      set
      {
        if (value != _Days_to_Expiry)
        {
          _Days_to_Expiry = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _FinalDestination = string.Empty;
    public string FinalDestination
    {
      get { return _FinalDestination; }
      set
      {
        if (value != _FinalDestination)
        {
          _FinalDestination = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsReject;
    public bool IsReject
    {
      get { return _IsReject; }
      set
      {
        if (value != _IsReject)
        {
          _IsReject = value;
          NotifyPropertyChanged();
        }
      }
    }


    private Results_Enum _RejectResult;
    public Results_Enum RejectResult
    {
      get { return _RejectResult; }
      set
      {
        if (value != _RejectResult)
        {
          _RejectResult = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _BatchCode;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_Pallet_State _State;
    public C200153_Pallet_State State
    {
      get { return _State; }
      set
      {
        if (value != _State)
        {
          _State = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Days_Spent_FromProduction;
    public int Days_Spent_FromProduction
    {
      get { return _Days_Spent_FromProduction; }
      set
      {
        if (value != _Days_Spent_FromProduction)
        {
          _Days_Spent_FromProduction = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsLabeled;
    public bool IsLabeled
    {
      get { return _IsLabeled; }
      set
      {
        if (value != _IsLabeled)
        {
          _IsLabeled = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsWrapped;
    public bool IsWrapped
    {
      get { return _IsWrapped; }
      set
      {
        if (value != _IsWrapped)
        {
          _IsWrapped = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _DepositorCode;
    public string DepositorCode
    {
      get { return _DepositorCode; }
      set
      {
        if (value != _DepositorCode)
        {
          _DepositorCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    public void Update(C200153_Pallet newPallet)
    {
      this.Code = newPallet.Code;
      this.DateBorn = newPallet.DateBorn;
      this.IsService = newPallet.IsService;
      this.CustomResult = newPallet.CustomResult;
      this.IsReject = newPallet.IsReject;
      this.ProductionDate = newPallet.ProductionDate;
      this.BatchCode = newPallet.BatchCode;
      this.ArticleCode = newPallet.ArticleCode;
      this.ArticleDescr = newPallet.ArticleDescr;
      this.IsLabeled = newPallet.IsLabeled;
      this.IsWrapped = newPallet.IsWrapped;
      this.DepositorCode = newPallet.DepositorCode;
    }

    public void Reset()
    {
      this.Code = string.Empty;
      this.DateBorn = DateTime.MinValue;
      this.IsService = false;
      this.CustomResult = C200153_CustomResult.UNDEFINED;
      this.IsReject = false;
      this.ProductionDate = DateTime.MinValue;
      this.BatchCode = string.Empty;
      this.ArticleCode = string.Empty;
      this.ArticleDescr = string.Empty;
      this.IsLabeled = false;
      this.IsWrapped = false;
      this.DepositorCode = string.Empty;
    }
  }
}
