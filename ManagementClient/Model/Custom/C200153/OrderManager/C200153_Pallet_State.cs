﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153.OrderManager
{
  public enum C200153_Pallet_State
  {
    OK = 0,
    BLOCKED = 1,
    QUALITY = 2,
  }
}
