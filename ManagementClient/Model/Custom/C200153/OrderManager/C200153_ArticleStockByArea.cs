﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153.OrderManager
{
  public class C200153_ArticleStockByArea
  {
    public string BatchCode { get; set; }
    public string ArticleCode { get; set; }
    public string ArticleDescr { get; set; }
    public ObservableCollectionFast<C200153_StockByArea> StockByAreaL { get; set; } = new ObservableCollectionFast<C200153_StockByArea>();
  }
}
