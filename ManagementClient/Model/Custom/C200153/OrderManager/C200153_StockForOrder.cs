﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Custom.C200153.ArticleManager;
namespace Model.Custom.C200153.OrderManager
{
  public class C200153_StockForOrder : ModelBase
  {
    private string _BatchCode;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_Article _Article;
    public C200153_Article Article
    {
      get { return _Article; }
      set
      {
        if (value != _Article)
        {
          _Article = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _NumPallet = 0;
    /// <summary>
    /// Numero di Pallet nei magazzini automatici
    /// </summary>
    public int NumPallet
    {
      get { return _NumPallet; }
      set
      {
        if (value != _NumPallet)
        {
          _NumPallet= value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
