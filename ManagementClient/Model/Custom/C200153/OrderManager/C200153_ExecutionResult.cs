﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153.OrderManager
{
  public enum C200153_ExecutionResult
  {
    NOT_SCANNED,    //...componente o lista mai analizzato 
    IN_PROCESS, //...componente o lista in analisi
    COMPLETED,  //...componente o lista completato
    SERVED,    //...componente o lista è stato assegnato un pallet

    ARTICLE_NOT_FOUND,  //...l'articolo non è presente
    ARTICLE_NOT_AVAILABLE,  //...l'articolo non è disponibile

    LANE_BUSY, //...la cella da dove devo prelevare è impegnata

    DESTINATION_DISABLED,   //...la destinazione è disabilitata
    DESTINATION_NOT_REACHABLE,   //...la destinazione non è raggiungibile
    DESTINATION_MAX_PALLET_IN_TRANSIT,//...la destinazione ha raggiunto il numero massimo di pallet in transito
    MASTER_MAX_WORKLOAD, //...master ha raggiunto il work load massimo
    WAREHOUSE_NOT_AVAILABLE,
    ERROR,
  }
}
