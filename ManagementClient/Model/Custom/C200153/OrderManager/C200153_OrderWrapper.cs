﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153.OrderManager
{
  public class C200153_OrderWrapper:ModelBase
  {
    public C200153_OrderType OrderType = C200153_OrderType.MNL;
    public ObservableCollectionFast<C200153_OrderComponentWrapper> CmpL { get; set; } = new ObservableCollectionFast<C200153_OrderComponentWrapper>();


    public C200153_OrderWrapper()
    {
      CmpL.CollectionChanged += CmpL_CollectionChanged;
    }

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsValid");
        }
      }
    }


    private string _Customer;
    public string Customer
    {
      get { return _Customer; }
      set
      {
        if (value != _Customer)
        {
          _Customer = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Priority;
    public int Priority
    {
      get { return _Priority; }
      set
      {
        if (value != _Priority)
        {
          _Priority = value;
          NotifyPropertyChanged();
        }
      }
    }



    private void CmpL_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
      NotifyPropertyChanged("IsValid");
    }

    public bool IsValid
    {
      get
      {
        return CmpL.Count > 0 && !string.IsNullOrEmpty(Code);
      }
    }

    
    

  
    public void Reset()
    {
      CmpL.Clear();
      Code= string.Empty;
      Customer= string.Empty;
      Priority= 0;

    }

  }
}
