﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153.OrderManager
{
  public class C200153_Order : ModelBase
  {

    private int _Id;
    public int Id
    {
      get { return _Id; }
      set
      {
        if (value != _Id)
        {
          _Id = value;
          NotifyPropertyChanged();
        }
      }
    }
    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }
    private string _Customer;
    public string Customer
    {
      get { return _Customer; }
      set
      {
        if (value != _Customer)
        {
          _Customer = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C200153_State _State;
    public C200153_State State
    {
      get { return _State; }
      set
      {
        if (value != _State)
        {
          _State = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_ListDestination _Destination = new C200153_ListDestination();
    public C200153_ListDestination Destination
    {
      get { return _Destination; }
      set
      {
        if (value != _Destination)
        {
          _Destination = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_ExecutionModality _ExecutionModality;
    public C200153_ExecutionModality ExecutionModality
    {
      get { return _ExecutionModality; }
      set
      {
        if (value != _ExecutionModality)
        {
          _ExecutionModality = value;
          NotifyPropertyChanged();
        }
      }
    }



    private C200153_OrderType _OrderType;
    public C200153_OrderType OrderType
    {
      get { return _OrderType; }
      set
      {
        if (value != _OrderType)
        {
          _OrderType = value;
          NotifyPropertyChanged();
        }
      }
    }


    private DateTime _DateBorn;
    public DateTime DateBorn
    {
      get { return _DateBorn; }
      set
      {
        if (value != _DateBorn)
        {
          _DateBorn = value;
          NotifyPropertyChanged();
        }
      }
    }


    private DateTime _DateSend;
    public DateTime DateSend
    {
      get { return _DateSend; }
      set
      {
        if (value != _DateSend)
        {
          _DateSend = value;
          NotifyPropertyChanged();
        }
      }
    }


    private DateTime _DateExec;
    public DateTime DateExec
    {
      get { return _DateExec; }
      set
      {
        if (value != _DateExec)
        {
          _DateExec = value;
          NotifyPropertyChanged();
        }
      }
    }


    private DateTime _DateEnd;
    public DateTime DateEnd
    {
      get { return _DateEnd; }
      set
      {
        if (value != _DateEnd)
        {
          _DateEnd = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _priority;
    public int Priority
    {
      get { return _priority; }
      set
      {
        if (value != _priority)
        {
          _priority = value;
          NotifyPropertyChanged();
        }
      }
    }


    
    private C200153_ExecutionResult _ExecutionResult;
    public C200153_ExecutionResult ExecutionResult
    {
      get { return _ExecutionResult; }
      set
      {
        if (value != _ExecutionResult)
        {
          _ExecutionResult= value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _LastScanningDateTime;
    public DateTime LastScanningDateTime
    {
      get { return _LastScanningDateTime; }
      set
      {
        if (value != _LastScanningDateTime)
        {
          _LastScanningDateTime = value;
          NotifyPropertyChanged();
        }
      }
    }
    #region PROPRIETA' CALCOLATE
    public int NumPalletInTransit
    {
      get
      {
        return CmpL.Sum(cmp => cmp.NumPalletInTransit);
      }
    }

    public int TotalQtyDelivered
    {
      get
      {
        return CmpL.Sum(cmp => cmp.QtyDelivered);
      }
    }

    public int TotalQuantityRequested
    {
      get
      {
        return CmpL.Sum(cmp => cmp.QtyRequested);
      }
    }

    public int TotalQuantityRemaining
    {
      get
      {
        return CmpL.Sum(cmp => cmp.QtyRemaining);
      }
    } 

    public bool HasDestinationValid
    {
      get
      {
        return !string.IsNullOrEmpty(Destination.Code);
      }
    }

    #endregion




    public void Update(C200153_Order orderNew)
    {
      this.DateSend = orderNew.DateSend;
      this.DateExec= orderNew.DateExec;
      this.DateEnd= orderNew.DateEnd;
      this.State = orderNew.State;
      this.Priority = orderNew.Priority;
      this.LastScanningDateTime = orderNew.LastScanningDateTime;
      this.ExecutionResult= orderNew.ExecutionResult;
      Destination_Update(orderNew.Destination);
      
      List<C200153_OrderCmp> cmpToAdd = new List<C200153_OrderCmp>();
      foreach (var cmpNew in orderNew.CmpL)
      {
        var c = CmpL.FirstOrDefault(cmp => cmp.Id == cmpNew.Id);
        if (c != null)
          c.Update(cmpNew);
        else
          cmpToAdd.Add(cmpNew);
      }
      cmpToAdd.ForEach(cmp => CmpL.Add(cmp));


      CollectionHelper.UpdateAll<C200153_PalletInTransit>(PalletInTransitL, orderNew.PalletInTransitL,"Code");

      NotifyPropertyChanged("NumPalletInTransit");
      NotifyPropertyChanged("TotalQuantityRequested");
      NotifyPropertyChanged("TotalQtyDelivered");
      NotifyPropertyChanged("TotalQuantityRemaining");
      NotifyPropertyChanged("HasDestinationValid");
      
    }

    public void Destination_Update(C200153_ListDestination newDestination)
    {
      if(Destination.Code!=newDestination.Code)
      {
        Destination = newDestination;
      }
    }


    public ObservableCollectionFast<C200153_OrderCmp> CmpL { get; set; } = new ObservableCollectionFast<C200153_OrderCmp>();
    public ObservableCollectionFast<C200153_PalletInTransit> PalletInTransitL { get; set; } = new ObservableCollectionFast<C200153_PalletInTransit>();





  }
}
