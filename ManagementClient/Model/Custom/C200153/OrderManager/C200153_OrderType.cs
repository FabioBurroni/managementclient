﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153.OrderManager
{
  public enum C200153_OrderType
  {
    AUTO, //...ordine di prelievo da magazzino centrale
    MNL,  //...ordine di prelievo manuale
    SRV,  //...non usata in SACI		
  }
}
