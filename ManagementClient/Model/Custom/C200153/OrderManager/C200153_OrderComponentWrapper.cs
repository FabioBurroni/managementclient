﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153.OrderManager
{
  public class C200153_OrderComponentWrapper:ModelBase
  {
    public string BatchCode { get; set; } = string.Empty;
    public string ArticleCode { get; set; } = string.Empty;
    public string ArticleDescr { get; set; } = string.Empty;


    private int _QtyRequested;
    public int QtyRequested
    {
      get { return _QtyRequested; }
      set
      {
        if (value != _QtyRequested)
        {
          _QtyRequested = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C200153_ListCmpTypeEnum _CmpType = C200153_ListCmpTypeEnum.FIFO;
    public C200153_ListCmpTypeEnum CmpType
    {
      get { return _CmpType; }
      set
      {
        if (value != _CmpType)
        {
          _CmpType = value;
          NotifyPropertyChanged();
        }
      }
    }


    public override string ToString()
    {
      return $"BathcCode:{BatchCode}, ArticleCode:{ArticleCode}, ArticleDescr:{ArticleDescr},QtyRequested:{QtyRequested}";
    }
  }
}
