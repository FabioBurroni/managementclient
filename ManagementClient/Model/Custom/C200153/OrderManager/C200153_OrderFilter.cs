﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Extensions;
namespace Model.Custom.C200153.OrderManager
{
  public class C200153_OrderFilter:ModelBase
  {
    #region Public properties

    public ObservableCollectionFast<View_OrderState> OrderStateL { get; set; } = new ObservableCollectionFast<View_OrderState>();
    public ObservableCollectionFast<View_OrderType> OrderTypeL { get; set; } = new ObservableCollectionFast<View_OrderType>();
    #endregion

    #region Constructor
    public C200153_OrderFilter()
    {
      OrderStateL.Add(new View_OrderState() { StateCode = "*", StateDescr = "ALL" });
      foreach (var item in Enum.GetValues(typeof(C200153_State)))
      {
        if (item.ToString() == C200153_State.LEXEC.ToString())
          OrderStateL.Add(new View_OrderState() { StateCode = item.ToString(), StateDescr = "EXECUTING" });
        if (item.ToString() == C200153_State.LWAIT.ToString())
          OrderStateL.Add(new View_OrderState() { StateCode = item.ToString(), StateDescr = "WAITING" });
        if (item.ToString() == C200153_State.LPAUSE.ToString())
          OrderStateL.Add(new View_OrderState() { StateCode = item.ToString(), StateDescr = "PAUSED" });
        if (item.ToString() == C200153_State.LDONE.ToString())
          OrderStateL.Add(new View_OrderState() { StateCode = item.ToString(), StateDescr = "DONE" });
        if (item.ToString() == C200153_State.LKILL.ToString())
          OrderStateL.Add(new View_OrderState() { StateCode = item.ToString(), StateDescr = "KILLED" });
        if (item.ToString() == C200153_State.LUNCOMPLETE.ToString())
          OrderStateL.Add(new View_OrderState() { StateCode = item.ToString(), StateDescr = "NOT COMPLETED" });
      }

      OrderTypeL.Add(new View_OrderType() { TypeCode = "*", TypeDescr = "ALL" });
      foreach (var item in Enum.GetValues(typeof(C200153_OrderType)))
      {
        if (item.ToString() == C200153_OrderType.AUTO.ToString())
          OrderTypeL.Add(new View_OrderType() { TypeCode = item.ToString(), TypeDescr = "AUTOMATIC" });
        if (item.ToString() == C200153_OrderType.MNL.ToString())
          OrderTypeL.Add(new View_OrderType() { TypeCode = item.ToString(), TypeDescr = "MANUAL" });
      }


      //Enum.GetValues(typeof(C200153_State)).OfType<C200153_State>().ToList().Where(s => s.ToString().StartsWith("L")).Select(s => s.ToString());
    }
    #endregion

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; }
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000, 5000, 10000, 30000 };
    public ObservableCollectionFast<int> PriorityElements { get; set; } = new ObservableCollectionFast<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    #endregion

    #region FILTRI
    private string _OrderCode;
    public string OrderCode
    {
      get { return _OrderCode; }
      set
      {
        if (value != _OrderCode)
        {
          _OrderCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private View_OrderState _StateSelected;
    public View_OrderState StateSelected
    {
      get { return _StateSelected; }
      set
      {
        if (value != _StateSelected)
        {
          _StateSelected = value;
          NotifyPropertyChanged();
        }
      }
    }


    private View_OrderType _OrderTypeSelected;
    public View_OrderType OrderTypeSelected
    {
      get { return _OrderTypeSelected; }
      set
      {
        if (value != _OrderTypeSelected)
        {
          _OrderTypeSelected = value;
          NotifyPropertyChanged();
        }
      }
    }



    private DateTime _DateStart = DateTime.MinValue;
    public DateTime DateStart
    {
      get { return _DateStart; }
      set
      {
        if (value != _DateStart)
        {
          _DateStart = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _DateEnd;
    public DateTime DateEnd
    {
      get { return _DateEnd; }
      set
      {
        if (value != _DateEnd)
        {
          _DateEnd = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _Priority;
    public int Priority
    {
      get { return _Priority; }
      set
      {
        if (value != _Priority)
        {
          _Priority = value;
          NotifyPropertyChanged();
        }
      }
    }



    private bool _PriorityIsEnabled;
    public bool PriorityIsEnabled
    {
      get { return _PriorityIsEnabled; }
      set
      {
        if (value != _PriorityIsEnabled)
        {
          _PriorityIsEnabled = value;
          NotifyPropertyChanged();
        }
      }
    }

    #endregion

    #region public methods

    public void StateSelected_Set(C200153_State state)
    {
      StateSelected = OrderStateL.FirstOrDefault(s => s.StateCode == state.ToString());
    }



    public void Reset()
    {

      OrderCode = "";
      StateSelected = OrderStateL.First();
      OrderTypeSelected = OrderTypeL.First();
      Priority = 0;
      PriorityIsEnabled = false;
    }

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        string.IsNullOrEmpty(OrderCode)?"":OrderCode.Base64Encode(),
        StateSelected.StateCode??"",
        OrderTypeSelected.TypeCode??"",
        GetXmlCommandParameterPriority()
      };

      //DateStart.ConvertToDateTimeFormatString(),
      //  DateEnd.ConvertToDateTimeFormatString(),
    }

    private string GetXmlCommandParameterPriority()
    {
      if (PriorityIsEnabled)
        return Priority.ToString();
      return "";
    }


    #endregion

    public class View_OrderState
    {
      public string StateCode { get; set; }
      public string StateDescr { get; set; }
    }

    public class View_OrderType
    {
      public string TypeCode { get; set; }
      public string TypeDescr { get; set; }
    }

  }
}
