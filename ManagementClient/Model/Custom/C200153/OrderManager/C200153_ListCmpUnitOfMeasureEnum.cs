﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153.OrderManager
{
  public enum C200153_ListCmpUnitOfMeasureEnum
  {
    P=1,  //...pallet
    W=2,  //...peso
    C=3,  //...scatole
  }
}
