﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153.OrderManager
{
  /// <summary>
  /// Riporta la possibile destinazione dell'ordine e le posizioni di handling associate
  /// </summary>
  public class C200153_ListDestination:ModelBase
  {

    public C200153_ListDestination()
    {

    }
    public string Code{ get; set; }
    
    /// <summary>
    /// Descrizione associata alla destinazione
    /// </summary>
    public string Description { get; set; }


    /// <summary>
    /// Elenco delle posizioni che rappresentano la destinazione
    /// </summary>
    public List<string> PositionList { get; set; } = new List<string>();

    /// <summary>
    /// Se true, in caso di esecuzione automatica dell'ordine viene assegnata quresta destinazione
    /// </summary>
    private bool _IsDefault;
    public bool IsDefault
    {
      get { return _IsDefault; }
      set
      {
        if (value != _IsDefault)
        {
          _IsDefault = value;
          NotifyPropertyChanged();
        }
      }
    }

    public string AreaCode { get; set; }

    public override string ToString()
    {
      return $"ListDestination: {Description} Positions: {string.Join("-", PositionList)}";
    }
  }
}
