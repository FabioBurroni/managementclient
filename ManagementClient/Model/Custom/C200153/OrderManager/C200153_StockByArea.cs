﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153.OrderManager
{
  public class C200153_StockByArea
  {
    public int NumPallet { get; set; } = 0;
    public string AreaCode { get; set; }
    public int Floor { get; set; }



    public override string ToString()
    {
      return $"AreaCode:{AreaCode}, Floor:{Floor}, NumPallet:{NumPallet}";
    }
  }
}
