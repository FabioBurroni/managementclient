﻿

namespace Model.Custom.C200153.Label
{
  public class C200153_Label: ModelBase
  {
    #region CONTRUSCTOR
    public C200153_Label()
    {

    }

    public C200153_Label(string code, string label)
    {
      _Code = code;
      _Label = label;
    }
    #endregion

    private string _Code;
    /// <summary>
    /// Code associated with label
    /// </summary>
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }



    private string _Label;
    /// <summary>
    /// Label Text
    /// </summary>
    public string Label
    {
      get { return _Label; }
      set
      {
        if (value != _Label)
        {
          _Label = value;
          NotifyPropertyChanged();
        }
      }
    }


    #region PUBLIC METHODS
    public void Update(C200153_Label newLabel)
    {
      _Code = newLabel.Code;
      _Label = newLabel.Label;
    }
    public void Clear()
    {
      _Code = string.Empty;
      _Label = string.Empty;
    }

    #endregion

  }
}
