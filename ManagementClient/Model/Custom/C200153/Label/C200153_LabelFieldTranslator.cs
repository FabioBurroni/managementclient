﻿
namespace Model.Custom.C200153.Label
{
  public class C200153_LabelFieldTranslator : ModelBase
  {
    private string _Field;
    public string Field
    {
      get { return _Field; }
      set
      {
        if (value != _Field)
        {
          _Field = value;
          NotifyPropertyChanged();
        }
      }
    }



    private string _FieldTranslated;
    public string FieldTranslated
    {
      get { return _FieldTranslated; }
      set
      {
        if (value != _FieldTranslated)
        {
          _FieldTranslated = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _FieldEnglish;
    public string FieldEnglish
    {
      get { return _FieldEnglish; }
      set
      {
        if (value != _FieldEnglish)
        {
          _FieldEnglish = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Description;
    public string Description
    {
      get { return _Description; }
      set
      {
        if (value != _Description)
        {
          _Description = value;
          NotifyPropertyChanged();
        }
      }
    }


    #region PUBLIC METHODS
    public void Update(C200153_LabelFieldTranslator newField)
    {
      _Field = newField.Field;
      _FieldTranslated = newField.FieldTranslated;
      _FieldEnglish = newField.FieldEnglish;
      _Description = newField.Description;
    }
    public void Clear()
    {
      _Field = string.Empty;
      _FieldTranslated = string.Empty;
      _FieldEnglish = string.Empty;
      _Description = string.Empty;
    }

    #endregion
  }
}
