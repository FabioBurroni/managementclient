﻿
using Model.Common;

namespace Model.Custom.C200153.ArticleManager
{
  public class C200153_ArticleDeleteResults : ModelBase
  {   
    private Results_Enum deletableResult;
    public Results_Enum DeletableResult
    {
      get { return deletableResult; }
      set
      {
        if (value != deletableResult)
        {
          deletableResult = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_CustomResult _deleteResult;
    public C200153_CustomResult DeleteResult
    {
      get { return _deleteResult; }
      set
      {
        if (value != _deleteResult)
        {
          _deleteResult = value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
