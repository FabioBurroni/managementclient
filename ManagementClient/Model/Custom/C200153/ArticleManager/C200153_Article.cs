﻿
namespace Model.Custom.C200153.ArticleManager
{
  public class C200153_Article : ModelBase
  {

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Descr;
    public string Descr
    {
      get { return _Descr; }
      set
      {
        if (value != _Descr)
        {
          _Descr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _StockWindow;
    public int StockWindow
    {
      get { return _StockWindow; }
      set
      {
        if (value != _StockWindow)
        {
          _StockWindow = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _FifoWindow;
    public int FifoWindow
    {
      get { return _FifoWindow; }
      set
      {
        if (value != _FifoWindow)
        {
          _FifoWindow = value;
          NotifyPropertyChanged();
        }
      }
    }
      

    private C200153_TurnoverEnum _Turnover;
    public C200153_TurnoverEnum Turnover
    {
      get { return _Turnover; }
      set
      {
        if (value != _Turnover)
        {
          _Turnover = value;
          NotifyPropertyChanged();
        }
      }
    }
  

    private int _WrappingProgram;
    public int WrappingProgram
    {
      get { return _WrappingProgram; }
      set
      {
        if (value != _WrappingProgram)
        {
          _WrappingProgram = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsDeleted;
    public bool IsDeleted
    {
      get { return _IsDeleted; }
      set
      {
        if (value != _IsDeleted)
        {
          _IsDeleted = value;
          NotifyPropertyChanged();
        }
      }
    }
  }
}
