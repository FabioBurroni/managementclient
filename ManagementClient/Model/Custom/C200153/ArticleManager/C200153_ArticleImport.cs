﻿
namespace Model.Custom.C200153.ArticleManager
{
  public class C200153_ArticleImport : ModelBase
  {
    
    
    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Descr;
    public string Descr
    {
      get { return _Descr; }
      set
      {
        if (value != _Descr)
        {
          _Descr = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C200153_TurnoverEnum _Turnover;
    public C200153_TurnoverEnum Turnover
    {
      get { return _Turnover; }
      set
      {
        if (value != _Turnover)
        {
          _Turnover = value;
          NotifyPropertyChanged();
        }
      }
    }   

    private int _WrappingProgram;
    public int WrappingProgram
    {
      get { return _WrappingProgram; }
      set
      {
        if (value != _WrappingProgram)
        {
          _WrappingProgram = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _Selected;
    public bool Selected
    {
      get { return _Selected; }
      set
      {
        if (value != _Selected)
        {
          _Selected = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _Valid;
    public bool Valid
    {
      get { return _Valid; }
      set
      {
        if (value != _Valid)
        {
          _Valid = value;
          NotifyPropertyChanged();
        }
      }
    }

    private C200153_CustomResult _Result;
    public C200153_CustomResult Result
    {
      get { return _Result; }
      set
      {
        if (value != _Result)
        {
          _Result = value;
          NotifyPropertyChanged();
        }
      }
    }

    

  }
}
