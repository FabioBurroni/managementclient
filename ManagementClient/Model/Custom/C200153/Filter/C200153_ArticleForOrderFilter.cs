﻿using System.Collections.Generic;
using Utilities.Extensions;


namespace Model.Custom.C200153.Filter
{
  public class C200153_ArticleForOrderFilter : ModelBase
  {

    public C200153_ArticleForOrderFilter()
    {
   
    }

    #region Paginazione
    private int _Index = 0;
    public int Index
    {
      get { return _Index; }
      set
      {
        if (value != _Index)
        {
          _Index = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _MaxItems = 100;
    public int MaxItems
    {
      get { return _MaxItems; }
      set
      {
        if (value != _MaxItems)
        {
          _MaxItems = value;
          NotifyPropertyChanged();
        }
      }
    }

    public ObservableCollectionFast<int> MaxItemElements { get; set; } 
      = new ObservableCollectionFast<int>() { 10, 100, 500, 1000,5000,10000,30000};
    #endregion

    private string _BatchCode = string.Empty;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _ArticleCode = string.Empty;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _Depositor = string.Empty;
    public string Depositor
    {
      get { return _Depositor; }
      set
      {
        if (value != _Depositor)
        {
          _Depositor = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsAvailable;
    public bool IsAvailable
    {
      get { return _IsAvailable; }
      set
      {
        if (value != _IsAvailable)
        {
          _IsAvailable = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _OnlyForMyPositon;
    public bool OnlyForMyPositon
    {
      get { return _OnlyForMyPositon; }
      set
      {
        if (value != _OnlyForMyPositon)
        {
          _OnlyForMyPositon = value;
          NotifyPropertyChanged();
        }
      }
    }




    #region public methods
    public void Reset()
    {
      BatchCode = string.Empty;
      ArticleCode = string.Empty; 
      Depositor = string.Empty;
      IsAvailable = false;
    }
    #endregion

    public List<string> GetXmlCommandParameters()
    {
      return new List<string>
      {
        IsAvailable.ToString(),
        BatchCode==null?"":BatchCode.Base64Encode(),
        ArticleCode==null?"":ArticleCode.Base64Encode(),
        Depositor ==null?"":Depositor.Base64Encode(),
        OnlyForMyPositon.ToString()
      };
    }

  }
}
