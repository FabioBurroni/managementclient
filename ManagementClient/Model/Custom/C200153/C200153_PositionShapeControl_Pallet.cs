﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom.C200153
{
  public class C200153_PositionShapeControl_Pallet : ModelBase
  {

    private string _PositionCode;
    public string PositionCode
    {
      get { return _PositionCode; }
      set
      {
        if (value != _PositionCode)
        {
          _PositionCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _PalletCode=string.Empty;
    public string PalletCode
    {
      get { return _PalletCode; }
      set
      {
        if (value != _PalletCode)
        {
          _PalletCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsService;
    public bool IsService
    {
      get { return _IsService; }
      set
      {
        if (value != _IsService)
        {
          _IsService = value;
          NotifyPropertyChanged();
      }
    }
    }

    private C200153_CustomResult _CustomResult;
    public C200153_CustomResult CustomResult
    {
      get { return _CustomResult; }
      set
      {
        if (value != _CustomResult)
        {
          _CustomResult = value;
          NotifyPropertyChanged();
          if(_CustomResult!= C200153_CustomResult.OK)
          {
            IsCustomReject = true;
          }
          else
          {
            IsCustomReject = false;
          }
        }
      }
    }



    private bool _IsCustomReject;
    public bool IsCustomReject
    {
      get { return _IsCustomReject; }
      set
      {
        if (value != _IsCustomReject)
        {
          _IsCustomReject = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsReject;
    public bool IsReject
    {
      get { return _IsReject; }
      set
      {
        if (value != _IsReject)
        {
          _IsReject = value;
          NotifyPropertyChanged();
        }
      }
    }


    private Results_Enum _RejectResult;
    public Results_Enum RejectResult
    {
      get { return _RejectResult; }
      set
      {
        if (value != _RejectResult)
        {
          _RejectResult = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleCode;
    public string ArticleCode
    {
      get { return _ArticleCode; }
      set
      {
        if (value != _ArticleCode)
        {
          _ArticleCode = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _ArticleDescr;
    public string ArticleDescr
    {
      get { return _ArticleDescr; }
      set
      {
        if (value != _ArticleDescr)
        {
          _ArticleDescr = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _Weight;
    public int Weight
    {
      get { return _Weight; }
      set
      {
        if (value != _Weight)
        {
          _Weight = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _Height;
    public int Height
    {
      get { return _Height; }
      set
      {
        if (value != _Height)
        {
          _Height = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsLabeled;
    public bool IsLabeled
    {
      get { return _IsLabeled; }
      set
      {
        if (value != _IsLabeled)
        {
          _IsLabeled = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsWrapped;
    public bool IsWrapped
    {
      get { return _IsWrapped; }
      set
      {
        if (value != _IsWrapped)
        {
          _IsWrapped = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _BatchCode;
    public string BatchCode
    {
      get { return _BatchCode; }
      set
      {
        if (value != _BatchCode)
        {
          _BatchCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _BatchLength;
    public int BatchLength
    {
      get { return _BatchLength; }
      set
      {
        if (value != _BatchLength)
        {
          _BatchLength = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _DepositorCode;
    public string DepositorCode
    {
      get { return _DepositorCode; }
      set
      {
        if (value != _DepositorCode)
        {
          _DepositorCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _DepositorAddress;
    public string DepositorAddress
    {
      get { return _DepositorAddress.Replace('\n', new char()).Replace('\r', new char()); }
      set
      {
        if (value != _DepositorAddress)
        {
          _DepositorAddress = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _ProductionDate = DateTime.MinValue;
    public DateTime ProductionDate
    {
      get { return _ProductionDate; }
      set
      {
        if (value != _ProductionDate)
        {
          _ProductionDate = value;
          NotifyPropertyChanged();
        }
      }
    }

    private DateTime _ExpiryDate = DateTime.MinValue;
    public DateTime ExpiryDate
    {
      get { return _ExpiryDate; }
      set
      {
        if (value != _ExpiryDate)
        {
          _ExpiryDate = value;
          NotifyPropertyChanged();
        }
      }
    }
  }
}
