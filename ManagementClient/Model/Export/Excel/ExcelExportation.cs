﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ExtendedUtilities.FileDialog;
using Localization;
using MaterialDesignThemes.Wpf;
using OfficeOpenXml;

namespace Model.Export.Excel
{
	public static class ExcelExportation
	{
		#region ExportToExcel

		public static string FileCreatedName { get; set; } = "";

		/// <summary>
		/// Esporta in formato Excel 2007 tramite la libreria EPPlus http://epplus.codeplex.com/
		/// Si devono passare anche le colonne da esportare, perchè non tutte le colonne della datagridview devono essere esportare nel foglio excel
		/// </summary>
		/// <param name="exportables">Lista dei record da esportare</param>
		/// <param name="worksheetTitle">Titolo della cartella di lavoro (non tradotto)</param>
		public static async Task<ExportResult> ExportToExcel(this IList<IExportable> exportables, DialogHost dialogHost, string title)
		{
			if (exportables == null)
			{
				return ExportResult.EMPTY_LIST;
				throw new ArgumentNullException(nameof(exportables));
			}

			if (!exportables.Any())
			{
				return ExportResult.EMPTY_LIST;
				throw new ArgumentException("Cannot export empty collection");
			}

			if (exportables.GroupBy(e => e.GetType()).Count() > 1)
			{
				return ExportResult.TYPE_NOT_VALID;
				throw new ArgumentException("Cannot export multiple types at once");
			}

			try
			{
				var save = await SaveFileDialogs(title, dialogHost);
				if (!save.Canceled)
				{
					if (!string.IsNullOrEmpty(save.File))
					{
						FileCreatedName = save.File;
						FileInfo fi = new FileInfo(save.File);

						if (fi.Extension == ".xlsx")
						{
							return ExportToExcelByEPPlus(exportables, title, save.File);
						}
						else
						{
							return ExportResult.FILE_EXTENSION_NOT_VALID;
						}
					}
					else
					{
						return ExportResult.FILE_NAME_NOT_VALID;
					}
				}
				else
				{
					return ExportResult.CANCELED;
				}
			}
			catch (Exception ex)
			{
				string exc = ex.ToString();

				return ExportResult.FAIL;
			}

		}

		private static async Task<SaveFileDialogResult> SaveFileDialogs(string title, DialogHost dialogHost)
		{
			SaveFileDialogArguments dialogArgs = new SaveFileDialogArguments()
			{
				Width = 1000,
				Height = 1000,
				CurrentDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
				CreateNewDirectoryEnabled = true,
				ShowHiddenFilesAndDirectories = false,
				ShowSystemFilesAndDirectories = true,
				SwitchPathPartsAsButtonsEnabled = true,
				PathPartsAsButtons = true,

				Filename = CleanFileName($"{Localize.LocalizeDefaultString(title)} {DateTime.Now.ToString(GetDateFormat())}", "-") + ".xlsx",
				ForceFileExtensionOfFileFilter = true,
				Filters = $@"{Localize.LocalizeDefaultString("Excel Workbook")} (*.xlsx)|*.xlsx",
				FilterIndex = 0,
				CurrentFile = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + CleanFileName($"{Localize.LocalizeDefaultString(title)} {DateTime.Now.ToString(GetDateFormat())}", "-") + ".xlsx"
			};


			SaveFileDialogResult result = await SaveFileDialog.ShowDialogAsync(dialogHost.Identifier.ToString(), dialogArgs);

			return result;
		}
		#endregion

		#region ExportToExcelByEPPlus

		private static ExportResult ExportToExcelByEPPlus(this IList<IExportable> exportables, string worksheetTitle, string saveFileName)
		{
			using (var package = new ExcelPackage())
			{
				//traduco il titolo
				worksheetTitle = Localize.LocalizeDefaultString(worksheetTitle);

				// add a new worksheet to the empty workbook
				ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(worksheetTitle);

				package.Workbook.Properties.Title = worksheetTitle;
				package.Workbook.Properties.Company = "Cassioli S.r.l.";
				package.Workbook.Properties.Author = "Cassioli S.r.l.";
				package.Workbook.Properties.Comments = "Self Generated Report";

				int currentRow = 0, currentColumn = 0;

				//mi precarico tutte le definizioni dell'esportazione
				var exportationDefinitions = new ExportationDefinitions(exportables.First());

				//scorro tutte righe da esportare
				foreach (var exp in exportables)
				{
					currentColumn = 0;

					var type = exp.GetType();

					//scorro tutte le proprietà ordinate che devo esportare
					foreach (var definition in exportationDefinitions.SortedDefinitions)
					{
						//recupero la proprietà che corrisponde al nome
						var prop = type.GetProperty(definition.Name);

						//controllo che non sia nulla, ma giusto per scrupolo, ci deve essere, le ho ricavate prima...
						if (prop == null)
							throw new NullReferenceException($"{definition.Name} not found");

						//recupero il valore
						var value = prop.GetValue(exp);


						if (currentRow == 0)
						{
							//è la prima riga, scrivo i nomi delle colonne
							var localizedColumnName = Localize.LocalizeDefaultString(definition.ColumnName);
							worksheet.Cells[currentRow + 1, currentColumn + 1].Value = localizedColumnName;
						}

						worksheet.Cells[currentRow + 2, ++currentColumn].Value = value;
					}

					++currentRow;
				}

				#region Excel Alignment

				//Usare se si vuole l'allineamento centrale del valore in cella
				//using (var excelRange = worksheet.Cells[1, 1, ++currentRow, currentColumn])
				//{
				//  excelRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
				//  excelRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
				//}

				#endregion

				#region Header Bold

				//header a bold
				using (var excelRange = worksheet.Cells[1, 1, 1, currentColumn])
				{
					excelRange.Style.Font.Bold = true;
				}

				#endregion

				#region AutoFitColumns

				worksheet.Cells.AutoFitColumns();

				#endregion

				#region Format

				//scorro tutte le proprietà ordinate che devo esportare
				var definitions = exportationDefinitions.SortedDefinitions;
				for (var i = 0; i < definitions.Count; i++)
				{
					var definition = definitions[i];

					#region DateTime Format

					if (new[] { typeof(DateTime), typeof(DateTime?) }.Contains(definition.Type))
					{
						//la colonna è tipo datetime, metto la formattazione speciale


						//devo sommare 1 alle colonne perchè si parte da 0
						using (var excelRange = worksheet.Cells[2, i + 1, currentRow + 1, i + 1])
						{
							excelRange.Style.Numberformat.Format = GetDateTimeFormat().FixExcelDateTimeFormat() /*"HH:mm:ss dd/MM/yyyy"*/ /*"MM/dd/yyyy hh:mm:ss AM/PM"*/;
						}

						//la data richiede una lunghezza minima di colonna perchè è calcolata, altrimenti compaiono i #### e tocca allungare la colonna a mano
						worksheet.Column(i + 1).Width = 22;
					}

					#endregion
				}

				#endregion

				//Salvo il file
				package.SaveAs(new FileInfo(saveFileName));

				FileCreatedName = saveFileName;

				return ExportResult.OK;

			}
		}

		/// <summary>
		/// Restituisce il formato attuale della data in base alla lingua impostata
		/// </summary>
		private static string GetDateFormat()
		{
			//recupero la cultura attuale
			var culture = CultureManager.Instance.CultureInfo;

			//restituisco il formato della data
			return culture.DateTimeFormat.ShortDatePattern;
		}

		/// <summary>
		/// Restituisce il formato attuale della data e dell'ora in base alla lingua impostata
		/// </summary>
		private static string GetDateTimeFormat()
		{
			//recupero la cultura attuale
			var culture = CultureManager.Instance.CultureInfo;

			//http://www.basicdatepicker.com/samples/cultureinfo.aspx
			//il formato restituito è una concatenazione di data e ora 
			return $"{culture.DateTimeFormat.ShortDatePattern} {culture.DateTimeFormat.LongTimePattern}";
		}

		/// <summary>
		/// Rimuove i caratteri invalidi dal nome del file e li sostituisce con quello indicato
		/// </summary>
		private static string CleanFileName(this string fileName, string replacement)
		{
			string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
			var r = new System.Text.RegularExpressions.Regex(string.Format("[{0}]", System.Text.RegularExpressions.Regex.Escape(regexSearch)));
			return r.Replace(fileName, replacement);
		}

		/// <summary>
		/// Mette a posto il formato della data per utilizzarlo in un file excel
		/// </summary>
		private static string FixExcelDateTimeFormat(this string dateTimeFormat)
		{
			//sostituisco l'eventuale stringa 'tt' nella stringa 'AM/PM' perchè excel non la digerisce
			return dateTimeFormat.Replace("tt", "AM/PM");
		}


		#region Support Class

		private class ExportationDefinitions
		{
			private readonly SortedList<int, Definition> _definitions = new SortedList<int, Definition>();

			public ExportationDefinitions(IExportable exportable)
			{
				if (exportable == null)
					throw new ArgumentNullException(nameof(exportable));

				LoadDefinitions(exportable);
			}

			private void LoadDefinitions(IExportable exportable)
			{
				//scorro tutte le proprietà della classe
				foreach (var property in exportable.GetType().GetProperties())
				{
					//recupero l'attributo per l'esportazione (se presente)
					var expAttr = property.GetCustomAttributes(true).OfType<ExportationAttribute>().SingleOrDefault();
					if (expAttr == null)
						continue;

					//aggiungo l'indice e il nome della proprietà (se schianta tutto, vi ritrovate qui, metti gli indici a modo!)
					_definitions.Add(expAttr.ColumnIndex, new Definition(expAttr.ColumnIndex, property.Name, property.PropertyType, expAttr.ColumnName));
				}
			}

			public IList<Definition> SortedDefinitions => _definitions.Values.ToArray();
		}

		private class Definition
		{
			public Definition(int index, string name, Type type, string columnName)
			{
				Index = index;
				Name = name;
				Type = type;
				ColumnName = columnName;
			}

			public int Index { get; }
			public string Name { get; }
			public Type Type { get; }
			public string ColumnName { get; }
		}

		#endregion

		#endregion
	}
}