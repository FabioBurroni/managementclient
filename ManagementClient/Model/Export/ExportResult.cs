﻿
namespace Model.Export
{
  public enum ExportResult
  {

    UNDEFINED = 0,
    OK = 1,
    FAIL = 2,
    FILE_EXTENSION_NOT_VALID = 3,
    CANCELED = 4,
    FILE_NAME_NOT_VALID = 5,
    CONFIGURATION_NOT_VALID = 6,
    FILE_IMPOSSIBILE_TO_OVERWRITE = 7,
    EMPTY_LIST = 8,
    TYPE_NOT_VALID = 9
  }
}
