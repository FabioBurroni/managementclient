﻿using System;
using System.Windows.Media;

namespace Model.Export.Pdf
{
  public class PdfFormat
  {
    public string Title { get; set; }

    public string FileName { get; set; }

    public bool Landscape { get; set; }

    public Header Header { get; set; }

    public Main Main { get; set; }

    public Footer Footer { get; set; }
  }

  public class Header
  {
    public bool IsEnabled { get; set; }

    public string Logo1Path { get; set; }

    public Position Logo1Position { get; set; }

    public string Logo2Path { get; set; }

    public Position Logo2Position { get; set; }

    public Position ContentPosition { get; set; }

    public string Title { get; set; }

    public DateTime DateTime { get; set; }

    public string Author { get; set; }

    public string Note { get; set; }
  }

  public class Footer
  {
    public bool IsEnabled { get; set; }

    public Position FileNamePosition { get; set; }

    public Position PageNumberPosition { get; set; }
  }

  public class Main
  {
    public bool RepeteHeader { get; set; }

    public System.Reflection.PropertyInfo[] Info { get; set; }

    public string[] Headers { get; set; }

    public float[] Sizes { get; set; }

    public Color[] ColorHeader { get; set; }
  }

  public enum Position
  {
    None = 0,
    Left = 1,
    Center = 2,
    Right = 3
  }
}