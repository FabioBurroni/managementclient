﻿using ExtendedUtilities.FileDialog;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Localization;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Model.Export.Pdf
{
  public static class PdfExportation
  {
    public static string FileCreatedName { get; set; } = "";

    public static string CleanFileName(string fileName, string replacement)
    {
      string regexSearch = new string(System.IO.Path.GetInvalidFileNameChars()) + new string(System.IO.Path.GetInvalidPathChars());
      var r = new System.Text.RegularExpressions.Regex(string.Format("[{0}]", System.Text.RegularExpressions.Regex.Escape(regexSearch)));
      return r.Replace(fileName, replacement);
    }

    private static string GetDateFormat()
    {
      //recupero la cultura attuale
      var culture = CultureManager.Instance.CultureInfo;

      //restituisco il formato della data
      return culture.DateTimeFormat.ShortDatePattern;
    }

    /// <summary>
    /// Export in  pdf file
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="L">collection to Export</param>
    /// <param name="info">List of properties to export</param>
    /// <param name="columnheader">list of columns header</param>
    /// <param name="columnwidth">list of columns width</param>
    /// <param name="landscape">is landscape format</param>
    /// <param name="title">Report title</param>
    public static async Task<ExportResult> Export<T>(DialogHost dialogHost, IList<T> L, PdfFormat pdfFormat)
    {
      var save = await SaveFileDialogs(pdfFormat.Title, dialogHost);
      if (!save.Canceled)
      {
        if (!string.IsNullOrEmpty(save.File))
        {
          FileCreatedName = save.File;
          FileInfo fi = new FileInfo(save.File);

          if (fi.Extension == ".pdf")
          {
            return ExportToPdf(fi.FullName, L, pdfFormat);
          }
          else
          {
            return ExportResult.FILE_EXTENSION_NOT_VALID;
          }
        }
        else
        {
          return ExportResult.FILE_NAME_NOT_VALID;
        }
      }
      else
      {
        return ExportResult.CANCELED;
      }
    }

    private static async Task<SaveFileDialogResult> SaveFileDialogs(string title, DialogHost dialogHost)
    {
      SaveFileDialogArguments dialogArgs = new SaveFileDialogArguments()
      {
        Width = 1000,
        Height = 1000,
        CurrentDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
        CreateNewDirectoryEnabled = true,
        ShowHiddenFilesAndDirectories = false,
        ShowSystemFilesAndDirectories = true,
        SwitchPathPartsAsButtonsEnabled = true,
        PathPartsAsButtons = true,

        Filename = CleanFileName($"{Localize.LocalizeDefaultString(title)} {DateTime.Now.ToString(GetDateFormat())}", "-") + ".pdf",
        ForceFileExtensionOfFileFilter = true,
        Filters = "pdf files (*.pdf)|*.pdf ",
        FilterIndex = 0,
        CurrentFile = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + CleanFileName($"{Localize.LocalizeDefaultString(title)} {DateTime.Now.ToString(GetDateFormat())}", "-") + ".pdf"
      };

      SaveFileDialogResult result = await SaveFileDialog.ShowDialogAsync(dialogHost.Identifier.ToString(), dialogArgs);

      return result;
    }

    private static ExportResult ExportToPdf<T>(string filename, IList<T> genericList, PdfFormat pdfFormat)
    {
      BaseFont bf = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
      Font f = new Font(bf, 8, Font.NORMAL);

      if (pdfFormat.Main.Info.Count() == pdfFormat.Main.Headers.Count() && pdfFormat.Main.Info.Count() == pdfFormat.Main.Sizes.Count())
      {
        var pdfDocument = new Document();
        var maxHeight = 0; //max table height

        //check landscape or portrait (default)
        if (pdfFormat.Landscape)
        {
          pdfDocument = new Document(PageSize.A4.Rotate(), 20, 20, 10, 20); //formato orizzontale
          maxHeight = 400;
        }
        else
        {
          pdfDocument = new Document(PageSize.A4, 20, 20, 10, 20); //formato verticale
          maxHeight = 650;
        }

        var pdfFile = filename;
        pdfFormat.FileName = filename;

        try
        {
          if (File.Exists(pdfFile))
            File.Delete(pdfFile);
        }
        catch (Exception ex)
        {
          string exc = ex.ToString();
          return ExportResult.FILE_IMPOSSIBILE_TO_OVERWRITE;
        }

        var pdfWriter = PdfWriter.GetInstance(pdfDocument, new FileStream(pdfFile, FileMode.Create));
        pdfWriter.PageEvent = new ITextEvents(pdfFormat);

        pdfDocument.Open();

        pdfDocument.AddAuthor("Cassioli");
        pdfDocument.AddCreationDate();

        pdfDocument.AddTitle(pdfFormat.Title);

        try
        {
          if (!pdfFormat.Header.IsEnabled)
          {
            Paragraph para = new Paragraph(Localize.LocalizeDefaultString("TITLE") + "   :   " + pdfFormat.Title, new Font(bf, 18, Font.NORMAL));
            // Setting paragraph's text alignment using iTextSharp.text.Element class
            para.Alignment = Element.ALIGN_JUSTIFIED;
            // Adding this 'para' to the Document object
            pdfDocument.Add(para);

            para = new Paragraph(Localize.LocalizeDefaultString("DATE") + "   :   " + DateTime.Now.ToString("dd/MM/yyyy HH:mm"), new Font(bf, 18, Font.NORMAL));
            // Setting paragraph's text alignment using iTextSharp.text.Element class
            para.Alignment = Element.ALIGN_JUSTIFIED;
            para.SpacingAfter = 15;
            // Adding this 'para' to the Document object
            pdfDocument.Add(para);
          }

          //nomi delle colonne...
          var infoH = pdfFormat.Main.Headers;
          // numeri delle colonne...
          var pdfPTable = new PdfPTable(infoH.Count());
          pdfPTable.TotalWidth = pdfDocument.PageSize.Width - 40;

          foreach (var prop in infoH)
          {
            var color = Color.FromRgb(254, 222, 179);
            try
            {
              var index = Array.IndexOf(infoH, prop);
              color = pdfFormat.Main.ColorHeader.ElementAt(index);
            }
            catch (Exception) { }

            var pdfPCell = new PdfPCell(new Phrase(prop, f));
            pdfPCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            pdfPCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
            pdfPCell.BackgroundColor = new BaseColor(color.R, color.G, color.B);
            pdfPTable.AddCell(pdfPCell);
          }

          // set larghezza colonne
          pdfPTable.SetWidths(pdfFormat.Main.Sizes);

          foreach (var obj in genericList)
          {
            foreach (var prop in pdfFormat.Main.Info)
            {
              string val = prop.GetValue(obj, null)?.ToString() ?? "";
              var Cell = new PdfPCell(new Phrase(val, f));
              Cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
              Cell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
              pdfPTable.AddCell(Cell);

              if (pdfPTable.TotalHeight > maxHeight)
              {
                pdfPTable.WriteSelectedRows(0, -1, pdfDocument.Left, pdfDocument.Top - 100, pdfWriter.DirectContent);
                pdfDocument.NewPage();
                pdfPTable = new PdfPTable(infoH.Count());
                pdfPTable.TotalWidth = pdfDocument.PageSize.Width - 40;

                if (pdfFormat.Main.RepeteHeader)
                {
                  foreach (var prop2 in infoH)
                  {
                    var color = Color.FromRgb(254, 222, 179);
                    try
                    {
                      var index = Array.IndexOf(infoH, prop2);
                      color = pdfFormat.Main.ColorHeader.ElementAt(index);
                    }
                    catch (Exception) { }

                    var pdfPCell = new PdfPCell(new Phrase(prop2, f));
                    pdfPCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    pdfPCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                    pdfPCell.BackgroundColor = new BaseColor(color.R, color.G, color.B);
                    pdfPTable.AddCell(pdfPCell);
                  }
                }
              }
            }
          }

          pdfPTable.WriteSelectedRows(0, -1, pdfDocument.Left, pdfDocument.Top - 100, pdfWriter.DirectContent);
        }
        catch (Exception ex)
        {
          throw ex;
        }
        finally
        {
          pdfDocument.Add(new Phrase(" "));
          pdfDocument.Close();
        }

        return ExportResult.OK;
      }
      return ExportResult.CONFIGURATION_NOT_VALID;
    }
  }

  public class ITextEvents : PdfPageEventHelper
  {
    // This is the contentbyte object of the writer
    private PdfContentByte cb;

    // we will put the final number of pages in a template
    private PdfTemplate footerTemplate;

    // this is the BaseFont we are going to use for the header / footer
    private BaseFont bf = null;

    private PdfFormat pdfFormat = new PdfFormat();

    public ITextEvents(PdfFormat _pdfFormat)
    {
      pdfFormat = _pdfFormat;
    }

    public override void OnOpenDocument(PdfWriter writer, Document document)
    {
      try
      {
        bf = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        cb = writer.DirectContent;
        footerTemplate = cb.CreateTemplate(50, 50);
      }
      catch (DocumentException de)
      {
        string exc = de.ToString();
      }
      catch (IOException ioe)
      {
        string exc = ioe.ToString();
      }
    }

    public override void OnEndPage(PdfWriter writer, Document document)
    {
      base.OnEndPage(writer, document);

      PdfGState state = new PdfGState();
      state.FillOpacity = 0.5f;
      state.StrokeOpacity = 0.5f;

      //Define table header
      PdfPTable pdfTab = new PdfPTable(3);
      pdfTab.SetWidths(new float[] { 1f, 1f, 1f });
      pdfTab.TotalWidth = document.PageSize.Width - 20;
      pdfTab.DefaultCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;

      PdfPCell cell1 = new PdfPCell(new Phrase(""));
      PdfPCell cell2 = new PdfPCell(new Phrase(""));
      PdfPCell cell3 = new PdfPCell(new Phrase(""));

      cell1.Border = Rectangle.NO_BORDER;
      cell2.Border = Rectangle.NO_BORDER;
      cell3.Border = Rectangle.NO_BORDER;

      #region HEADER PAGE NUMBER

      if (pdfFormat.Footer.IsEnabled && pdfFormat.Footer.PageNumberPosition != 0)
      {
        String text = Localize.LocalizeDefaultString("PAGE") + " " + writer.PageNumber + " " + Localize.LocalizeDefaultString("OF") + " ";

        if (pdfFormat.Footer.PageNumberPosition == Position.Left)
        {
          cb.BeginText();
          cb.SetFontAndSize(bf, 12);
          cb.SetTextMatrix(document.PageSize.GetLeft(100), document.PageSize.GetBottom(30));
          cb.ShowText(text);
          cb.EndText();
          float len = bf.GetWidthPoint(text, 12);
          cb.AddTemplate(footerTemplate, document.PageSize.GetLeft(100) + len, document.PageSize.GetBottom(30));
        }
        else if (pdfFormat.Footer.PageNumberPosition == Position.Right)
        {
          cb.BeginText();
          cb.SetFontAndSize(bf, 12);
          cb.SetTextMatrix(document.PageSize.GetRight(180), document.PageSize.GetBottom(30));
          cb.ShowText(text);
          cb.EndText();
          float len = bf.GetWidthPoint(text, 12);
          cb.AddTemplate(footerTemplate, document.PageSize.GetRight(180) + len, document.PageSize.GetBottom(30));
        }
      }

      #endregion HEADER PAGE NUMBER

      #region HEADER

      //set Cassioli logo
      if (pdfFormat.Header.IsEnabled)
      {
        if (pdfFormat.Header.Logo1Position != 0 && File.Exists(pdfFormat.Header.Logo1Path))
        {
          var logo1 = Image.GetInstance(pdfFormat.Header.Logo1Path);

          if(pdfFormat.Landscape)
          {
            // Figure out the ratio
            float ratioX = 230 / logo1.Width;
            float ratioY = 85 / logo1.Height;
            // use whichever multiplier is smaller
            float ratio = ratioX < ratioY ? ratioX : ratioY;
            // now we can get the new height and width
            float newHeight = logo1.Height * ratio;
            float newWidth = Convert.ToInt32(logo1.Width * ratio);
            // Now calculate the X,Y position of the upper-left corner 
            // (one of these will always be zero)
            float posX = (230 - (logo1.Width * ratio)) / 2;
            float posY = (85 - (logo1.Height * ratio)) / 2;

            logo1.ScaleAbsoluteHeight(newHeight);
            logo1.ScaleAbsoluteWidth(newWidth);
            logo1.Alignment = Image.ALIGN_CENTER;

            if (pdfFormat.Header.Logo1Position == Position.Left)
              logo1.SetAbsolutePosition(25 + posX, document.Top - logo1.ScaledHeight - posY);
            else if (pdfFormat.Header.Logo1Position == Position.Center)
              logo1.SetAbsolutePosition(305 + posX, document.Top - logo1.ScaledHeight - posY);
            else if (pdfFormat.Header.Logo1Position == Position.Right)
              logo1.SetAbsolutePosition(585 + posX, document.Top - logo1.ScaledHeight - posY);
          }
          else
          {
            float ratioX = 190 / logo1.Width;
            float ratioY = 85 / logo1.Height;

            float ratio = ratioX < ratioY ? ratioX : ratioY;

            float newHeight = logo1.Height * ratio;
            float newWidth = logo1.Width * ratio;

            float posX = (190 - (logo1.Width * ratio)) / 2;
            float posY = (5 - (logo1.Height * ratio)) / 2;

            logo1.ScaleAbsoluteHeight(newHeight);
            logo1.ScaleAbsoluteWidth(newWidth);
            logo1.Alignment = Image.ALIGN_CENTER;

            if (pdfFormat.Header.Logo1Position == Position.Left)
              logo1.SetAbsolutePosition(4 + posX, document.Top - logo1.ScaledHeight - posY);
            else if (pdfFormat.Header.Logo1Position == Position.Center)
              logo1.SetAbsolutePosition(194 + posX, document.Top - logo1.ScaledHeight - posY);
            else if (pdfFormat.Header.Logo1Position == Position.Right)
              logo1.SetAbsolutePosition(384 + posX, document.Top - logo1.ScaledHeight - posY);
          }

          document.Add(logo1);
        }

        //set customer logo
        if (pdfFormat.Header.Logo2Position != 0 && File.Exists(pdfFormat.Header.Logo2Path))
        {
          var logo2 = Image.GetInstance(pdfFormat.Header.Logo2Path);
          if (pdfFormat.Landscape)
          {
            float ratioX = 230 / logo2.Width;
            float ratioY = 85 / logo2.Height;

            float ratio = ratioX < ratioY ? ratioX : ratioY;

            float newHeight = logo2.Height * ratio;
            float newWidth = logo2.Width * ratio;

            float posX = (230 - (logo2.Width * ratio)) / 2;
            float posY = (85 - (logo2.Height * ratio)) / 2;

            logo2.ScaleAbsoluteHeight(newHeight);
            logo2.ScaleAbsoluteWidth(newWidth);
            logo2.Alignment = Image.ALIGN_CENTER;

            if (pdfFormat.Header.Logo2Position == Position.Left)
              logo2.SetAbsolutePosition(25 + posX, document.Top - logo2.ScaledHeight - posY);
            else if (pdfFormat.Header.Logo2Position == Position.Center)
              logo2.SetAbsolutePosition(305 + posX, document.Top - logo2.ScaledHeight - posY);
            else if (pdfFormat.Header.Logo2Position == Position.Right)
              logo2.SetAbsolutePosition(585 + posX, document.Top - logo2.ScaledHeight - posY);
          }
          else
          {
            float ratioX = 190 / logo2.Width;
            float ratioY = 85 / logo2.Height;

            float ratio = ratioX < ratioY ? ratioX : ratioY;

            float newHeight = logo2.Height * ratio;
            float newWidth = logo2.Width * ratio;

            float posX = (190 - (logo2.Width * ratio)) / 2;
            float posY = (85 - (logo2.Height * ratio)) / 2;

            logo2.ScaleAbsoluteHeight(newHeight);
            logo2.ScaleAbsoluteWidth(newWidth);
            logo2.Alignment = Image.ALIGN_CENTER;

            if (pdfFormat.Header.Logo2Position == Position.Left)
              logo2.SetAbsolutePosition(4 + posX, document.Top - logo2.ScaledHeight - posY);
            else if (pdfFormat.Header.Logo2Position == Position.Center)
              logo2.SetAbsolutePosition(194 + posX, document.Top - logo2.ScaledHeight - posY);
            else if (pdfFormat.Header.Logo2Position == Position.Right)
              logo2.SetAbsolutePosition(384 + posX, document.Top - logo2.ScaledHeight - posY);
          }

          document.Add(logo2);
        }

        //set content
        if (pdfFormat.Header.ContentPosition != 0)
        {
          var mainText = default(string);
          var note = default(string);

          if (!string.IsNullOrEmpty(pdfFormat.Header.Title))
            mainText += pdfFormat.Header.Title + "\n";
          if (!string.IsNullOrEmpty(pdfFormat.Header.Author))
            mainText += Localize.LocalizeDefaultString("AUTHOR") + ": " + pdfFormat.Header.Author;
          if (pdfFormat.Header.DateTime != null)
            note += pdfFormat.Header.DateTime.Date.ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern) + "\n";
          if (!string.IsNullOrEmpty(pdfFormat.Header.Note))
            note += Localize.LocalizeDefaultString("NOTE") + ": " + pdfFormat.Header.Note;

          var paragraph = new Paragraph("");
          var paragraphNote = new Paragraph("");

          if(pdfFormat.Landscape)
          {
            paragraph = new Paragraph(mainText, new Font(bf, 12, Font.NORMAL));
            paragraphNote = new Paragraph(note, new Font(bf, 10, Font.NORMAL));
          }
          else
          {
            paragraph = new Paragraph(mainText, new Font(bf, 9, Font.NORMAL));
            paragraphNote = new Paragraph(note, new Font(bf, 7, Font.NORMAL));
          }

          if (pdfFormat.Header.ContentPosition == Position.Left)
          {
            cell1.AddElement(paragraph);
            cell1.AddElement(paragraphNote);
            cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            cell1.VerticalAlignment = Element.ALIGN_TOP;
          }
          else if (pdfFormat.Header.ContentPosition == Position.Center)
          {
            cell2.AddElement(paragraph);
            cell2.AddElement(paragraphNote);
            cell2.HorizontalAlignment = Element.ALIGN_LEFT;
            cell2.VerticalAlignment = Element.ALIGN_TOP;
          }
          else if (pdfFormat.Header.ContentPosition == Position.Right)
          {
            cell3.AddElement(paragraph);
            cell3.AddElement(paragraphNote);
            cell3.HorizontalAlignment = Element.ALIGN_LEFT;
            cell3.VerticalAlignment = Element.ALIGN_TOP;
          }
        }

        pdfTab.AddCell(cell1);
        pdfTab.AddCell(cell2);
        pdfTab.AddCell(cell3);

        pdfTab.WriteSelectedRows(0, -1, document.Left, document.Top, writer.DirectContent);
      }

      #endregion HEADER

      #region FOOTER FILE NAME

      if (pdfFormat.Footer.IsEnabled)
      {
        if (pdfFormat.Footer.FileNamePosition != 0)
        {
          PdfPTable pdfTabF = new PdfPTable(1);
          pdfTabF.TotalWidth = document.PageSize.Width - 20;

          PdfPCell cell = new PdfPCell(new Phrase(""));
          cell.Border = Rectangle.NO_BORDER;

          var fileInfo = new FileInfo(pdfFormat.FileName);

          if (pdfFormat.Footer.FileNamePosition == Position.Left)
          {
            cell.AddElement(new Phrase(fileInfo.Name));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            pdfTabF.AddCell(cell);

            pdfTabF.WriteSelectedRows(0, -1, document.Left, document.Bottom + 30, writer.DirectContent);
          }
          else if (pdfFormat.Footer.FileNamePosition == Position.Right)
          {
            cell.AddElement(new Phrase(fileInfo.Name));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            pdfTabF.AddCell(cell);

            float len = bf.GetWidthPoint(pdfFormat.FileName, 12);

            pdfTabF.WriteSelectedRows(0, -1, document.Right - len - 20, document.Bottom + 30, writer.DirectContent);
          }
        }
      }

      #endregion FOOTER FILE NAME

      #region LINE SEPARATOR

      cb.SetGState(state);

      if (pdfFormat.Header.IsEnabled)
      {
        //draw header line
        cb.MoveTo(5, document.PageSize.Height - 100);
        cb.LineTo(document.PageSize.Width - 5, document.PageSize.Height - 100);
        cb.Stroke();
      }

      if (pdfFormat.Footer.IsEnabled)
      {
        //draw footer line
        cb.MoveTo(5, document.PageSize.GetBottom(50));
        cb.LineTo(document.PageSize.Width - 5, document.PageSize.GetBottom(50));
        cb.Stroke();
      }

      #endregion LINE SEPARATOR
    }

    public override void OnCloseDocument(PdfWriter writer, Document document)
    {
      base.OnCloseDocument(writer, document);

      // set footer total page number
      if (pdfFormat.Footer.PageNumberPosition != 0 && pdfFormat.Footer.IsEnabled)
      {
        footerTemplate.BeginText();
        footerTemplate.SetFontAndSize(bf, 12);
        footerTemplate.SetTextMatrix(0, 0);
        footerTemplate.ShowText((writer.PageNumber).ToString());
        footerTemplate.EndText();
      }
    }
  }
}