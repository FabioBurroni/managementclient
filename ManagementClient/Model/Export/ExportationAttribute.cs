﻿using System;

namespace Model.Export
{
  [AttributeUsage(AttributeTargets.Property)]
  public class ExportationAttribute : Attribute
  {
    /// <summary>
    /// Indice della colonna
    /// </summary>
    public int ColumnIndex { get; set; }

    /// <summary>
    /// Nome della colonna
    /// </summary>
    public string ColumnName { get; set; }
  }
}