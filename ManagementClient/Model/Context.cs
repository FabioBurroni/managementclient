﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model.Common.Bcr;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlServer;

namespace Model
{
	public sealed class Context
	{
		#region Istanza

		private static readonly Lazy<Context> Lazy = new Lazy<Context>(() => new Context());

		public static Context Instance => Lazy.Value;

		private Context()
		{
		}

		#endregion

		#region XmlClient

		public IXmlClient XmlClient { get; set; }

		#endregion

		#region XmlServer

		public IXmlServer XmlServer { get; set; }

		#endregion

		#region Positions

		/// <summary>
		/// Posizione di riferimento per il client (ne caso in cui il client configurato per più posizioni, viene restituita la prima)
		/// </summary>
		public string Position => Positions.FirstOrDefault();

		/// <summary>
		/// Posizioni di riferimento per il client
		/// </summary>
		public IList<string> Positions { get; } = new List<string>();

    #endregion

    #region BcrManager

    /// <summary>
    /// Istanza del gestore di barcode
    /// </summary>
    public BcrManager BcrManager { get; } = BcrManager.Instance;

    #endregion


    #region TRANSLATE
    public string TranslateAlarmServiceString(string item)
    {
      if (Utilities.Extensions.DesignTimeHelper.IsInDesignMode)
        return item;
      return Localization.Localize.LocalizeAlarmServiceString(item);
    }
    public string TranslateAuthentication(string item)
    {
      if (Utilities.Extensions.DesignTimeHelper.IsInDesignMode)
        return item;
      return Localization.Localize.LocalizeAuthenticationString(item);
    }
    public string TranslateDefault(string item)
    {
      if (Utilities.Extensions.DesignTimeHelper.IsInDesignMode)
        return item;
      return Localization.Localize.LocalizeDefaultString(item);
    }
    #endregion
  }
}