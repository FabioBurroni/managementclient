﻿namespace Model
{
	public class GenericDataResult : GenericResult
	{
		#region Constructor

		public GenericDataResult(string okNok, IModel data) : base(okNok)
		{
			Data = data;
		}

		#endregion

		#region Properties

		public IModel Data { get; }

		#endregion
	}
}