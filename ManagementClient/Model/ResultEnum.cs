﻿namespace ManagementClient.Model
{
    public enum ResultEnum
    {
        UNDEFINED = 0,
        OK = 1,

        SYSTEM_CONFIGURATION_ERROR_UPDATING_PROPERTY = 11,
        SYSTEM_CONFIGURATION_NOT_FOUND = 12,
        SYSTEM_IS_NOT_SETUP_MODE = 13, //20171217 il sistema non è in modalità setup

        #region Session

        SESSION_NOT_FOUND = 50,

        #endregion

        //...GENERICI
        FORMAT_NOT_VALID = 100,
        OUT_OF_RANGE = 101,
        CONFIGURATION_ERROR = 102, //...errore generico di configurazione
        DATABASE_EXCEPTION = 103, //...eccezione esecuzione query
        SETUP_NOT_ALLOWED_WITH_RUNNING_PICKLIST = 104, //errore, non si può cambiare lo stato del sistema con PickList in esecuzione

        //...GENERICI COMANDI XML
        XMLCOMMAND_NUMBER_OF_PARAMETER_NOT_VALID = 200,
        XMLCOMMAND_EXCEPTION_EXECUTING_COMMAND = 201,

        //...barcode
        BCR_NOT_VALID = 301,

        //...Articoli
        ARTICLE_CODE_NOT_VALID = 1001,
        ARTICLE_DESCR_NOT_VALID = 1002,
        ARTICLE_TYPE_NOT_VALID = 1003,
        ARTICLE_WEIGHT_OUT_OF_RANGE = 1004,
        ARTICLE_TRAYHEIGHT_OUT_OF_RANGE = 1005,
        ARTICLE_LOTTIMEFORFIFO_OUT_OF_RANGE = 1006,
        ARTICLE_MAXCONTAINERS_NOT_VALID = 1007,
        ARTICLETYPE_NOT_VALID = 1008,
        ARTICLETYPE_NOT_PRESENT = 1009,
        ARTICLE_ERROR_INSERT = 1010,
        ARTICLE_ERROR_UPDATE = 1011,
        ARTICLEINFO_ERROR_INSERT = 1012,
        ARTICLEINFO_ERROR_UPDATE = 1013,
        ARTICLE_EXCEPTION_INSERTORUPDATE = 1014,
        ARTICLE_NOT_PRESENT = 1015,

        SKUINPICKLIST_NOT_VALID = 1016,
        FAMILY_NOT_VALID = 1017,

        SKU_TRAYTYPE_NOT_COMPLIANT_WITH_DESTINATION = 1018,
        SKU_HIGH_NOT_COMPLIANT_WITH_DESTINATION = 1019,
        SKU_MULTISKU_NOT_COMPLIANT_WITH_DESTINATION = 1020,
        SKU_HIGH_NOT_COMPLIANT_WITH_TRAYTYPE = 1021,
        SKU_NOT_COMPLIANT_WITH_ALREADY_EXISTING_SKU = 1022, //...errore generato quando si cerca di inserire un nuovo articolo che ha parametri dviversi da articoli esistenti con stesso skuInPickList
        SKU_REVISION_NOT_VALID = 1023,
        SKU_TRAYTYPE_CANNOT_BE_CHANGED = 1024, //...il tipo di tray non può essere modificato

        //20180123 
        SKU_DESTINATION_FULL = 1025,    //...destinazione ha già raggiunto numero massimo di articoli configuabili
                                        //20180123 
        SKU_ALREADY_PRESENT = 1026,  //...lo sku è già presente

        SKU_DELETE_NOT_POSSIBLE_PALLET_PRESENT = 1027, //20180306 cancellazione sku non ammessa, pallet contenenti l'articolo presenti nel sistema
        SKU_DELETE_NOT_POSSIBLE_PICKLIST_PRESENT = 1028, //20180306 cancellazione sku non ammessa, picklist presente contenenti lo skuInPickList da cancellare
        SKU_DELETE_NOT_POSSIBLE_ORDER_PRESENT = 1029, //20180306 cancellazione sku non ammessa, ordine presente contenente lo skuInPickList da cancellare
        SKU_DELETE_ERROR = 1030, //20180306 errore cancellazione sku 


        //...prodotti
        PRODUCT_QTY_OUT_OF_RANGE = 1100,
        PRODUCT_GROUP_NOT_PRESENT = 1101,
        PRODUCT_ERROR_INSERT = 1102,
        PRODUCTINFO_ERROR_INSERT = 1103,
        PRODUCT_EXCEPTION_INSERT = 1104,
        PRODUCT_ERROR_LOADING = 1105,
        PRODUCT_NOT_PRESENT = 1106,
        PRODUCT_ERROR_DELETE = 1107,


        //...pallet 
        PALLET_ERROR_INSERT = 1200,
        PALLETINFO_ERROR_INSERT = 1201,
        PALLET_EXCEPTION_INSERT = 1202,
        PALLET_ALREADY_PRESENT = 1203,
        PALLET_NOT_VALID = 1204,
        PALLET_ERROR_CREATING_FINALDEST = 1205,

        PALLET_DIMENSION_ERROR = 1206, //...errore sulle dimensioni del pallet
        PALLET_ERROR_UPDATING = 1207, //...errore aggiornamento dati pallet
        PALLET_NO_AVAILABLE_CELL = 1208, //...celle non disponibili per il pallet

        PALLET_WRONG_DESTINATION = 1209, //...pallet con destinazione errata
        PALLET_NO_AVAILABLE_DESTINATION = 1210, //...nessuna destinazione disponibile per il pallet




        //...pallet_type
        PALLETTYPE_NOT_PRESENT = 1300,
        //...mapping
        MAPPING_ERROR_INSERT = 1401,

        //...posizioni
        POSITION_NOT_PRESENT = 1500,

        #region PickList

        PICKLIST_NOT_FOUND = 1600,
        PICKLIST_COMPONENT_NOT_FOUND = 1601,
        PICKLIST_COMPONENT_NOT_BELONGING_TO_PICKLIST = 1602,
        PICKLIST_STATE_NOT_VALID = 1603,
        CANNOT_KILL_PICKLIST_WITH_PICKPUT_OPERATIONS = 1604,
        PICKLIST_STATE_NOT_SET = 1605,
        PICKLIST_COMPONENT_STATE_NOT_VALID = 1606,
        CANNOT_KILL_PICKLIST_COMPONENT_WITH_PICKPUT_OPERATION = 1607,
        PICKLIST_COMPONENT_STATE_NOT_SET = 1608,
        PICKLIST_MUST_BE_IN_PAUSE = 1609,
        CANNOT_SUBSTITUTE_CART_WHILE_PICKLIST_HAS_OPERATIONS = 1610,
        CANNOT_SUBSTITUTE_CART_IN_PICKLIST_WITHOUT_CART = 1611,
        CANNOT_SUBSTITUTE_CART_WITH_PRODUCTS_INSIDE = 1612,
        ERROR_SUBSTITUTING_CART_IN_PICKLIST = 1613,

        PICKLIST_PRIORITY_NOT_VALID = 1620,

        CANNOT_SKIP_PICKLIST_WITH_CART = 1621,
        CANNOT_SKIP_PICKLIST_WITH_PICKPUT_OPERATIONS = 1622,
        CANNOT_SKIP_PICKLIST = 1623,

        OPERATION_NOT_FOUND = 1630,
        CANNOT_ABORT_OPERATION_WITH_RUNNING_PICKLIST_SET_PAUSE_STATE = 1631,
        CANNOT_ABORT_PICKPUT_OPERATION_WITH_CONTAINER_SET = 1632,
        PICKLIST_ADVANCED_USER_CREDENTIALS_NOT_VALID = 1633,
        PICKLIST_ADVANCED_USER_IS_REQUIRED = 1634,
        PICKLIST_ADVANCED_USER_NOT_AUTHORIZED = 1635,

        ASSOCIATE_CART_OPERATION_NOT_FOUND = 1650,
        OPERATOR_NOT_AUTHORIZED_TO_EXECUTE_OPERATION = 1651,
        CART_BCR_NOT_VALID = 1652,
        ERROR_RETRIEVING_CART = 1653,
        OPERATION_TYPE_NOT_VALID = 1654,
        PICKLIST_NOT_EXECUTABLE = 1655,
        CART_ALREADY_LINKED_TO_ANOTHER_PICKLIST = 1656,
        ERROR_ASSOCIATING_CART_TO_PICKLIST = 1657,
        MOVE_CART_OPERATION_NOT_FOUND = 1658,
        CART_NOT_FOUND = 1658,
        CART_NOT_VALID_SCAN_PROPER_BCR = 1659,
        CART_NOT_VALID = 1660,

        #endregion

        //...request table
        REQUEST_ERROR_INSERT = 2001,
        REQUEST_EXCEPTION_INSERT = 2002,
        REQUEST_NOT_PRESENT = 2003,
        REQUEST_EXCEPTION_UPDATE = 2004,
        REQUEST_ALREADY_PRESENT = 2005,


        //...20171109
        ENGINE_IS_RUNNING = 2500,
    }
}