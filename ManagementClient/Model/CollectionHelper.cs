﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
  public static class CollectionHelper
  {
    public static void UpdateAll<T>(IList<T> currentCollection, IList<T> newCollection, string propName)
    {
      List<T> ret = new List<T>();
      //....elementi da rimuovere
      var elementsToRemove = Collections_NotIn(newCollection, currentCollection, propName);
      //...rimozione
      elementsToRemove.ForEach(elToRemove => currentCollection.Remove(elToRemove));
      //...aggiorno gli elementi in comune
      Collections_Update(currentCollection, newCollection, propName);
      //...elementi da aggiungere
      var elementsToAdd = Collections_NotIn(currentCollection, newCollection, propName);
      elementsToAdd.ForEach(elToAdd => currentCollection.Add(elToAdd));
    }


    /// <summary>
    /// Restituisce una nuova lista con gli elementi delle collection2 che non sono nelle collection1
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="currentCol"></param>
    /// <param name="newCol"></param>
    /// <param name="propName"></param>
    /// <returns></returns>
    public static List<T> Collections_NotIn<T>(IList<T> collection1, IList<T> collection2, string propName)
    {
      return collection2.Where(nce => !collection1.Any(cc => ObjectsAreEqual(nce, cc, propName))).ToList<T>();
    }

    /// <summary>
    /// Restituisce gli elementi della collection2 che sono anche nelle collection1
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection1"></param>
    /// <param name="collection2"></param>
    /// <param name="propName"></param>
    /// <returns></returns>
    public static List<T> Collections_In<T>(IList<T> collection1, IList<T> collection2, string propName)
    {
      return collection2.Where(nce => collection1.Any(cc => ObjectsAreEqual(nce, cc, propName))).ToList<T>();
    }

    /// <summary>
    /// Aggiorna gli elementi della currentCollection che sono presenti nelle newCollection. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="currentCollection"></param>
    /// <param name="newCollection"></param>
    /// <param name="propName"></param>
    private static void Collections_Update<T>(IList<T> currentCollection, IList<T> newCollection, string propName)
    {
      foreach (var el1 in currentCollection)
      {
        var newElement = newCollection.FirstOrDefault(el2 => ObjectsAreEqual(el1, el2, propName));
        ((IUpdatable)el1).Update((IUpdatable)newElement);
      }
    }

    private static bool ObjectsAreEqual(object obj1, object obj2, string propName)
    {
      var ret = GetPropValue(obj1, propName).ToString().ToLower() == GetPropValue(obj2, propName).ToString().ToLower();
      return ret;
    }

    private static object GetPropValue(object src, string propName)
    {
      var val = src.GetType().GetProperty(propName).GetValue(src, null);
      return val;
    }

  }
}
