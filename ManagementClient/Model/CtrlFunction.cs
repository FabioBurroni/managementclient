﻿using System.Windows.Controls;

namespace Model
{
	public class CtrlFunction : UserControl
	{
		#region Public Properties

		public virtual bool IsSelected { get; set; }

		#endregion
				
		#region Public Methods

		public virtual void BcrScanned(string data)
		{
		}

		public virtual void Closing()
		{
		}

		#endregion
	}
}