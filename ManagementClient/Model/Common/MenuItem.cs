﻿using System;
using Localization;

namespace Model.Common
{
	public class MenuInfoItem : ModelBase
	{
		#region Fields

		private string _text;

		#endregion

		#region Costructor

		public MenuInfoItem(string code, string originalText, Uri imageUri, string controlName, string icon, string iconColor)
		{
			if (code == null)
				throw new ArgumentNullException(nameof(code));
			if (originalText == null)
				throw new ArgumentNullException(nameof(originalText));
			if (controlName == null)
				throw new ArgumentNullException(nameof(controlName));

			Code = code;
			OriginalText = originalText;
			ImageUri = imageUri;
			ControlName = controlName;
			Icon = icon;
			IconColor =   iconColor;

			LocalizeText();
		}

		#endregion

		#region Properties

		/// <summary>
		/// Codice dell'item
		/// </summary>
		public string Code { get; }

		/// <summary>
		/// Testo originale (non tradotto)
		/// </summary>
		public string OriginalText { get; }

		/// <summary>
		/// URI dell'immagine
		/// </summary>
		public Uri ImageUri { get; }

		/// <summary>
		/// Nome del controllo
		/// </summary>
		public string ControlName { get; }

		/// <summary>
		/// Nome dell'icona
		/// </summary>
		public string Icon { get; }

		/// <summary>
		/// Colore dell'Icona
		/// </summary>
		public string IconColor { get; }


		/// <summary>
		/// Traduzione nella lingua attuale della proprietà <see cref="OriginalText"/>
		/// </summary>
		public string Text
		{
			get { return _text; }
			private set
			{
				if (_text != value)
				{
					_text = value;
					NotifyPropertyChanged();
				}
			}
		}

		public CtrlFunction Control { get; set; }

		#endregion

		#region Public Methods

		public void LocalizeText()
		{
			Text = Localize.LocalizeDefaultString(OriginalText).ToUpperInvariant();
		}

		#endregion
	}
}