﻿using System;

namespace Model.Common.LayoutSetting
{
  public class LayoutSettingParameter : LayoutSettingBase
  {
    private string _Icon = "";
    public string Icon
    {
      get { return _Icon; }
      set
      {
        if (value != _Icon)
        {
          _Icon = value;
          NotifyPropertyChanged();
        }
      }
    }

    protected Type _paramType;
    public virtual Type ParamType
    {
      get { return _paramType; }
      set { _paramType = value; }
    }

    private string _Title;
    public string Title
    {
      get { return _Title; }
      set
      {
        if (value != _Title)
        {
          _Title = value;
          NotifyPropertyChanged();
        }
      }
    }

    protected object _Value;
    virtual public object Value
    {
      get { return _Value; }
      set
      {
        if (value != _Value)
        {
          _Value = value;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsChanged");
        }
      }
    }


    private object _DefaultValue;
    public object DefaultValue
    {
      get { return _DefaultValue; }
      set
      {
        if (value != _DefaultValue)
        {
          _DefaultValue = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _SelectedValue;
    public string SelectedValue
    {
      get { return _SelectedValue; }
      set
      {
        if (value != _SelectedValue)
        {
          _SelectedValue = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _Descr;
    public string Descr
    {
      get { return _Descr; }
      set
      {
        if (value != _Descr)
        {
          _Descr = value;
          NotifyPropertyChanged();
        }
      }
    }

    public virtual bool IsChanged
    {
      get
      {
        if (Value != null)
          return Value.ToString().ToLower() != OriginalValue.ToLower();

        return false;
      }

    }

    private bool _IsOnError;
    public bool IsOnError
    {
      get { return _IsOnError; }
      set
      {
        if (value != _IsOnError)
        {
          _IsOnError = value;
          NotifyPropertyChanged();
        }
      }
    }

    public virtual void Update(LayoutSettingBase newConfPar)
    {
      this.OriginalValue = newConfPar.OriginalValue;
      this.Value = newConfPar.OriginalValue;
    }


    public virtual void SetDefault()
    {
      if (DefaultValue != null)
        Value = DefaultValue;
    }


    public override string ToString()
    {
      return base.ToString() + $",Value{Value}";
    }

    private string _Section;
    public string Section
    {
      get { return _Section; }
      set
      {
        if (value != _Section)
        {
          _Section = value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
