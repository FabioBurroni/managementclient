﻿using System;
using System.Windows.Data;
using System.Collections.Generic;
using System.Linq;

namespace Model.Common.LayoutSetting
{
  public class LayoutSettingOption : LayoutSettingParameter
  {

    public LayoutSettingOption(IValueConverter converter)
    {
      Converter = converter;
    }


    public List<KeyValuePair<string, string>> OptionList { get; set; } = new List<KeyValuePair<string, string>>();


    public override Type ParamType
    {
      get
      {
        return _paramType;
      }
      set
      {
        _paramType = value;

        if (value.IsEnum)
        {

          foreach (var enumVal in Enum.GetValues(value))
          {
            string key = enumVal.ToString();
            string val = Converter.Convert(enumVal, typeof(string), null, null).ToString();
            OptionList.Add(new KeyValuePair<string, string>(key, val));
          }
        }
      }
    }


    public IValueConverter Converter
    {
      get; set;
    }



    public override object Value
    {
      get { return _Value; }
      set
      {
        if (_Value != value)
        {
          _Value = value;
          SelectedValue = ((KeyValuePair<string, string>)value).Key;
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsChanged");
        }
      }
    }


    public override bool IsChanged
    {
      get
      {
        if (Value != null)
        {
          return ((KeyValuePair<string, string>)Value).Key.ToLower() != OriginalValue.ToLower();
        }
        return false;
      }

    }

    public override void Update(LayoutSettingBase newConfPar)
    {
      this.OriginalValue = newConfPar.OriginalValue;
      this.Value = OptionList.FirstOrDefault(op => op.Key == newConfPar.OriginalValue);
      //NotifyPropertyChanged("Icon");

    }

    public override void SetDefault()
    {
      if (DefaultValue != null)
        this.Value = OptionList.FirstOrDefault(op => op.Key == DefaultValue.ToString());

    }

  }
}
