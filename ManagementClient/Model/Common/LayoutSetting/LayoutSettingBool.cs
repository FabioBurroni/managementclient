﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Common.LayoutSetting
{
  public class LayoutSettingBool : LayoutSettingParameter
  {
    public LayoutSettingBool() : base()
    {
      ParamType = typeof(bool);
    }

    public override object Value
    {
      get { return _Value; }
      set
      {
        if (value != _Value)
        {
          bool tmp = false;
          if (!bool.TryParse(value.ToString(), out tmp))
          {
            IsOnError = true;
            return;
          }
          IsOnError = false;
          _Value = value;
          SelectedValue = value.ToString();
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsChanged");
        }
      }
    }
  }
}
