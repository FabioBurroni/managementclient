﻿
namespace Model.Common.LayoutSetting
{
  public class LayoutSettingString : LayoutSettingParameter
  {
    public LayoutSettingString()
    {
      ParamType = typeof(string);
    }

    public override object Value 
    {
      get { return _Value; }
      set
      {
        base.Value = value;
        SelectedValue = (string)value;
      }
    }
  }
}
