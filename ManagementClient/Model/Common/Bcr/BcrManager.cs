﻿using System;
using Configuration;
using Configuration.Settings.Global.Bcr;
using Model.Common.Bcr.EmulatedKeyboard;
using Model.Common.Bcr.Serial;

namespace Model.Common.Bcr
{
	public sealed class BcrManager : IDisposable
	{
		#region Events

		public event SerialBcrChangedHandler SerialBcrChanged;
		public event BcrDataReceivedHandler BcrDataReceived;

		#endregion

		#region Fields

		private EmulatedKeyboardBcrSetting _emulatedKeyboardBcrSetting;
		private SerialBcrSetting _serialBcrSetting;

		private EmulatedKeyboardBcrManager _emulatedKeyboardBcrManager;
		private SerialBcrManager _serialBcrManager;

		#endregion

		public static BcrManager Instance { get { return BcrManagerNested.instance; } }

		private class BcrManagerNested
		{
			// Explicit static constructor to tell C# compiler
			// not to mark type as beforefieldinit
			static BcrManagerNested()
			{
			}

			internal static readonly BcrManager instance = new BcrManager();
		}

		#region Constructor

		private BcrManager()
		{
		}

		#endregion

		#region Public Methods

		public void Start()
		{
			#region EmulatedKeyboard

			_emulatedKeyboardBcrSetting = ConfigurationManager.Parameters.BcrSetting?.EmulatedKeyboardBcrSetting;

			if (_emulatedKeyboardBcrSetting != null)
			{
				_emulatedKeyboardBcrManager = new EmulatedKeyboardBcrManager(_emulatedKeyboardBcrSetting.StartChar, _emulatedKeyboardBcrSetting.EndChar);
				_emulatedKeyboardBcrManager.BcrRead += EmulatedKeyboardBcrManagerOnBcrRead;

				if (_emulatedKeyboardBcrSetting?.Enabled == true)
					_emulatedKeyboardBcrManager.Start();
			}

			#endregion

			#region SerialBcr

			_serialBcrSetting = ConfigurationManager.Parameters.BcrSetting?.SerialBcrSetting;

			if (_serialBcrSetting != null)
			{
				_serialBcrManager = new SerialBcrManager(_serialBcrSetting.Port, _serialBcrSetting.BaudRate, _serialBcrSetting.Terminator);
				_serialBcrManager.SerialBarcodeReaderChanged += SerialBcrManagerOnSerialBarcodeReaderChanged;
				_serialBcrManager.SerialBarcodeReaderDataReceived += SerialBcrManagerOnSerialBarcodeReaderDataReceived;

				if (_serialBcrSetting?.Enabled == true)
					_serialBcrManager.Start();
			}

			#endregion
		}

		#endregion

		#region Event Handler

		private void EmulatedKeyboardBcrManagerOnBcrRead(object sender, BcrReadEventArgs e)
		{
			BcrDataReceived?.Invoke(this, new BcrDataReceivedEventArgs(e.DataRead));
		}

		private void SerialBcrManagerOnSerialBarcodeReaderChanged(object sender, SerialBarcodeReaderChangedEventArgs e)
		{
			SerialBcrChanged?.Invoke(this, new SerialBcrChangedEventArgs(e.SerialBarcodeReader?.SerialPort?.PortName));
		}

		private void SerialBcrManagerOnSerialBarcodeReaderDataReceived(object sender, SerialBarcodeReaderDataReceivedEventArgs e)
		{
			BcrDataReceived?.Invoke(this, new BcrDataReceivedEventArgs(e.Data));
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			_emulatedKeyboardBcrManager?.Dispose();
			_serialBcrManager?.Dispose();
		}

		#endregion
	}

	#region Events and Delegates

	public delegate void SerialBcrChangedHandler(object sender, SerialBcrChangedEventArgs e);
	public class SerialBcrChangedEventArgs : EventArgs
	{
		public SerialBcrChangedEventArgs(string port)
		{
			Port = port;
		}

		/// <summary>
		/// Porta seriale attualmente in uso, può essere nullo in caso di rimozione
		/// </summary>
		public string Port { get; }

		/// <summary>
		/// Indica se BCR è connesso
		/// </summary>
		public bool Connected => !string.IsNullOrEmpty(Port);
	}

	public delegate void BcrDataReceivedHandler(object sender, BcrDataReceivedEventArgs e);
	public class BcrDataReceivedEventArgs : EventArgs
	{
		public BcrDataReceivedEventArgs(string data)
		{
			Data = data;
		}

		public string Data { get; }
	}

	#endregion
}