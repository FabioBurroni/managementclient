﻿using System;
using System.IO.Ports;
using System.Linq;
using System.Management;

namespace Model.Common.Bcr.Serial
{
	public static class SerialPortService
	{
		private static readonly object Lock = new object();

		private static string[] _serialPorts;

		private static ManagementEventWatcher _arrival;

		private static ManagementEventWatcher _removal;

		static SerialPortService()
		{
			_serialPorts = GetAvailableSerialPorts();
			MonitorDeviceChanges();
		}

		/// <summary>
		/// If this method isn't called, an InvalidComObjectException will be thrown (like below):
		/// System.Runtime.InteropServices.InvalidComObjectException was unhandled
		///Message=COM object that has been separated from its underlying RCW cannot be used.
		///Source=mscorlib
		///StackTrace:
		///     at System.StubHelpers.StubHelpers.StubRegisterRCW(Object pThis, IntPtr pThread)
		///     at System.Management.IWbemServices.CancelAsyncCall_(IWbemObjectSink pSink)
		///     at System.Management.SinkForEventQuery.Cancel()
		///     at System.Management.ManagementEventWatcher.Stop()
		///     at System.Management.ManagementEventWatcher.Finalize()
		///InnerException: 
		/// </summary>
		public static void CleanUp()
		{
			_arrival.Stop();
			_removal.Stop();
		}

		public static event EventHandler<PortsChangedArgs> PortsChanged;

		private static void MonitorDeviceChanges()
		{
			try
			{
				var deviceArrivalQuery = new WqlEventQuery("SELECT * FROM Win32_DeviceChangeEvent WHERE EventType = 2");
				var deviceRemovalQuery = new WqlEventQuery("SELECT * FROM Win32_DeviceChangeEvent WHERE EventType = 3");

				_arrival = new ManagementEventWatcher(deviceArrivalQuery);
				_removal = new ManagementEventWatcher(deviceRemovalQuery);

				_arrival.EventArrived += (o, args) => RaisePortsChangedIfNecessary(EventType.Insertion);
				_removal.EventArrived += (sender, eventArgs) => RaisePortsChangedIfNecessary(EventType.Removal);

				// Start listening for events
				_arrival.Start();
				_removal.Start();
			}
			catch (ManagementException err)
			{
				var exc = err.ToString();
			}
		}

		private static void RaisePortsChangedIfNecessary(EventType eventType)
		{
			lock (Lock)
			{
				var availableSerialPorts = GetAvailableSerialPorts();
				if (!_serialPorts.SequenceEqual(availableSerialPorts))
				{
					_serialPorts = availableSerialPorts;
					PortsChanged?.Invoke(null, new PortsChangedArgs(eventType, _serialPorts));
				}
			}
		}

		public static string[] GetAvailableSerialPorts()
		{
			return SerialPort.GetPortNames();
		}
	}

	public enum EventType
	{
		Insertion,
		Removal,
	}

	public class PortsChangedArgs : EventArgs
	{
		public PortsChangedArgs(EventType eventType, string[] serialPorts)
		{
			EventType = eventType;
			SerialPorts = serialPorts;
		}

		public EventType EventType { get; }
		public string[] SerialPorts { get; }
	}
}