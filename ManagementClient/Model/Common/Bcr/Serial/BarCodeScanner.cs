﻿using System.Windows.Forms;

namespace Model.Common.Bcr.Serial
{
	public class BarcodeScanner
	{
		public delegate void OnBarCodeScannedHandler(string barCode);

		#region BCR
		private KeyboardHook _keyboardHook;

		public event OnBarCodeScannedHandler OnBarCodeScanned;

		public BarcodeScanner()
		{
			_keyboardHook = new KeyboardHook("");
			_keyboardHook.KeyPress += _keyboardHook_KeyPress;
		}

		public BarcodeScanner(int startChar, int endChar)
		{
			_startChar = (char)startChar;
			_endChar = (char)endChar;

			_keyboardHook = new KeyboardHook("");
			_keyboardHook.KeyPress += _keyboardHook_KeyPress;
		}

		void _keyboardHook_KeyPress(object sender, KeyPressEventArgs e)
		{
			//Console.WriteLine(e.KeyChar);
			MyKeyPress(null, e);
		}

		private char _startChar = (char)43;//+
		private char _endChar = (char)13; //CR
		private bool flag = false;
		private string resultFromBCR = "";

		#region ARRIVA UN CARATTERE DA TASTIERA
		public void MyKeyPress(object sender, KeyPressEventArgs e)
		{

			if (flag)
			{
				if (e.KeyChar == _endChar)
				{
					flag = false;
					if (OnBarCodeScanned != null)
						OnBarCodeScanned(resultFromBCR);
				}
				else
				{
					//Console.WriteLine("resultFromBCR " + resultFromBCR);
					resultFromBCR = resultFromBCR + e.KeyChar;
				}
			}
			if (e.KeyChar == _startChar)
			{
				flag = true;
				resultFromBCR = "";
			}
		}
		#endregion
		#endregion
	}
}
