﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Model.Common.Bcr.Serial
{
	public class KeyboardHook : IDisposable
	{
		/// <summary>
		/// Parameters accepted by the KeyboardHook constructor.
		/// </summary>
		public enum Parameters
		{
			None,
			DenyAltTab,
			DenyWindowsKey,
			DenyAltTabAndWindows,
			DenyAltF4,
			DenyAltF4AndAltTab,
			DenyAltF4AndWindows,
			DenyAltF4AndAltTabAndWindows,
			DenyAll,
			//PassAllKeysToNextApp
		}

		//Internal parameters
		//private bool PassAllKeysToNextApp = false;
		private bool AllowAltTab = true;
		private bool AllowWindowsKey = true;
		private bool AllowAltF4 = true;
		private Parameters _parameters = Parameters.None;

		private DateTime _lastInteraction = DateTime.MinValue;

		//Variables used in the call to SetWindowsHookEx
		private Win32.HookHandlerDelegate proc;
		private IntPtr hookID = IntPtr.Zero;
		//internal delegate IntPtr HookHandlerDelegate(
		//    int nCode, IntPtr wParam, ref KBDLLHOOKSTRUCT lParam);

		/// <summary>
		/// Event triggered when a keystroke is intercepted by the 
		/// low-level hook.
		/// </summary>
		//public event KeyboardHookEventHandler KeyIntercepted;

		//public event HandlerSystemKeyEventArgs OnSystemKeyDown;
		//public event HandlerSystemKeyEventArgs OnSystemKeyUp;

		public delegate void OnKeyPressedHandler(char keyPressed);
		public event KeyPressEventHandler KeyPress;



		//// Structure returned by the hook whenever a key is pressed
		//internal struct KBDLLHOOKSTRUCT
		//{
		//  public int vkCode;
		//  int scanCode;
		//  public int flags;
		//  int time;
		//  int dwExtraInfo;
		//}

		#region Constructors
		/// <summary>
		/// Sets up a keyboard hook to trap all keystrokes without 
		/// passing any to other applications.
		/// </summary>
		public KeyboardHook()
		{
			proc = new Win32.HookHandlerDelegate(HookCallback);
			using (Process curProcess = Process.GetCurrentProcess())
			using (ProcessModule curModule = curProcess.MainModule)
			{
				hookID = Win32.SetWindowsHookEx(Win32.WH_KEYBOARD_LL, proc, Win32.GetModuleHandle(curModule.ModuleName), 0);
			}
		}

		/// <summary>
		/// Sets up a keyboard hook with custom parameters.
		/// </summary>
		/// <param name="param">A valid name from the Parameter enum; otherwise, the 
		/// default parameter Parameter.None will be used.</param>
		public KeyboardHook(string param)
			: this()
		{
			if (!String.IsNullOrEmpty(param) && Enum.IsDefined(typeof(Parameters), param))
			{
				SetParameters((Parameters)Enum.Parse(typeof(Parameters), param));
			}
		}

		/// <summary>
		/// Sets up a keyboard hook with custom parameters.
		/// </summary>
		/// <param name="param">A value from the Parameters enum.</param>
		public KeyboardHook(Parameters param)
			: this()
		{
			SetParameters(param);
		}

		public void SetParameters(Parameters param)
		{
			_parameters = param;
			AllowAltF4 = true;
			AllowAltTab = true;
			AllowWindowsKey = true;
			switch (param)
			{
				case Parameters.None:
					break;
				case Parameters.DenyAltTab:
					AllowAltTab = false;
					break;
				case Parameters.DenyWindowsKey:
					AllowWindowsKey = false;
					break;
				case Parameters.DenyAltTabAndWindows:
					AllowAltTab = false;
					AllowWindowsKey = false;
					break;
				case Parameters.DenyAltF4:
					AllowAltF4 = false;
					break;
				case Parameters.DenyAltF4AndAltTab:
					AllowAltF4 = false;
					AllowAltTab = false;
					break;
				case Parameters.DenyAltF4AndAltTabAndWindows:
					AllowAltF4 = false;
					AllowAltTab = false;
					AllowWindowsKey = false;
					break;
				case Parameters.DenyAll:
					AllowAltF4 = false;
					AllowAltTab = false;
					AllowWindowsKey = false;
					break;
					//case Parameters.PassAllKeysToNextApp:
					//  PassAllKeysToNextApp = true;
					//  break;
			}
		}
		#endregion

		public DateTime lastInteraction { get { return _lastInteraction; } }

		//#region Check Modifier keys
		///// <summary>
		///// Checks whether Alt, Shift, Control or CapsLock
		///// is enabled at the same time as another key.
		///// Modify the relevant sections and return type 
		///// depending on what you want to do with modifier keys.
		///// </summary>
		//private void CheckModifiers()
		//{
		//  StringBuilder sb = new StringBuilder();

		//  if ((Win32.GetKeyState(Win32.VK_CAPITAL) & 0x0001) != 0)
		//  {
		//    //CAPSLOCK is ON
		//    sb.AppendLine("Capslock is enabled.");
		//  }

		//  if ((Win32.GetKeyState(Win32.VK_SHIFT) & 0x8000) != 0)
		//  {
		//    //SHIFT is pressed
		//    sb.AppendLine("Shift is pressed.");
		//  }
		//  if ((Win32.GetKeyState(Win32.VK_CONTROL) & 0x8000) != 0)
		//  {
		//    //CONTROL is pressed
		//    sb.AppendLine("Control is pressed.");
		//  }
		//  if ((Win32.GetKeyState(Win32.VK_MENU) & 0x8000) != 0)
		//  {
		//    //ALT is pressed
		//    sb.AppendLine("Alt is pressed.");
		//  }
		//  //Console.WriteLine(sb.ToString());
		//}
		//#endregion Check Modifier keys

		private const int WM_KEYDOWN = 0x100;

		#region Hook Callback Method
		/// <summary>
		/// Processes the key event captured by the hook.
		/// </summary>
		private IntPtr HookCallback(int nCode, IntPtr wParam, ref Win32.KBDLLHOOKSTRUCT lParam)
		{
			///Console.WriteLine("ncode" + nCode + " wparam: " + wParam + " lParam scanCode: " + lParam.scanCode + " lParam vkCode: " + lParam.vkCode);
			if (KeyPress != null && (wParam == (IntPtr)256)) //...key pressed
																											 //if (KeyPress != null && (wParam == (IntPtr)256 || wParam == (IntPtr)260))
																											 //if (KeyPress != null)
			{
				byte[] keyState = new byte[256];
				Win32.GetKeyboardState(keyState);
				byte[] inBuffer = new byte[2];

				if (Win32.ToAscii(lParam.vkCode, lParam.scanCode, keyState, inBuffer, lParam.flags) == 1)
				{
					char key = (char)inBuffer[0];
					//Console.WriteLine("CARATTERE RICONOSCIUTO: " + key + " ncode" + nCode + " wparam: " + wParam + " lParam scanCode: " + lParam.scanCode + " lParam vkCode: " + lParam.vkCode);
					KeyPressEventArgs e = new KeyPressEventArgs(key);
					KeyPress(this, e);
				}
				else
				{
					//Console.WriteLine("CARATTERE NON RICONOSCIUTO: " + "ncode" + nCode + " wparam: " + wParam + " lParam scanCode: " + lParam.scanCode + " lParam vkCode: " + lParam.vkCode);

				}
			}
			else
			{
				//Console.WriteLine("CARATTERE NON RICONOSCIUTO: " + "ncode" + nCode + " wparam: " + wParam + " lParam scanCode: " + lParam.scanCode + " lParam vkCode: " + lParam.vkCode);
			}
			//Pass key to next application
			return Win32.CallNextHookEx(hookID, nCode, wParam, ref lParam);
		}
		#endregion

		//#region Event Handling

		//public delegate void HandlerSystemKeyEventArgs(object sender, SystemKeyEventArgs e);

		//public class SystemKeyEventArgs : System.EventArgs
		//{
		//  private bool _isAltF4 = false;
		//  private bool _isAltTab = false;
		//  private bool _isWindowsKey = false;
		//  internal SystemKeyEventArgs(bool isAltF4, bool isAltTab, bool isWindowsKey)
		//  {
		//    _isAltF4 = isAltF4;
		//    _isAltTab = isAltTab;
		//    _isWindowsKey = isWindowsKey;
		//  }
		//  public bool isAltF4 { get { return _isAltF4; } }
		//  public bool isAltTab { get { return _isAltTab; } }
		//  public bool isWindowsKey { get { return _isWindowsKey; } }
		//}

		//public void EventOnSystemKeyDown(SystemKeyEventArgs e)
		//{
		//  if (OnSystemKeyDown != null)
		//    OnSystemKeyDown(this, e);
		//}

		/////// <summary>
		/////// Raises the KeyIntercepted event.
		/////// </summary>
		/////// <param name="e">An instance of KeyboardHookEventArgs</param>
		////public void OnKeyIntercepted(KeyboardHookEventArgs e)
		////{
		////  if (KeyIntercepted != null)
		////    KeyIntercepted(e);
		////}

		/////// <summary>
		/////// Delegate for KeyboardHook event handling.
		/////// </summary>
		/////// <param name="e">An instance of InterceptKeysEventArgs.</param>
		////public delegate void KeyboardHookEventHandler(KeyboardHookEventArgs e);

		/////// <summary>
		/////// Event arguments for the KeyboardHook class's KeyIntercepted event.
		/////// </summary>
		////public class KeyboardHookEventArgs : System.EventArgs
		////{

		////  private string keyName;
		////  private int keyCode;
		////  private bool passThrough;

		////  /// <summary>
		////  /// The name of the key that was pressed.
		////  /// </summary>
		////  public string KeyName
		////  {
		////    get { return keyName; }
		////  }

		////  /// <summary>
		////  /// The virtual key code of the key that was pressed.
		////  /// </summary>
		////  public int KeyCode
		////  {
		////    get { return keyCode; }
		////  }

		////  /// <summary>
		////  /// True if this key combination was passed to other applications,
		////  /// false if it was trapped.
		////  /// </summary>
		////  public bool PassThrough
		////  {
		////    get { return passThrough; }
		////  }

		////  public KeyboardHookEventArgs(int evtKeyCode, bool evtPassThrough)
		////  {
		////    keyName = ((Keys)evtKeyCode).ToString();
		////    keyCode = evtKeyCode;
		////    passThrough = evtPassThrough;
		////  }

		////}

		//#endregion

		#region IDisposable Members
		/// <summary>
		/// Releases the keyboard hook.
		/// </summary>
		public void Dispose()
		{
			Win32.UnhookWindowsHookEx(hookID);
		}
		#endregion


		private class KeyboardHookStruct
		{
			/// <summary>
			/// Specifies a virtual-key code. The code must be a value in the range 1 to 254. 
			/// </summary>
			public int vkCode;
			/// <summary>
			/// Specifies a hardware scan code for the key. 
			/// </summary>
			public int scanCode;
			/// <summary>
			/// Specifies the extended-key flag, event-injected flag, context code, and transition-state flag.
			/// </summary>
			public int flags;
			/// <summary>
			/// Specifies the time stamp for this message.
			/// </summary>
			public int time;
			/// <summary>
			/// Specifies extra information associated with the message. 
			/// </summary>
			public int dwExtraInfo;
		}

	}
}
