﻿using System;
using System.IO.Ports;
using System.Text;

namespace Model.Common.Bcr.Serial
{
	public class SerialBcr : IDisposable
	{
		public delegate void DataReceivedHandler(object sender, string data);
		public event DataReceivedHandler DataReceived;

		private readonly SerialPort _serialPort;
		private readonly string _terminator;
		private readonly StringBuilder _data = new StringBuilder();

		public SerialBcr(string serialPort, int baudRate, string terminator)
		{
			_terminator = terminator ?? "";
			_serialPort = new SerialPort(serialPort, baudRate);
			_serialPort.Open();
			_serialPort.DataReceived += serialPort_DataReceived;
		}

		public SerialPort SerialPort => _serialPort;

		private void serialPort_DataReceived(object s, SerialDataReceivedEventArgs e)
		{
			SerialPort sp = (SerialPort)s;
			string indata = sp.ReadExisting() ?? "";

			Utilities.Logging.WriteToFile(nameof(SerialBcr), $"Read: {indata}");

			//aggiungo tutti i caratteri letti alla stringa attuale
			_data.Append(indata);

			//se la stringa non termina con i caratteri di riconoscimento, esco
			if (!string.IsNullOrEmpty(_terminator) && !_data.ToString().EndsWith(_terminator))
				return;

			//stringa riconosciuta, restituisco solo i caratteri letti senza il terminatore
			indata = _data.Remove(_data.Length - _terminator.Length, _terminator.Length).ToString();

			//SOLO Framework 4.0
			//_data.Clear();

			_data.Length = 0;
			_data.Capacity = 0;

			DataReceived?.Invoke(this, indata);
		}

		public void Dispose()
		{
			_serialPort?.Dispose();
		}
	}
}