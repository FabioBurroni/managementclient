﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace Model.Common.Bcr.Serial
{
	public class Win32
	{
		private const int WM_SYSCOMMAND = 0x0112;
		private const int SC_CLOSE = 0xF060;
		private const int BM_CLICK = 0xF5;
		private const int SWP_HIDEWINDOW = 0x80;
		private const int SWP_SHOWWINDOW = 0x40;
		private const int SWP_DRAWFRAME = 0x20;
		private const int SWP_NOMOVE = 0x2;
		private const int SWP_NOSIZE = 0x1;
		private const int SWP_NOZORDER = 0x4;
		private const int SWP_NOACTIVATE = 0x10;
		private const int SWP_NOSENDCHANGING = 0x0400;
		private const int SW_HIDE = 0;
		private const int SW_SHOW = 5;
		private const int SW_RESTORE = 9;

		private const string VistaStartMenuCaption = "Start";
		private static IntPtr vistaStartMenuWnd = IntPtr.Zero;
		private delegate bool EnumThreadProc(IntPtr hwnd, IntPtr lParam);

		[DllImport("user32.dll")]
		public static extern IntPtr GetForegroundWindow();

		[DllImport("user32.dll")]
		public static extern IntPtr GetActiveWindow();

		[DllImport("user32.dll")]
		public static extern IntPtr SetActiveWindow(IntPtr hwnd);

		[DllImport("user32.dll")]
		public static extern int SetParent(int hWndChild, int hWndNewParent);

		[DllImport("user32.dll", EntryPoint = "SetWindowPos")]
		public static extern bool SetWindowPos(
			int hWnd,               // handle to window
			int hWndInsertAfter,    // placement-order handle
			int X,                  // horizontal position
			int Y,                  // vertical position
			int cx,                 // width
			int cy,                 // height
			uint uFlags             // window-positioning options
		);

		[DllImport("user32.dll", EntryPoint = "MoveWindow")]
		public static extern bool MoveWindow(
			int hWnd,
			int X,
			int Y,
			int nWidth,
			int nHeight,
			bool bRepaint
		);

		[DllImport("user32.dll")]
		public static extern IntPtr FindWindow(
			 string lpClassName, // class name 
			 string lpWindowName // window name 
		);

		[DllImport("user32.dll")]
		public static extern IntPtr FindWindowEx(IntPtr parent /*HWND*/,
																IntPtr next /*HWND*/,
																string sClassName,
																string sWindowTitle);

		[DllImport("user32.dll")]
		public static extern int SendMessage(
			 IntPtr hWnd, // handle to destination window 
			 int Msg, // message 
			 int wParam, // first message parameter 
			 int lParam // second message parameter 
		);

		[DllImport("User32.dll")]
		public static extern int ShowWindow(IntPtr hwnd, int nCmdShow);

		[DllImport("user32.dll")]
		private static extern uint GetWindowThreadProcessId(IntPtr hwnd, out int lpdwProcessId);

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		private static extern bool EnumThreadWindows(int threadId, EnumThreadProc pfnEnum, IntPtr lParam);

		//public static void setWindowStatusBar(bool show)
		//{
		//  uint uFlag = SWP_HIDEWINDOW;
		//  if (show)
		//    uFlag = SWP_SHOWWINDOW;
		//  IntPtr hwnd = FindWindow("Shell_TrayWnd", "");
		//  if (hwnd != null)
		//  {
		//    SetWindowPos(hwnd.ToInt32(), 0, 0, 0, 0, 0, uFlag);
		//  }
		//}

		//private static void SetVisibility(bool show)
		public static void setWindowStatusBar(bool show)
		{
			IntPtr taskBarWnd = FindWindow("Shell_TrayWnd", null);

			IntPtr startWnd = FindWindowEx(taskBarWnd, IntPtr.Zero, "Button", "Start");
			if (startWnd == IntPtr.Zero)
			{
				startWnd = FindWindow("Button", null);

				if (startWnd == IntPtr.Zero)
				{
					startWnd = GetVistaStartMenuWnd(taskBarWnd);
				}
			}

			ShowWindow(taskBarWnd, show ? SW_SHOW : SW_HIDE);
			ShowWindow(startWnd, show ? SW_SHOW : SW_HIDE);
		}

		private static IntPtr GetVistaStartMenuWnd(IntPtr taskBarWnd)
		{
			int procId;
			GetWindowThreadProcessId(taskBarWnd, out procId);

			System.Diagnostics.Process p = Process.GetProcessById(procId);
			if (p != null)
			{
				foreach (System.Diagnostics.ProcessThread t in p.Threads)
				{
					EnumThreadWindows(t.Id, MyEnumThreadWindowsProc, IntPtr.Zero);
				}
			}
			return vistaStartMenuWnd;
		}

		private static bool MyEnumThreadWindowsProc(IntPtr hWnd, IntPtr lParam)
		{
			StringBuilder buffer = new StringBuilder(256);
			if (GetWindowText(hWnd, buffer, buffer.Capacity) > 0)
			{
				//Console.WriteLine(buffer);
				if (buffer.ToString() == VistaStartMenuCaption)
				{
					vistaStartMenuWnd = hWnd;
					return false;
				}
			}
			return true;
		}

		[DllImport("user32")]
		public static extern int GetKeyboardState(byte[] pbKeyState);

		[DllImport("user32")]
		public static extern int ToAscii(
			 int uVirtKey,
			 int uScanCode,
			 byte[] lpbKeyState,
			 byte[] lpwTransKey,
			 int fuState);


		[DllImport("user32.dll", SetLastError = false)]
		static extern IntPtr GetDesktopWindow();

		//public static void setZOrderAfterDesktop(System.Windows.Forms.Form form)
		//{
		//	IntPtr HWND_BOTTOM = new IntPtr(1);
		//	SetWindowPos((int)form.Handle, (int)HWND_BOTTOM, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
		//}

		public static void setWindow(IntPtr hwnd, bool show)
		{
			int uFlag = SW_HIDE;
			if (show)
			{
				//uFlag = SW_RESTORE;
				uFlag = SW_SHOW;
			}
			//if (hwnd != null)
			//{
			ShowWindow(hwnd, uFlag);
			//}
		}

		public static void setWindow(string caption, bool show)
		{
			//IntPtr hwnd = FindWindow(caption, "");
			IntPtr hwnd = FindWindow("", caption);
			setWindow(hwnd, show);
		}

		[DllImport("Shell32", CharSet = CharSet.Auto)]
		private static extern int ExtractIconEx(
			 string lpszFile,
			 int nIconIndex,
			 IntPtr[] phIconLarge,
			 IntPtr[] phIconSmall,
			 int nIcons);

		[DllImport("Shell32.dll")]
		private static extern IntPtr ExtractIcon(int hInst, string FileName, int nIconIndex);

		[DllImport("user32.dll", EntryPoint = "DestroyIcon", SetLastError = true)]
		private static extern int DestroyIcon(IntPtr hIcon);

		//public static System.Drawing.Icon ExtractIconFromExe(string file, bool large)
		//{
		//	System.Drawing.Icon ret = null;
		//	int maxRetrie = 3;
		//	try
		//	{
		//		for (int i = 0; ret == null && i < maxRetrie; i++)
		//		{
		//			int readIconCount = 0;
		//			IntPtr[] hDummy = new IntPtr[1] { IntPtr.Zero };
		//			IntPtr[] hIconEx = new IntPtr[1] { IntPtr.Zero };

		//			try
		//			{
		//				if (large)
		//					readIconCount = ExtractIconEx(file, 0, hIconEx, hDummy, 1);
		//				else
		//					readIconCount = ExtractIconEx(file, 0, hDummy, hIconEx, 1);

		//				if (readIconCount > 0 && hIconEx[0] != IntPtr.Zero)
		//				{
		//					ret = (System.Drawing.Icon)System.Drawing.Icon.FromHandle(hIconEx[0]).Clone();
		//				}
		//			}
		//			catch (Exception ex)
		//			{
		//				//throw new ApplicationException("Could not extract icon", ex);
		//			}
		//			finally
		//			{
		//				// RELEASE RESOURCES
		//				foreach (IntPtr ptr in hIconEx)
		//					if (ptr != IntPtr.Zero)
		//						DestroyIcon(ptr);

		//				foreach (IntPtr ptr in hDummy)
		//					if (ptr != IntPtr.Zero)
		//						DestroyIcon(ptr);
		//			}
		//			if (ret == null)
		//			{
		//				System.Threading.Thread.Sleep(500);
		//			}
		//		}

		//	}
		//	catch
		//	{
		//		ret = null;
		//	}
		//	return ret;
		//}

		//public static System.Drawing.Bitmap ExtractIconFromExe(System.Diagnostics.Process proc)
		//{
		//	System.Drawing.Bitmap ret = null;
		//	IntPtr hIcon = IntPtr.Zero;

		//	try
		//	{
		//		hIcon = ExtractIcon((int)proc.MainModule.BaseAddress, proc.MainModule.FileName, 0);
		//		if (hIcon != IntPtr.Zero)
		//			ret = System.Drawing.Bitmap.FromHicon(hIcon);
		//		else
		//		{
		//			//Console.WriteLine("Cassioli.Win32.ExtractIconFromExe: proc.MainModule.BaseAddress=" + proc.MainModule.BaseAddress + " proc.MainModule.FileName=" + proc.MainModule.FileName);
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		//Console.WriteLine("Cassioli.Win32.ExtractIconFromExe: " + ex.Message);
		//	}
		//	finally
		//	{
		//		if (hIcon != IntPtr.Zero)
		//			DestroyIcon(hIcon);
		//	}
		//	return ret;
		//}


		//[DllImport("User32.dll")]
		//public static extern int SetWindowRgn(int hWnd, int hRgn,bool bRedraw);

		[DllImport("gdi32.dll")]
		public static extern IntPtr CreateEllipticRgn(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect);

		//[DllImport("gdi32.dll")]
		//public static extern int CombineRgn(int hwndDest, int hwndSrc1, int hwndSrc2, int fnCombineMode);



		[DllImport("Gdi32.dll", EntryPoint = "CombineRgn", SetLastError = true, CharSet = CharSet.Auto)]
		public static extern int CombineRegion(IntPtr dst, IntPtr src1, IntPtr src2, CombineRegionOptions options);
		[DllImport("User32.dll", EntryPoint = "SetWindowRgn", SetLastError = true, CharSet = CharSet.Auto)]
		public static extern int SetWindowRegion(IntPtr hWnd, IntPtr hRgn, int visible);
		[DllImport("Gdi32.dll", EntryPoint = "CreateRectRgn", SetLastError = true, CharSet = CharSet.Auto)]
		public static extern IntPtr CreateRectangleRegion(int left, int top, int right, int bottom);
		[DllImport("Gdi32.dll", EntryPoint = "DeleteObject", SetLastError = true, CharSet = CharSet.Auto)]
		public static extern int DeleteObject(IntPtr obj);

		public enum CombineRegionOptions : int
		{
			RGN_AND = 1,
			RGN_OR = 2,
			RGN_XOR = 3,
			RGN_DIFF = 4,
			RGN_COPY = 5,
			RGN_MIN = RGN_AND,
			RGN_MAX = RGN_COPY
		}


		public const int WH_KEYBOARD_LL = 13;
		public const int WH_MOUSE = 7;
		public const int WH_MOUSE_LL = 14;

		public const int WM_KEYUP = 0x0101;
		public const int WM_SYSKEYUP = 0x0105;

		public const int WM_KEYDOWN = 0x0100;
		public const int WM_SYSKEYDOWN = 0x01;

		public const int VK_SHIFT = 0x10;
		public const int VK_CONTROL = 0x11;
		public const int VK_MENU = 0x12;
		public const int VK_CAPITAL = 0x14;

		public delegate IntPtr HookHandlerDelegate(int nCode, IntPtr wParam, ref KBDLLHOOKSTRUCT lParam);

		// Structure returned by the hook whenever a key is pressed
		public struct KBDLLHOOKSTRUCT
		{
			public int vkCode;
			public int scanCode;
			public int flags;
			int time;
			int dwExtraInfo;
		}

		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		public static extern IntPtr GetModuleHandle(string lpModuleName);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		public static extern IntPtr SetWindowsHookEx(int idHook, HookHandlerDelegate lpfn, IntPtr hMod, uint dwThreadId);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool UnhookWindowsHookEx(IntPtr hhk);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		public static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, ref KBDLLHOOKSTRUCT lParam);

		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		public static extern short GetKeyState(int keyCode);



		[DllImport("user32.dll")]
		public static extern IntPtr SetWindowsHookEx(int idHook, HookMouseHandler lpfn, IntPtr hMod, uint dwThreadId);

		[DllImport("user32.dll")]
		public static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

		public delegate IntPtr HookMouseHandler(int code, IntPtr wParam, IntPtr lParam);

		public struct MOUSEHOOKSTRUCT
		{
			public POINT pt;
			public IntPtr hwnd;
			public uint wHitTestCode;
			public IntPtr dwExtraInfo;
		}

		public struct POINT
		{
			public int x;
			public int y;
		}

		public const int WM_MOUSEMOVE = 0x0200;

		public const int WM_LBUTTONDOWN = 0x0201;
		public const int WM_LBUTTONUP = 0x0202;
		public const int WM_LBUTTONDBLCLK = 0x0203;

		public const int WM_RBUTTONDOWN = 0x0204;
		public const int WM_RBUTTONUP = 0x0205;
		public const int WM_RBUTTONDBLCLK = 0x0206;

		public const int WM_MBUTTONDOWN = 0x0207;
		public const int WM_MBUTTONUP = 0x0208;
		public const int WM_MBUTTONDBLCLK = 0x0209;

		public const int WM_XBUTTONDOWN = 0x020B;
		public const int WM_XBUTTONUP = 0x020C;
		public const int WM_XBUTTONDBLCLK = 0x020D;

		public const int WM_NCMOUSEMOVE = 0x00A0;
		public const int WM_NCLBUTTONDOWN = 0x00A1;
		public const int WM_NCLBUTTONUP = 0x00A2;
		public const int WM_NCLBUTTONDBLCLK = 0x00A3;
		public const int WM_NCRBUTTONDOWN = 0x00A4;
		public const int WM_NCRBUTTONUP = 0x00A5;
		public const int WM_NCRBUTTONDBLCLK = 0x00A6;
		public const int WM_NCMBUTTONDOWN = 0x00A7;
		public const int WM_NCMBUTTONUP = 0x00A8;
		public const int WM_NCMBUTTONDBLCLK = 0x00A9;
		public const int WM_NCXBUTTONDOWN = 0x00AB;
		public const int WM_NCXBUTTONUP = 0x00AC;
		public const int WM_NCXBUTTONDBLCLK = 0x00AD;

		public const int VK_XBUTTON1 = 0x05;
		public const int VK_XBUTTON2 = 0x06;

		public const int HC_ACTION = 0;
		public const int HC_NOREMOVE = 3;




		[DllImport("user32.dll")]
		public static extern IntPtr DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);

		[DllImport("user32.dll")]
		public static extern IntPtr GetSystemMenu(IntPtr hwnd, int bRevert);

		public const int MF_BYPOSITION = 0x400;

		//public static void removeFormSystemMenus(System.Windows.Forms.Form frm, bool restore, bool move, bool size,
		//  bool minimize, bool maximize, bool seperator, bool close)
		//{
		//	IntPtr hMenu = GetSystemMenu(frm.Handle, 0);
		//	if (hMenu.ToInt32() > 0)
		//		removeFormSystemMenus(hMenu, restore, move, size, minimize, maximize, seperator, close);
		//}

		public static void removeFormSystemMenus(IntPtr frmHwnd, bool restore, bool move, bool size,
			bool minimize, bool maximize, bool seperator, bool close)
		{
			if (frmHwnd.ToInt32() > 0)
			{
				IntPtr hMenu = GetSystemMenu(frmHwnd, 0);
				if (restore)
				{
					DeleteMenu(hMenu, 0, MF_BYPOSITION);
				}
				if (move)
				{
					DeleteMenu(hMenu, 1, MF_BYPOSITION);
				}
				if (size)
					DeleteMenu(hMenu, 2, MF_BYPOSITION);
				if (minimize)
					DeleteMenu(hMenu, 3, MF_BYPOSITION);
				if (maximize)
					DeleteMenu(hMenu, 4, MF_BYPOSITION);
				if (seperator)
				{
					DeleteMenu(hMenu, 5, MF_BYPOSITION);
					System.Threading.Thread.Sleep(500);
				}
				if (close)
				{
					DeleteMenu(hMenu, 6, MF_BYPOSITION);
				}
			}
		}

		[DllImport("user32.Dll")]
		public static extern int LockWorkStation();


		[DllImport("user32.Dll")]
		public static extern int EnumWindows(EnumWinCallBack x, int y);
		[DllImport("User32.Dll")]
		public static extern int GetWindowText(IntPtr hWnd, StringBuilder s, int nMaxCount);
		[DllImport("User32.Dll")]
		public static extern void GetClassName(int h, StringBuilder s, int nMaxCount);

		public delegate bool EnumWinCallBack(int hwnd, int lParam);
		private static IntPtr _windowHwnd = IntPtr.Zero;
		private static string _windowString = null;
		private static object _objForLock = new object();

		public static IntPtr GetWindowHwnd(string winCaption)
		{
			lock (_objForLock)
			{
				_windowHwnd = IntPtr.Zero;
				if (winCaption != null)
				{
					_windowString = winCaption;
					EnumWindows(new EnumWinCallBack(EnumWindowCallBack), 0);
				}
			}
			return _windowHwnd;
		}

		public static bool EnumWindowCallBack(int hwnd, int lParam)
		{
			IntPtr windowHandle = (IntPtr)hwnd;
			StringBuilder sb = new StringBuilder(1024);
			StringBuilder sbc = new StringBuilder(256);
			GetClassName(hwnd, sbc, sbc.Capacity);
			GetWindowText(windowHandle, sb, sb.Capacity);
			if (sb.ToString().Trim() == _windowString)
			{
				//Console.WriteLine(sb.ToString().Trim()+" - HWND: "+windowHandle.ToInt32().ToString());
				_windowHwnd = windowHandle;
			}
			return true;
		}

	}
}
