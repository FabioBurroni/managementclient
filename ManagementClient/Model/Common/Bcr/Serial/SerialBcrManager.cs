﻿using System;
using System.Linq;
using Utilities.Extensions;

namespace Model.Common.Bcr.Serial
{
	internal class SerialBcrManager : IDisposable
	{
		#region Events

		public event SerialBarcodeReaderChangedEventHandler SerialBarcodeReaderChanged;
		public event SerialBarcodeReaderDataReceivedHandler SerialBarcodeReaderDataReceived;

		#endregion

		#region Fields

		private bool _started;

		private readonly string _port;
		private readonly int _baudRate;
		private readonly string _terminator;

		private SerialBcr _serialBarcodeReader;

		#endregion

		#region Constructor

		public SerialBcrManager(string port, int baudRate, string terminator)
		{
			_port = port;
			_baudRate = baudRate;
			_terminator = terminator;
		}

		#endregion

		#region Properties

		public SerialBcr SerialBarcodeReader => _serialBarcodeReader;

		#endregion

		#region Public Methods

		public void Start()
		{
			if(_started)
				return;

			_started = true;

			StartBcr();
			StartListener();
		}

		#endregion

		#region Private Methods

		#region Bcr

		private void StartBcr()
		{
			try
			{
				_serialBarcodeReader = new SerialBcr(_port, _baudRate, _terminator);
				_serialBarcodeReader.DataReceived += SerialBarcodeReaderOnDataReceived;

				SerialBarcodeReaderChanged?.Invoke(this, new SerialBarcodeReaderChangedEventArgs(_serialBarcodeReader));
			}
			catch (Exception e)
			{
				Utilities.Logging.Message($"{nameof(SerialBcrManager)}.{nameof(StartBcr)} Exception={e.Message}");
			}
		}

		private void StopBcr()
		{
			if (_serialBarcodeReader == null)
				return;

			_serialBarcodeReader.DataReceived -= SerialBarcodeReaderOnDataReceived;
			_serialBarcodeReader.Dispose();
			_serialBarcodeReader = null;

			SerialBarcodeReaderChanged?.Invoke(this, new SerialBarcodeReaderChangedEventArgs(null));
		}

		#endregion

		#region Listener

		/// <summary>
		/// Si sintonizza sulla variazione delle porte COM per percepire l'inserimento e la rimozione del bcr
		/// </summary>
		private void StartListener()
		{
			SerialPortService.PortsChanged += SerialPortServiceOnPortsChanged;
		}

		private void StopListener()
		{
			SerialPortService.PortsChanged -= SerialPortServiceOnPortsChanged;
		}

		#endregion

		#endregion

		#region Event Handler

		private void SerialBarcodeReaderOnDataReceived(object sender, string data)
		{
			var bcr = sender as SerialBcr;
			if (bcr == null)
				return;

			SerialBarcodeReaderDataReceived?.Invoke(this, new SerialBarcodeReaderDataReceivedEventArgs(bcr, data));
		}

		private void SerialPortServiceOnPortsChanged(object sender, PortsChangedArgs portsChangedArgs)
		{
			//se il mio bcr è nullo ed è stato inserita una nuova com che corrisponde a quella della configurazione, provo a farlo partire
			if (_serialBarcodeReader == null && portsChangedArgs.EventType == EventType.Insertion && portsChangedArgs.SerialPorts.Any(p => p.EqualsIgnoreCase(_port)))
			{
				StartBcr();
				return;
			}

			//se il mio bcr è avvalorato ed è stato rimossa la sua com, lo devo resettare
			if (_serialBarcodeReader != null && portsChangedArgs.EventType == EventType.Removal && !portsChangedArgs.SerialPorts.Any(p => p.EqualsIgnoreCase(_port)))
			{
				StopBcr();
				return;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			StopListener();
			StopBcr();
		}

		#endregion
	}

	#region Events and Delegates

	internal delegate void SerialBarcodeReaderChangedEventHandler(object sender, SerialBarcodeReaderChangedEventArgs e);
	internal class SerialBarcodeReaderChangedEventArgs : EventArgs
	{
		public SerialBarcodeReaderChangedEventArgs(SerialBcr serialBarcodeReader)
		{
			SerialBarcodeReader = serialBarcodeReader;
		}

		/// <summary>
		/// BCR attualmente utilizzato, può essere nullo in caso di rimozione
		/// </summary>
		public SerialBcr SerialBarcodeReader { get; }
	}


	internal delegate void SerialBarcodeReaderDataReceivedHandler(object sender, SerialBarcodeReaderDataReceivedEventArgs e);
	internal class SerialBarcodeReaderDataReceivedEventArgs : EventArgs
	{
		public SerialBarcodeReaderDataReceivedEventArgs(SerialBcr serialBarcodeReader, string data)
		{
			if (serialBarcodeReader == null)
				throw new ArgumentNullException(nameof(serialBarcodeReader));

			SerialBarcodeReader = serialBarcodeReader;
			Data = data;
		}

		public SerialBcr SerialBarcodeReader { get; }
		public string Data { get; }
	}

	#endregion
}