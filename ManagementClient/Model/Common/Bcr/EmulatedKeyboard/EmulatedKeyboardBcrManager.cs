﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using MouseKeyboardActivityMonitor;
using MouseKeyboardActivityMonitor.WinApi;

namespace Model.Common.Bcr.EmulatedKeyboard
{
	internal class EmulatedKeyboardBcrManager : IDisposable
	{
		#region Delegates and Events

		public delegate void BcrReadHandler(object sender, BcrReadEventArgs e);

		/// <summary>
		/// Evento scatenato ogni volta che c'è una lettura tramite bcr o tastiera i cui dati letti sono compresi
		/// tra i caratteri di riconoscimento preimpostati
		/// </summary>
		public event BcrReadHandler BcrRead;

		#endregion

		#region Fields

		private bool _started = false;

		private char _hookStartChar = (char)27;//esc
		private char _hookEndChar = (char)13; //enter

		private string _hookBuffer = string.Empty;

		/// <summary>
		/// Permette di validare una stringa secondo le regole dell'hook
		/// La stringa parte da _hookStartChar, ha n caratteri e finisce per _hookEndChar
		/// </summary>
		private Regex _hookRegex;

		// First, a MouseHookListener object must exist in the class
		private KeyboardHookListener _keyboardHook;

		#region Thread

		private readonly Queue<string> _dataQueue = new Queue<string>();
		private readonly ManualResetEvent _hasNewItems = new ManualResetEvent(false);
		private readonly ManualResetEvent _terminate = new ManualResetEvent(false);
		private readonly ManualResetEvent _waiting = new ManualResetEvent(false);

		private Thread _hookThread;

		#endregion

		#endregion

		#region Constructor

		public EmulatedKeyboardBcrManager(char startChar, char endChar)
		{
			UpdateRecognitionChars(startChar, endChar);
		}

		#endregion

		#region Public Methods

		public void Start()
		{
			if (_started)
				return;

			_started = true;

			StartThread();
			ActivateHook();
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Subroutine for activating the hook
		/// </summary>
		private void ActivateHook()
		{
			if (_hookRegex == null)
				throw new Exception($"Invalid call, need to call method {nameof(UpdateRecognitionChars)} first");

			// Note: for an application hook, use the AppHooker class instead
			_keyboardHook = new KeyboardHookListener(new AppHooker());

			// The listener is not enabled by default
			_keyboardHook.Enabled = true;

			// Set the event handler
			// recommended to use the Extended handlers, which allow input suppression among other additional information
			_keyboardHook.KeyPress += KeyboardHookOnKeyPress;
		}

		/// <summary>
		/// Subroutine for deactivating the hook
		/// </summary>
		private void DeactivateHook()
		{
			_keyboardHook?.Dispose();
		}

		/// <summary>
		/// Aggiorna i caratteri di riconoscimento
		/// </summary>
		private void UpdateRecognitionChars(char startChar, char endChar)
		{
			_hookStartChar = startChar;
			_hookEndChar = endChar;

			_hookRegex = new Regex(string.Format("(?<={0})(.*?)(?={1})", _hookStartChar, _hookEndChar));
		}

		#endregion

		#region Private Methods

		private void KeyboardHookOnKeyPress(object sender, KeyPressEventArgs key)
		{
			lock (_hookBuffer)
			{
				_hookBuffer = string.Concat(_hookBuffer, key.KeyChar);

				//se il mio buffer non contiene il carattere di partenza, pulisco ed esco
				if (!_hookBuffer.Contains(_hookStartChar))
				{
					_hookBuffer = string.Empty;
					return;
				}

				//conto le occorrenze che fanno match con il pattern
				var occurrences = _hookRegex.Matches(_hookBuffer);

				if (occurrences.Count <= 0)
				{
					//non ho occorrenze, ancora non ho letto il carattere di fine, esco
					return;
				}

				//ho trovato almeno un'occorrenza di quello che cerco, azzero la stringa di hook
				_hookBuffer = string.Empty;

				//accoda le occorrenze trovate
				EnqueueData(occurrences.Cast<Match>().Select(m => m.Value));
			}
		}

		private void StartThread()
		{
			_hookThread = new Thread(ProcessQueue) { IsBackground = true };
			_hookThread.Start();
		}

		private void StopThread()
		{
			_terminate.Set();
			_hookThread?.Join();
		}

		private void EnqueueData(string datum)
		{
			EnqueueData(new[] { datum });
		}

		/// <summary>
		/// Accoda le occorrenze trovate
		/// </summary>
		private void EnqueueData(IEnumerable<string> data)
		{
			if (data == null)
				throw new ArgumentNullException(nameof(data));

			lock (_dataQueue)
			{
				foreach (var datum in data)
				{
					if (datum == null)
						throw new NullReferenceException(nameof(datum));

					_dataQueue.Enqueue(datum);
				}
			}

			_hasNewItems.Set();
		}

		private int QueueCount()
		{
			lock (_dataQueue)
			{
				return _dataQueue.Count;
			}
		}

		private void ProcessQueue()
		{
			while (true)
			{
				_waiting.Set();

				int i = WaitHandle.WaitAny(new WaitHandle[] { _hasNewItems, _terminate });
				// terminate was signaled 
				if (i == 1 && QueueCount() == 0) return;

				_hasNewItems.Reset();
				_waiting.Reset();

				Queue<string> queueCopy;
				lock (_dataQueue)
				{
					queueCopy = new Queue<string>(_dataQueue);
					_dataQueue.Clear();
				}

				foreach (var datum in queueCopy)
				{
					OnBcrRead(datum);
				}
			}
		}

		private void OnBcrRead(string data)
		{
			//invio evento bcr
			//if (BcrRead != null) BcrRead(this, new BcrReadEventArgs(data));
			BcrRead?.Invoke(this, new BcrReadEventArgs(data));
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			DeactivateHook();
			StopThread();
		}

		#endregion
	}

	public class BcrReadEventArgs : EventArgs
	{
		public string DataRead { get; }

		public BcrReadEventArgs(string dataRead)
		{
			if (dataRead == null)
				throw new ArgumentNullException(nameof(dataRead));

			DataRead = dataRead;
		}
	}
}