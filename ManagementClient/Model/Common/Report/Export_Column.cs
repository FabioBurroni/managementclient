﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media;

namespace Model.Common.Report
{
  [Serializable()]
  public class Export_Column : INotifyPropertyChanged
  {
    [field: NonSerialized]
    public event PropertyChangedEventHandler PropertyChanged;

    protected void NotifyPropertyChanged(string propertyName)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public Export_Column()
    {
    }

    public List<float> SizeL { get; set; } = new List<float>() { 5f, 10f, 15f, 20f, 25f, 30f, 35f, 40f, 45f, 50f };

    private string _Name;

    public string Name
    {
      get { return _Name; }
      set
      {
        if (value != _Name)
        {
          _Name = value;
          NotifyPropertyChanged("Name");
        }
      }
    }

    private string _Description;

    public string Description
    {
      get { return _Description; }
      set
      {
        if (value != _Description)
        {
          _Description = value;
          NotifyPropertyChanged("Description");
        }
      }
    }

    private float _Size;

    public float Size
    {
      get { return _Size; }
      set
      {
        if (value != _Size)
        {
          _Size = value;
          NotifyPropertyChanged("Size");
        }
      }
    }

    private bool _Selected;

    public bool Selected
    {
      get { return _Selected; }
      set
      {
        if (value != _Selected)
        {
          _Selected = value;
          NotifyPropertyChanged("Selected");
        }
      }
    }

    private int _Position;

    public int Position
    {
      get { return _Position; }
      set
      {
        if (value != _Position)
        {
          _Position = value;
          NotifyPropertyChanged("Position");
        }
      }
    }

    private string _Property;

    public string Property
    {
      get { return _Property; }
      set
      {
        if (value != _Property)
        {
          _Property = value;
          NotifyPropertyChanged("Property");
        }
      }
    }

    [field: NonSerialized]
    private Color _Color;

    [field: NonSerialized]
    public Color Color
    {
      get { return _Color; }
      set
      {
        if (value != _Color)
        {
          _Color = value;
          NotifyPropertyChanged("Color");
        }
      }
    }
  }
}