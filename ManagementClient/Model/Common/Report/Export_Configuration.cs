﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Model.Export;
using Localization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Media;
using Configuration;

namespace Model.Common.Report
{

  [Serializable()]
  public class Export_Configuration : INotifyPropertyChanged
  {
    [field: NonSerialized]
    public event PropertyChangedEventHandler PropertyChanged;
    protected void NotifyPropertyChanged(string propertyName)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public Export_Configuration()
    {

    }

    public Export_Configuration(string title, bool landscape, IExportable sample)
    {
      if (sample == null)
        return;

      Title = title;
      Landscape = landscape;

      LoadColumns(sample);

     }

    private bool _Landscape;
    public bool Landscape
    {
      get { return _Landscape; }
      set
      {
        if (value != _Landscape)
        {
          _Landscape = value;
          NotifyPropertyChanged("Landscape");
        }
      }
    }

    private string _Title;
    public string Title
    {
      get { return _Title; }
      set
      {
        if (value != _Title)
        {
          _Title = value;
          NotifyPropertyChanged("Title");
        }
      }
    }

   
    public List<Export_Column> Columns { get; set; } = new List<Export_Column>();

    #region public methods

    #region STATIC
    public static Export_Configuration ReportConfiguration_Read(string reportName)
    {
      Export_Configuration reportConfiguration = null;
      //string reportConfigurationFilePath = System.IO.Path.Combine($"{AppDomain.CurrentDomain.BaseDirectory}", "Conf\\") + $"{reportName}.ser";
      string reportConfigurationFilePath = $"{ConfigurationManager.AppConfDirectory}"+ "\\" + $"{reportName}.ser";
      
      object reportConfigurationObject = null;
      try
      {
        reportConfigurationObject = DeSerialize(reportConfigurationFilePath);
        if (reportConfigurationObject != null)
        {
          reportConfiguration = (Export_Configuration)reportConfigurationObject;
        }
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();
      }

      return reportConfiguration;
    }

    public static bool ReportConfiguration_Save(string reportName, Export_Configuration reportConfiguration)
    {
      return Save(reportName, reportConfiguration); ;
    }
    #endregion

    #region NON STATIC
    public bool Save(string reportName)
    { 
      return Save(reportName, this);
    }

    #endregion

    #endregion

    #region private methods 

    private void LoadColumns(IExportable exportable)
    {
       int pos = 1;
      //scorro tutte le proprietà della classe
      foreach (var property in exportable.GetType().GetProperties())
      {

        //recupero l'attributo per l'esportazione (se presente)
        var expAttr = property.GetCustomAttributes(true).OfType<ExportationAttribute>().SingleOrDefault();
        if (expAttr == null)
          continue;

        Type type = typeof(IExportable);

        Columns.Add(new Export_Column()
        {
          Name = expAttr.ColumnName,  // quello messo negli attributi è quello che si vuole visualizzare
          Description = Localize.LocalizeDefaultString(expAttr.ColumnName), // Localizzato quello messo negli attributi è quello che si vuole visualizzare
          Property = property.Name, //type.GetProperty(property.Name).ToString(),//property.Name,
          Size = 20f,
          Position = pos++,
          Selected = true,
          Color = Color.FromRgb(254, 222, 179)
        });


      }
    }

    private static bool Save(string reportName, Export_Configuration reportConfiguration)
    {
      //string reportConfigurationFilePath = System.IO.Path.Combine($"{AppDomain.CurrentDomain.BaseDirectory}", "Conf\\") + $"{reportName}.ser";
      string reportConfigurationFilePath = $"{ConfigurationManager.AppConfDirectory}" + "\\" + $"{reportName}.ser";

      try
      {
        Serialize(reportConfigurationFilePath, reportConfiguration);
      }
      catch (Exception ex)
      {
        string exc = ex.ToString();

        return false;
      }
      return true;
    }
    
    private static void Serialize(string filePath, object obj)
    {
      // Opens a file and serializes the object into it in binary format.
      try
      {
        Stream stream = File.Open(filePath, FileMode.Create);
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(stream, obj);
        stream.Close();
      }
      catch (Exception ex)
      {
        throw ex;
      }
      // Empties obj.
      //obj = null;
    }

    private static object DeSerialize(string filePath)
    {
      object ret = null;
      try
      {
        Stream stream = File.Open(filePath, FileMode.Open);
        BinaryFormatter formatter = new BinaryFormatter();
        ret = formatter.Deserialize(stream);
        stream.Close();
      }
      catch (Exception ex)
      {
        throw ex;
      }

      return ret;
    }

    #endregion


  }
}
