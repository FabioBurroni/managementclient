﻿
namespace Model.Common
{
  public enum Results_Enum
  {
    UNDEFINED = 0,

    OK = 1,
    //...errori di sistema 
    DATABASE_NOT_CONNECTED = 2,
    STORED_PROCEDURE_EXCEPTION = 3,
    QUERY_EXCEPTION = 4,

    COMMUNICATION_TIMEOUT = 5,         //v2 20170913 
    COMMUNICATION_COMCOUNTERS_NOT_ALIGNED = 6,  //v2 20170913 
    COMMUNICATION_NOT_FOUND = 7,                //v2 20170913 
    COMMUNICATION_DATA_NOT_VALID = 8,      //v2 20170913 

    //...errori tipici dei comandi da 10
    PARAMETER_NUMBER_NOK = 10,
    EXCEPTION_EXECUTING_COMMAND = 11,

    //...posizione da 20
    POSITION_NOT_FOUND = 20,
    POSITION_HAS_NO_COMM = 21,
    NO_REACHABLE_AREA = 22,

    //...shape control da 50
    SHAPECONTROL_WIDTH_NOT_VALID = 51,
    SHAPECONTROL_DEPTH_NOT_VALID = 52,
    SHAPECONTROL_HEIGHT_NOT_VALID = 53,
    SHAPECONTROL_WEIGHT_NOT_VALID = 54,
    SHAPECONTROL_PRODUCT_NOT_VALID = 55,
    SHAPECONTROL_ARTICLE_NOT_VALID = 56,
    SHAPECONTROL_PALLET_NOT_VALID = 57,
    SHAPECONTROL_POSITION_NOT_VALID = 58,
    SHAPECONTROL_NO_AVAILABLE_CELL = 59,
    SHAPECONTROL_NO_AVAILABLE_AREA = 60,
    SHAPECONTROL_NO_REACHABLE_AREA = 61,
    SHAPECONTROL_PALLET_IN_WH = 62,
    SHAPECONTROL_PALLET_FOR_ANTICIPATED_EXIT = 63,
    SHAPECONTROL_LABEL_NOT_VALID = 64,
    SHAPECONTROL_PALLETTYPE_NOT_VALID = 65,
    SHAPECONTROL_SLAT_NOT_COMPLIANT = 66, //...stecche del pallet non conformi

    SHAPECONTROL_GENERIC_ERROR = 70,

    //...engine source da 70
    ENGINE_SOURCE_JOB_ALREADY_PRESENT = 71,
    ENGINE_SOURCE_NO_CONDITION_TO_CREATE_JOB = 72,
    ENGINE_SOURCE_ERROR = 73,
    ENGINE_SOURCE_PALLET_WITHOUT_FINALDEST = 74,
    ENGINE_SOURCE_NO_REACHABLE_AREA = 75,
    ENGINE_SOURCE_NO_AVAILABLE_CELL = 76,
    ENGINE_SOURCE_PALLET_NOT_VALID = 77,
    ENGINE_SOURCE_PALLET_NOT_MAPPED_IN_POSITION = 78, //...il pallet non risulta mappato nella posizione
    ENGINE_SOURCE_POSITION_IS_DISABLED = 79, //...posizione disabilitata

    //...cella da 80
    CELL_NOT_FOUND = 80,
    AREA_NOT_FOUND = 81,

    SOURCE_CELL_IS_NOT_EMPTY = 82,     //20170508 aggiunto per trasferimento pallet
    SOURCE_CELL_IS_EMPTY = 83,
    DESTINATION_CELL_IS_NOT_EMPTY = 84,
    DESTINATION__CELL_IS_EMPTY = 85,


    //...pallet da 90
    PALLET_NOT_FOUND = 90,

    //...mappatura satelliti 100
    SLAVE_CANNOT_MAPPED_IN_MASTER = 100,
    SLAVE_CANNOT_MAPPED_IN_POSITION = 101,
    MASTER_NOT_FOUND = 102,
    MASTER_CANNOT_EXECUTE_COMMAND = 103,
    //...stato master
    MASTER_NOT_IN_MANUAL_STATE = 104,

    MASTER_HAS_A_SLAVE_ON_BOARD = 105,

    //...mappatura pallet in cella da 110
    PALLET_GEOMETRY_NOT_COMPLIANT = 111,
    CELL_CANNOT_HOST_PALLET = 112,

    //...satelliti slave da 120
    SAT_NOT_FOUND = 121,
    SAT_STATUS_NOT_FOUND = 122,
    SAT_ERROR_UPDATING_STATUS = 123,

    //...job da 150
    JOB_NOT_FOUND = 151,
    JOB_ID_NOT_VALID = 152,
    JOB_STATUS_NOT_VALID = 153,
    JOB_ERROR_UPDATING_STATUS = 154,
    JOB_POSSOURCE_NOT_VALID = 155,
    JOB_POSDEST_NOT_VALID = 156,
    JOB_JOBTYPE_NOT_VALID = 157,
    JOB_ERROR_INSERTING = 158,
    JOB_MASTER_NOT_VALID = 159,
    JOB_SLAVESOURCE_NOT_VALID = 160,
    JOB_SLAVEDEST_NOT_VALID = 161,
    JOB_ERROR_UPDATING = 162,
    JOB_PALLET_STATUS_NOT_VALID = 163,
    JOB_PALLET_NOT_FOUND = 164,
    JOB_PALLET_ERROR_INSERTING = 165,
    JOB_PALLET_ALREADY_PRESENT = 166,
    JOB_CANNOT_CREATE_NEW_JOB = 167,
    JOB_ERROR_CREATING_NEW_JOB = 168,
    JOB_ALREADY_PRESENT = 169,

    //...comandi da 170
    CMD_JOB_NOT_FOUND = 171,
    CMD_CMDTYPE_NOT_FOUND = 172,
    CMD_PALLET_NOT_FOUND = 173,
    CMD_SAT_NOT_FOUND = 174,
    CMD_POSITION_NOT_FOUND = 175,
    CMD_WAITSTATE_NOT_FOUND = 176,
    CMD_CMDRESULT_NOT_FOUND = 177,
    CMD_ERROR_INSERT = 178,
    CMD_NOT_FOUND = 179,
    CMD_ERROR_UPDATING = 180,
    CMD_STATUS_NOT_FOUND = 181,
    CMD_ERROR_STATUS_UPDATING = 182,
    CMD_ALREADY_ACKNOWLEDGE = 183,     //...il comando ha già ricevuto l'ack

    //...reservedCell da 190
    SOURCE_CELL_RESERVED = 190,     //20170508 aggiunto per traferimento pallet
    DESTINATION_CELL_RESERVED = 191,       //20170508 aggiunto per traferimento pallet

    //...listManager da 200
    LM_ARTICLE_NOT_FOUND = 201,
    LM_CANNOT_CREATE_JOB = 202,
    LM_FINALDEST_ERROR = 203,
    LM_DESTINATION_CANNOT_HOST_PALLET = 204,
    LM_NO_LCP_TO_SERVE = 205,
    LM_DESTINATION_DISABLED = 206,
    LM_NO_LIST_TO_SERVE = 207,
    LM_CMP_NOT_SCANNED = 208,
    LM_PRELOADLINE_REACHED_MAX_PALLET_IN_TRANIST = 209, //... la linea di preload ha raggiunto il numero massimo di pallet in transit
    LM_DESTINATION_NOT_VALID = 210,
    LM_ONDA_IS_FULL = 211, //...non si può aggiungere altra lista all'onda che contiene il numero massimo di liste
    LM_ONDA_DEST_POS_HAS_MAX_PALLET_IN_TRANSIT = 212, //...la posizione di destinazione ha raggiunto il numero massimo di pallet in transit
    LM_WHOUTPUT_DESTINATION_NOT_VALID = 213, //...la posizione di uscita da  magazzino non è stata trovata
    LM_ONDA_ITEM_COMPLETE = 214, //...l'item dell'onda è completo
    LM_ONDA_REACHED_MAX_ITEM_OPENED = 215, //...l'onda ha già il numero massimo di item aperti
    LM_ONDA_ITEM_NO_PALLET_SELECTABLE = 216, //...nessun pallet è selezionabile
    LM_ONDA_ITEM_ERROR_CREATING_JOB = 217, //...nessun pallet è selezionabile
    LM_ONDA_NO_ITEM_TO_SERVE = 218, //...nessun item dell'onda da servire
    LM_ARTICLE_TEMPORARILY_NOT_AVAILABLE = 219, //...l'articolo è presente, ma al momento non è disponibile
    LM_LCP_SERVED = 220,                         //...il componenente di lista è stato servito
    LM_NO_AVAILABLE_MASTER = 221,             //...nessun master disponibile

    LM_ERROR_PRELOAD_LINE_CANNOT_BE_ASSIGNED = 222, //v2 20170918...la preload line specificata non può essere assegnata
    LM_PRELOAD_LINE_CANNOT_HOST_PALLET = 223, //v2 20170918...la preload line specificata non può ospitare pallet
    LM_MASTER_MAX_WORKLOAD = 224,      //v2 20170918...il master che si vorrebbe usare ha un carico di lavoro massimo

    //...job di parcheggio e di ricarica
    JM_PARKING_NO_AVAILABLE_PARK = 250,       //...nessuna posizione di parcheggio disponibile
    JM_PARKING_SAT_ALREADY_PARKED = 251,      //...il satellite da parcheggiare si trova già in parcheggio
    JM_PARKING_ERROR_CREATING_JOB = 252,      //...si è verificato un errore nella creazione del job
    JM_PARKING_ERROR_UNKNOWN_SOURCE_POSITION_FOR_SLAVE = 253, //...non è stata trovata la posizione sorgente del satellite da parcheggiare
    JM_PARKING_SAT_HAS_ALREADY_PARKING_JOB = 254,
    JM_PARKING_DESTINATION_HAS_ALREADY_SLAVE = 255,
    JM_PARKING_DESTINATION_HAS_ALREADY_JOB = 256,
    JM_PARKING_DESTINATION_NOT_VALID = 257,
    JM_PARKING_SLAVE_IS_CHARGING = 258,      //...il satellite è sotto carica non può essere parcheggiato

    JM_CHARGING_NO_AVAILABLE_CHARGER_POSITION = 260,   //...nessuna posizione di RICARICA disponibile
    JM_CHARGING_SAT_ALREADY_IN_CHARGER_POSITION = 261,   //...il satellite da RICARICARE si trova già nella posizione di ricarica
    JM_CHARGING_ERROR_CREATING_JOB = 262,       //...si è verificato un errore nella creazione del job
    JM_CHARGING_ERROR_UNKNOWN_SOURCE_POSITION_FOR_SLAVE = 263, //...non è stata trovata la posizione sorgente del satellite da parcheggiare
    JM_CHARGING_SAT_HAS_ALREADY_CHARGING_JOB = 264, //...non è stata trovata la posizione sorgente del satellite da parcheggiare
    JM_CHARGING_NO_SAT_ON_POSITION_CHARGER = 265,         //...nessun satellite a bordo della posizione di ricarica
    JM_CHARGING_CHARGER_POSITION_ALREADY_ON = 266,   //...caricatore già attivato
    JM_CHARGING_CHARGER_POSITION_ALREADY_OFF = 267,         //...caricatore già disattivato
    JM_CHARGING_CHARGER_CANNOT_TURNED_OFF = 268,         //...caricatore non può essere spento
    JM_CHARGING_CHARGER_CANNOT_TURNED_ON = 269,         //...caricatore non può essere acceso
    //...ServicePallet da 300
    SERVICE_PALLET_NO_AVAILABLE = 300, //...nessun pallet di servizio disponibile
    SERVICE_PALLET_ERROR_CREATING_NEW = 301, //...errore creazione nuovo pallet di servizio

    //...CheckIn da 400
    CHECKIN_ERROR_PALLET_IN_WH = 400,      //...pallet già presente in magazzino
    CHECKIN_ERROR_PALLET_IN_HANDLING = 401,      //...pallet già presente in handling
    CHECKIN_ERROR_PALLET_NOT_FOUND = 402,     //...pallet non trovato
    CHECKIN_ERROR_SHAPECONTROL_NOT_FOUND = 403,     //...posizione di controllo sagoma n onn trovata
    CHECKIN_ERROR_NO_PALLET_PRESENT_IN_POSITION = 404,    //...nessun pallet presente in posizione 
    CHECKIN_ERROR_COM_TIMEOUT = 405,                //...timeout comunicazione con plc
    CHECKIN_ERROR_COM_NOT_FOUND = 406,
    CHECKIN_ERROR_PALLET_ALREDY_PRESENT = 407,     //...
    CHECKIN_ERROR_PALLET_PRODUCT_GROUP_NOT_FOUND = 408,      //...
    CHECKIN_ARTICLE_NOT_FOUND = 409,      //...
    CHECKIN_PALLETTYPE_NOT_VALID = 410,      //...
    CHECKIN_ERROR_CREATING_PRODUCT = 411,      //...
    CHECKIN_ERROR_CREATING_PALLET = 412,      //...
    CHECKIN_ERROR_INSERTING_PRODUCT = 413,      //...
    CHECKIN_ERROR_UPDATATING_MAPPING = 414,      //...
    CHECKIN_ERROR_DATE_NOT_VALID = 415,      //...
    CHECKIN_ERROR_QTY_NOT_VALID = 416,      //...
    CHECKIN_ERROR_ARTICLE_NOT_FOUND = 417,      //...
    CHECKIN_ERROR_QTY_EXCEED_STANDARD = 418,      //...
    CHECKIN_ERROR_CREATEPALLETDB = 419,      //...
    CHECKIN_ERROR_EVACUATION = 420,      //...
    CHECKIN_ERROR_PALLETINPOS = 421,      //...
    CHECKIN_ERROR_PALLET_ALREADY_IN_MAP = 423,      //..
    CHECKIN_ERROR_PRINTER = 424,
    CHECKIN_ERROR_PRINTER_UNDEFINED = 425,
    CHECKIN_ERROR_BATCH_NOT_FOUND = 426,

    POSITION_ERROR_FRESENIUS_PRINTER = 430,
    POSITION_ERROR_ODMS_PRINTER = 431,
    POSITION_ERROR_FRESENIUS_PRINTER_UNDEFINED = 432,
    POSITION_ERROR_ODMS_PRINTER_UNDEFINED = 433,
    POSITION_ERROR_FRESENIUS_PRINTER_LIST_EMPTY = 434,
    POSITION_ERROR_ODMS_PRINTER_LIST_EMPTY = 435,

    //...order manager da 1000
    OM_DESTINATION_POSITION_NOT_VALID = 1001,
    OM_LISTTYPE_NOT_VALID = 1002,
    OM_WAITSTATE_NOT_FOUND = 1003,
    OM_KILLSTATE_NOT_FOUND = 1004,
    OM_USER_NOT_FOUND = 1005,
    OM_ERROR_INSERTING_ORDER = 1006,
    OM_ERROR_INSERTING_DESTINATION = 1007,
    OM_CMP_WAITSTATE_NOT_FOUND = 1008,
    OM_ARTICLE_NOT_FOUND = 1009,
    OM_QTY_NOT_VALID = 1010,
    OM_ERROR_INSERTING_CMP = 1011,
    OM_ERROR_INSERTING_ASE = 1012,

    OM_ORDER_NOT_FOUND = 1013,
    OM_ORDER_NOT_IN_WAIT_SATATE = 1014,
    OM_DESTINATION_1_NOT_VALID = 1015,
    OM_DESTINATION_2_NOT_VALID = 1016,
    OM_ERROR_CHANGING_DESTINATION = 1017,

    OM_CHANGE_STATE_NOT_ALLOWED = 1018,
    OM_CHANGE_STATE_ERROR = 1019,

    OM_EXECUTION_MODALITY_NOT_VALID = 1020,
    OM_CHANGE_EXECUTION_MODALITY_ERROR = 1021,

    OM_CMP_NOT_FOUND = 1022,
    OM_CMP_STATE_NOT_VALID = 1023,
    OM_CMP_CHANGE_STATE_ERROR = 1024,

    OM_ERROR_ASSIGNING_PRELOAD_LINE = 1025,
    OM_ERROR_PRELOAD_LINE_HAS_ORDER_ASSIGNED = 1026, //...la preload line specificata ha già un ordine assegnato
    OM_ERROR_PRELOAD_LINE_NOT_ASSIGNED = 1027, //...la preload line specificata ha già un ordine assegnato
    OM_ERROR_SEQUENCE_NUMBER_NOT_VALID = 1028,
    OM_ERROR_INSERTING_LIST_INFO = 1029,

    OM_ERROR_UPDATING_ARTICLE = 1030,
    OM_ERROR_UPDATING_ARTICLE_INFO = 1031,

    OM_ARTICLE_NOT_DELETABLE = 1032, //...articolo di sistema non cancellabile
    OM_ERROR_DELETING_ARTICLE_PALLET_IN_SYSTEM = 1033, //...l'articolo appartiene a pallet nel sistema
    OM_ERROR_DELETING_ARTICLE_ORDERS_IN_SYSTEM = 1034, //...l'articolo ha ordini associati
    OM_ERROR_DELETING_ARTICLE = 1035, //...errore cancellazione articolo 

    //...preload line da 1100
    PL_NOT_VALID = 1101,
    PL_HAS_EXECUTING_ORDER = 1102, // ordine in esecuzione per preloadLine
    PL_HAS_PALLET_IN_TRANSIT = 1103, // prealod line ha pallet in transito
    PL_NO_ORDER_ASSIGNED = 1104, // prealod line non ha ordine assegnato
    PL_HAS_SLAVE_ON_LANE = 1105, // la prealod line ha uno slave in corsia

    //...reject pallet
    PALLET_MANUAL_EXPELLED = 1200, // pallet espulso manualmente
    PALLET_MANUAL_REMOVED = 1201, //20170126 pallet rimosso
    PALLET_AUTO_EXPELLED = 1202, //20180925 pallet espulso automaticamente


    //...esportazione dati
    EXPORT_PALLET_OUT_ERROR = 2001,
    EXPORT_PALLET_REJECT_ERROR = 2002,
    EXPORT_PALLET_IN_ERROR = 2003,

    EXPORT_ORDER_ALREADY_EXPORTED = 2004, //...ordine già esportato
    EXPORT_ORDER_ERROR = 2005, //...ordine già esportato

    //20190930 Release 3 aggiunti nuovi codici di errore
    //2100 data fine maggiore di data inizio
    SCHEDULE_ENDTIME_SMALL_STARTTIME = 2100,
    SCHEDULE_NOT_FOUND = 2101,
    SCHEDULE_IS_OVERLAPPED_TO_ANOTHER = 2102,
    SCHEDULE_ERROR_INSERT = 2103,






    //da 3000 login
    LOGIN_NOT_VALID = 3001,
    LOGIN_ERROR_UPDATING_USER = 3002,  //errore aggiornaemnto utente
    LOGIN_ERROR_INSERTING_USER = 3003,  //errore inserimento utente
    LOGIN_USER_NOT_FOUND = 3004,    //errore utente non trovato
    LOGIN_PROFILE_NOT_VALID = 3005,   //errore profilo non valido

    //...rifasamento da 5000
    RESTORE_ERROR = 5000,            //...si è verificato un errroe durante il rifasamento
    RESTORE_ACTIVITY_DELETED = 5001,                         //...attività di rifasamento cancellata
    RESTORE_MASTER_NOT_FOUND = 5002,         //...satellite master nullo
    RESTORE_SLAVE_NOT_FOUND = 5003,         //...satellite master nullo
    RESTORE_ACTIVITY_NOT_FOUND = 5004,        //...nessuna attività di rifasamento per il master
    RESTORE_MASTER_CANNOT_HOST_SLAVE = 5005,      //...il satellite master non può ospitare lo slave
    RESTORE_MASTER_CANNOT_HOST_SLAVE_AND_PALLET = 5006,  //...il satellite master non può ospitare lo slave con il pallet
    RESTORE_MASTER_CANNOT_HOST_PALLET = 5007,      //...il satellite master non può ospitare lo slave
    RESTORE_ERROR_SENDING_COMMAND = 5008,                    //...errore nell invio del comando di rifasamento
    RESTORE_ERROR_CANNOT_CREATE_COMMAND = 5009,              //...errore non è possibile creare il comando
    RESTORE_ERROR_CANNOT_SEND_COMMAND = 5010,                //...errore non è possibile inviare il comando
    RESTORE_ERROR_POSITION_NOT_FOUND = 5011,                 //...errore posizione non trovata
    RESTORE_ERROR_PALLET_NOT_VALID = 5012,       //...errore pallet non valido
    RESTORE_ERROR_PLC_CONFIGURATION = 5013,                  //...errore di configurazione PLC
    RESTORE_ERROR_MASTER_HAS_ALREADY_A_COMMAND = 5014,       //...20170119 errore il master ha un comando in corso e non ne può eseguire un altro.
    RESTORE_ERROR_PALLET_NOT_FOUND = 5015,                   //...20170120 errore pallet non trovato
    RESTORE_ERROR_PALLET_NOT_DETECTED_BY_SENSORS = 5016,     //...20170120 pallet non rilevato dai sensori

  }
}
