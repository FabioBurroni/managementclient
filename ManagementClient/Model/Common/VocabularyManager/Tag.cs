﻿namespace Model.Common.VocabularyManager
{
  public class Tag
  {
    public string lang { get; set; }
    public string value { get; set; }
    public Tag()
    {
      lang = "";
      value = "";
    }
    public Tag(string a, string b)
    {
      lang = a;
      value = b;
    }
  }
}
