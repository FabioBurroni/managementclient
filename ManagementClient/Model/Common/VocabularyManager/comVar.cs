﻿using System.Data;

namespace Model.Common.VocabularyManager
{
  public static class comVar
  {
    public static DataView CurrentData { get; set; }
    public static int row { get; set; }
    public static int col { get; set; }
  }
}
