﻿using System.Windows.Controls;

namespace Model.Common.VocabularyManager
{
  public class CourseValidationRule : ValidationRule
  {
   public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
    {
      try
      {
        string v = comVar.CurrentData[comVar.row].Row[comVar.col].ToString();

        if (v == "" || v == " " || v == null)
        {
          return new ValidationResult(false, "Default column can't be empty.");
        }
        else
        {
          return ValidationResult.ValidResult;
        }
      }
      catch (System.Exception)
      {
        return new ValidationResult(false, "Index Error: Update DataGrid.");
      }
    }
  }
}
