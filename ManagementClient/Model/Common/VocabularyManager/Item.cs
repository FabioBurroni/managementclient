﻿using System.Collections.Generic;

namespace Model.Common.VocabularyManager
{
  public class Item
  {
    public int id { get; set; }
    public string defValue { get; set; }
    public List<Tag> lang { get; set; }

    public Item()
    {
      id = 0;
      defValue = "";
      lang = new List<Tag>();
    }

    public Item(int a, string b, List<Tag> c)
    {
      id = a;
      defValue = b;
      lang.AddRange(c);
    }
  }
}