﻿

namespace Model.Common
{
  public enum ShowDialogResults
  {

    UNDEFINED = 0,
    OK = 1,
    FAIL = 2,    
    CANCELED = 4,
   
  }
}
