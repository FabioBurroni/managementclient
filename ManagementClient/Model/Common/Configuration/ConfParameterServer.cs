﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Common.Configuration
{
  public class ConfParameterServer:ModelBase
  {
    private string _Name;
    public string Name
    {
      get { return _Name; }
      set
      {
        if (value != _Name)
        {
          _Name = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _OriginalValue = string.Empty;
    public string OriginalValue
    {
      get { return _OriginalValue; }
      set
      {
        if (value != _OriginalValue)
        {
          _OriginalValue = value;
          NotifyPropertyChanged();
        }
      }
    }


    private string _Category;
    public string Category
    {
      get { return _Category; }
      set
      {
        if (value != _Category)
        {
          _Category = value;
          NotifyPropertyChanged();
        }
      }
    }


    public override string ToString()
    {
      return $"Name:{Name}, OriginalValue:{OriginalValue}, Category:{Category}";
    }
  }
}
