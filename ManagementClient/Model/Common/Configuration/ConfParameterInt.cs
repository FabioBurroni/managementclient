﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Common.Configuration
{
  public class ConfParameterInt:ConfParameterClient
  {
    public ConfParameterInt()
    {
      ParamType = typeof(int);
    }
    public override object Value
    {
      get { return _Value; }
      set
      {
        if (value != _Value)
        {
          int tmp = 0;
          if (!int.TryParse(value.ToString(), out tmp))
          {
            IsOnError = true;
            return;
          }
          IsOnError = false;
          _Value = value;
          SelectedValue = value.ToString();
          NotifyPropertyChanged();
          NotifyPropertyChanged("IsChanged");
        }
      }
    }
  }
}
