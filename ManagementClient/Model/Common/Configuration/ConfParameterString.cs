﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Common.Configuration
{
  public class ConfParameterString:ConfParameterClient
  {
    public ConfParameterString()
    {
      ParamType = typeof(string);
    }

    public override object Value 
    {
      get { return _Value; }
      set
      {
        base.Value = value;
        SelectedValue = (string)value;
      }
    }
  }
}
