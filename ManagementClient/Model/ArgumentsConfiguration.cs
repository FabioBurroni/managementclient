﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;

namespace Model
{
  public static class ArgumentsConfiguration
  {
    /// <summary>
    /// Indica se è permesso fare un login veloce come cassioli. Il valore di default è false.
    /// </summary>
    public static bool QuickLoginAsCassioli { get; private set; }

    /// <summary>
    /// Indica se il logout è permesso all'operatore. Il valore di default è false.
    /// In caso sia negato, si può solo chiudere l'applicazione (nota bene, il comando di logout viene comunque mandato al server in fase di chiusura)
    /// In caso non sia negato (quindi permesso), dopo aver fatto il logout, riappare l'interfaccia di login.
    /// </summary>
    public static bool DenyLogout { get; private set; }

    /// <summary>
    /// Indica se è abilitato il login veloce nelle posizioni. Se non specificato è nullo e quindi disabilitato.
    /// In caso di presenza del parametro, questo sovrascrive totalmente la configurazione letta da file.
    /// </summary>
    public static QuickLoginInPositions QuickLoginInPositions { get; private set; } = new QuickLoginInPositions();

    #region Internal Methods

    public static void ReadArguments(IList<string> args)
    {
      //nel caso siano stati settati i parametri, li controllo
      if (args != null)
      {
        for (int i = 0; i < args.Count; i++)
        {
          var arg = args[i];

          switch (arg)
          {
            #region case d

            case "/d":
            case "-d":
              {
                //indica se negare o permettere il logout e rimostare l'interfaccia di login oppure non mostare il logout e quindi chiudere sempre l'app 
                DenyLogout = true;
              }
              break;

            #endregion

            #region case l

            case "/l":
            case "-l":
              {
                //login veloce come cassioli
                if (i + 1 < args.Count)
                {
                  QuickLoginAsCassioli = args[i + 1].EqualsIgnoreCase("cassioli");
                }
              }
              break;

            #endregion

            #region case p

            case "/p":
            case "-p":
              {
                //prendo il parametro successivo, se è un numero mi specifica il numero delle posizioni da leggere.
                //se non è presente, allora ho un login in una sola posizione.
                var next = args.ElementAtOrDefault(i + 1);

                int posNr;
                if (string.IsNullOrEmpty(next) || next.StartsWith("/") || next.StartsWith("-"))
                {
                  //il prossimo paramentro è nullo oppure inizia per / o -, allora ho un login veloce senza posizioni
                  posNr = 0;
                }
                else if (!next.TryParse(out posNr))
                {
                  //non sono riuscito a parsare, allora ho una sola posizione
                  posNr = 1;
                }

                var positions = new List<string>();
                for (var j = 0; j < posNr; j++)
                {
                  var pos = args[i + 1 + j];
                  positions.Add(pos);
                }

                QuickLoginInPositions = new QuickLoginInPositions(true, positions);
              }
              break;

              #endregion
          }
        }
      }
    }

    #endregion
  }

  public class QuickLoginInPositions
  {
    public QuickLoginInPositions(bool enabled = false, IList<string> positions = null)
    {
      Enabled = enabled;
      Positions = positions.EmptyIfNull().Distinct(StringComparer.OrdinalIgnoreCase).Select(p => p.Trim()).ToArray();
    }

    public bool Enabled { get; }
    public IList<string> Positions { get; }
  }
}
