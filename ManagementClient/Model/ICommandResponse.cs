﻿using System.Collections.Generic;

namespace Model
{
	public interface ICommandResponse
	{
		/// <summary>
		/// Nome del comando
		/// </summary>
		string CommandName { get; }

		/// <summary>
		/// Parametri del cpmando
		/// </summary>
		IList<string> CommandParameters { get; }

		/// <summary>
		/// Modello della risposta
		/// </summary>
		IModel ResponseModel { get; }
	}
}