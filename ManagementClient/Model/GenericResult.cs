﻿using System;
using System.Linq;
using Utilities.Extensions;

namespace Model
{
	public class GenericResult : ModelBase
	{
		#region Constructor

		public GenericResult(string okNok)
		{
			if (okNok == null)
				throw new ArgumentNullException(nameof(okNok));

			if (!new[] { "ok", "nok" }.Contains(okNok, StringComparer.OrdinalIgnoreCase))
				throw new ArgumentException($"{nameof(okNok)} must have value 'ok' or 'nok'");

			OkNok = okNok;
		}

		#endregion

		#region Properties

		public string OkNok { get; }
		public bool IsOk => OkNok.EqualsIgnoreCase("ok");
		public int? Result { get; set; }
		public string ResultDescr { get; set; }

		#endregion
	}
}